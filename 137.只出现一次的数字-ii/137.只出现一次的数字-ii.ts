/*
 * @lc app=leetcode.cn id=137 lang=typescript
 *
 * [137] 只出现一次的数字 II
 */

// @lc code=start
function singleNumber(nums: number[]): number {
  const map: Map<number, number> = new Map();
  for (const num of nums) {
    map.set(num, (map.get(num) || 0) + 1);
  }

  let result: number;
  for (const [key, val] of map.entries()) {
    if (val === 1) result = key;
  }

  return result;
}
// @lc code=end
