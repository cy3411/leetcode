# 题目
给你一个整数数组 `nums` ，除某个元素仅出现 **一次** 外，其余每个元素都恰出现 **三次** 。请你找出并返回那个只出现了一次的元素。

提示：  
+ $1 <= nums.length <= 3 * 10^4$
+ $-2^{31} <= nums[i] <= 2^{31} - 1$
+ `nums` 中，除某个元素仅出现 **一次** 外，其余每个元素都恰出现 **三次**

# 示例
```
输入：nums = [2,2,3,2]
输出：3
```
# 题解
## 位运算
统计所有数字在某个二进制位是否可以被3整除，如果不可以，代表这个二进制位是只出现1次的那个数字。
```js
function singleNumber(nums: number[]): number {
  let res = 0;
  for (let i = 0; i < 32; i++) {
    let total = 0;
    for (const num of nums) {
      total += (num >> i) & 1;
    }
    if (total % 3 !== 0) {
      res |= 1 << i;
    }
  }

  return res;
}
```
## hash
使用map记录所有数字的出现次数，最后返回出现次数的1的数字。
```js
function singleNumber(nums: number[]): number {
  const map: Map<number, number> = new Map();
  for (const num of nums) {
    map.set(num, (map.get(num) || 0) + 1);
  }

  let result: number;
  for (const [key, val] of map.entries()) {
    if (val === 1) result = key;
  }

  return result;
}
```