# 题目

给定一个二叉树，判断其是否是一个有效的二叉搜索树。

假设一个二叉搜索树具有如下特征：

节点的左子树只包含小于当前节点的数。
节点的右子树只包含大于当前节点的数。
所有左子树和右子树自身必须也是二叉搜索树。

# 示例

```
输入:
    2
   / \
  1   3
输出: true
```

# 题解

## 中序遍历

二叉搜索树的中序遍历是一个单调递增序列。

利用这个特性，再中序遍历过程中，如果前一个节点值>=当前节点值，表示这是一个非有效的二叉搜索树。

```ts
function isValidBST(root: TreeNode | null): boolean {
  let pre: TreeNode | null = null;
  const inorder = (root: TreeNode | null): boolean => {
    if (root === null) return true;
    // 左树无效
    if (!inorder(root.left)) return false;
    // 非递增，表示不是有效二叉搜索树
    if (pre && pre.val >= root.val) return false;
    // 记录前一个结点
    pre = root;
    // 右树无效
    if (!inorder(root.right)) return false;
    return true;
  };
  return inorder(root);
}
```
