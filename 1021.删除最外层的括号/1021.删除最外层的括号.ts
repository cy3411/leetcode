/*
 * @lc app=leetcode.cn id=1021 lang=typescript
 *
 * [1021] 删除最外层的括号
 */

// @lc code=start
function removeOuterParentheses(s: string): string {
  const n = s.length;
  let count = 0;
  let ans = '';
  let start = 0;
  for (let i = 0; i < n; i++) {
    const c = s[i];
    if (c === '(') count++;
    else count--;
    if (count > 0) continue;
    ans += s.slice(start + 1, i);
    start = i + 1;
  }
  return ans;
}
// @lc code=end
