# 题目

有效括号字符串为空 ("")、"(" + A + ")" 或 A + B，其中 A 和 B 都是有效的括号字符串，+ 代表字符串的连接。例如，""，"()"，"(())()" 和 "(()(()))" 都是有效的括号字符串。

如果有效字符串 S 非空，且不存在将其拆分为 S = A+B 的方法，我们称其为原语（primitive），其中 A 和 B 都是非空有效括号字符串。

给出一个非空有效字符串 S，考虑将其进行原语化分解，使得：S = P_1 + P_2 + ... + P_k，其中 P_i 是有效括号字符串原语。

对 S 进行原语化分解，删除分解中每个原语字符串的最外层括号，返回 S 。

提示：

- S.length <= 10000
- S[i] 为 "(" 或 ")"
- S 是一个有效括号字符串

# 示例

```
输入："(()())(())"
输出："()()()"
解释：
输入字符串为 "(()())(())"，原语化分解得到 "(()())" + "(())"，
删除每个部分中的最外层括号后得到 "()()" + "()" = "()()()"。
```

# 方法

## 计数

定义 count：

- 遇见左括号，count 加 1
- 遇见右括号，count-1

当 count 为 0 的时候，肯定是最外层括号

```js
/**
 * @param {string} S
 * @return {string}
 */
var removeOuterParentheses = function (S) {
  // 找非最外层括号
  let count = 0;
  let result = '';
  for (let char of S) {
    if (char === '(' && count++ > 0) {
      result += char;
    } else if (char === ')' && count-- > 1) {
      result += char;
    }
  }

  return result;
};
```

```js
/**
 * @param {string} S
 * @return {string}
 */
var removeOuterParentheses = function (S) {
  // 找最外层括号
  let count = 0;
  let result = '';
  let start = 0;
  for (let i = 0; i < S.length; i++) {
    let char = S[i];
    if (char === '(') count++;
    else count--;
    if (count !== 0) continue;
    result += S.substring(start + 1, i);
    start = i + 1;
  }

  return result;
};
```

```ts
function removeOuterParentheses(s: string): string {
  const n = s.length;
  let count = 0;
  let ans = '';
  let start = 0;
  for (let i = 0; i < n; i++) {
    const c = s[i];
    if (c === '(') count++;
    else count--;
    if (count > 0) continue;
    ans += s.slice(start + 1, i);
    start = i + 1;
  }
  return ans;
}
```
