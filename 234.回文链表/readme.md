# 题目

给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。

提示：

- 链表中节点数目在范围[1, $10^5$] 内
- 0 <= Node.val <= 9

进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？

# 示例

[![59UMZV.png](https://z3.ax1x.com/2021/10/08/59UMZV.png)](https://imgtu.com/i/59UMZV)

```
输入：head = [1,2,2,1]
输出：true
```

# 题解

## 快慢指针+反转链表

回文链表就是从头往后看，和从后往前看，结果都是一样的。

使用快慢指针找到中间位置，快指针每次走二步，慢指针每次走一步，当快指针到走到链表尾部的时候，快指针刚好到中间位置。

我们将慢指针后的链表反转，然后用双指针，一个从头开始，一个从慢指针的下一个节点开始，依次比较。

```ts
function isPalindrome(head: ListNode | null): boolean {
  if (head === null) return false;
  let slow = head;
  let fast = head;
  // 快慢指针，寻找中间位置
  // 这里找的是中间位置的前一个节点
  while (fast.next && fast.next.next) {
    slow = slow.next;
    fast = fast.next.next;
  }
  const reverse = (head: ListNode): ListNode => {
    const ans = new ListNode();
    let p = head;
    let q = null;
    while (p) {
      q = p.next;
      p.next = ans.next;
      ans.next = p;
      p = q;
    }
    return ans.next;
  };

  // slow节点就是中位节点，反转中位后的节点
  slow.next = reverse(slow.next);

  // 双指针比较
  let p = head;
  let q = slow.next;
  while (q) {
    if (p.val !== q.val) return false;
    p = p.next;
    q = q.next;
  }

  return true;
}
```
