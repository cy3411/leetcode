/*
 * @lc app=leetcode.cn id=1413 lang=typescript
 *
 * [1413] 逐步求和得到正数的最小值
 */

// @lc code=start
function minStartValue(nums: number[]): number {
  let sum = 0;
  let min = 0;
  for (const n of nums) {
    sum += n;
    min = Math.min(min, sum);
  }

  return 1 - min;
}
// @lc code=end
