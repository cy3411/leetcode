# 题目
给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。

# 示例
```
输入: 1->1->2
输出: 1->2
```

```
输入: 1->1->2->3->3
输出: 1->2->3
```
# 方法
删除节点，可能涉及到头节点的处理，所以这里需要虚拟头节点来优化边界问题。

定义`prev=dummy,curr=dummy.next`，判断节点是否相同：
+ 不同，2个节点同时后移
+ 相同，移动curr节点，直到找到不相同的位置，将prev和curr后移

```js
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
  if (!head || !head.next) return head;
  // 虚拟头节点
  let dummy = new ListNode(-1);
  dummy.next = head;

  let prev = dummy;
  let curr = dummy.next;

  while (curr && curr.next) {
    // 找出最后一个相等的节点
    if (prev.next.val === curr.next.val) {
      while (curr && curr.next && prev.next.val === curr.next.val) {
        curr = curr.next;
      }
      prev.next = curr;
      prev = prev.next;
      curr = curr.next;
    } else {
      // 节点不同的话同时后移一步
      prev = prev.next;
      curr = curr.next;
    }
  }

  return dummy.next;
};
```