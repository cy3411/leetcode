/*
 * @lc app=leetcode.cn id=83 lang=javascript
 *
 * [83] 删除排序链表中的重复元素
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
  if (!head || !head.next) return head;
  // 虚拟头节点
  let dummy = new ListNode(-1);
  dummy.next = head;

  let prev = dummy;
  let curr = dummy.next;

  while (curr && curr.next) {
    // 找出最后一个相等的节点
    if (prev.next.val === curr.next.val) {
      while (curr && curr.next && prev.next.val === curr.next.val) {
        curr = curr.next;
      }
      prev.next = curr;
      prev = prev.next;
      curr = curr.next;
    } else {
      // 节点不同的话同时后移一步
      prev = prev.next;
      curr = curr.next;
    }
  }

  return dummy.next;
};
// @lc code=end
