/*
 * @lc app=leetcode.cn id=1252 lang=typescript
 *
 * [1252] 奇数值单元格的数目
 */

// @lc code=start
function oddCells(m: number, n: number, indices: number[][]): number {
  const matrix = new Array(m).fill(0).map(() => new Array(n).fill(0));

  for (const [i, j] of indices) {
    for (let x = 0; x < n; x++) {
      matrix[i][x]++;
    }
    for (let y = 0; y < m; y++) {
      matrix[y][j]++;
    }
  }

  let ans = 0;
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (matrix[i][j] & 1) ans++;
    }
  }

  return ans;
}
// @lc code=end
