/*
 * @lc app=leetcode.cn id=1252 lang=cpp
 *
 * [1252] 奇数值单元格的数目
 */

// @lc code=start
class Solution {
public:
    int oddCells(int m, int n, vector<vector<int>> &indices) {
        vector<int> rows(m, 0), cols(n, 0);
        // 统计每行和每列的累加次数
        for (auto &x : indices) {
            rows[x[0]]++;
            cols[x[1]]++;
        }

        int oddx = 0, oddy = 0;
        for (int i = 0; i < m; i++) {
            if (rows[i] & 1) oddx++;
        }
        for (int i = 0; i < n; i++) {
            if (cols[i] & 1) oddy++;
        }

        return oddx * (n - oddy) + oddy * (m - oddx);
    }
};
// @lc code=end
