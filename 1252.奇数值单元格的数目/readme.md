# 题目

给你一个 `m x n` 的矩阵，最开始的时候，每个单元格中的值都是 `0`。

另有一个二维索引数组 `indices`，$indices[i] = [r_i, c_i]$ 指向矩阵中的某个位置，其中 `ri` 和 `ci` 分别表示指定的行和列（从 `0` 开始编号）。

对 `indices[i]` 所指向的每个位置，应同时执行下述增量操作：

- `ri` 行上的所有单元格，加 `1` 。
- `ci` 列上的所有单元格，加 `1` 。

给你 `m`、`n` 和 `indices` 。请你在执行完所有 `indices` 指定的增量操作后，返回矩阵中 `奇数值单元格` 的数目。

提示：

- $1 \leq m, n \leq 50$
- $1 \leq indices.length \leq 100$
- $0 \leq r_i < m$
- $0 \leq c_i < n$

# 示例

[![j26fij.png](https://s1.ax1x.com/2022/07/12/j26fij.png)](https://imgtu.com/i/j26fij)

```
输入：m = 2, n = 3, indices = [[0,1],[1,1]]
输出：6
解释：最开始的矩阵是 [[0,0,0],[0,0,0]]。
第一次增量操作后得到 [[1,2,1],[0,1,0]]。
最后的矩阵是 [[1,3,1],[1,3,1]]，里面有 6 个奇数。
```

[![j26hJs.png](https://s1.ax1x.com/2022/07/12/j26hJs.png)](https://imgtu.com/i/j26hJs)

```
输入：m = 2, n = 2, indices = [[1,1],[0,0]]
输出：0
解释：最后的矩阵是 [[2,2],[2,2]]，里面没有奇数。
```

# 题解

## 模拟

使用一个 $m * n$ 的矩阵来存放结果，遍历 indices，对每个索引执行增量操作，并将结果存入矩阵中。

最后遍历矩阵，将矩阵中的奇数值累加起来。返回累加结果。

```ts
function oddCells(m: number, n: number, indices: number[][]): number {
  const matrix = new Array(m).fill(0).map(() => new Array(n).fill(0));

  for (const [i, j] of indices) {
    for (let x = 0; x < n; x++) {
      matrix[i][x]++;
    }
    for (let y = 0; y < m; y++) {
      matrix[y][j]++;
    }
  }

  let ans = 0;
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (matrix[i][j] & 1) ans++;
    }
  }

  return ans;
}
```

## 技术统计

我们可以统计累加次数为奇数的行数 x (累加次数为偶数的行数为 m - a)，累加次数为计数的列数为 y (累加次数为偶数的列数为 n - y)。

如果想要得到奇数，必然是偶数加上奇数才可以。

综上所述，最终答案为 $x * (n - y) + y * (m - x)$

```cpp
class Solution {
public:
    int oddCells(int m, int n, vector<vector<int>> &indices) {
        vector<int> rows(m, 0), cols(n, 0);
        // 统计每行和每列的累加次数
        for (auto &x : indices) {
            rows[x[0]]++;
            cols[x[1]]++;
        }

        int oddx = 0, oddy = 0;
        for (int i = 0; i < m; i++) {
            if (rows[i] & 1) oddx++;
        }
        for (int i = 0; i < n; i++) {
            if (cols[i] & 1) oddy++;
        }

        return oddx * (n - oddy) + oddy * (m - oddx);
    }
};
```
