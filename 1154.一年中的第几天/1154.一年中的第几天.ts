/*
 * @lc app=leetcode.cn id=1154 lang=typescript
 *
 * [1154] 一年中的第几天
 */

// @lc code=start
function dayOfYear(date: string): number {
  const [year, month, day] = date.split('-').map((item) => Number(item));
  const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  // 判断闰年
  if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
    days[1] = 29;
  }
  return days.slice(0, month - 1).reduce((a, b) => a + b, day);
}
// @lc code=end
