# 题目

给你一个字符串 `date` ，按 `YYYY-MM-DD` 格式表示一个 [现行公元纪年法](https://baike.baidu.com/item/%E5%85%AC%E5%85%83/17855) 日期。请你计算并返回该日期是当年的第几天。

通常情况下，我们认为 1 月 1 日是每年的第 1 天，1 月 2 日是每年的第 2 天，依此类推。每个月的天数与现行公元纪年法（格里高利历）一致。

提示：

- `date.length == 10`
- `date[4] == date[7] == '-'`，其他的 `date[i]` 都是数字
- `date` 表示的范围从 1900 年 1 月 1 日至 2019 年 12 月 31 日

# 示例

```
输入：date = "2019-01-09"
输出：9
```

```
输入：date = "2019-02-10"
输出：41
```

# 题解

## 模拟

预处理每月天数，然后模拟每天的累加。

```ts
function dayOfYear(date: string): number {
  const [year, month, day] = date.split('-').map((item) => Number(item));
  const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  // 判断闰年
  if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
    days[1] = 29;
  }
  return days.slice(0, month - 1).reduce((a, b) => a + b, day);
}
```
