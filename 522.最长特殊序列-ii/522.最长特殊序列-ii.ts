/*
 * @lc app=leetcode.cn id=522 lang=typescript
 *
 * [522] 最长特殊序列 II
 */

// @lc code=start
function findLUSlength(strs: string[]): number {
  const n = strs.length;
  let ans = -1;
  // s1 是否为 s2 的子序列
  const isSubSeq = (s1: string, s2: string) => {
    let i = 0;
    let j = 0;
    while (i < s1.length && j < s2.length) {
      if (s1[i] === s2[j]) {
        i++;
      }
      j++;
    }
    return i === s1.length;
  };

  // 找到最长的不是子序列的序列
  for (let i = 0; i < n; i++) {
    let flag = true;
    for (let j = 0; j < n; j++) {
      // 跳过重复的序列
      if (i === j) continue;
      if (isSubSeq(strs[i], strs[j])) {
        flag = false;
        break;
      }
    }
    // 当前字符串是特殊序列，更新答案
    if (flag) {
      ans = Math.max(ans, strs[i].length);
    }
  }

  return ans;
}
// @lc code=end
