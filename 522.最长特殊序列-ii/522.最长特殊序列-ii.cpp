/*
 * @lc app=leetcode.cn id=522 lang=cpp
 *
 * [522] 最长特殊序列 II
 */

// @lc code=start
class Solution {
public:
    int isSubSeq(string &s1, string &s2) {
        int m = s1.size(), n = s2.size();
        int i = 0, j = 0;
        while (i < m && j < n) {
            if (s1[i] == s2[j]) i++;
            j++;
        }
        return i == m;
    }
    int findLUSlength(vector<string> &strs) {
        int n = strs.size();
        int ans = -1;
        for (int i = 0; i < n; i++) {
            int flag = 1;
            for (int j = 0; j < n; j++) {
                if (i == j) continue;
                if (isSubSeq(strs[i], strs[j])) {
                    flag = 0;
                    break;
                }
            }
            if (flag) {
                ans = max(ans, (int)strs[i].size());
            };
        }
        return ans;
    }
};
// @lc code=end
