# 题目

给定字符串列表 `strs` ，返回其中 **最长的特殊序列** 。如果最长特殊序列不存在，返回 `-1` 。

**特殊序列** 定义如下：该序列为某字符串 **独有的子序列（即不能是其他字符串的子序列）**。

`s` 的**子序列**可以通过删去字符串 `s` 中的某些字符实现。

例如，`"abc"` 是 `"aebdc"` 的子序列，因为您可以删除 `"aebdc"` 中的字符来得到 `"abc"` 。`"aebdc"`的子序列还包括 `"aebdc"` 、 `"aeb"` 和 `""` (空字符串)。

提示:

- $2 \leq strs.length \leq 50$
- $1 \leq strs[i].length \leq 10$
- `strs[i]` 只包含小写英文字母

# 示例

```
输入: strs = ["aba","cdc","eae"]
输出: 3
```

```
输入: strs = ["aaa","aaa","aa"]
输出: -1
```

# 题解

## 枚举

枚举每个字符串，判断其是否是其他字符串的子序列。如果不是，则记录其长度。

```ts
function findLUSlength(strs: string[]): number {
  const n = strs.length;
  let ans = -1;
  // s1 是否为 s2 的子序列
  const isSubSeq = (s1: string, s2: string) => {
    let i = 0;
    let j = 0;
    while (i < s1.length && j < s2.length) {
      if (s1[i] === s2[j]) {
        i++;
      }
      j++;
    }
    return i === s1.length;
  };

  // 找到最长的不是子序列的序列
  for (let i = 0; i < n; i++) {
    let flag = true;
    for (let j = 0; j < n; j++) {
      // 跳过重复的序列
      if (i === j) continue;
      if (isSubSeq(strs[i], strs[j])) {
        flag = false;
        break;
      }
    }
    // 当前字符串是特殊序列，更新答案
    if (flag) {
      ans = Math.max(ans, strs[i].length);
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int isSubSeq(string &s1, string &s2) {
        int m = s1.size(), n = s2.size();
        int i = 0, j = 0;
        while (i < m && j < n) {
            if (s1[i] == s2[j]) i++;
            j++;
        }
        return i == m;
    }
    int findLUSlength(vector<string> &strs) {
        int n = strs.size();
        int ans = -1;
        for (int i = 0; i < n; i++) {
            int flag = 1;
            for (int j = 0; j < n; j++) {
                if (i == j) continue;
                if (isSubSeq(strs[i], strs[j])) {
                    flag = 0;
                    break;
                }
            }
            if (flag) {
                ans = max(ans, (int)strs[i].size());
            };
        }
        return ans;
    }
};
```
