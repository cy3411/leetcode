# 题目

这里有 `n` 门不同的在线课程，按从 `1` 到 `n` 编号。给你一个数组 `courses` ，其中 `courses[i] = [durationi, lastDayi]` 表示第 `i` 门课将会 **持续** 上 `durationi` 天课，并且必须在不晚于 `lastDayi` 的时候完成。

你的学期从第 `1` 天开始。且不能同时修读两门及两门以上的课程。

返回你最多可以修读的课程数目。

提示:

- $\color{goldenrod}1 <= courses.length <= 10^4$
- $\color{goldenrod}1 <= durationi, lastDayi <= 10^4$

# 示例

```
输入：courses = [[100, 200], [200, 1300], [1000, 1250], [2000, 3200]]
输出：3
解释：
这里一共有 4 门课程，但是你最多可以修 3 门：
首先，修第 1 门课，耗费 100 天，在第 100 天完成，在第 101 天开始下门课。
第二，修第 3 门课，耗费 1000 天，在第 1100 天完成，在第 1101 天开始下门课程。
第三，修第 2 门课，耗时 200 天，在第 1300 天完成。
第 4 门课现在不能修，因为将会在第 3300 天完成它，这已经超出了关闭日期。
```

# 题解

## 贪心

题目要求返回最多的课程的数目，所以我们需要尽可能的多选择课程。

为了尽可能多选，需要先选择结束时间最早的课程，所以将 `courses` 按照 `lastDay` 升序排序。

遍历 courses，每次选择一门课程，如果该课程的结束时间没有超过 `lastDay`，则放入结果。

如果该课程的结束时间超过了 `lastDay`，判断该课程的 duration 是否小于结果中最大的 duration，如果小于则替换该课程。

把 duration 更短的替换掉 duration 更长的，这样就能保证总的 duration 最小，可以选择更多的课程。

```ts
function scheduleCourse(courses: number[][]): number {
  const n = courses.length;
  if (n === 0) return 0;
  // 按照lastDay排序
  courses.sort((a, b) => a[1] - b[1]);

  let total = 0;
  let ans: number[] = [];
  for (const [d, l] of courses) {
    // 如果总时间小于等于当前课程的结束时间，加入课程表
    if (total + d <= l) {
      total += d;
      ans.push(d);
    } else if (ans.length > 0 && ans[ans.length - 1] > d) {
      // 如果当前课程的持续时间小于ans中最大的持续时间，则替换ans中最大的持续时间
      total = total - ans.pop() + d;
      ans.push(d);
    }
    // 结果的持续时间排序,这里可以使用大顶堆实现
    ans.sort((a, b) => a - b);
  }

  return ans.length;
}
```

```c++
class Solution
{
public:
    int scheduleCourse(vector<vector<int>> &courses)
    {
        sort(courses.begin(), courses.end(), [](const auto &c0, const auto &c1)
             { return c0[1] < c1[1]; });

        priority_queue<int> q;
        // 优先队列中所有课程的总时间
        int total = 0;

        for (const auto &course : courses)
        {
            int ti = course[0], di = course[1];
            if (total + ti <= di)
            {
                total += ti;
                q.push(ti);
            }
            else if (!q.empty() && q.top() > ti)
            {
                total -= q.top() - ti;
                q.pop();
                q.push(ti);
            }
            printf("%d\n", total);
        }
        return q.size();
    }
};
```
