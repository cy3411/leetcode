/*
 * @lc app=leetcode.cn id=630 lang=typescript
 *
 * [630] 课程表 III
 */

// @lc code=start
function scheduleCourse(courses: number[][]): number {
  const n = courses.length;
  if (n === 0) return 0;
  // 按照lastDay排序
  courses.sort((a, b) => a[1] - b[1]);

  let total = 0;
  let ans: number[] = [];
  for (const [d, l] of courses) {
    // 如果总时间小于等于当前课程的结束时间，加入课程表
    if (total + d <= l) {
      total += d;
      ans.push(d);
    } else if (ans.length > 0 && ans[ans.length - 1] > d) {
      // 如果当前课程的持续时间小于ans中最大的持续时间，则替换ans中最大的持续时间
      total = total - ans.pop() + d;
      ans.push(d);
    }
    // 结果的持续时间排序,这里可以使用大顶堆实现
    ans.sort((a, b) => a - b);
  }

  return ans.length;
}
// @lc code=end
