/*
 * @lc app=leetcode.cn id=551 lang=typescript
 *
 * [551] 学生出勤记录 I
 */

// @lc code=start
function checkRecord(s: string): boolean {
  // 缺勤天数
  let countA = 0;
  // 连续迟到天数
  let countL = 0;
  for (const c of s) {
    if (c === 'A') {
      countA++;
      countL = 0;
      if (countA >= 2) return false;
    } else if (c === 'L') {
      countL++;
      if (countL >= 3) return false;
    } else {
      countL = 0;
    }
  }

  return true;
}
// @lc code=end
