/*
 * @lc app=leetcode.cn id=967 lang=typescript
 *
 * [967] 连续差相同的数字
 */

// @lc code=start
function numsSameConsecDiff(n: number, k: number): number[] {
  // 二叉树遍历，根点是第一个数字
  const dfs = (i: number, num: number): void => {
    // 如果数字长度满足要求，则添加到结果中
    if (i === n) {
      ans.push(num);
      return;
    }
    // 当前个位数字
    const lastDigit = num % 10;
    // 下一个数字差值满足要求
    if (lastDigit - k >= 0) {
      dfs(i + 1, num * 10 + (lastDigit - k));
    }
    // 如果k为0的话，加减操作就属于重复操作，这里需要特判一下
    if (lastDigit + k < 10 && k !== 0) {
      dfs(i + 1, num * 10 + (lastDigit + k));
    }
  };
  const ans = [];
  for (let i = 1; i <= 9; i++) {
    dfs(1, i);
  }

  return ans;
}
// @lc code=end
