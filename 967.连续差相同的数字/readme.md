# 题目

返回所有长度为 `n` 且满足其每两个连续位上的数字之间的差的绝对值为 `k` 的 **非负整数** 。

请注意，**除了** 数字 **0** 本身之外，答案中的每个数字都 **不能** 有前导零。例如，`01` 有一个前导零，所以是无效的；但 `0` 是有效的。

你可以按 **任何顺序** 返回答案。

提示：

- $2 \leq n \leq 9$
- $0 \leq k \leq 9$

# 示例

```
输入：n = 3, k = 7
输出：[181,292,707,818,929]
解释：注意，070 不是一个有效的数字，因为它有前导零。
```

```
输入：n = 2, k = 1
输出：[10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
```

# 题解

## 递归

从 `[1,9]` 范围遍历第一个数字，然后递归计算后面符合条件的数字。当数字的长度达到 `n` 时，将当前结果加入到答案中。

假设 `lastDigit` 为当前数字的最后一位，分别递归 `lastDigit + k` 和 `lastDigit - k` 两个数字。计算的数字要满足 `[0, 9]` 范围。

需要注意一点，如果 `k` 为 `0`，则 `lastDigit + k` 和 `lastDigit - k` 结果一样，需要去重。

```ts
function numsSameConsecDiff(n: number, k: number): number[] {
  // 二叉树遍历，根点是第一个数字
  const dfs = (i: number, num: number): void => {
    // 如果数字长度满足要求，则添加到结果中
    if (i === n) {
      ans.push(num);
      return;
    }
    // 当前个位数字
    const lastDigit = num % 10;
    // 下一个数字差值满足要求
    if (lastDigit - k >= 0) {
      dfs(i + 1, num * 10 + (lastDigit - k));
    }
    // 如果k为0的话，加减操作就属于重复操作，这里需要特判一下
    if (lastDigit + k < 10 && k !== 0) {
      dfs(i + 1, num * 10 + (lastDigit + k));
    }
  };
  const ans = [];
  for (let i = 1; i <= 9; i++) {
    dfs(1, i);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    void dfs(int n, int k, int len, int num, vector<int> &ans)
    {
        if (len == n)
        {
            ans.push_back(num);
            return;
        }
        int lastDigit = num % 10;
        if (lastDigit - k >= 0)
        {
            dfs(n, k, len + 1, num * 10 + (lastDigit - k), ans);
        }
        if (lastDigit + k < 10 && k != 0)
        {
            dfs(n, k, len + 1, num * 10 + (lastDigit + k), ans);
        }
        return;
    }
    vector<int> numsSameConsecDiff(int n, int k)
    {
        vector<int> ans;
        for (int i = 1; i <= 9; i++)
        {
            dfs(n, k, 1, i, ans);
        }

        return ans;
    }
};
```

```cpp
void dfs(int n, int k, int *returnSize, int len, int sum, int *ans)
{
    if (len == n)
    {
        ans[(*returnSize)++] = sum;
        return;
    }
    int lastDigit = sum % 10;
    if (lastDigit - k >= 0)
    {
        dfs(n, k, returnSize, len + 1, sum * 10 + (lastDigit - k), ans);
    }
    if (lastDigit + k < 10 && k != 0)
    {
        dfs(n, k, returnSize, len + 1, sum * 10 + (lastDigit + k), ans);
    }
    return;
}

int *numsSameConsecDiff(int n, int k, int *returnSize)
{
    int *ans = (int *)malloc(sizeof(int) * 10000);
    *returnSize = 0;
    for (int i = 1; i <= 9; i++)
    {
        dfs(n, k, returnSize, 1, i, ans);
    }
    return ans;
}
```
