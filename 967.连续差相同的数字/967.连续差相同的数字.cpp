/*
 * @lc app=leetcode.cn id=967 lang=cpp
 *
 * [967] 连续差相同的数字
 */

// @lc code=start
class Solution
{
public:
    void dfs(int n, int k, int len, int num, vector<int> &ans)
    {
        if (len == n)
        {
            ans.push_back(num);
            return;
        }
        int lastDigit = num % 10;
        if (lastDigit - k >= 0)
        {
            dfs(n, k, len + 1, num * 10 + (lastDigit - k), ans);
        }
        if (lastDigit + k < 10 && k != 0)
        {
            dfs(n, k, len + 1, num * 10 + (lastDigit + k), ans);
        }
        return;
    }
    vector<int> numsSameConsecDiff(int n, int k)
    {
        vector<int> ans;
        for (int i = 1; i <= 9; i++)
        {
            dfs(n, k, 1, i, ans);
        }

        return ans;
    }
};
// @lc code=end
