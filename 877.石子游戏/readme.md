# 题目

亚历克斯和李用几堆石子在做游戏。偶数堆石子排成一行，每堆都有正整数颗石子 `piles[i]` 。

游戏以谁手中的石子最多来决出胜负。石子的总数是奇数，所以没有平局。

亚历克斯和李轮流进行，亚历克斯先开始。 每回合，玩家从行的开始或结束处取走整堆石头。 这种情况一直持续到没有更多的石子堆为止，此时手中石子最多的玩家获胜。

假设亚历克斯和李都发挥出最佳水平，当亚历克斯赢得比赛时返回 `true` ，当李赢得比赛时返回 `false` 。

提示：

- `2 <= piles.length <= 500`
- `piles.length` 是偶数。
- `1 <= piles[i] <= 500`
- `sum(piles)` 是奇数。

# 示例

```
输入：[5,3,4,5]
输出：true
解释：
亚历克斯先开始，只能拿前 5 颗或后 5 颗石子 。
假设他取了前 5 颗，这一行就变成了 [3,4,5] 。
如果李拿走前 3 颗，那么剩下的是 [4,5]，亚历克斯拿走后 5 颗赢得 10 分。
如果李拿走后 5 颗，那么剩下的是 [3,4]，亚历克斯拿走后 4 颗赢得 9 分。
这表明，取前 5 颗石子对亚历克斯来说是一个胜利的举动，所以我们返回 true 。
```

# 题解

## 记忆化递归

题目可知，每次只能从开始或者结束的位置取走石子，这样就保证了剩下石子堆有连续性。

我们可以假设 alex 每次取走开始或者结束的石头，lee 在 `[i+1,j]` 或者 `[j-1,i]` 区间中取石子，判断二者的差值，如果是正数，那么 alex 就必然赢得比赛

```ts
function stoneGame(piles: number[]): boolean {
  const hash: Map<string, number> = new Map();
  // 选择 [i,j] 区间范围
  const dfs = (piles: number[], i: number, j: number): number => {
    if (i === j) return piles[i];
    const key = `${i}#${j}`;
    if (hash.has(key)) return hash.get(key);
    // alex选择i位置,lee选择[i+1,j]区间里的
    // alex选择j位置,lee选择[i,j-1]区间里的
    // 返回最大的差值，表示alex能选择到比lee大的石子
    const result = Math.max(piles[i] - dfs(piles, i + 1, j), piles[j] - dfs(piles, i, j - 1));
    hash.set(key, result);
    return hash.get(key);
  };

  return dfs(piles, 0, piles.length - 1) > 0;
}
```

## 动态规划

**状态**

`dp[i][j]` 表示在区间[i,j]中，双方选择石子数量的最大差值

**base case**

`dp[i][i] = piles[i]`,区间只有 1 个石堆的时候，就只能选择当前石堆了

**选择**

选择第一个石堆 piles[i]，和区间 `[i+1,j]` 的选择相减

选择最后一个石堆 piles[j]，和区间 `[i,j-1]` 的选择相减

取上面的较大值。

$$
dp[i][j] = max(piles[i]-dp[i+1][j],piles[j]-dp[i][j-1])
$$

```ts
function stoneGame(piles: number[]): boolean {
  const size = piles.length;
  // dp[i][j]表示此区间内双方石子数量的最大差值
  const dp = new Array(size).fill(0).map(() => new Array(size).fill(0));

  for (let i = 0; i < size; i++) {
    dp[i][i] = piles[i];
  }

  // 我们要求的结果是dp[0][size-1]，所以这里需要逆序遍历
  for (let i = size - 2; i >= 0; i--) {
    for (let j = i + 1; j < size; j++) {
      dp[i][j] = Math.max(piles[i] - dp[i + 1][j], piles[j] - dp[i][j - 1]);
    }
  }

  return dp[0][size - 1] > 0;
}
```
