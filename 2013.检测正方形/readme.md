# 题目

给你一个在 X-Y 平面上的点构成的数据流。设计一个满足下述要求的算法：

- 添加 一个在数据流中的新点到某个数据结构中。可以添加 重复 的点，并会视作不同的点进行处理。
- 给你一个查询点，请你从数据结构中选出三个点，使这三个点和查询点一同构成一个 面积为正 的 轴对齐正方形 ，统计 满足该要求的方案数目。

轴对齐正方形 是一个正方形，除四条边长度相同外，还满足每条边都与 x-轴 或 y-轴 平行或垂直。

实现 `DetectSquares` 类：

- `DetectSquares()` 使用空数据结构初始化对象
- `void add(int[] point)` 向数据结构添加一个新的点 `point = [x, y]`
- `int count(int[] point`) 统计按上述方式与点 `point = [x, y]` 共同构造 **轴对齐正方形** 的方案数。

提示：

- $\color{burlywood}point.length \equiv 2$
- $\color{burlywood}0 \leq x, y \leq 1000$
- 调用 `add` 和 `count` 的 **总次数** 最多为 `5000`

# 示例

[![7OtJw4.png](https://s4.ax1x.com/2022/01/26/7OtJw4.png)](https://imgtu.com/i/7OtJw4)

```
输入：
["DetectSquares", "add", "add", "add", "count", "count", "add", "count"]
[[], [[3, 10]], [[11, 2]], [[3, 2]], [[11, 10]], [[14, 8]], [[11, 2]], [[11, 10]]]
输出：
[null, null, null, null, 1, 0, null, 2]

解释：
DetectSquares detectSquares = new DetectSquares();
detectSquares.add([3, 10]);
detectSquares.add([11, 2]);
detectSquares.add([3, 2]);
detectSquares.count([11, 10]); // 返回 1 。你可以选择：
                               //   - 第一个，第二个，和第三个点
detectSquares.count([14, 8]);  // 返回 0 。查询点无法与数据结构中的这些点构成正方形。
detectSquares.add([11, 2]);    // 允许添加重复的点。
detectSquares.count([11, 10]); // 返回 2 。你可以选择：
                               //   - 第一个，第二个，和第三个点
                               //   - 第一个，第三个，和第四个点
```

# 题解

## 哈希表

定义 `x`,`y` 为输入的 `point` 的横纵坐标。

则形成正方形的上下两条边中，其中一条边的纵坐标是 `y`，我们通过枚举另一条边的纵坐标 `y'`。则正方形的边长为 $d = |y - y'|, d > 0$ 。

有了一个点的坐标 `x`,`y` 和另一条边的纵坐标 `y'`，我们可以得到正方形的四个点的坐标为：

- `(x, y),(x, y'),(x + d, y),(x + d, y') `
- `(x, y),(x, y'),(x - d, y),(x - d, y')`

通过以上分析，我们可以通过哈希表来记录加入的点，`key` 为 `y`，`value` 为另一个哈希表，保存 `key` 为 `x`，`value` 为这个点出现的次数。

通过枚举匹配的 `y'`，来计算最终的答案。

```ts
class DetectSquares {
  map: Map<number, Map<number, number>> = new Map();
  constructor() {}

  add(point: number[]): void {
    const [x, y] = point;
    if (!this.map.has(y)) {
      this.map.set(y, new Map<number, number>());
    }
    const yMap = this.map.get(y);
    yMap.set(x, (yMap.get(x) || 0) + 1);
  }

  count(point: number[]): number {
    const [x, y] = point;
    // 没有对应的y坐标，无法形成正方形
    if (!this.map.has(y)) return 0;

    const yMap = this.map.get(y);
    const entries = this.map.entries();
    let res = 0;
    // 枚举上下边的另一个y坐标
    for (const [col, cnt] of entries) {
      // 同一行，先跳过。下面会处理
      if (col === y) continue;
      const diff = col - y;
      // 由于会有重复的点，所有这里用乘法来计数
      res += (cnt.get(x) ?? 0) * (yMap.get(x + diff) ?? 0) * (cnt.get(x + diff) ?? 0);
      res += (cnt.get(x) ?? 0) * (yMap.get(x - diff) ?? 0) * (cnt.get(x - diff) ?? 0);
    }

    return res;
  }
}
```

```cpp
class DetectSquares
{
public:
    unordered_map<int, unordered_map<int, int>> cnt;
    DetectSquares()
    {
    }

    void add(vector<int> point)
    {
        int x = point[0], y = point[1];
        cnt[y][x]++;
    }

    int count(vector<int> point)
    {
        int x = point[0], y = point[1];
        if (!cnt.count(y))
        {
            return 0;
        }

        int res = 0;
        unordered_map<int, int> &yCnt = cnt[y];
        for (auto &[col, colCnt] : cnt)
        {
            if (col == y)
                continue;
            int d = col - y;
            res += (colCnt.count(x) ? colCnt[x] : 0) * (yCnt.count(x + d) ? yCnt[x + d] : 0) * (colCnt.count(x + d) ? colCnt[x + d] : 0);
            res += (colCnt.count(x) ? colCnt[x] : 0) * (yCnt.count(x - d) ? yCnt[x - d] : 0) * (colCnt.count(x - d) ? colCnt[x - d] : 0);
        }

        return res;
    }
};
```
