/*
 * @lc app=leetcode.cn id=179 lang=javascript
 *
 * [179] 最大数
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {string}
 */
var largestNumber = function (nums) {
  // 降序排序
  nums.sort((a, b) => {
    let aCarry = 10;
    let bCarry = 10;
    while (aCarry <= a) {
      aCarry *= 10;
    }
    while (bCarry <= b) {
      bCarry *= 10;
    }
    return b * aCarry + a - (a * bCarry + b);
  });
  // 边界条件
  // 排序结果首位为0
  if (nums[0] === 0) return '0';

  return nums.join('');
};
// @lc code=end
