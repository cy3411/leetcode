# 题目
给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。

注意：输出结果可能非常大，所以你需要返回一个字符串而不是整数。

提示：

+ 1 <= nums.length <= 100
+ 0 <= nums[i] <= 109

# 示例
```
输入：nums = [10,2]
输出："210"
```

```
输入：nums = [3,30,34,5,9]
输出："9534330"
```

# 题解
将nums做降序排序，最后的结果转换成字符串就可以了。

重点是排序的规则，我们看一下例子`nums = [10,2]`，例子可以生成的结果是 `102` 和 `210`，明显 `210` 比较大，我们要将 `2` 放到前面。

我们可以将需要比较的整数转成字符串连接起来，然后比较排序。这里字符串的比较首先要**等长**，等长的字符串比较首位从高位开始比较，逐步降低位数比较。字符串大的话，那么对应的整数也大。

```js
/**
 * @param {number[]} nums
 * @return {string}
 */
var largestNumber = function (nums) {
  // 字符串比较，降序排序
  nums.sort((a, b) => {
    return String(b) + String(a) > String(a) + String(b) ? 1 : -1;
  });
  // 边界条件
  // 排序结果首位为0
  if (nums[0] === 0) return '0';

  return nums.join('');
};
```

字符串连接的操作过多，会影响到效率，我们也可以通过计算进位的方式，用整数的比较来实现上面同样规则的排序。

```js
/**
 * @param {number[]} nums
 * @return {string}
 */
var largestNumber = function (nums) {
  // 整数比较，降序排序
  nums.sort((a, b) => {
    let aCarry = 10;
    let bCarry = 10;
    while (aCarry <= a) {
      aCarry *= 10;
    }
    while (bCarry <= b) {
      bCarry *= 10;
    }
    return b * aCarry + a - (a * bCarry + b);
  });
  // 边界条件
  // 排序结果首位为0
  if (nums[0] === 0) return '0';

  return nums.join('');
};
```