# 题目

给你由 `n` 个小写字母字符串组成的数组 `strs`，其中每个字符串长度相等。

这些字符串可以每个一行，排成一个网格。例如，`strs = ["abc", "bce", "cae"]` 可以排列为：

```
abc
bce
cae
```

你需要找出并删除 不是按字典序升序排列的 `列`。在上面的例子（下标从 0 开始）中，列 0（`'a', 'b', 'c'`）和列 2（`'c', 'e', 'e'`）都是按升序排列的，而列 1（`'b', 'c', 'a'`）不是，所以要删除列 1 。

返回你需要删除的列数。

提示：

- $n \equiv strs.length$
- $1 \leq n \leq 100$
- $1 \leq strs[i].length \leq 1000$
- `strs[i]` 由小写英文字母组成

# 示例

```
输入：strs = ["cba","daf","ghi"]
输出：1
解释：网格示意如下：
  cba
  daf
  ghi
列 0 和列 2 按升序排列，但列 1 不是，所以只需要删除列 1 。
```

```
输入：strs = ["a","b"]
输出：0
解释：网格示意如下：
  a
  b
只有列 0 这一列，且已经按升序排列，所以不用删除任何列。
```

# 题解

## 遍历

按列遍历数组，比较相邻列的字符是否是升序。如果不是升序，答案计数加一，同时跳出当前列的遍历。继续其他列的比较。

```ts
function minDeletionSize(strs: string[]): number {
  const m = strs.length;
  const n = strs[0].length;

  let ans = 0;
  // 每一列
  for (let j = 0; j < n; j++) {
    // 按行比较
    for (let i = 0; i < m - 1; i++) {
      if (strs[i][j] > strs[i + 1][j]) {
        ans++;
        break;
      }
    }
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int minDeletionSize(vector<string> &strs)
    {
        int row = strs.size(), col = strs[0].size(), ans = 0;
        for (int i = 0; i < col; i++)
        {
            for (int j = 0; j < row - 1; j++)
            {
                if (strs[j][i] > strs[j + 1][i])
                {
                    ans++;
                    break;
                }
            }
        }
        return ans;
    }
};
```

```python
class Solution:
    def minDeletionSize(self, strs: List[str]) -> int:
        return sum(any(x > y for x, y in pairwise(col)) for col in zip(*strs))
```
