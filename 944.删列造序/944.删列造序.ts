/*
 * @lc app=leetcode.cn id=944 lang=typescript
 *
 * [944] 删列造序
 */

// @lc code=start
function minDeletionSize(strs: string[]): number {
  const m = strs.length;
  const n = strs[0].length;

  let ans = 0;
  // 每一列
  for (let j = 0; j < n; j++) {
    // 按行比较
    for (let i = 0; i < m - 1; i++) {
      if (strs[i][j] > strs[i + 1][j]) {
        ans++;
        break;
      }
    }
  }
  return ans;
}
// @lc code=end
