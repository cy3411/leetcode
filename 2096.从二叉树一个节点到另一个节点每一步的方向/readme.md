# 题目

给你一棵 二叉树 的根节点 root ，这棵二叉树总共有 n 个节点。每个节点的值为 1 到 n 中的一个整数，且互不相同。给你一个整数 startValue ，表示起点节点 s 的值，和另一个不同的整数 destValue ，表示终点节点 t 的值。

请找到从节点 s 到节点 t 的 最短路径 ，并以字符串的形式返回每一步的方向。每一步用 大写 字母 'L' ，'R' 和 'U' 分别表示一种方向：

- 'L' 表示从一个节点前往它的 左孩子 节点。
- 'R' 表示从一个节点前往它的 右孩子 节点。
- 'U' 表示从一个节点前往它的 父 节点。

请你返回从 s 到 t 最短路径 每一步的方向。

提示：

- 树中节点数目为 `n` 。
- $\color{burlywood} 2 \leq n \leq 10^5$
- $1 \leq Node.val \leq n$
- 树中所有节点的值 **互不相同** 。
- $\color{burlywood} 1 \leq startValue, destValue \leq n$
- $\color{burlywood} startValue != destValue$

# 示例

[![H1zHT1.png](https://s4.ax1x.com/2022/02/08/H1zHT1.png)](https://imgtu.com/i/H1zHT1)

```
输入：root = [5,1,2,3,null,6,4], startValue = 3, destValue = 6
输出："UURL"
解释：最短路径为：3 → 1 → 5 → 2 → 6 。
```

# 题解

## 递归

先求出从根节点到起点节点和终点节点的路径。通过两个路径比较，可以看出，两个路径相同的前缀就是两者走过的的重复路径。刨去这个重复路径，就是最短路径。

因为要求的是起点到终点的，所以需要把起点的路径的字符全部改成"U"，然后拼接上终点的路径就行了。

```ts
function getDirections(root: TreeNode | null, startValue: number, destValue: number): string {
  let sPath = '';
  let dPath = '';
  const path = new Array<string>(100);
  const getPathString = (node: TreeNode | null, path: string[] = [], deep: number) => {
    path[deep] = '';
    if (node === null) return;

    if (node.val === startValue) sPath = path.slice(0, deep).join('');
    if (node.val === destValue) dPath = path.slice(0, deep).join('');
    path[deep] = 'L';
    getPathString(node.left, path, deep + 1);
    path[deep] = 'R';
    getPathString(node.right, path, deep + 1);
  };

  // 计算起点和终点的字符串路径
  getPathString(root, path, 0);

  let i = 0;
  // 两个路径相同的前缀代表二者的祖父节点位置
  while (sPath[i] && sPath[i] === dPath[i]) i++;
  // 将相同路径去除后，剩下的就是一条最短路径
  sPath = sPath.slice(i);
  dPath = dPath.slice(i);

  // 将起始节点的路径字符都转成 U
  let sPathRes = '';
  for (const _ of sPath) {
    sPathRes += 'U';
  }
  return sPathRes + dPath;
}
```

```cpp
class Solution
{
    char path[500000];

public:
    void getPathString(TreeNode *root, int startValue, int destValue, int k, string &s_path, string &d_path)
    {
        path[k] = 0;
        if (root == NULL)
        {
            return;
        }
        if (root->val == startValue)
        {
            s_path = path;
        }
        if (root->val == destValue)
        {
            d_path = path;
        }
        path[k] = 'L';
        getPathString(root->left, startValue, destValue, k + 1, s_path, d_path);
        path[k] = 'R';
        getPathString(root->right, startValue, destValue, k + 1, s_path, d_path);
        return;
    }
    string getDirections(TreeNode *root, int startValue, int destValue)
    {
        string s_path, d_path;
        getPathString(root, startValue, destValue, 0, s_path, d_path);

        int i = 0;
        while (s_path[i] && s_path[i] == d_path[i])
        {
            i++;
        }
        s_path = s_path.substr(i, s_path.size());
        d_path = d_path.substr(i, d_path.size());

        for (i = 0; s_path[i]; i++)
        {
            s_path[i] = 'U';
        }

        return s_path + d_path;
    }
};
```
