/*
 * @lc app=leetcode.cn id=2096 lang=cpp
 *
 * [2096] 从二叉树一个节点到另一个节点每一步的方向
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution
{
    char path[500000];

public:
    void getPathString(TreeNode *root, int startValue, int destValue, int k, string &s_path, string &d_path)
    {
        path[k] = 0;
        if (root == NULL)
        {
            return;
        }
        if (root->val == startValue)
        {
            s_path = path;
        }
        if (root->val == destValue)
        {
            d_path = path;
        }
        path[k] = 'L';
        getPathString(root->left, startValue, destValue, k + 1, s_path, d_path);
        path[k] = 'R';
        getPathString(root->right, startValue, destValue, k + 1, s_path, d_path);
        return;
    }
    string getDirections(TreeNode *root, int startValue, int destValue)
    {
        string s_path, d_path;
        getPathString(root, startValue, destValue, 0, s_path, d_path);

        int i = 0;
        while (s_path[i] && s_path[i] == d_path[i])
        {
            i++;
        }
        s_path = s_path.substr(i, s_path.size());
        d_path = d_path.substr(i, d_path.size());

        for (i = 0; s_path[i]; i++)
        {
            s_path[i] = 'U';
        }

        return s_path + d_path;
    }
};
// @lc code=end
