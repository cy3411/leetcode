/*
 * @lc app=leetcode.cn id=1752 lang=typescript
 *
 * [1752] 检查数组是否经排序和轮转得到
 */

// @lc code=start
function check(nums: number[]): boolean {
  const n = nums.length;
  let x = 0;
  // 找到轮转位置
  for (let i = 1; i < n; i++) {
    if (nums[i] < nums[i - 1]) {
      x = i;
      break;
    }
  }
  // 目标数组是升序数组，不用轮转
  if (x === 0) {
    return true;
  }
  // 检查轮转位置的后半部分是否为升序
  for (let i = x + 1; i < n; i++) {
    if (nums[i] < nums[i - 1]) {
      return false;
    }
  }

  return nums[0] >= nums[n - 1];
}
// @lc code=end
