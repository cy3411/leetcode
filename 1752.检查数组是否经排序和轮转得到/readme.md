# 题目

给你一个数组 nums 。nums 的源数组中，所有元素与 nums 相同，但按非递减顺序排列。

如果 nums 能够由源数组轮转若干位置（包括 0 个位置）得到，则返回 true ；否则，返回 false 。

源数组中可能存在 重复项 。

注意：我们称数组 A 在轮转 x 个位置后得到长度相同的数组 B ，当它们满足 `A[i] == B[(i+x) % A.length]` ，其中 % 为取余运算。

提示：

- $1 \leq nums.length \leq 100$
- $1 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [3,4,5,1,2]
输出：true
解释：[1,2,3,4,5] 为有序的源数组。
可以轮转 x = 3 个位置，使新数组从值为 3 的元素开始：[3,4,5,1,2] 。
```

```
输入：nums = [2,1,3,4]
输出：false
解释：源数组无法经轮转得到 nums 。
```

# 题解

## 遍历

题意可以看出，轮转后的数组，分为两个升序数组，且第一个元素大于等于最后一个元素。

遍历数组，找到轮转点，判断轮转点后的数组是否为升序，最后判断首元素是否大于等于最后一个元素。

```ts
function check(nums: number[]): boolean {
  const n = nums.length;
  let x = 0;
  // 找到轮转位置
  for (let i = 1; i < n; i++) {
    if (nums[i] < nums[i - 1]) {
      x = i;
      break;
    }
  }
  // 目标数组是升序数组，不用轮转
  if (x === 0) {
    return true;
  }
  // 检查轮转位置的后半部分是否为升序
  for (let i = x + 1; i < n; i++) {
    if (nums[i] < nums[i - 1]) {
      return false;
    }
  }

  return nums[0] >= nums[n - 1];
}
```

```py
class Solution:
    def check(self, nums: List[int]) -> bool:
        n = len(nums)
        x = 0
        for i in range(1, n):
            if nums[i] < nums[i - 1]:
                x = i
                break
        if x == 0:
            return True
        for i in range(x + 1, n):
            if (nums[i] < nums[i - 1]):
                return False
        return nums[0] >= nums[-1]
```
