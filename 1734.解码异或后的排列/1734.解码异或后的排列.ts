/*
 * @lc app=leetcode.cn id=1734 lang=typescript
 *
 * [1734] 解码异或后的排列
 */

// @lc code=start
function decode(encoded: number[]): number[] {
  let n = encoded.length + 1;
  let total = ((n + 1) / 2) % 2;

  let odd = 0;
  // 获得奇数下标的异或结果
  for (let i = 1; i < encoded.length; i += 2) {
    odd ^= encoded[i];
  }

  const prem = new Array(n).fill(0);
  // 获得原始数组首位的元素
  prem[0] = total ^ odd;
  // 返回原始数组
  for (let i = 1; i < n; i++) {
    prem[i] = prem[i - 1] ^ encoded[i - 1];
  }

  return prem;
}
// @lc code=end
