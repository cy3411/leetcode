# 题目
给你一个整数数组 `perm` ，它是前 `n` 个正整数的排列，且 `n` 是个 奇数 。

它被加密成另一个长度为 `n - 1` 的整数数组 `encoded` ，满足 `encoded[i] = perm[i] XOR perm[i + 1]` 。比方说，如果 `perm = [1,3,2]` ，那么 `encoded = [2,1]` 。

给你 `encoded` 数组，请你返回原始数组 `perm` 。题目保证答案存在且唯一。

提示：
+ 3 <= n < 105
+ n 是奇数。
+ encoded.length == n - 1

# 示例
```
输入：encoded = [3,1]
输出：[1,2,3]
解释：如果 perm = [1,2,3] ，那么 encoded = [1 XOR 2,2 XOR 3] = [3,1]
```

# 题解
## 位运算
要从 `encoded` 数组中，返回原始数组，那就需要知道原始数组中的第一个元素或者最后一个元素才可以。

题目给出了`n`是个奇数，那么 `encoded` 中的元素必然是个偶数。

```
encoded[0] = perm[0]^perm [1]
encoded[1] = perm[1]^perm [2]
encoded[2] = perm[2]^perm [3]
encoded[3] = perm[3]^perm [4]
encoded[4] = perm[4]^perm [5]
encoded[5] = perm[5]^perm [6]
encoded[6] = perm[6]^perm [7]
```
从上面例子可以看出，`encoded` 奇数下标的值，就是 `perm` 中除了下标 `0` 以外的异或结果。

我们可以让 `n` 个数的异或结果和`encoded`奇数下标的异或结果再次异或，就可以得到 `perm[0` 的结果了。

剩下的就是遍历 `encoded` ，来返回原始数组 `perm`。 

```ts
function decode(encoded: number[]): number[] {
  let n = encoded.length + 1;
  let total = 0;
  // 获得n个数的异或结果
  for (let i = 1; i <= n; i++) {
    total ^= i;
  }

  let odd = 0;
  // 获得奇数下标的异或结果
  for (let i = 1; i < encoded.length; i += 2) {
    odd ^= encoded[i];
  }

  const prem = new Array(n).fill(0);
  // 获得原始数组首位的元素
  prem[0] = total ^ odd;
  // 返回原始数组
  for (let i = 1; i < n; i++) {
    prem[i] = prem[i - 1] ^ encoded[i - 1];
  }

  return prem;
}
```
```
1^2^3 = 0
1^2^3^4^5 = 1
1^2^3^4^5^6^7 = 0
1^2^3^4^5^6^7^8^9 = 1
```
观察上面例子，可以看出，当 `n` 是奇数时候，`1-n` 之间的异或结果可以简化一下：`(n + 1) / 2 % 2`。

这样上面的代码就不用多遍历一起了。

```ts
function decode(encoded: number[]): number[] {
  let n = encoded.length + 1;
  
  let total = ((n + 1) / 2) % 2;

  let odd = 0;
  // 获得奇数下标的异或结果
  for (let i = 1; i < encoded.length; i += 2) {
    odd ^= encoded[i];
  }

  const prem = new Array(n).fill(0);
  // 获得原始数组首位的元素
  prem[0] = total ^ odd;
  // 返回原始数组
  for (let i = 1; i < n; i++) {
    prem[i] = prem[i - 1] ^ encoded[i - 1];
  }

  return prem;
}
```

