# 题目
有 `n` 个城市，其中一些彼此相连，另一些没有相连。如果城市 `a` 与城市 `b` 直接相连，且城市 `b` 与城市 `c` 直接相连，那么城市 `a` 与城市 `c` 间接相连。

省份 是一组直接或间接相连的城市，组内不含其他没有相连的城市。

给你一个 `n x n` 的矩阵 `isConnected` ，其中 `isConnected[i][j] = 1` 表示第 `i` 个城市和第 `j` 个城市直接相连，而 `isConnected[i][j] = 0` 表示二者不直接相连。

返回矩阵中 **省份** 的数量。

# 示例
```
输入：isConnected = [[1,1,0],[1,1,0],[0,0,1]]
输出：2
```
# 题解
## 并查集
这个题目考虑是各个城市之间的连通性，这里可以使用并查集来处理。

省份的数量就是最后并查集中集合的总数。
```js
// 并查集模板
class UnionFind {
  parent: number[];
  size: number[];
  count: number;
  constructor(n: number = 0) {
    this.parent = new Array(n).fill(0).map((_, i) => i);
    this.size = new Array(n).fill(1);
    this.count = n;
  }

  find(x: number): number {
    if (x === this.parent[x]) return x;
    this.parent[x] = this.find(this.parent[x]);
    return this.parent[x];
  }

  union(x: number, y: number) {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.parent[rx] = ry;
    this.size[ry] += this.size[rx];
    this.count--;
  }
}

function findCircleNum(isConnected: number[][]): number {
  const size = isConnected.length;
  const unionFind = new UnionFind(size);

  for (let i = 0; i < size; i++) {
    let item = isConnected[i];
    for (let j = 0; j < item.length; j++) {
      if (isConnected[i][j]) {
        unionFind.union(i, j);
      }
    }
  }
  return unionFind.count;
}
```