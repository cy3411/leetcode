/*
 * @lc app=leetcode.cn id=938 lang=typescript
 *
 * [938] 二叉搜索树的范围和
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function rangeSumBST(root: TreeNode | null, low: number, high: number): number {
  let result = 0;
  const inorder = (root: TreeNode | null): void => {
    if (root === null) return;
    inorder(root.left);
    // 结算节点和
    if (root.val >= low && root.val <= high) {
      result += root.val;
    }
    inorder(root.right);
  };
  inorder(root);
  return result;
}
// @lc code=end
