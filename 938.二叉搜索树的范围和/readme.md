# 题目
给定二叉搜索树的根结点 `root`，返回值位于范围 `[low, high]` 之间的所有结点的值的和。

提示：
+ 树中节点数目在范围 [1, 2 * 104] 内
+ 1 <= Node.val <= 105
+ 1 <= low <= high <= 105
+ 所有 Node.val 互不相同

# 示例
![gpqzlV.png](https://z3.ax1x.com/2021/04/27/gpqzlV.png)
```
输入：root = [10,5,15,3,7,null,18], low = 7, high = 15
输出：32
```
# 题解
## 中序遍历
二叉搜索树的中序遍历结果就是一个递增的序列。

可以在遍历过程中计算节点和或者在遍历结果中计算节点和。

```js
function rangeSumBST(root: TreeNode | null, low: number, high: number): number {
  let result = 0;
  const inorder = (root: TreeNode | null): void => {
    if (root === null) return;
    inorder(root.left);
    // 结算节点和
    if (root.val >= low && root.val <= high) {
      result += root.val;
    }
    inorder(root.right);
  };
  inorder(root);
  return result;
}
```