# 题目

请实现两个函数，分别用来序列化和反序列化二叉树。

你需要设计一个算法来实现二叉树的序列化与反序列化。这里不限定你的序列 / 反序列化算法执行逻辑，你只需要保证一个二叉树可以被序列化为一个字符串并且将这个字符串反序列化为原始的树结构。

提示：输入输出格式与 LeetCode 目前使用的方式一致，详情请参阅 LeetCode 序列化二叉树的格式。你并非必须采取这种方式，你也可以采用其他的方法解决这个问题。

# 示例

```
输入：root = [1,2,3,null,null,4,5]
输出：[1,2,3,null,null,4,5]
```

# 题解

## 层序遍历

序列化

- 起始将 root 入队
- 队列中取出节点，判断是否为 null
  - 是，将'null'放入答案种
  - 否，判断是否有左右子树
    - 有子树，继续放入队列
    - 无子树，将 null 放入队列

反序列化

- 起始将第一个元素构建为 node 对象，放入队列
- 循环的时候每次判断连续 2 个值是否为'null'
  - 是的话，跳过这个元素
  - 否的话，创建左右子节点

当前，基本的判空的操作还是要有的。

```js
/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */
var serialize = function (root) {
  if (root === null) return 'null';
  const queue = [];
  const ans = [];
  queue.push(root);

  while (queue.length) {
    const node = queue.shift();
    if (node === null) {
      ans.push('null');
    } else {
      ans.push(node.val);
      queue.push(node.left ? node.left : null);
      queue.push(node.right ? node.right : null);
    }
  }
  return ans.join(',');
};

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function (data) {
  if (data === 'null') return null;
  const arr = data.split(',');
  const root = new TreeNode(Number(arr[0]));
  const queue = [root];
  let i = 1;
  while (queue.length) {
    const node = queue.shift();
    if (arr[i] !== 'null') {
      node.left = new TreeNode(Number(arr[i]));
      queue.push(node.left);
    }
    i++;
    if (arr[i] !== 'null') {
      node.right = new TreeNode(Number(arr[i]));
      queue.push(node.right);
    }
    i++;
  }
  return root;
};
```
