# 题目

给你一个字符串 `s`，请你返回 两个相同字符之间的最长子字符串的长度 ，计算长度时不含这两个字符。如果不存在这样的子字符串，返回 `-1` 。

子字符串 是字符串中的一个连续字符序列。

提示：

- $1 \leq s.length \leq 300$
- `s` 只含小写英文字母

# 示例

```
输入：s = "aa"
输出：0
解释：最优的子字符串是两个 'a' 之间的空子字符串。
```

```
输入：s = "abca"
输出：2
解释：最优的子字符串是 "bc" 。
```

# 题解

## 哈希表

使用哈希表记录每个字符出现的位置，遍历哈希表，当字符出现次数超过 1 次时，计算最小下标和最大下标之间的字符数量，取最大值。

```js
function maxLengthBetweenEqualCharacters(s: string): number {
  const idx = new Map<string, number[]>();
  const n = s.length;
  for (let i = 0; i < n; i++) {
    if (!idx.has(s[i])) {
      idx.set(s[i], []);
    }
    idx.get(s[i])!.push(i);
  }

  let ans = -1;
  for (const [_, v] of idx.entries()) {
    if (v.length > 1) {
      ans = Math.max(ans, v[v.length - 1] - v[0] - 1);
    }
  }

  return ans;
}
```
