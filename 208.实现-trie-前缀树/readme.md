# 题目

Trie（发音类似 "try"）或者说 前缀树 是一种树形数据结构，用于高效地存储和检索字符串数据集中的键。这一数据结构有相当多的应用情景，例如自动补完和拼写检查。

请你实现 Trie 类：

- Trie() 初始化前缀树对象。
- void insert(String word) 向前缀树中插入字符串 word 。
- boolean search(String word) 如果字符串 word 在前缀树中，返回 true（即，在检索之前已经插入）；否则，返回 false 。
- boolean startsWith(String prefix) 如果之前已经插入的字符串 word 的前缀之一为 prefix ，返回 true ；否则，返回 false 。

提示：

- 1 <= word.length, prefix.length <= 2000
- word 和 prefix 仅由小写英文字母组成
- insert、search 和 startsWith 调用次数 总计 不超过 3 \* 104 次

# 示例

```
输入
["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
[[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
输出
[null, null, true, false, true, null, true]

解释
Trie trie = new Trie();
trie.insert("apple");
trie.search("apple");   // 返回 True
trie.search("app");     // 返回 False
trie.startsWith("app"); // 返回 True
trie.insert("app");
trie.search("app");     // 返回 True
```

# 题解

```js
// Tree Node
class TreeNode {
  constructor() {
    // 只有26个小写字母
    this.R = 26;

    this.value = null;
    this.next = new Array(this.R);
  }
}

/**
 * Initialize your data structure here.
 */
var Trie = function () {
  this.base = 'a'.charCodeAt();
  this.root = null;
  /**
   * @description: 查询是否存在key的节点
   * @param {TreeNode} x
   * @param {string} key
   * @param {number} deepth
   * @return {TreeNode} null,带value的node,不带value的node
   */
  this._search = (x, key, deepth) => {
    if (!x) return null;
    if (key.length === deepth) {
      return x;
    }
    const char = key.charCodeAt(deepth) - this.base;
    return this._search(x.next[char], key, deepth + 1);
  };
};

/**
 * Inserts a word into the trie.
 * @param {string} word
 * @return {void}
 */
Trie.prototype.insert = function (word) {
  const _insert = (x, key, value, deepth) => {
    if (!x) x = new TreeNode();
    if (key.length === deepth) {
      x.value = value;
      return x;
    }
    const char = key.charCodeAt(deepth) - this.base;
    x.next[char] = _insert(x.next[char], word, word, deepth + 1);
    return x;
  };

  this.root = _insert(this.root, word, word, 0);
};

/**
 * Returns if the word is in the trie.
 * @param {string} word
 * @return {boolean}
 */
Trie.prototype.search = function (word) {
  // 如果返回的节点有value，那就代表查询到了
  const x = this._search(this.root, word, 0);
  if (x === null) return false;
  return x.value === null ? false : true;
};

/**
 * Returns if there is any word in the trie that starts with the given prefix.
 * @param {string} prefix
 * @return {boolean}
 */
Trie.prototype.startsWith = function (prefix) {
  const x = this._search(this.root, prefix, 0);

  return x === null ? false : true;
};
```

```ts
// 节点
class TrieNode {
  // 当前节点为结尾是否成词
  isWord: boolean;
  // 下一个字符节点
  next: TrieNode[];
  constructor() {
    this.isWord = false;
    this.next = new Array(26).fill(null);
  }
}

class Trie {
  root: TrieNode;
  base: number;
  constructor() {
    this.root = new TrieNode();
    this.base = 'a'.charCodeAt(0);
  }

  insert(word: string): void {
    let node = this.root;
    const base = this.base;
    for (let char of word) {
      const idx = char.charCodeAt(0) - base;
      if (node.next[idx] === null) {
        node.next[idx] = new TrieNode();
      }
      node = node.next[idx];
    }
    node.isWord = true;
  }

  search(word: string): boolean {
    let node = this.root;
    const base = this.base;
    for (let char of word) {
      const idx = char.charCodeAt(0) - base;
      node = node.next[idx];
      if (node === null) return false;
    }
    return node.isWord;
  }

  startsWith(prefix: string): boolean {
    let node = this.root;
    const base = this.base;
    for (let char of prefix) {
      const idx = char.charCodeAt(0) - base;
      node = node.next[idx];
      if (node === null) return false;
    }
    return true;
  }
}
```
