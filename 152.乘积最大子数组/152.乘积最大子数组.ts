/*
 * @lc app=leetcode.cn id=152 lang=typescript
 *
 * [152] 乘积最大子数组
 */

// @lc code=start
function maxProduct(nums: number[]): number {
  let ans = Number.NEGATIVE_INFINITY;
  let max = 1;
  let min = 1;
  for (let n of nums) {
    // 处理负数问题，2个负数相乘的话可能会更大
    if (n < 0) [max, min] = [min, max];
    max = Math.max(n, max * n);
    min = Math.min(n, min * n);
    ans = Math.max(ans, max);
  }
  return ans;
}
// @lc code=end
