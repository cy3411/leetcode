# 题目

给你一个整数数组 `nums` ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。

# 示例

```
输入: [2,3,-2,4]
输出: 6
解释: 子数组 [2,3] 有最大乘积 6。
```

```
输入: [-2,0,-1]
输出: 0
解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
```

# 题解

## 递推

遍历数组，累计乘积，并同步更新答案。

由于数字有负数的存在，所以我们需要同时记录每次乘积的最大和最小值，当判断当前数字是负数的话，我们将之前记录的最大和最小值交换，看看是否能得到更大的数。（负负得正）

```ts
function maxProduct(nums: number[]): number {
  let ans = Number.NEGATIVE_INFINITY;
  let max = 1;
  let min = 1;
  for (let n of nums) {
    // 处理负数问题，2个负数相乘的话可能会更大
    if (n < 0) [max, min] = [min, max];
    max = Math.max(n, max * n);
    min = Math.min(n, min * n);
    ans = Math.max(ans, max);
  }
  return ans;
}
```
