/*
 * @lc app=leetcode.cn id=925 lang=javascript
 *
 * [925] 长按键入
 */

// @lc code=start
/**
 * @param {string} name
 * @param {string} typed
 * @return {boolean}
 */

var isLongPressedName = function (name, typed) {
  let i = 0, j = 0
  const m = name.length, n = typed.length

  while (j < n) {
    if (typed[j] === name[i] && i < m) {
      i++
      j++
    } else if (j > 0 && typed[j] === typed[j - 1]) {
      j++
    } else {
      return false
    }
  }

  return i === m
};
// @lc code=end


/**
 * 时间复杂度：O(N+M)，其中 M,N 分别为两个字符串的长度
 * 空间复杂度：O(1)
 */

