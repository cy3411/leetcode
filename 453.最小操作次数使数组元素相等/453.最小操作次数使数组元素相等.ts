/*
 * @lc app=leetcode.cn id=453 lang=typescript
 *
 * [453] 最小操作次数使数组元素相等
 */

// @lc code=start
function minMoves(nums: number[]): number {
  const min = Math.min(...nums);
  let ans = 0;
  for (let num of nums) {
    // 每个数减少到最小数的次数累加
    ans += num - min;
  }
  return ans;
}
// @lc code=end
