# 题目

给你一个长度为 n 的整数数组，每次操作将会使 n - 1 个元素增加 1 。返回让数组所有元素相等的最小操作次数。

提示：

- `n == nums.length`
- $\color{#cda869}1 \leq nums.length \leq 10^5$
- $\color{#cda869}-10^9 \leq nums[i] \leq10^9$
- 答案保证符合 32-bit 整数

# 示例

```
输入：nums = [1,2,3]
输出：3
解释：
只需要3次操作（注意每次操作会增加两个元素的值）：
[1,2,3]  =>  [2,3,3]  =>  [3,4,3]  =>  [4,4,4]
```

# 题解

## Math

题目给出每次操作都是 `n-1` 个元素加 `1`，那么相对的，也可以理解为 `1` 个元素减 `1`。

要计算数组中所有元素相等的操作次数，只要计算所有元素减少到最小数的操作次数即可。

```ts
function minMoves(nums: number[]): number {
  const min = Math.min(...nums);
  let ans = 0;
  for (let num of nums) {
    // 每个数减少到最小数的次数累加
    ans += num - min;
  }
  return ans;
}
```
