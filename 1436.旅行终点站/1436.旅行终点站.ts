/*
 * @lc app=leetcode.cn id=1436 lang=typescript
 *
 * [1436] 旅行终点站
 */

// @lc code=start
function destCity(paths: string[][]): string {
  // 存储cityA集和
  const cityA = new Set();

  for (let [a] of paths) {
    cityA.add(a);
  }
  // 遍历cityB，没出现在cityA中的是答案
  for (let [_, b] of paths) {
    if (cityA.has(b)) continue;
    return b;
  }

  return '';
}
// @lc code=end
