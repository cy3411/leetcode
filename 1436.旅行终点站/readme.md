# 题目

给你一份旅游线路图，该线路图中的旅行线路用数组 `paths` 表示，其中 $\color{#CDA869}paths[i] = [cityA_i, cityB_i]$ 表示该线路将会从 $\color{#CDA869}cityA_i$ 直接前往 $\color{#CDA869}cityB_i$ 。请你找出这次旅行的终点站，即没有任何可以通往其他城市的线路的城市。

题目数据保证线路图会形成一条不存在循环的线路，因此恰有一个旅行终点站。

提示：

- $\color{#CDA869}1 \leq paths.length \leq 100$
- `paths[i].length == 2`
- $\color{#CDA869}1 \leq cityA_i.length, cityB_i.length \leq 10$
- $\color{#cda869}cityA_i != cityB_i$
- 所有字符串均由大小写英文字母和空格字符组成。

# 示例

```
输入：paths = [["London","New York"],["New York","Lima"],["Lima","Sao Paulo"]]
输出："Sao Paulo"
解释：从 "London" 出发，最后抵达终点站 "Sao Paulo" 。本次旅行的路线是 "London" -> "New York" -> "Lima" -> "Sao Paulo" 。
```

# 题解

## 哈希表

根据题目的定义，终点站不会出现在 cityA 的集合中。

因此我们可以提前将 cityA 存入哈希表中，然后遍历 cityB,没有出现在哈希表中的 cityB 就是答案。

```ts
function destCity(paths: string[][]): string {
  // 存储cityA集和
  const cityA = new Set();

  for (let [a] of paths) {
    cityA.add(a);
  }
  // 遍历cityB，没出现在cityA中的是答案
  for (let [_, b] of paths) {
    if (cityA.has(b)) continue;
    return b;
  }

  return '';
}
```
