/*
 * @lc app=leetcode.cn id=2043 lang=typescript
 *
 * [2043] 简易银行系统
 */

// @lc code=start
class Bank {
  balance: number[];
  n: number;
  constructor(balance: number[]) {
    this.balance = balance;
    this.n = balance.length;
  }

  private isValiteAccount(account: number): boolean {
    return account > 0 && account <= this.n;
  }

  private isValiteMoney(money: number, balance?: number): boolean {
    if (balance !== void 0) {
      return money >= 0 && money <= balance;
    }
    return money >= 0;
  }

  transfer(account1: number, account2: number, money: number): boolean {
    if (!this.isValiteAccount(account1) || !this.isValiteAccount(account2)) {
      return false;
    }
    if (!this.isValiteMoney(money, this.balance[account1 - 1])) {
      return false;
    }
    this.withdraw(account1, money);
    this.deposit(account2, money);
    return true;
  }

  deposit(account: number, money: number): boolean {
    if (!this.isValiteAccount(account)) {
      return false;
    }
    if (!this.isValiteMoney(money)) {
      return false;
    }
    this.balance[account - 1] += money;
    return true;
  }

  withdraw(account: number, money: number): boolean {
    if (!this.isValiteAccount(account)) {
      return false;
    }
    if (!this.isValiteMoney(money, this.balance[account - 1])) {
      return false;
    }
    this.balance[account - 1] -= money;
    return true;
  }
}

/**
 * Your Bank object will be instantiated and called as such:
 * var obj = new Bank(balance)
 * var param_1 = obj.transfer(account1,account2,money)
 * var param_2 = obj.deposit(account,money)
 * var param_3 = obj.withdraw(account,money)
 */
// @lc code=end
