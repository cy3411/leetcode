/*
 * @lc app=leetcode.cn id=2043 lang=cpp
 *
 * [2043] 简易银行系统
 */

// @lc code=start
class Bank
{
    int n;
    vector<long long> data;

    bool isValidAccount(int account)
    {
        return account > 0 && account <= n;
    }

    bool isValidMoney(long long money, long long balance = -1)
    {
        if (balance != -1)
        {
            return money >= 0 && balance >= money;
        }
        return money >= 0;
    }

public:
    Bank(vector<long long> &balance) : data(balance)
    {
        n = balance.size();
    }

    bool transfer(int account1, int account2, long long money)
    {
        if (!isValidAccount(account1) || !isValidAccount(account2) || !isValidMoney(money, data[account1 - 1]))
        {
            return false;
        }

        data[account1 - 1] -= money;
        data[account2 - 1] += money;
        return true;
    }

    bool deposit(int account, long long money)
    {
        if (!isValidAccount(account) || !isValidMoney(money))
        {
            return false;
        }
        data[account - 1] += money;
        return true;
    }

    bool withdraw(int account, long long money)
    {
        if (!isValidAccount(account) || !isValidMoney(money, data[account - 1]))
        {
            return false;
        }
        data[account - 1] -= money;
        return true;
    }
};

/**
 * Your Bank object will be instantiated and called as such:
 * Bank* obj = new Bank(balance);
 * bool param_1 = obj->transfer(account1,account2,money);
 * bool param_2 = obj->deposit(account,money);
 * bool param_3 = obj->withdraw(account,money);
 */
// @lc code=end
