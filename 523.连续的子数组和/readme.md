# 题目

给你一个整数数组 `nums` 和一个整数 `k` ，编写一个函数来判断该数组是否含有同时满足下述条件的连续子数组：

- 子数组大小 **至少为 2** ，且
- 子数组元素总和为 `k` 的倍数。

如果存在，返回 `true` ；否则，返回 `false` 。

如果存在一个整数 `n` ，令整数 `x` 符合 `x = n * k` ，则称 `x` 是 `k` 的一个倍数。

# 示例

```
输入：nums = [23,2,4,6,7], k = 6
输出：true
解释：[2,4] 是一个大小为 2 的子数组，并且和为 6 。
```

```
输入：nums = [23,2,6,4,7], k = 6
输出：true
解释：[23, 2, 6, 4, 7] 是大小为 5 的子数组，并且和为 42 。
42 是 6 的倍数，因为 42 = 7 * 6 且 7 是一个整数。
```

# 题解

## 前缀和
如果 `x % k = u`，那么 `(x + n * k) = u`。

我们可以利用上面的特点来处理逻辑。

使用哈希来保存上一次取模结果和数组下标，然后继续累加前缀和跟 `k` 取模，查询哈希表中是否有重复的：

- 有，代表找到 `k` 的倍数。判断一下下标是否大于等于2即可
- 无，继续迭代

```ts
function checkSubarraySum(nums: number[], k: number): boolean {
  let size = nums.length;
  const hash = new Map([[0, -1]]);

  let prefixSum = 0;

  for (let i = 0; i < size; i++) {
    // 保存取模的结果
    prefixSum = (prefixSum + nums[i]) % k;
    if (hash.has(prefixSum)) {
      let preIdx = hash.get(prefixSum);
      if (i - preIdx >= 2) {
        return true;
      }
    } else {
      hash.set(prefixSum, i);
    }
  }

  return false;
}
```
