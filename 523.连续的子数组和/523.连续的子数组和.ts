/*
 * @lc app=leetcode.cn id=523 lang=typescript
 *
 * [523] 连续的子数组和
 */

// @lc code=start
function checkSubarraySum(nums: number[], k: number): boolean {
  let size = nums.length;
  const hash = new Map([[0, -1]]);

  let prefixSum = 0;

  for (let i = 0; i < size; i++) {
    // 保存取模的结果
    prefixSum = (prefixSum + nums[i]) % k;
    if (hash.has(prefixSum)) {
      let preIdx = hash.get(prefixSum);
      if (i - preIdx >= 2) {
        return true;
      }
    } else {
      hash.set(prefixSum, i);
    }
  }

  return false;
}
// @lc code=end
