/*
 * @lc app=leetcode.cn id=875 lang=typescript
 *
 * [875] 爱吃香蕉的珂珂
 */

// @lc code=start
function minEatingSpeed(piles: number[], h: number): number {
  const getTimes = (speed: number): number => {
    let times = 0;
    for (let i = 0; i < piles.length; i++) {
      times += Math.ceil(piles[i] / speed);
    }
    return times;
  };

  let l = 1;
  let r = Math.max(...piles);
  let mid: number;
  while (l < r) {
    mid = Math.floor((l + r) / 2);
    const speed = getTimes(mid);
    if (speed > h) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }
  return l;
}
// @lc code=end
