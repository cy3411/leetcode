# 题目

珂珂喜欢吃香蕉。这里有 `n` 堆香蕉，第 `i` 堆中有 `piles[i]` 根香蕉。警卫已经离开了，将在 `h` 小时后回来。

珂珂可以决定她吃香蕉的速度 `k` （单位：根/小时）。每个小时，她将会选择一堆香蕉，从中吃掉 `k` 根。如果这堆香蕉少于 `k` 根，她将吃掉这堆的所有香蕉，然后这一小时内不会再吃更多的香蕉。

珂珂喜欢慢慢吃，但仍然想在警卫回来前吃掉所有的香蕉。

返回她可以在 `h` 小时内吃掉所有香蕉的最小速度 `k`（k 为整数）

提示：

- $1 \leq piles.length \leq 10^4$
- $piles.length \leq h \leq 10^9$
- $1 \leq piles[i] \leq 10^9$

# 示例

```
输入：piles = [3,6,7,11], h = 8
输出：4
```

```
输入：piles = [30,11,23,4,20], h = 5
输出：30
```

# 题解

## 二分查找

由于吃香蕉的速度 speed 和是否可以在规定时间内吃完香蕉存在单调性，因此可以使用二分查找。

二分查找每小时吃香蕉的数量，计算出 speed 值，判断当前速度是否满足要求。

```ts
function minEatingSpeed(piles: number[], h: number): number {
  const getTimes = (speed: number): number => {
    let times = 0;
    for (let i = 0; i < piles.length; i++) {
      times += Math.ceil(piles[i] / speed);
    }
    return times;
  };

  let l = 1;
  let r = Math.max(...piles);
  let mid: number;
  while (l < r) {
    mid = Math.floor((l + r) / 2);
    const speed = getTimes(mid);
    if (speed > h) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }
  return l;
}
```
