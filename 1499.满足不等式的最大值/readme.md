# 题目

给你一个数组 points 和一个整数 k 。数组中每个元素都表示二维平面上的点的坐标，并按照横坐标 x 的值从小到大排序。也就是说 points[i] = [xi, yi] ，并且在 1 <= i < j <= points.length 的前提下， xi < xj 总成立。

请你找出 yi + yj + |xi - xj| 的 最大值，其中 |xi - xj| <= k 且 1 <= i < j <= points.length。

题目测试数据保证至少存在一对能够满足 |xi - xj| <= k 的点。

提示：

- 2 <= points.length <= 10^5
- points[i].length == 2
- -10^8 <= points[i][0], points[i][1] <= 10^8
- 0 <= k <= 2 \* 10^8
- 对于所有的 1 <= i < j <= points.length ，points[i][0] < points[j][0] 都成立。也就是说，xi 是严格递增的。

# 示例

```
输入：points = [[1,3],[2,0],[5,10],[6,-10]], k = 1
输出：4
解释：前两个点满足 |xi - xj| <= 1 ，代入方程计算，则得到值 3 + 0 + |1 - 2| = 4 。第三个和第四个点也满足条件，得到值 10 + -10 + |5 - 6| = 1 。
没有其他满足条件的点，所以返回 4 和 1 中最大的那个。
```

# 题解

## 单调队列

题目给出的数据是按照 x 升序排序的，并且在 $1 <= i < j <= points.length$ 的前提下， $x_i < x_j$ 总成立。

所求的结果是 $y_i + y_j + |x_i - x_j|$ 的 **最大值**。

因为$xi < xj$ 总成立，所以上面的公式可以转换成 $x_j+y_j+(y_i-x_i)$。

所以要求得最大值，我们只要找到$y_i$和$x_i$的最大差值即可。维护一个区间的最值，考虑使用单调队列。

题目还要保证区间数据要满足$x_j-x_i<k$,不满足这个条件的数据，我们可以将之移动出队列。

```ts
function findMaxValueOfEquation(points: number[][], k: number): number {
  const m = points.length;
  // 单调递减队列
  const deque = [0];
  let ans = Number.NEGATIVE_INFINITY;

  for (let i = 1; i < m; i++) {
    // 不在k的范围内，不符合计算条件，头部出队
    while (deque.length && points[i][0] - points[deque[0]][0] > k) {
      deque.shift();
    }
    // 如果队列还有元素，计算答案
    if (deque.length) {
      const xi = points[deque[0]][0];
      const yi = points[deque[0]][1];
      const xj = points[i][0];
      const yj = points[i][1];
      ans = Math.max(ans, xj + yj + yi - xi);
    }
    // 不满足单调性，处理队尾出队
    while (
      deque.length &&
      points[i][1] - points[i][0] >
        points[deque[deque.length - 1]][1] - points[deque[deque.length - 1]][0]
    ) {
      deque.pop();
    }
    // 入队
    deque.push(i);
  }

  return ans;
}
```
