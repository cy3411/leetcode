/*
 * @lc app=leetcode.cn id=223 lang=typescript
 *
 * [223] 矩形面积
 */

// @lc code=start
function computeArea(
  ax1: number,
  ay1: number,
  ax2: number,
  ay2: number,
  bx1: number,
  by1: number,
  bx2: number,
  by2: number
): number {
  // 计算2个矩形面积
  const a = (ax2 - ax1) * (ay2 - ay1);
  const b = (bx2 - bx1) * (by2 - by1);
  // 相交区域的宽高
  const overlayWidth = Math.min(ax2, bx2) - Math.max(ax1, bx1);
  const overlayHeight = Math.min(ay2, by2) - Math.max(ay1, by1);
  // 相交面积。不相交的话可能宽高会有负数情况，所以需要跟0取最大值
  const overlay = Math.max(overlayWidth, 0) * Math.max(overlayHeight, 0);
  // 两个矩形面积减去覆盖面积
  return a + b - overlay;
}
// @lc code=end
