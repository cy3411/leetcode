# 题目

给你 二维 平面上两个 **由直线构成的** 矩形，请你计算并返回两个矩形覆盖的总面积。

每个矩形由其 左下 顶点和 右上 顶点坐标表示：

第一个矩形由其左下顶点 `(ax1, ay1)` 和右上顶点 `(ax2, ay2)` 定义。
第二个矩形由其左下顶点 `(bx1, by1)` 和右上顶点 `(bx2, by2)` 定义。

提示：

- -104 <= ax1, ay1, ax2, ay2, bx1, by1, bx2, by2 <= 104

# 示例

[![4TVFYD.md.png](https://z3.ax1x.com/2021/09/30/4TVFYD.md.png)](https://imgtu.com/i/4TVFYD)

```
输入：ax1 = -3, ay1 = 0, ax2 = 3, ay2 = 4, bx1 = 0, by1 = -1, bx2 = 9, by2 = 2
输出：45
```

# 题解

## 容斥原理

我们已知两个矩形的右上角和左下角，计算得到矩形面积。用两个矩形之和减去重叠面积，就是两个矩形覆盖的总面积。

计算交集面积，我们可以先计算两个矩形在坐标轴上的重合长度和宽度，最后即可计算出面积。

```ts
function computeArea(
  ax1: number,
  ay1: number,
  ax2: number,
  ay2: number,
  bx1: number,
  by1: number,
  bx2: number,
  by2: number
): number {
  // 计算2个矩形面积
  const a = (ax2 - ax1) * (ay2 - ay1);
  const b = (bx2 - bx1) * (by2 - by1);
  // 相交区域的宽高
  const overlayWidth = Math.min(ax2, bx2) - Math.max(ax1, bx1);
  const overlayHeight = Math.min(ay2, by2) - Math.max(ay1, by1);
  // 相交面积。不相交的话可能宽高会有负数情况，所以需要跟0取最大值
  const overlay = Math.max(overlayWidth, 0) * Math.max(overlayHeight, 0);
  // 两个矩形面积减去覆盖面积
  return a + b - overlay;
}
```
