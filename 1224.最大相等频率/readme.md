# 题目

给你一个正整数数组 `nums`，请你帮忙从该数组中找出能满足下面要求的 **最长** 前缀，并返回该前缀的长度：

- 从前缀中 **恰好删除一个** 元素后，剩下每个数字的出现次数都相同。

如果删除这个元素后没有剩余元素存在，仍可认为每个数字都具有相同的出现次数（也就是 0 次）。

提示：

- $2 \leq nums.length \leq 10^5$
- $1 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [2,2,1,1,5,3,3,5]
输出：7
解释：对于长度为 7 的子数组 [2,2,1,1,5,3,3]，如果我们从中删去 nums[4] = 5，就可以得到 [2,2,1,1,3,3]，里面每个数字都出现了两次。
```

```
输入：nums = [1,1,1,2,2,2,3,3,3,4,4,4,5]
输出：13
```

# 题解

## 哈希表+计数

使用哈希表 count 记录 nums 中每个数字出现的次数。
使用 freq 记录频率 f 出现的次数。
使用 maxFreq 记录数字最大的出现次数，也就是最大频率。

遍历 nums，同步更新上面三个变量。满足如下条件，就可以更新答案：

- `maxFreq = 1` ，说明说有的数字只出现的一次，随便删除一个即可满足要求。
- 当前所有数字出现的频率都是 `maxFreq`，同时只有 1 个数字的频率是 1 次，那么删除这个数字即可满足要求。
- `maxFreq` 频率出现的次数只有一次，那么找到 `maxFreq - 1` 的数字，删除掉 `maxFreq` 频率的数字一次即可满足要求。

```js
function maxEqualFreq(nums: number[]): number {
  // 每个数字出现的频率
  const count = new Map<number, number>();
  // 每种频率出现的次数
  const freq = new Map<number, number>();

  const n = nums.length;
  let ans = 0;
  let maxFreq = 0;
  for (let i = 0; i < n; i++) {
    const num = nums[i];
    // 初始化
    if (!count.has(num)) {
      count.set(num, 0);
    }
    // 当前数字之前出现的频率
    let c = count.get(num)!;
    // 如果频率次数之前有，这次频率变了，需要将之间的频率计数减少1次
    if (c > 0) {
      freq.set(c, freq.get(c)! - 1);
    }
    // 增加频率计数
    count.set(num, c + 1);
    // 更新最大频率
    maxFreq = Math.max(maxFreq, count.get(num)!);
    // 当前数字新的出现频率
    c = count.get(num)!;
    // 初始化频率出现次数
    if (!freq.has(c)) {
      freq.set(c, 0);
    }
    // 增加频率出现次数
    freq.set(c, freq.get(c)! + 1);

    // 判断状态
    let isOk = false;
    if (maxFreq === 1) {
      // {1,1,1,1} 这种情况
      isOk = true;
    } else if (
      // {1,1,2,2,2} 这种情况
      freq.get(maxFreq)! * maxFreq + freq.get(maxFreq - 1)! * (maxFreq - 1) === i + 1 &&
      freq.get(maxFreq) === 1
    ) {
      isOk = true;
    } else if (freq.get(maxFreq)! * maxFreq + 1 === i + 1 && freq.get(1) === 1) {
      // {1,1,2,2,3} 这种情况
      isOk = true;
    }

    // 更新答案
    if (isOk) ans = Math.max(ans, i + 1);
  }

  return ans;
}
```
