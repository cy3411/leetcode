/*
 * @lc app=leetcode.cn id=1224 lang=typescript
 *
 * [1224] 最大相等频率
 */

// @lc code=start
function maxEqualFreq(nums: number[]): number {
  // 每个数字出现的频率
  const count = new Map<number, number>();
  // 每种频率出现的次数
  const freq = new Map<number, number>();

  const n = nums.length;
  let ans = 0;
  let maxFreq = 0;
  for (let i = 0; i < n; i++) {
    const num = nums[i];
    // 初始化
    if (!count.has(num)) {
      count.set(num, 0);
    }
    // 当前数字之前出现的频率
    let c = count.get(num)!;
    // 如果频率次数之前有，这次频率变了，需要将之间的频率计数减少1次
    if (c > 0) {
      freq.set(c, freq.get(c)! - 1);
    }
    // 增加频率计数
    count.set(num, c + 1);
    // 更新最大频率
    maxFreq = Math.max(maxFreq, count.get(num)!);
    // 当前数字新的出现频率
    c = count.get(num)!;
    // 初始化频率出现次数
    if (!freq.has(c)) {
      freq.set(c, 0);
    }
    // 增加频率出现次数
    freq.set(c, freq.get(c)! + 1);

    // 判断状态
    let isOk = false;
    if (maxFreq === 1) {
      // {1,1,1,1} 这种情况
      isOk = true;
    } else if (
      // {1,1,2,2,2} 这种情况
      freq.get(maxFreq)! * maxFreq + freq.get(maxFreq - 1)! * (maxFreq - 1) === i + 1 &&
      freq.get(maxFreq) === 1
    ) {
      isOk = true;
    } else if (freq.get(maxFreq)! * maxFreq + 1 === i + 1 && freq.get(1) === 1) {
      // {1,1,2,2,3} 这种情况
      isOk = true;
    }

    // 更新答案
    if (isOk) ans = Math.max(ans, i + 1);
  }

  return ans;
}
// @lc code=end
