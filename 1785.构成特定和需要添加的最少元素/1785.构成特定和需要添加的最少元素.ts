/*
 * @lc app=leetcode.cn id=1785 lang=typescript
 *
 * [1785] 构成特定和需要添加的最少元素
 */

// @lc code=start
function minElements(nums: number[], limit: number, goal: number): number {
  let sum = 0;
  // 数组总和
  for (const n of nums) {
    sum += n;
  }
  // 距离目标goal还差多少
  const diff = goal - sum;

  return Math.ceil(Math.abs(diff) / limit);
}
// @lc code=end
