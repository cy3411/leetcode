#
# @lc app=leetcode.cn id=1785 lang=python3
#
# [1785] 构成特定和需要添加的最少元素
#

# @lc code=start
class Solution:
    def minElements(self, nums: List[int], limit: int, goal: int) -> int:
        return (abs(sum(nums) - goal) + limit - 1) // limit
# @lc code=end
