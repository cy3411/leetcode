/*
 * @lc app=leetcode.cn id=1785 lang=cpp
 *
 * [1785] 构成特定和需要添加的最少元素
 */

// @lc code=start
class Solution {
public:
    int minElements(vector<int> &nums, int limit, int goal) {
        long long sum = 0;
        for (int n : nums) {
            sum += n;
        }
        long long diff = abs(goal - sum);

        return (diff + limit - 1) / limit;
    }
};
// @lc code=end
