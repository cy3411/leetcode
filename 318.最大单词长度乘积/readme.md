# 题目

给定一个字符串数组 `words`，找到 `length(word[i]) \* length(word[j])` 的最大值，并且这两个单词不含有公共字母。你可以认为每个单词只包含小写字母。如果不存在这样的两个单词，返回 0。

# 示例

```
输入: ["abcw","baz","foo","bar","xtfn","abcdef"]
输出: 16
解释: 这两个单词为 "abcw", "xtfn"。
```

# 题解

## 哈希表

题目的意思就是找出两个字符串长度最长，并且两个字符串之间没有公共的字符。

我们可以遍历每一种组合，判断两个字符串之间是否有公共字符，如果没有就更新结果。

题目给出了字符串只有小写字母，我们可以通过哈希的方式映射每个小写字符，记录每个字符是否出现过，快速判断两个字符串是否含有公共的字符。

```ts
function maxProduct(words: string[]): number {
  const isCommonChar = (s1: string, s2: string): boolean => {
    const bits = new Array(26).fill(0);
    const base = 'a'.charCodeAt(0);
    for (const c of s1) {
      const index = c.charCodeAt(0) - base;
      bits[index] = 1;
    }
    for (const c of s2) {
      const index = c.charCodeAt(0) - base;
      if (bits[index]) return true;
    }
    return false;
  };

  let ans = 0;
  const size = words.length;
  for (let i = 0; i < size; i++) {
    for (let j = i + 1; j < size; j++) {
      if (isCommonChar(words[i], words[j])) continue;
      ans = Math.max(ans, words[i].length * words[j].length);
    }
  }

  return ans;
}
```
