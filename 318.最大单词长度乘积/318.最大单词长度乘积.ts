/*
 * @lc app=leetcode.cn id=318 lang=typescript
 *
 * [318] 最大单词长度乘积
 */

// @lc code=start
function maxProduct(words: string[]): number {
  const isCommonChar = (s1: string, s2: string): boolean => {
    const bits = new Array(26).fill(0);
    const base = 'a'.charCodeAt(0);
    for (const c of s1) {
      const index = c.charCodeAt(0) - base;
      bits[index] = 1;
    }
    for (const c of s2) {
      const index = c.charCodeAt(0) - base;
      if (bits[index]) return true;
    }
    return false;
  };

  let ans = 0;
  const size = words.length;
  for (let i = 0; i < size; i++) {
    for (let j = i + 1; j < size; j++) {
      if (isCommonChar(words[i], words[j])) continue;
      ans = Math.max(ans, words[i].length * words[j].length);
    }
  }

  return ans;
}
// @lc code=end
