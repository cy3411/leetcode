# 题目

树是一个无向图，其中任何两个顶点只通过一条路径连接。 换句话说，一个任何没有简单环路的连通图都是一棵树。

给你一棵包含 `n` 个节点的树，标记为 `0` 到 `n - 1` 。给定数字 `n` 和一个有 `n - 1` 条无向边的 `edges` 列表（每一个边都是一对标签），其中 $edges[i] = [a_i, b_i]$ 表示树中节点 $a_i$ 和 $b_i$ 之间存在一条无向边。

可选择树中任何一个节点作为根。当选择节点 `x` 作为根节点时，设结果树的高度为 `h` 。在所有可能的树中，具有最小高度的树（即，`min(h)`）被称为 **最小高度树** 。

请你找到所有的 **最小高度树** 并按 **任意顺序** 返回它们的根节点标签列表。

树的 **高度** 是指根节点和叶子节点之间最长向下路径上边的数量。

提示：

- $1 \leq n \leq 2 * 10^4$
- $edges.length \equiv n - 1$
- $0 \leq a_i, b_i < n$
- $a_i \not = b_i$
- 所有 $(a_i, b_i)$ 互不相同
- 给定的输入 **保证** 是一棵树，并且 **不会有重复的边**

# 示例

[![qv1mNR.png](https://s1.ax1x.com/2022/04/06/qv1mNR.png)](https://imgtu.com/i/qv1mNR)

```
输入：n = 4, edges = [[1,0],[1,2],[1,3]]
输出：[1]
解释：如图所示，当根是标签为 1 的节点时，树的高度是 1 ，这是唯一的最小高度树。
```

[![qv1sbQ.png](https://s1.ax1x.com/2022/04/06/qv1sbQ.png)](https://imgtu.com/i/qv1sbQ)

```
输入：n = 6, edges = [[3,0],[3,1],[3,2],[3,4],[5,4]]
输出：[3,4]
```

# 题解

## 拓扑排序

要想使树的高度最低，根节点尽量是中间的节点，那么就需要把树拓扑排序，从最外层节点开始，一层一层往里访问，最后一次访问的就是最小高度数的根节点。

```ts
function findMinHeightTrees(n: number, edges: number[][]): number[] {
  const ans: number[] = [];
  // 只有一个节点
  if (n === 1) {
    ans.push(0);
    return ans;
  }
  // 初始化邻接表和节点出度
  const graph = new Array(n).fill(0).map(() => []);
  const outDegree = new Array(n).fill(0);
  for (const [u, v] of edges) {
    outDegree[u]++;
    outDegree[v]++;
    graph[u].push(v);
    graph[v].push(u);
  }
  // 广度优先搜索
  const queue: number[] = [];
  // 将出度为1的节点放入队列
  for (let i = 0; i < n; i++) {
    if (outDegree[i] === 1) queue.push(i);
  }
  // 最后一层节点是中间节点，也就是最小高度树的根节点
  while (queue.length) {
    const size = queue.length;
    ans.length = 0;
    for (let i = 0; i < size; i++) {
      const node = queue.shift();
      ans.push(node);
      for (const neighbor of graph[node]) {
        outDegree[neighbor]--;
        if (outDegree[neighbor] === 1) queue.push(neighbor);
      }
    }
  }

  return ans;
}
```
