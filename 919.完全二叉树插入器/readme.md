# 题目

**完全二叉树** 是每一层（除最后一层外）都是完全填充（即，节点数达到最大）的，并且所有的节点都尽可能地集中在左侧。

设计一种算法，将一个新节点插入到一个完整的二叉树中，并在插入后保持其完整。

实现 `CBTInserter` 类:

- `CBTInserter(TreeNode root)` 使用头节点为 `root` 的给定树初始化该数据结构；
- `CBTInserter.insert(int v)` 向树中插入一个值为 `Node.val == val` 的新节点 `TreeNode`。使树保持完全二叉树的状态，并返回插入节点 `TreeNode` 的父节点的值；
- `CBTInserter.get_root()` 将返回树的头节点。

提示：

- 树中节点数量范围为 `[1, 1000] `
- $0 \leq Node.val \leq 5000$
- `root` 是完全二叉树
- $0 \leq val \leq 5000$
- 每个测试用例最多调用 `insert` 和 `get_root` 操作 `10^4` 次

# 示例

[![jxdkzq.png](https://s1.ax1x.com/2022/07/25/jxdkzq.png)](https://imgtu.com/i/jxdkzq)

```
输入
["CBTInserter", "insert", "insert", "get_root"]
[[[1, 2]], [3], [4], []]
输出
[null, 1, 2, [1, 2, 3, 4]]

解释
CBTInserter cBTInserter = new CBTInserter([1, 2]);
cBTInserter.insert(3);  // 返回 1
cBTInserter.insert(4);  // 返回 2
cBTInserter.get_root(); // 返回 [1, 2, 3, 4]
```

# 题解

## 队列+广度优先搜索

完全二叉树可以插入节点的地方只有倒数第二层靠右边的节点和最后一层的节点。

我们可以层序遍历出满足插入条件的节点，然后存到队列中。

执行 insert 的时候，按照队列顺序找到父节点，然后插入节点。如果当前父节点有左右子节点后，则将父节点移除队列。新节点同时插入到队列后面。

```ts
class CBTInserter {
  root: TreeNode | null;
  // 没有左右子节点的节点
  candidate: TreeNode[] = [];
  constructor(root: TreeNode | null) {
    this.root = root;
    const queue = [root];
    while (queue.length) {
      const node = queue.shift();
      if (node.left) {
        queue.push(node.left);
      }
      if (node.right) {
        queue.push(node.right);
      }
      if (!node.right) {
        this.candidate.push(node);
      }
    }
  }

  insert(val: number): number {
    const child = new TreeNode(val);
    const parent = this.candidate[0];
    if (!parent.left) {
      parent.left = child;
    } else {
      parent.right = child;
      this.candidate.shift();
    }
    this.candidate.push(child);
    return parent.val;
  }

  get_root(): TreeNode | null {
    return this.root;
  }
}
```

```cpp
class CBTInserter {
private:
    TreeNode *root;
    vector<TreeNode *> candidate;

public:
    void getCandidate(TreeNode *root) {
        queue<TreeNode *> que;
        que.push(root);
        while (!que.empty()) {
            TreeNode *node = que.front();
            que.pop();
            if (node->left) que.push(node->left);
            if (node->right) que.push(node->right);
            if (!node->right) {
                candidate.push_back(node);
            }
        }
    }
    CBTInserter(TreeNode *root) {
        this->root = root;
        this->getCandidate(root);
    }

    int insert(int val) {
        TreeNode *child = new TreeNode(val);
        TreeNode *parent = candidate.front();
        if (!parent->left) {
            parent->left = child;
        } else {
            parent->right = child;
            this->candidate.erase(this->candidate.begin());
        }
        this->candidate.push_back(child);
        return parent->val;
    }

    TreeNode *get_root() { return this->root; }
};
```

```py
class CBTInserter:

    def __init__(self, root: TreeNode):
        self.root = root
        self.candidate = deque()

        que = deque([root])
        while que:
            node = que.popleft()
            if node.left:
                que.append(node.left)
            if node.right:
                que.append(node.right)
            if not node.right:
                self.candidate.append(node)

    def insert(self, val: int) -> int:
        child = TreeNode(val)
        parent = self.candidate[0]
        if not parent.left:
            parent.left = child
        else:
            parent.right = child
            self.candidate.popleft()

        self.candidate.append(child)

        return parent.val

    def get_root(self) -> TreeNode:
        return self.root
```
