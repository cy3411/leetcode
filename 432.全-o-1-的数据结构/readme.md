# 题目

请你设计一个用于存储字符串计数的数据结构，并能够返回计数最小和最大的字符串。

实现 `AllOne` 类：

- `AllOne()` 初始化数据结构的对象。
- `inc(String key)` 字符串 `key` 的计数增加 `1` 。如果数据结构中尚不存在 `key` ，那么插入计数为 `1` 的 `key` 。
- `dec(String key)` 字符串 `key` 的计数减少 `1` 。如果 `key` 的计数在减少后为 `0` ，那么需要将这个 `key` 从数据结构中删除。测试用例保证：在减少计数前，`key` 存在于数据结构中。
- `getMaxKey()` 返回任意一个计数最大的字符串。如果没有元素存在，返回一个空字符串 "" 。
- `getMinKey()` 返回任意一个计数最小的字符串。如果没有元素存在，返回一个空字符串 "" 。

提示：

- $1 <= key.length <= 10$
- `key` 由小写英文字母组成
- 测试用例保证：在每次调用 `dec` 时，数据结构中总存在 `key`
- 最多调用 `inc`、`dec`、`getMaxKey` 和 `getMinKey` 方法 $5 * 10^4$ 次

# 示例

```
输入
["AllOne", "inc", "inc", "getMaxKey", "getMinKey", "inc", "getMaxKey", "getMinKey"]
[[], ["hello"], ["hello"], [], [], ["leet"], [], []]
输出
[null, null, null, "hello", "hello", null, "hello", "leet"]

解释
AllOne allOne = new AllOne();
allOne.inc("hello");
allOne.inc("hello");
allOne.getMaxKey(); // 返回 "hello"
allOne.getMinKey(); // 返回 "hello"
allOne.inc("leet");
allOne.getMaxKey(); // 返回 "hello"
allOne.getMinKey(); // 返回 "leet"
```

# 题解

## 哈希表+双向链表

题目要求每次操作的时间复杂度为 `O(1)`，那么可以用哈希表+双向链表来解决。

使用双向链表，按照计数的升序排序来保存数据，`getMinKey` 和 `getMaxKey` 操作时，只需要返回链表头和链表尾即可。

使用哈希表来对每一个链表结点做映射，方便查找结点。

- `inc` 操作：
  - `key` 不在链表中：若链表为空或者头节点的计数大于 `1`，则新建一个结点，计数为 `1` ，插入到链表头部，并将 `key` 放入新节点的 `keys` 中
  - `key` 在链表中：假设 `key` 所在的结点为 `curr` 。若 `curr.next` 为空或者 `curr.next` 的计数大于 `curr.count + 1`，则新建一个结点，计数为 `curr.count + 1`，插入到 `curr` 之后，并将 `key` 放入新节点的 `keys` 中。同时，将 `key` 从 `curr` 中删除，如果移除后的 `curr.keys` 为空，则将 `curr` 结点从链表移除
  - 更新哈希表中 `key` 的映射
- `dec` 操作：
  - `key` 的计数为 `1`，将其从哈希表中删除
  - `key` 的计数不止 `1` 次：假设 `key` 所在的结点为 `curr` 。若 `curr.prev` 为空或者 `curr.prev` 的计数小于 `curr.count -1`，则新建一个结点，计数为 `curr.count - 1`，插入到 `curr` 之前，并将 `key` 放入新节点的 `keys` 中。更新哈希表映射。
  - 最后，将 `key` 从 `curr` 中删除，如果移除后的 `curr.keys` 为空，则将 `curr` 结点从链表移除

```ts
class LinkNode {
  keys: Set<string>;
  next: LinkNode | null;
  prev: LinkNode | null;
  constructor(public key?: string, public count?: number) {
    // 相同计数的结点可能不止一个，这里使用集合来保存
    this.keys = new Set<string>();
    if (key !== void 0) this.keys.add(key);
    if (count !== void 0) this.count = count;
  }

  insert(node: LinkNode) {
    node.next = this.next;
    node.prev = this;
    node.prev.next = node;
    node.next.prev = node;
  }

  remove() {
    this.prev.next = this.next;
    this.next.prev = this.prev;
  }
}

class AllOne {
  root: LinkNode;
  map: Map<string, LinkNode>;
  constructor() {
    // 使用双向链表存储数据,按照升序排列
    this.root = new LinkNode();
    this.root.next = this.root;
    this.root.prev = this.root;
    // 使用map存储结点
    this.map = new Map();
  }

  inc(key: string): void {
    if (this.map.has(key)) {
      const curr = this.map.get(key);
      const next = curr.next;
      if (curr.next === this.root || next.count > curr.count + 1) {
        // 下一个count比当前的大，或者只有一个结点。直接在当前结点后面插入
        const node = new LinkNode(key, curr.count + 1);
        curr.insert(node);
        this.map.set(key, node);
      } else {
        //  下一个结点的count刚好相等，直接更新下一个结点的值
        next.keys.add(key);
        this.map.set(key, next);
      }
      curr.keys.delete(key);
      if (curr.keys.size === 0) {
        curr.remove();
      }
    } else {
      if (this.root.next === this.root || this.root.next.count > 1) {
        const node = new LinkNode(key, 1);
        this.root.insert(node);
        this.map.set(key, node);
      } else {
        this.root.next.keys.add(key);
        this.map.set(key, this.root.next);
      }
    }
  }

  dec(key: string): void {
    const curr = this.map.get(key);
    if (curr.count === 1) {
      this.map.delete(key);
    } else {
      const prev = curr.prev;
      if (prev === this.root || prev.count < curr.count - 1) {
        const node = new LinkNode(key, curr.count - 1);
        prev.insert(node);
        this.map.set(key, node);
      } else {
        prev.keys.add(key);
        this.map.set(key, prev);
      }
    }
    curr.keys.delete(key);
    if (curr.keys.size === 0) {
      curr.remove();
    }
  }

  getMaxKey(): string {
    if (this.root.prev === this.root) return '';
    for (const maxKey of this.root.prev.keys) {
      return maxKey;
    }
  }

  getMinKey(): string {
    if (this.root.next === this.root) return '';
    for (const minKey of this.root.next.keys) {
      return minKey;
    }
  }
}
```
