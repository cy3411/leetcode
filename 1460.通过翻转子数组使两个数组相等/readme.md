# 题目

给你两个长度相同的整数数组 `target` 和 `arr` 。每一步中，你可以选择 `arr` 的任意 **非空子数组** 并将它翻转。你可以执行此过程任意次。

如果你能让 `arr` 变得与 `target` 相同，返回 True；否则，返回 False 。

提示：

- $target.length \equiv arr.length$
- $1 \leq target.length \leq 1000$
- $1 \leq target[i] \leq 1000$
- $1 \leq arr[i] \leq 1000$

# 示例

```
输入：target = [1,2,3,4], arr = [2,4,1,3]
输出：true
解释：你可以按照如下步骤使 arr 变成 target：
1- 翻转子数组 [2,4,1] ，arr 变成 [1,4,2,3]
2- 翻转子数组 [4,2] ，arr 变成 [1,2,4,3]
3- 翻转子数组 [4,3] ，arr 变成 [1,2,3,4]
上述方法并不是唯一的，还存在多种将 arr 变成 target 的方法。
```

```
输入：target = [7], arr = [7]
输出：true
解释：arr 不需要做任何翻转已经与 target 相等。
```

# 题解

## 哈希表

反转次数没有限制，所以只要 target 和 arr 中的元素相同，就一定可以将 arr 翻转到 target。

所以我们只需要判断 target 和 arr 中的元素是否相同即可。

```js
function canBeEqual(target: number[], arr: number[]): boolean {
  // target中元素数量
  const count1 = new Map<number, number>();
  for (const num of target) {
    count1.set(num, (count1.get(num) || 0) + 1);
  }
  // arr中的元素数量
  const count2 = new Map<number, number>();
  for (const num of arr) {
    count2.set(num, (count2.get(num) || 0) + 1);
  }
  // 元素数量不匹配
  if (count1.size !== count2.size) {
    return false;
  }

  // 元素是否相等
  for (const [key, value] of count1.entries()) {
    if (!count2.has(key) || count2.get(key) !== value) return false;
  }

  return true;
}
```

## 排序后比较

```cpp
class Solution {
public:
    bool canBeEqual(vector<int> &target, vector<int> &arr) {
        sort(target.begin(), target.end());
        sort(arr.begin(), arr.end());
        return target == arr;
    }
};
```

```py
class Solution:
    def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
        target.sort()
        arr.sort()
        return target == arr
```
