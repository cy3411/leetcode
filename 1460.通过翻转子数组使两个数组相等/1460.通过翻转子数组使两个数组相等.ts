/*
 * @lc app=leetcode.cn id=1460 lang=typescript
 *
 * [1460] 通过翻转子数组使两个数组相等
 */

// @lc code=start
function canBeEqual(target: number[], arr: number[]): boolean {
  // target中元素数量
  const count1 = new Map<number, number>();
  for (const num of target) {
    count1.set(num, (count1.get(num) || 0) + 1);
  }
  // arr中的元素数量
  const count2 = new Map<number, number>();
  for (const num of arr) {
    count2.set(num, (count2.get(num) || 0) + 1);
  }
  // 元素数量不匹配
  if (count1.size !== count2.size) {
    return false;
  }

  // 元素是否相等
  for (const [key, value] of count1.entries()) {
    if (!count2.has(key) || count2.get(key) !== value) return false;
  }

  return true;
}
// @lc code=end
