/*
 * @lc app=leetcode.cn id=766 lang=javascript
 *
 * [766] 托普利茨矩阵
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {boolean}
 */
var isToeplitzMatrix = function (matrix) {
  const M = matrix.length;
  const N = matrix[0].length;

  for (let i = 0; i < M - 1; i++) {
    let curr = matrix[i].slice(0, N - 1).toString();
    let next = matrix[i + 1].slice(1).toString();
    if (curr !== next) {
      return false;
    }
  }

  return true;
};
// @lc code=end
