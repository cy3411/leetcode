# 题目

给你二叉搜索树的根节点 `root` ，同时给定最小边界 `low` 和最大边界 `high`。通过修剪二叉搜索树，使得所有节点的值在`[low, high]`中。修剪树 不应该 改变保留在树中的元素的相对结构 (即，如果没有被移除，原有的父代子代关系都应当保留)。 可以证明，存在 **唯一的答案** 。

所以结果应当返回修剪好的二叉搜索树的新的根节点。注意，根节点可能会根据给定的边界发生改变。

提示：

- 树中节点数在范围 $[1, 10^4]$ 内
- $0 \leq Node.val \leq 10^4$
- 树中每个节点的值都是 **唯一** 的
- 题目数据保证输入是一棵有效的二叉搜索树
- $0 \leq low \leq high \leq 10^4$

# 示例

[![vOQDgK.png](https://s1.ax1x.com/2022/09/10/vOQDgK.png)](https://imgse.com/i/vOQDgK)

```
输入：root = [1,0,2], low = 1, high = 2
输出：[1,null,2]
```

[![vOQx2V.png](https://s1.ax1x.com/2022/09/10/vOQx2V.png)](https://imgse.com/i/vOQx2V)

```
输入：root = [3,0,4,null,2,null,null,1], low = 1, high = 3
输出：[3,2,null,1]
```

# 题解

## 深度优先搜索

对根结点进行深度优先搜索，如果节点为空，返回空。

如果结点值小于 low ， 表示该结点和结点左子树不符合题意，我们返回结点右子树的修剪结果。

如果结点值大于 hight ，表示该结点和结点右子树不符合题意，我们返回结点左子树的修剪结果。

如果结点值在 [low, high] 之间，我们将结点的左子树设为对它左子树的修剪结果，将结点的右子树设为对它右子树的修剪结果。

```js
function trimBST(root: TreeNode | null, low: number, high: number): TreeNode | null {
  if (root === null) return null;
  if (root.val < low) return trimBST(root.right, low, high);
  else if (root.val > high) return trimBST(root.left, low, high);
  else {
    root.left = trimBST(root.left, low, high);
    root.right = trimBST(root.right, low, high);
    return root;
  }
}
```

```cpp
class Solution {
public:
    TreeNode *trimBST(TreeNode *root, int low, int high) {
        if (root == nullptr) {
            return nullptr;
        }
        if (root->val < low) {
            return trimBST(root->right, low, high);
        } else if (root->val > high) {
            return trimBST(root->left, low, high);
        } else {
            root->left = trimBST(root->left, low, high);
            root->right = trimBST(root->right, low, high);
            return root;
        }
    }
};
```
