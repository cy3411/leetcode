# 题目
给你一个 `m x n` 的矩阵 `matrix` 和一个整数 `k` ，找出并返回矩阵内部矩形区域的不超过 `k` 的最大数值和。

题目数据保证总会存在一个数值和不超过 `k` 的矩形区域。

提示：  
+ `m == matrix.length`
+ `n == matrix[i].length`
+ `1 <= m, n <= 100`
+ `-100 <= matrix[i][j] <= 100`
+ `-105 <= k <= 105`

进阶：如果行数远大于列数，该如何设计解决方案？

# 示例
![cODXcj.png](https://z3.ax1x.com/2021/04/23/cODXcj.png)

```
输入：matrix = [[1,0,1],[0,-2,3]], k = 2
输出：2
解释：蓝色边框圈出来的矩形区域 [[0, 1], [-2, 3]] 的数值和是 2，且 2 是不超过 k 的最大数字（k = 2）。
```

# 题解
## 前缀和
遍历矩形的左右边界，计算该边界内的每列的元素和，当前问题就转化成一维数组问题：  

给定一个整数数组和一个 `k` 值，计算该数组最大子集和，大小不能超过 `k` 。

![cOylXd.png](https://z3.ax1x.com/2021/04/23/cOylXd.png)

如上图，前缀和黄色部分的子集和，就是matrix绿色部分的矩形区域和。    

```js
function maxSumSubmatrix(matrix: number[][], k: number): number {
  const rows = matrix.length;
  if (rows === 0) return 0;
  const cols = matrix[0].length;
  let result = Number.NEGATIVE_INFINITY;

  /**
   * @description 计算最大不超过k的子集和
   *
   * 这里用的暴力解法，可以使用其他的有序集合的数据结构，可以降低时间复杂度
   */
  const maxSum = (prefixSum: number[], k: number): number => {
    let max = Number.NEGATIVE_INFINITY;
    const size = prefixSum.length;
    for (let i = 0; i < size; i++) {
      let sum = 0;
      for (let j = i; j < size; j++) {
        sum += prefixSum[j];
        if (sum <= k) {
          max = Math.max(max, sum);
        }
      }
    }
    return max;
  };

  // 遍历左边界
  for (let left = 0; left < cols; left++) {
    // 计算从left开始每一行的前缀和
    const prefixSum = new Array(rows).fill(0);
    // 遍历右边界
    for (let right = left; right < cols; right++) {
      // 按行计算前缀和
      for (let i = 0; i < rows; i++) {
        prefixSum[i] += matrix[i][right];
      }
      //更新结果
      result = Math.max(result, maxSum(prefixSum, k));
    }
  }

  return result;
}
```