# 题目
给定一个二叉树，我们在树的节点上安装摄像头。

节点上的每个摄影头都可以监视其父对象、自身及其直接子对象。

计算监控树的所有节点所需的最小摄像头数量。

# 示例
![cKXJl4.png](https://z3.ax1x.com/2021/04/04/cKXJl4.png)
```
输入：[0,0,null,0,0]
输出：1
解释：如图所示，一台摄像头足以监控所有节点。
```
![cKXBtK.png](https://z3.ax1x.com/2021/04/04/cKXBtK.png)
```
输入：[0,0,null,0,null,0,null,null,0]
输出：2
解释：需要至少两个摄像头来监视树的所有节点。 上图显示了摄像头放置的有效位置之一。
```

# 题解
自底向上推导，也就是走后续遍历的顺序，确定左右子节点的状态向上推导。

## 贪心
尽量让叶子节点的父节点安装摄像头，这样摄像头的数量才能最少。

定义状态：
+ 0：当前节点无覆盖
+ 1：当前节点安装了摄像头
+ 2：当前节点有覆盖

未安装摄像头的状态被0和1包括了，所以这里只有3种状态。

Base Case:

当遇到空节点的时候，因为我们尽量要让尽叶子节点的父节点安装摄像头，所以这里需要返回覆盖状态2.

状态转移：

+ 左右节点都被覆盖了，当前节点是无覆盖状态
+ 左右节点至少有一个无覆盖状态，当前节点必须要安装摄像头才可以
+ 左右节点至少有一个安装摄像头，当前节点就是覆盖状态

```js
/**
 * @param {TreeNode} root
 * @return {number}
 */
var minCameraCover = function (root) {
  // 0：当前节点未覆盖
  // 1：当前节点已安装摄像头
  // 2：当前节点已覆盖
  const helper = (root) => {
    // 空节点表示已覆盖，这样才能让叶子节点的父节点安装摄像头，数量最少
    if (root === null) return 2;

    let left = helper(root.left);
    let right = helper(root.right);

    // 左右节点都已经被覆盖了，当前节点就是无覆盖状态，需要去当前节点的父节点去覆盖它才能最小
    if (left === 2 && right === 2) return 0;
    // 左右节点有一个没被覆盖，需要当前节点安装摄像头去覆盖
    if (left === 0 || right === 0) {
      result++;
      return 1;
    }
    // 左右节点中有安装摄像头的，当前节点就是被覆盖到的。
    if (left === 1 || right === 1) return 2;
  };

  let result = 0;
  // 当前根无覆盖，根需要安装一个摄像头
  if (helper(root) === 0) {
    result++;
  }

  return result;
};
```


## 树形DP
定义状态：
+ withCamer：当前节点安装摄像头情况下的最小摄像头数量
+ noCamerWatchByParent：当前节点被父节点覆盖情况下的最小摄像头数量
+ noCarmerWachByChildren：当前节点被子节点覆盖情况下的最小摄像头数量

Base case:

空节点无法安装摄像头，也不需要被覆盖。

+ withCamer=极大值，方便向上推到去更新最小摄像头数量
+ noCamerWatchByParent = 0
+ noCarmerWachByChildren = 0

状态转移：
+ 当前节点安装了摄像头，这里结果需要加1，因为安装了摄像头
  
  + 左右节点都被父节点覆盖了，`left.noCamerWatchByParent + right.noCamerWatchByParent`
  + 左节点有摄像头，右节点被父节点覆盖了，`left.withCamer + right.noCamerWatchByParent`
  + 左节点被父节点覆盖了，右节点有摄像头`left.noCamerWatchByParent + right.withCamer`

+ 当前节点没有摄像头但是被父节点覆盖

  + 左右节点都被孩子节点覆盖  `left.noCarmerWachByChildren + right.noCarmerWachByChildren`  
  + 左节点有摄像头，右节点被孩子覆盖 `left.withCamer + right.noCarmerWachByChildren`
  + 左节点被孩子覆盖，右节点有摄像头 `left.noCarmerWachByChildren + right.withCamer`
  + 左右都有摄像头 `left.withCamer + right.withCamer`

+ 当前节点没有摄像头但是被子节点覆盖
  
  + 左右都有摄像头 `left.withCamer + right.withCamer`
  + 左节点有摄像头，右节点被孩子覆盖 `left.withCamer + right.noCarmerWachByChildren`
  + 左节点被孩子覆盖，右节点有摄像头 `left.noCarmerWachByChildren + right.withCamer`


```js
/**
 * @param {TreeNode} root
 * @return {number}
 */
var minCameraCover = function (root) {
  const helper = (root) => {
    if (root === null) {
      return {
        // 当前节点安装摄像头情况下的最小摄像头数量
        withCamer: Number.MAX_VALUE,
        // 当前节点被父节点覆盖情况下的最小摄像头数量
        noCamerWatchByParent: 0,
        // 当前节点被子节点覆盖情况下的最小摄像头数量
        noCarmerWachByChildren: 0,
      };
    }

    const left = helper(root.left);
    const right = helper(root.right);

    const withCamer =
      1 +
      Math.min(
        left.noCamerWatchByParent + right.noCamerWatchByParent,
        left.withCamer + right.noCamerWatchByParent,
        left.noCamerWatchByParent + right.withCamer
      );

    const noCamerWatchByParent = Math.min(
      left.noCarmerWachByChildren + right.noCarmerWachByChildren,
      left.withCamer + right.withCamer,
      left.withCamer + right.noCarmerWachByChildren,
      left.noCarmerWachByChildren + right.withCamer
    );

    const noCarmerWachByChildren = Math.min(
      left.withCamer + right.withCamer,
      left.withCamer + right.noCarmerWachByChildren,
      left.noCarmerWachByChildren + right.withCamer
    );

    return { withCamer, noCamerWatchByParent, noCarmerWachByChildren };
  };

  const result = helper(root);
  // (根节点有摄像头或根节点无摄像头但是被孩子覆盖)两种状态的最小值
  return Math.min(result.withCamer, result.noCarmerWachByChildren);
};
```