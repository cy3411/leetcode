/*
 * @lc app=leetcode.cn id=67 lang=javascript
 *
 * [67] 二进制求和
 */

// @lc code=start
/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
var addBinary = function (a, b) {
  let carry = 0
  let ret = ''
  const size = Math.max(a.length, b.length)
  for (let i = 0; i < size; i++) {
    carry += i < a.length ? parseInt(a[a.length - 1 - i]) : 0
    carry += i < b.length ? parseInt(b[b.length - 1 - i]) : 0
    ret = carry % 2 + ret
    carry = Math.floor(carry / 2)
  }
  if (carry > 0) {
    ret = carry + ret
  }
  return ret
};

// addBinary('11', '1')
// @lc code=end

