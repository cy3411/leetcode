/*
 * @lc app=leetcode.cn id=2079 lang=cpp
 *
 * [2079] 给植物浇水
 */

// @lc code=start
class Solution {
public:
    int wateringPlants(vector<int> &plants, int capacity) {
        int n = plants.size();
        int step = 0;
        int cap = capacity;
        int i = 0;

        while (i < n) {
            if (plants[i] <= cap) {
                cap -= plants[i];
                i++;
                step++;
            } else {
                cap = capacity;
                step += 2 * i;
            }
        }
        return step;
    }
};
// @lc code=end
