/*
 * @lc app=leetcode.cn id=2079 lang=typescript
 *
 * [2079] 给植物浇水
 */

// @lc code=start
function wateringPlants(plants: number[], capacity: number): number {
  const n = plants.length;
  let i = 0;
  let step = 0;
  let cap = capacity;
  while (i < n) {
    if (plants[i] <= cap) {
      // 可以继续浇水
      cap -= plants[i]; // 减去浇水的水量
      i++; // 继续浇水
      step++; // 步数+1
    } else {
      // 不能继续浇水
      step += 2 * i; // 回去灌水然后回来消耗的步数
      cap = capacity; // 将水灌满，重新开始
    }
  }
  return step;
}
// @lc code=end
