/*
 * @lc app=leetcode.cn id=1894 lang=typescript
 *
 * [1894] 找到需要补充粉笔的学生编号
 */

// @lc code=start
function chalkReplacer(chalk: number[], k: number): number {
  const m = chalk.length;

  // 记录每轮消耗粉笔数
  let total = 0;
  for (let n of chalk) {
    total += n;
  }

  // 由于每轮消耗都是固定的，所以我们只要取得最后一轮的剩余数量去循环一次就可以了。
  k %= total;

  for (let i = 0; i < m; i++) {
    if (chalk[i] > k) return i;
    k -= chalk[i];
  }
}
// @lc code=end
