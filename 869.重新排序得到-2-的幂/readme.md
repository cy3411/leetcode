# 题目

给定正整数 `N` ，我们按任何顺序（包括原始顺序）将数字重新排序，注意其前导数字不能为零。

如果我们可以通过上述方式得到 `2` 的幂，返回 `true`；否则，返回 `false`。

提示：

- $1 \leq N \leq 10^9$

# 示例

```
输入：1
输出：true
```

```
输入：10
输出：false
```

# 题解

## 回溯

题意要求对 `n` 的位数重新排序后，是否可以得到 `2` 的幂。

我们可以采用全排列的方式，枚举出每一个合法的排列，然后对其检测是否为 2 的幂。

```ts
function reorderedPowerOf2(n: number): boolean {
  const isValid = (n: number): boolean => {
    return (n & (n - 1)) === 0;
  };

  const backtrack = (nums: string[], idx: number, num: number): boolean => {
    // 得到排列后判断是否满足为2的幂次方
    if (idx === nums.length) {
      return isValid(num);
    }

    // 全排列
    for (let i = 0; i < nums.length; i++) {
      // 不能有前导零
      // 当前位访问过
      if ((num === 0 && nums[i] === '0') || visited[i]) {
        continue;
      }
      visited[i] = 1;
      if (backtrack(nums, idx + 1, num * 10 + Number(nums[i]))) {
        return true;
      }
      visited[i] = 0;
    }

    return false;
  };

  const nums = Array.from(String(n));
  const visited = new Array(nums.length).fill(0);

  return backtrack(nums, 0, 0);
}
```
