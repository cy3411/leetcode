/*
 * @lc app=leetcode.cn id=824 lang=typescript
 *
 * [824] 山羊拉丁文
 */

// @lc code=start
function toGoatLatin(sentence: string): string {
  const vowels = new Set(['a', 'e', 'i', 'o', 'u']);

  let ans = '';
  const words = sentence.split(' ');
  for (let i = 0; i < words.length; i++) {
    const word = words[i];
    if (vowels.has(word[0].toLocaleLowerCase())) {
      ans += word + 'ma';
    } else {
      ans += word.slice(1) + word[0] + 'ma';
    }
    ans += 'a'.repeat(i + 1) + (i === words.length - 1 ? '' : ' ');
  }

  return ans;
}
// @lc code=end
