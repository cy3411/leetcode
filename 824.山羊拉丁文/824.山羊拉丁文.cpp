/*
 * @lc app=leetcode.cn id=824 lang=cpp
 *
 * [824] 山羊拉丁文
 */

// @lc code=start
class Solution
{
public:
    string toGoatLatin(string sentence)
    {
        unordered_set<char> vowels = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
        string ans;

        stringstream ss(sentence);
        string word;
        int i = 1;
        while (ss >> word)
        {
            if (vowels.count(word[0]))
            {
                ans += word + "ma";
            }
            else
            {
                ans += word.substr(1) + word[0] + "ma";
            }

            ans += string(i++, 'a') + " ";
        }
        return ans.substr(0, ans.size() - 1);
    }
};
// @lc code=end
