/*
 * @lc app=leetcode.cn id=342 lang=typescript
 *
 * [342] 4的幂
 */

// @lc code=start
function isPowerOfFour(n: number): boolean {
  // 10101010101010101010101010101010
  let mask = 0xaaaaaaaa 
  return n > 0 && (n & (n - 1)) === 0 && (n & mask) === 0;
}
// @lc code=end
