# 题目

给定一个整数，写一个函数来判断它是否是 4 的幂次方。如果是，返回 `true` ；否则，返回 `false` 。

整数 `n` 是 4 的幂次方需满足：存在整数 `x` 使得 `n == 4x`

提示：

- -231 <= n <= 231 - 1

进阶：

- 你能不使用循环或者递归来完成本题吗？

# 示例

```
输入：n = 16
输出：true
```

# 题解

先判断是否是 2 的冥。具体可以参考[231.2-的幂](https://gitee.com/cy3411/leetcode/tree/master/231.2-%E7%9A%84%E5%B9%82)

## 求模

4 的冥取模 3，结果必然是 1。

```ts
function isPowerOfFour(n: number): boolean {
  return n > 0 && (n & (n - 1)) === 0 && n % 3 === 1;
}
```

## 位于运算

4 的冥二进制中的 1，肯定在偶数位的位置上。(低位从 0 开始)

我们定义`mask`，将它的二进制位的偶数维位设为 0。

将 mask 与 4 的冥做位与运算，结果必然为`0`。

```ts
function isPowerOfFour(n: number): boolean {
  // 10101010101010101010101010101010
  let mask = 0xaaaaaaaa;
  return n > 0 && (n & (n - 1)) === 0 && (n & mask) === 0;
}
```
