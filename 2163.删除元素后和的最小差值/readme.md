# 题目

给你一个下标从 `0` 开始的整数数组 `nums` ，它包含 `3 * n` 个元素。

你可以从 `nums` 中删除 **恰好** `n` 个元素，剩下的 `2 * n` 个元素将会被分成两个 **相同大小** 的部分。

- 前面 `n` 个元素属于第一部分，它们的和记为 $sum_{first}$ 。
- 后面 `n` 个元素属于第二部分，它们的和记为 $sum_{second}$ 。

两部分和的 **差值** 记为 $sum_{first} - sum_{second}$ 。

- 比方说，$sum_{first} = 3$ 且 $sum_{second} = 2$ ，它们的差值为 `1` 。
- 再比方，$sum_{first} = 2$ 且 $sum_{second} = 3$ ，它们的差值为 `-1` 。

请你返回删除 `n` 个元素之后，剩下两部分和的 **差值的最小值** 是多少。

提示：

- $nums.length \leq 3 * n$
- $1 \leq n \leq 10^5$
- $1 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [3,1,2]
输出：-1
解释：nums 有 3 个元素，所以 n = 1 。
所以我们需要从 nums 中删除 1 个元素，并将剩下的元素分成两部分。
- 如果我们删除 nums[0] = 3 ，数组变为 [1,2] 。两部分和的差值为 1 - 2 = -1 。
- 如果我们删除 nums[1] = 1 ，数组变为 [3,2] 。两部分和的差值为 3 - 2 = 1 。
- 如果我们删除 nums[2] = 2 ，数组变为 [3,1] 。两部分和的差值为 3 - 1 = 2 。
两部分和的最小差值为 min(-1,1,2) = -1 。
```

```
输入：nums = [7,9,5,8,1,3]
输出：1
解释：n = 2 。所以我们需要删除 2 个元素，并将剩下元素分为 2 部分。
如果我们删除元素 nums[2] = 5 和 nums[3] = 8 ，剩下元素为 [7,9,1,3] 。和的差值为 (7+9) - (1+3) = 12 。
为了得到最小差值，我们应该删除 nums[1] = 9 和 nums[4] = 1 ，剩下的元素为 [7,5,8,3] 。和的差值为 (7+5) - (8+3) = 1 。
观察可知，最优答案为 1 。
```

# 题解

## 优先队列

先要差值最小，那么左边的和要尽量小，右边的和要尽量大。

使用优先队列，先顺序遍历，记录左边在满足区间大小的情况下，可以删除的最大和。

然后逆序遍历，记录右边在满足区间大小的情况下，可以删除的最小和。同时记录左右两边的和，计算差值。

```ts
class Heap {
  n: number;
  data: number[];
  constructor() {
    this.n = 0;
    this.data = [];
  }

  private swap(i: number, j: number): void {
    [this.data[i], this.data[j]] = [this.data[j], this.data[i]];
  }

  private less(i: number, j: number): boolean {
    return this.data[i] < this.data[j];
  }

  private swin(i: number): void {
    while (i > 0) {
      const p = (i - 1) >> 1;
      if (this.less(i, p)) {
        this.swap(i, p);
      } else {
        break;
      }
      i = p;
    }
  }

  private sink(i: number): void {
    while (i < this.n) {
      let j = (i << 1) + 1;
      if (j + 1 < this.n && this.less(j + 1, j)) {
        j++;
      }
      if (j >= this.n || this.less(i, j)) {
        break;
      }
      this.swap(i, j);
      i = j;
    }
  }

  insert(x: number): void {
    this.data.push(x);
    this.n++;
    this.swin(this.n - 1);
  }

  pop(): number {
    const x = this.data[0];
    this.swap(0, this.n - 1);
    this.data.pop();
    this.n--;
    this.sink(0);
    return x;
  }

  clear(): void {
    this.n = 0;
    this.data = [];
  }
}

function minimumDifference(nums: number[]): number {
  let m = nums.length;
  let n = m / 3;
  // 默认小顶堆
  let heap = new Heap();
  // 左边区间内可以减少的最大值
  const lsum = new Array(m).fill(0);
  let ltotal = 0;
  for (let i = 0, sum = 0; i < m; i++) {
    ltotal += nums[i];
    // 需要取当前区间的最大值，所以用的负数
    heap.insert(-nums[i]);
    if (heap.n < n) continue;
    if (heap.n > n) {
      sum -= heap.pop();
    }
    lsum[i] = sum;
  }

  heap.clear();
  let rtotal = 0;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let i = m - 1, sum = 0; i >= n; i--) {
    ltotal -= nums[i];
    rtotal += nums[i];
    // 需要取当前区间的最小值
    heap.insert(nums[i]);

    if (heap.n < n) continue;
    if (heap.n > n) {
      sum += heap.pop();
    }
    // 左边减去可以删除的最大值，右边减去可以删除的最小值
    // 这样两者之间的差值就是最小的
    ans = Math.min(ans, ltotal - lsum[i - 1] - (rtotal - sum));
  }

  return ans;
}
```
