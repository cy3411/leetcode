#
# @lc app=leetcode.cn id=915 lang=python3
#
# [915] 分割数组
#

# @lc code=start
class Solution:
    def partitionDisjoint(self, nums: List[int]) -> int:
        n = len(nums)
        leftMax = currMax = nums[0]
        pos = 0
        for i in range(1, n - 1):
            currMax = max(currMax, nums[i])
            if nums[i] < leftMax:
                leftMax = currMax
                pos = i
        return pos + 1
# @lc code=end
