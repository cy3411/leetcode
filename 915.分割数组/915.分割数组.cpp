/*
 * @lc app=leetcode.cn id=915 lang=cpp
 *
 * [915] 分割数组
 */

// @lc code=start
class Solution {
public:
    int partitionDisjoint(vector<int> &nums) {
        int n = nums.size();
        int leftMax = nums[0], currMax = nums[0], pos = 0;
        for (int i = 1; i < n; i++) {
            currMax = max(currMax, nums[i]);
            if (nums[i] < leftMax) {
                leftMax = currMax;
                pos = i;
            }
        }
        return pos + 1;
    }
};
// @lc code=end
