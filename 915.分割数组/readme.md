# 题目

给定一个数组 nums ，将其划分为两个连续子数组 left 和 right， 使得：

- left 中的每个元素都小于或等于 right 中的每个元素。
- left 和 right 都是非空的。
- left 的长度要尽可能小。

在完成这样的分组后返回 left 的 长度 。

用例可以保证存在这样的划分方法。

提示：

- $2 \leq nums.length \leq 10^5$
- $0 \leq nums[i] \leq 10^6$
- 可以保证至少有一种方法能够按题目所描述的那样对 `nums` 进行划分。

# 示例

```
输入：nums = [5,0,3,8,6]
输出：3
解释：left = [5,0,3]，right = [8,6]
```

```
输入：nums = [1,1,1,0,6,12]
输出：4
解释：left = [1,1,1,0]，right = [6,12]
```

# 题解

## 遍历

题意要求 left 中每个元素都要小于或等于 right 中的每个元素，等价于 left 中的最大值小于等于 right 中的最小值。

维护一个 minRights 数组，保存到访问到当前位置，出现的最小的数。

然后遍历数组，找到第一个最大值小于等于 minRights[i] 的位置就可以了。

```js
function partitionDisjoint(nums: number[]): number {
  const n = nums.length;
  // 右边每个位置的最小元素
  const minRights = new Array(n).fill(0);
  minRights[n - 1] = nums[n - 1];
  for (let i = n - 2; i >= 0; i--) {
    minRights[i] = Math.min(minRights[i + 1], nums[i]);
  }
  // 左边最大元素
  let maxLeft = 0;
  for (let i = 0; i < n - 1; i++) {
    maxLeft = Math.max(maxLeft, nums[i]);
    if (maxLeft <= minRights[i + 1]) {
      return i + 1;
    }
  }
  return n - 1;
}
```

```cpp
class Solution {
public:
    int partitionDisjoint(vector<int> &nums) {
        int n = nums.size();
        int leftMax = nums[0], currMax = nums[0], pos = 0;
        for (int i = 1; i < n; i++) {
            currMax = max(currMax, nums[i]);
            if (nums[i] < leftMax) {
                leftMax = currMax;
                pos = i;
            }
        }
        return pos + 1;
    }
};
```

```py
class Solution:
    def partitionDisjoint(self, nums: List[int]) -> int:
        n = len(nums)
        leftMax = currMax = nums[0]
        pos = 0
        for i in range(1, n - 1):
            currMax = max(currMax, nums[i])
            if nums[i] < leftMax:
                leftMax = currMax
                pos = i
        return pos + 1
```
