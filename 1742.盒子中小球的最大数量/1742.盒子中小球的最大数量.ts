/*
 * @lc app=leetcode.cn id=1742 lang=typescript
 *
 * [1742] 盒子中小球的最大数量
 */

// @lc code=start
function countBalls(lowLimit: number, highLimit: number): number {
  const map = new Map<number, number>();
  let ans = 0;
  for (let i = lowLimit; i <= highLimit; i++) {
    let x = 0;
    let y = i;
    while (y !== 0) {
      x += y % 10;
      y = (y / 10) >> 0;
    }
    map.set(x, (map.get(x) ?? 0) + 1);
    ans = Math.max(ans, map.get(x)!);
  }
  return ans;
}
// @lc code=end
