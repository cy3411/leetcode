# 题目

你在一家生产小球的玩具厂工作，有 n 个小球，编号从 lowLimit 开始，到 highLimit 结束（包括 lowLimit 和 highLimit ，即 $n == highLimit - lowLimit + 1$）。另有无限数量的盒子，编号从 1 到 infinity 。

你的工作是将每个小球放入盒子中，其中盒子的编号应当等于小球编号上每位数字的和。例如，编号 321 的小球应当放入编号 $3 + 2 + 1 = 6$ 的盒子，而编号 10 的小球应当放入编号 $1 + 0 = 1$ 的盒子。

给你两个整数 lowLimit 和 highLimit ，返回放有最多小球的盒子中的小球数量。如果有多个盒子都满足放有最多小球，只需返回其中任一盒子的小球数量。

提示：

- $1 \leq lowLimit \leq highLimit \leq 10^5$

# 示例

```
输入：lowLimit = 1, highLimit = 10
输出：2
解释：
盒子编号：1 2 3 4 5 6 7 8 9 10 11 ...
小球数量：2 1 1 1 1 1 1 1 1 0  0  ...
编号 1 的盒子放有最多小球，小球数量为 2 。
```

```
输入：lowLimit = 5, highLimit = 15
输出：2
解释：
盒子编号：1 2 3 4 5 6 7 8 9 10 11 ...
小球数量：1 1 1 1 2 2 1 1 1 0  0  ...
编号 5 和 6 的盒子放有最多小球，每个盒子中的小球数量都是 2 。
```

# 题解

## 哈希表

使用哈希表记录盒子和小球数量，最后返回哈希表中小球数最多的数量即可。

```ts
function countBalls(lowLimit: number, highLimit: number): number {
  const map = new Map<number, number>();
  let ans = 0;
  for (let i = lowLimit; i <= highLimit; i++) {
    let x = 0;
    let y = i;
    while (y !== 0) {
      x += y % 10;
      y = (y / 10) >> 0;
    }
    map.set(x, (map.get(x) ?? 0) + 1);
    ans = Math.max(ans, map.get(x)!);
  }
  return ans;
}
```

```py
from collections import Counter


class Solution:
    def countBalls(self, lowLimit: int, highLimit: int) -> int:
        counter = Counter(sum(map(int, str(i)))
                          for i in range(lowLimit, highLimit + 1))
        return max(counter.values())
```

```cpp
class Solution {
public:
    int countBalls(int lowLimit, int highLimit) {
        unordered_map<int, int> count;
        int ans = 0;
        for (int i = lowLimit; i <= highLimit; i++) {
            int x = 0, y = i;
            while (y != 0) {
                x += y % 10;
                y = y / 10;
            }
            count[x]++;
            ans = max(ans, count[x]);
        }
        return ans;
    }
};
```
