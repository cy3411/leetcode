/*
 * @lc app=leetcode.cn id=1742 lang=cpp
 *
 * [1742] 盒子中小球的最大数量
 */

// @lc code=start
class Solution {
public:
    int countBalls(int lowLimit, int highLimit) {
        unordered_map<int, int> count;
        int ans = 0;
        for (int i = lowLimit; i <= highLimit; i++) {
            int x = 0, y = i;
            while (y != 0) {
                x += y % 10;
                y = y / 10;
            }
            count[x]++;
            ans = max(ans, count[x]);
        }
        return ans;
    }
};
// @lc code=end
