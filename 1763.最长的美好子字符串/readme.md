# 题目

当一个字符串 `s` 包含的每一种字母的大写和小写形式 **同时** 出现在 `s` 中，就称这个字符串 `s` 是 **美好** 字符串。比方说，`"abABB"` 是美好字符串，因为 `'A'` 和 `'a'` 同时出现了，且 `'B'` 和 `'b'` 也同时出现了。然而，`"abA"` 不是美好字符串因为 `'b'` 出现了，而 `'B'` 没有出现。

给你一个字符串 `s` ，请你返回 `s` **最长的** 美好子字符串 。如果有多个答案，请你返回 **最早** 出现的一个。如果不存在美好子字符串，请你返回一个空字符串。

提示：

- $\color{burlywood}1 \leq s.length \leq 100$
- `s` 只包含大写和小写英文字母。

# 示例

```
输入：s = "YazaAay"
输出："aAa"
解释："aAa" 是一个美好字符串，因为这个子串中仅含一种字母，其小写形式 'a' 和大写形式 'A' 也同时出现了。
"aAa" 是最长的美好子字符串。
```

```
输入：s = "dDzeE"
输出："dD"
解释："dD" 和 "eE" 都是最长美好子字符串。
由于有多个美好子字符串，返回 "dD" ，因为它出现得最早。
```

# 题解

## 暴力

题目给出字符串长度最多为 100。可以暴力遍历每个子字符串，检查是否是美好字符串。同时更新最长的答案。

```ts
function longestNiceSubstring(s: string): string {
  const n = s.length;
  let maxPos = 0;
  let maxLen = 0;

  const lowerBase = 'a'.charCodeAt(0);
  const upperBase = 'A'.charCodeAt(0);

  for (let i = 0; i < n; i++) {
    // 位标记
    let lower_bit = 0;
    let upper_bit = 0;

    for (let j = i; j < n; j++) {
      // 出现过的字符，将对应位置标记为1
      if ('a' <= s[j] && s[j] <= 'z') {
        lower_bit |= 1 << (s[j].charCodeAt(0) - lowerBase);
      } else if ('A' <= s[j] && s[j] <= 'Z') {
        upper_bit |= 1 << (s[j].charCodeAt(0) - upperBase);
      }
      // 对比位标记
      // 对比位置长度是否大于当前最大长度
      if (lower_bit === upper_bit && j - i + 1 > maxLen) {
        maxPos = i;
        maxLen = j - i + 1;
      }
    }
  }

  return s.substring(maxPos, maxPos + maxLen);
}
```

```cpp
class Solution
{
public:
    string longestNiceSubstring(string s)
    {
        int n = s.size();
        int max_pos = 0, max_len = 0;

        for (int i = 0; i < n; i++)
        {
            int lower_bit = 0;
            int upper_bit = 0;
            for (int j = i; j < n; j++)
            {
                if (s[j] >= 'a' && s[j] <= 'z')
                {
                    lower_bit |= 1 << (s[j] - 'a');
                }
                else
                {
                    upper_bit |= 1 << (s[j] - 'A');
                }
                if (lower_bit == upper_bit && max_len < (j - i + 1))
                {
                    max_pos = i;
                    max_len = j - i + 1;
                }
            }
        }

        return s.substr(max_pos, max_len);
    }
};
```

## 分治

题意可知，美好子字符串的定义是，它的小写形式和大写形式都出现了。因此，如果有的字符不符合上面条件，那么它就属于不合法字符。

每次求解的时候，利用不合法字符，将字符串分割成更小的部分，然后递归求解。求解的过程中，持续维护最长的美好子字符串。

```cpp
class Solution
{
public:
    void dfs(string s, int start, int end, int &maxPos, int &maxLen)
    {
        if (start >= end)
        {
            return;
        }
        int lower_bit = 0, upper_bit = 0;
        for (int i = start; i <= end; i++)
        {
            if (s[i] >= 'a' && s[i] <= 'z')
            {
                lower_bit |= 1 << (s[i] - 'a');
            }
            else
            {
                upper_bit |= 1 << (s[i] - 'A');
            }
        }
        // 找到完美子串，更新起始点和长度
        if (lower_bit == upper_bit)
        {
            if (end - start + 1 > maxLen)
            {
                maxLen = end - start + 1;
                maxPos = start;
            }
            return;
        }
        // 上面未找到，以非法字符为切割点，递归查找其他区间
        // 所有合法的字符
        int valid = lower_bit & upper_bit;
        int pos = start;
        while (pos <= end)
        {
            start = pos;
            while (pos <= end && (valid & (1 << (tolower(s[pos]) - 'a'))))
            {
                pos++;
            }
            dfs(s, start, pos - 1, maxPos, maxLen);
            pos++;
        }
    }

    string longestNiceSubstring(string s)
    {
        int n = s.size();
        int max_pos = 0, max_len = 0;
        dfs(s, 0, n - 1, max_pos, max_len);
        return s.substr(max_pos, max_len);
    }
};
```
