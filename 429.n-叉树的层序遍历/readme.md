# 题目

给定一个 `N` 叉树，返回其节点值的*层序遍历*。（即从左到右，逐层遍历）。

树的序列化输入是用层序遍历，每组子节点都由 `null` 值分隔（参见示例）。

提示：

- 树的高度不会超过 `1000`
- 树的节点总数在 $[0, 10^4]$ 之间

# 示例

[![LpdCJf.png](https://s1.ax1x.com/2022/04/08/LpdCJf.png)](https://imgtu.com/i/LpdCJf)

```
输入：root = [1,null,3,2,4,null,5,6]
输出：[[1],[3,2,4],[5,6]]
```

[![LpdELj.png](https://s1.ax1x.com/2022/04/08/LpdELj.png)](https://imgtu.com/i/LpdELj)

```
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：[[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
```

# 题解

## 广度优先搜索

广度优先搜索，遍历树的每一层，将每一层的结点放入对应的数组中。

```ts
interface Cell {
  node: Node;
  deep: number;
}

function levelOrder(root: Node | null): number[][] {
  const ans: number[][] = [];
  if (root === null) return ans;
  // 初始化
  const queue: Cell[] = [{ node: root, deep: 0 }];
  // BFS
  while (queue.length) {
    const curr = queue.shift()!;
    // 如果当前层深度大于结果数组的长度，则补充新的数组
    if (curr.deep === ans.length) {
      ans.push([]);
    }
    // 将当前节点添加到结果数组
    ans[curr.deep].push(curr.node.val);
    for (const child of curr.node.children) {
      queue.push({ node: child, deep: curr.deep + 1 });
    }
  }
  return ans;
}
```

```cpp
class Solution
{

    typedef struct
    {
        Node *node;
        int deep;
    } Cell;

public:
    vector<vector<int>> levelOrder(Node *root)
    {
        vector<vector<int>> ans;
        if (root == nullptr)
        {
            return ans;
        }
        queue<Cell> que;
        que.push(Cell{root, 0});
        while (!que.empty())
        {
            Cell curr = que.front();
            que.pop();
            if (ans.size() == curr.deep)
            {
                ans.push_back(vector<int>());
            }
            ans[curr.deep].push_back(curr.node->val);
            for (auto child : curr.node->children)
            {
                que.push(Cell{child, curr.deep + 1});
            }
        }
        return ans;
    }
};
```
