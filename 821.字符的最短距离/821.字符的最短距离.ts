/*
 * @lc app=leetcode.cn id=821 lang=typescript
 *
 * [821] 字符的最短距离
 */

// @lc code=start
function shortestToChar(s: string, c: string): number[] {
  const n = s.length;
  const ans: number[] = new Array(n).fill(n);

  // 向后遍历，
  for (let i = 0, idx = -1; i < n; i++) {
    if (s[i] === c) idx = i;
    if (idx !== -1) ans[i] = i - idx;
  }

  for (let i = n - 1, idx = -1; i >= 0; i--) {
    if (s[i] === c) idx = i;
    if (idx !== -1) ans[i] = Math.min(ans[i], idx - i);
  }

  return ans;
}
// @lc code=end
