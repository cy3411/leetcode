# 题目

给你一个字符串 `s` 和一个字符 `c` ，且 `c` 是 `s` 中出现过的字符。

返回一个整数数组 `answer` ，其中 `answer.length == s.length` 且 `answer[i]` 是 `s` 中从下标 `i` 到离它 **最近** 的字符 `c` 的 **距离** 。

两个下标 i 和 j 之间的 距离 为 abs(i - j) ，其中 abs 是绝对值函数。

提示：

- $1 \leq s.length \leq 10^4$
- `s[i]` 和 `c` 均为小写英文字母
- 题目数据保证 `c` 在 `s` 中至少出现一次

# 示例

```
输入：s = "loveleetcode", c = "e"
输出：[3,2,1,0,1,0,0,1,2,2,1,0]
解释：字符 'e' 出现在下标 3、5、6 和 11 处（下标从 0 开始计数）。
距下标 0 最近的 'e' 出现在下标 3 ，所以距离为 abs(0 - 3) = 3 。
距下标 1 最近的 'e' 出现在下标 3 ，所以距离为 abs(1 - 3) = 2 。
对于下标 4 ，出现在下标 3 和下标 5 处的 'e' 都离它最近，但距离是一样的 abs(4 - 3) == abs(4 - 5) = 1 。
距下标 8 最近的 'e' 出现在下标 6 ，所以距离为 abs(8 - 6) = 2 。
```

```
输入：s = "aaab", c = "b"
输出：[3,2,1,0]
```

# 题解

## 遍历

第一次遍历找左边离 i 元素最近的 c，第二次遍历找右边离 i 元素最近的 c，取两次结果的最小距离。

初始化默认的结果给一个极大值，这里的给的是 s 的长度 n

```ts
function shortestToChar(s: string, c: string): number[] {
  const n = s.length;
  const ans: number[] = new Array(n).fill(n);

  // 向后遍历，找左边 c 离 i 最近的距离
  for (let i = 0, idx = -1; i < n; i++) {
    if (s[i] === c) idx = i;
    if (idx !== -1) ans[i] = i - idx;
  }
  // 向前遍历，找右边 c 离 i 最近的距离
  for (let i = n - 1, idx = -1; i >= 0; i--) {
    if (s[i] === c) idx = i;
    if (idx !== -1) ans[i] = Math.min(ans[i], idx - i);
  }

  return ans;
}
```

## 广度优先搜索

将 c 所在的位置当作搜索的起点，使用广度优先搜索，来更新结果。

```cpp
class Solution
{
public:
    vector<int> shortestToChar(string s, char c)
    {
        int n = s.size();
        deque<int> q;
        // 默认为 -1，表示还没更新过结果
        vector<int> ans(n, -1);
        // 将 c 字符位置当作BFS起点，并将其结果置为 0
        for (int i = 0; i < n; i++)
        {
            if (s[i] == c)
            {
                ans[i] = 0;
                q.push_back(i);
            }
        }

        int dir[2] = {1, -1};
        while (!q.empty())
        {
            int curr = q.front();
            q.pop_front();
            for (auto x : dir)
            {
                int next = curr + x;
                if (next < 0 || next >= n || ans[next] != -1)
                {
                    continue;
                }
                ans[next] = ans[curr] + 1;
                q.push_back(next);
            }
        }

        return ans;
    }
};
```
