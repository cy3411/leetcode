# 题目

给你一个非空数组，返回此数组中 第三大的数 。如果不存在，则返回数组中最大的数。

# 示例

```
输入：[3, 2, 1]
输出：1
解释：第三大的数是 1 。
```

```输入：[1, 2]
输出：2
解释：第三大的数不存在, 所以返回最大的数 2 。
```

```
输入：[2, 2, 3, 1]
输出：1
解释：注意，要求返回第三大的数，是指在所有不同数字中排第三大的数。
此例中存在两个值为 2 的数，它们都排第二。在所有不同数字中排第三大的数为 1 。
```

# 题解

## 一次遍历

遍历数组，利用 `max`,`mid`,`min` 遍历来保存最大值、次大值和第三大值。为了方便操作，可以将三个变量的初始值设置为负无穷。

遍历数组，对于数组中的值 num：

- `num > max`,将 `min` 替换为 `mid`，`mid` 替换为 `max`，`max` 替换为 `num`
- `num > mid && num < max`,将 `min` 替换为 `mid`,将 `mid` 替换为 `num`
- `num > min && num < mid`,将 `min` 替换为 `num`

```ts
function thirdMax(nums: number[]): number {
  let max = Number.NEGATIVE_INFINITY;
  let mid = Number.NEGATIVE_INFINITY;
  let min = Number.NEGATIVE_INFINITY;

  for (let num of nums) {
    // 重复的忽略
    if (num === max || num === mid || num === min) continue;
    // 按照题意赋值
    if (num > max) {
      min = mid;
      mid = max;
      max = num;
    } else if (num > mid) {
      min = mid;
      mid = num;
    } else if (num > min) {
      min = num;
    }
  }
  // 如果第三大有值就返回
  if (min !== Number.NEGATIVE_INFINITY) {
    return min;
  }
  // 第三大没值就返回最大的
  return max;
}
```
