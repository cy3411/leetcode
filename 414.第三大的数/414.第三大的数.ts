/*
 * @lc app=leetcode.cn id=414 lang=typescript
 *
 * [414] 第三大的数
 */

// @lc code=start
function thirdMax(nums: number[]): number {
  let max = Number.NEGATIVE_INFINITY;
  let mid = Number.NEGATIVE_INFINITY;
  let min = Number.NEGATIVE_INFINITY;

  for (let num of nums) {
    // 重复的忽略
    if (num === max || num === mid || num === min) continue;
    // 按照题意赋值
    if (num > max) {
      min = mid;
      mid = max;
      max = num;
    } else if (num > mid) {
      min = mid;
      mid = num;
    } else if (num > min) {
      min = num;
    }
  }
  // 如果第三大有值就返回
  if (min !== Number.NEGATIVE_INFINITY) {
    return min;
  }
  // 第三大没值就返回最大的
  return max;
}
// @lc code=end
