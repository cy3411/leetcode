# 题目
给你一个整数数组 `nums`，请你将该数组升序排列。

提示：
+ 1 <= nums.length <= 50000
+ -50000 <= nums[i] <= 50000
# 示例
```
输入：nums = [5,2,3,1]
输出：[1,2,3,5]
```

# 题解
## 快速排序
```ts
// 三路快排
function sortArray(nums: number[]): number[] {
  // 三点取中
  const medina = (arr: number[], l: number, r: number): void => {
    const mid = l + ((r - l) >> 1);
    if (arr[l] > arr[mid]) {
      [arr[l], arr[mid]] = [arr[mid], arr[l]];
    }
    if (arr[l] > arr[r]) {
      [arr[l], arr[r]] = [arr[r], arr[l]];
    }
    if (arr[mid] > arr[r]) {
      [arr[mid], arr[r]] = [arr[r], arr[mid]];
    }

    // 将中位数放到数组最前面
    [arr[l], arr[mid]] = [arr[mid], arr[l]];
  };
  // 快速排序
  const quicksort = (arr: number[], l: number, r: number): void => {
    if (l >= r) return;
    medina(arr, l, r);
    let x = l;
    let i = l + 1;
    let y = r;
    let base = arr[l];
    while (i <= y) {
      if (arr[i] < base) {
        [arr[x], arr[i]] = [arr[i], arr[x]];
        x++;
        i++;
      } else if (arr[i] > base) {
        [arr[i], arr[y]] = [arr[y], arr[i]];
        y--;
      } else {
        i++;
      }
    }
    quicksort(arr, l, x - 1);
    quicksort(arr, y + 1, r);
  };

  quicksort(nums, 0, nums.length - 1);

  return nums;
}
```

```ts
// 无监督，单边递归快排
function sortArray(nums: number[]): number[] {
  // 三点取中
  const medina = (arr: number[], l: number, r: number): void => {
    const mid = l + ((r - l) >> 1);
    if (arr[l] > arr[mid]) {
      [arr[l], arr[mid]] = [arr[mid], arr[l]];
    }
    if (arr[l] > arr[r]) {
      [arr[l], arr[r]] = [arr[r], arr[l]];
    }
    if (arr[mid] > arr[r]) {
      [arr[mid], arr[r]] = [arr[r], arr[mid]];
    }

    // 将中位数放到数组最前面
    [arr[l], arr[mid]] = [arr[mid], arr[l]];
  };
  // 快速排序
  const quicksort = (arr: number[], l: number, r: number): void => {
    while (l < r) {
      medina(arr, l, r);
      let x = l;
      let i = l + 1;
      let y = r;
      let base = arr[l];
      while (i <= y) {
        if (arr[i] < base) {
          [arr[x], arr[i]] = [arr[i], arr[x]];
          x++;
          i++;
        } else if (arr[i] > base) {
          [arr[i], arr[y]] = [arr[y], arr[i]];
          y--;
        } else {
          i++;
        }
      }
      quicksort(arr, y + 1, r);
      // 重写右边界
      r = x - 1;
    }
  };

  quicksort(nums, 0, nums.length - 1);

  return nums;
}
```
```ts
// 按照给定大小使用快排分区，然后使用插入排序最后处理
function sortArray(nums: number[]): number[] {
  // 按大小分区
  const threshold = 16;
  // 三点取中
  const medina = (arr: number[], l: number, r: number): void => {
    const mid = l + ((r - l) >> 1);
    if (arr[l] > arr[mid]) {
      [arr[l], arr[mid]] = [arr[mid], arr[l]];
    }
    if (arr[l] > arr[r]) {
      [arr[l], arr[r]] = [arr[r], arr[l]];
    }
    if (arr[mid] > arr[r]) {
      [arr[mid], arr[r]] = [arr[r], arr[mid]];
    }

    // 将中位数放到数组最前面
    [arr[l], arr[mid]] = [arr[mid], arr[l]];
  };
  // 快速排序
  const quicksort = (arr: number[], l: number, r: number): void => {
    while (r - l > threshold) {
      medina(arr, l, r);
      let x = l;
      let i = l + 1;
      let y = r;
      let base = arr[l];
      while (i <= y) {
        if (arr[i] < base) {
          [arr[x], arr[i]] = [arr[i], arr[x]];
          x++;
          i++;
        } else if (arr[i] > base) {
          [arr[i], arr[y]] = [arr[y], arr[i]];
          y--;
        } else {
          i++;
        }
      }
      quicksort(arr, y + 1, r);
      // 重写右边界
      r = x - 1;
    }
  };

  // 插入排序
  const insertsort = (arr: number[]): void => {
    const size = arr.length;
    for (let i = 1; i < size; i++) {
      for (let j = i; j > 0; j--) {
        if (arr[j] < arr[j - 1]) {
          [arr[j], arr[j - 1]] = [arr[j - 1], arr[j]];
        }
      }
    }
  };

  quicksort(nums, 0, nums.length - 1);
  insertsort(nums);
  return nums;
}
```

## 归并排序
```ts
function sortArray(nums: number[]): number[] {
  const merge = (arr: number[], l: number, r: number): void => {
    if (l >= r) return;
    let mid = l + ((r - l) >> 1);
    merge(arr, l, mid);
    merge(arr, mid + 1, r);

    // 临时数组，存储区间有序状态
    const temp = new Array(r - l + 1);
    let k = 0;
    let left = l;
    let right = mid + 1;
    while (left <= mid || right <= r) {
      if (right > r || (left <= mid && nums[left] <= nums[right])) {
        temp[k++] = nums[left++];
      } else {
        temp[k++] = nums[right++];
      }
    }

    // 合并
    for (let i = l; i <= r; i++) {
      arr[i] = temp[i - l];
    }
  };

  merge(nums, 0, nums.length - 1);
  return nums;
}
```
