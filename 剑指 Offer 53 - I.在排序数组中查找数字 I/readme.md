# 题目

统计一个数字在排序数组中出现的次数。

限制：

- 0 <= 数组长度 <= 50000

# 示例

```
输入: nums = [5,7,7,8,8,10], target = 8
输出: 2
```

# 题解

## 二分搜索

题目给出的是一个有序数组，这样我们可以使用二分搜索，分两次搜索：

1. 定义 l 为搜索第一个大于等于 target 的位置
2. 定义 r 为搜索第一个大于 target 的位置

最后判断 nums[l]和 nums[r-1]是否等于 target，是的话，那么两者之间的跨度就是答案。

```js
var search = function (nums, target) {
  const binarySearch = (isLower) => {
    let l = 0;
    let r = nums.length - 1;
    let mid;
    while (l <= r) {
      mid = l + ((r - l) >> 1);
      if (nums[mid] > target || (isLower && nums[mid] >= target)) {
        r = mid - 1;
      } else {
        l = mid + 1;
      }
    }
    return l;
  };

  let ans = 0;
  const l = binarySearch(true);
  const r = binarySearch(false) - 1;

  if (l <= r && r <= nums.length && nums[l] === target && nums[r] === target) {
    ans = r - l + 1;
  }

  return ans;
};
```
