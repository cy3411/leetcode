# 题目

给你一个字符串 `s` 和一个字符串数组 `dictionary` 作为字典，找出并返回字典中最长的字符串，该字符串可以通过删除 `s` 中的某些字符得到。

如果答案不止一个，返回长度最长且字典序最小的字符串。如果答案不存在，则返回空字符串。

# 示例

```
输入：s = "abpcplea", dictionary = ["ale","apple","monkey","plea"]
输出："apple"
```

# 题解

## 双指针

初始化指针 `i` 和 `j`，分别指向 `d` 和 `s` 的初始位置。

每次 d[i]和 s[j]互相比较：

- 相同，`i` 加 `1`,`j` 加 `1`
- 不同的话，`j` 加 `1`

遍历到最后，如果 `i` 等于 `d.length`，表示找到匹配字串。

按照题意更新答案。

```ts
function findLongestWord(s: string, dictionary: string[]): string {
  let ans = '';

  for (let d of dictionary) {
    let i = 0;
    let j = 0;
    // 双指针同时对比字符串
    while (i < d.length && j < s.length) {
      if (d[i] === s[j]) {
        i++;
      }
      j++;
    }
    if (i === d.length) {
      // 找到匹配，更新答案
      if (d.length > ans.length || (d.length === ans.length && d < ans)) {
        ans = d;
      }
    }
  }

  return ans;
}
```
