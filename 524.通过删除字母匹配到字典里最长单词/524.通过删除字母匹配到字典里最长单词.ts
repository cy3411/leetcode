/*
 * @lc app=leetcode.cn id=524 lang=typescript
 *
 * [524] 通过删除字母匹配到字典里最长单词
 */

// @lc code=start
function findLongestWord(s: string, dictionary: string[]): string {
  let ans = '';

  for (let d of dictionary) {
    let i = 0;
    let j = 0;
    // 双指针同时对比字符串
    while (i < d.length && j < s.length) {
      if (d[i] === s[j]) {
        i++;
      }
      j++;
    }
    if (i === d.length) {
      // 找到匹配，更新答案
      if (d.length > ans.length || (d.length === ans.length && d < ans)) {
        ans = d;
      }
    }
  }

  return ans;
}
// @lc code=end
