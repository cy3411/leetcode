# 题目

给你一个由 '('、')' 和小写字母组成的字符串 s。

你需要从字符串中删除最少数目的 '(' 或者 ')' (可以删除任意位置的括号)，使得剩下的「括号字符串」有效。

请返回任意一个合法字符串。

有效「括号字符串」应当符合以下 任意一条 要求：

- 空字符串或只包含小写字母的字符串
- 可以被写作 AB（A 连接 B）的字符串，其中 A 和 B 都是有效「括号字符串」
- 可以被写作 (A) 的字符串，其中 A 是一个有效的「括号字符串」

提示：

- 1 <= s.length <= 10^5
- s[i] 可能是 '('、')' 或英文小写字母

# 示例

```
输入：s = "lee(t(c)o)de)"
输出："lee(t(c)o)de"
解释："lee(t(co)de)" , "lee(t(c)ode)" 也是一个可行答案。
```

# 方法

## 栈

使用双栈分别记录左右括号的下标，遇到右括号时：

- 左括号为空，不合法，栈中记录右括号
- 左括号不给空，合法，弹出左括号栈顶

扫描完后，就可以获取到不合法的括号下标，处理一下。

```js
/**
 * @param {string} s
 * @return {string}
 */
var minRemoveToMakeValid = function (s) {
  // 双栈记录左右括号的下标
  const leftBracket = [];
  const rightBracket = [];

  for (let i = 0; i < s.length; i++) {
    const char = s[i];
    if (char === '(') {
      leftBracket.push(i);
    } else if (char === ')') {
      // 左右括号都有，表示合法，就出栈
      if (leftBracket.length) {
        leftBracket.pop();
        continue;
      }
      rightBracket.push(i);
    }
  }

  const skipIndex = [...leftBracket, ...rightBracket];
  let result = s.split('');
  // 把不合法的清空
  for (let i = 0; i < skipIndex.length; i++) {
    result[skipIndex[i]] = '';
  }

  return result.join('');
};
```

## 两次扫描

我们可以定义个常量 count:

- 遇到左括号，count+1
- 遇到右括号号，count-1

**反方向扫描时，上面的状态取反**

当 count 小于 0 的时候，肯定是不合法的括号，就可以跳过。

第一次扫描，从左往右，删掉多余的右括号
第二次扫描，从右往左，删掉多余的左括号

```js
/**
 * @param {string} s
 * @return {string}
 */
var minRemoveToMakeValid = function (s) {
  let firstResult = '';
  let count = 0;
  // 第一次从左往右扫描，删除多余的右括号
  for (let i = 0; i < s.length; i++) {
    let char = s[i];
    if (char === '(') {
      firstResult += char;
      count++;
    } else if (char === ')') {
      if (count === 0) continue;
      count--;
      firstResult += char;
    } else {
      firstResult += char;
    }
  }

  let twoResult = '';
  count = 0;
  // 第一次从右往左扫描，删除多余的左括号
  for (let i = firstResult.length - 1; i >= 0; i--) {
    let char = firstResult[i];
    if (char === ')') {
      twoResult = char + twoResult;
      count++;
    } else if (char === '(') {
      if (count === 0) continue;
      count--;
      twoResult = char + twoResult;
    } else {
      twoResult = char + twoResult;
    }
  }

  return twoResult;
};
```
