# 题目

给你一个大小为 m \* n 的矩阵 mat，矩阵由若干军人和平民组成，分别用 1 和 0 表示。

请你返回矩阵中战斗力最弱的 k 行的索引，按从最弱到最强排序。

如果第 i 行的军人数量少于第 j 行，或者两行军人数量相同但 i 小于 j，那么我们认为第 i 行的战斗力比第 j 行弱。

军人 总是 排在一行中的靠前位置，也就是说 1 总是出现在 0 之前。

提示：

- m == mat.length
- n == mat[i].length
- 2 <= n, m <= 100
- 1 <= k <= m
- matrix[i][j] 不是 0 就是 1

# 示例

```
输入：mat =
[[1,1,0,0,0],
 [1,1,1,1,0],
 [1,0,0,0,0],
 [1,1,0,0,0],
 [1,1,1,1,1]],
k = 3
输出：[2,0,3]
解释：
每行中的军人数目：
行 0 -> 2
行 1 -> 4
行 2 -> 1
行 3 -> 2
行 4 -> 5
从最弱到最强对这些行排序后得到 [2,0,3,1,4]
```

# 题解

## 排序

遍历数组，统计每个元素中 1 的个数，并将个数和索引保存到结果数组中。

对结果数组按照个数、索引排序。

取结果数组中前 k 个元素，放入答案。

```ts
function kWeakestRows(mat: number[][], k: number): number[] {
  const temp: [number, number][] = [];
  let m = mat.length;
  let n = mat[0].length;
  // 统计每个元素中1的数量，保存到temp中
  for (let i = 0; i < m; i++) {
    let count = 0;
    for (let j = 0; j < n; j++) {
      if (mat[i][j] === 0) break;
      count++;
    }
    temp.push([i, count]);
  }
  // 按照数量、索引顺序排序
  temp.sort((a: [number, number], b: [number, number]) => {
    if (a[1] === b[1]) return a[0] - b[0];
    return a[1] - b[1];
  });

  const ans = [];
  // 输出前k个元素
  for (let i = 0; i < k; i++) {
    ans.push(temp[i][0]);
  }

  return ans;
}
```
