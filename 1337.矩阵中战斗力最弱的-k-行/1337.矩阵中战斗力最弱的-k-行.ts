/*
 * @lc app=leetcode.cn id=1337 lang=typescript
 *
 * [1337] 矩阵中战斗力最弱的 K 行
 */

// @lc code=start
function kWeakestRows(mat: number[][], k: number): number[] {
  const temp: [number, number][] = [];
  let m = mat.length;
  let n = mat[0].length;
  // 统计每个元素中1的数量，保存到temp中
  for (let i = 0; i < m; i++) {
    let count = 0;
    for (let j = 0; j < n; j++) {
      if (mat[i][j] === 0) break;
      count++;
    }
    temp.push([i, count]);
  }
  // 按照数量、索引顺序排序
  temp.sort((a: [number, number], b: [number, number]) => {
    if (a[1] === b[1]) return a[0] - b[0];
    return a[1] - b[1];
  });

  const ans = [];
  // 输出前k个元素
  for (let i = 0; i < k; i++) {
    ans.push(temp[i][0]);
  }

  return ans;
}
// @lc code=end
