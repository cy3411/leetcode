/*
 * @lc app=leetcode.cn id=424 lang=javascript
 *
 * [424] 替换后的最长重复字符
 */

// @lc code=start
/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var characterReplacement = function (s, k) {
  const size = s.length;
  // 存储指针范围的字符数量
  const idx = new Array(26).fill(0);
  const base = 'A'.charCodeAt();
  let i = 0;
  let j = 0;
  let maxIdx = 0;

  while (j < size) {
    idx[s.charCodeAt(j) - base]++;
    maxIdx = Math.max(maxIdx, idx[s.charCodeAt(j) - base]);
    // 判断双指针范围内最少字符数量是否在范围内
    if (j - i + 1 - maxIdx > k) {
      idx[s.charCodeAt(i) - base]--;
      i++;
    }
    j++;
  }

  return j - i;
};
// @lc code=end
