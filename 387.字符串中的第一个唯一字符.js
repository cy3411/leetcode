/*
 * @lc app=leetcode.cn id=387 lang=javascript
 *
 * [387] 字符串中的第一个唯一字符
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var firstUniqChar = function (s) {
  const size = s.length;
  const hash = new Map();
  const queue = [];

  for (const [index, char] of Array.from(s).entries()) {
    if (hash.has(char)) {
      hash.set(char, -1);
      while (queue.length && hash.get(queue[0][0]) === -1) {
        queue.shift();
      }
    } else {
      hash.set(char, index);
      queue.push([char, index]);
    }
  }

  return queue.length ? queue[0][1] : -1;
};
// @lc code=end
