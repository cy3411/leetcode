# 题目

给你一个整数数组 arr ，请使用 煎饼翻转 完成对数组的排序。

一次煎饼翻转的执行过程如下：

- 选择一个整数 k ，1 <= k <= arr.length
- 反转子数组 arr[0...k-1]（下标从 0 开始）
  例如，arr = [3,2,1,4] ，选择 k = 3 进行一次煎饼翻转，反转子数组 [3,2,1] ，得到 arr = [1,2,3,4] 。

以数组形式返回能使 arr 有序的煎饼翻转操作所对应的 k 值序列。任何将数组排序且翻转次数在 10 \* arr.length 范围内的有效答案都将被判断为正确。

提示：

- 1 <= arr.length <= 100
- 1 <= arr[i] <= arr.length
- arr 中的所有整数互不相同（即，arr 是从 1 到 arr.length 整数的一个排列）

# 示例

```
输入：[3,2,4,1]
输出：[4,2,4,3]
解释：
我们执行 4 次煎饼翻转，k 值分别为 4，2，4，和 3。
初始状态 arr = [3, 2, 4, 1]
第一次翻转后（k = 4）：arr = [1, 4, 2, 3]
第二次翻转后（k = 2）：arr = [4, 1, 2, 3]
第三次翻转后（k = 4）：arr = [3, 2, 1, 4]
第四次翻转后（k = 3）：arr = [1, 2, 3, 4]，此时已完成排序。
```

```
输入：[1,2,3]
输出：[]
解释：
输入已经排序，因此不需要翻转任何内容。
请注意，其他可能的答案，如 [3，3] ，也将被判断为正确。
```

# 方法

## 排序

结果需要的是升序，由于只能反转前 n 位，可以考虑先把最大的翻转到第一位，然后再反转到最后，依次执行下去即可。

```js
/**
 * @param {number[]} arr
 * @return {number[]}
 */
var pancakeSort = function (arr) {
  const reverse = (arr, n, index) => {
    let i = 0;
    let j = n - 1;
    while (i < j) {
      [arr[i], arr[j]] = [arr[j], arr[i]];
      index[arr[i]] = i;
      index[arr[j]] = j;
      i++;
      j--;
    }
    return;
  };
  const index = new Array(arr.length + 1).fill(-1);
  const result = [];
  // 保存排序数字对应的下标
  for (let i = 0; i < arr.length; i++) {
    index[arr[i]] = i;
  }

  for (let i = arr.length; i >= 1; i--) {
    // 如果下标和原数组的位置对应，不需要排序。
    // 比如：数字4就在下标3的位置，类似[3,2,1,4]
    if (index[i] === i - 1) continue;
    // 需要反转的数已经在第一位了，就不用反转了
    if (index[i] !== 0) {
      result.push(index[i] + 1);
      reverse(arr, index[i] + 1, index);
    }
    // 如果第一位是数字1了也不需要反转了
    if (i !== 1) {
      result.push(i);
      reverse(arr, i, index);
    }
  }

  return result;
};
```

```ts
function pancakeSort(arr: number[]): number[] {
  const n = arr.length;
  const reverse = (arr: number[], idxs: number[], len: number): void => {
    let i = 0;
    let j = len - 1;
    while (i < j) {
      [arr[i], arr[j]] = [arr[j], arr[i]];
      [idxs[arr[i]], idxs[arr[j]]] = [i, j];
      i++;
      j--;
    }
  };
  // 保存数字对应的arr下标
  const idxs = new Array(n + 1).fill(-1);
  for (const [idx, num] of arr.entries()) {
    idxs[num] = idx;
  }

  const ans: number[] = [];
  // 遍历每个数字的下标
  for (let i = n; i >= 1; i--) {
    // 如果当前数字已经在合适的位置，这里不用翻转
    if (idxs[i] === i - 1) continue;
    // 先将最大的数字翻转到第一位
    // 如果已经在第一位了，就不用翻转了
    if (idxs[i] + 1 !== 1) {
      // 将反转长度放入答案
      ans.push(idxs[i] + 1);
      reverse(arr, idxs, idxs[i] + 1);
    }
    // 再将最大的数字翻转到最后的位置
    // 如果当前是第一个数字就不用再翻转了
    if (i !== 1) {
      ans.push(i);
      reverse(arr, idxs, i);
    }
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    void reverse(vector<int> &arr, vector<int> &idxs, int len)
    {
        int i = 0, j = len - 1;
        while (i < j)
        {
            swap(arr[i], arr[j]);
            idxs[arr[i]] = i;
            idxs[arr[j]] = j;
            i++;
            j--;
        }
    }
    vector<int> pancakeSort(vector<int> &arr)
    {
        int n = arr.size();
        vector<int> idxs(n + 1);
        for (int i = 0; i < n; i++)
        {
            idxs[arr[i]] = i;
        }

        vector<int> ans;
        for (int i = n; i >= 1; i--)
        {
            if (idxs[i] == i - 1)
            {
                continue;
            }

            if (idxs[i] + 1 != 1)
            {
                ans.push_back(idxs[i] + 1);
                reverse(arr, idxs, idxs[i] + 1);
            }

            if (i != 1)
            {
                ans.push_back(i);
                reverse(arr, idxs, i);
            }
        }

        return ans;
    }
};
```
