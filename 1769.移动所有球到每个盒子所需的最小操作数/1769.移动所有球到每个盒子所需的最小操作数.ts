/*
 * @lc app=leetcode.cn id=1769 lang=typescript
 *
 * [1769] 移动所有球到每个盒子所需的最小操作数
 */

// @lc code=start
function minOperations(boxes: string): number[] {
  const n = boxes.length;
  const ans: number[] = new Array(n).fill(0);
  // 遍历终点
  for (let i = 0; i < n; i++) {
    let cnt = 0;
    // 遍历每个球，计算转移到终点的操作数
    for (let j = 0; j < n; j++) {
      if (boxes[j] === '1') {
        cnt += Math.abs(j - i);
      }
    }
    ans[i] = cnt;
  }
  return ans;
}
// @lc code=end
