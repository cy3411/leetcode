# 题目

如果字符串中不含有任何 `'aaa'`，`'bbb'` 或 `'ccc'` 这样的字符串作为子串，那么该字符串就是一个「快乐字符串」。

给你三个整数 `a`，`b` ，`c`，请你返回 **任意一个** 满足下列全部条件的字符串 `s`：

- `s` 是一个尽可能长的快乐字符串。
- `s` 中 最多 有 `a` 个字母 `'a'`、`b` 个字母 `'b'`、`c` 个字母 `'c'` 。
- `s` 中只含有 `'a'`、`'b'` 、`'c'` 三种字母。

如果不存在这样的字符串 `s` ，请返回一个空字符串 `""`。

提示：

- $\color{burlywood} 0 \leq a, b, c \leq 100$
- $\color{burlywood} a + b + c > 0$

# 示例

```
输入：a = 1, b = 1, c = 7
输出："ccaccbcc"
解释："ccbccacc" 也是一种正确答案。
```

```
输入：a = 2, b = 2, c = 1
输出："aabbc"
```

```
输入：a = 7, b = 1, c = 0
输出："aabaa"
解释：这是该测试用例的唯一正确答案。
```

# 题解

## 贪心

要找到符合题意的更长的字符串，要尽可能优先使用当前数量最多的字符，因为最后同一种字符串剩余的越多，越会出现字符连续相同的情况。

依次从最多的数量的字符开始，如果出现连续 3 个的情况，则跳过该字符，直到找到可以添加的字符。如果尝试所有字符都不能添加，则直接退出，此时已经构成的字符串就是最长的。

```ts
type Case = [number, string];

function longestDiverseString(a: number, b: number, c: number): string {
  const ans = [];
  const map: Case[] = [
    [a, 'a'],
    [b, 'b'],
    [c, 'c'],
  ];

  while (1) {
    map.sort((a, b) => b[0] - a[0]);
    // 是否还可以在添加字符
    let hasNext = false;
    for (let i = 0; i < map.length; i++) {
      const [count, char] = map[i];
      // 没有可用的字符了
      if (count <= 0) break;

      const n = ans.length;
      // 如果连续两个字符相同，则不能添加
      if (n >= 2 && ans[n - 1] === char && ans[n - 2] === char) continue;
      hasNext = true;
      ans.push(char);
      map[i][0]--;
      break;
    }
    // 无法拼接字符串
    if (!hasNext) break;
  }

  return ans.join('');
}
```

```cpp
typedef pair<int, char> PII;

class Solution
{
public:
    string longestDiverseString(int a, int b, int c)
    {
        vector<PII> p = {{a, 'a'}, {b, 'b'}, {c, 'c'}};
        string ans;

        while (1)
        {
            sort(p.begin(), p.end(), [](const PII &p1, const PII &p2)
                 { return p1.first > p2.first; });

            bool hasNext = false;
            for (auto &[count, ch] : p)
            {
                if (count <= 0)
                {

                    break;
                }
                int m = ans.size();
                if (m >= 2 && ans[m - 1] == ch && ans[m - 2] == ch)
                {
                    continue;
                }
                hasNext = true;
                ans.push_back(ch);
                count--;
                break;
            }
            if (!hasNext)
            {
                break;
            }
        }

        return ans;
    }
};
```
