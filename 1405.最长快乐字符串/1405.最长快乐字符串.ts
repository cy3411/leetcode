/*
 * @lc app=leetcode.cn id=1405 lang=typescript
 *
 * [1405] 最长快乐字符串
 */

// @lc code=start
type Case = [number, string];

function longestDiverseString(a: number, b: number, c: number): string {
  const ans = [];
  const map: Case[] = [
    [a, 'a'],
    [b, 'b'],
    [c, 'c'],
  ];

  while (1) {
    map.sort((a, b) => b[0] - a[0]);
    // 是否还可以在添加字符
    let hasNext = false;
    for (let i = 0; i < map.length; i++) {
      const [count, char] = map[i];
      // 没有可用的字符了
      if (count <= 0) break;

      const n = ans.length;
      // 如果连续两个字符相同，则不能添加
      if (n >= 2 && ans[n - 1] === char && ans[n - 2] === char) continue;
      hasNext = true;
      ans.push(char);
      map[i][0]--;
      break;
    }
    // 无法拼接字符串
    if (!hasNext) break;
  }

  return ans.join('');
}
// @lc code=end
