/*
 * @lc app=leetcode.cn id=1405 lang=cpp
 *
 * [1405] 最长快乐字符串
 */

// @lc code=start

typedef pair<int, char> PII;

class Solution
{
public:
    string longestDiverseString(int a, int b, int c)
    {
        vector<PII> p = {{a, 'a'}, {b, 'b'}, {c, 'c'}};
        string ans;

        while (1)
        {
            sort(p.begin(), p.end(), [](const PII &p1, const PII &p2)
                 { return p1.first > p2.first; });

            bool hasNext = false;
            for (auto &[count, ch] : p)
            {
                if (count <= 0)
                {

                    break;
                }
                int m = ans.size();
                if (m >= 2 && ans[m - 1] == ch && ans[m - 2] == ch)
                {
                    continue;
                }
                hasNext = true;
                ans.push_back(ch);
                count--;
                break;
            }
            if (!hasNext)
            {
                break;
            }
        }

        return ans;
    }
};
// @lc code=end
