/*
 * @lc app=leetcode.cn id=1716 lang=cpp
 *
 * [1716] 计算力扣银行的钱
 */

// @lc code=start
class Solution
{
public:
    int totalMoney(int n)
    {
        int weeks = n / 7;
        int days = n % 7;
        int moneyWeek = (1 + 7) * 7 / 2;

        int i = 1;
        int ans = 0;
        while (i <= weeks)
        {
            if (i == 1)
            {
                ans += moneyWeek;
            }
            else
            {
                ans += ans + (i - 1) * 7;
            }
            i++;
        }

        ans += (i + i + days - 1) * days / 2;

        return ans;
    }
};
// @lc code=end
