# 题目

Hercy 想要为购买第一辆车存钱。他 每天 都往力扣银行里存钱。

最开始，他在周一的时候存入 `1` 块钱。从周二到周日，他每天都比前一天多存入 `1` 块钱。在接下来每一个周一，他都会比 前一个周一 多存入 `1` 块钱。

给你 `n` ，请你返回在第 `n` 天结束的时候他在力扣银行总共存了多少块钱。

提示：

- $\color{burlywood}1 \leq n \leq 1000$

# 示例

```
输入：n = 4
输出：10
解释：第 4 天后，总额为 1 + 2 + 3 + 4 = 10 。
```

```
输入：n = 10
输出：37
解释：第 10 天后，总额为 (1 + 2 + 3 + 4 + 5 + 6 + 7) + (2 + 3 + 4) = 37 。注意到第二个星期一，Hercy 存入 2 块钱。
```

# 题解

## 模拟

每周的存钱数是一个等差数列，同时下一周比前一周要多 7 元钱。

我们可以先计算出有多少周，利用上面的特性，计算出 n 周后的总额。

最后把剩下的天数的存钱累加上去就可以了。

```ts
function totalMoney(n: number): number {
  // 周的数量
  const weeks = (n / 7) >> 0;
  // 剩余天数
  const days = n % 7;

  let i = 1;
  let ans = 0;
  // 计算整周的存钱
  while (i <= weeks) {
    ans += ((i + i + 6) * 7) / 2;
    i++;
  }
  // 计算剩余天数的存钱
  ans += ((i + i + (days - 1)) * days) / 2;

  return ans;
}
```

```cpp
class Solution
{
public:
    int totalMoney(int n)
    {
        int weeks = n / 7;
        int days = n % 7;
        int moneyWeek = (1 + 7) * 7 / 2;

        int i = 1;
        int ans = 0;
        while (i <= weeks)
        {
            if (i == 1)
            {
                ans += moneyWeek;
            }
            else
            {
                ans += ans + (i - 1) * 7;
            }
            i++;
        }

        ans += (i + i + days - 1) * days / 2;

        return ans;
    }
};
```
