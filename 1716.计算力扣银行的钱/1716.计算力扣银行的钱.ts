/*
 * @lc app=leetcode.cn id=1716 lang=typescript
 *
 * [1716] 计算力扣银行的钱
 */

// @lc code=start
function totalMoney(n: number): number {
  // 周的数量
  const weeks = (n / 7) >> 0;
  // 剩余天数
  const days = n % 7;

  let i = 1;
  let ans = 0;
  // 计算整周的存钱
  while (i <= weeks) {
    ans += ((i + i + 6) * 7) / 2;
    i++;
  }
  // 计算剩余天数的存钱
  ans += ((i + i + (days - 1)) * days) / 2;

  return ans;
}
// @lc code=end
