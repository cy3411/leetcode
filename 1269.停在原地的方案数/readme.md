# 题目
有一个长度为 `arrLen` 的数组，开始有一个指针在索引 `0` 处。

每一步操作中，你可以将指针向左或向右移动 1 步，或者停在原地（指针不能被移动到数组范围外）。

给你两个整数 `steps` 和 `arrLen` ，请你计算并返回：在恰好执行 `steps` 次操作以后，指针仍然指向索引 `0` 处的方案数。

由于答案可能会很大，请返回方案数 **模** `10^9 + 7` 后的结果。

提示：
+ 1 <= steps <= 500
+ 1 <= arrLen <= 10^6

# 示例
```
输入：steps = 3, arrLen = 2
输出：4
解释：3 步后，总共有 4 种不同的方法可以停在索引 0 处。
向右，向左，不动
不动，向右，向左
向右，不动，向左
不动，不动，不动
```
# 题解
## 动态规划
**状态**
定义`dp[i][j]`，表示在 `i` 步操作之后，指针位于下标 `j` 的方案数。其中，`i` 的取值范围是 `0≤i≤steps`，`j` 的取值范围是 `0≤j≤arrLen−1`。

**选择**
每一步操作中，指针可以不动、左移或者右移。  
当 `1 <= i <= steps`时， `dp[i][j]` 的状态可以通过3种状态转移过来：
+ `dp[i-1][j]`，不动
+ `dp[i-1][j-1]`, 左移
+ `dp[i-1][j+1]`，右移

`dp[i][j] = dp[i-1][j] + dp[i-1][j-1] + dp[i-1][j+1]`

需要考虑`j`的边界 `0<=j<=arrLen-1`

**BaseCase**
`dp[0][0]=1`，原地也算一种方案。

```ts
function numWays(steps: number, arrLen: number): number {
  const mod = 10 ** 9 + 7;
  // 下标移动的范围不能超过steps
  // 这里可以限制一下下标的范围，做个优化
  const limit = Math.min(arrLen - 1, steps);

  const dp = new Array(steps + 1).fill(0).map(() => new Array(limit + 1).fill(0));
  dp[0][0] = 1;

  for (let i = 1; i <= steps; i++) {
    for (let j = 0; j <= limit; j++) {
      // 原地
      dp[i][j] = dp[i - 1][j];
      // 左移
      if (j - 1 >= 0) {
        dp[i][j] = (dp[i][j] + dp[i - 1][j - 1]) % mod;
      }
      // 右移
      if (j + 1 <= limit) {
        dp[i][j] = (dp[i][j] + dp[i - 1][j + 1]) % mod;
      }
    }
  }

  return dp[steps][0];
}
```