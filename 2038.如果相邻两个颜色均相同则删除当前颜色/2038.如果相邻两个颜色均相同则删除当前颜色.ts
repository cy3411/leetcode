/*
 * @lc app=leetcode.cn id=2038 lang=typescript
 *
 * [2038] 如果相邻两个颜色均相同则删除当前颜色
 */

// @lc code=start
function winnerOfGame(colors: string): boolean {
  // Alice可以操作的次数
  let acnt = 0;
  // Bob可以操作的次数
  let bcnt = 0;
  // 当前颜色
  let curr = '';
  // 当前颜色连续出现的次数
  let cnt = 0;
  for (const color of colors) {
    if (curr === color) {
      cnt++;
      // 连续3个颜色可以增加一次操作次数
      if (cnt >= 3) {
        if (curr === 'A') {
          acnt++;
        } else if (curr === 'B') {
          bcnt++;
        }
      }
    } else {
      curr = color;
      cnt = 1;
    }
  }
  // 如果Alice操作次数大于Bob操作次数，则Alice胜利
  return acnt > bcnt;
}
// @lc code=end
