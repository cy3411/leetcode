/*
 * @lc app=leetcode.cn id=2038 lang=cpp
 *
 * [2038] 如果相邻两个颜色均相同则删除当前颜色
 */

// @lc code=start
class Solution
{
public:
    bool winnerOfGame(string colors)
    {
        int fraq[] = {0, 0}, cnt = 0;
        char curr = 'C';
        for (auto c : colors)
        {
            if (curr == c)
            {
                cnt++;
                if (cnt >= 3)
                {
                    fraq[curr - 'A']++;
                }
            }
            else
            {
                curr = c;
                cnt = 1;
            }
        }

        return fraq[0] > fraq[1];
    }
};
// @lc code=end
