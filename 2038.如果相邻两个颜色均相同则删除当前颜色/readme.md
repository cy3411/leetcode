# 题目

总共有 `n` 个颜色片段排成一列，每个颜色片段要么是 `'A'` 要么是 `'B'` 。给你一个长度为 `n` 的字符串 `colors` ，其中 `colors[i]` 表示第 `i` 个颜色片段的颜色。

`Alice` 和 `Bob` 在玩一个游戏，他们 **轮流** 从这个字符串中删除颜色。`Alice` 先手 。

- 如果一个颜色片段为 `'A'` 且 **相邻两个颜色** 都是颜色 `'A'` ，那么 `Alice` 可以删除该颜色片段。`Alice` **不可以** 删除任何颜色 `'B'` 片段。
- 如果一个颜色片段为 `'B'` 且 **相邻两个颜色** 都是颜色 `'B'` ，那么 `Bob` 可以删除该颜色片段。`Bob` **不可以** 删除任何颜色 `'A'` 片段。
- `Alice` 和 `Bob` 不能 从字符串两端删除颜色片段。
- 如果其中一人无法继续操作，则该玩家 **输** 掉游戏且另一玩家 **获胜** 。

假设 `Alice` 和 `Bob` 都采用最优策略，如果 `Alice` 获胜，请返回 `true`，否则 `Bob` 获胜，返回 `false`。

提示：

- $1 \leq colors.length \leq 10^5$
- `colors` 只包含字母 `'A'` 和 `'B'`

# 示例

```
输入：colors = "AAABABB"
输出：true
解释：
AAABABB -> AABABB
Alice 先操作。
她删除从左数第二个 'A' ，这也是唯一一个相邻颜色片段都是 'A' 的 'A' 。

现在轮到 Bob 操作。
Bob 无法执行任何操作，因为没有相邻位置都是 'B' 的颜色片段 'B' 。
因此，Alice 获胜，返回 true 。
```

```
输入：colors = "AA"
输出：false
解释：
Alice 先操作。
只有 2 个 'A' 且它们都在字符串的两端，所以她无法执行任何操作。
因此，Bob 获胜，返回 false 。
```

# 题解

## 计数

题目给出不能删除字符串两端的颜色片段，所以连续字符串能够给出的操作次数是连续字符串的长度减去 2 。

同时删除颜色片段后也不会影响到对方的操作，因此可以计算两个用户的可操作次数，记做 `A` 和 `B` 。

当 `A` 大于 `B` 时，说明 `Alice` 获胜，返回 `true` 。

```ts
function winnerOfGame(colors: string): boolean {
  // Alice可以操作的次数
  let acnt = 0;
  // Bob可以操作的次数
  let bcnt = 0;
  // 当前颜色
  let curr = '';
  // 当前颜色连续出现的次数
  let cnt = 0;
  for (const color of colors) {
    if (curr === color) {
      cnt++;
      // 连续3个颜色可以增加一次操作次数
      if (cnt >= 3) {
        if (curr === 'A') {
          acnt++;
        } else if (curr === 'B') {
          bcnt++;
        }
      }
    } else {
      curr = color;
      cnt = 1;
    }
  }
  // 如果Alice操作次数大于Bob操作次数，则Alice胜利
  return acnt > bcnt;
}
```

```cpp
class Solution
{
public:
    bool winnerOfGame(string colors)
    {
        int fraq[] = {0, 0}, cnt = 0;
        char curr = 'C';
        for (auto c : colors)
        {
            if (curr == c)
            {
                cnt++;
                if (cnt >= 3)
                {
                    fraq[curr - 'A']++;
                }
            }
            else
            {
                curr = c;
                cnt = 1;
            }
        }

        return fraq[0] > fraq[1];
    }
};
```
