/*
 * @lc app=leetcode.cn id=1078 lang=typescript
 *
 * [1078] Bigram 分词
 */

// @lc code=start
function findOcurrences(text: string, first: string, second: string): string[] {
  const ans: string[] = [];
  const arr = text.split(' ');

  for (let i = 0; i < arr.length - 2; i++) {
    // first和second相同，将第三个单词放入ans
    if (arr[i] === first && arr[i + 1] === second) {
      ans.push(arr[i + 2]);
    }
  }

  return ans;
}
// @lc code=end
