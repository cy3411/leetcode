# 题目

给出第一个词 `first` 和第二个词 `second`，考虑在某些文本 `text` 中可能以 `"first second third"` 形式出现的情况，其中 `second` 紧随 `first` 出现，`third` 紧随 `second` 出现。

对于每种这样的情况，将第三个词 `"third"` 添加到答案中，并返回答案。

提示：

- $\color{BurlyWood}1 \leq text.length \leq 1000$
- `text` 由小写英文字母和空格组成
- `text` 中的所有单词之间都由 **单个空格字符** 分隔
- $\color{BurlyWood}1 \leq first.length, second.length \leq 10$
- `first` 和 `second` 由小写英文字母组成

# 示例

```
输入：text = "alice is a good girl she is a good student", first = "a", second = "good"
输出：["girl","student"]
```

```
输入：text = "we will we will rock you", first = "we", second = "will"
输出：["we","rock"]
```

# 题解

## 遍历

遍历 text , 当前元素和下一个元素与 first 和 second 同时相同时，将第三个单词放入 ans 中。

```ts
function findOcurrences(text: string, first: string, second: string): string[] {
  const ans: string[] = [];
  const arr = text.split(' ');

  for (let i = 0; i < arr.length - 2; i++) {
    // first和second相同，将第三个单词放入ans
    if (arr[i] === first && arr[i + 1] === second) {
      ans.push(arr[i + 2]);
    }
  }

  return ans;
}
```
