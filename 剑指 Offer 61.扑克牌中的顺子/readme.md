# 题目

从若干副扑克牌中随机抽 `5` 张牌，判断是不是一个顺子，即这 `5` 张牌是不是连续的。`2 ～ 10` 为数字本身，`A` 为 `1`，`J` 为 `11`，`Q` 为 `12`，`K` 为 `13`，而大、小王为 `0` ，可以看成任意数字。`A` 不能视为 `14`。

限制：

- 数组长度为 `5`
- 数组的数取值为 `[0, 13]`

# 示例

```
输入: [1,2,3,4,5]
输出: True
```

```
输入: [0,0,1,2,5]
输出: True
```

# 题解

## 排序

对 `nums` 升序排序，然后遍历 `nums` ， 统计 `0` 的个数，也是大小王的个数。 还需要统计是否还有连续相同的其他数字，如果有，则不是顺子。

根据 `0` 的个数，可以得到剩下数字的区间，计算区间内最大值和最小值的差值，如果小于 `5`，则是顺子。

```ts
function isStraight(nums: number[]): boolean {
  nums.sort((a, b) => a - b);
  // 统计 0 的个数
  let jokes = 0;
  for (const n of nums) {
    if (n === 0) jokes++;
  }
  // 除了 0 ，是否还有其他数字重复
  for (let i = jokes; i < 5; i++) {
    if (nums[i] === nums[i - 1]) return false;
  }
  // 当前序列的最大值和最小值，两者之间差值不能大于 5
  let maxNum = nums[4];
  let minNum = jokes === 5 ? nums[4] : nums[jokes];
  if (maxNum - minNum < 5) {
    return true;
  }

  return false;
}
```

```cpp
class Solution
{
public:
    bool isStraight(vector<int> &nums)
    {
        sort(nums.begin(), nums.end());
        int jokes = 0;
        for (auto n : nums)
        {
            if (n == 0)
            {
                jokes++;
            }
        }
        for (int i = jokes + 1; i < nums.size(); i++)
        {
            if (nums[i] == nums[i - 1])
            {
                return false;
            }
        }

        int maxNum = nums[4];
        int minNum = jokes == 5 ? nums[4] : nums[jokes];
        if (maxNum - minNum < 5)
        {
            return true;
        }
        return false;
    }
};
```
