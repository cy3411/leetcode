// @algorithm @lc id=100341 lang=typescript
// @title bu-ke-pai-zhong-de-shun-zi-lcof
// @test([0,0,1,2,5])=true
function isStraight(nums: number[]): boolean {
  nums.sort((a, b) => a - b);
  // 统计 0 的个数
  let jokes = 0;
  for (const n of nums) {
    if (n === 0) jokes++;
  }
  // 除了 0 ，是否还有其他数字重复
  for (let i = jokes + 1; i < 5; i++) {
    if (nums[i] === nums[i - 1]) return false;
  }
  // 当前序列的最大值和最小值，两者之间差值不能大于 5
  let maxNum = nums[4];
  let minNum = jokes === 5 ? nums[4] : nums[jokes];
  if (maxNum - minNum < 5) {
    return true;
  }

  return false;
}
