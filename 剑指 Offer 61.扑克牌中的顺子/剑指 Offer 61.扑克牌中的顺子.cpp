class Solution
{
public:
    bool isStraight(vector<int> &nums)
    {
        sort(nums.begin(), nums.end());
        int jokes = 0;
        for (auto n : nums)
        {
            if (n == 0)
            {
                jokes++;
            }
        }
        for (int i = jokes + 1; i < nums.size(); i++)
        {
            if (nums[i] == nums[i - 1])
            {
                return false;
            }
        }

        int maxNum = nums[4];
        int minNum = jokes == 5 ? nums[4] : nums[jokes];
        if (maxNum - minNum < 5)
        {
            return true;
        }
        return false;
    }
};