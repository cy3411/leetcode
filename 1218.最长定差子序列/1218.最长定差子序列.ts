/*
 * @lc app=leetcode.cn id=1218 lang=typescript
 *
 * [1218] 最长定差子序列
 */

// @lc code=start
function longestSubsequence(arr: number[], difference: number): number {
  const n = arr.length;
  const dp = new Map<number, number>();

  for (let x of arr) {
    // 当前x的最长子序列长度
    dp.set(x, (dp.get(x - difference) || 0) + 1);
  }
  // 取出所有 dp[x] 的值，取其中最大的
  return Math.max(...dp.values());
}
// @lc code=end
