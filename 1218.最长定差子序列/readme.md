# 题目

给你一个整数数组 `arr` 和一个整数 `difference`，请你找出并返回 `arr` 中最长等差子序列的长度，该子序列中相邻元素之间的差等于 `difference` 。

**子序列** 是指在不改变其余元素顺序的情况下，通过删除一些元素或不删除任何元素而从 `arr` 派生出来的序列。

# 示例

```
输入：arr = [1,2,3,4], difference = 1
输出：4
解释：最长的等差子序列是 [1,2,3,4]。
```

```
输入：arr = [1,3,5,7], difference = 1
输出：1
解释：最长的等差子序列是任意单个元素。
```

# 题解

## 动态规划

**状态定义**

dp[i] 表示以 `arr[i]` 为结尾的最长等差子序列的长度

**状态转移**

$$
dp[i]=dp[j]+1, j\in[0,i-1] \land arr[i]-arr[j]\equiv difference
$$

```ts
function longestSubsequence(arr: number[], difference: number): number {
  const n = arr.length;
  const dp = new Map<number, number>();

  for (let x of arr) {
    // 当前x的最长子序列长度
    dp.set(x, (dp.get(x - difference) || 0) + 1);
  }
  // 取出所有 dp[x] 的值，取其中最大的
  return Math.max(...dp.values());
}
```
