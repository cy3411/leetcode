# 题目

已知存在一个按非降序排列的整数数组 `nums` ，数组中的值不必互不相同。

在传递给函数之前，`nums` 在预先未知的某个下标 `k（0 <= k < nums.length）`上进行了 旋转 ，使数组变为 `[nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]`（下标 从 0 开始 计数）。例如， `[0,1,2,4,4,4,5,6,6,7]` 在下标 5 处经旋转后可能变为 `[4,5,6,6,7,0,1,2,4,4]` 。

给你 旋转后 的数组 nums 和一个整数 target ，请你编写一个函数来判断给定的目标值是否存在于数组中。如果 nums 中存在这个目标值 target ，则返回 true ，否则返回 false 。

提示：

- 1 <= nums.length <= 5000
- -104 <= nums[i] <= 104
- 题目数据保证 nums 在预先未知的某个下标上进行了旋转
- -104 <= target <= 104

# 示例

```
输入：nums = [2,5,6,0,0,1,2], target = 0
输出：true
```

```
输入：nums = [2,5,6,0,0,1,2], target = 3
输出：false
```

# 题解

这是 [搜索旋转排序数组](https://gitee.com/cy3411/leecode/tree/master/33.%E6%90%9C%E7%B4%A2%E6%97%8B%E8%BD%AC%E6%8E%92%E5%BA%8F%E6%95%B0%E7%BB%84) 的延伸题目，本题中的 nums 可能包含重复元素。

当数组出现重复数组的时候，会出现`nums[left]=nums[mid]=nums[right]`，无法判断左右哪个区间。

对于这种情况，可以将 left 加 1 和 right 减 1，同时缩窄，在新的区间上继续。

```js
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {boolean}
 */
var search = function (nums, target) {
  const size = nums.length;
  if (size === 0) return false;
  if (size === 1) return target === nums[0];

  let left = 0;
  let right = size - 1;

  while (left <= right) {
    let mid = left + ((right - left) >> 1);
    if (nums[mid] === target) {
      return true;
    }
    // 无法区分左右区间，需要收窄范围
    if (nums[left] === nums[mid] && nums[right] === nums[mid]) {
      left++;
      right--;
      // 左边是非降序区间
    } else if (nums[left] <= nums[mid]) {
      // target是否在左边区间
      if (nums[left] <= target && target < nums[mid]) {
        // 将right缩窄左边范围
        right = mid - 1;
      } else {
        // 将left缩窄到右边区间
        left = mid + 1;
      }
      // 右边是非降序排列
    } else {
      // target是否在右边区域
      if (nums[mid] < target && target <= nums[size - 1]) {
        // 将left缩窄到右边范围
        left = mid + 1;
      } else {
        // 将right缩窄到左边范围
        right = mid - 1;
      }
    }
  }

  return false;
};
```

```ts
function search(nums: number[], target: number): boolean {
  const n = nums.length;
  if (n === 1) return nums[0] === target;
  if (nums[0] === target) return true;

  let head = 0;
  let tail = n - 1;
  let mid: number;

  // 由于重复元素，避免mid位置的元素和头尾元素相等，无法二分
  while (head < tail && nums[head] === nums[0]) {
    head++;
  }
  while (head < tail && nums[tail] === nums[0]) {
    tail--;
  }

  while (head <= tail) {
    mid = head + ((tail - head) >> 1);
    if (nums[mid] === target) return true;
    if (nums[mid] <= nums[tail]) {
      // nums[mid]在右边升序区间中
      if (target > nums[mid] && target <= nums[tail]) {
        head = mid + 1;
      } else {
        tail = mid - 1;
      }
    } else {
      // nums[mid]在左边升序区间中
      if (target < nums[mid] && target >= nums[head]) {
        tail = mid - 1;
      } else {
        head = mid + 1;
      }
    }
  }

  return false;
}
```

还有一种比较直观的做法就是找到旋转点 idx，分别对[left,idx]和[idx+1,right]区间进行二分查找。

因为这里有重复的数组，如果旋转使得重复元素被分割，会破坏二分查找的二段性。比如[2,3,4,5,0,1,2]，如果 target 是 2 的话，不知道去左边还是右边了。

所以需要先恢复二段性，就是前后比较，如果相同，将 right 指针前移。

`因为「二分」的本质是二段性，并非单调性。只要一段满足某个性质，另外一段不满足某个性质，就可以用「二分」。`

```js
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {boolean}
 */
var search = function (nums, target) {
  const size = nums.length;
  if (size === 0) return false;
  if (size === 1) return target === nums[0];

  const binarySearch = (nums, left, right, target) => {
    while (left < right) {
      let mid = left + ((right - left) >> 1);
      if (nums[mid] >= target) {
        right = mid;
      } else {
        left = mid + 1;
      }
    }
    return nums[left] === target ? left : -1;
  };
  let left = 0;
  let right = size - 1;

  // 恢复二段性
  while (left < right && nums[0] === nums[right]) right--;
  // 查找旋转点
  while (left < right) {
    let mid = (right + left + 1) >> 1;
    if (nums[0] <= nums[mid]) {
      left = mid;
    } else {
      right = mid - 1;
    }
  }
  let idx = size - 1;
  //[0,idx]和[idx+1,size-1]分成2个有序区间
  if (nums[right] >= nums[0] && right < idx) idx = right;

  // 左区间
  let result = binarySearch(nums, 0, idx, target);
  if (result !== -1) return true;
  // 右区间
  result = binarySearch(nums, idx + 1, size - 1, target);
  return result !== -1;
};
```
