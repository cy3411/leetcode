# 题目

给你一个数组 target 和一个整数 n。每次迭代，需要从 list = { 1 , 2 , 3 ..., n } 中依次读取一个数字。

请使用下述操作来构建目标数组 target ：

- "Push"：从 list 中读取一个新元素， 并将其推入数组中。
- "Pop"：删除数组中的最后一个元素。
- 如果目标数组构建完成，就停止读取更多元素。

题目数据保证目标数组严格递增，并且只包含 1 到 n 之间的数字。

请返回构建目标数组所用的操作序列。如果存在多个可行方案，返回任一即可。

提示：

- $1 \leq target.length \leq 100$
- $1 \leq n \leq 100$
- $1 \leq target[i] \leq n$
- `target` 严格递增

# 示例

```
输入：target = [1,3], n = 3
输出：["Push","Push","Pop","Push"]
解释：
读取 1 并自动推入数组 -> [1]
读取 2 并自动推入数组，然后删除它 -> [1]
读取 3 并自动推入数组 -> [1,3]
```

```
输入：target = [1,2,3], n = 3
输出：["Push","Push","Push"]
```

# 题解

## 模拟

操作 [1,n] 的数字，按序选择数字，如果当前数字在 target 中，则将它 push 入栈，否则，将其 push 入栈，紧接着马上 pop 出来。

```js
function buildArray(target: number[], n: number): string[] {
  let i = 0;
  let j = 1;
  const ans: string[] = [];
  while (j <= n) {
    if (target[i] === j) {
      ans.push('Push');
      i++;
      // 如果当前长度和target相同，表示已经完成，后续的数序不需要再遍历了。
      if (i === target.length) break;
    } else {
      ans.push('Push');
      ans.push('Pop');
    }
    j++;
  }
  return ans;
}
```

```cpp
class Solution {
public:
    vector<string> buildArray(vector<int> &target, int n) {
        vector<string> ans;
        int prev = 0;
        for (int number : target) {
            // 在target两个数字之间插入 push pop
            for (int i = 0; i < number - prev - 1; i++) {
                ans.emplace_back("Push");
                ans.emplace_back("Pop");
            }
            ans.emplace_back("Push");
            prev = number;
        }
        return ans;
    }
};
```

```py
class Solution:
    def buildArray(self, target: List[int], n: int) -> List[str]:
        ans = []
        i = 0
        j = 1
        while j <= n:
            if target[i] == j:
                ans.append('Push')
                i += 1
                if i == len(target):
                    break
            else:
                ans.append('Push')
                ans.append('Pop')
            j += 1
        return ans
```
