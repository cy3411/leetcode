#
# @lc app=leetcode.cn id=1441 lang=python3
#
# [1441] 用栈操作构建数组
#

# @lc code=start
class Solution:
    def buildArray(self, target: List[int], n: int) -> List[str]:
        ans = []
        i = 0
        j = 1
        while j <= n:
            if target[i] == j:
                ans.append('Push')
                i += 1
                if i == len(target):
                    break
            else:
                ans.append('Push')
                ans.append('Pop')
            j += 1
        return ans
# @lc code=end
