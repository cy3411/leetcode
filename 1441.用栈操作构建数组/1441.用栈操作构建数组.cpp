/*
 * @lc app=leetcode.cn id=1441 lang=cpp
 *
 * [1441] 用栈操作构建数组
 */

// @lc code=start
class Solution {
public:
    vector<string> buildArray(vector<int> &target, int n) {
        vector<string> ans;
        int prev = 0;
        for (int number : target) {
            // 在target两个数字之间插入 push pop
            for (int i = 0; i < number - prev - 1; i++) {
                ans.emplace_back("Push");
                ans.emplace_back("Pop");
            }
            ans.emplace_back("Push");
            prev = number;
        }
        return ans;
    }
};
// @lc code=end
