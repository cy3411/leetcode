/*
 * @lc app=leetcode.cn id=1441 lang=typescript
 *
 * [1441] 用栈操作构建数组
 */

// @lc code=start
function buildArray(target: number[], n: number): string[] {
  let i = 0;
  let j = 1;
  const ans: string[] = [];
  while (j <= n) {
    if (target[i] === j) {
      ans.push('Push');
      i++;
      if (i === target.length) break;
    } else {
      ans.push('Push');
      ans.push('Pop');
    }
    j++;
  }
  return ans;
}
// @lc code=end
