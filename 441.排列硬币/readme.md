# 题目

你总共有 n 枚硬币，并计划将它们按阶梯状排列。对于一个由 k 行组成的阶梯，其第 i 行必须正好有 i 枚硬币。阶梯的最后一行 可能 是不完整的。

给你一个数字 n ，计算并返回可形成 **完整阶梯行** 的总行数。

提示：

- $1 <= n <= 2^{31} - 1$

# 示例

[![5EWeV1.png](https://z3.ax1x.com/2021/10/10/5EWeV1.png)](https://imgtu.com/i/5EWeV1)

```
输入：n = 5
输出：2
解释：因为第三行不完整，所以返回 2 。
```

# 题解

## 二分查找

根据题目可知，第 `i` 行正好有 `i` 枚硬币，根据等差数列求和公式$\frac{i*(i+1)}{2}$可知第 `i` 行共有 `x` 枚硬币。

我们就是寻找一个 `i` 使得 `x` 小于等于 `n` ，并且这个 `x` 值在 `1` 到 `n` 之间。

```ts
function arrangeCoins(n: number): number {
  // 计算n行需要多少数字
  const calc = (n: number): number => {
    return ((1 + n) * n) / 2;
  };

  let left = 1;
  let right = n;
  while (left < right) {
    let mid = left + Math.floor((right - left + 1) / 2);
    if (calc(mid) <= n) {
      left = mid;
    } else {
      right = mid - 1;
    }
  }

  return left;
}
```
