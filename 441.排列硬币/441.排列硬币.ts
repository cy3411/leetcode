/*
 * @lc app=leetcode.cn id=441 lang=typescript
 *
 * [441] 排列硬币
 */

// @lc code=start
function arrangeCoins(n: number): number {
  // 计算n行需要多少数字
  const calc = (n: number): number => {
    return ((1 + n) * n) / 2;
  };

  let left = 1;
  let right = n;
  while (left < right) {
    let mid = left + Math.floor((right - left + 1) / 2);
    if (calc(mid) <= n) {
      left = mid;
    } else {
      right = mid - 1;
    }
  }

  return left;
}
// @lc code=end
