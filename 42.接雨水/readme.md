# 题目

给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。

提示：

- n == height.length
- 0 <= n <= 3 \* 104
- 0 <= height[i] <= 105

# 示例

[![WRATb9.png](https://z3.ax1x.com/2021/07/25/WRATb9.png)](https://imgtu.com/i/WRATb9)

```
输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
输出：6
解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。
```

# 题解

## 单调栈

利用单调递减栈的特性，每当当前元素不满足单调性的时候：

比如入栈 3，2，1，当前元素是 4 的时候，2，1，4 就会形成一个接雨水的凹槽。

这个时候，在弹栈的过程中，就可以计算每个凹槽接雨水的数量。

```ts
function trap(height: number[]): number {
  const stack = [];
  let ans = 0;
  for (let i = 0; i < height.length; i++) {
    while (stack.length && height[i] > height[stack[stack.length - 1]]) {
      const bottom = stack.pop();
      // 栈空了，不能形成凹槽，没法接雨水
      if (stack.length === 0) break;
      let l = height[stack[stack.length - 1]] - height[bottom];
      let r = height[i] - height[bottom];
      // 跨度*高度差，每层接的雨水量
      ans += (i - stack[stack.length - 1] - 1) * Math.min(l, r);
    }
    stack.push(i);
  }
  return ans;
}
```
