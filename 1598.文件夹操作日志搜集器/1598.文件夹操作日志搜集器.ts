/*
 * @lc app=leetcode.cn id=1598 lang=typescript
 *
 * [1598] 文件夹操作日志搜集器
 */

// @lc code=start
function minOperations(logs: string[]): number {
  let ans = 0;
  for (const log of logs) {
    // 当前目录不变
    if (log === './') continue;
    if (log === '../') {
      // 已经在根目录
      if (ans === 0) continue;
      ans--;
    } else ans++;
  }
  return ans;
}
// @lc code=end
