# 题目

每当用户执行变更文件夹操作时，LeetCode 文件系统都会保存一条日志记录。

下面给出对变更操作的说明：

- `"../"` ：移动到当前文件夹的父文件夹。如果已经在主文件夹下，则 继续停留在当前文件夹 。
- `"./"` ：继续停留在当前文件夹。
- `"x/"` ：移动到名为 `x` 的子文件夹中。题目数据 保证总是存在文件夹 `x` 。

给你一个字符串列表 `logs` ，其中 `logs[i]` 是用户在 $i^{th}$ 步执行的操作。

文件系统启动时位于主文件夹，然后执行 `logs` 中的操作。

执行完所有变更文件夹操作后，请你找出 **返回主文件夹所需的最小步数** 。

提示：

- $1 <= logs.length <= 103$
- $2 <= logs[i].length <= 10$
- `logs[i]` 包含小写英文字母，数字，`'.'` 和 `'/'`
- `logs[i]` 符合语句中描述的格式
- 文件夹名称由小写英文字母和数字组成

# 示例

[![vLDpcQ.png](https://s1.ax1x.com/2022/09/09/vLDpcQ.png)](https://imgse.com/i/vLDpcQ)

```
输入：logs = ["d1/","d2/","../","d21/","./"]
输出：2
解释：执行 "../" 操作变更文件夹 2 次，即可回到主文件夹
```

[![vLD9Xj.png](https://s1.ax1x.com/2022/09/09/vLD9Xj.png)](https://imgse.com/i/vLD9Xj)

```
输入：logs = ["d1/","d2/","./","d3/","../","d31/"]
输出：3
```

# 题解

## 遍历

遍历 logs ，对不同变更做如下处理：

- './'，层数不变
- '../'，层数减 1。层数为 0 的话，表示已经在主文件夹，需要保持不变。
- 'x/'，层数加 1

最后的层数，就是答案。

```js
function minOperations(logs: string[]): number {
  let ans = 0;
  for (const log of logs) {
    // 当前目录不变
    if (log === './') continue;
    if (log === '../') {
      // 已经在根目录
      if (ans === 0) continue;
      ans--;
    } else ans++;
  }
  return ans;
}
```

```cpp
class Solution {
public:
    int minOperations(vector<string> &logs) {
        int ans = 0;
        for (auto log : logs) {
            if (log == "./") continue;
            if (log == "../") {
                if (ans == 0) continue;
                ans--;
            } else {
                ans++;
            }
        }
        return ans;
    }
};
```

```py
class Solution:
    def minOperations(self, logs: List[str]) -> int:
        ans = 0
        for log in logs:
            if log == './':
                continue
            if log != '../':
                ans += 1
            elif ans:
                ans -= 1
        return ans
```
