# 题目

给你一个以字符串表示的非负整数 num 和一个整数 k ，移除这个数中的 k 位数字，使得剩下的数字最小。请你以字符串形式返回这个最小的数字。

提示：

- 1 <= k <= num.length <= 105
- num 仅由若干位数字（0 - 9）组成
- 除了 0 本身之外，num 不含任何前导零

# 示例

```
输入：num = "1432219", k = 3
输出："1219"
解释：移除掉三个数字 4, 3, 和 2 形成一个新的最小的数字 1219
```

```
输入：num = "10", k = 2
输出："0"
解释：从原数字移除所有的数字，剩余为空就是 0 。
```

# 题解

## 单调栈

要返回较小的数，肯定是先从高位把较大的数移除。

我们可以利用单调递增栈的特性，在 k>0 的情况下，将高位的大数移除，最后栈中剩下的就是结果。

需要注意的就是，如果题目给出的 num 本身就是递增的，那么遍历完毕，k 的次数肯定大于 1。我们可以这个时候将栈顶的最大数移除 k 个即可。

```ts
function removeKdigits(num: string, k: number): string {
  if (k >= num.length) return '0';
  // 单调递增栈
  const stack = [];
  for (const c of num) {
    // 找到高位比较大的数组，且k要大于0
    while (stack.length && k > 0 && c < stack[stack.length - 1]) {
      stack.pop();
      k--;
    }
    stack.push(c);
  }
  // 如果k大于0，表示num中有小于k个元素满足不满足单调递增性
  // 栈中剩下的元素都是递增的，所以只有从后面把最大的弹出就可以了。
  while (k-- > 0) {
    stack.pop();
  }

  let ans = '';
  for (const c of stack) {
    if (c === '0' && ans === '') continue;
    ans += c;
  }

  return ans === '' ? '0' : ans;
}
```
