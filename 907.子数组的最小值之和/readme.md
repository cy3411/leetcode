# 题目

给定一个整数数组 arr，找到 min(b) 的总和，其中 b 的范围为 arr 的每个（连续）子数组。

由于答案可能很大，因此 返回答案模 10^9 + 7 。

提示：

- 1 <= arr.length <= 3 \* 104
- 1 <= arr[i] <= 3 \* 104

# 示例

```
输入：arr = [3,1,2,4]
输出：17
解释：
子数组为 [3]，[1]，[2]，[4]，[3,1]，[1,2]，[2,4]，[3,1,2]，[1,2,4]，[3,1,2,4]。
最小值为 3，1，2，4，1，1，2，1，1，1，和为 17。
```

# 题解

## 单调栈

每一个元素能贡献的次数是上一个比当前元素小的元素贡献的次数加上当前元素最小区间跨度乘以当前元素。

比如：arr = [3,1,2,4]
3 前面没有比他小的，3 最小的区间是 1，贡献 3*1 次。
1 前面没有比他小的，1 最小的区间是 2，贡献了 1*2 次。
2 前面有 1，2 最小的区间是 1，贡献 2*1，在加上 1 贡献的次数 2
4 前面有 2，4 最小的区间是 1，贡献 4*1，再加上 2 贡献的次数 4

我们可以利用单调递增栈的特性，来找到上一个比当前元素小的位置，来计算每个元素可以贡献的次数。

```ts
function sumSubarrayMins(arr: number[]): number {
  const m = arr.length;
  const sum = new Array(m + 1).fill(0);
  const stack: number[] = [];
  let ans = 0;
  for (let i = 0; i < m; i++) {
    while (stack.length && arr[i] <= arr[stack[stack.length - 1]]) {
      stack.pop();
    }
    // 比当前元素小的上一个位置
    let pre = stack.length ? stack[stack.length - 1] : -1;
    stack.push(i);
    // 当前元素贡献的次数=比当前元素小的上一个元素贡献的次数+当前元素最小的区间跨度*当前元素
    sum[stack.length] = sum[stack.length - 1] + arr[i] * (i - pre);
    // 累加结果
    ans += sum[stack.length];
    ans %= 1e9 + 7;
  }

  return ans;
}
```
