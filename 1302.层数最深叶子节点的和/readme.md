# 题目

给你一棵二叉树的根节点 `root` ，请你返回 **层数最深的叶子节点的和** 。

提示：

- 树中节点数目在范围 $[1, 10^4]$ 之间。
- $1 \leq Node.val \leq 100$

# 示例

```
输入：root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
输出：15
```

# 题解

## 前序遍历

遍历二叉树，同时记录当前的深度。使用何种遍历方式都可以，这里采用的是前序遍历。

如果深度大于最大深度，则更新最大深度，并且记录当前结点的值。

如果深度等于最大深度，则累加当前层的值。

最后返回累加和即可。

```ts
function deepestLeavesSum(root: TreeNode | null): number {
  let maxDepth = 0;
  let ans = 0;
  const dfs = (node: TreeNode | null, depth: number): number => {
    if (node === null) return;
    if (depth === maxDepth) {
      ans += node.val;
    } else if (depth > maxDepth) {
      maxDepth = depth;
      ans = node.val;
    }
    dfs(node.left, depth + 1);
    dfs(node.right, depth + 1);
  };

  dfs(root, 0);
  return ans;
}
```

```cpp
class Solution
{
public:
    void dfs(TreeNode *root, int depth, int &maxDepth, int &ans)
    {
        if (root == NULL)
            return;
        if (depth == maxDepth)
        {
            ans += root->val;
        }
        else if (depth > maxDepth)
        {
            maxDepth = depth;
            ans = root->val;
        }
        dfs(root->left, depth + 1, maxDepth, ans);
        dfs(root->right, depth + 1, maxDepth, ans);
    }
    int deepestLeavesSum(TreeNode *root)
    {
        int maxDepth = 0, ans = 0;
        dfs(root, 0, maxDepth, ans);
        return ans;
    }
};
```

## 广度优先搜索

逐层遍历，计算每层的节点和。最后返回答案即可。

```py
class Solution:
    def deepestLeavesSum(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0
        queue = [root]
        ans: int
        while queue:
            size = len(queue)
            ans = 0
            for _ in range(size):
                node = queue.pop(0)
                ans += node.val
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)

        return ans
```
