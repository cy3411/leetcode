/*
 * @lc app=leetcode.cn id=881 lang=typescript
 *
 * [881] 救生艇
 */

// @lc code=start
function numRescueBoats(people: number[], limit: number): number {
  people.sort((a, b) => a - b);

  let ans = 0;
  let i = 0;
  let j = people.length - 1;

  while (i <= j) {
    // 船上最多2人，最重和最轻如何可以一起，那么同时将2人放在一条船
    // 否则将最重的放在一条船
    if (people[i] + people[j] <= limit) {
      i++;
    }
    j--;
    ans++;
  }

  return ans;
}
// @lc code=end
