// @algorithm @lc id=1000292 lang=typescript
// @title qIsx9U

class MovingAverage {
  data: number[] = [];
  size: number = 0;
  constructor(size: number) {
    this.size = size;
  }

  next(val: number): number {
    this.data.push(val);
    if (this.data.length > this.size) {
      this.data.shift();
    }
    return this.data.reduce((a, b) => a + b, 0) / this.data.length;
  }
}

/**
 * Your MovingAverage object will be instantiated and called as such:
 * var obj = new MovingAverage(size)
 * var param_1 = obj.next(val)
 */
