class MovingAverage:

    def __init__(self, size: int):
        """
        Initialize your data structure here.
        """
        self.size = size
        self.sum = 0
        self.que = deque()

    def next(self, val: int) -> float:
        if (len(self.que) == self.size):
            self.sum -= self.que.popleft()
        self.que.append(val)
        self.sum += val
        return self.sum / len(self.que)
