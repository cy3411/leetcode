# 题目

给定一个整数数据流和一个窗口大小，根据该滑动窗口的大小，计算滑动窗口里所有数字的平均值。

实现 `MovingAverage` 类：

- `MovingAverage(int size)` 用窗口大小 `size` 初始化对象。
- `double next(int val)` 成员函数 `next` 每次调用的时候都会往滑动窗口增加一个整数，请计算并返回数据流中最后 `size` 个值的移动平均值，即滑动窗口里所有数字的平均值。

提示：

- $1 \leq size \leq 1000$
- $-10^5 \leq val \leq 10^5$
- 最多调用 `next` 方法 $10^4$ 次

# 示例

```
输入：
inputs = ["MovingAverage", "next", "next", "next", "next"]
inputs = [[3], [1], [10], [3], [5]]
输出：
[null, 1.0, 5.5, 4.66667, 6.0]

解释：
MovingAverage movingAverage = new MovingAverage(3);
movingAverage.next(1); // 返回 1.0 = 1 / 1
movingAverage.next(10); // 返回 5.5 = (1 + 10) / 2
movingAverage.next(3); // 返回 4.66667 = (1 + 10 + 3) / 3
movingAverage.next(5); // 返回 6.0 = (10 + 3 + 5) / 3
```

# 题解

## 队列

使用队列维护窗口中的数据，每次插入新数据时，如果队列数据长度超过 size，将队列头部的数据移除，并将新数据插入队列尾部。

最后返回队列中的平均值。

```ts
class MovingAverage {
  data: number[] = [];
  size: number = 0;
  constructor(size: number) {
    this.size = size;
  }

  next(val: number): number {
    this.data.push(val);
    if (this.data.length > this.size) {
      this.data.shift();
    }
    return this.data.reduce((a, b) => a + b, 0) / this.data.length;
  }
}
```

```cpp
class MovingAverage {
private:
    int size;
    double sum;
    queue<int> que;

public:
    /** Initialize your data structure here. */
    MovingAverage(int size) {
        this->size = size;
        this->sum = 0;
    }

    double next(int val) {
        if (que.size() == this->size) {
            this->sum -= que.front();
            que.pop();
        }
        que.emplace(val);
        this->sum += val;
        return this->sum / que.size();
    }
};
```
