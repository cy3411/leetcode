/*
 * @lc app=leetcode.cn id=89 lang=typescript
 *
 * [89] 格雷编码
 */

// @lc code=start
function grayCode(n: number): number[] {
  const ans = [0];

  for (let i = 1; i <= n; i++) {
    const n = ans.length;
    // 对称构造
    for (let j = n - 1; j >= 0; j--) {
      ans.push(ans[j] + Math.pow(2, i - 1));
    }
  }

  return ans;
}
// @lc code=end
