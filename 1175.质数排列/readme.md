# 题目

请你帮忙给从 `1` 到 `n` 的数设计排列方案，使得所有的「质数」都应该被放在「质数索引」（索引从 1 开始）上；你需要返回可能的方案总数。

让我们一起来回顾一下「质数」：质数一定是大于 1 的，并且不能用两个小于它的正整数的乘积来表示。

由于答案可能会很大，所以请你返回答案 **模 mod** $10^9 + 7$ 之后的结果即可。

提示：

- $1 \leq n \leq 100$

# 示例

```
输入：n = 5
输出：12
解释：举个例子，[1,2,5,4,3] 是一个有效的排列，但 [5,2,3,4,1] 不是，因为在第二种情况里质数 5 被错误地放在索引为 1 的位置上。
```

```
输入：n = 100
输出：682289015
```

# 题解

## 质数判断+组合

所有的方案数为质数的组合数乘以合数的组合数。

根据题意求得质数的数量，然后求组合数的数量。

根据质数的数量得到合数的数量，然后求取合数的组合数。

```ts
function numPrimeArrangements(n: number): number {
  const mod = 1e9 + 7;
  // 试除法求质数个数
  const isPrime = (n: number): boolean => {
    if (n <= 1) return false;
    for (let i = 2; i * i <= n; i++) {
      if (n % i === 0) return false;
    }
    return true;
  };

  let primes = 0;
  for (let i = 2; i <= n; i++) {
    if (isPrime(i)) primes++;
  }

  let ans = 1;
  let nPrimes = n - primes;
  // 素数的排列方案数
  while (primes > 0) {
    ans %= mod;
    ans *= primes--;
  }
  // 非素数的排列方案数
  while (nPrimes > 0) {
    ans %= mod;
    ans *= nPrimes--;
  }
  return ans;
}
```

```cpp
class Solution {
public:
    int mod = 1e9 + 7;
    int isPrime(int n) {
        if (n <= 1) return 0;
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) return 0;
        }
        return 1;
    }
    long factorial(int n) {
        long res = 1;
        while (n) {
            res = res % mod;
            res = res * n;
            n--;
        }
        return res;
    }
    int numPrimeArrangements(int n) {
        int primes = 0, ans = 1;
        for (int i = 1; i <= n; i++) {
            if (isPrime(i)) primes++;
        }
        int nPrimes = n - primes;

        ans = (int)(factorial(primes) * factorial(nPrimes) % mod);

        return ans;
    }
};
```
