/*
 * @lc app=leetcode.cn id=1877 lang=typescript
 *
 * [1877] 数组中最大数对和的最小值
 */

// @lc code=start
function minPairSum(nums: number[]): number {
  const m = nums.length;
  let ans = 0;
  nums.sort((a, b) => a - b);
  for (let i = 0; i < m / 2; i++) {
    // 将元素中第i小和第i大的相加
    // 这样可以得到对数和中的最小值
    ans = Math.max(ans, nums[i] + nums[m - i - 1]);
  }

  return ans;
}
// @lc code=end
