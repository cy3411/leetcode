# 题目

一个整数区间 `[a, b]` ( `a < b` ) 代表着从 `a` 到 `b` 的所有连续整数，包括 `a` 和 `b`。

给你一组整数区间 `intervals`，请找到一个最小的集合 `S`，使得 `S` 里的元素与区间 `intervals` 中的每一个整数区间都至少有 `2` 个元素相交。

输出这个最小集合 `S` 的大小。

注意:

- `intervals` 的长度范围为`[1, 3000]`。
- `intervals[i]` 长度为 `2`，分别代表左、右边界。
- `intervals[i][j]` 的值是 `[0, 10^8]`范围内的整数。

# 示例

```
输入: intervals = [[1, 3], [1, 4], [2, 5], [3, 5]]
输出: 3
解释:
考虑集合 S = {2, 3, 4}. S与intervals中的四个区间都有至少2个相交的元素。
且这是S最小的情况，故我们输出3。
```

```
输入: intervals = [[1, 2], [2, 3], [2, 4], [4, 5]]
输出: 5
解释:
最小的集合S = {1, 2, 3, 4, 5}.
```

# 题解

## 贪心

对 `intervals` 排序，按照 `intervals[0]` 升序， `intervals[1]` 降序，然后逆序遍历数组。

定义 `l = intervals[n-1][0]` 和 ` r = intervals[n-1][0] + 1` 作为开始的两个集合元素。

分类处理前一个区间 [lt, rt] 的状态：

- 如果 $rt >= r$ ，表示两个区间覆盖了，一定满足有两个交集的情况
- 如果 $rt >= l and rt < r$ ,表示两个区间有一个交集，取 `l = rl, r = l + 1`
- 如果 $rt < l ，表示两个区间没有交集，取 `r = l, l = lt`

根据状态，增加 ans 的值。最后返回 ans。

```ts
function intersectionSizeTwo(intervals: number[][]): number {
  intervals.sort((a, b) => {
    if (a[0] === b[0]) {
      return b[1] - a[1];
    }
    return a[0] - b[0];
  });

  const n = intervals.length;
  let ans = 2;
  let l = intervals[n - 1][0];
  let r = l + 1;
  for (let i = n - 2; i >= 0; i--) {
    const [lt, rt] = intervals[i];
    if (rt >= l && rt < r) {
      ans++;
      r = l;
      l = lt;
    } else if (rt < l) {
      ans += 2;
      l = lt;
      r = l + 1;
    }
  }

  return ans;
}
```
