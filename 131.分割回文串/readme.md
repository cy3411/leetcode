## 描述
给定一个字符串 s，将 s 分割成一些子串，使每个子串都是回文串。
返回 s 所有可能的分割方案。

## 示例
```
输入: "aab"
输出:
[
  ["aa","b"],
  ["a","a","b"]
]
```

## 方法
由于需要求出字符串 s 的所有分割方案，因此我们考虑使用搜索 + 回溯的方法枚举所有可能的分割方法并进行判断
```javascript
var partition = function (s) {
  /**
   * @description: 回溯
   * @param {string*} str
   * @param {number} idx 递归用的索引
   * @param {[[string]]} res
   * @param {[string]} store 存储分割的字符串
   * @return {void}
   */
  const backtrack = (str, idx, res, store) => {
    if (idx === str.length) {
      res.push([...store]);
      return;
    }

    for (let i = idx; i < str.length; i++) {
      const substr = str.substring(idx, i + 1);
      if (!isPlalindrome(substr)) continue;
      store.push(substr);
      backtrack(str, i + 1, res, store);
      store.pop();
    }
  };
  /**
   * @description: 判断是否回文
   * @param {string} str
   * @return {bollean}
   */
  const isPlalindrome = (str) => {
    let i = 0;
    let j = str.length - 1;
    while (i < j) {
      if (str[i++] !== str[j--]) {
        return false;
      }
    }
    return true;
  };

  const result = [];
  backtrack(s, 0, result, []);

  return result;
};
```

加入递归调用isPlalindrome判断会有重复情况，所以可以加入记忆化减枝
```javascript
var partition = function (s) {
  /**
   * @description: 回溯
   * @param {string*} str
   * @param {number} idx 递归用的索引
   * @param {[[string]]} res
   * @param {[string]} store 存储分割的字符串
   * @return {void}
   */
  const backtrack = (str, idx, res, store) => {
    if (idx === str.length) {
      res.push([...store]);
      return;
    }

    for (let i = idx; i < str.length; i++) {
      const substr = str.substring(idx, i + 1);
      if (memo.get(substr) === false) continue;
      if (memo.get(substr) === true || isPlalindrome(substr, memo)) {
        store.push(substr);
        backtrack(str, i + 1, res, store, memo);
        store.pop();
      }
    }
  };
  /**
   * @description: 判断是否回文
   * @param {string} str
   * @return {bollean}
   */
  const isPlalindrome = (str, memo) => {
    let i = 0;
    let j = str.length - 1;
    while (i < j) {
      if (str[i++] !== str[j--]) {
        memo.set(str, false);
        return false;
      }
    }
    memo.set(str, true);
    return true;
  };

  const result = [];
  const memo = new Map();
  backtrack(s, 0, result, [], memo);

  return result;
};
```
