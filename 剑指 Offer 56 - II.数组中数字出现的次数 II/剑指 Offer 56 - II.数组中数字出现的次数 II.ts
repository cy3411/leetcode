// @algorithm @lc id=100321 lang=typescript
// @title shu-zu-zhong-shu-zi-chu-xian-de-ci-shu-ii-lcof
function singleNumber(nums: number[]): number {
  // 记录每个数字二进制位上的 1 的个数
  const bits = new Array(32).fill(0);
  for (let num of nums) {
    let idx = 0;
    while (num !== 0) {
      bits[idx] += num & 1;
      num = num >>> 1;
      idx++;
    }
  }
  // 计算答案
  let ans = 0;
  // 将对应位置的结果取模3，如果不是0，表示这个位置是答案的某个二进制位
  // 我们将结果累加到答案中
  for (let i = 0; i < 32; i++) {
    bits[i] %= 3;
    if (bits[i] !== 0) {
      ans += 1 << i;
    }
  }

  return ans;
}
