# 题目

在一个数组 `nums` 中除一个数字只出现一次之外，其他数字都出现了三次。请找出那个只出现一次的数字

限制：

- $1 \leq nums.length \leq 10000$
- $1 \leq nums[i] < 2^31$

# 示例

```
输入：nums = [3,4,3,3]
输出：4
```

```
输入：nums = [9,1,7,9,7,9,7]
输出：1
```

# 题解

## 位运算

统计每个数字二进制位 `1` 出现的次数，最后将统计结果中的 `1` 的数量取模 `3`，得到的二进制就是结果的二进制。

因为多余的数字都是出现 `3` 次，那么它们二进制位的 `1` 的数量都是 `3` 的倍数，所以可以直接取模。

```ts
function singleNumber(nums: number[]): number {
  // 记录每个数字二进制位上的 1 的个数
  const bits = new Array(32).fill(0);
  for (let num of nums) {
    let idx = 0;
    while (num !== 0) {
      bits[idx] += num & 1;
      num = num >>> 1;
      idx++;
    }
  }
  // 计算答案
  let ans = 0;
  // 将对应位置的结果取模3，如果不是0，表示这个位置是答案的某个二进制位
  // 我们将结果累加到答案中
  for (let i = 0; i < 32; i++) {
    bits[i] %= 3;
    if (bits[i] !== 0) {
      ans += 1 << i;
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int singleNumber(vector<int>& nums) {
      int bits[32] = {0};
      int i;
      for (auto num : nums) {
        i = 0;
        while (num != 0) {
          bits[i] += num & 1;
          num >>= 1;
          i++;
        }
      }
      int ans = 0;
      for (i = 0; i < 32; i++) {
        bits[i] %= 3;
        if (bits[i] != 0) {
          ans += 1 << i;
        }
      }
      return ans;
    }
};
```

```cpp
int singleNumber(int* nums, int numsSize){
  int bits[32] = {0};
  int i, j;
  for (i = 0; i < numsSize; i++) {
    int num = nums[i];
    j = 0;
    while (num != 0) {
      bits[j] += (num & 1);
      num >>= 1;
      j++;
    }
  }
  int ans = 0;
  for(i = 0; i < 32; i++) {
    int bit = bits[i] % 3;
    if (bit != 0) {
      ans += 1 << i;
    }
  }
  return ans;
}
```
