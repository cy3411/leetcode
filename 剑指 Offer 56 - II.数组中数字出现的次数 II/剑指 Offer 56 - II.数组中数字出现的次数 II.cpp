class Solution
{
public:
    int singleNumber(vector<int> &nums)
    {
        int bits[32] = {0};
        int i;
        for (auto num : nums)
        {
            i = 0;
            while (num != 0)
            {
                bits[i] += num & 1;
                num >>= 1;
                i++;
            }
        }
        int ans = 0;
        for (i = 0; i < 32; i++)
        {
            bits[i] %= 3;
            if (bits[i] != 0)
            {
                ans += 1 << i;
            }
        }
        return ans;
    }
};