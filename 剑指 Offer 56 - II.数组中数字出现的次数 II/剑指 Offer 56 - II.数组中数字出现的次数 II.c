int singleNumber(int *nums, int numsSize)
{
    int bits[32] = {0};
    int i, j;
    for (i = 0; i < numsSize; i++)
    {
        int num = nums[i];
        j = 0;
        while (num != 0)
        {
            bits[j] += (num & 1);
            num >>= 1;
            j++;
        }
    }
    int ans = 0;
    for (i = 0; i < 32; i++)
    {
        int bit = bits[i] % 3;
        if (bit != 0)
        {
            ans += 1 << i;
        }
    }
    return ans;
}