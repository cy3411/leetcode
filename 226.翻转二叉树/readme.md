# 题目
翻转一棵二叉树。

# 示例
```
输入：
     4
   /   \
  2     7
 / \   / \
1   3 6   9

输出：
     4
   /   \
  7     2
 / \   / \
9   6 3   1
```

# 方法
翻转当前节点的左右树，然后递归去翻转当前节点的左右子树
```js
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function (root) {
  if (root === null) return root;
  // 左右翻转
  [root.left, root.right] = [root.right, root.left];
  // 翻转左树
  invertTree(root.left);
  // 翻转右树
  invertTree(root.right);
  return root;
};
```