/*
 * @lc app=leetcode.cn id=226 lang=javascript
 *
 * [226] 翻转二叉树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function (root) {
  if (root === null) return root;
  // 左右翻转
  [root.left, root.right] = [root.right, root.left];
  // 翻转左树
  invertTree(root.left);
  // 翻转右树
  invertTree(root.right);
  return root;
};
// @lc code=end
