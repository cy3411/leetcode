/*
 * @lc app=leetcode.cn id=462 lang=typescript
 *
 * [462] 最少移动次数使数组元素相等 II
 */

// @lc code=start
function minMoves2(nums: number[]): number {
  nums.sort((a, b) => a - b);
  const n = nums.length;
  // 找到中位数
  const mid = nums[(n / 2) >> 0];

  let ans = 0;
  for (let num of nums) {
    // 和中位数之间的差值就是最小移动次数
    ans += Math.abs(num - mid);
  }

  return ans;
}
// @lc code=end
