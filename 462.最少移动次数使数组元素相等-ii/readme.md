# 题目

给定一个非空整数数组，找到使所有数组元素相等所需的最小移动数，其中每次移动可将选定的一个元素加 1 或减 1。 您可以假设数组的长度最多为 10000。

# 示例

```
输入:
[1,2,3]

输出:
2

说明：
只有两个动作是必要的（记得每一步仅可使其中一个元素加1或减1）：

[1,2,3]  =>  [2,2,3]  =>  [2,2,2]
```

# 题解

## 排序寻找中位数

题目的意思就是在 N 个数中，找到一个数 x,使得这 N 个数字与 x 之间的差值之和最小。

只有当 x 是中位数的时候，可以使得差值最小。

对数组排序，找到中位数，遍历数组，累加当前元素和中位数的差的绝对值即可。

```ts
function minMoves2(nums: number[]): number {
  nums.sort((a, b) => a - b);
  const n = nums.length;
  // 找到中位数
  const mid = nums[(n / 2) >> 0];

  let ans = 0;
  for (let num of nums) {
    // 和中位数之间的差值就是最小移动次数
    ans += Math.abs(num - mid);
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int minMoves2(vector<int> &nums) {
        sort(nums.begin(), nums.end());
        int mid = nums[nums.size() / 2];
        int ans = 0;
        for (auto x : nums) {
            ans += abs(x - mid);
        }
        return ans;
    }
};
```

```py
class Solution:
    def minMoves2(self, nums: List[int]) -> int:
        nums.sort()
        n = len(nums)
        mid = nums[n // 2]
        ans = 0
        for i in range(n):
            ans += abs(nums[i] - mid)
        return ans
```
