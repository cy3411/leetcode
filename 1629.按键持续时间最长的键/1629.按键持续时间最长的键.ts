/*
 * @lc app=leetcode.cn id=1629 lang=typescript
 *
 * [1629] 按键持续时间最长的键
 */

// @lc code=start
function slowestKey(releaseTimes: number[], keysPressed: string): string {
  let max = 0;
  let ans = '';

  for (let i = 0; i < keysPressed.length; i++) {
    if (i === 0) {
      max = releaseTimes[i];
      ans = keysPressed[i];
      continue;
    }
    const diff = releaseTimes[i] - releaseTimes[i - 1];
    // 持续时间最长的键
    if (diff > max) {
      max = diff;
      ans = keysPressed[i];
    }
    // 持续时间相同的键
    if (diff === max) {
      ans = ans < keysPressed[i] ? keysPressed[i] : ans;
    }
  }

  return ans;
}
// @lc code=end
