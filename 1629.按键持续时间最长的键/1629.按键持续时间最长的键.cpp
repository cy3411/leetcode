/*
 * @lc app=leetcode.cn id=1629 lang=cpp
 *
 * [1629] 按键持续时间最长的键
 */

// @lc code=start
class Solution
{
public:
    char slowestKey(vector<int> &releaseTimes, string keysPressed)
    {
        int max = releaseTimes[0];
        char ans = keysPressed[0];

        for (int i = 1; i < keysPressed.size(); i++)
        {
            int diff = releaseTimes[i] - releaseTimes[i - 1];
            if (diff > max)
            {
                max = diff;
                ans = keysPressed[i];
            }
            else if (diff == max)
            {
                ans = ans < keysPressed[i] ? keysPressed[i] : ans;
            }
        }

        return ans;
    }
};
// @lc code=end
