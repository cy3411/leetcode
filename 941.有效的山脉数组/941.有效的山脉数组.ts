/*
 * @lc app=leetcode.cn id=941 lang=typescript
 *
 * [941] 有效的山脉数组
 */

// @lc code=start
function validMountainArray(arr: number[]): boolean {
  let i = 0;
  let j = arr.length - 1;
  // 从左往右找到第一个非递增的数
  while (i < arr.length && arr[i] < arr[i + 1]) {
    i++;
  }
  // 从右往左找到第一个非递增的数
  while (j > 0 && arr[j] < arr[j - 1]) {
    j--;
  }

  // 如果是峰顶，则i和j相等
  return i === j && i > 0 && j < arr.length - 1;
}
// @lc code=end
