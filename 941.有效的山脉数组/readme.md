# 题目

给定一个整数数组 `arr`，如果它是有效的山脉数组就返回 `true`，否则返回 `false`。

让我们回顾一下，如果 A 满足下述条件，那么它是一个山脉数组：

- `arr.length >= 3`
- 在 `0 < i < arr.length - 1` 条件下，存在 `i` 使得：
  - `arr[0] < arr[1] < ... arr[i-1] < arr[i]`
  - `arr[i] > arr[i+1] > ... > arr[arr.length - 1]`

[![oXqnY9.md.png](https://s4.ax1x.com/2021/12/13/oXqnY9.md.png)](https://imgtu.com/i/oXqnY9)

提示：

- $1 \leq arr.length \leq 10^4$
- $0 \leq arr[i] \leq 10^4$

# 示例

```
输入：arr = [2,1]
输出：false
```

```
输入：arr = [3,5,5]
输出：false
```

# 题解

## 双指针

定义 i，往后找到第一个非递增的数。

定义 j，往前找到第一个非递增的数。

如果最后指针位置相遇，那么就找到了峰顶，就是一个有效的山脉数组。

最后还需要判断一下 i,j 的位置不能在两端。

```ts
function validMountainArray(arr: number[]): boolean {
  let i = 0;
  let j = arr.length - 1;
  // 从左往右找到第一个非递增的数
  while (i < arr.length && arr[i] < arr[i + 1]) {
    i++;
  }
  // 从右往左找到第一个非递增的数
  while (j > 0 && arr[j] < arr[j - 1]) {
    j--;
  }

  // 如果是峰顶，则i和j相等
  return i === j && i > 0 && j < arr.length - 1;
}
```
