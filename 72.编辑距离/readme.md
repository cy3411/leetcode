# 题目
给你两个单词 word1 和 word2，请你计算出将 word1 转换成 word2 所使用的最少操作数 。

你可以对一个单词进行如下三种操作：

+ 插入一个字符
+ 删除一个字符
+ 替换一个字符

提示：

+ 0 <= word1.length, word2.length <= 500
+ word1 和 word2 由小写英文字母组成
  
# 示例
```
输入：word1 = "horse", word2 = "ros"
输出：3
解释：
horse -> rorse (将 'h' 替换为 'r')
rorse -> rose (删除 'r')
rose -> ros (删除 'e')
```

```
输入：word1 = "intention", word2 = "execution"
输出：5
解释：
intention -> inention (删除 't')
inention -> enention (将 'i' 替换为 'e')
enention -> exention (将 'n' 替换为 'x')
exention -> exection (将 'n' 替换为 'c')
exection -> execution (插入 'u')
```

# 题解
解决两个字符串的动态规划问题，一般都是用两个指针 i,j 分别指向两个字符串的最后，然后一步步往前走，缩小问题的规模。

明确状态：
word1[0,i]和word2[0,j]最小操作数

明确选择：
`word1[i]=word2[j]`，不需要操作，直接前移
`word1[i]!=word2[j]`，有3种状态：
+ 删除word1[i]，前移i，
+ 在word[i]后插入字符，j前移
+ 替换word[i]，i和j前移

## DP函数
我们这里处理的是word1向word2变化。

明确base case:

i或者j小于0，表示字符串遍历完毕。

+ 如果word1还有字符，表示这些是需要删除的，直接返回i+1
+ 如果word2还有字符，表示这些是需要插入的，直接返回j+1
```js
/**
 * @param {string} word1
 * @param {string} word2
 * @return {number}
 */
var minDistance = function (word1, word2) {
  // 返回word1[0,i]和word2[0,j]的最小编辑距离
  const dp = (i, j) => {
    // 备忘录
    let key = `${i}&${j}`;
    if (memo.get(key)) return memo.get(key);

    // word1走到头，word2剩下需要插入的步骤
    if (i === -1) return j + 1;
    // word2走到头，word1剩下需要删除的步骤
    if (j === -1) return i + 1;

    if (word1[i] === word2[j]) {
      // 相同不做操作，继续找[0,i-1]和[0,j-1]的最小编辑距离
      memo.set(key, dp(i - 1, j - 1));
    } else {
      memo.set(
        key,
        Math.min(
          // 替换操作
          dp(i - 1, j - 1) + 1,
          // 删除操作
          dp(i - 1, j) + 1,
          // 插入操作
          dp(i, j - 1) + 1
        )
      );
    }

    return memo.get(key);
  };

  const memo = new Map();

  return dp(word1.length - 1, word2.length - 1);
};
```

## DP数组
```js
/**
 * @param {string} word1
 * @param {string} word2
 * @return {number}
 */
var minDistance = function (word1, word2) {
  let m = word1.length;
  let n = word2.length;
  //dp[i][j]表示word1[0,i]和word2[0,j]最小编辑距离
  const dp = new Array(m + 1).fill(0).map((_) => new Array(n + 1).fill(0));
  // 明确base case
  for (let i = 1; i <= m; i++) {
    // j为0，表示需要删除的操作步骤
    dp[i][0] = i;
  }
  for (let j = 1; j <= n; j++) {
    // i为0，表示需要插入的操作步骤
    dp[0][j] = j;
  }

  // 0是base case 状态，需要从1开始遍历，所以word1和word2的访问需要-1
  for (let i = 1; i <= m; i++) {
    for (let j = 1; j <= n; j++) {
      if (word1[i - 1] === word2[j - 1]) {
        dp[i][j] = dp[i - 1][j - 1];
      } else {
        dp[i][j] = Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1, dp[i - 1][j - 1] + 1);
      }
    }
  }
  return dp[m][n];
};
```