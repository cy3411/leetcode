// @algorithm @lc id=1000019 lang=typescript
// @title binode-lcci
// @test([4,2,5,1,3,null,6,0])=[0,null,1,null,2,null,3,null,4,null,5,null,6]
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function convertBiNode(root: TreeNode | null): TreeNode | null {
  // 虚拟头节点
  const head = new TreeNode(-1);
  // 指针地址
  let p = head;
  const inorder = (root: TreeNode | null) => {
    if (root === null) return;
    inorder(root.left);
    p.right = root;
    p = root;
    // 将left置空
    root.left = null;
    inorder(root.right);
  };
  inorder(root);
  return head.right;
}
