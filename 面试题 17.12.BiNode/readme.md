# 题目

二叉树数据结构 TreeNode 可用来表示单向链表（其中 left 置空，right 为下一个链表节点）。实现一个方法，把二叉搜索树转换为单向链表，要求依然符合二叉搜索树的性质，转换操作应是原址的，也就是在原始的二叉搜索树上直接修改。

返回转换后的单向链表的头节点。

提示：

- 节点数量不会超过 100000。

# 示例

```
输入： [4,2,5,1,3,null,6,0]
输出： [0,null,1,null,2,null,3,null,4,null,5,null,6]
```

# 题解

## 中序遍历

二叉搜索树的中序遍历遍历结果是升序的。

定义 head 虚拟头节点和 p 指针，p 初始指向 head。

中序遍历中，更新 p.right 为当前 root，并将 root.left 置空(题意要求),同时更新 p 指针位置。

```ts
function convertBiNode(root: TreeNode | null): TreeNode | null {
  // 虚拟头节点
  const head = new TreeNode(-1);
  // 指针地址
  let p = head;
  const inorder = (root: TreeNode | null) => {
    if (root === null) return;
    inorder(root.left);
    p.right = root;
    p = root;
    // 将left置空
    root.left = null;
    inorder(root.right);
  };
  inorder(root);
  return head.right;
}
```
