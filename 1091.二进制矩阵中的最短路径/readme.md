# 题目

给你一个 `n x n` 的二进制矩阵 `grid` 中，返回矩阵中最短 **畅通路径** 的长度。如果不存在这样的路径，返回 `-1` 。

二进制矩阵中的 **畅通路径** 是一条从 **左上角** 单元格（即，`(0, 0)`）到 **右下角** 单元格（即，`(n - 1, n - 1)`）的路径，该路径同时满足下述要求：

- 路径途经的所有单元格都的值都是 `0` 。
- 路径中所有相邻的单元格应当在 `8` 个方向之一 上连通（即，相邻两单元之间彼此不同且共享一条边或者一个角）。

**畅通路径的长度** 是该路径途经的单元格总数。

提示：

- `n == grid.length`
- `n == grid[i].length`
- `1 <= n <= 100`
- `grid[i][j]` 为 `0` 或 `1`

# 示例

[![RiceL4.md.png](https://z3.ax1x.com/2021/06/20/RiceL4.md.png)](https://imgtu.com/i/RiceL4)

```
输入：grid = [[0,1],[1,0]]
输出：2
```

# 题解

## 广度优先搜索

遍历每个位置的 `8` 个方向，将正确的搜索状态入队列，继续遍历，直到找到结束的位置。

```ts
// 搜索状态
class Data {
  i: number;
  j: number;
  length: number;
  constructor(i: number = 0, j: number = 0, length: number = 1) {
    this.i = i;
    this.j = j;
    this.length = length;
  }
}

function shortestPathBinaryMatrix(grid: number[][]): number {
  if (grid[0][0] !== 0) return -1;
  // 8个方向
  const dir = [
    [-1, 0],
    [-1, 1],
    [0, 1],
    [1, 1],
    [1, 0],
    [1, -1],
    [0, -1],
    [-1, -1],
  ];
  let ans = -1;
  const n = grid.length;
  const visited = new Array(n).fill(0).map(() => new Array(n).fill(0));
  const queue: Data[] = [];
  // 初始化队列和已访问数组
  visited[0][0] = 1;
  queue.push(new Data(0, 0, 1));
  while (queue.length) {
    const data = queue.shift();
    // 到达目标
    if (data.i === n - 1 && data.j === n - 1) {
      ans = data.length;
      break;
    }
    // 向8个方向扩展
    for (let i = 0; i < dir.length; i++) {
      let x = dir[i][0] + data.i;
      let y = dir[i][1] + data.j;
      if (x < 0 || x >= n) continue;
      if (y < 0 || y >= n) continue;
      if (visited[x][y] === 1) continue;
      if (grid[x][y] === 1) continue;
      visited[x][y] = 1;
      queue.push(new Data(x, y, data.length + 1));
    }
  }

  return ans;
}
```
