/*
 * @lc app=leetcode.cn id=396 lang=cpp
 *
 * [396] 旋转函数
 */

// @lc code=start
class Solution
{
public:
    int maxRotateFunction(vector<int> &nums)
    {
        int n = nums.size();
        int sum = 0;
        for (auto x : nums)
        {
            sum += x;
        }

        int f = 0;
        for (int i = 0; i < n; i++)
        {
            f += i * nums[i];
        }

        int ans = f;
        for (int i = 1; i < n; i++)
        {
            f += sum - n * nums[n - i];
            ans = max(ans, f);
        }

        return ans;
    }
};
// @lc code=end
