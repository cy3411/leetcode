# 题目

给定一个长度为 `n` 的整数数组 `nums` 。

假设 $arr_k$ 是数组 `nums` 顺时针旋转 `k` 个位置后的数组，我们定义 `nums` 的 旋转函数 `F` 为：

- `F(k) = 0 * arrk[0] + 1 * arrk[1] + ... + (n - 1) * arrk[n - 1]`

返回 `F(0), F(1), ..., F(n-1)` 中的最大值 。

生成的测试用例让答案符合 `32` 位 **整数**。

提示:

- $n \equiv nums.length$
- $1 \leq n \leq 10^5$
- $-100 \leq nums[i] \leq 100$

# 示例

```
输入: nums = [4,3,2,6]
输出: 26
解释:
F(0) = (0 * 4) + (1 * 3) + (2 * 2) + (3 * 6) = 0 + 3 + 4 + 18 = 25
F(1) = (0 * 6) + (1 * 4) + (2 * 3) + (3 * 2) = 0 + 4 + 6 + 6 = 16
F(2) = (0 * 2) + (1 * 6) + (2 * 4) + (3 * 3) = 0 + 6 + 8 + 9 = 23
F(3) = (0 * 3) + (1 * 2) + (2 * 6) + (3 * 4) = 0 + 2 + 12 + 12 = 26
所以 F(0), F(1), F(2), F(3) 中的最大值是 F(3) = 26 。
```

```
输入: nums = [100]
输出: 0
```

# 题解

## 递推

根据题目的公式，可以推出如下递推公式：

假设 `nums = {A,B,C,D}`

$$
\begin{aligned}
    F(0)= & 0*A+1*B+2*C+3*D \\
    F(1)= & 0*D+1*A+2*B+3*C \\
          & F(0)+A+B+C-3*D\\
          & F(0)+A+B+C+D-4*D\\
          & F(0)+sum-4*D\\
    F(2)= & 0*C+1*D+2*A+3*B \\
          & F(1)+D+A+B-3*C\\
          & F(1)+D+A+B+C-4*C\\
          & F(1)+sum-4*C\\
\end{aligned}


$$

由上面可得:

$$
F(i)=F(i-1)+sum-n*nums(n-i)
$$

根据以上递推公式，计算每个 `i` 的旋转函数值，取最大值即可。

```ts
function maxRotateFunction(nums: number[]): number {
  let n = nums.length;
  let f = 0;
  const sum = nums.reduce((a, b) => a + b, 0);

  for (let i = 0; i < n; i++) {
    f += i * nums[i];
  }

  let ans = f;
  for (let i = 1; i < n; i++) {
    f += sum - n * nums[n - i];
    ans = Math.max(ans, f);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int maxRotateFunction(vector<int> &nums)
    {
        int n = nums.size();
        int sum = 0;
        for (auto x : nums)
        {
            sum += x;
        }

        int f = 0;
        for (int i = 0; i < n; i++)
        {
            f += i * nums[i];
        }

        int ans = f;
        for (int i = 1; i < n; i++)
        {
            f += sum - n * nums[n - i];
            ans = max(ans, f);
        }

        return ans;
    }
};
```
