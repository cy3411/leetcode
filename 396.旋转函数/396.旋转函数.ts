/*
 * @lc app=leetcode.cn id=396 lang=typescript
 *
 * [396] 旋转函数
 */

// @lc code=start
function maxRotateFunction(nums: number[]): number {
  let n = nums.length;
  let f = 0;
  const sum = nums.reduce((a, b) => a + b, 0);

  for (let i = 0; i < n; i++) {
    f += i * nums[i];
  }

  let ans = f;
  for (let i = 1; i < n; i++) {
    f += sum - n * nums[n - i];
    ans = Math.max(ans, f);
  }

  return ans;
}
// @lc code=end
