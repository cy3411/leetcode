/*
 * @lc app=leetcode.cn id=388 lang=typescript
 *
 * [388] 文件的最长绝对路径
 */

// @lc code=start
function lengthLongestPath(input: string): number {
  const n = input.length;
  const stack: number[] = [];
  let i = 0;
  let ans = 0;
  while (i < n) {
    let depth = 1;
    // 利用 \t 的数量来计算当前文件或目录的深度
    while (i < n && input[i] === '\t') {
      depth++, i++;
    }
    // 判断是否是文件，并记录字符串长度
    let len = 0;
    let isFile = false;
    while (i < n && input[i] !== '\n') {
      if (input[i] === '.') isFile = true;
      len++, i++;
    }
    // 跳过\n
    i++;

    // 如果栈的深度超过当前文件的深度，需要出栈
    while (stack.length >= depth) {
      stack.pop();
    }
    // 累加路径的长度
    if (stack.length) {
      // 两个路径之间需要用 / 分割，所以需要 + 1
      len += stack[stack.length - 1] + 1;
    }
    // 是否更新答案
    if (isFile) {
      ans = Math.max(ans, len);
    } else {
      stack.push(len);
    }
  }

  return ans;
}
// @lc code=end
