# 题目

假设有一个同时存储文件和目录的文件系统。下图展示了文件系统的一个示例：

[![LyuFat.png](https://s1.ax1x.com/2022/04/21/LyuFat.png)](https://imgtu.com/i/LyuFat)

这里将 `dir` 作为根目录中的唯一目录。`dir` 包含两个子目录 `subdir1` 和 `subdir2` 。`subdir1` 包含文件 `file1.ext` 和子目录 `subsubdir1`；`subdir2` 包含子目录 `subsubdir2`，该子目录下包含文件 `file2.ext` 。

如果是代码表示，上面的文件系统可以写为 `"dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"` 。`'\n'` 和 `'\t'` 分别是换行符和制表符。

文件系统中的每个文件和文件夹都有一个唯一的 **绝对路径** ，即必须打开才能到达文件/目录所在位置的目录顺序，所有路径用 `'/'` 连接。上面例子中，指向 `file2.ext` 的 绝对路径 是 `"dir/subdir2/subsubdir2/file2.ext"` 。每个目录名由字母、数字和/或空格组成，每个文件名遵循 `name.extension` 的格式，其中 `name` 和 `extension` 由字母、数字和/或空格组成。

给定一个以上述格式表示文件系统的字符串 `input` ，返回文件系统中 _指向 文件_ 的 **最长绝对路径** 的长度 。 如果系统中没有文件，返回 `0`。

```
dir
⟶ subdir1
⟶ ⟶ file1.ext
⟶ ⟶ subsubdir1
⟶ subdir2
⟶ ⟶ subsubdir2
⟶ ⟶ ⟶ file2.ext
```

# 示例

[![Lyuexg.png](https://s1.ax1x.com/2022/04/21/Lyuexg.png)](https://imgtu.com/i/Lyuexg)

```
输入：input = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext"
输出：20
解释：只有一个文件，绝对路径为 "dir/subdir2/file.ext" ，路径长度 20
```

[![LyunMQ.png](https://s1.ax1x.com/2022/04/21/LyunMQ.png)](https://imgtu.com/i/LyunMQ)

```
输入：input = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"
输出：32
解释：存在两个文件：
"dir/subdir1/file1.ext" ，路径长度 21
"dir/subdir2/subsubdir2/file2.ext" ，路径长度 32
返回 32 ，因为这是最长的路径
```

# 题解

## 栈

从题目可以看出， 连续 '\t' 的数量表示文件或者目录的深度，相邻的文件或者目录是用 '\n' 分隔的。

我们利用栈来保存遍历过的路径的长度，栈中元素的个数就表示当前路径的深度。

如果遍历到文件的时候，我们就取出栈顶元素和当前文件长度相加，更新答案，去两者中的较大值。否则，继续将当前计算后的长度压入栈中。

如果当前深度小于栈中元素个数，表示遍历到了外层的目录。我们就将栈顶元素弹出，直到栈中元素个数小于当前深度。

```ts
function lengthLongestPath(input: string): number {
  const n = input.length;
  const stack: number[] = [];
  let i = 0;
  let ans = 0;
  while (i < n) {
    let depth = 1;
    // 利用 \t 的数量来计算当前文件或目录的深度
    while (i < n && input[i] === '\t') {
      depth++, i++;
    }
    // 判断是否是文件，并记录字符串长度
    let len = 0;
    let isFile = false;
    while (i < n && input[i] !== '\n') {
      if (input[i] === '.') isFile = true;
      len++, i++;
    }
    // 跳过\n
    i++;

    // 如果栈的深度超过当前文件的深度，需要出栈
    while (stack.length >= depth) {
      stack.pop();
    }
    // 累加路径的长度
    if (stack.length) {
      // 两个路径之间需要用 / 分割，所以需要 + 1
      len += stack[stack.length - 1] + 1;
    }
    // 是否更新答案
    if (isFile) {
      ans = Math.max(ans, len);
    } else {
      stack.push(len);
    }
  }

  return ans;
}
```
