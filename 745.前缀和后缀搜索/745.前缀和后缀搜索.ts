/*
 * @lc app=leetcode.cn id=745 lang=typescript
 *
 * [745] 前缀和后缀搜索
 */

// @lc code=start
class WordFilter {
  hash = new Map<string, number>();
  constructor(words: string[]) {
    const n = words.length;
    for (let i = 0; i < n; i++) {
      const word = words[i];
      const m = word.length;
      for (let prefixSize = 1; prefixSize <= m; prefixSize++) {
        for (let suffixSize = 1; suffixSize <= m; suffixSize++) {
          const prefix = word.substring(0, prefixSize);
          const suffix = word.substring(m - suffixSize);
          const key = `${prefix}@${suffix}`;
          this.hash.set(key, i);
        }
      }
    }
  }

  f(pref: string, suff: string): number {
    const key = `${pref}@${suff}`;
    return this.hash.get(key) ?? -1;
  }
}

/**
 * Your WordFilter object will be instantiated and called as such:
 * var obj = new WordFilter(words)
 * var param_1 = obj.f(pref,suff)
 */
// @lc code=end
