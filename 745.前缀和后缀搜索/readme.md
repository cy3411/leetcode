# 题目

设计一个包含一些单词的特殊词典，并能够通过前缀和后缀来检索单词。

实现 `WordFilter` 类：

- `WordFilter(string[] words)` 使用词典中的单词 `words` 初始化对象。
- `f(string pref, string suff)` 返回词典中具有前缀 `prefix` 和后缀 `suff` 的单词的下标。如果存在不止一个满足要求的下标，返回其中 **最大的下标** 。如果不存在这样的单词，返回 `-1` 。

提示：

- $1 \leq words.length \leq 10^4$
- $1 \leq words[i].length \leq 7$
- $1 \leq pref.length, suff.length \leq 7$
- ` words[i]``、pref ` 和 `suff` 仅由小写英文字母组成
- 最多对函数 `f` 执行 $10^4$ 次调用

# 示例

```
输入
["WordFilter", "f"]
[[["apple"]], ["a", "e"]]
输出
[null, 0]
解释
WordFilter wordFilter = new WordFilter(["apple"]);
wordFilter.f("a", "e"); // 返回 0 ，因为下标为 0 的单词：前缀 prefix = "a" 且 后缀 suff = "e" 。
```

# 题解

## 哈希表

预处理每个单词的所有前后缀可能，使用特殊符号分割，作为键，对应单词的最大下标最为值保存到哈希表中。

检索时，只需要将前后缀用特殊符号隔开后，作为键，查找哈希表即可。

```ts
class WordFilter {
  hash = new Map<string, number>();
  constructor(words: string[]) {
    const n = words.length;
    for (let i = 0; i < n; i++) {
      const word = words[i];
      const m = word.length;
      for (let prefixSize = 1; prefixSize <= m; prefixSize++) {
        for (let suffixSize = 1; suffixSize <= m; suffixSize++) {
          const prefix = word.substring(0, prefixSize);
          const suffix = word.substring(m - suffixSize);
          const key = `${prefix}@${suffix}`;
          this.hash.set(key, i);
        }
      }
    }
  }

  f(pref: string, suff: string): number {
    const key = `${pref}@${suff}`;
    return this.hash.get(key) ?? -1;
  }
}
```
