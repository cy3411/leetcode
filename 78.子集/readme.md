# 题目

给你一个整数数组 `nums` ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。你可以按 **任意顺序** 返回解集。

提示：

- 1 <= nums.length <= 10
- -10 <= nums[i] <= 10
- nums 中的所有元素 互不相同

# 示例

```
输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
```

# 题解

## 子集枚举法

组合的数量是的$2^n$，n 为 nums 的长度。

所以我们可以使用二进制位标记数组元素是否选择，遍历[0,$2^n$]，然后判断当前位是否可以选择，可以话，放入结果。

例如：nums = [1,2,3]，我们使用三位二进制来表示元素是否选择，

```
000 []
001 [1]
010 [2]
100 [3]
011 [1,2]
```

```ts
function subsets(nums: number[]): number[][] {
  // 子集枚举法，二进制
  const m = nums.length;
  const ans: number[][] = [];

  for (let i = 0; i < 1 << m; i++) {
    const res = [];
    // 枚举每一位
    for (let j = 0; j < m; j++) {
      if (i & (1 << j)) {
        res.push(nums[j]);
      }
    }
    ans.push(res);
  }

  return ans;
}
```

2 次幂的二进制只有一个数字 1，我们可以提前使用哈希，将 2 次幂和数组下标关联起来，这样可以快速定位到数组元素。

理解两个位操作：

- x&(x-1)，可以消除 x 的低位 1

```
0110 x
0101 x-1
0100 位与结果
```

- x&(-x)，可以找到 x 的低位 1

```
0110 x
1010 -x(x的反码+1)
0010 位与结果
```

```ts
function subsets(nums: number[]): number[][] {
  // 子集枚举法，二进制
  const m = nums.length;
  // 位中1和nums索引的对应关系
  const mark: Map<number, number> = new Map();
  for (let i = 0, j = 1; i < 1 << m; i++, j <<= 1) {
    mark[j] = i;
  }

  const ans: number[][] = [];

  for (let i = 0; i < 1 << m; i++) {
    const res = [];
    // 枚举每一位1
    let val = i;
    while (val !== 0) {
      // 找到低位1，并从mark中找到对应的数组下标
      res.push(nums[mark[val & -val]]);
      //   将低位1消除
      val &= val - 1;
    }
    ans.push(res);
  }

  return ans;
}
```
