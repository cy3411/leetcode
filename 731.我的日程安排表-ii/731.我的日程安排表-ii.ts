/*
 * @lc app=leetcode.cn id=731 lang=typescript
 *
 * [731] 我的日程安排表 II
 */

// @lc code=start
class MyCalendarTwo {
  // 日程表
  books: number[][] = [];
  // 覆盖区域
  overlaps: number[][] = [];
  constructor() {}

  book(start: number, end: number): boolean {
    // 新的时间段是否和覆盖区域有重叠
    for (const [l, r] of this.overlaps) {
      if (start < r && end > l) {
        return false;
      }
    }
    // 新的时间段所能形成的覆盖区
    for (const [l, r] of this.books) {
      if (start < r && end > l) {
        this.overlaps.push([Math.max(start, l), Math.min(end, r)]);
      }
    }
    // 将新的时间段加入日程表
    this.books.push([start, end]);
    return true;
  }
}

/**
 * Your MyCalendarTwo object will be instantiated and called as such:
 * var obj = new MyCalendarTwo()
 * var param_1 = obj.book(start,end)
 */
// @lc code=end
