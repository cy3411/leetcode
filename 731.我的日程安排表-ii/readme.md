# 题目

实现一个 `MyCalendar` 类来存放你的日程安排。如果要添加的时间内不会导致三重预订时，则可以存储这个新的日程安排。

`MyCalendar` 有一个 `book(int start, int end)`方法。它意味着在 `start` 到 `end` 时间内增加一个日程安排，注意，这里的时间是半开区间，即 `[start, end)`, 实数 `x` 的范围为， $start \leq x < end$。

当三个日程安排有一些时间上的交叉时（例如三个日程安排都在同一时间内），就会产生三重预订。

每次调用 `MyCalendar.book` 方法时，如果可以将日程安排成功添加到日历中而不会导致三重预订，返回 `true`。否则，返回 `false` 并且不要将该日程安排添加到日历中。

请按照以下步骤调用 `MyCalendar` 类: `MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)`

提示：

- 每个测试用例，调用 `MyCalendar.book` 函数最多不超过 `1000` 次。
- 调用函数 `MyCalendar.book(start, end)`时， `start` 和 `end` 的取值范围为 $[0, 10^9]$。

# 示例

```
MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(50, 60); // returns true
MyCalendar.book(10, 40); // returns true
MyCalendar.book(5, 15); // returns false
MyCalendar.book(5, 10); // returns true
MyCalendar.book(25, 55); // returns true
解释：
前两个日程安排可以添加至日历中。 第三个日程安排会导致双重预订，但可以添加至日历中。
第四个日程安排活动（5,15）不能添加至日历中，因为它会导致三重预订。
第五个日程安排（5,10）可以添加至日历中，因为它未使用已经双重预订的时间10。
第六个日程安排（25,55）可以添加至日历中，因为时间 [25,40] 将和第三个日程安排双重预订；
时间 [40,50] 将单独预订，时间 [50,55）将和第二个日程安排双重预订。
```

# 题解

## 遍历

定义 books 记录课程表区间，overlaps 记录已经预订过两次的区间。

当预订新的区间时，如果和 overlps 中的区间有重叠，直接返回 false。否则，将新的区间与 books 中的区间比较，如果有重叠，将新的重叠区间添加到 overlaps 中，同时将新的区间添加到 books 中。

```ts
class MyCalendarTwo {
  // 日程表
  books: number[][] = [];
  // 覆盖区域
  overlap: number[][] = [];
  constructor() {}

  book(start: number, end: number): boolean {
    // 新的时间段是否和覆盖区域有重叠
    for (const [l, r] of this.overlap) {
      if (start < r && end > l) {
        return false;
      }
    }
    // 新的时间段所能形成的覆盖区
    for (const [l, r] of this.books) {
      if (start < r && end > l) {
        this.overlap.push([Math.max(start, l), Math.min(end, r)]);
      }
    }
    // 将新的时间段加入日程表
    this.books.push([start, end]);
    return true;
  }
}
```

```cpp
class MyCalendarTwo {
private:
    vector<pair<int, int>> books;
    vector<pair<int, int>> overlaps;

public:
    MyCalendarTwo() {}

    bool book(int start, int end) {
        for (auto x : overlaps) {
            if (start < x.second && end > x.first) return false;
        }
        for (auto x : books) {
            if (start < x.second && end > x.first) {
                overlaps.push_back({max(start, x.first), min(end, x.second)});
            }
        }
        books.push_back({start, end});
        return true;
    }
};
```
