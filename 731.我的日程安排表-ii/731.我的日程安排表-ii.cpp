/*
 * @lc app=leetcode.cn id=731 lang=cpp
 *
 * [731] 我的日程安排表 II
 */

// @lc code=start
class MyCalendarTwo {
private:
    vector<pair<int, int>> books;
    vector<pair<int, int>> overlaps;

public:
    MyCalendarTwo() {}

    bool book(int start, int end) {
        for (auto x : overlaps) {
            if (start < x.second && end > x.first) return false;
        }
        for (auto x : books) {
            if (start < x.second && end > x.first) {
                overlaps.push_back({max(start, x.first), min(end, x.second)});
            }
        }
        books.push_back({start, end});
        return true;
    }
};

/**
 * Your MyCalendarTwo object will be instantiated and called as such:
 * MyCalendarTwo* obj = new MyCalendarTwo();
 * bool param_1 = obj->book(start,end);
 */
// @lc code=end
