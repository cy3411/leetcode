# 题目

有个内含单词的超大文本文件，给定任意两个`不同的`单词，找出在这个文件中这两个单词的最短距离(相隔单词数)。如果寻找过程在这个文件中会重复多次，而每次寻找的单词不同，你能对此优化吗?

- $words.length <= 100000$

# 示例

```
输入：words = ["I","am","a","student","from","a","university","in","a","city"], word1 = "a", word2 = "student"
输出：1
```

# 题解

## 双指针

遍历 words，定义 idx1，idx2 分别记录 word1 和 word2 的位置，当 idx1 和 idx2 都不为 -1 时，计算距离，并且更新 idx1 和 idx2。

```ts
function findClosest(words: string[], word1: string, word2: string): number {
  const n = words.length;
  let idx1 = -1;
  let idx2 = -1;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < n; i++) {
    const w = words[i];
    if (w === word1) idx1 = i;
    else if (w === word2) idx2 = i;

    if (idx1 >= 0 && idx2 >= 0) {
      ans = Math.min(ans, Math.abs(idx1 - idx2));
    }
  }

  return ans;
}
```
