// @algorithm @lc id=1000039 lang=typescript
// @title find-closest-lcci
// @test(["I","am","a","student","from","a","university","in","a","city"],"a","student")=1
function findClosest(words: string[], word1: string, word2: string): number {
  const n = words.length;
  let idx1 = -1;
  let idx2 = -1;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < n; i++) {
    const w = words[i];
    if (w === word1) idx1 = i;
    else if (w === word2) idx2 = i;

    if (idx1 >= 0 && idx2 >= 0) {
      ans = Math.min(ans, Math.abs(idx1 - idx2));
    }
  }

  return ans;
}
