// @algorithm @lc id=100292 lang=javascript
// @title er-jin-zhi-zhong-1de-ge-shu-lcof
// @test(9)=2
/**
 * @param {number} n - a positive integer
 * @return {number}
 */
var hammingWeight = function (n) {
  let ans = 0;

  while (n !== 0) {
    n &= n - 1;
    ans++;
  }

  return ans;
};
