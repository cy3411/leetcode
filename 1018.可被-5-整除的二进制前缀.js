/*
 * @lc app=leetcode.cn id=1018 lang=javascript
 *
 * [1018] 可被 5 整除的二进制前缀
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {boolean[]}
 */
var prefixesDivBy5 = function (A) {
  const result = [];
  let prefix = 0n;

  for (let i = 0; i < A.length; i++) {
    prefix = (prefix << 1n) + BigInt(A[i]);
    result.push(prefix % 5n === 0n ? true : false);
  }

  return result;
};
// @lc code=end
