# 题目

给你一份 n 位朋友的亲近程度列表，其中 n 总是 偶数 。

对每位朋友 i，preferences[i] 包含一份 按亲近程度从高到低排列 的朋友列表。换句话说，排在列表前面的朋友与 i 的亲近程度比排在列表后面的朋友更高。每个列表中的朋友均以 0 到 n-1 之间的整数表示。

所有的朋友被分成几对，配对情况以列表 pairs 给出，其中 pairs[i] = [xi, yi] 表示 xi 与 yi 配对，且 yi 与 xi 配对。

但是，这样的配对情况可能会是其中部分朋友感到不开心。在 x 与 y 配对且 u 与 v 配对的情况下，如果同时满足下述两个条件，x 就会不开心：

- x 与 u 的亲近程度胜过 x 与 y，且
- u 与 x 的亲近程度胜过 u 与 v

返回 不开心的朋友的数目 。

提示：

- 2 <= n <= 500
- n 是偶数
- preferences.length == n
- preferences[i].length == n - 1
- 0 <= preferences[i][j] <= n - 1
- preferences[i] 不包含 i
- preferences[i] 中的所有值都是独一无二的
- pairs.length == n/2
- pairs[i].length == 2
- xi != yi
- 0 <= xi, yi <= n - 1
- 每位朋友都 恰好 被包含在一对中

# 示例

```
输入：n = 4, preferences = [[1, 2, 3], [3, 2, 0], [3, 1, 0], [1, 2, 0]], pairs = [[0, 1], [2, 3]]
输出：2
解释：
朋友 1 不开心，因为：
- 1 与 0 配对，但 1 与 3 的亲近程度比 1 与 0 高，且
- 3 与 1 的亲近程度比 3 与 2 高。
朋友 3 不开心，因为：
- 3 与 2 配对，但 3 与 1 的亲近程度比 3 与 2 高，且
- 1 与 3 的亲近程度比 1 与 0 高。
朋友 0 和 2 都是开心的。
```

# 题解

## 模拟

按照题目的意思去模拟，找到错误的组队就可以了。

比如，x 爱着 u ，却把 x 和 y 组成了一队，同时，u 也爱着 x，却把 u 和 v 组成了一队，这时候 x 和 u 都是不开心的。

所以，针对 x，需要先找到组队的 y，看 x 亲近朋友表里是否还有比 y 更大的 u。如果有，在看亲近朋友表里是否有 v，比 x 的跟 u 的亲近关系更高，如果有，x 就是不开心的。

这个过程中，我们需要快速定位 x 与 y 亲近程度和与 x 组队的朋友。这里采用预处理，将两种关系用哈希表存储。

```ts
function unhappyFriends(n: number, preferences: number[][], pairs: number[][]): number {
  const order = new Array(n).fill(0).map((_) => new Array(n).fill(0));
  // i和j的亲密关系，j约小，亲密关系越大
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n - 1; j++) {
      order[i][preferences[i][j]] = j;
    }
  }

  const match = new Array(n).fill(0);
  // pairs的匹配关系
  for (const [x, y] of pairs) {
    match[x] = y;
    match[y] = x;
  }

  let ans = 0;
  for (let x = 0; x < n; x++) {
    const y = match[x];
    const idx = order[x][y];
    console.log(idx);
    for (let j = 0; j < idx; j++) {
      const u = preferences[x][j];
      const v = match[u];
      if (order[u][x] < order[u][v]) {
        ans++;
        break;
      }
    }
  }

  return ans;
```

```cpp
class Solution
{
public:
    int unhappyFriends(int n, vector<vector<int>> &preferences, vector<vector<int>> &pairs)
    {
        // g[i][j]表示 i 对 j 亲近程度
        vector<vector<int>> g(n, vector<int>(n, 0));
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n - 1; j++)
            {
                g[i][preferences[i][j]] = j;
            }
        }
        // pairs中，双方亲近程度
        vector<int> like(n);
        for (auto &p : pairs)
        {
            like[p[0]] = g[p[0]][p[1]];
            like[p[1]] = g[p[1]][p[0]];
        }

        int ans = 0;
        // 遍历，判断是否有不开心的朋友
        for (int i = 0; i < n; i++)
        {
            // i 当前配对的人之前是否还有人
            for (int j = 0; j < like[i]; j++)
            {
                int p = preferences[i][j];
                // p 对 i 的亲近程度不是最高的，i 不开心
                if (g[p][i] < like[p])
                {
                    ans++;
                    break;
                }
            }
        }
        return ans;
    }
};
```
