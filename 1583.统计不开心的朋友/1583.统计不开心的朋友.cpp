/*
 * @lc app=leetcode.cn id=1583 lang=cpp
 *
 * [1583] 统计不开心的朋友
 */

// @lc code=start
class Solution
{
public:
    int unhappyFriends(int n, vector<vector<int>> &preferences, vector<vector<int>> &pairs)
    {
        // g[i][j]表示 i 对 j 亲近程度
        vector<vector<int>> g(n, vector<int>(n, 0));
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n - 1; j++)
            {
                g[i][preferences[i][j]] = j;
            }
        }
        // pairs中，双方亲近程度
        vector<int> like(n);
        for (auto &p : pairs)
        {
            like[p[0]] = g[p[0]][p[1]];
            like[p[1]] = g[p[1]][p[0]];
        }

        int ans = 0;
        // 遍历，判断是否有不开心的朋友
        for (int i = 0; i < n; i++)
        {
            // i 当前配对的人之前是否还有人
            for (int j = 0; j < like[i]; j++)
            {
                int p = preferences[i][j];
                // p 对 i 的亲近程度不是最高的，i 不开心
                if (g[p][i] < like[p])
                {
                    ans++;
                    break;
                }
            }
        }
        return ans;
    }
};
// @lc code=end
