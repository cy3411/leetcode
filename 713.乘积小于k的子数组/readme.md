# 题目

给你一个整数数组 `nums` 和一个整数 `k` ，请你返回子数组内所有元素的乘积严格小于 `k` 的连续子数组的数目。

提示:

- $1 \leq nums.length \leq 3 * 10^4$
- $1 \leq nums[i] \leq 1000$
- $0 \leq k \leq 10^6$

# 示例

```
输入：nums = [10,5,2,6], k = 100
输出：8
解释：8 个乘积小于 100 的子数组分别为：[10]、[5]、[2],、[6]、[10,5]、[5,2]、[2,6]、[5,2,6]。
需要注意的是 [10,5,2] 并不是乘积小于 100 的子数组。
```

```
输入：nums = [1,2,3], k = 0
输出：0
```

# 题解

## 滑动窗口

枚举子数组的右边界 `j` ，并且左边界是 `i = 0` 开始，记录子数组 `[i, j]` 的乘积 `prod` 。

每枚举一个 `j`，就更新 `prod = prod * nums[j]`。如果 `prod >= k`，那么就右移左边界 `i`，并更新 `prod = prod / nums[i]`。直到 `prod < k` 或者 `i > j`，当前区间的子数组数目就是 `j - i + 1`。所有子树组数目之和就是答案。

```ts
function numSubarrayProductLessThanK(nums: number[], k: number): number {
  const n = nums.length;
  let prod = 1;
  let i = 0;
  let ans = 0;
  for (let j = 0; j < n; j++) {
    prod *= nums[j];
    while (i <= j && prod >= k) {
      prod /= nums[i];
      i++;
    }
    ans += j - i + 1;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int numSubarrayProductLessThanK(vector<int> &nums, int k)
    {
        int n = nums.size(), prod = 1, ans = 0, i = 0;
        for (int j = 0; j < n; j++)
        {
            prod *= nums[j];
            while (i <= j && prod >= k)
            {
                prod /= nums[i];
                i++;
            }
            ans += j - i + 1;
        }
        return ans;
    }
};
```

```python
class Solution:
    def numSubarrayProductLessThanK(self, nums: List[int], k: int) -> int:
        n = len(nums)
        ans = 0
        prod = 1
        i = 0
        for j in range(n):
            prod *= nums[j]
            while prod >= k and i <= j:
                prod /= nums[i]
                i += 1
            ans += j - i + 1
        return ans
```
