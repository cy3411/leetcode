/*
 * @lc app=leetcode.cn id=713 lang=typescript
 *
 * [713] 乘积小于K的子数组
 */

// @lc code=start
function numSubarrayProductLessThanK(nums: number[], k: number): number {
  const n = nums.length;
  let prod = 1;
  let i = 0;
  let ans = 0;
  for (let j = 0; j < n; j++) {
    prod *= nums[j];
    while (i <= j && prod >= k) {
      prod /= nums[i];
      i++;
    }
    ans += j - i + 1;
  }
  return ans;
}
// @lc code=end
