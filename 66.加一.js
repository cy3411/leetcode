/*
 * @lc app=leetcode.cn id=66 lang=javascript
 *
 * [66] 加一
 */

// @lc code=start
/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function (digits) {
  const size = digits.length
  for (let j = size - 1; j >= 0; j--) {
    digits[j]++
    digits[j] %= 10
    if (digits[j] !== 0) {
      return digits
    }
  }

  return [1].concat(Array(size).fill(0))

};
// @lc code=end

