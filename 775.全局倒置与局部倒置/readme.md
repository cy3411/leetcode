# 题目

给你一个长度为 n 的整数数组 nums ，表示由范围 [0, n - 1] 内所有整数组成的一个排列。

全局倒置 的数目等于满足下述条件不同下标对 (i, j) 的数目：

- $0 \leq i < j < n$
- $nums[i] > nums[j]$

局部倒置 的数目等于满足下述条件的下标 i 的数目：

- $0 \leq i < n - 1$
- $nums[i] > nums[i + 1]$

当数组 nums 中 全局倒置 的数量等于 局部倒置 的数量时，返回 true ；否则，返回 false 。

提示：

- $n == nums.length$
- $1 \leq n \leq 10^5$
- $0 \leq nums[i] < n$
- nums 中的所有整数 互不相同
- nums 是范围 [0, n - 1] 内所有数字组成的一个排列

# 示例

```
输入：nums = [1,0,2]
输出：true
解释：有 1 个全局倒置，和 1 个局部倒置。
```

```
输入：nums = [1,2,0]
输出：false
解释：有 2 个全局倒置，和 1 个局部倒置。
```

# 题解

## 后缀最小值

题意可以看出，一个局部倒置就是一个全局倒置，因此要判断两者是否相等，只要检查数组中是否存在不是局部倒置，但是全局倒置的组合。也就是 $nums[i] > nums[j]并且i < j-1$。

我们维护一个最小后缀值，倒序遍历，如果有一个元素大于最小后缀，那么直接返回 false。因为出现了全局倒置，但不是一个局部倒置的组合。遍历结束后，返回 true。

```ts
function isIdealPermutation(nums: number[]): boolean {
  const n = nums.length;
  let minSuffix = nums[n - 1];

  for (let i = n - 3; i >= 0; i--) {
    if (nums[i] > minSuffix) return false;
    minSuffix = Math.min(minSuffix, nums[i + 1]);
  }
  return true;
}
```
