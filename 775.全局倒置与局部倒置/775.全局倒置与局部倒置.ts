/*
 * @lc app=leetcode.cn id=775 lang=typescript
 *
 * [775] 全局倒置与局部倒置
 */

// @lc code=start
function isIdealPermutation(nums: number[]): boolean {
  const n = nums.length;
  let minSuffix = nums[n - 1];
  for (let i = n - 3; i >= 0; i--) {
    if (nums[i] > minSuffix) return false;
    minSuffix = Math.min(minSuffix, nums[i + 1]);
  }
  return true;
}
// @lc code=end
