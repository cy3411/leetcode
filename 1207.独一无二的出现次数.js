/*
 * @lc app=leetcode.cn id=1207 lang=javascript
 *
 * [1207] 独一无二的出现次数
 */

// @lc code=start
/**
 * @param {number[]} arr
 * @return {boolean}
 */
var uniqueOccurrences = function (arr) {
  const mapper = new Map();
  // 统计单个数字出现的数量，存储在hash
  for (let n of arr) {
    if (mapper.has(n)) {
      mapper.set(n, mapper.get(n) + 1);
    } else {
      mapper.set(n, 1);
    }
  }

  const time = new Set();
  // 统计出现的次数，放入set去重
  for (let [key, value] of mapper) {
    time.add(value);
  }
  // 返回2个hash的size比较
  return time.size === mapper.size;
};
// @lc code=end

/**
 * 时间复杂度：O(N)，其中 N 为数组的长度。遍历原始数组需要 O(N) 时间，而遍历中间过程产生的哈希表又需要 O(N) 的时间
 * 空间复杂度：O(N)
 */
