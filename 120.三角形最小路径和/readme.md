# 题目

给定一个三角形 `triangle` ，找出自顶向下的最小路径和。

每一步只能移动到下一行中相邻的结点上。**相邻的结点** 在这里指的是 **下标** 与 **上一层结点下标** 相同或者等于 **上一层结点下标 + 1** 的两个结点。也就是说，如果正位于当前行的下标 i ，那么下一步可以移动到下一行的下标 `i` 或 `i + 1` 。

提示：

- `1 <= triangle.length <= 200`
- `triangle[0].length == 1`
- `triangle[i].length == triangle[i - 1].length + 1`
- `-104 <= triangle[i][j] <= 104`

进阶：

你可以只使用 `O(n)` 的额外空间（`n` 为三角形的总行数）来解决这个问题吗？

# 示例

```
输入：triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
输出：11
解释：如下面简图所示：
   2
  3 4
 6 5 7
4 1 8 3
自顶向下的最小路径和为 11（即，2 + 3 + 5 + 1 = 11）。
```

# 题解

## 动态规划

**状态**

定义 `dp[i][j]`表示`[i,j]`位置的最小路径和。

我们这里从底向上递推出结果。

**转移**

题意给出，状态的转移是根据相邻节点来确定的。我们需要寻找最小路径和，所以需要从值较小的相邻节点转移过来。

$$dp[i][j] = triangle[i][j] + min(dp[i+1][j], dp[i+1][j-1])$$

由于状态转移只依赖于上一个状态，所以我们使用滚动数组来优化。

```ts
function minimumTotal(triangle: number[][]): number {
  const m = triangle.length;
  // 滚动数组
  const dp = new Array(2).fill(0).map((_) => new Array(m));
  // 初始化
  dp[(m - 1) % 2] = [...triangle[m - 1]];

  for (let i = m - 2; i >= 0; i--) {
    const currIdx = i % 2;
    const nextIdx = Number(!currIdx);
    for (let j = 0; j <= i; j++) {
      // 当前值加上下一层的相邻节点的最小值
      dp[currIdx][j] = triangle[i][j] + Math.min(dp[nextIdx][j], dp[nextIdx][j + 1]);
    }
  }

  return dp[0][0];
}
```
