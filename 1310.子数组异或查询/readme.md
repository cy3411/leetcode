# 题目
有一个正整数数组 `arr`，现给你一个对应的查询数组 `queries`，其中 `queries[i] = [Li, Ri]`。

对于每个查询 `i`，请你计算从 `Li` 到 `Ri` 的 **XOR** 值（即 `arr[Li] xor arr[Li+1] xor ... xor arr[Ri]`）作为本次查询的结果。

并返回一个包含给定查询 `queries` 所有结果的数组。

提示：
+ 1 <= arr.length <= 3 * 10^4
+ 1 <= arr[i] <= 10^9
+ 1 <= queries.length <= 3 * 10^4
+ queries[i].length == 2
+ 0 <= queries[i][0] <= queries[i][1] < arr.length

# 示例
```
输入：arr = [1,3,4,8], queries = [[0,1],[1,2],[0,3],[3,3]]
输出：[2,7,14,8] 
解释：
数组中元素的二进制表示形式是：
1 = 0001 
3 = 0011 
4 = 0100 
8 = 1000 
查询的 XOR 值为：
[0,1] = 1 xor 3 = 2 
[1,2] = 3 xor 4 = 7 
[0,3] = 1 xor 3 xor 4 xor 8 = 14 
[3,3] = 8
```

# 题解
## 位运算
遍历 `queries` ，根据下标从 `arr` 取出元素，按照题意运算出结果。
```ts
function xorQueries(arr: number[], queries: number[][]): number[] {
  const result = [];

  for (let [l, r] of queries) {
    let temp = 0;
    for (let i = l; i <= r; i++) {
      temp ^= arr[i];
    }
    result.push(temp);
  }

  return result;
}
```

## 前缀和
获得某个区间范围的值，可以使用前缀和。

计算 `arr` 异或前缀和数组，最后遍历 `queries` 按照范围取出对应的结果。

```ts
function xorQueries(arr: number[], queries: number[][]): number[] {
  const result = [];
  // 前缀和数组
  const presum = new Array(arr.length);
  presum[0] = arr[0];

  for (let i = 1; i < presum.length; i++) {
    presum[i] = presum[i - 1] ^ arr[i];
  }

  for (const [l, r] of queries) {
    if (l === 0) {
      result.push(presum[r]);
    } else {
      result.push(presum[r] ^ presum[l - 1]);
    }
  }

  return result;
}
```
