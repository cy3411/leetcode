/*
 * @lc app=leetcode.cn id=1310 lang=typescript
 *
 * [1310] 子数组异或查询
 */

// @lc code=start
function xorQueries(arr: number[], queries: number[][]): number[] {
  const result = [];
  // 前缀和数组
  const presum = new Array(arr.length);
  presum[0] = arr[0];

  for (let i = 1; i < presum.length; i++) {
    presum[i] = presum[i - 1] ^ arr[i];
  }

  for (const [l, r] of queries) {
    if (l === 0) {
      result.push(presum[r]);
    } else {
      result.push(presum[r] ^ presum[l - 1]);
    }
  }

  return result;
}
// @lc code=end
