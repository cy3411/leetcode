# 题目

给你一个字符串 s ，找出其中最长的回文子序列，并返回该序列的长度。

子序列定义为：不改变剩余字符顺序的情况下，删除某些字符或者不删除任何字符形成的一个序列。

提示：

- 1 <= s.length <= 1000
- s 仅由小写英文字母组成

# 示例

```
输入：s = "bbbab"
输出：4
解释：一个可能的最长回文子序列为 "bbbb" 。
```

```
输入：s = "cbbd"
输出：2
解释：一个可能的最长回文子序列为 "bb" 。
```

# 题解

## 动态规划

区间 dp 问题，我们从最小区间，逐步判断首尾 2 个字符是否相等，相等的话就拼接上，形成新的回文。不等的话，可以取中间区间的最大长度。

**状态**

定义 $dp[i][j],i<=j$，区间内最长回文的长度

**转移**

遍历 i 的位置，然后遍历 j 的位置。$i<j$。
当 i 和 j 位置的字符相同，$dp[i][j] = dp[i+1][j-1]+2$。
字符不同，$max(dp[i+1][j], dp[i][j-1])$

$$
dp[i][j] = \begin{cases}
    dp[i+1][j-1]+2, ci=cj \\
    max(dp[i+1][j],dp[i][j-1]),ci \neq cj
\end{cases}
$$

**Base Case**

$dp[i][i]=1， 0<=i<m$，1 个字符也是回文，长度为 1

```ts
function longestPalindromeSubseq(s: string): number {
  const m = s.length;
  // dp[i][j]表示[i,j]区间内最长回文长度
  const dp = new Array(m).fill(0).map((_) => new Array(m).fill(0));

  for (let i = m - 1; i >= 0; i--) {
    // 单个字符一定是回文，长度为1
    dp[i][i] = 1;
    const ci = s[i];
    for (let j = i + 1; j < m; j++) {
      const cj = s[j];
      if (ci === cj) {
        // 首尾字符相等，可以根[i+1,j-1]连成新的回文
        dp[i][j] = dp[i + 1][j - 1] + 2;
      } else {
        // 首尾不相等，取[i+1,j]和[i,j-1]区间的最大长度
        dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]);
      }
    }
  }

  return dp[0][m - 1];
}
```
