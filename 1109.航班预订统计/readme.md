# 题目

这里有 `n` 个航班，它们分别从 `1` 到 `n` 进行编号。

有一份航班预订表 `bookings` ，表中第 `i` 条预订记录 $\color{olive}bookings[i] = [first_i, last_i, seats_i]$ 意味着在从 $\color{olive}first_i$ 到 $\color{olive}last_i$ （包含 $\color{olive} first_i$ 和 $\color{olive} last_i$ ）的 每个航班 上预订了 $\color{olive} seats_i$ 个座位。

请你返回一个长度为 `n` 的数组 `answer`，其中 `answer[i]` 是航班 `i` 上预订的座位总数。

提示：

- $\color{olive}{1 <= n <= 2 * 10^4}$
- $\color{olive}{1 <= bookings.length <= 2 * 10^4}$
- `bookings[i].length == 3`
- $\color{olive}{1 <= first_i <= last_i <= n}$
- $\color{olive}{1 <= seats_i <= 104}$

# 示例

```
输入：bookings = [[1,2,10],[2,3,20],[2,5,25]], n = 5
输出：[10,55,45,25,25]
解释：
航班编号        1   2   3   4   5
预订记录 1 ：   10  10
预订记录 2 ：       20  20
预订记录 3 ：       25  25  25  25
总座位数：      10  55  45  25  25
因此，answer = [10,55,45,25,25]
```

# 题解

## 暴力

遍历每一个预定表,将表中的 `first` 到 `last` 的航班全部累加计算.

```ts
function corpFlightBookings(bookings: number[][], n: number): number[] {
  const ans = new Array(n).fill(0);

  for (let i = 0; i < bookings.length; i++) {
    const [first, last, seats] = bookings[i];
    for (let j = first; j <= last; j++) {
      ans[j - 1] += seats;
    }
  }

  return ans;
}
```

## 差分

差分数组的性质是，当我们希望对原数组的某一个区间 `[l,r]` 施加一个增量`inc` 时，差分数组 d 对应的改变是：`d[l]` 增加 `inc`，`d[r+1]` 减少 `inc`。

这样对于区间的修改就变为了对于两个位置的修改。并且这种修改是可以叠加的，即当我们多次对原数组的不同区间施加不同的增量，我们只要按规则修改差分数组即可。

```ts
function corpFlightBookings(bookings: number[][], n: number): number[] {
  const ans: number[] = new Array(n).fill(0);
  // 计算差分
  for (const [l, r, k] of bookings) {
    ans[l - 1] += k;
    if (r < n) ans[r] -= k;
  }
  // 计算前缀和,就是答案
  for (let i = 1; i < n; i++) {
    ans[i] += ans[i - 1];
  }

  return ans;
}
```

## 线段树

利用差分的特性将 bookings 的元素放入线段树中，然后计算线段树中每个元素的和即可。

```ts
class FenwickTree {
  data: number[];
  n: number;
  constructor(n: number) {
    this.n = n;
    this.data = new Array(n + 1).fill(0);
  }

  _lowbit(x: number): number {
    return x & -x;
  }

  update(i: number, v: number) {
    while (i <= this.n) {
      this.data[i] += v;
      i += this._lowbit(i);
    }
  }

  query(i: number) {
    let sum = 0;
    while (i > 0) {
      sum += this.data[i];
      i -= this._lowbit(i);
    }
    return sum;
  }
}
function corpFlightBookings(bookings: number[][], n: number): number[] {
  const fenwickTree = new FenwickTree(n);
  for (const [i, j, value] of bookings) {
    fenwickTree.update(i, value);
    fenwickTree.update(j + 1, -value);
  }

  const ans = new Array(n).fill(0);
  for (let i = 1; i <= n; i++) {
    // 每个区间的前缀和
    ans[i - 1] = fenwickTree.query(i);
  }
  return ans;
}
```
