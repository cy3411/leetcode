/*
 * @lc app=leetcode.cn id=1109 lang=typescript
 *
 * [1109] 航班预订统计
 */

// @lc code=start
class FenwickTree {
  data: number[];
  n: number;
  constructor(n: number) {
    this.n = n;
    this.data = new Array(n + 1).fill(0);
  }

  _lowbit(x: number): number {
    return x & -x;
  }

  update(i: number, v: number) {
    while (i <= this.n) {
      this.data[i] += v;
      i += this._lowbit(i);
    }
  }

  query(i: number) {
    let sum = 0;
    while (i > 0) {
      sum += this.data[i];
      i -= this._lowbit(i);
    }
    return sum;
  }
}
function corpFlightBookings(bookings: number[][], n: number): number[] {
  const fenwickTree = new FenwickTree(n);
  for (const [i, j, value] of bookings) {
    fenwickTree.update(i, value);
    fenwickTree.update(j + 1, -value);
  }

  const ans = new Array(n).fill(0);
  for (let i = 1; i <= n; i++) {
    ans[i - 1] = fenwickTree.query(i);
  }
  return ans;
}
// @lc code=end
