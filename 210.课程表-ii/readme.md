# 题目
现在你总共有 n 门课需要选，记为 `0` 到` n-1`。

在选修某些课程之前需要一些先修课程。 例如，想要学习课程 0 ，你需要先完成课程 1 ，我们用一个匹配来表示他们: `[0,1]`

给定课程总量以及它们的先决条件，返回你为了学完所有课程所安排的学习顺序。

可能会有多个正确的顺序，你只要返回一种就可以了。如果不可能完成所有课程，返回一个空数组。

# 示例
```
输入: 2, [[1,0]] 
输出: [0,1]
解释: 总共有 2 门课程。要学习课程 1，你需要先完成课程 0。因此，正确的课程顺序为 [0,1] 。
```
# 题解
## 拓扑排序
我们将学习课程顺序理解成一条有向边，那这里求的就是拓扑排序的结果。

### 广度优先
先建模，将入度为`0`的点入队。遍历队首所能到达的点，再次把入度`0`为点入队，直接队列清空。

每次出队的点就是我们拓扑排序的顺序。
```ts
function findOrder(numCourses: number, prerequisites: number[][]): number[] {
  const graph: number[][] = new Array(numCourses).fill(0).map(() => []);
  const degree: number[] = new Array(numCourses).fill(0);

  // 建模
  for (let [to, from] of prerequisites) {
    graph[from].push(to);
    degree[to]++;
  }

  const queue = [];
  for (let i = 0; i < numCourses; i++) {
    degree[i] === 0 && queue.push(i);
  }

  const result = [];
  while (queue.length) {
    let v = queue.shift();
    result.push(v);
    for (let w of graph[v]) {
      degree[w]--;
      degree[w] === 0 && queue.push(w);
    }
  }
  // 长度一致表示无环，输出拓扑排序结果，否则有环，输出空数组
  return result.length === numCourses ? result : [];
}
```