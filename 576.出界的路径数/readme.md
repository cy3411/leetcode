# 题目

给你一个大小为 m x n 的网格和一个球。球的起始坐标为 [startRow, startColumn] 。你可以将球移到在四个方向上相邻的单元格内（可以穿过网格边界到达网格之外）。你 最多 可以移动 maxMove 次球。

给你五个整数 m、n、maxMove、startRow 以及 startColumn ，找出并返回可以将球移出边界的路径数量。因为答案可能非常大，返回对 109 + 7 取余 后的结果。

提示：

- 1 <= m, n <= 50
- 0 <= maxMove <= 50
- 0 <= startRow < m
- 0 <= startColumn < n

# 示例

[![fgi1L8.md.png](https://z3.ax1x.com/2021/08/15/fgi1L8.md.png)](https://imgtu.com/i/fgi1L8)

```
输入：m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0
输出：6
```

# 题解

## 深度优先搜索

递归搜索不同路径，当越界时返回 1，当最大移动次数用完，返回 0。

由于重复路径很多，需要采用记忆化搜索。

```ts
function findPaths(
  m: number,
  n: number,
  maxMove: number,
  startRow: number,
  startColumn: number
): number {
  const dirs = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  let mod = 10 ** 9 + 7;
  let map: Map<string, number> = new Map();
  const dfs = (startRow: number, startColumn: number, maxMove: number) => {
    // 4个方向出界，找到1个结果
    if (startRow < 0 || startRow === m || startColumn < 0 || startColumn === n) {
      return 1;
    }
    // 最大移动次数为0，没有找到结果
    if (maxMove === 0) return 0;
    let ans = 0;
    // 记忆化存储的k，有3个变量
    let key = `${startRow}-${startColumn}-${maxMove}`;
    if (map.has(key)) return map.get(key);
    // 递归找其他4个方向
    for (let [x, y] of dirs) {
      let nx = startRow + x;
      let ny = startColumn + y;
      ans += dfs(nx, ny, maxMove - 1);
      ans %= mod;
    }
    map.set(key, ans);
    return ans;
  };

  return dfs(startRow, startColumn, maxMove);
}
```
