# 题目

给你一个有 `n` 个节点的 **有向无环图（DAG）**，请你找出所有从节点 `0` 到节点 `n-1` 的路径并输出（**不要求按特定顺序**）

二维数组的第 `i` 个数组中的单元都表示有向图中 `i` 号节点所能到达的下一些节点，空就是没有下一个结点了。

译者注：有向图是有方向的，即规定了 a→b 你就不能从 b→a 。

提示：

- `n == graph.length`
- `2 <= n <= 15`
- `0 <= graph[i][j] < n`
- `graph[i][j] != i`（即，不存在自环）
- `graph[i]` 中的所有元素 **互不相同**
- 保证输入为**有向无环图（DAG**）

# 示例

[![henLOf.png](https://z3.ax1x.com/2021/08/25/henLOf.png)](https://imgtu.com/i/henLOf)

```
输入：graph = [[1,2],[3],[3],[]]
输出：[[0,1,3],[0,2,3]]
解释：有两条路径 0 -> 1 -> 3 和 0 -> 2 -> 3
```

# 题解

## 回溯

遍历所有的可能的路径，自然就想到了回溯，深度搜索每条可行的路径，将满足条件的结果放入答案。

回溯回去，选择其他可行的路径。直到全部遍历完毕。

```ts
function allPathsSourceTarget(graph: number[][]): number[][] {
  const m = graph.length;
  // 已访问过的节点
  const visited = new Array(m).fill(0);
  const backtrack = (idx: number, path: number[] = []) => {
    visited[idx] = 1;
    path.push(idx);
    // 已经到达目标，更新答案，退出递归
    if (idx === m - 1) {
      ans.push([...path]);
      return;
    }
    // 遍历下一次选择
    for (const to of graph[idx]) {
      if (visited[to]) continue;
      backtrack(to, path);
      visited[to] = 0;
      path.pop();
    }
  };
  const ans: number[][] = [];
  backtrack(0);
  return ans;
}
```
