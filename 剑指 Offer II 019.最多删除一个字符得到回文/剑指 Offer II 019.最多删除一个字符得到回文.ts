// @algorithm @lc id=1000255 lang=typescript
// @title RQku0D
// @test("aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga")=true
function validPalindrome(s: string): boolean {
  const check = (s: string, l: number, r: number): boolean => {
    while (l < r) {
      if (s[l++] !== s[r--]) return false;
    }
    return true;
  };
  let l = 0;
  let r = s.length - 1;
  while (l < r) {
    if (s[l] === s[r]) {
      l++;
      r--;
    } else {
      // 有一个不相等，检查剩下的区间是否有回文
      return check(s, l + 1, r) || check(s, l, r - 1);
    }
  }
  return true;
}
