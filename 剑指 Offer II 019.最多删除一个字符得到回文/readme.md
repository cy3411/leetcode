# 题目

给定一个非空字符串 `s`，请判断如果 **最多** 从字符串中删除一个字符能否得到一个回文字符串。

提示:

- $1 \leq s.length \leq 10^5$
- `s` 由小写英文字母组成

注意：[本题与主站 680 题相同](https://leetcode-cn.com/problems/valid-palindrome-ii/)

# 示例

```
输入: s = "aba"
输出: true
```

```
输入: s = "abca"
输出: true
解释: 可以删除 "c" 字符 或者 "b" 字符
```

# 题解

## 双指针

定义双指针指向字符串的两端，向中间移动，判断是否为回文。

如果遇到一个不同的字符，则跳过这个字符，再判断是否为回文。

```ts
function validPalindrome(s: string): boolean {
  const check = (s: string, l: number, r: number): boolean => {
    while (l < r) {
      if (s[l++] !== s[r--]) return false;
    }
    return true;
  };
  let l = 0;
  let r = s.length - 1;
  while (l < r) {
    if (s[l] === s[r]) {
      l++;
      r--;
    } else {
      // 有一个不相等，检查剩下的区间是否有回文
      return check(s, l + 1, r) || check(s, l, r - 1);
    }
  }
  return true;
}
```

```cpp
class Solution
{
public:
    bool check(string &s, int l, int r)
    {
        while (l < r)
        {
            if (s[l] == s[r])
            {
                l++, r--;
            }
            else
            {
                return false;
            }
        }
        return true;
    }
    bool validPalindrome(string s)
    {
        int l = 0, r = s.size() - 1;
        while (l < r)
        {
            if (s[l] == s[r])
            {
                l++, r--;
            }
            else
            {
                return check(s, l + 1, r) || check(s, l, r - 1);
            }
        }
        return true;
    }
};
```
