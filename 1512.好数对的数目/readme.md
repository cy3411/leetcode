# 题目

给你一个整数数组 `nums` 。

如果一组数字 `(i,j)` 满足 `nums[i] == nums[j]` 且 `i < j` ，就可以认为这是一组 **好数对** 。

返回好数对的数目。

提示：

- $\color{GoldenRod} 1 \leq nums.length \leq 100$
- $\color{goldenrod}1 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [1,2,3,1,1,3]
输出：4
解释：有 4 组好数对，分别是 (0,3), (0,4), (3,4), (2,5) ，下标从 0 开始
```

```
输入：nums = [1,1,1,1]
输出：6
解释：数组中的每组数字都是好数对
```

# 题解

## 遍历

只需要判断当前元素之前出现过多少个相同的元素，就可以知道当前元素的好数对的数量。

```ts
function numIdenticalPairs(nums: number[]): number {
  // 每个数字出现的次数
  const hash = new Array(101).fill(0);
  let ans = 0;

  for (const num of nums) {
    // 每个数字都能和之前出现过的相同数字形成组合
    // 因此只需要计算出现过的数字的个数即可
    ans += hash[num];
    hash[num]++;
  }

  return ans;
}
```
