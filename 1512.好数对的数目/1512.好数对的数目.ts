/*
 * @lc app=leetcode.cn id=1512 lang=typescript
 *
 * [1512] 好数对的数目
 */

// @lc code=start
function numIdenticalPairs(nums: number[]): number {
  // 每个数字出现的次数
  const hash = new Array(101).fill(0);
  let ans = 0;

  for (const num of nums) {
    // 每个数字都能和之前出现过的相同数字形成组合
    // 因此只需要计算出现过的数字的个数即可
    ans += hash[num];
    hash[num]++;
  }

  return ans;
}
// @lc code=end
