/*
 * @lc app=leetcode.cn id=830 lang=javascript
 *
 * [830] 较大分组的位置
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number[][]}
 */
var largeGroupPositions = function (s) {
  const size = s.length;
  if (size < 3) {
    return [];
  }
  const result = [];
  let start = 0;

  for (let i = 1; i <= size; i++) {
    if (s[start] !== s[i]) {
      if (i - start > 2) {
        result.push([start, i - 1]);
      }
      start = i;
    }
  }

  return result;
};
// @lc code=end
