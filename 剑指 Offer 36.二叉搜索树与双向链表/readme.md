# 题目

输入一棵二叉搜索树，将该二叉搜索树转换成一个排序的循环双向链表。要求不能创建任何新的节点，只能调整树中节点指针的指向。

# 示例

为了让您更好地理解问题，以下面的二叉搜索树为例：

[![WzOnJO.md.png](https://z3.ax1x.com/2021/08/01/WzOnJO.md.png)](https://imgtu.com/i/WzOnJO)

我们希望将这个二叉搜索树转化为双向循环链表。链表中的每个节点都有一个前驱和后继指针。对于双向循环链表，第一个节点的前驱是最后一个节点，最后一个节点的后继是第一个节点。

下图展示了上面的二叉搜索树转化成的链表。“head” 表示指向链表中有最小元素的节点。

[![WzO4X9.md.png](https://z3.ax1x.com/2021/08/01/WzO4X9.md.png)](https://imgtu.com/i/WzO4X9)

特别地，我们希望可以就地完成转换操作。当转化完成以后，树中节点的左指针需要指向前驱，树中节点的右指针需要指向后继。还需要返回链表中的第一个节点的指针。

# 题解

## 链表操作

二叉搜索树的中序遍历结果，就是一个有序的结果。

定义 head 为链表的虚拟头节点，p 为当前链表的指针地址。

在中序遍历的过程中，调整节点的左右指针地址和 p 的指针地址。

遍历结束，p 的指针一定是最后一个节点，将 head.right 和 p 的节点相连，就是一个循环链表。

```js
/**
 * @param {Node} root
 * @return {Node}
 */
var treeToDoublyList = function (root) {
  // 树为空，链表也是空
  if (root === null) return null;
  // 中序遍历
  const inorder = (node) => {
    if (node === null) return;
    inorder(node.left);
    // 双向链表
    node.left = p;
    p.right = node;
    p = p.right;
    inorder(node.right);
  };
  // 虚拟头节点
  const head = new Node(null, null, null);
  // 当前头指针
  let p = head;
  inorder(root);
  // 头尾相连，循环链表
  head.right.left = p;
  p.right = head.right;
  return head.right;
};
```
