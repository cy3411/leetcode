/*
 * @lc app=leetcode.cn id=134 lang=javascript
 *
 * [134] 加油站
 */

// @lc code=start
/**
 * @param {number[]} gas
 * @param {number[]} cost
 * @return {number}
 */
var canCompleteCircuit = function (gas, cost) {
  const size = gas.length;
  let start = 0;
  let currSum = 0;
  let totalSum = 0;

  for (let i = 0; i < size; i++) {
    currSum += gas[i] - cost[i];
    totalSum += gas[i] - cost[i];
    if (currSum < 0) {
      start = i + 1;
      currSum = 0;
    }
  }
  // 总消耗大于0，表示可以走一圈
  if (totalSum >= 0) {
    return start;
  } else {
    return -1;
  }
};
// @lc code=end
