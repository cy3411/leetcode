// @algorithm @lc id=1000044 lang=typescript
// @title missing-two-lcci
// @test([1])=[2,3]
// @test([2,3])=[1,4]
function missingTwo(nums: number[]): number[] {
  let xor = 0;
  let n = nums.length + 2;
  for (const x of nums) {
    xor ^= x;
  }
  for (let i = 1; i <= n; i++) {
    xor ^= i;
  }

  let t0 = 0;
  let t1 = 0;
  const flag = xor & -xor;
  for (const x of nums) {
    if (flag & x) {
      t0 ^= x;
    } else {
      t1 ^= x;
    }
  }
  for (let i = 1; i <= n; i++) {
    if (flag & i) {
      t0 ^= i;
    } else {
      t1 ^= i;
    }
  }

  return [t0, t1];
}
