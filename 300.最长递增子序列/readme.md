# 题目

给定一个无序的整数数组，找到其中最长上升子序列的长度。

说明:

- 可能会有多种最长上升子序列的组合，你只需要输出对应的长度即可。
- 你算法的时间复杂度应该为 O(n2) 。

进阶: 你能将算法的时间复杂度降低到 O(n log n) 吗?

# 示例

```
输入: [10,9,2,5,3,7,101,18]
输出: 4
解释: 最长的上升子序列是 [2,3,7,101]，它的长度是 4。
```

# 方法

## 动态规划

定义 dp[i]是为 i 为结尾的最长路径.

如果 nums[i]大于它位置之前的某个数，那么 nums[i]就可以在这个数后面形成一个更长的递增子序列

状态转移方程式：`dp[i] = max(dp[i], 1 + dp[j] for j < i if nums[j] < nums[i])`

```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function (nums) {
  const size = nums.length;
  // 初始值为1，1个字符是长度为1的上升子序列
  const dp = new Array(size).fill(1);

  for (let i = 0; i < size; i++) {
    for (let j = 0; j < i; j++) {
      // 状态转移
      if (nums[j] < nums[i]) {
        dp[i] = Math.max(dp[i], dp[j] + 1);
      }
    }
  }
  // 返回最长的
  return Math.max(...dp);
};
```

## 二分查找

如果已经得到的上升子序列的结尾的数越小，那么遍历的时候后面接上一个数，会有更大的可能构成一个长度更长的上升子序列。

我们定义数组 result 保存最长序列，初始化的值是 nums[0]。

遍历 nums

- `nums[i]>result[result.length-1]`，表明这是一个递增了序列，直接放入 result
- 否则，需要找到 result 中第一个大于 nums[i]的值，替换成 nums[i]，这样就找到一个结尾更小的同长度的上升子序列

上升子序列是一个有序的数组，所以使用二分来执行替换操作。

```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function (nums) {
  const size = nums.length;
  // 单调递增的上升子序列
  const result = [nums[0]];

  for (let i = 1; i < size; i++) {
    if (nums[i] > result[result.length - 1]) {
      result.push(nums[i]);
    } else {
      // 二分查找，替换掉result中最接近并大于nums[i]的值
      let left = 0;
      let right = result.length - 1;
      while (left < right) {
        let mid = left + ((right - left) >> 1);
        if (result[mid] < nums[i]) {
          left = mid + 1;
        } else {
          right = mid;
        }
      }
      result[left] = nums[i];
    }
  }

  return result.length;
};
```

```ts
function lengthOfLIS(nums: number[]): number {
  const n = nums.length;
  const stack = [nums[0]];

  const biranySearch = (arr: number[], target: number) => {
    let l = 0;
    let r = arr.length;
    let mid: number;
    while (l < r) {
      mid = l + ((r - l) >> 1);
      if (arr[mid] >= target) r = mid;
      else l = mid + 1;
    }
    return l;
  };

  for (let i = 1; i < n; i++) {
    const num = nums[i];
    if (num > stack[stack.length - 1]) {
      stack.push(num);
    } else {
      const index = biranySearch(stack, num);
      stack[index] = num;
    }
  }
  return stack.length;
}
```
