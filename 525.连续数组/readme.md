# 题目

给定一个二进制数组 `nums` , 找到含有相同数量的 `0` 和 `1` 的最长连续子数组，并返回该子数组的长度。

# 示例

```
输入: nums = [0,1]
输出: 2
说明: [0, 1] 是具有相同数量0和1的最长连续子数组。
```

# 题解

## 前缀和

题意是找出含有相同数量的 `0` 和 `1` 的最长连续子数组，也就说 `1` 的数量减去 `0` 的数量刚好为 `0`.

我们可以将 `0` 改成 `-1`，然后计算前缀和数组，这样问题就转换成和为 `0` 的最长子数组长度

使用 hash 记录前一个元素的下标，遍历前缀和数组，如果值出现在hash中的话，更新子数组长度。

```ts
function findMaxLength(nums: number[]): number {
  let size = nums.length;
  if (size < 2) return 0;

  const prefixSum = new Array(size + 1).fill(0);
  for (let i = 1; i <= size; i++) {
    prefixSum[i] = prefixSum[i - 1] + (nums[i - 1] === 1 ? 1 : -1);
  }

  let result = 0;
  const hash: Map<number, number> = new Map();

  for (let i = 0; i <= size; i++) {
    if (hash.has(prefixSum[i])) {
      if (i - hash.get(prefixSum[i]) >= 2) {
        result = Math.max(result, i - hash.get(prefixSum[i]));
      }
    } else {
      hash.set(prefixSum[i], i);
    }
  }
  return result;
}
```

前缀和数组中只有加一减一的操作，我们可以采用滚动数组的形式，一边遍历数组，一边计算前缀和，同时更新结果。

```ts
function findMaxLength(nums: number[]): number {
  let size = nums.length;
  if (size < 2) return 0;

  let result = 0;
  let prefix = 0;
  const hash: Map<number, number> = new Map([[0, -1]]);

  for (let i = 0; i < size; i++) {
    const num = nums[i];
    if (num === 1) {
      prefix++;
    } else if (num === 0) {
      prefix--;
    }
    if (hash.has(prefix)) {
      result = Math.max(result, i - hash.get(prefix));
    } else {
      hash.set(prefix, i);
    }
  }
  return result;
}
```