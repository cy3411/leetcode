/*
 * @lc app=leetcode.cn id=658 lang=typescript
 *
 * [658] 找到 K 个最接近的元素
 */

// @lc code=start
function findClosestElements(arr: number[], k: number, x: number): number[] {
  const temp = [...arr];
  temp.sort((a: number, b: number) => {
    if (Math.abs(a - x) !== Math.abs(b - x)) {
      return Math.abs(a - x) - Math.abs(b - x);
    } else {
      return a - b;
    }
  });

  const ans = temp.slice(0, k);
  ans.sort((a: number, b: number) => {
    return a - b;
  });

  return ans;
}
// @lc code=end
