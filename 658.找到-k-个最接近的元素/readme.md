# 题目

给定一个 **排序好** 的数组 `arr` ，两个整数 `k` 和 `x` ，从数组中找到最靠近 `x`（两数之差最小）的 `k` 个数。返回的结果必须要是按升序排好的。

整数 `a` 比整数 `b` 更接近 `x` 需要满足：

- `|a - x| < |b - x|` 或者
- `|a - x| == |b - x|` 且 `a < b`

提示：

- $1 \leq k \leq arr.length$
- $1 \leq arr.length \leq 10^4$
- `arr` 按 **升序** 排列
- $-10^4 \leq arr[i], x \leq 10^4$

# 示例

```
输入：arr = [1,2,3,4,5], k = 4, x = 3
输出：[1,2,3,4]
```

```
输入：arr = [1,2,3,4,5], k = 4, x = -1
输出：[1,2,3,4]
```

# 题解

## 排序

将元素按照与 x 之间的绝对值差升序排序，k 个最接近 x 的元素就是排序结果中的前 k 个元素，将前 k 个元素排序后返回即可。

```js
function findClosestElements(arr: number[], k: number, x: number): number[] {
  const temp = [...arr];
  temp.sort((a: number, b: number) => {
    if (Math.abs(a - x) !== Math.abs(b - x)) {
      return Math.abs(a - x) - Math.abs(b - x);
    } else {
      return a - b;
    }
  });

  const ans = temp.slice(0, k);
  ans.sort((a: number, b: number) => {
    return a - b;
  });

  return ans;
}
```
