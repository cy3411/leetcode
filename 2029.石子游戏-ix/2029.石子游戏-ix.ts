/*
 * @lc app=leetcode.cn id=2029 lang=typescript
 *
 * [2029] 石子游戏 IX
 */

// @lc code=start
function stoneGameIX(stones: number[]): boolean {
  // 因为决胜条件是是否被3整除，这里只统计与3取模的次数
  const cnt = new Array(3).fill(0);
  for (const stone of stones) {
    cnt[stone % 3]++;
  }

  // 返回Alice的必胜条件
  return (
    (cnt[0] % 2 === 0 && cnt[1] >= 1 && cnt[2] >= 1) ||
    (cnt[0] % 2 === 1 && Math.abs(cnt[1] - cnt[2]) >= 3)
  );
}
// @lc code=end
