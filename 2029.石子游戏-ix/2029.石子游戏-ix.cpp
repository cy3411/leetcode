/*
 * @lc app=leetcode.cn id=2029 lang=cpp
 *
 * [2029] 石子游戏 IX
 */

// @lc code=start
class Solution
{
public:
    bool stoneGameIX(vector<int> &stones)
    {
        long long cnt[3] = {0};

        for (auto stone : stones)
        {
            cnt[stone % 3]++;
        }

        return (cnt[0] % 2 == 0 && cnt[1] * cnt[2] >= 1) || (cnt[0] % 2 == 1 && abs(cnt[1] - cnt[2]) >= 3);
    }
};
// @lc code=end
