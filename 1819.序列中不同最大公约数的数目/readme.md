# 题目

给你一个由正整数组成的数组 `nums` 。

数字序列的 最大公约数 定义为序列中所有整数的共有约数中的最大整数。

例如，序列 `[4,6,16]` 的最大公约数是 `2` 。
数组的一个 子序列 本质是一个序列，可以通过删除数组中的某些元素（或者不删除）得到。

例如，`[2,5,10]` 是 `[1,2,1,2,4,1,5,10]` 的一个子序列。
计算并返回 nums 的所有 **非空** 子序列中 不同 **最大公约数的** 数目 。

提示：

- $\color{burlywood}1 \leq nums.length \leq 10^5$
- $\color{burlywood}1 \leq nums[i] \leq 2 * 10^5$

# 示例

[![78JYCQ.png](https://s4.ax1x.com/2022/01/14/78JYCQ.png)](https://imgtu.com/i/78JYCQ)

```
输入：nums = [6,10,3]
输出：5
解释：上图显示了所有的非空子序列与各自的最大公约数。
不同的最大公约数为 6 、10 、3 、2 和 1 。
```

# 题解

## 穷举法

定义 `max` 为 `nums` 中最大的数字。穷举 `[1,max]` 的数字 `i`，判断是否可以在 `nums` 中找到最大公约数为 `i` 的子序列。

对 `nums` 预处理，去重然后使用哈希记录一下，后面遍历的时候可以跳过不需要的数字。

```ts
function countDifferentSubsequenceGCDs(nums: number[]): number {
  const gcd = (a: number, b: number) => (b === 0 ? a : gcd(b, a % b));
  const hash = new Map<number, number>();
  let max = 0;
  // 对nums数字去重并记录在hash中
  for (const num of nums) {
    max = Math.max(max, num);
    hash.set(num, 1);
  }

  let ans = 0;
  // 遍历，判断是否可以找到最大公约数是 i 的序列
  for (let i = 1; i <= max; i++) {
    let res = -1;
    // 对i的倍数且在hash中的数字求最大公约数
    for (let j = i; j <= max; j += i) {
      if (!hash.has(j)) continue;
      if (res === -1) {
        res = j;
      } else {
        res = gcd(res, j);
      }
    }
    // 结果等于 i，则说明可以找到最大公约数是 i 的序列
    if (res === i) ans++;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int countDifferentSubsequenceGCDs(vector<int> &nums)
    {
        int hash[200001] = {0}, maxNum = 0;
        for (int x : nums)
        {
            maxNum = max(maxNum, x);
            hash[x]++;
        }

        int ans = 0;
        for (int i = 1; i <= maxNum; i++)
        {
            int num = -1;
            for (int j = i; j <= maxNum; j += i)
            {
                // 如果hash[j]为0，说明j不在序列中，不用计算
                if (hash[j] == 0)
                {
                    continue;
                }
                // 计算序列里的最大公约数
                if (num == -1)
                {
                    num = j;
                }
                else
                {
                    num = gcd(num, j);
                }
            }

            if (num == i)
            {
                ans++;
            }
        }

        return ans;
    }
};
```
