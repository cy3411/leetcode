# 题目
在两条独立的水平线上按给定的顺序写下 `nums1` 和 `nums2` 中的整数。

现在，可以绘制一些连接两个数字 `nums1[i]` 和 `nums2[j]` 的直线，这些直线需要同时满足满足：

+ nums1[i] == nums2[j]
+ 且绘制的直线不与任何其他连线（非水平线）相交。

请注意，连线即使在端点也不能相交：**每个数字只能属于一条连线**。

以这种方法绘制线条，并返回可以绘制的最大连线数。

提示：
+ 1 <= nums1.length <= 500
+ 1 <= nums2.length <= 500
+ 1 <= nums1[i], nums2[i] <= 2000

# 示例
```
输入：nums1 = [1,4,2], nums2 = [1,2,4]
输出：2
解释：可以画出两条不交叉的线，如上图所示。 
但无法画出第三条不相交的直线，因为从 nums1[1]=4 到 nums2[2]=4 的直线将与从 nums1[2]=2 到 nums2[1]=2 的直线相交。
```

# 题解
## 动态规划
题目可以转换成求两个数组之间最长的公共子序列。

**状态**  
定义 `dp[i][j` 为 `nums1[0..i]` 和 `nums2[0,j]` 的最长公共子序列的个数。

**BaseCase**    
+ `dp[0][j] = 0`
+ `dp[i][0] = 0`

**选择**  
$$
 dp[i][j] \equiv 
 \begin{cases}
     dp[i-1][j-1] + 1,& nums[i-1] \equiv nums[j-1] \\
     max(dp[i][j-1], dp[i-1][j]), & nums[i-1] \neq nums[j-1]
 \end{cases}
$$


```ts
function maxUncrossedLines(nums1: number[], nums2: number[]): number {
  const m = nums1.length;
  const n = nums2.length;
  const dp = new Array(m + 1).fill(0).map(() => new Array(n + 1).fill(0));

  for (let i = 1; i <= m; i++) {
    for (let j = 1; j <= n; j++) {
      if (nums1[i - 1] === nums2[j - 1]) {
        dp[i][j] = dp[i - 1][j - 1] + 1;
      } else {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }

  return dp[m][n];
}
```
