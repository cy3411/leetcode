#
# @lc app=leetcode.cn id=901 lang=python3
#
# [901] 股票价格跨度
#

# @lc code=start


class StockSpanner:

    def __init__(self):
        self.stk = [(-1, inf)]
        self.index = -1

    def next(self, price: int) -> int:
        self.index += 1
        while price >= self.stk[-1][1]:
            self.stk.pop()

        self.stk.append((self.index, price))

        return self.index - self.stk[-2][0]


# Your StockSpanner object will be instantiated and called as such:
# obj = StockSpanner()
# param_1 = obj.next(price)
# @lc code=end
