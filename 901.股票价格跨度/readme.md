# 题目

编写一个 StockSpanner 类，它收集某些股票的每日报价，并返回该股票当日价格的跨度。

今天股票价格的跨度被定义为股票价格小于或等于今天价格的最大连续日数（从今天开始往回数，包括今天）。

例如，如果未来 7 天股票的价格是 [100, 80, 60, 70, 60, 75, 85]，那么股票跨度将是 [1, 1, 1, 2, 1, 4, 6]。

提示：

- 调用 StockSpanner.next(int price) 时，将有 1 <= price <= 10^5。
- 每个测试用例最多可以调用 10000 次 StockSpanner.next。
- 在所有测试用例中，最多调用 150000 次 StockSpanner.next。
- 此问题的总时间限制减少了 50%。

# 示例

```
输入：["StockSpanner","next","next","next","next","next","next","next"], [[],[100],[80],[60],[70],[60],[75],[85]]
输出：[null,1,1,1,2,1,4,6]
解释：
首先，初始化 S = StockSpanner()，然后：
S.next(100) 被调用并返回 1，
S.next(80) 被调用并返回 1，
S.next(60) 被调用并返回 1，
S.next(70) 被调用并返回 2，
S.next(60) 被调用并返回 1，
S.next(75) 被调用并返回 4，
S.next(85) 被调用并返回 6。

注意 (例如) S.next(75) 返回 4，因为截至今天的最后 4 个价格
(包括今天的价格 75) 小于或等于今天的价格。
```

# 题解

## 单调栈

利用单调递减栈，当 price 不满足单调性的时候，price 和栈顶元素之间的跨度就是答案。

```ts
class StockSpanner {
  index: number;
  stack: [number, number][];
  constructor() {
    this.index = 0;
    // 加入一个绝对大的元素在栈底，减少边界处理
    this.stack = [[Number.POSITIVE_INFINITY, this.index++]];
  }

  next(price: number): number {
    //  不满足单调性，需要弹出的元素都是满足答案的
    while (price >= this.stack[this.stack.length - 1][0]) {
      this.stack.pop();
    }
    // 当前元素和栈顶元素的区间就是答案
    let ans = this.index - this.stack[this.stack.length - 1][1];
    this.stack.push([price, this.index++]);
    return ans;
  }
}
```

```cpp
class StockSpanner {
public:
    StockSpanner() {
        stk.emplace(-1, __INT_MAX__);
        index = -1;
    }

    int next(int price) {
        index++;
        while (price >= stk.top().second) {
            stk.pop();
        }
        int ans = index - stk.top().first;
        stk.emplace(index, price);
        return ans;
    }

private:
    int index;
    stack<pair<int, int>> stk;
};
```

```py

class StockSpanner:

    def __init__(self):
        self.stk = [(-1, inf)]
        self.index = -1

    def next(self, price: int) -> int:
        self.index += 1
        while price >= self.stk[-1][1]:
            self.stk.pop()

        self.stk.append((self.index, price))

        return self.index - self.stk[-2][0]
```
