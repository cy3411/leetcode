/*
 * @lc app=leetcode.cn id=901 lang=cpp
 *
 * [901] 股票价格跨度
 */

// @lc code=start
class StockSpanner {
public:
    StockSpanner() {
        stk.emplace(-1, __INT_MAX__);
        index = -1;
    }

    int next(int price) {
        index++;
        while (price >= stk.top().second) {
            stk.pop();
        }
        int ans = index - stk.top().first;
        stk.emplace(index, price);
        return ans;
    }

private:
    int index;
    stack<pair<int, int>> stk;
};

/**
 * Your StockSpanner object will be instantiated and called as such:
 * StockSpanner* obj = new StockSpanner();
 * int param_1 = obj->next(price);
 */
// @lc code=end
