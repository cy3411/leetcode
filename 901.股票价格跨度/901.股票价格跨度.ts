/*
 * @lc app=leetcode.cn id=901 lang=typescript
 *
 * [901] 股票价格跨度
 */

// @lc code=start
class StockSpanner {
  index: number;
  stack: [number, number][];
  constructor() {
    this.index = 0;
    this.stack = [[Number.POSITIVE_INFINITY, this.index++]];
  }

  next(price: number): number {
    while (price >= this.stack[this.stack.length - 1][0]) {
      this.stack.pop();
    }
    // 当前元素和栈顶元素的区间就是答案
    let ans = this.index - this.stack[this.stack.length - 1][1];
    this.stack.push([price, this.index++]);
    return ans;
  }
}

/**
 * Your StockSpanner object will be instantiated and called as such:
 * var obj = new StockSpanner()
 * var param_1 = obj.next(price)
 */
// @lc code=end
