/*
 * @lc app=leetcode.cn id=376 lang=javascript
 *
 * [376] 摆动序列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var wiggleMaxLength = function (nums) {
  const size = nums.length;
  if (size < 2) {
    return size;
  }

  const up = new Array(size).fill(0);
  const down = new Array(size).fill(0);

  up[0] = down[0] = 1;

  for (let i = 1; i < size; i++) {
    if (nums[i] > nums[i - 1]) {
      up[i] = Math.max(down[i - 1] + 1, up[i - 1]);
      down[i] = down[i - 1];
    } else if (nums[i] < nums[i - 1]) {
      up[i] = up[i - 1];
      down[i] = Math.max(down[i - 1], up[i - 1] + 1);
    } else {
      up[i] = up[i - 1];
      down[i] = down[i - 1];
    }
  }

  return Math.max(up[size - 1], down[size - 1]);
};
// @lc code=end
