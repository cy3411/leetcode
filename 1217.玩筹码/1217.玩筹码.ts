/*
 * @lc app=leetcode.cn id=1217 lang=typescript
 *
 * [1217] 玩筹码
 */

// @lc code=start
function minCostToMoveChips(position: number[]): number {
  let odd = 0;
  let even = 0;

  for (const pos of position) {
    if (pos & 1) {
      odd++;
    } else {
      even++;
    }
  }

  return Math.min(odd, even);
}
// @lc code=end
