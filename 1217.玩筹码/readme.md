# 题目

有 `n` 个筹码。第 `i` 个筹码的位置是 `position[i]` 。

我们需要把所有筹码移到同一个位置。在一步中，我们可以将第 `i` 个筹码的位置从 `position[i]` 改变为:

- `position[i] + 2` 或 `position[i] - 2` ，此时 `cost = 0`
- `position[i] + 1` 或 `position[i] - 1` ，此时 `cost = 1`

返回将所有筹码移动到同一位置上所需要的 最小代价 。

提示：

- $1 \leq chips.length \leq 100$
- $1 \leq chips[i] \leq 10^9$

# 示例

[![jD8Nbd.png](https://s1.ax1x.com/2022/07/08/jD8Nbd.png)](https://imgtu.com/i/jD8Nbd)

```
输入：position = [1,2,3]
输出：1
解释：第一步:将位置3的筹码移动到位置1，成本为0。
第二步:将位置2的筹码移动到位置1，成本= 1。
总成本是1。
```

[![jD8dUI.png](https://s1.ax1x.com/2022/07/08/jD8dUI.png)](https://imgtu.com/i/jD8dUI)

```
输入：position = [2,2,2,3,3]
输出：2
解释：我们可以把位置3的两个筹码移到位置2。每一步的成本为1。总成本= 2。
```

# 题解

## 贪心

题目给出移动 2 个距离是没有消耗的，所以我们优先使用 0 消耗的操作来移动筹码。

通过上面的方式，可以将所有的筹码移动到 [1,2] 这 2 个位置上，然后只需要在移动一次即可。

最终只需要考虑偶数位和奇数位的筹码数量，取其中的最小值即可。

```ts
function minCostToMoveChips(position: number[]): number {
  let odd = 0;
  let even = 0;

  for (const pos of position) {
    if (pos & 1) {
      odd++;
    } else {
      even++;
    }
  }

  return Math.min(odd, even);
}
```

```cpp
class Solution {
public:
    int minCostToMoveChips(vector<int> &position) {
        int odd = 0, even = 0;

        for (auto pos : position) {
            if (pos & 1)
                odd++;
            else
                even++;
        }

        return min(odd, even);
    }
};
```

```py
class Solution:
    def minCostToMoveChips(self, position: List[int]) -> int:
        return min(odds := sum(p & 1 for p in position), len(position) - odds)
```
