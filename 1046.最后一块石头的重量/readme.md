# 题目
有一堆石头，每块石头的重量都是正整数。

每一回合，从中选出两块 **最重的** 石头，然后将它们一起粉碎。假设石头的重量分别为 `x` 和 `y`，且 `x <= y`。那么粉碎的可能结果如下：

+ 如果 `x == y`，那么两块石头都会被完全粉碎；
+ 如果 `x != y`，那么重量为 `x` 的石头将会完全粉碎，而重量为 `y` 的石头新重量为 `y-x`。
  
最后，最多只会剩下一块石头。返回此石头的重量。如果没有石头剩下，就返回 0。

# 示例
```
输入：[2,7,4,1,8,1]
输出：1
解释：
先选出 7 和 8，得到 1，所以数组转换为 [2,4,1,1,1]，
再选出 2 和 4，得到 2，所以数组转换为 [2,1,1,1]，
接着是 2 和 1，得到 1，所以数组转换为 [1,1,1]，
最后选出 1 和 1，得到 0，最终数组转换为 [1]，这就是最后剩下那块石头的重量。
```

# 题解
## 大顶堆
从集合中选取最值，自然就想到了堆。

建立大顶堆，每次取2个最重的，根据题意，不相等的时候将差值重新放入堆中。

当堆的尺寸小于2的时候，就终止循环。堆的结果就是答案。

```js
/**
 * @param {number[]} stones
 * @return {number}
 */
var lastStoneWeight = function (stones) {
  const maxPQ = new MaxPQ(stones);
  while (maxPQ.size() > 1) {
    let y = maxPQ.poll();
    let x = maxPQ.poll();
    if (x === y) continue;
    maxPQ.insert(y - x);
  }
  return maxPQ.size() > 0 ? maxPQ.peek() : 0;
};
```


```js
// 堆
class PQ {
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;

    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  less(i, j) {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }

  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }

  size() {
    return this.keys.length;
  }

  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }

  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }

  peek() {
    return this.keys[0];
  }
}
// 大顶堆
class MaxPQ extends PQ {}
// 小顶堆
class MinPQ extends PQ {
  constructor(keys, mapKeysValue = (x) => x) {
    super(keys, (x) => -1 * mapKeysValue(x));
  }
}
```