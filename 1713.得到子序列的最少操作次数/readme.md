# 题目

给你一个数组 target ，包含若干 互不相同 的整数，以及另一个整数数组 arr ，arr 可能 包含重复元素。

每一次操作中，你可以在 arr 的任意位置插入任一整数。比方说，如果 arr = [1,4,1,2] ，那么你可以在中间添加 3 得到 [1,4,3,1,2] 。你可以在数组最开始或最后面添加整数。

请你返回 最少 操作次数，使得 target 成为 arr 的一个子序列。

一个数组的 子序列 指的是删除原数组的某些元素（可能一个元素都不删除），同时不改变其余元素的相对顺序得到的数组。比方说，[2,7,4] 是 [4,2,3,7,2,1,4] 的子序列（加粗元素），但 [2,4,2] 不是子序列。

提示：

- 1 <= target.length, arr.length <= 105
- 1 <= target[i], arr[i] <= 109
- target 不包含任何重复元素。

# 示例

```
输入：target = [5,1,3], arr = [9,4,2,3,4]
输出：2
解释：你可以添加 5 和 1 ，使得 arr 变为 [5,9,4,1,2,3,4] ，target 为 arr 的子序列。
```

# 题解

## 哈希+二分搜索

题目要求找到最少操作次数，使 target 成为 arr 的一个子序列。这样问题可以转换成求 target 和 arr 的公共最长子序列 lcs。target 的长度减去 lcs 的长度就是答案。

题目给出 target 不包含重复的元素，我们可以使用哈希来记录 target 中的元素和下标的映射关系，并将 arr 中的元素也映射到哈希对应的下标中。

比如：target = [6,4,8,1,3,2], arr = [4,7,6,2,3,8,6,1]

arr 的映射关系就是[1,0,5,4,2,0,3],target 的映射关系就是[0,1,2,3,4,5]。

可以看出 target 的使单调递增序列，可以使用二分搜索，求两个映射关系的最长公共递增子序列即可。

```ts
function minOperations(target: number[], arr: number[]): number {
  // 找到lcs中第一个大于target的值
  const binarySearch = (lcs: number[], target: number): number => {
    const m = lcs.length;
    // 数组为空或者target大于数组中的所有值
    if (m === 0 || target > lcs[m - 1]) {
      return m;
    }
    let l = 0;
    let r = m - 1;
    let mid: number;
    while (l < r) {
      mid = l + ((r - l) >> 1);
      if (lcs[mid] < target) l = mid + 1;
      else r = mid;
    }
    return l;
  };
  // 记录数字在target中的下标
  const m = target.length;
  const hash: Map<number, number> = new Map();
  for (let i = 0; i < m; i++) {
    hash.set(target[i], i);
  }
  // 计算2个数组间最长公共子序列
  const lcs = [];
  for (const num of arr) {
    if (hash.has(num)) {
      const idx = hash.get(num);
      const pos = binarySearch(lcs, idx);
      if (pos === lcs.length) {
        lcs.push(idx);
      } else {
        lcs[pos] = idx;
      }
    }
  }
  return m - lcs.length;
}
```
