# 题目

给你一座由 `n x n` 个街区组成的城市，每个街区都包含一座立方体建筑。给你一个下标从 0 开始的 `n x n` 整数矩阵 `grid` ，其中 `grid[r][c]` 表示坐落于 `r` 行 `c` 列的建筑物的 高度 。

城市的 **天际线** 是从远处观察城市时，所有建筑物形成的外部轮廓。从东、南、西、北四个主要方向观测到的 天际线 可能不同。

我们被允许为 任意数量的建筑物 的高度增加 任意增量（不同建筑物的增量可能不同） 。 高度为 `0` 的建筑物的高度也可以增加。然而，增加的建筑物高度 不能影响 从任何主要方向观察城市得到的 **天际线** 。

在 **不改变** 从任何主要方向观测到的城市 天际线 的前提下，返回建筑物可以增加的 **最大高度增量总和** 。

提示：

- `n == grid.length`
- `n == grid[r].length`
- `2 <= n <= 50`
- `0 <= grid[r][c] <= 100`

# 示例

[![oX7o0e.md.png](https://s4.ax1x.com/2021/12/13/oX7o0e.md.png)](https://imgtu.com/i/oX7o0e)

```
输入：grid = [[3,0,8,4],[2,4,5,7],[9,2,6,3],[0,3,1,0]]
输出：35
解释：建筑物的高度如上图中心所示。
用红色绘制从不同方向观看得到的天际线。
在不影响天际线的情况下，增加建筑物的高度：
gridNew = [ [8, 4, 8, 7],
            [7, 4, 7, 7],
            [9, 4, 8, 7],
            [3, 3, 3, 3] ]
```

# 题解

## 贪心

从东西方向看，城市天际线等于每行房子的最大高度。

从南北方向看，城市天际线等于每列房子的最大高度。

定义 `rowMax` 为每行房子的最大高度，`colMax` 为每列房子的最大高度。

为了保持各个方向的天际线不变，房子增加的高度不能超过其所在行列的房子最大高度。

也就是 min(rowMax[i], colMax[j])。

其所能增加的最大高度增量为 `min(rowMax[i], colMax[j]) - grid[i][j]`。

```ts
function maxIncreaseKeepingSkyline(grid: number[][]): number {
  const n = grid.length;
  const rowMax = new Array(n).fill(0);
  const colMax = new Array(n).fill(0);

  // 记录每行、每列的最大值
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      rowMax[i] = Math.max(rowMax[i], grid[i][j]);
      colMax[j] = Math.max(colMax[j], grid[i][j]);
    }
  }

  let ans = 0;
  // 计算每个房子可以增加的最大值
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      // 可以增加的最大值 = 当前行列的最小高度 - 当前房子的高度
      ans += Math.min(rowMax[i], colMax[j]) - grid[i][j];
    }
  }

  return ans;
}
```
