# 题目

给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回该列名称对应的列序号。

提示：

- 1 <= columnTitle.length <= 7
- columnTitle 仅由大写英文组成
- columnTitle 在范围 ["A", "FXSHRXW"] 内

# 示例

```
输入: columnTitle = "A"
输出: 1
```

# 题解

## 数学进制

列名中出现的字符只有大写字母，这个显然就是一个 26 进制转换 10 进制的题目。

我们需要使用数组或者哈希来保存字母到对应数字的映射关系，剩下的就是遍历字符串，来计算结果。

```ts
function titleToNumber(columnTitle: string): number {
  const base = 'A'.charCodeAt(0);
  const decimal = 26;
  const map = new Array(decimal).fill(0).map((_, idx) => idx + 1);

  let ans = 0;
  let m = columnTitle.length - 1;
  // 26进制的计算
  for (let i = 0; i <= m; i++) {
    const idx = columnTitle.charCodeAt(i) - base;
    ans = ans * decimal + map[idx];
  }

  return ans;
}
```
