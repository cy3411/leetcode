/*
 * @lc app=leetcode.cn id=171 lang=typescript
 *
 * [171] Excel表列序号
 */

// @lc code=start
function titleToNumber(columnTitle: string): number {
  const base = 'A'.charCodeAt(0);
  const decimal = 26;
  const map = new Array(decimal).fill(0).map((_, idx) => idx + 1);

  let ans = 0;
  let m = columnTitle.length - 1;
  // 26进制的计算
  for (let i = 0; i <= m; i++) {
    const idx = columnTitle.charCodeAt(i) - base;
    ans = ans * decimal + map[idx];
  }

  return ans;
}
// @lc code=end
