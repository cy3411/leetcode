# 描述
给你一个字符串表达式 s ，请你实现一个基本计算器来计算并返回它的值。

整数除法仅保留整数部分。
# 示例
```
输入：s = "3+2*2"
输出：7
```
```
输入：s = " 3+5 / 2 "
输出：5
```
# 方法
由于乘除优先于加减计算，所以需要先计算乘除运算，再把运算结果放回表达式原来的位置，最后表达式的值就是一系列加减的结果
## 单栈
```js
var calculate = function (s) {
  const stack = [];
  let preSign = '+';
  let sum = 0;

  for (let i = 0; i < s.length; i++) {
    if (/\d/.test(s[i])) {
      sum = sum * 10 + Number(s[i]);
    }

    if (/[^\d\s]/.test(s[i]) || i === s.length - 1) {
      if (preSign === '+') {
        stack.push(sum);
      } else if (preSign === '-') {
        stack.push(-sum);
      } else if (preSign === '*') {
        stack.push(stack.pop() * sum);
      } else if (preSign === '/') {
        stack.push((stack.pop() / sum) >> 0);
      }
      preSign = s[i];
      sum = 0;
    }
  }

  let result = 0;
  for (let n of stack) {
    result += n;
  }

  return result;
};
```

## 双栈
numsStack保存字面量，opsStack保存操作符。可以计算加减乘除和括号运算。
```js
var calculate = function (s) {
  function calc(nums, ops) {
    if (nums.length < 2) return;
    if (ops.length === 0) return;

    let b = nums.pop();
    let a = nums.pop();
    let op = ops.pop();
    let result = 0;

    // console.log(a, b, op);

    if (op === '+') {
      result = a + b;
    } else if (op === '-') {
      result = a - b;
    } else if (op === '*') {
      result = a * b;
    } else if (op === '/') {
      result = (a / b) >> 0;
    }

    nums.push(result);
  }
  // 运算符优先级
  const priority = new Map();
  priority.set('+', 1);
  priority.set('-', 1);
  priority.set('*', 2);
  priority.set('/', 2);

  s = s.replace(/\s/g, '');
  // 处理括号内第一位是负数的情况
  s = s.replace('(-', '(0-');

  let n = s.length;
  // 加上0为了处理第一位是负数的情况
  const numsStack = [0];
  const opsStack = [];
  for (let i = 0; i < n; i++) {
    let char = s[i];
    if (char === '(') {
      opsStack.push(char);
    } else if (char === ')') {
      // 计算到上一个'('
      while (opsStack.length) {
        if (opsStack[opsStack.length - 1] !== '(') {
          calc(numsStack, opsStack);
        } else {
          opsStack.pop();
          break;
        }
      }
    } else {
      if (/\d/.test(char)) {
        let num = 0;
        let j = i;
        while (j < n && /\d/.test(s[j])) {
          num = num * 10 + Number(s[j++]);
        }
        numsStack.push(num);
        i = j - 1;
      } else {
        while (opsStack.length && opsStack[opsStack.length - 1] !== '(') {
          // 如果当前栈内的符号优先级高，先计算栈内元素
          let prev = opsStack[opsStack.length - 1];
          if (priority.get(prev) >= priority.get(char)) {
            calc(numsStack, opsStack);
          } else {
            break;
          }
        }
        opsStack.push(char);
      }
    }
  }

  while (opsStack.length) calc(numsStack, opsStack);
  return numsStack[numsStack.length - 1];
};
```
