# 题目

如果一个数列 至少有三个元素 ，并且任意两个相邻元素之差相同，则称该数列为等差数列。

- 例如，[1,3,5,7,9]、[7,7,7,7] 和 [3,-1,-5,-9] 都是等差数列。

你一个整数数组 nums ，返回数组 nums 中所有为等差数组的 子数组 个数。

子数组 是数组中的一个连续序列。

提示：

- 1 <= nums.length <= 5000
- -1000 <= nums[i] <= 1000

# 示例

```
输入：nums = [1,2,3,4]
输出：3
解释：nums 中有三个子等差数组：[1, 2, 3]、[2, 3, 4] 和 [1,2,3,4] 自身。
```

# 题解

## 计数

定义 diff 为初始的差分，然后遍历数组，计算$nums[i - 1] - nums[i]$是否等于 diff:

- 相同，计数加 1
- 不同，计数清 0，并且 diff 更新为新的差分

每次更新完计数，将结果累加到答案中去。

```ts
function numberOfArithmeticSlices(nums: number[]): number {
  // 题意要求数列长度最少为3，小于3的直接返回0
  if (nums.length < 3) return 0;
  let diff: number = nums[0] - nums[1];
  let count = 0;
  let ans = 0;
  for (let i = 2; i < nums.length; i++) {
    // 差分相同，计数增加
    if (nums[i - 1] - nums[i] === diff) {
      count++;
    } else {
      // 更新差分，计数清0
      diff = nums[i - 1] - nums[i];
      count = 0;
    }
    ans += count;
  }
  return ans;
}
```
