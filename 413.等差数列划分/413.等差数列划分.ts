/*
 * @lc app=leetcode.cn id=413 lang=typescript
 *
 * [413] 等差数列划分
 */

// @lc code=start
function numberOfArithmeticSlices(nums: number[]): number {
  // 题意要求数列长度最少为3，小于3的直接返回0
  if (nums.length < 3) return 0;
  let diff: number = nums[0] - nums[1];
  let count = 0;
  let ans = 0;
  for (let i = 2; i < nums.length; i++) {
    // 差分相同，计数增加
    if (nums[i - 1] - nums[i] === diff) {
      count++;
    } else {
      // 更新差分，计数清0
      diff = nums[i - 1] - nums[i];
      count = 0;
    }
    ans += count;
  }
  return ans;
}
// @lc code=end
