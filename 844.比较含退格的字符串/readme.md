# 题目
给定 S 和 T 两个字符串，当它们分别被输入到空白的文本编辑器后，判断二者是否相等，并返回结果。 # 代表退格字符。

注意：如果对空文本输入退格字符，文本继续为空。

提示：

+ 1 <= S.length <= 200
+ 1 <= T.length <= 200
+ S 和 T 只含有小写字母以及字符 '#'。

进阶：

+ 你可以用 O(N) 的时间复杂度和 O(1) 的空间复杂度解决该问题吗？

# 示例
```
输入：S = "ab#c", T = "ad#c"
输出：true
解释：S 和 T 都会变成 “ac”。
```

```
输入：S = "ab##", T = "c#d#"
输出：true
解释：S 和 T 都会变成 “”。
```

# 方法
## 栈实现
```js
/**
 * @param {string} S
 * @param {string} T
 * @return {boolean}
 */
var backspaceCompare = function (S, T) {
  const tranform = (s) => {
    // 使用栈保存字符
    const stack = [];
    for (let char of s) {
      if (char !== '#') {
        stack.push(char);
      } else {
        // 遇到#号就弹出栈顶元素
        stack.pop();
      }
    }
    return stack;
  };

  let stackS = tranform(S);
  let stackT = tranform(T);

  return stackS.join('') === stackT.join('');
};
```

## 双指针
题目给出了的进阶的要求，O(1)的空间复杂度，这里考虑使用双指针，从后往前扫描，遇见#号记录数量，
遇见字符判断#的是否为0：
+ 为0的话，去判断字符是否相等
+ 不为0的，继续往前扫描，#号数量减1，表示跳过这个字符

```js
/**
 * @param {string} S
 * @param {string} T
 * @return {boolean}
 */
var backspaceCompare = function (S, T) {
  let i = S.length - 1;
  let skipS = 0;
  let j = T.length - 1;
  let skipT = 0;

  while (i >= 0 || j >= 0) {
    // 找出S中#号消除后的第一个字符
    while (i >= 0) {
      if (S[i] === '#') {
        skipS++;
        i--;
      } else {
        if (skipS > 0) {
          skipS--;
          i--;
        } else {
          break;
        }
      }
    }
    // 找出T中#号消除后的第一个字符
    while (j >= 0) {
      if (T[j] === '#') {
        skipT++;
        j--;
      } else {
        if (skipT > 0) {
          skipT--;
          j--;
        } else {
          break;
        }
      }
    }

    // 比较找到的2个字符是否相同
    if (i >= 0 && j >= 0) {
      if (S[i] !== T[j]) return false;
    } else {
      // 如果有一个检索到头了，另一个还有字符没比较
      if (i >= 0 || j >= 0) return false;
    }

    i--;
    j--;
  }

  return true;
};
```