/*
 * @lc app=leetcode.cn id=896 lang=javascript
 *
 * [896] 单调数列
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {boolean}
 */
var isMonotonic = function (A) {
  const n = A.length;
  let asc = true;
  let dec = true;

  for (let i = 0; i < n - 1; i++) {
    if (A[i] < A[i + 1]) {
      dec = false;
    }
    if (A[i] > A[i + 1]) {
      asc = false;
    }
  }
  return dec || asc;
};
// @lc code=end
