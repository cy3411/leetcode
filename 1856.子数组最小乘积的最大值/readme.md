# 题目

一个数组的 最小乘积 定义为这个数组中 最小值 乘以 数组的 和 。

- 比方说，数组 [3,2,5] （最小值是 2）的最小乘积为 2 _ (3+2+5) = 2 _ 10 = 20 。

给你一个正整数数组 nums ，请你返回 nums 任意 非空子数组 的最小乘积 的 最大值 。由于答案可能很大，请你返回答案对 109 + 7 取余 的结果。

请注意，最小乘积的最大值考虑的是取余操作 之前 的结果。题目保证最小乘积的最大值在 不取余 的情况下可以用 64 位有符号整数 保存。

子数组 定义为一个数组的 连续 部分。

提示：

- 1 <= nums.length <= 105
- 1 <= nums[i] <= 107

# 示例

```
输入：nums = [2,3,3,1,2]
输出：18
解释：最小乘积的最大值由子数组 [3,3] （最小值是 3）得到。
3 * (3+3) = 3 * 6 = 18 。
```

# 题解

## 单调栈+前缀和

利用单调递增栈的特性，找到每一个元素左边和右边第一个小于它的位置，这样就可以确定当前元素为最小值的子数组区间。

最后统计每个元素的最小乘积，取其中的最大值。

子数组的和可以使用前缀和数组来快速计算。

```ts
function maxSumMinProduct(nums: number[]): bigint {
  const m = nums.length;
  const l: number[] = new Array(m).fill(-1);
  const r: number[] = new Array(m).fill(m);
  const stack: number[] = [];
  // 计算当前元素左边最近小于它的和右边最近小于它的
  for (let i = 0; i < m; i++) {
    while (stack.length && nums[i] <= nums[stack[stack.length - 1]]) {
      r[stack.pop()] = i;
    }
    if (stack.length) l[i] = stack[stack.length - 1];
    stack.push(i);
  }
  // 前缀和，方便计算区间和
  const sum: number[] = new Array(m + 1).fill(0);
  for (let i = 1; i <= m; i++) {
    sum[i] = nums[i - 1] + sum[i - 1];
  }

  let ans: bigint = BigInt(0);
  const mod = BigInt(1e9 + 7);
  // 计算每个元素的最小乘积，取其中的最大值
  for (let i = 0; i < m; i++) {
    let total = BigInt(nums[i]) * BigInt(sum[r[i]] - sum[l[i] + 1]);
    if (total > ans) ans = total;
  }

  return ans % mod;
}
```
