# 题目

超级丑数 是一个正整数，并满足其所有质因数都出现在质数数组 primes 中。

给你一个整数 n 和一个整数数组 primes ，返回第 n 个 超级丑数 。

题目数据保证第 n 个 超级丑数 在 32-bit 带符号整数范围内。

提示：

- 1 <= n <= 106
- 1 <= primes.length <= 100
- 2 <= primes[i] <= 1000
- 题目数据 保证 primes[i] 是一个质数
- primes 中的所有值都 互不相同 ，且按 递增顺序 排列

# 示例

```
输入：n = 12, primes = [2,7,13,19]
输出：32
解释：给定长度为 4 的质数数组 primes = [2,7,13,19]，前 12 个超级丑数序列为：[1,2,4,7,8,13,14,16,19,26,28,32] 。
```

```
输入：n = 1, primes = [2,3,5]
输出：1
解释：1 不含质因数，因此它的所有质因数都在质数数组 primes = [2,3,5] 中。
```

# 题解

## 小顶堆

定义最小超级丑数为 x, 那么 x 与 primes 中的元素乘积将生成新的超级丑数。

定义小顶堆 minPQ，将最小超级丑数 1 放入堆中。

每次取出堆顶元素，并将新的超级丑数放入堆中，直到 n 的次数用完。

考虑到计算新的超级丑数会出现重复情况，可以使用哈希来保存出现过的状态，来判断是否让如堆中。

```ts
class MinPQ {
  heap: number[];
  size: number;
  constructor() {
    this.heap = [];
    this.size = 0;
  }
  less(i: number, j: number): boolean {
    const diff = this.heap[i] - this.heap[j];
    return diff < 0 ? true : false;
  }
  swap(i: number, j: number) {
    [this.heap[i], this.heap[j]] = [this.heap[j], this.heap[i]];
  }
  sink(k: number) {
    let idx = k * 2 + 1;
    while (idx < this.size) {
      let j = idx;
      if (j + 1 < this.size && this.less(j + 1, j)) {
        j++;
      }
      if (this.less(k, j)) {
        return;
      }
      this.swap(k, j);
      k = j;
      idx = j * 2 + 1;
    }
  }
  swin(k: number) {
    let idx = (k - 1) >> 1;
    while (idx >= 0 && this.less(k, idx)) {
      this.swap(k, idx);
      k = idx;
      idx = (idx - 1) >> 1;
    }
  }
  insert(key: number) {
    this.heap.push(key);
    this.swin(this.size);
    this.size++;
  }
  popMin() {
    const min = this.heap[0];
    this.swap(0, this.size - 1);
    this.heap.pop();
    this.size--;
    this.sink(0);
    return min;
  }
}

function nthSuperUglyNumber(n: number, primes: number[]): number {
  // 小顶堆
  const minPQ = new MinPQ();
  // 保存数字是否出现过
  const visited: Set<number> = new Set();
  minPQ.insert(1);
  visited.add(1);

  let ans: number = 1;
  // 循环n次，取出前n个最小值
  while (n--) {
    const min = minPQ.popMin();
    ans = min;
    // 最小值与primse数组元素生成新的丑数放入小顶堆
    for (const prime of primes) {
      let next = min * prime;
      // 如果出现过的丑数，就跳过
      if (visited.has(next)) continue;
      minPQ.insert(next);
      visited.add(next);
    }
  }

  return ans;
}
```

## 动态规划

**状态**

定义 dp 数组，表示当前位置的元素的最小丑数

**Base Case**

最小丑数为 1，$dp[i] = 1$

**转换**

$dp[i] = min({dp[p[j]]} \times{ primes[j]})$

p 数组保存的是 dp 中最小丑数的位置，表示 primes 对应位置的元素可以与哪个最小丑数计算得到新的最小丑数。

```ts
function nthSuperUglyNumber(n: number, primes: number[]): number {
  const m = primes.length;
  // 超级丑数
  const dp = new Array(n + 1).fill(1);
  // p位置的当前丑数和质因数的积，得到下一个超级丑数
  const p = new Array(m).fill(1);

  for (let i = 2; i <= n; i++) {
    const next: number[] = new Array(m).fill(0);
    let min = Number.POSITIVE_INFINITY;
    for (let j = 0; j < m; j++) {
      next[j] = dp[p[j]] * primes[j];
      min = Math.min(min, next[j]);
    }
    dp[i] = min;
    // 更新p指针
    for (let j = 0; j < m; j++) {
      if (next[j] === min) p[j]++;
    }
  }
  return dp[n];
}
```
