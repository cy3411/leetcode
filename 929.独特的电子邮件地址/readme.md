# 题目

每个 **有效电子邮件地址** 都由一个 **本地名** 和一个 **域名** 组成，以 `'@'` 符号分隔。除小写字母之外，电子邮件地址还可以含有一个或多个 `'.'` 或 `'+'` 。

- 例如，在 `alice@leetcode.com` 中， `alice` 是 本地名 ，而 `leetcode.com` 是 域名 。

如果在电子邮件地址的 本地名 部分中的某些字符之间添加句点（`'.'`），则发往那里的邮件将会转发到本地名中没有点的同一地址。请注意，此规则 **不适用于域名** 。

- 例如，"`alice.z@leetcode.com`” 和 “`alicez@leetcode.com`” 会转发到同一电子邮件地址。

如果在 本地名 中添加加号（`'+'`），则会忽略第一个加号后面的所有内容。这允许过滤某些电子邮件。同样，此规则 **不适用于域名** 。

- 例如 `m.y+name@email.com` 将转发到 `my@email.com`。

可以同时使用这两个规则。

给你一个字符串数组 `emails`，我们会向每个 `emails[i]` 发送一封电子邮件。返回实际收到邮件的不同地址数目。

提示：

- $1 \leq emails.length \leq 100$
- $1 \leq emails[i].length \leq 100$
- `emails[i]` 由小写英文字母、`'+'`、`'.'` 和 `'@'` 组成
- 每个 `emails[i]` 都包含有且仅有一个 `'@'` 字符
- 所有本地名和域名都不为空
- 本地名不会以 `'+'` 字符作为开头

# 示例

```
输入：emails = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
输出：2
解释：实际收到邮件的是 "testemail@leetcode.com" 和 "testemail@lee.tcode.com"。
```

```
输入：emails = ["a@leetcode.com","b@leetcode.com","c@leetcode.com"]
输出：3
```

# 题解

## 哈希表

遍历 emails , 按照题意过滤每一个 email 地址：

- 如果本地名中包含 `'.'`， 忽略字符 `'.'`
- 如果本地名中包含 `'+'`， 忽略 `'+'` 和 它后面的所有字符

最后返回不同的 email 地址的数量。

```ts
function numUniqueEmails(emails: string[]): number {
  const rewriteEmails = (email: string) => {
    const [local, domain] = email.split('@');
    let newLocal = '';
    for (let c of local) {
      // . 忽略
      if (c === '.') {
        continue;
      }
      // + 后面的字符忽略
      if (c === '+') {
        break;
      }
      newLocal += c;
    }
    return `${newLocal}&${domain}`;
  };

  // 使用哈希set过滤重复字符串
  const set = new Set<string>();
  for (const email of emails) {
    set.add(rewriteEmails(email));
  }

  return set.size;
}
```

```cpp
class Solution {
public:
    int numUniqueEmails(vector<string> &emails) {
        unordered_set<string> setEmail;
        for (auto email : emails) {
            string local;
            for (auto c : email) {
                if (c == '@' || c == '+') break;
                if (c != '.') local.push_back(c);
            }
            setEmail.insert(local + email.substr(email.find('@')));
        }
        return setEmail.size();
    }
};
```

```py
class Solution:
    def numUniqueEmails(self, emails: List[str]) -> int:
        emailSet = set()
        for email in emails:
            idx = email.find('@')
            local = email[:idx].split('+')[0].replace('.', '')
            emailSet.add(local + email[idx:])
        return len(emailSet)
```
