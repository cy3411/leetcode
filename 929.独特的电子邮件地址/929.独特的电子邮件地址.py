#
# @lc app=leetcode.cn id=929 lang=python3
#
# [929] 独特的电子邮件地址
#

# @lc code=start
class Solution:
    def numUniqueEmails(self, emails: List[str]) -> int:
        emailSet = set()
        for email in emails:
            idx = email.find('@')
            local = email[:idx].split('+')[0].replace('.', '')
            emailSet.add(local + email[idx:])
        return len(emailSet)

# @lc code=end
