/*
 * @lc app=leetcode.cn id=929 lang=typescript
 *
 * [929] 独特的电子邮件地址
 */

// @lc code=start
function numUniqueEmails(emails: string[]): number {
  const rewriteEmails = (email: string) => {
    const [local, domain] = email.split('@');
    let newLocal = '';
    for (let c of local) {
      // . 忽略
      if (c === '.') {
        continue;
      }
      // + 后面的字符忽略
      if (c === '+') {
        break;
      }
      newLocal += c;
    }
    return `${newLocal}&${domain}`;
  };

  // 使用哈希set过滤重复字符串
  const set = new Set<string>();
  for (const email of emails) {
    set.add(rewriteEmails(email));
  }

  return set.size;
}
// @lc code=end
