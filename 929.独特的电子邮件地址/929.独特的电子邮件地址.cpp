/*
 * @lc app=leetcode.cn id=929 lang=cpp
 *
 * [929] 独特的电子邮件地址
 */

// @lc code=start
class Solution {
public:
    int numUniqueEmails(vector<string> &emails) {
        unordered_set<string> setEmail;
        for (auto email : emails) {
            string local;
            for (auto c : email) {
                if (c == '@' || c == '+') break;
                if (c != '.') local.push_back(c);
            }
            setEmail.insert(local + email.substr(email.find('@')));
        }
        return setEmail.size();
    }
};
// @lc code=end
