# 题目

给你一个 `m x n` 的整数网格 `accounts` ，其中 `accounts[i][j]` 是第 `i`​​​​​​​​​​​​ 位客户在第 `j` 家银行托管的资产数量。返回最富有客户所拥有的 资产总量 。

客户的 **资产总量** 就是他们在各家银行托管的资产数量之和。最富有客户就是 **资产总量** 最大的客户。

提示：

- $m \equiv accounts.length$
- $n \equiv accounts[i].length$
- $1 \leq m, n \leq 50$
- $1 \leq accounts[i][j] \leq 100$

# 示例

```
输入：accounts = [[1,2,3],[3,2,1]]
输出：6
解释：
第 1 位客户的资产总量 = 1 + 2 + 3 = 6
第 2 位客户的资产总量 = 3 + 2 + 1 = 6
两位客户都是最富有的，资产总量都是 6 ，所以返回 6 。
```

```
输入：accounts = [[1,5],[7,3],[3,5]]
输出：10
解释：
第 1 位客户的资产总量 = 6
第 2 位客户的资产总量 = 10
第 3 位客户的资产总量 = 8
第 2 位客户是最富有的，资产总量是 10
```

# 题解

## 遍历

分别计算每位客户在各家银行托管的资产数量之和，返回这些资产总量的最大值

```ts
function maximumWealth(accounts: number[][]): number {
  const m = accounts.length;
  let ans = 0;
  for (let i = 0; i < m; i++) {
    const sum = accounts[i].reduce((a, b) => a + b, 0);
    ans = Math.max(ans, sum);
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int maximumWealth(vector<vector<int>> &accounts)
    {
        int m = accounts.size();
        int n = accounts[0].size();
        int ans = 0;
        for (int i = 0; i < m; i++)
        {
            int sum = 0;
            for (int j = 0; j < n; j++)
            {
                sum += accounts[i][j];
            }
            ans = max(ans, sum);
        }
        return ans;
    }
};
```
