/*
 * @lc app=leetcode.cn id=1672 lang=typescript
 *
 * [1672] 最富有客户的资产总量
 */

// @lc code=start
function maximumWealth(accounts: number[][]): number {
  const m = accounts.length;
  let ans = 0;
  for (let i = 0; i < m; i++) {
    const sum = accounts[i].reduce((a, b) => a + b, 0);
    ans = Math.max(ans, sum);
  }
  return ans;
}
// @lc code=end
