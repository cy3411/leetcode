/*
 * @lc app=leetcode.cn id=1122 lang=typescript
 *
 * [1122] 数组的相对排序
 */

// @lc code=start
function relativeSortArray(arr1: number[], arr2: number[]): number[] {
  const hash = new Array(1001).fill(0);
  // 统计元素出现的次数
  for (let num of arr1) {
    hash[num] += 1;
  }

  let idx = -1;
  // 按照顺序将元素放入数组
  for (let num of arr2) {
    while (hash[num]--) {
      arr1[++idx] = num;
    }
  }
  // 将未出现在arr2中元素放入
  for (let i = 0; i < hash.length; i++) {
    if (hash[i] <= 0) continue;
    while (hash[i]--) {
      arr1[++idx] = i;
    }
  }

  return arr1;
}
// @lc code=end
