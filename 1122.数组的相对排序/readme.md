# 题目
给你两个数组，`arr1` 和 `arr2`，

`arr2` 中的元素各不相同
`arr2` 中的每个元素都出现在 `arr1` 中
对 `arr1` 中的元素进行排序，使 `arr1` 中项的相对顺序和 `arr2` 中的相对顺序相同。未在 `arr2` 中出现过的元素需要按照升序放在 `arr1` 的末尾。

提示：
+ 1 <= arr1.length, arr2.length <= 1000
+ 0 <= arr1[i], arr2[i] <= 1000
+ arr2 中的元素 arr2[i] 各不相同
+ arr2 中的每个元素 arr2[i] 都出现在 arr1 中

# 示例
```
输入：arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
输出：[2,2,2,1,4,3,3,9,6,7,19]
```

# 题解
## 计数排序
统计 `arr1` 中元素出现的次数，把 `arr2` 当作计数排序后的数组进行排序即可。
```ts
function relativeSortArray(arr1: number[], arr2: number[]): number[] {
  const hash = new Array(1001).fill(0);
  // 统计元素出现的次数
  for (let num of arr1) {
    hash[num] += 1;
  }

  let idx = -1;
  // 按照顺序将元素放入数组
  for (let num of arr2) {
    while (hash[num]--) {
      arr1[++idx] = num;
    }
  }
  // 将未出现在arr2中元素放入
  for (let i = 0; i < hash.length; i++) {
    if (hash[i] <= 0) continue;
    while (hash[i]--) {
      arr1[++idx] = i;
    }
  }

  return arr1;
}
```