/*
 * @lc app=leetcode.cn id=598 lang=typescript
 *
 * [598] 范围求和 II
 */

// @lc code=start
function maxCount(m: number, n: number, ops: number[][]): number {
  let minx = m;
  let miny = n;
  // 最大值一定在最左上角
  for (const op of ops) {
    minx = Math.min(minx, op[0]);
    miny = Math.min(miny, op[1]);
  }

  return minx * miny;
}
// @lc code=end
