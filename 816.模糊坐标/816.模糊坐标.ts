/*
 * @lc app=leetcode.cn id=816 lang=typescript
 *
 * [816] 模糊坐标
 */

// @lc code=start
function getPos(s: string): string[] {
  const pos: string[] = [];
  const n = s.length;
  // 前导0的字符串无效
  if (s === '0' || s[0] !== '0') {
    pos.push(s);
  }
  // 添加小数点
  for (let i = 1; i < n; i++) {
    if (s[n - 1] === '0' || (i !== 1 && s[0] === '0')) {
      continue;
    }
    pos.push(`${s.substring(0, i)}.${s.substring(i)}`);
  }
  return pos;
}

function ambiguousCoordinates(s: string): string[] {
  const n = s.length - 2;
  // 将数字提前出来
  s = s.substring(1, n + 1);
  const ans: string[] = [];
  // 枚举每个位置，将字符串分割为两个部分，然后将对应字符串添加符合条件的小数点
  for (let i = 1; i < n; i++) {
    const ls = getPos(s.substring(0, i));
    if (ls.length === 0) continue;
    const rs = getPos(s.substring(i));
    if (rs.length === 0) continue;
    for (const l of ls) {
      for (const r of rs) {
        ans.push(`(${l}, ${r})`);
      }
    }
  }
  return ans;
}
// @lc code=end
