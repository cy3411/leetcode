# 题目

我们有一些二维坐标，如 "(1, 3)" 或 "(2, 0.5)"，然后我们移除所有逗号，小数点和空格，得到一个字符串 S。返回所有可能的原始字符串到一个列表中。

原始的坐标表示法不会存在多余的零，所以不会出现类似于"00", "0.0", "0.00", "1.0", "001", "00.01"或一些其他更小的数来表示坐标。此外，一个小数点前至少存在一个数，所以也不会出现“.1”形式的数字。

最后返回的列表可以是任意顺序的。而且注意返回的两个数字中间（逗号之后）都有一个空格。

提示:

- $4 \leq S.length \leq 12$
- S[0] = "(", S[S.length - 1] = ")", 且字符串 S 中的其他元素都是数字

# 示例

```
示例 1:
输入: "(123)"
输出: ["(1, 23)", "(12, 3)", "(1.2, 3)", "(1, 2.3)"]
```

```
示例 2:
输入: "(00011)"
输出:  ["(0.001, 1)", "(0, 0.011)"]
解释:
0.0, 00, 0001 或 00.01 是不被允许的。
```

# 题解

## 枚举

枚举分割点，将 s 分成左右两个字串。然后将每个子串分别添加合法的小数点，然后对所有合法的集合合并结果。

合法状态：

- 添加或者不添加小数点后的整数部分需要为 0 或者不含有前导零的整数
- 添加小数点后，其结尾的数字不能为 0

```tsx
function getPos(s: string): string[] {
  const pos: string[] = [];
  const n = s.length;
  // 前导0的字符串无效
  if (s === '0' || s[0] !== '0') {
    pos.push(s);
  }
  // 添加小数点
  for (let i = 1; i < n; i++) {
    // 避免1.0或者01.1这样的情况
    if (s[n - 1] === '0' || (i !== 1 && s[0] === '0')) {
      continue;
    }
    pos.push(`${s.substring(0, i)}.${s.substring(i)}`);
  }
  return pos;
}

function ambiguousCoordinates(s: string): string[] {
  const n = s.length - 2;
  // 将数字提前出来
  s = s.substring(1, n + 1);
  const ans: string[] = [];
  // 枚举每个位置，将字符串分割为两个部分，然后将对应字符串添加符合条件的小数点
  for (let i = 1; i < n; i++) {
    const ls = getPos(s.substring(0, i));
    if (ls.length === 0) continue;
    const rs = getPos(s.substring(i));
    if (rs.length === 0) continue;
    for (const l of ls) {
      for (const r of rs) {
        ans.push(`(${l}, ${r})`);
      }
    }
  }
  return ans;
}
```
