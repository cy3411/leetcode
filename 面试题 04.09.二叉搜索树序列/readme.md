# 题目

从左向右遍历一个数组，通过不断将其中的元素插入树中可以逐步地生成一棵二叉搜索树。给定一个由不同节点组成的二叉搜索树，输出所有可能生成此树的数组。

# 示例

```
给定如下二叉树
     2
    / \
    1   3
返回：
[
   [2,1,3],
   [2,3,1]
]
```

# 题解

## 回溯

每个根节点必须要先插入，左右子节点插入的顺序不用考虑。

```ts
function mergeSeq(l, lIdx, r, rIdx, buff, ans) {
  // 如果左右子树的序列都选择过了，表示一个组合完成，将结果放入答案中
  if (lIdx === l.length && rIdx === r.length) {
    ans.push([...buff]);
    return ans;
  }
  // 左边取一个放入组合中，递归
  if (lIdx < l.length) {
    buff.push(l[lIdx]);
    mergeSeq(l, lIdx + 1, r, rIdx, buff, ans);
    buff.pop();
  }
  // 右边取一个放入组合中，递归
  if (rIdx < r.length) {
    buff.push(r[rIdx]);
    mergeSeq(l, lIdx, r, rIdx + 1, buff, ans);
    buff.pop();
  }
}
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var BSTSequences = function (root) {
  const ans = [];
  if (root === null) {
    ans.push([]);
    return ans;
  }
  // 左右字数的序列集合
  const lSeq = BSTSequences(root.left);
  const rSeq = BSTSequences(root.right);

  for (const l of lSeq) {
    for (const r of rSeq) {
      const buff = [root.val];
      // 将左右子树的序列做一次排列组合，放入结果
      mergeSeq(l, 0, r, 0, buff, ans);
    }
  }

  return ans;
};
```
