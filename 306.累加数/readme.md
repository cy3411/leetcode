# 题目

**累加数** 是一个字符串，组成它的数字可以形成累加序列。

一个有效的 **累加序列** 必须 **至少** 包含 `3` 个数。除了最开始的两个数以外，字符串中的其他数都等于它之前两个数相加的和。

给你一个只包含数字 `'0'-'9'` 的字符串，编写一个算法来判断给定输入是否是 **累加数** 。如果是，返回 `true` ；否则，返回 `false` 。

说明：累加序列里的数，除数字 `0` 之外，不会 以 `0` 开头，所以不会出现 `1, 2, 03` 或者 `1, 02, 3` 的情况。

提示：

- $\color{burlywood}1 \leq num.length \leq 35$
- `num` 仅由数字`（0 - 9）`组成

进阶：你计划如何处理由过大的整数输入导致的溢出?

# 示例

```
输入："112358"
输出：true
解释：累加序列为: 1, 1, 2, 3, 5, 8 。1 + 1 = 2, 1 + 2 = 3, 2 + 3 = 5, 3 + 5 = 8
```

```
输入："199100199"
输出：true
解释：累加序列为: 1, 99, 100, 199。1 + 99 = 100, 99 + 100 = 199
```

# 题解

## 穷举

令 `[secondStart,secondEnd]` 为第二个数字的下标范围，那么可以得到第一个数字的下标范围为 `[0,secondStart-1] `，第三个数的范围为 `[secondEnd+1,secondEnd + third.length]` 。

我们枚举第二个数字的范围，然后通过上面条件得到另外两个数字是否满足累加条件。满足的话，就将数字后移，当第三个数到达末尾时，就可以返回 `true`。

```ts
function isAdditiveNumber(num: string): boolean {
  const n = num.length;

  // 验证区间是否合法
  const isValid = (secondStart: number, secondEnd: number, num: string): boolean => {
    const n = num.length;
    let firstStart = 0;
    let firstEnd = secondStart - 1;
    while (secondEnd <= n - 1) {
      const third = buildString(firstStart, firstEnd, secondStart, secondEnd, num);
      const thirdStart = secondEnd + 1;
      const thirdEnd = secondEnd + third.length;
      if (thirdEnd >= n) break;

      if (num.slice(thirdStart, thirdEnd + 1) !== third) break;
      // 遍历到最后一个数字，是合法序列
      if (thirdEnd === n - 1) return true;
      firstStart = secondStart;
      firstEnd = secondEnd;
      secondStart = thirdStart;
      secondEnd = thirdEnd;
    }
    return false;
  };

  // 根据前2个数的范围，构建第3个数
  // 为了避免大数溢出，将数字按位放入数组，最后合并成字符串
  const buildString = (
    firstStart: number,
    firstEnd: number,
    secondStart: number,
    secondEnd: number,
    num: string
  ): string => {
    const third = [];
    let carry = 0;
    let current = 0;
    while (firstStart <= firstEnd || secondStart <= secondEnd || carry !== 0) {
      current = carry;
      if (firstStart <= firstEnd) {
        current += Number(num[firstEnd]);
        firstEnd--;
      }
      if (secondStart <= secondEnd) {
        current += Number(num[secondEnd]);
        secondEnd--;
      }
      carry = (current / 10) >> 0;
      current %= 10;
      third.push(String(current));
    }

    return third.reverse().join('');
  };

  // 枚举第二个数的范围[secondStart, secondEnd]
  for (let secondStart = 1; secondStart < n - 1; secondStart++) {
    // 处理前缀0的情况
    if (num[0] === '0' && secondStart !== 1) break;
    for (let secondEnd = secondStart; secondEnd < n - 1; secondEnd++) {
      // 处理前缀0的情况
      if (num[secondStart] === '0' && secondEnd !== secondStart) break;
      if (isValid(secondStart, secondEnd, num)) return true;
    }
  }

  return false;
}
```
