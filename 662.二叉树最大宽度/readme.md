# 题目

给定一个二叉树，编写一个函数来获取这个树的最大宽度。树的宽度是所有层中的最大宽度。这个二叉树与满二叉树（full binary tree）结构相同，但一些节点为空。

每一层的宽度被定义为两个端点（该层最左和最右的非空节点，两端点间的 null 节点也计入长度）之间的长度。

注意: 答案在 32 位有符号整数的表示范围内。

# 示例

```
输入:

           1
         /   \
        3     2
       / \     \
      5   3     9

输出: 4
解释: 最大值出现在树的第 3 层，宽度为 4 (5,3,null,9)。
```

```
输入:

          1
         /
        3
       / \
      5   3

输出: 2
解释: 最大值出现在树的第 3 层，宽度为 2 (5,3)。
```

# 题解

## 广度优先搜索

我们可以将二叉树看成是数组的存储方式，记 root 的索引值为 i，左子树的索引就是`2*i`，右子树的索引是`2*i+1`。

通过广度优先搜索，队列中的第一个就是最左边的节点(L)，最后一个是最右边的节点(R)，`R-L+1`就是我们需要的结果。

有一点要注意的就是数量的增长级是 2^N 次方，所以计算索引的时候要处理一下，防止结果溢出。

```js
/**
 * @param {TreeNode} root
 * @return {number}
 */
var widthOfBinaryTree = function (root) {
  // 使用完全二叉树的数组存储方式来查询每层的节点数量
  // 逐层扫描节点计算最大和最小索引
  if (root === null) return 0;
  // 队列列存储当前索引和节点
  const queue = [[1, root]];
  let result = 1;

  while (queue.length) {
    let size = queue.length;
    // 更新结果(每层结束的索引-开始的索引+1)
    result = Math.max(result, queue[queue.length - 1][0] - queue[0][0] + 1);
    // 位移值，防止结果溢出
    // 也可以用bigint类型
    let offset = queue[queue.length - 1][0];
    // 更新队列节点
    while (size-- > 0) {
      let [idx, node] = queue.shift();
      if (node.left) queue.push([idx * 2 - offset, node.left]);
      if (node.right) queue.push([idx * 2 + 1 - offset, node.right]);
    }
  }

  return result;
};
```

```js
interface Data {
  idx: number;
  node: TreeNode;
}

function widthOfBinaryTree(root: TreeNode | null): number {
  const queue: Data[] = new Array();
  queue.push({ idx: 1, node: root });
  let ans = 1;
  while (queue.length) {
    let n = queue.length;
    ans = Math.max(ans, queue[n - 1].idx - queue[0].idx + 1);
    const offset = queue[n - 1].idx;
    while (n--) {
      const { idx, node } = queue.shift()!;
      if (node.left) queue.push({ idx: idx * 2 - offset, node: node.left });
      if (node.right) queue.push({ idx: idx * 2 + 1 - offset, node: node.right });
    }
  }
  return ans;
}
```
