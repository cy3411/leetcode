// @algorithm @lc id=100092 lang=typescript
// @title deep-dark-fraction

// @test([],1),result: [1,0],expect: [1,0]
function fraction(cont: number[]): number[] {
  const gcd = (a: number, b: number) => (b === 0 ? a : gcd(b, a % b));

  const n = cont.length;
  // 分母
  let x = 1;
  // 分子
  let y = 0;
  for (let i = n - 1; i >= 0; i--) {
    [x, y] = [y, x];
    x += y * cont[i];
    const g = gcd(x, y);
    x /= g;
    y /= g;
  }

  return [x, y];
}
