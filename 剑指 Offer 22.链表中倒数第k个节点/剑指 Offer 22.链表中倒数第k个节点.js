// @algorithm @lc id=100294 lang=javascript
// @title lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var getKthFromEnd = function (head, k) {
  let p = head;
  let n = 0;
  // 计算从节点数
  while (p) {
    p = p.next;
    n++;
  }
  // 取倒数k个节点
  p = head;
  while (n > k) {
    p = p.next;
    n--;
  }

  return p;
};
