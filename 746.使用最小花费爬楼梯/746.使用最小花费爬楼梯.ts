/*
 * @lc app=leetcode.cn id=746 lang=typescript
 *
 * [746] 使用最小花费爬楼梯
 */

// @lc code=start
function minCostClimbingStairs(cost: number[]): number {
  let n = cost.length;
  const dp = new Array(n + 1).fill(0);
  // 初始化
  dp[0] = cost[0];
  dp[1] = cost[1];
  // 当作楼顶
  cost.push(0);
  for (let i = 2; i <= n; i++) {
    // 决策，每次取最小花费加上当前的消费
    dp[i] = cost[i] + Math.min(dp[i - 1], dp[i - 2]);
  }

  return dp[n];
}
// @lc code=end
