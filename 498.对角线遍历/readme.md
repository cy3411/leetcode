# 题目

给你一个大小为 `m x n` 的矩阵 `mat` ，请以对角线遍历的顺序，用一个数组返回这个矩阵中的所有元素。

提示：

- $m \equiv mat.length$
- $n \equiv mat[i].length$
- $1 \leq m, n \leq 10^4$
- $1 \leq m * n \leq 10^4$
- $-10^5 \leq mat[i][j] \leq 10^5$

# 示例

[![XI2SIJ.png](https://s1.ax1x.com/2022/06/15/XI2SIJ.png)](https://imgtu.com/i/XI2SIJ)

```
输入：mat = [[1,2,3],[4,5,6],[7,8,9]]
输出：[1,2,4,7,5,3,6,8,9]
```

# 题解

## 模拟

设 mat 行数为 m， 列数为 n， 观察可以发现，一共有 $m+n-1$ 条对角线。

每次遍历一条对角线 i ：

- 当 i 为偶数时，从左下向右上遍历，每次行索引减 1，列索引加 1
  - 当 i < m 时，起点位置为 (i, 0)
  - 当 i >= m 时，起点位置为 (m-1, i-m+1)
- 当 i 为奇数时，从右上向左下遍历，每次行索引加 1，列索引减 1
  - 当 i < n 时，起点位置为 (i, 0)
  - 当 i >= n 时，起点位置为 (i-n+1, n-1)

```ts
function findDiagonalOrder(mat: number[][]): number[] {
  const m = mat.length;
  const n = mat[0].length;
  const ans = new Array(m * n);
  // 对角线数量
  const diagonals = m + n - 1;
  // 结果数组的索引
  let idx = 0;
  for (let i = 0; i < diagonals; i++) {
    if (i % 2 === 0) {
      // 对角线为偶数时，左上往右上遍历
      let x = i < m ? i : m - 1;
      let y = i < m ? 0 : i - m + 1;
      while (x >= 0 && y < n) {
        ans[idx++] = mat[x--][y++];
      }
    } else {
      // 对角线为奇数时，右上往左下遍历
      let x = i < n ? 0 : i - n + 1;
      let y = i < n ? i : n - 1;
      while (x < m && y >= 0) {
        ans[idx++] = mat[x++][y--];
      }
    }
  }

  return ans;
}
```
