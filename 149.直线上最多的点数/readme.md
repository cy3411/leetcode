# 题目

给你一个数组 points ，其中 points[i] = [xi, yi] 表示 X-Y 平面上的一个点。求最多有多少个点在同一条直线上。

# 示例

[![RGiMbq.png](https://z3.ax1x.com/2021/06/26/RGiMbq.png)](https://imgtu.com/i/RGiMbq)

```
输入：points = [[1,1],[2,2],[3,3]]
输出：3
```

# 题解

## 哈希表

迭代每个坐标点，将能与当前坐标点得到同一个公式结果的存入哈希表中，最后统计哈希表中的最多数量的就是结果。

```ts
function maxPoints(points: number[][]): number {
  if (points.length === 1) return 1;

  points.sort((a, b) => {
    if (a[0] !== b[0]) return a[0] - b[0];
    return a[1] - b[1];
  });

  const hash: Map<string, number> = new Map();
  const m = (x1: number, y1: number, x2: number, y2: number): { t: number; c: number } => {
    // 数学公式
    let t = (y2 - y1) / (x2 - x1);
    let c = (x2 * y1 - x1 * y2) / (x2 - x1);

    return { t, c };
  };

  for (let i = 0; i < points.length; i++) {
    let [x1, y1] = points[i];
    for (let j = i + 1; j < points.length; j++) {
      let [x2, y2] = points[j];
      let { t, c } = m(x1, y1, x2, y2);
      const key = `${x1}:${y1}:${t}:${c}`;
      hash.set(key, (hash.get(key) || 1) + 1);
    }
  }

  return Math.max(...hash.values());
}
```
