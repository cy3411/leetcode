# 题目

给你一个由若干括号和字母组成的字符串 `s` ，删除最小数量的无效括号，使得输入的字符串有效。

返回所有可能的结果。答案可以按 **任意顺序** 返回。

提示：

- `1 <= s.length <= 25`
- `s` 由小写英文字母以及括号 `'('` 和 `')'` 组成
- `s` 中至多含 `20` 个括号

# 示例

```
输入：s = "()())()"
输出：["(())()","()()()"]
```

```
输入：s = "(a)())()"
输出：["(a())()","(a)()()"]
```

# 题解

## 广度优先搜索

题目要求删除最小数量的无效括号，我们可以使用广度优先搜索，每次将删除 1 个括号的字符串当作下一次遍历的状态。如果有符合题意的结果，就直接跳出循环返回。

为了避免重复检测，可以利用哈希对结果去重，提高效率。

```ts
function removeInvalidParentheses(s: string): string[] {
  // 验证字符串是否是合法字符串
  const isValid = (str: string): boolean => {
    // 左括号数量
    let lcount = 0;
    for (let char of str) {
      if (char === '(') {
        lcount++;
      } else if (char === ')') {
        if (lcount === 0) return false;
        lcount--;
      }
    }
    return lcount === 0;
  };

  const ans: string[] = [];
  let currSet: Set<string> = new Set();
  currSet.add(s);
  while (true) {
    for (let str of currSet) {
      if (isValid(str)) ans.push(str);
    }
    // 找到答案，直接返回结果
    if (ans.length) return ans;

    // 去掉一个括号，进行下一次搜索
    const nextSet: Set<string> = new Set();
    for (let str of currSet) {
      for (let i = 0; i < str.length; i++) {
        // 连续相同的字符，只要处理一次即可
        if (i > 0 && str[i] === str[i - 1]) continue;
        if (str[i] === '(' || str[i] === ')') {
          // 增加下一次的搜索状态
          nextSet.add(str.substring(0, i) + str.substring(i + 1));
        }
      }
    }
    currSet = nextSet;
  }
}
```
