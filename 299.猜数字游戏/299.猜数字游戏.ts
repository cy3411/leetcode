/*
 * @lc app=leetcode.cn id=299 lang=typescript
 *
 * [299] 猜数字游戏
 */

// @lc code=start
function getHint(secret: string, guess: string): string {
  const hash = new Map<string, number>();
  let bulls = 0;
  let cows = 0;
  // 计算bulls，并记录每个数字出现的次数
  for (let i = 0; i < secret.length; i++) {
    if (secret[i] === guess[i]) {
      bulls++;
    } else {
      if (hash.has(secret[i])) {
        hash.set(secret[i], hash.get(secret[i]) + 1);
      } else {
        hash.set(secret[i], 1);
      }
    }
  }
  // 计算cows
  for (let i = 0; i < guess.length; i++) {
    if (guess[i] === secret[i]) {
      continue;
    }
    if (hash.has(guess[i]) && hash.get(guess[i]) > 0) {
      cows++;
      hash.set(guess[i], hash.get(guess[i]) - 1);
    }
  }

  return `${bulls}A${cows}B`;
}
// @lc code=end
