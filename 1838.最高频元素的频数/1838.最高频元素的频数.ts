/*
 * @lc app=leetcode.cn id=1838 lang=typescript
 *
 * [1838] 最高频元素的频数
 */

// @lc code=start
function maxFrequency(nums: number[], k: number): number {
  nums.sort((a, b) => a - b);
  let sum = 0;
  let ans = 1;
  let l = 0;
  for (let r = 1; r < nums.length; r++) {
    // 统计前一位数字累加成后一位数字需要累加几次
    sum += (nums[r] - nums[r - 1]) * (r - l);
    // 累加总次数超过限制，左边界移动，同时减去窗口外的数字
    while (sum > k) {
      sum -= nums[r] - nums[l];
      l++;
    }
    ans = Math.max(ans, r - l + 1);
  }

  return ans;
}
// @lc code=end
