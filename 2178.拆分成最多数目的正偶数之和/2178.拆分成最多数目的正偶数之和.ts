/*
 * @lc app=leetcode.cn id=2178 lang=typescript
 *
 * [2178] 拆分成最多数目的正偶数之和
 */

// @lc code=start
function maximumEvenSplit(finalSum: number): number[] {
  // 奇数无法拆分
  if (finalSum % 2 === 1) return [];

  const ans: number[] = [];
  let i = 2;
  while (i <= finalSum) {
    ans.push(i);
    finalSum -= i;
    i += 2;
  }
  // 如果最后剩下的数字不够拆分，直接加在答案数组的最后一个结果上
  if (finalSum > 0) {
    ans[ans.length - 1] += finalSum;
  }
  return ans;
}
// @lc code=end
