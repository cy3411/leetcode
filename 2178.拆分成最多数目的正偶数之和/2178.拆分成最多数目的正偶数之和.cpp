/*
 * @lc app=leetcode.cn id=2178 lang=cpp
 *
 * [2178] 拆分成最多数目的正偶数之和
 */

// @lc code=start
class Solution
{
public:
    vector<long long> maximumEvenSplit(long long finalSum)
    {
        vector<long long> ans;
        if (finalSum % 2 == 1)
            return ans;

        for (int i = 2; i <= finalSum; i += 2)
        {
            ans.push_back(i);
            finalSum -= i;
        }

        ans[ans.size() - 1] += finalSum;

        return ans;
    }
};
// @lc code=end
