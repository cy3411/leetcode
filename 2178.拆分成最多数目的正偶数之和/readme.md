# 题目

给你一个整数 `finalSum` 。请你将它拆分成若干个 **互不相同** 的正偶数之和，且拆分出来的正偶数数目 **最多** 。

比方说，给你 `finalSum = 12` ，那么这些拆分是 **符合要求** 的（互不相同的正偶数且和为 `finalSum`）：`(2 + 10)` ，`(2 + 4 + 6)` 和 `(4 + 8)` 。它们中，`(2 + 4 + 6)` 包含最多数目的整数。注意 `finalSum` 不能拆分成 `(2 + 2 + 4 + 4)` ，因为拆分出来的整数必须互不相同。
请你返回一个整数数组，表示将整数拆分成 **最多** 数目的正偶数数组。如果没有办法将 `finalSum` 进行拆分，请你返回一个 `空` 数组。你可以按 **任意** 顺序返回这些整数。

提示：

- $1 \leq finalSum \leq 10^10$

# 示例

```
输入：finalSum = 12
输出：[2,4,6]
解释：以下是一些符合要求的拆分：(2 + 10)，(2 + 4 + 6) 和 (4 + 8) 。
(2 + 4 + 6) 为最多数目的整数，数目为 3 ，所以我们返回 [2,4,6] 。
[2,6,4] ，[6,2,4] 等等也都是可行的解。
```

```
输入：finalSum = 7
输出：[]
解释：没有办法将 finalSum 进行拆分。
所以返回空数组。
```

```
输入：finalSum = 28
输出：[6,8,2,12]
解释：以下是一些符合要求的拆分：(2 + 26)，(6 + 8 + 2 + 12) 和 (4 + 24) 。
(6 + 8 + 2 + 12) 有最多数目的整数，数目为 4 ，所以我们返回 [6,8,2,12] 。
[10,2,4,12] ，[6,2,4,16] 等等也都是可行的解。
```

# 题解

## 贪心

要想获得更多的正偶数，我们就要尽量选择较小的正偶数来拆分。

选择从最小的正偶数 2 开始拆分，每次拆分的正偶数都是 2 的倍数，知道拆分的正偶数大于 finalSum 为止。

如果最后无法拆分，但 finalSum 还有剩余，就将结果累加到结果数组的最后一个元素上即可。

```ts
function maximumEvenSplit(finalSum: number): number[] {
  // 奇数无法拆分
  if (finalSum % 2 === 1) return [];

  const ans: number[] = [];
  let i = 2;
  while (i <= finalSum) {
    ans.push(i);
    finalSum -= i;
    i += 2;
  }
  // 如果最后剩下的数字不够拆分，直接加在答案数组的最后一个结果上
  if (finalSum > 0) {
    ans[ans.length - 1] += finalSum;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    vector<long long> maximumEvenSplit(long long finalSum)
    {
        vector<long long> ans;
        if (finalSum % 2 == 1)
            return ans;

        for (int i = 2; i <= finalSum; i += 2)
        {
            ans.push_back(i);
            finalSum -= i;
        }

        ans[ans.size() - 1] += finalSum;

        return ans;
    }
};
```
