#
# @lc app=leetcode.cn id=151 lang=python3
#
# [151] 翻转字符串里的单词
#

# @lc code=start
class Solution:
    def reverseWords(self, s: str) -> str:
        arr = s.strip().split(' ')
        arr.reverse()

        while '' in arr:
            arr.remove('')
        
        return ' '.join(arr)
# @lc code=end

