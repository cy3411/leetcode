# 题目

给定一个由 0 和 1 组成的数组 arr ，将数组分成 3 个非空的部分 ，使得所有这些部分表示相同的二进制值。

如果可以做到，请返回任何 [i, j]，其中 i+1 < j，这样一来：

- arr[0], arr[1], ..., arr[i] 为第一部分；
- arr[i + 1], arr[i + 2], ..., arr[j - 1] 为第二部分；
- arr[j], arr[j + 1], ..., arr[arr.length - 1] 为第三部分。
- 这三个部分所表示的二进制值相等。

如果无法做到，就返回 [-1, -1]。

注意，在考虑每个部分所表示的二进制时，应当将其看作一个整体。例如，[1,1,0] 表示十进制中的 6，而不会是 3。此外，前导零也是被允许的，所以 [0,1,1] 和 [1,1] 表示相同的值。

提示：

- $3 \leq arr.length \leq 3 * 10^4$
- `arr[i]` 是 `0` 或 `1`

# 示例

```
输入：arr = [1,0,1,0,1]
输出：[0,3]
```

```
输入：arr = [1,1,0,1,1]
输出：[-1,-1]
```

# 题解

## 等分 1 的数量

如果三等分二进制串，那么三个非空部分 1 的数量一定是相等的。

先统计 1 的数量，假设为 sum，如果不能被 3 整除，那么直接返回 [-1,-1]，表示不能被等分。

如果 sum 为 0， 那么随便返回一个区间即可。

否则，每一部分 1 的数量为 $part = \frac{sum}{3}$。

我们先找到第一个 1 出现的位置 p1 ，第 $part + 1$ 个 1 出现的位置是 p2 ，第 $2 * part + 1$ 个 1 出现的位置是 p3。

那么 [p3, arr.length - 1] 就确定了二进制串的长度。

假设 $size = arr.length - p3$ 为二进制的长度。

接下来，只需要验证 $[p1, p1+size)，[p2, p2+size)和[p3, p3 + size)$ 区间的字符串是否相等即可。

```js
function threeEqualParts(arr: number[]): number[] {
  const sum = arr.reduce((pre, cur) => pre + cur);
  if (sum % 3 !== 0) return [-1, -1];
  if (sum === 0) return [0, 2];

  const part = sum / 3;
  const n = arr.length;
  // 第一区间的开始位置
  let p1 = 0;
  // 第二区间的开始位置
  let p2 = 0;
  // 第三区间的开始位置
  let p3 = 0;
  let cur = 0;
  for (let i = 0; i < n; i++) {
    if (arr[i] === 1) {
      if (cur === 0) {
        p1 = i;
      } else if (cur === part) {
        p2 = i;
      } else if (cur === 2 * part) {
        p3 = i;
      }
      cur++;
    }
  }

  // 二进制串的长度
  let size = n - p3;
  if (p1 + size <= p2 && p2 + size <= p3) {
    let i = 0;
    while (p3 + i < n) {
      if (arr[p1 + i] !== arr[p3 + i] || arr[p2 + i] !== arr[p3 + i]) {
        return [-1, -1];
      }
      i++;
    }
    return [p1 + size - 1, p2 + size];
  }

  return [-1, -1];
}
```
