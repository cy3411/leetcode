# 题目

给定一个不重复的整数数组 `nums` 。 最大二叉树 可以用下面的算法从 `nums` 递归地构建:

- 创建一个根节点，其值为 `nums` 中的最大值。
- 递归地在最大值 **左边** 的 **子数组前缀上** 构建左子树。
- 递归地在最大值 **右边** 的 **子数组后缀上** 构建右子树。

返回 nums 构建的 最大二叉树 。

提示：

- $1 \leq nums.length \leq 1000$
- $0 \leq nums[i] \leq 1000$
- `nums` 中的所有整数 互不相同

# 示例

```
输入：nums = [3,2,1,6,0,5]
输出：[6,3,5,null,2,0,null,null,1]
解释：递归调用如下所示：
- [3,2,1,6,0,5] 中的最大值是 6 ，左边部分是 [3,2,1] ，右边部分是 [0,5] 。
    - [3,2,1] 中的最大值是 3 ，左边部分是 [] ，右边部分是 [2,1] 。
        - 空数组，无子节点。
        - [2,1] 中的最大值是 2 ，左边部分是 [] ，右边部分是 [1] 。
            - 空数组，无子节点。
            - 只有一个元素，所以子节点是一个值为 1 的节点。
    - [0,5] 中的最大值是 5 ，左边部分是 [0] ，右边部分是 [] 。
        - 只有一个元素，所以子节点是一个值为 0 的节点。
        - 空数组，无子节点。
```

# 题解

## 递归

找到数组中的最大值，创建一个节点，然后缩减区间递归的去构建它的左右子树。

```js
function constructMaximumBinaryTree(nums: number[]): TreeNode | null {
  const construct = (nums: number[], start: number, end: number) => {
    if (start > end) return null;
    let maxIndex = start;
    for (let i = start + 1; i <= end; i++) {
      if (nums[i] > nums[maxIndex]) {
        maxIndex = i;
      }
    }
    const node = new TreeNode(nums[maxIndex]);
    node.left = construct(nums, start, maxIndex - 1);
    node.right = construct(nums, maxIndex + 1, end);
    return node;
  };

  return construct(nums, 0, nums.length - 1);
}
```
