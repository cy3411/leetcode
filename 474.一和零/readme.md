# 题目

给你一个二进制字符串数组 `strs` 和两个整数 `m` 和 `n` 。

请你找出并返回 `strs` 的最大子集的大小，该子集中 最多 有 `m` 个 0 和 `n` 个 1 。

如果 `x` 的所有元素也是 `y` 的元素，集合 `x` 是集合 `y` 的 子集 。

# 示例

```
输入：strs = ["10", "0001", "111001", "1", "0"], m = 5, n = 3
输出：4
解释：最多有 5 个 0 和 3 个 1 的最大子集是 {"10","0001","1","0"} ，因此答案是 4 。
其他满足题意但较小的子集包括 {"0001","1"} 和 {"10","1","0"} 。{"111001"} 不满足题意，因为它含 4 个 1 ，大于 n 的值 3 。
```

# 题解

## 动态规划

**状态**

定义 dp[i][j][k] ，表示前 `i` 个字符中使用 `j` 个 `0` 和 `k` 个 `1` 的情况下有多少种选择

**BaseCase**

当没有任何字符串的情况下，能得到的选择数量智能为 0.

$dp[0][j][k]=0,0<=j<=m 和 0<=k<=n$

**选择**

$$
dp[i][j][k] =
\begin{cases}
dp[i-1][j][k] & \text{j<zeros | k <= ones} \\
max(dp[i][j][k], dp[i-1][j-zeros][k-ones]+1) & \text{j>=zeros \& k>=ones}
\end{cases}
$$

```ts
function findMaxForm(strs: string[], m: number, n: number): number {
  // 字段字符串中0和1个个数
  // 返回结果:[0的个数，1的个数]
  const getZerosOnes = (str: string): number[] => {
    const result = new Array(2).fill(0);
    for (const char of str) {
      result[Number(char)]++;
    }
    return result;
  };

  const size = strs.length;
  // 定义dp[i][j][k]，表示前i个字符串中使用j个0和k个1的情况下最多得到多少个字符串
  const dp = new Array(size + 1)
    .fill(0)
    .map(() => new Array(m + 1).fill(0).map(() => new Array(n + 1).fill(0)));

  for (let i = 1; i <= size; i++) {
    const [zeros, ones] = getZerosOnes(strs[i - 1]);
    for (let j = 0; j <= m; j++) {
      for (let k = 0; k <= n; k++) {
        dp[i][j][k] = dp[i - 1][j][k];
        if (j >= zeros && k >= ones) {
          dp[i][j][k] = Math.max(dp[i][j][k], dp[i - 1][j - zeros][k - ones] + 1);
        }
      }
    }
  }

  return dp[size][m][n];
}
```

定义 `dp[i][j]` 为 `i` 个 `0` 和 `j` 个 `1` 凑出最多的子集。

```ts
function findMaxForm(strs: string[], m: number, n: number): number {
  // dp[i][j] i个0和j个1凑出最多的子集
  const dp = new Array(m + 1).fill(0).map((_) => new Array(n + 1).fill(0));

  for (let str of strs) {
    let count0 = 0;
    let count1 = 0;
    // 当前字符串0和1个个数
    for (let x of str) {
      if (x === '1') {
        count1++;
      } else {
        count0++;
      }
    }
    for (let i = m; i >= count0; i--) {
      for (let j = n; j >= count1; j--) {
        dp[i][j] = Math.max(dp[i][j], dp[i - count0][j - count1] + 1);
      }
    }
  }

  return dp[m][n];
}
```
