/*
 * @lc app=leetcode.cn id=1652 lang=cpp
 *
 * [1652] 拆炸弹
 */

// @lc code=start
class Solution {
public:
    vector<int> decrypt(vector<int> &code, int k) {
        int n = code.size();

        vector<int> ans(n);
        if (k == 0) return ans;

        code.resize(n * 2);
        copy(code.begin(), code.begin() + n, code.begin() + n);

        int l = k > 0 ? 1 : n + k;
        int r = k > 0 ? k : n - 1;
        int sum = 0;
        // 初始化窗口内的元素和
        for (int i = l; i <= r; i++) {
            sum += code[i];
        }
        // 计算结果
        for (int i = 0; i < n; i++) {
            ans[i] = sum;
            sum -= code[l];
            sum += code[r + 1];
            l++, r++;
        }

        return ans;
    }
};
// @lc code=end
