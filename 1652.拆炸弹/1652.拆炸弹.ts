/*
 * @lc app=leetcode.cn id=1652 lang=typescript
 *
 * [1652] 拆炸弹
 */

// @lc code=start
function decrypt(code: number[], k: number): number[] {
  const n = code.length;
  if (k === 0) {
    return new Array(n).fill(0);
  }

  const ans: number[] = [];
  for (let i = 0; i < n; i++) {
    let sum = 0;
    const start = k < 0 ? i + k + n - 1 : i;
    for (let j = 1; j <= Math.abs(k); j++) {
      const x = (start + j + n) % n;
      sum += code[x];
    }
    ans.push(sum);
  }

  return ans;
}
// @lc code=end
