# 题目

你有一个炸弹需要拆除，时间紧迫！你的情报员会给你一个长度为 `n` 的 循环 数组 `code` 以及一个密钥 k 。

为了获得正确的密码，你需要替换掉每一个数字。所有数字会 **同时** 被替换。

- 如果 $k > 0$ ，将第 `i` 个数字用 接下来 `k` 个数字之和替换。
- 如果 $k < 0$ ，将第 `i` 个数字用 之前 `k` 个数字之和替换。
- 如果 $k \equiv 0$ ，将第 `i` 个数字用 `0` 替换。

由于 `code` 是循环的， `code[n-1]` 下一个元素是 `code[0]` ，且 `code[0]` 前一个元素是 `code[n-1]` 。

给你 循环 数组 code 和整数密钥 k ，请你返回解密后的结果来拆除炸弹！

提示：

- $n \equiv code.length$
- $1 \leq n \leq 100$
- $1 \leq code[i] \leq 100$
- $-(n - 1) \leq k \leq n - 1$

# 示例

```
输入：code = [5,7,1,4], k = 3
输出：[12,10,16,13]
解释：每个数字都被接下来 3 个数字之和替换。解密后的密码为 [7+1+4, 1+4+5, 4+5+7, 5+7+1]。注意到数组是循环连接的。
```

```
输入：code = [2,4,9,3], k = -2
输出：[12,5,6,13]
解释：解密后的密码为 [3+9, 2+3, 4+2, 9+4] 。注意到数组是循环连接的。如果 k 是负数，那么和为 之前 的数字。
```

# 题解

## 索引取模

遍历数组，确定每个元素的累加起始地址，然后逐个计算。

```js
function decrypt(code: number[], k: number): number[] {
  const n = code.length;
  if (k === 0) {
    return new Array(n).fill(0);
  }

  const ans: number[] = [];
  for (let i = 0; i < n; i++) {
    let sum = 0;
    const start = k < 0 ? i + k + n - 1 : i;
    for (let j = 1; j <= Math.abs(k); j++) {
      const x = (start + j + n) % n;
      sum += code[x];
    }
    ans.push(sum);
  }

  return ans;
}
```

## 滑动窗口

我们将原数组进行拼接， 类似 $code = code + code $。这样模拟出一个循环数组。

维护一个长度为 k 的滑动窗口值，遍历 code ，每次计算完当前元素的累加和后，同时更新滑动窗口的值。

```cpp
class Solution {
public:
    vector<int> decrypt(vector<int> &code, int k) {
        int n = code.size();

        vector<int> ans(n);
        if (k == 0) return ans;

        code.resize(n * 2);
        copy(code.begin(), code.begin() + n, code.begin() + n);

        int l = k > 0 ? 1 : n + k;
        int r = k > 0 ? k : n - 1;
        int sum = 0;
        // 初始化窗口内的元素和
        for (int i = l; i <= r; i++) {
            sum += code[i];
        }
        // 计算结果
        for (int i = 0; i < n; i++) {
            ans[i] = sum;
            sum -= code[l];
            sum += code[r + 1];
            l++, r++;
        }

        return ans;
    }
};
```
