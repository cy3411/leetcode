# 题目

在一个 `n x n` 的国际象棋棋盘上，一个骑士从单元格 `(row, column)` 开始，并尝试进行 `k` 次移动。行和列是 **从 0 开始** 的，所以左上单元格是 `(0,0)` ，右下单元格是 `(n - 1, n - 1)` 。

象棋骑士有 `8` 种可能的走法，如下图所示。每次移动在基本方向上是两个单元格，然后在正交方向上是一个单元格。

提示:

- $1 \leq n \leq 25$
- $0 \leq k \leq 100$
- $0 \leq row, column \leq n$

[![HHpblt.png](https://s4.ax1x.com/2022/02/19/HHpblt.png)](https://imgtu.com/i/HHpblt)

每次骑士要移动时，它都会随机从 8 种可能的移动中选择一种(即使棋子会离开棋盘)，然后移动到那里。

骑士继续移动，直到它走了 k 步或离开了棋盘。

返回 骑士在棋盘停止移动后仍留在棋盘上的概率 。

# 示例

```
输入: n = 3, k = 2, row = 0, column = 0
输出: 0.0625
解释: 有两步(到(1,2)，(2,1))可以让骑士留在棋盘上。
在每一个位置上，也有两种移动可以让骑士留在棋盘上。
骑士留在棋盘上的总概率是0.0625。
```

# 题解

## 动态规划

定义 `dp[k][i][j]` 为骑士在第 `k` 步停止后，仍留在棋盘上的概率。

当点 `(i,j)` 在棋盘上且 `k=0` 时，`dp[k][i][j]=1`。

转移方程如下：

$$
    dp[k][i][j] = \sum_{di,dj} dp[k-1][i+d_i][j+d_j] * \frac{1}{8}，(d_i,d_j)表示走法的偏移量
$$

```ts
function knightProbability(n: number, k: number, row: number, column: number): number {
  const dirs = [
    [-2, -1],
    [-2, 1],
    [2, -1],
    [2, 1],
    [-1, -2],
    [-1, 2],
    [1, -2],
    [1, 2],
  ];
  const dp = new Array(k + 1)
    .fill(0)
    .map((_) => new Array(n).fill(0).map((_) => new Array(n).fill(0)));

  for (let step = 0; step <= k; step++) {
    for (let i = 0; i < n; i++) {
      for (let j = 0; j < n; j++) {
        if (step === 0) {
          dp[step][i][j] = 1;
        } else {
          for (const [x, y] of dirs) {
            const ni = i + x;
            const nj = j + y;
            if (ni < 0 || ni >= n) continue;
            if (nj < 0 || nj >= n) continue;
            dp[step][i][j] += dp[step - 1][ni][nj] / 8;
          }
        }
      }
    }
  }

  return dp[k][row][column];
}
```

```cpp
class Solution
{
    int dirs[8][2] = {
        {-2, -1},
        {-2, 1},
        {2, -1},
        {2, 1},
        {-1, -2},
        {-1, 2},
        {1, -2},
        {1, 2},
    };

public:
    double knightProbability(int n, int k, int row, int column)
    {
        vector<vector<vector<double>>> dp(k + 1, vector<vector<double>>(n, vector<double>(n)));

        for (int step = 0; step <= k; step++)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (step == 0)
                    {
                        dp[step][i][j] = 1;
                    }
                    else
                    {
                        for (auto &dir : dirs)
                        {
                            int ni = i + dir[0];
                            int nj = j + dir[1];
                            if (ni < 0 || ni >= n || nj < 0 || nj >= n)
                            {

                                continue;
                            }
                            dp[step][i][j] += dp[step - 1][ni][nj] / 8.0;
                        }
                    }
                }
            }
        }

        return dp[k][row][column];
    }
};
```
