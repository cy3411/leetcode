# 题目

给你一个整数数组 `nums` 和一个整数 `target` 。

向数组中的每个整数前添加 '+' 或 '-' ，然后串联起所有整数，可以构造一个 表达式 ：

- 例如，`nums = [2, 1]` ，可以在 `2` 之前添加 '+' ，在 `1` 之前添加 '-' ，然后串联起来得到表达式 `"+2-1"` 。

返回可以通过上述方法构造的、运算结果等于 `target` 的不同 **表达式** 的数目。

# 示例

```
输入：nums = [1,1,1,1,1], target = 3
输出：5
解释：一共有 5 种方法让最终目标和为 3 。
-1 + 1 + 1 + 1 + 1 = 3
+1 - 1 + 1 + 1 + 1 = 3
+1 + 1 - 1 + 1 + 1 = 3
+1 + 1 + 1 - 1 + 1 = 3
+1 + 1 + 1 + 1 - 1 = 3
```

# 题解

## 深度优先搜索

我们可以将加减操作理解为二叉树的左右分支，这样使用递归左右 2 棵子树，直到递归的深度等于数组的长度的时候，判断结果是否等于 target，是的话就记录一次。

最后将左右子树返回的结果相加，就是答案。

```ts
function findTargetSumWays(nums: number[], target: number): number {
  const dfs = (nums: number[], target: number, index: number, result: number): number => {
    if (index > nums.length) return 0;
    if (index === nums.length) {
      if (result === target) return 1;
      else return 0;
    }

    const left = dfs(nums, target, index + 1, result + nums[index]);
    const right = dfs(nums, target, index + 1, result - nums[index]);

    return left + right;
  };

  return dfs(nums, target, 0, 0);
}
```

可以添加记忆化，进行剪枝操作。

```ts
function findTargetSumWays(nums: number[], target: number): number {
  const hash = new Map();
  const dfs = (nums: number[], target: number, index: number, result: number): number => {
    const key = `${index}_${result}`;

    if (hash.has(key)) return hash.get(key);

    if (index === nums.length) {
      hash.set(key, result === target ? 1 : 0);
      return hash.get(key);
    }

    const left = dfs(nums, target, index + 1, result + nums[index]);
    const right = dfs(nums, target, index + 1, result - nums[index]);

    hash.set(key, left + right);
    return hash.get(key);
  };

  return dfs(nums, target, 0, 0);
}
```

## 动态规划

记数组的元素之和为 sum，添加减号的元素之和为 neg。那么剩下的添加加号的元素之和就是$sum-neg$。

那么得到的表达式就是：

$$
(sum-neg)-neg = sum-2*eng = target
$$

转化得到：

$$
neg = \frac{sum-target}{2}
$$

并且题意要求数组都是整数数组，所以 neg 也必须是整数。根据上面等式，neg 必然是偶数，否则直接返回 0.

这个时候题目就转化成，在数组中选取若然元素，使元素和等于 neg 的方案数。

**状态**

定义 dp[i][j]，在前 i 个元素中选择若干元素，使元素和等于 j 的方案数量。

**basecase**

$dp[0][0]=1$，0 个元素中选择元素和等于 0 的的方法只有 1 个。

**选择**

$$
dp[i][j] = \begin{cases}
    dp[i-1][j], & \text{j<nums[i]} \\
    dp[i-1][j] + dp[i-1][j-nums[i]], & \text{{j>=nums[i]}}
\end{cases}
$$

```ts
function findTargetSumWays(nums: number[], target: number): number {
  let sum = 0;
  for (const num of nums) {
    sum += num;
  }

  const diff = sum - target;
  // 如果差值小于0，表示数组总和小于target，不能形成组合
  // 如果插值取模不等于0，表示当前不符合题意的整数数组
  if (diff < 0 || diff % 2 !== 0) {
    return 0;
  }

  const n = nums.length;
  const neg = diff / 2;
  // dp[i][j]，在前i个元素中选取若干元素，使元素之和等于j的方案数量。
  const dp = new Array(n + 1).fill(0).map(() => new Array(neg + 1).fill(0));
  // base case，0个元素中选取元素之和等于0，有1个方案
  dp[0][0] = 1;

  for (let i = 1; i <= n; i++) {
    let num = nums[i - 1];
    for (let j = 0; j <= neg; j++) {
      dp[i][j] = dp[i - 1][j];
      if (j >= num) {
        dp[i][j] += dp[i - 1][j - num];
      }
    }
  }

  return dp[n][neg];
}
```
