/*
 * @lc app=leetcode.cn id=1688 lang=typescript
 *
 * [1688] 比赛中的配对次数
 */

// @lc code=start
function numberOfMatches(n: number): number {
  let ans = 0;
  while (n > 1) {
    // 记录匹配的队伍数量
    ans += Math.floor(n / 2);
    // 更新已晋级的队伍数量
    n = Math.ceil(n / 2);
  }
  return ans;
}
// @lc code=end
