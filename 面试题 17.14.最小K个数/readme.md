# 题目

设计一个算法，找出数组中最小的 `k` 个数。以任意顺序返回这 `k` 个数均可。

提示：

- `0 <= len(arr) <= 100000`
- `0 <= k <= min(100000, len(arr))`

# 示例

```
输入： arr = [1,3,5,7,2,4,6,8], k = 4
输出： [1,2,3,4]
```

# 题解

## 优先队列

维护一个长度为 `k` 的大顶堆,遍历数组,将元素插入大顶堆中,遍历结束后堆中的元素就是答案.

```js
var smallestK = function (arr, k) {
  // 大顶堆
  const maxpq = new MaxPQ();

  for (let i = 0; i < arr.length; i++) {
    maxpq.insert(arr[i]);
    // 只保留k个数字
    if (maxpq.keys.length > k) {
      maxpq.poll();
    }
  }

  return maxpq.keys;
};

class PQ {
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;

    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  less(i, j) {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }

  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }

  size() {
    return this.keys.length;
  }

  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }

  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }

  peek() {
    return this.keys[0];
  }
}
// 大顶堆
class MaxPQ extends PQ {}
```
