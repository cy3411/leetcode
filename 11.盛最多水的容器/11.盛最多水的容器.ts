/*
 * @lc app=leetcode.cn id=11 lang=typescript
 *
 * [11] 盛最多水的容器
 */

// @lc code=start
function maxArea(height: number[]): number {
  let l = 0;
  let r = height.length - 1;

  let result = 0;
  while (l < r) {
    let w = r - l;
    let h = Math.min(height[l], height[r]);
    result = Math.max(result, w * h);
    // 移动最小高度的指针
    height[l] < height[r] ? l++ : r--;
  }

  return result;
}
// @lc code=end
