# 题目
给你 `n` 个非负整数 `a1，a2，...，an`，每个数代表坐标中的一个点 `(i, ai)` 。在坐标内画 `n` 条垂直线，垂直线 `i` 的两个端点分别为 `(i, ai)` 和 `(i, 0)` 。找出其中的两条线，使得它们与 `x` 轴共同构成的容器可以容纳最多的水。

说明：你不能倾斜容器。

提示：
+ $n = height.length$
+ $2 <= n <= 3 * 10^4$
+ $0 <= height[i] <= 3 * 10^4$

# 示例
![gYWyIe.md.png](https://z3.ax1x.com/2021/05/09/gYWyIe.md.png)
```
输入：[1,8,6,2,5,4,8,3,7]
输出：49 
解释：图中垂直线代表输入数组 [1,8,6,2,5,4,8,3,7]。在此情况下，容器能够容纳水（表示为蓝色部分）的最大值为 49。
```
# 题解
## 双指针
定义左右指针`l`和`r`，容器的宽度就是`r-l`，高度就是`min(height[l],height[r])`。

每次更新结果后，将左右指针中最小高度的指针移动。

```js
function maxArea(height: number[]): number {
  let l = 0;
  let r = height.length - 1;

  let result = 0;
  while (l < r) {
    let w = r - l;
    let h = Math.min(height[l], height[r]);
    result = Math.max(result, w * h);
    // 移动最小高度的指针
    height[l] < height[r] ? l++ : r--;
  }

  return result;
}
```