/*
 * @lc app=leetcode.cn id=1185 lang=cpp
 *
 * [1185] 一周中的第几天
 */

// @lc code=start
class Solution
{
public:
    bool isLeapYear(int year)
    {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    };

    int calcDaysByYear(int year)
    {
        int days = 0;
        for (int i = 1971; i < year; i++)
        {
            days += isLeapYear(i) ? 366 : 365;
        }
        return days;
    };

    int calcDaysByMonth(int year, int month)
    {
        int days = 0;
        int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int i = 1; i < month; i++)
        {
            days += isLeapYear(year) && i == 2 ? 29 : months[i - 1];
        }
        return days;
    };

    string dayOfTheWeek(int day, int month, int year)
    {
        string days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

        int todalDays = calcDaysByYear(year) + calcDaysByMonth(year, month) + day;
        int week = (todalDays + 4) % 7;
        return days[week];
    };
};
// @lc code=end
