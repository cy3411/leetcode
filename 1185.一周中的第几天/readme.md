# 题目

给你一个日期，请你设计一个算法来判断它是对应一周中的哪一天。

输入为三个整数：`day`、`month` 和 `year`，分别表示日、月、年。

您返回的结果必须是这几个值中的一个 `{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}`。

提示：

- 给出的日期一定是在 `1971` 到 `2100` 年之间的有效日期。

# 示例

```
输入：day = 31, month = 8, year = 2019
输出："Saturday"
```

```
输入：day = 18, month = 7, year = 1999
输出："Sunday"
```

# 题解

## JS API

利用 `Date` 对象的 `getDay()` 方法，可以得到一周中的第几天。

```ts
function dayOfTheWeek(day: number, month: number, year: number): string {
  const days: string[] = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  const date = new Date(year, month - 1, day);
  return days[date.getDay()];
}
```

## 模拟

题目保证了给定的日期一定是在 `1971` 到 `2100` 年之间的有效日期，我可以利用计算给定日期到 1970 年最后一天的天数来模拟一周中的第几天。

- 循环取出 [1971,year-1] 时间段的天数；
- 然后处理当前年 year 的月份在 [1,month-1] 时间段的天数；
- 最后计算当前月经过了多少天，也就是 day 。

```ts
function dayOfTheWeek(day: number, month: number, year: number): string {
  const days: string[] = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  const months: number[] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  // 判断闰年
  const isLeapYear = (year: number): boolean => {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  };
  // 当前年到1971年的总天数
  const calcDaysByYear = (year: number): number => {
    let res = 0;
    for (let i = 1971; i < year; i++) {
      res += isLeapYear(i) ? 366 : 365;
    }
    return res;
  };
  // 当前年到1月的总天数
  const calcDaysByMonth = (year: number, month: number): number => {
    let res = 0;
    for (let i = 1; i < month; i++) {
      res += months[i - 1];
      if (isLeapYear(year) && i === 2) {
        res++;
      }
    }
    return res;
  };

  let totalDay = calcDaysByYear(year) + calcDaysByMonth(year, month) + day;
  // 1970年的最后一天是周4，所以这里加上4
  const week = (totalDay + 4) % 7;

  return days[week];
}
```

```cpp
class Solution
{
public:
    bool isLeapYear(int year)
    {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    };

    int calcDaysByYear(int year)
    {
        int days = 0;
        for (int i = 1971; i < year; i++)
        {
            days += isLeapYear(i) ? 366 : 365;
        }
        return days;
    };

    int calcDaysByMonth(int year, int month)
    {
        int days = 0;
        int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int i = 1; i < month; i++)
        {
            days += isLeapYear(year) && i == 2 ? 29 : months[i - 1];
        }
        return days;
    };

    string dayOfTheWeek(int day, int month, int year)
    {
        string days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

        int todalDays = calcDaysByYear(year) + calcDaysByMonth(year, month) + day;
        int week = (todalDays + 4) % 7;
        return days[week];
    };
};
```
