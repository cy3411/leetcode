/*
 * @lc app=leetcode.cn id=1185 lang=typescript
 *
 * [1185] 一周中的第几天
 */

// @lc code=start
function dayOfTheWeek(day: number, month: number, year: number): string {
  const days: string[] = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  const months: number[] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  // 判断闰年
  const isLeapYear = (year: number): boolean => {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  };
  // 当前年到1971年的总天数
  const calcDaysByYear = (year: number): number => {
    let res = 0;
    for (let i = 1971; i < year; i++) {
      res += isLeapYear(i) ? 366 : 365;
    }
    return res;
  };
  // 当前年到1月的总天数
  const calcDaysByMonth = (year: number, month: number): number => {
    let res = 0;
    for (let i = 1; i < month; i++) {
      res += months[i - 1];
      if (isLeapYear(year) && i === 2) {
        res++;
      }
    }
    return res;
  };

  let totalDay = calcDaysByYear(year) + calcDaysByMonth(year, month) + day;
  // 1970年的最后一天是周4，所以这里加上4
  const week = (totalDay + 4) % 7;

  return days[week];
}
// @lc code=end
