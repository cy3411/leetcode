# 题目
中位数是有序列表中间的数。如果列表长度是偶数，中位数则是中间两个数的平均值。

例如，

[2,3,4] 的中位数是 3

[2,3] 的中位数是 (2 + 3) / 2 = 2.5

设计一个支持以下两种操作的数据结构：

+ void addNum(int num) - 从数据流中添加一个整数到数据结构中。
+ double findMedian() - 返回目前所有元素的中位数。

# 示例
```
addNum(1)
addNum(2)
findMedian() -> 1.5
addNum(3) 
findMedian() -> 2
```

# 题解
有序列表的中位数的特点就是： 
+ 中位数左边的数，是左边列表的最大值
+ 中位数右边的数，是右边列表的最小值

## 堆
定义left是大顶堆，right是小顶堆。默认left最多比right最多多一个元素。

按序把流数组添加到堆中，获取中位数的时候判断一下奇偶，奇数输出left的堆顶，偶数把left和right的堆顶求和后平分就可以了。

```js
class PQ {
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;

    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  less(i, j) {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }

  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }

  size() {
    return this.keys.length;
  }

  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }

  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }

  peek() {
    return this.keys[0];
  }
}
// 大顶堆
class MaxPQ extends PQ {}
// 小顶堆
class MinPQ extends PQ {
  constructor(keys, mapKeysValue = (x) => x) {
    super(keys, (x) => -1 * mapKeysValue(x));
  }
}

/**
 * initialize your data structure here.
 */
var MedianFinder = function () {
  // 对顶堆
  this.left = new MaxPQ();
  this.right = new MinPQ();
};

/**
 * @param {number} num
 * @return {void}
 */
MedianFinder.prototype.addNum = function (num) {
  if (this.left.size() === 0 || num <= this.left.peek()) {
    this.left.insert(num);
  } else {
    this.right.insert(num);
  }

  // 调整平衡，left最多比right多一个元素
  if (this.left.size() === this.right.size() + 2) {
    this.right.insert(this.left.poll());
  }
  if (this.left.size() < this.right.size()) {
    this.left.insert(this.right.poll());
  }
};

/**
 * @return {number}
 */
MedianFinder.prototype.findMedian = function () {
  let size = this.left.size() + this.right.size();
  // 奇数
  if ((size & 1) === 1) return this.left.peek();
  // 偶数
  return (this.left.peek() + this.right.peek()) / 2;
};
```