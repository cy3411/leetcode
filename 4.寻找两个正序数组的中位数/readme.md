# 题目

给定两个大小分别为 `m` 和 `n` 的正序（从小到大）数组 `nums1` 和 `nums2`。请你找出并返回这两个正序数组的 **中位数** 。

# 示例

```
输入：nums1 = [1,3], nums2 = [2]
输出：2.00000
解释：合并数组 = [1,2,3] ，中位数 2
```

# 题解

题意比较简单，可以暴力求解。将数组合并后，求出中位数即可。

题目的进阶要求是时间复杂度 `O(log(m+n))`。如果时间复杂度要求的是 `log`，大部分都可以使用二分算法来解决。

## 二分算法

定义 `k=(m+n)/2`。

找中位数，也就是找到数组中第 k 小的元素。如果长度是偶数的话，再找一次 `k+1` 的位置即可。

由于是 `2` 个数组，我们可以将` k/2` 分别去 `2` 个数组中求中位。

然后将中位值较小的数组缩窄范围，继续去求 `k-k/2` 元素位置，直到 `k=1`。这个时候 `2` 个数组中第一个元素哪个小哪个就是中位值。

```ts
function findMedianSortedArrays(nums1: number[], nums2: number[]): number {
  const findK = (nums1: number[], i: number, nums2: number[], j: number, k: number): number => {
    // num1空了，直接返回num2第k小的元素
    if (i >= nums1.length) return nums2[j + k - 1];
    //  num2空了，直接返回num1第k小的元素
    if (j >= nums2.length) return nums1[i + k - 1];
    // 返回最小的那个
    if (k === 1) return Math.min(nums1[i], nums2[j]);

    // 将k二分
    const half = k >> 1;

    // 计算中间值
    let mid1 = i + half - 1 < nums1.length ? nums1[i + half - 1] : Number.POSITIVE_INFINITY;
    let mid2 = j + half - 1 < nums2.length ? nums2[j + half - 1] : Number.POSITIVE_INFINITY;

    if (mid1 < mid2) {
      // 缩小nums1的区间
      return findK(nums1, i + half, nums2, j, k - half);
    }
    // 缩小num2的范围
    return findK(nums1, i, nums2, j + half, k - half);
  };

  const n = nums1.length + nums2.length;
  const x = findK(nums1, 0, nums2, 0, (n + 1) >> 1);
  if ((n & 1) === 1) {
    return x;
  } else {
    const y = findK(nums1, 0, nums2, 0, ((n + 1) >> 1) + 1);
    return (x + y) / 2;
  }
}
```
