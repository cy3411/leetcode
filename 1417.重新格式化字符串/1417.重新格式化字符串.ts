/*
 * @lc app=leetcode.cn id=1417 lang=typescript
 *
 * [1417] 重新格式化字符串
 */

// @lc code=start
function reformat(s: string): string {
  let numbers = '';
  let letters = '';
  for (const c of s) {
    if (c >= '0' && c <= '9') {
      numbers += c;
    } else {
      letters += c;
    }
  }

  const nSize = numbers.length;
  const lSize = letters.length;

  if (Math.abs(nSize - lSize) > 1) return '';

  let ans = '';
  let n = Math.min(nSize, lSize);
  for (let i = 0; i < n; i++) {
    ans += nSize > lSize ? numbers[i] + letters[i] : letters[i] + numbers[i];
  }

  if (nSize > lSize) {
    ans += numbers[n];
  } else if (lSize > nSize) {
    ans += letters[n];
  }

  return ans;
}
// @lc code=end
