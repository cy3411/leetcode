# 题目

给你一个混合了数字和字母的字符串 `s`，其中的字母均为小写英文字母。

请你将该字符串重新格式化，使得任意两个相邻字符的类型都不同。也就是说，字母后面应该跟着数字，而数字后面应该跟着字母。

请你返回 **重新格式化后** 的字符串；如果无法按要求重新格式化，则返回一个 **空字符串** 。

提示：

- $1 \leq s.length \leq 500$
- `s` 仅由小写英文字母和/或数字组成。

# 示例

```
输入：s = "a0b1c2"
输出："0a1b2c"
解释："0a1b2c" 中任意两个相邻字符的类型都不同。 "a0b1c2", "0a1b2c", "0c2a1b" 也是满足题目要求的答案。
```

```
输入：s = "leetcode"
输出：""
解释："leetcode" 中只有字母，所以无法满足重新格式化的条件。
```

# 题解

## 模拟

分别取出字符和数字，然后将个数多的放在答案的偶数位，个数少的放在奇数位。

```js
function reformat(s: string): string {
  let numbers = '';
  let letters = '';
  for (const c of s) {
    if (c >= '0' && c <= '9') {
      numbers += c;
    } else {
      letters += c;
    }
  }

  const nSize = numbers.length;
  const lSize = letters.length;

  if (Math.abs(nSize - lSize) > 1) return '';

  let ans = '';
  let n = Math.min(nSize, lSize);
  for (let i = 0; i < n; i++) {
    ans += nSize > lSize ? numbers[i] + letters[i] : letters[i] + numbers[i];
  }

  if (nSize > lSize) {
    ans += numbers[n];
  } else if (lSize > nSize) {
    ans += letters[n];
  }

  return ans;
}
```
