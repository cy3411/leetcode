# 题目

给定一个由 0 和 1 组成的矩阵，找出每个元素到最近的 0 的距离。

两个相邻元素间的距离为 1 。

提示：

- 给定矩阵的元素个数不超过 10000。
- 给定矩阵中至少有一个元素是 0。
- 矩阵中的元素只在四个方向上相邻: 上、下、左、右。

# 示例

```
输入：
[[0,0,0],
 [0,1,0],
 [0,0,0]]

输出：
[[0,0,0],
 [0,1,0],
 [0,0,0]]
```

# 题解

## 广度优先

将所有值为 `0` 的元素加入队列，然后对队列中每个元素进行广度优先搜索，然后将每个元素的搜索状态再次放入队列，并同时更新结果。直到搜索结束。

```ts
class Data {
  i: number;
  j: number;
  l: number;
  constructor(i: number = 0, j: number = 0, l: number = 0) {
    this.i = i;
    this.j = j;
    this.l = l;
  }
}

function updateMatrix(mat: number[][]): number[][] {
  const dir = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  const n = mat.length;
  const m = mat[0].length;
  const visited: number[][] = new Array(n).fill(0).map(() => new Array(m).fill(-1));
  const queue: Data[] = [];
  // 将元素值为0的坐标加入队列
  const initQueue = (queue: Data[], mat: number[][], visited: number[][]) => {
    for (let i = 0; i < mat.length; i++) {
      for (let j = 0; j < mat[i].length; j++) {
        if (mat[i][j] !== 0) continue;
        visited[i][j] = 0;
        queue.push(new Data(i, j, 0));
      }
    }
  };

  initQueue(queue, mat, visited);

  while (queue.length) {
    const data = queue.shift();
    for (let k = 0; k < dir.length; k++) {
      const x = data.i + dir[k][0];
      const y = data.j + dir[k][1];
      if (x < 0 || x >= n) continue;
      if (y < 0 || y >= m) continue;
      if (visited[x][y] !== -1) continue;
      visited[x][y] = data.l + 1;
      queue.push(new Data(x, y, data.l + 1));
    }
  }

  return visited;
}
```
