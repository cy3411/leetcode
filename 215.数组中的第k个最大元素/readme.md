# 题目
在未排序的数组中找到第 k 个最大的元素。请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。

说明:

你可以假设 k 总是有效的，且 1 ≤ k ≤ 数组的长度。

# 示例
```
输入: [3,2,1,5,6,4] 和 k = 2
输出: 5
```

```
输入: [3,2,3,1,2,4,5,5,6] 和 k = 4
输出: 4
```

# 方法

## 堆

维护一个大小为 `k` 的小顶堆，遍历 `nums` , 把元素放入堆中。

当堆的大小大于 `k` 时，堆顶元素弹出，最后的堆顶就是结果。

堆的代码可以查看：[ 1046.最后一块石头的重量 ](https://gitee.com/cy3411/leecode/tree/master/1046.%E6%9C%80%E5%90%8E%E4%B8%80%E5%9D%97%E7%9F%B3%E5%A4%B4%E7%9A%84%E9%87%8D%E9%87%8F)

```js
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findKthLargest = function (nums, k) {
  const minPQ = new MinPQ();

  for (let i = 0; i < nums.length; i++) {
    minPQ.insert(nums[i]);
    if (minPQ.size() > k) {
      minPQ.poll();
    }
  }

  return minPQ.peek();
};
```


## 排序
```js
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findKthLargest = function (nums, k) {
  return nums.sort((a, b) => a - b)[nums.length - k];
};
```