# 题目

给定两个大小相等的数组 nums1 和 nums2，nums1 相对于 nums2 的优势可以用满足 nums1[i] > nums2[i] 的索引 i 的数目来描述。

返回 nums1 的任意排列，使其相对于 nums2 的优势最大化。

提示：

- $1 \leq nums1.length \leq 10^5$
- $nums2.length \equiv nums1.length$
- $0 \leq nums1[i], nums2[i] \leq 10^9$

# 示例

```
输入：nums1 = [2,7,11,15], nums2 = [1,10,4,11]
输出：[2,11,7,15]
```

```
输入：nums1 = [12,24,8,32], nums2 = [13,25,32,11]
输出：[24,32,8,12]
```

# 题解

## 贪心

将两个数组升序排序，随后只需要考虑两个数组的首元素即可：

- nums1 首元素大于 nums2 首元素，将两者关联
- nums1 首元素小于等于 nums2 首元素，将 num1 首元素和 num2 最大的元素关联

```js
function advantageCount(nums1: number[], nums2: number[]): number[] {
  const n = nums1.length;
  const idxs = new Array() < number > n.fill(0).map((_, i) => i);
  // 升序排序，nums2 相对位置有用，采用索引排序
  idxs.sort((a, b) => nums2[a] - nums2[b]);
  nums1.sort((a, b) => a - b);

  const ans = new Array() < number > n;
  let l = 0;
  let r = n - 1;
  for (let i = 0; i < n; i++) {
    // nums1 首个元素大于 nums2 首个元素，保持优势
    if (nums1[i] > nums2[idxs[l]]) {
      ans[idxs[l++]] = nums1[i];
    } else {
      // 否则，将nums1首个元素和 num2最大的元素匹配
      ans[idxs[r--]] = nums1[i];
    }
  }
  return ans;
}
```
