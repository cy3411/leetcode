# 题目
在一个长度为 `n` 的数组 `nums` 里的所有数字都在 `0～n-1` 的范围内。数组中某些数字是重复的，但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中**任意一个重复的数字**。

限制：
2 <= n <= 100000

# 示例
```
输入：
[2, 3, 1, 0, 2, 5, 3]
输出：2 或 3 
```

# 题解
## 排序
数组排序，遍历比较，有相同的直接返回结果。
```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var findRepeatNumber = function (nums) {
  // 升序排序
  nums.sort((a, b) => a - b);

  for (let i = 0; i < nums.length - 1; i++) {
    if (nums[i] === nums[i + 1]) return nums[i];
  }

  return false;
```
+ 时间复杂度：O(nlogn)
+ 空间复杂度：O(1)


## HashTable
遍历nums，将当前元素存入到hash中，如果表中已经有当前元素了，直接返回结果
```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var findRepeatNumber = function (nums) {
  const memo = new Map();
  for (let i = 0; i < nums.length; i++) {
    if (memo.has(nums[i])) {
      return memo.get(nums[i]);
    }
    memo.set(nums[i], nums[i]);
  }
  return false;
};
```
+ 时间复杂度：O(n)
+ 空间复杂度：O(n)

## 扫描数组

数组 `nums` 里的所有数字都在 `0～n-1` 的范围内，也就是说没有重复元素的情况下，数组中的元素都和数组的下标是一一对应的。

我们遍历数组，将`nums[i]`和`i`比较:
+ 相同继续往后
+ 不同的话
  + 比较`nums[i]`和`nums[nums[i]]`,
    + 相同，就是找到的重复元素，直接返回结果
    + 不同，交换双方的值，确保了`nums[nums[i]]`中的值跟下标是正确的

```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var findRepeatNumber = function (nums) {
  let size = nums.length;
  // 判断是否符合所有数字都在 0～n-1 的范围内
  for (const num of nums) {
    if (num < 0 || num > size - 1) return false;
  }

  for (let i = 0; i < size; i++) {
    while (nums[i] !== i) {
      if (nums[i] === nums[nums[i]]) return nums[i];
      let temp = nums[i];
      nums[i] = nums[temp];
      nums[temp] = temp;
    }
  }
  return false;
};
```
+ 时间复杂度：O(n)，嵌套的while循环次数是个常数，所以不计算
+ 空间复杂度：O(1)