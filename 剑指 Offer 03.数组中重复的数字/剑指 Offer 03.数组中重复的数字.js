// @algorithm @lc id=100275 lang=javascript
// @title shu-zu-zhong-zhong-fu-de-shu-zi-lcof
// @test([2, 3, 1, 0, 2, 5, 3])=2
/**
 * @param {number[]} nums
 * @return {number}
 */
var findRepeatNumber = function (nums) {
  let size = nums.length;
  // 判断是否符合所有数字都在 0～n-1 的范围内
  for (const num of nums) {
    if (num < 0 || num > size - 1) return false;
  }

  for (let i = 0; i < size; i++) {
    while (nums[i] !== i) {
      if (nums[i] === nums[nums[i]]) return nums[i];
      let temp = nums[i];
      nums[i] = nums[temp];
      nums[temp] = temp;
    }
  }
  return false;
};
