# 题目

给你一个数组 `nums` ，请你完成两类查询，其中一类查询要求更新数组下标对应的值，另一类查询要求返回数组中某个范围内元素的总和。

实现 `NumArray` 类：

- `NumArray(int[] nums)` 用整数数组 `nums` 初始化对象
- `void update(int index, int val)` 将 `nums[index]` 的值更新为 `val`
- `int sumRange(int left, int right)` 返回子数组 `nums[left, right]` 的总和（即，`nums[left] + nums[left + 1], ..., nums[right]`）

提示：

- $1 \leq nums.length \leq 3 * 10^4$
- $-100 \leq nums[i] \leq 100$
- $0 \leq index < nums.length$
- $-100 \leq val \leq 100$
- $0 \leq left \leq right < nums.length$
- 最多调用 $3 * 10^4$ 次 update 和 sumRange 方法

# 示例

```
输入：
["NumArray", "sumRange", "update", "sumRange"]
[[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
输出：
[null, 9, null, 8]

解释：
NumArray numArray = new NumArray([1, 3, 5]);
numArray.sumRange(0, 2); // 返回 9 ，sum([1,3,5]) = 9
numArray.update(1, 2);   // nums = [1,2,5]
numArray.sumRange(0, 2); // 返回 8 ，sum([1,2,5]) = 8
```

# 题解

## 线段树

线段树是一种树形结构，其中每个节点包含一个区间，其中区间的左端点和右端点都是整数，并且左端点小于右端点。

优点就是单点修改的时候复杂度为 O(logN)，而且查询的时候也是 O(logN)。

```ts
class FenwickTree {
  data: number[];
  n: number;
  constructor(n: number) {
    this.n = n;
    this.data = new Array(n + 1).fill(0);
  }
  _lowbit(x: number) {
    return x & -x;
  }
  update(i: number, val: number) {
    while (i <= this.n) {
      this.data[i] += val;
      i += this._lowbit(i);
    }
  }
  query(i: number) {
    let sum = 0;
    while (i > 0) {
      sum += this.data[i];
      i -= this._lowbit(i);
    }
    return sum;
  }
  at(i: number) {
    return this.query(i) - this.query(i - 1);
  }
}

class NumArray {
  fenwickTree: FenwickTree;
  constructor(nums: number[]) {
    this.fenwickTree = new FenwickTree(nums.length);
    for (let i = 0; i < nums.length; i++) {
      this.fenwickTree.update(i + 1, nums[i]);
    }
  }

  update(index: number, val: number): void {
    this.fenwickTree.update(index + 1, val - this.fenwickTree.at(index + 1));
  }

  sumRange(left: number, right: number): number {
    return this.fenwickTree.query(right + 1) - this.fenwickTree.query(left);
  }
}
```
