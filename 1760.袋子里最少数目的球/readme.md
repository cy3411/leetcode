# 题目

给你一个整数数组 nums ，其中 nums[i] 表示第 i 个袋子里球的数目。同时给你一个整数 maxOperations 。

你可以进行如下操作至多 maxOperations 次：

- 选择任意一个袋子，并将袋子里的球分到 2 个新的袋子中，每个袋子里都有 正整数 个球。
  - 比方说，一个袋子里有 5 个球，你可以把它们分到两个新袋子里，分别有 1 个和 4 个球，或者分别有 2 个和 3 个球。
    你的开销是单个袋子里球数目的 最大值 ，你想要 最小化 开销。

请你返回进行上述操作后的最小开销。

提示：

- 1 <= nums.length <= 105
- 1 <= maxOperations, nums[i] <= 109

# 示例

```
输入：nums = [9], maxOperations = 2
输出：3
解释：
- 将装有 9 个球的袋子分成装有 6 个和 3 个球的袋子。[9] -> [6,3] 。
- 将装有 6 个球的袋子分成装有 3 个和 3 个球的袋子。[6,3] -> [3,3,3] 。
装有最多球的袋子里装有 3 个球，所以开销为 3 并返回 3 。
```

# 题解

## 二分查找

我们遍历最后每个袋子里可能出现的球的数量 x，然后判断如果要分成每堆 x 个球，需要的操作次数 y：

- y 小于等于 maxOperations ，表示 x 太多了
- y 大于 maxOperations ，表示 x 太少了

因为每堆球分的数量越多，操作次数就越少，反之依然。

这样，可以使用二分查找，来实现目标。

```ts
function minimumSize(nums: number[], maxOperations: number): number {
  // 计算需要操作多少次，可以分成每堆x个球
  const operations = (x: number): number => {
    let count = 0;
    for (let i = 0; i < nums.length; i++) {
      if (nums[i] % x === 0) {
        // 能整除的话，堆数减去1次
        count += nums[i] / x - 1;
      } else {
        // 不能整除,余数加1表示堆数，再减去1
        count += ((nums[i] / x) >> 0) + 1 - 1;
      }
    }
    return count;
  };

  // 二分查找范围是最少的球和最多球之间
  let min = 1;
  let max = Math.max(...nums);
  while (min < max) {
    let mid = min + ((max - min) >> 1);
    // 当前操作次数小于给定次数，表示球多了，减小球的数量
    if (operations(mid) <= maxOperations) max = mid;
    // 增大球的数量
    else min = mid + 1;
  }
  return min;
}
```
