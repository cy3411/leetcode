/*
 * @lc app=leetcode.cn id=290 lang=javascript
 *
 * [290] 单词规律
 */

// @lc code=start
/**
 * @param {string} pattern
 * @param {string} s
 * @return {boolean}
 */
var wordPattern = function (pattern, s) {
  const words = s.split(' ');
  const patternMap = new Map();
  const wordMap = new Map();
  if (pattern.length !== words.length) {
    return false;
  }
  let i = 0;

  while (i < words.length) {
    let p = pattern[i];
    let w = words[i];
    if (
      (patternMap.has(p) && patternMap.get(p) !== w) ||
      (wordMap.has(w) && wordMap.get(w) !== p)
    ) {
      return false;
    }
    patternMap.set(p, w);
    wordMap.set(w, p);
    i++;
  }

  return true;
};
// @lc code=end
