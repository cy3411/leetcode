# 题目
以数组 `intervals` 表示若干个区间的集合，其中单个区间为 `intervals[i] = [starti, endi]` 。请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。

提示：
+ 1 <= intervals.length <= 104
+ intervals[i].length == 2
+ 0 <= starti <= endi <= 104

# 示例
```
输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
输出：[[1,6],[8,10],[15,18]]
解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
```

# 题解
## 排序
将区间数组按照左边界升序排序。  

定义一个栈保存结果。

迭代数组，当栈空或者栈顶元素的右边界小于当前元素的左边界时候，将当前元素入栈。

否则，区间就有重合状态，更新栈顶元素的右边界即可。

```ts
function merge(intervals: number[][]): number[][] {
  const size = intervals.length;
  if (intervals.length === 0) return [];
  // 排序
  intervals.sort((a, b) => a[0] - b[0]);

  // 定义一个栈保存结果
  const result: number[][] = [];
  for (let i = 0; i < size; i++) {
    let l = intervals[i][0];
    let r = intervals[i][1];
    //结果为空或者最后一个元素的右边界小于当前的左边界，表示可以插入结果
    if (result.length === 0 || result[result.length - 1][1] < l) {
      result.push([l, r]);
    } else {
      // 更新右边界
      result[result.length - 1][1] = Math.max(result[result.length - 1][1], r);
    }
  }

  return result;
}
```


```ts
function merge(intervals: number[][]): number[][] {
  const size = intervals.length;
  if (intervals.length === 0) return [];

  // 保存区间左右边界状态
  const marks: number[][] = [];
  // 左边界，状态1。
  // 右边界，状态-1。
  for (let [l, r] of intervals) {
    marks.push([l, 1]);
    marks.push([r, -1]);
  }
  // 按照左边界升序
  // 如果左边界相同，按照状态降序
  marks.sort((a, b) => {
    if (a[0] - b[0]) {
      return a[0] - b[0];
    } else {
      return b[1] - a[1];
    }
  });

  const result: number[][] = [];
  let left = -1;
  let count = 0;
  for (let [idx, mark] of marks) {
    if (left === -1) left = idx;
    // 累计计算状态值
    count += mark;
    // 如果为0，就找到一个合并区间，更新答案
    if (count === 0) {
      result.push([left, idx]);
      left = -1;
    }
  }

  return result;
}
```
