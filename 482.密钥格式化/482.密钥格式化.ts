/*
 * @lc app=leetcode.cn id=482 lang=typescript
 *
 * [482] 密钥格式化
 */

// @lc code=start
function licenseKeyFormatting(s: string, k: number): string {
  let n = s.length;
  let count = 0;
  const ans: string[] = [];

  for (let i = n - 1; i >= 0; i--) {
    const char = s[i];
    if (char === '-') continue;
    count++;
    ans.push(char.toUpperCase());
    if (count % k === 0) {
      ans.push('-');
    }
  }
  // 如果可以整分的话，需要去掉最后一个分隔符
  if (ans[ans.length - 1] === '-') ans.pop();
  return ans.reverse().join('');
}
// @lc code=end
