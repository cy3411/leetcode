# 题目

有一个密钥字符串 S ，只包含字母，数字以及 '-'（破折号）。其中， N 个 '-' 将字符串分成了 N+1 组。

给你一个数字 K，请你重新格式化字符串，使每个分组恰好包含 K 个字符。特别地，第一个分组包含的字符个数必须小于等于 K，但至少要包含 1 个字符。两个分组之间需要用 '-'（破折号）隔开，并且将所有的小写字母转换为大写字母。

给定非空字符串 S 和数字 K，按照上面描述的规则进行格式化。

提示:

S 的长度可能很长，请按需分配大小。K 为正整数。
S 只包含字母数字（a-z，A-Z，0-9）以及破折号'-'
S 非空

# 示例

```
输入：S = "5F3Z-2e-9-w", K = 4
输出："5F3Z-2E9W"
解释：字符串 S 被分成了两个部分，每部分 4 个字符；
     注意，两个额外的破折号需要删掉。
```

```
输入：S = "2-5g-3-J", K = 2
输出："2-5G-3J"
解释：字符串 S 被分成了 3 个部分，按照前面的规则描述，第一部分的字符可以少于给定的数量，其余部分皆为 2 个字符。
```

# 题解

## 模拟

计算出所有字符的数量，如果不能与 `k` 整除，那么余数就是第一组字符串的数量。

```ts
function licenseKeyFormatting(s: string, k: number): string {
  const strs = s.split('-').join('');
  const n = strs.length;

  const mod = n % k;

  let ans = '';
  let separator = 0;
  for (let i = 0; i < n; i++) {
    const char = strs[i];
    ans += char.toUpperCase();
    separator++;

    // 最后一组不需要分割符
    if (i === n - 1) break;

    // 如果不能平均分配，第一组的分隔符位置
    if (mod > 0 && i < mod) {
      if (separator === mod) {
        ans += '-';
        separator = 0;
      }
    } else if (separator === k) {
      ans += '-';
      separator = 0;
    }
  }

  return ans;
}
```

## 倒序扫描

倒序扫描字符串，如果是分隔符就跳过，否则就将 count 加 1，并将字符加入结果。

计数的同时，每隔 k 个字符就在结果加入分隔符。

最后将结果反转即可。

需要注意的就是如果是整分的字符串，最后会多一个分隔符，需要删除掉。

```ts
function licenseKeyFormatting(s: string, k: number): string {
  let n = s.length;
  let count = 0;
  const ans: string[] = [];

  for (let i = n - 1; i >= 0; i--) {
    const char = s[i];
    if (char === '-') continue;
    count++;
    ans.push(char.toUpperCase());
    if (count % k === 0) {
      ans.push('-');
    }
  }
  // 如果可以整分的话，需要去掉最后一个分隔符
  if (ans[ans.length - 1] === '-') ans.pop();
  return ans.reverse().join('');
}
```
