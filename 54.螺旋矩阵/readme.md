# 题目
给你一个 m 行 n 列的矩阵 matrix ，请按照**顺时针螺旋顺序** ，返回矩阵中的所有元素。

# 示例
```
输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
输出：[1,2,3,6,9,8,7,4,5]
```

# 方法
## 模拟旋转
按照顺时针定义一个方向数组，当路径超出界限或者进入之前访问过的位置时，顺时针旋转，进入下一个方向。
判断是否进入之前访问过的位置，需要一个辅助数组visited来存储状态。
```js
var spiralOrder = function (matrix) {
  if (!matrix.length || !matrix[0].length) {
    return [];
  }

  let m = matrix.length;
  let n = matrix[0].length;
  const visited = new Array(m).fill(0).map((_) => new Array(n).fill(false));
  const result = new Array(m * n);

  let row = 0;
  let col = 0;
  // 顺时针的方向
  let directions = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
  ];
  let directionsIndex = 0;

  for (let i = 0; i < m * n; i++) {
    result[i] = matrix[row][col];
    visited[row][col] = true;
    const nextRow = row + directions[directionsIndex][0];
    const nextCol = col + directions[directionsIndex][1];
    // 如果不满足条件的话就需要旋转了
    if (
      !(nextRow >= 0 && nextRow < m && nextCol >= 0 && nextCol < n && !visited[nextRow][nextCol])
    ) {
      directionsIndex = (directionsIndex + 1) % 4;
    }
    row += directions[directionsIndex][0];
    col += directions[directionsIndex][1];
  }

  return result;
};
```

## 逐层扫描
按照顺时针方向从外往里逐层扫描，定义top,bottom,left,right来控制扫描的范围。
```js
var spiralOrder = function (matrix) {
  if (!matrix.length || !matrix[0].length) {
    return [];
  }

  const result = [];
  let top = 0;
  let bottom = matrix.length - 1;
  let left = 0;
  let right = matrix[0].length - 1;

  while (true) {
    // 左->右
    for (let i = left; i <= right; i++) {
      result.push(matrix[top][i]);
    }
    if (++top > bottom) break;
    // 上->下
    for (let i = top; i <= bottom; i++) {
      result.push(matrix[i][right]);
    }
    if (left > --right) break;
    // 右->左
    for (let i = right; i >= left; i--) {
      result.push(matrix[bottom][i]);
    }
    if (top > --bottom) break;
    // 下->上
    for (let i = bottom; i >= top; i--) {
      result.push(matrix[i][left]);
    }
    if (++left > right) break;
  }

  return result;
};
```