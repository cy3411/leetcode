# 题目

段式回文 其实与 一般回文 类似，只不过是最小的单位是 一段字符 而不是 单个字母。

举个例子，对于一般回文 `"abcba"` 是回文，而 `"volvo"` 不是，但如果我们把 `"volvo"` 分为 `"vo"、"l"、"vo"` 三段，则可以认为 `“(vo)(l)(vo)”` 是段式回文（分为 3 段）。

给你一个字符串 `text` ，在确保它满足段式回文的前提下，请你返回 **段** 的 **最大数量** `k`。

如果段的最大数量为 `k` ，那么存在满足以下条件的 `a_1, a_2, ..., a_k`：

- 每个 `a_i` 都是一个非空字符串；
- 将这些字符串首位相连的结果 `a_1 + a_2 + ... + a_k` 和原始字符串 `text` 相同；
- 对于所有 `1 <= i <= k`，都有 `a*i = a*{k+1 - i}` 。

提示：

- `text` 仅由小写英文字符组成。
- $\color{burlywood}1 \leq text.length \leq 1000$

# 示例

```
输入：text = "ghiabcdefhelloadamhelloabcdefghi"
输出：7
解释：我们可以把字符串拆分成 "(ghi)(abcdef)(hello)(adam)(hello)(abcdef)(ghi)"。
```

```
输入：text = "merchant"
输出：1
解释：我们可以把字符串拆分成 "(merchant)"。
```

# 题解

## 递归

双指针比较 text 头尾是否有回文段，如果有就递归继续比较中间位置的字符串。

直到比较的字符串位置头尾没有回文段。

```ts
function longestDecomposition(text: string): number {
  const getResult = (text: string, l: number, r: number): number => {
    const n = r - l + 1;
    // 区间长度小于2，不能分割，直接返回长度
    if (n <= 1) return n;
    // 遍历比较字符串的长度
    for (let i = 1; i <= Math.floor(n / 2); i++) {
      let flag = true;
      // 双指针，前半截和后半截字符开始比较
      for (let j = l, k = r - i + 1; k <= r; j++, k++) {
        if (text[j] === text[k]) continue;
        flag = false;
        break;
      }
      // 如果头尾有段式回文，递归比较中间的字符串
      if (flag) return getResult(text, l + i, r - i) + 2;
    }
    // 如果头尾没有段式回文，直接返回1
    return 1;
  };

  return getResult(text, 0, text.length - 1);
}
```

```cpp
class Solution
{
public:
    int getResult(string &text, int l, int r)
    {
        int n = r - l + 1;
        if (n <= 1)
        {
            return n;
        }
        // 遍历比较长度
        for (int i = 1; i <= n / 2; i++)
        {
            bool flag = true;
            for (int j = l, k = r - i + 1; k <= r; k++, j++)
            {
                if (text[k] == text[j])
                {

                    continue;
                }
                flag = false;
                break;
            }
            if (flag)
            {
                return getResult(text, l + i, r - i) + 2;
            }
        }

        return 1;
    }

    int longestDecomposition(string text)
    {
        return getResult(text, 0, text.size() - 1);
    }
};
```
