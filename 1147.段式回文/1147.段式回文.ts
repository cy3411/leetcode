/*
 * @lc app=leetcode.cn id=1147 lang=typescript
 *
 * [1147] 段式回文
 */

// @lc code=start
function longestDecomposition(text: string): number {
  const getResult = (text: string, l: number, r: number): number => {
    const n = r - l + 1;
    // 区间长度小于2，不能分割，直接返回长度
    if (n <= 1) return n;
    // 遍历比较字符串的长度
    for (let i = 1; i <= Math.floor(n / 2); i++) {
      let flag = true;
      // 双指针，前半截和后半截字符开始比较
      for (let j = l, k = r - i + 1; k <= r; j++, k++) {
        if (text[j] === text[k]) continue;
        flag = false;
        break;
      }
      // 如果头尾有段式回文，递归比较上下中间的字符串
      if (flag) return getResult(text, l + i, r - i) + 2;
    }
    // 如果头尾没有段式回文，直接返回1
    return 1;
  };

  return getResult(text, 0, text.length - 1);
}
// @lc code=end
