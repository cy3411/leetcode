/*
 * @lc app=leetcode.cn id=1147 lang=cpp
 *
 * [1147] 段式回文
 */

// @lc code=start
class Solution
{
public:
    int getResult(string &text, int l, int r)
    {
        int n = r - l + 1;
        if (n <= 1)
        {
            return n;
        }
        // 遍历比较长度
        for (int i = 1; i <= n / 2; i++)
        {
            bool flag = true;
            for (int j = l, k = r - i + 1; k <= r; k++, j++)
            {
                if (text[k] == text[j])
                {

                    continue;
                }
                flag = false;
                break;
            }
            if (flag)
            {
                return getResult(text, l + i, r - i) + 2;
            }
        }

        return 1;
    }

    int longestDecomposition(string text)
    {
        return getResult(text, 0, text.size() - 1);
    }
};
// @lc code=end
