/*
 * @lc app=leetcode.cn id=13 lang=typescript
 *
 * [13] 罗马数字转整数
 */

// @lc code=start
function romanToInt(s: string): number {
  const hash = new Map([
    ['I', 1],
    ['IV', 4],
    ['V', 5],
    ['IX', 9],
    ['X', 10],
    ['XL', 40],
    ['L', 50],
    ['XC', 90],
    ['C', 100],
    ['CD', 400],
    ['D', 500],
    ['CM', 900],
    ['M', 1000],
  ]);
  let result = 0;
  let i = 0;
  const size = s.length;

  while (i < size) {
    if (i + 1 < size && hash.has(s.substring(i, i + 2))) {
      result += hash.get(s.substring(i, i + 2));
      i += 2;
    } else {
      result += hash.get(s[i]);
      i++;
    }
  }

  return result;
}
// @lc code=end
