# 题目
给你一个整数数组 `nums` ，你可以对它进行一些操作。

每次操作中，选择任意一个 `nums[i]` ，删除它并获得 `nums[i]` 的点数。之后，你必须删除**每个**等于` nums[i] - 1` 或 `nums[i] + 1` 的元素。

开始你拥有 `0` 个点数。返回你能通过这些操作获得的最大点数。

# 示例
```
输入：nums = [3,4,2]
输出：6
解释：
删除 4 获得 4 个点数，因此 3 也被删除。
之后，删除 2 获得 2 个点数。总共获得 6 个点数。
```
# 题解
## 动态规划
根据题意，当选择了元素`x`的时候，`x-1`和`x+1`元素就会被删除。  
当多个`x`的时候应被全部选择，尽可能多的获得点数。

我们用数组`sum`记录`nums`中所有相同元素之和，如果选择`x`，则无法选择`x+1`和`x-1`，这就是[198.打家劫舍](https://gitee.com/cy3411/leetcode/tree/master/198.%E6%89%93%E5%AE%B6%E5%8A%AB%E8%88%8D)的题目了。

```js
function deleteAndEarn(nums: number[]): number {
  let maxVal = 0;
  for (const num of nums) {
    maxVal = Math.max(maxVal, num);
  }

  const sum = new Array(maxVal + 1).fill(0);
  for (let i = 0; i < nums.length; i++) {
    sum[nums[i]] += nums[i];
  }

  const rob = (sum: number[]): number => {
    const size = sum.length;
    let first = sum[0];
    let second = Math.max(sum[0], sum[1]);
    for (let i = 2; i < size; i++) {
      let temp = second;
      second = Math.max(first + sum[i], second);
      first = temp;
    }
    return second;
  };

  return rob(sum);
}
```