/*
 * @lc app=leetcode.cn id=740 lang=typescript
 *
 * [740] 删除与获得点数
 */

// @lc code=start
function deleteAndEarn(nums: number[]): number {
  let maxVal = 0;
  for (const num of nums) {
    maxVal = Math.max(maxVal, num);
  }

  const sum = new Array(maxVal + 1).fill(0);
  for (let i = 0; i < nums.length; i++) {
    sum[nums[i]] += nums[i];
  }

  const rob = (sum: number[]): number => {
    const size = sum.length;
    let first = sum[0];
    let second = Math.max(sum[0], sum[1]);
    for (let i = 2; i < size; i++) {
      let temp = second;
      second = Math.max(first + sum[i], second);
      first = temp;
    }
    return second;
  };

  return rob(sum);
}
// @lc code=end
