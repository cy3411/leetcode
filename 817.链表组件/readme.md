# 题目

给定链表头结点 head，该链表上的每个结点都有一个 唯一的整型值 。同时给定列表 nums，该列表是上述链表中整型值的一个子集。

返回列表 nums 中组件的个数，这里对组件的定义为：链表中一段最长连续结点的值（该值必须在列表 nums 中）构成的集合。

提示：

- 链表中节点数为`n`
- $1 \leq n \leq 104$
- $0 \leq Node.val < n$
- `Node.val` 中所有值 不同
- $1 \leq nums.length \leq n$
- $0 \leq nums[i] < n$
- `nums` 中所有值 不同

# 示例

[![xa17xe.png](https://s1.ax1x.com/2022/10/12/xa17xe.png)](https://imgse.com/i/xa17xe)

```
输入: head = [0,1,2,3], nums = [0,1,3]
输出: 2
解释: 链表中,0 和 1 是相连接的，且 nums 中不包含 2，所以 [0, 1] 是 nums 的一个组件，同理 [3] 也是一个组件，故返回 2。
```

[![xa3AZn.png](https://s1.ax1x.com/2022/10/12/xa3AZn.png)](https://imgse.com/i/xa3AZn)

```
输入: head = [0,1,2,3,4], nums = [0,3,1,4]
输出: 2
解释: 链表中，0 和 1 是相连接的，3 和 4 是相连接的，所以 [0, 1] 和 [3, 4] 是两个组件，故返回 2。
```

# 题解

## 统计组件头位置

遍历链表，统计组件头的数量。

组件头指的是节点值在链表中，且前一个节点不在 nums 中。

因为需要多次判断节点值是否在 nums 中，可以使用哈希表来存储 nums ，来降低时间复杂度。

```js
function numComponents(head: ListNode | null, nums: number[]): number {
  const numsSet = new Set() < number > [...nums];
  // 是否是组件头
  let isHead = false;
  let ans = 0;
  while (head) {
    if (numsSet.has(head.val)) {
      if (!isHead) {
        isHead = true;
        ans++;
      }
    } else {
      isHead = false;
    }
    head = head.next;
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int numComponents(ListNode *head, vector<int> &nums) {
        unordered_set<int> numsSet;
        for (int num : nums) {
            numsSet.emplace(num);
        }

        int isHead = 0, ans = 0;
        while (head) {
            if (numsSet.find(head->val) != numsSet.end()) {
                if (!isHead) {
                    isHead = 1;
                    ans++;
                }
            } else {
                isHead = 0;
            }
            head = head->next;
        }

        return ans;
    }
};
```

```py
class Solution:
    def numComponents(self, head: Optional[ListNode], nums: List[int]) -> int:
        numsSet = set(nums)
        isHead = False
        ans = 0
        while head:
            if head.val not in numsSet:
                isHead = False
            elif not isHead:
                isHead = True
                ans += 1
            head = head.next
        return ans
```
