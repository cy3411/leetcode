#
# @lc app=leetcode.cn id=817 lang=python3
#
# [817] 链表组件
#

# @lc code=start
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def numComponents(self, head: Optional[ListNode], nums: List[int]) -> int:
        numsSet = set(nums)
        isHead = False
        ans = 0
        while head:
            if head.val not in numsSet:
                isHead = False
            elif not isHead:
                isHead = True
                ans += 1
            head = head.next
        return ans
# @lc code=end
