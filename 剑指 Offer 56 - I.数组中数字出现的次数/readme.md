# 题目

一个整型数组 `nums` 里除两个数字之外，其他数字都出现了两次。请写程序找出这两个只出现一次的数字。要求时间复杂度是 `O(n)`，空间复杂度是 `O(1)`。

限制：

- $2 \leq nums.length \leq 10000$

# 示例

```
输入：nums = [4,1,4,6]
输出：[1,6] 或 [6,1]
```

```
输入：nums = [1,2,10,4,1,4,3,3]
输出：[2,10] 或 [10,2]
```

# 题解

## 位运算

将 `nums` 中所有的数字跟 `0` 异或后的结果是两个只出现一次的数字的异或结果。

找到结果数字的二进制位中找到某个 `1` 所在的位置，这个表示两个数字不同的位置。

最后重新遍历 `nums`，分组异或，同位置是 `1` 的一组，不是 `1` 的一组，最后的两个数字就是答案。

```ts
function singleNumbers(nums: number[]): number[] {
  const n = nums.length;
  let xorbit = 0;
  // 全部异或
  for (const num of nums) {
    xorbit ^= num;
  }

  // 取最低位1
  let idx = 0;
  while ((xorbit & 1) === 0) {
    xorbit >>= 1;
    idx++;
  }

  // 根据最低位 1 的位置分组异或
  let a = 0;
  let b = 0;
  for (const num of nums) {
    if (num & (1 << idx)) {
      a ^= num;
    } else {
      b ^= num;
    }
  }

  return [a, b];
}
```

```cpp
class Solution {
public:
    vector<int> singleNumbers(vector<int>& nums) {
      int n = nums.size(), bits = 0;
      for (auto x : nums) {
        bits ^= x;
      }

      int idx = 0;
      while ((bits & 1) == 0) {
        idx++;
        bits >>= 1;
      }

      int a = 0, b = 0;
      for (auto x : nums) {
        if (x & (1 << idx)) {
          a ^= x;
        }else {
          b ^= x;
        }
      }

      return vector<int>{a, b};
    }
};
```
