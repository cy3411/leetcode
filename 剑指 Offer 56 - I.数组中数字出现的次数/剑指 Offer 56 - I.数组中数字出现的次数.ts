// @algorithm @lc id=100320 lang=typescript
// @title shu-zu-zhong-shu-zi-chu-xian-de-ci-shu-lcof
// @test([1,2,10,4,1,4,3,3])=[10,2]
function singleNumbers(nums: number[]): number[] {
  const n = nums.length;
  let xorbit = 0;
  // 全部异或
  for (const num of nums) {
    xorbit ^= num;
  }

  // 取最低位1
  let idx = 0;
  while ((xorbit & 1) === 0) {
    xorbit >>= 1;
    idx++;
  }

  // 根据最低位 1 的位置分组异或
  let a = 0;
  let b = 0;
  for (const num of nums) {
    if (num & (1 << idx)) {
      a ^= num;
    } else {
      b ^= num;
    }
  }

  return [a, b];
}
