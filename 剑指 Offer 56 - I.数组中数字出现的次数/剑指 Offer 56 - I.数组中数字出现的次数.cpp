class Solution
{
public:
    vector<int> singleNumbers(vector<int> &nums)
    {
        int n = nums.size(), bits = 0;
        for (auto x : nums)
        {
            bits ^= x;
        }

        int idx = 0;
        while ((bits & 1) == 0)
        {
            idx++;
            bits >>= 1;
        }

        int a = 0, b = 0;
        for (auto x : nums)
        {
            if (x & (1 << idx))
            {
                a ^= x;
            }
            else
            {
                b ^= x;
            }
        }

        return vector<int>{a, b};
    }
};