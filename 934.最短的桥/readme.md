# 题目

给你一个大小为 n x n 的二元矩阵 grid ，其中 1 表示陆地，0 表示水域。

岛 是由四面相连的 1 形成的一个最大组，即不会与非组内的任何其他 1 相连。grid 中 恰好存在两座岛 。

你可以将任意数量的 0 变为 1 ，以使两座岛连接起来，变成 一座岛 。

返回必须翻转的 0 的最小数目。

提示：

- $n \equiv grid.length \equiv grid[i].length$
- $2 \leq n \leq 100$
- grid[i][j] 为 0 或 1
- grid 中恰有两个岛

# 示例

```
输入：grid = [[0,1],[1,0]]
输出：1
```

```
输入：grid = [[0,1,0],[0,0,0],[0,0,1]]
输出：2
```

# 题解

## 广度优先搜索

题目给出了只有两个岛屿，计算两座岛屿之间最短的距离。

考虑使用 BFS，先找到一座岛屿，然后广度搜索第一个岛屿的坐标，直到找到下一个岛屿，返回其步骤。

```js
function shortestBridge(grid: number[][]): number {
  const n = grid.length;
  const dirs = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  // 岛屿坐标
  const islands: number[][] = [];
  // 队列,BFS
  const queue: number[][] = [];
  // 遍历每个单元，将访问过的岛屿记作-1
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      // 不是岛屿或者已访问过
      if (grid[i][j] !== 1) continue;
      queue.push([i, j]);
      // 标记访问过
      grid[i][j] = -1;
      // bfs岛屿
      while (queue.length) {
        const island = queue.shift()!;
        islands.push(island);
        //遍历四周
        for (const [x, y] of dirs) {
          const nx = island[0] + x;
          const ny = island[1] + y;
          if (nx < 0 || nx >= n) continue;
          if (ny < 0 || ny >= n) continue;
          if (grid[nx][ny] !== 1) continue;
          queue.push([nx, ny]);
          grid[nx][ny] = -1;
        }
      }

      // 利用第一个找到的岛屿 bfs 寻找另一个岛屿
      for (const island of islands) {
        queue.push(island);
      }

      let step = 0;
      while (queue.length) {
        const sz = queue.length;
        for (let k = 0; k < sz; k++) {
          const island = queue.shift()!;
          for (const [x, y] of dirs) {
            const nx = island[0] + x;
            const ny = island[1] + y;
            if (nx < 0 || nx >= n) continue;
            if (ny < 0 || ny >= n) continue;
            if (grid[nx][ny] === -1) continue;
            if (grid[nx][ny] === 0) {
              queue.push([nx, ny]);
              grid[nx][ny] = -1;
            } else if (grid[nx][ny] === 1) {
              return step;
            }
          }
        }
        step++;
      }
    }
  }
  return 0;
}
```
