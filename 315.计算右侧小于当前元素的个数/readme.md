# 题目
给定一个整数数组 `nums`，按要求返回一个新数组 `counts`。数组 `counts` 有该性质： `counts[i]` 的值是  `nums[i]` **右侧小于** `nums[i]` 的元素的数量。

提示：
+ 0 <= nums.length <= 10^5
+ -10^4 <= nums[i] <= 10^4

# 示例
```
输入：nums = [5,2,6,1]
输出：[2,1,1,0] 
解释：
5 的右侧有 2 个更小的元素 (2 和 1)
2 的右侧仅有 1 个更小的元素 (1)
6 的右侧有 1 个更小的元素 (1)
1 的右侧有 0 个更小的元素
```

# 题解
## 归并
将数组元素归并降序排序，在合并的时候，在左区间记录小于该元素的其他元素数量。

由于合并时候，左右区间都处于有序状态。如果右区间有元素小于左区间的元素，那么该元素后的所有元素都小于左区间的元素。这个时候就可以统计个数了。

```ts
function countSmaller(nums: number[]): number[] {
  const size = nums.length;

  if (size === 0) return [];

  const temp = new Array(size);
  const mergeSort = (arr: number[][], l: number, r: number): void => {
    if (l >= r) return;
    let mid = l + ((r - l) >> 1);
    mergeSort(arr, l, mid);
    mergeSort(arr, mid + 1, r);

    let k = l;
    let i = l;
    let j = mid + 1;
    // 降序排序
    while (i <= mid || j <= r) {
      // 合并的时候都是有序状态，所以右区间剩余的就是小于当前元素的
      if (j > r || (i <= mid && arr[i][0] > arr[j][0])) {
        temp[k] = arr[i];
        arr[i][2] += r - j + 1;
        k++, i++;
      } else {
        temp[k] = arr[j];
        k++, j++;
      }
    }
    for (let i = l; i <= r; i++) {
      arr[i] = temp[i];
    }
  };

  const arr = [];
  // 将元素、下标和统计的元素个数生成新数组
  for (let i = 0; i < size; i++) {
    arr.push([nums[i], i, 0]);
  }

  mergeSort(arr, 0, size - 1);

  const result = new Array(size);
  // 生成结果
  for (let i = 0; i < size; i++) {
    result[arr[i][1]] = arr[i][2];
  }

  return result;
}
```