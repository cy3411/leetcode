# 题目

在社交媒体网站上有 `n` 个用户。给你一个整数数组 `ages` ，其中 `ages[i]` 是第 `i` 个用户的年龄。

如果下述任意一个条件为真，那么用户 `x` 将不会向用户 `y（x != y）` 发送好友请求：

- `age[y] <= 0.5 * age[x] + 7`
- `age[y] > age[x]`
- `age[y] > 100 && age[x] < 100`

否则，`x` 将会向 `y` 发送一条好友请求。

注意，如果 `x` 向 `y` 发送一条好友请求，`y` 不必也向 `x` 发送一条好友请求。另外，用户不会向自己发送好友请求。

返回在该社交媒体网站上产生的好友请求总数。

提示：

- `n == ages.length`
- $\color{burlywood}1 \leq n \leq 2 * 10^4$
- $\color{burlywood}1 \leq ages[i] \leq 120$

# 示例

```
输入：ages = [16,16]
输出：2
解释：2 人互发好友请求。
```

```
输入：ages = [16,17,18]
输出：2
解释：产生的好友请求为 17 -> 16 ，18 -> 17 。
```

# 题解

## 双指针

根据题目的给出的条件，可以发现，如果满足条件 2，那么条件 3 一定满足。因此，只要不满足条件 1 和条件 2，用户 x 就会向 y 发送好友请求。那么 y 一定满足：
$$(0.5 * age[x] + 7) < age[y] <= age[x]$$

如果 `age[x] < 15 ` ，不存在满足的 age[y]，所以只要考虑 `age[x] >=15` 的情况。

定义双指针分别指向左右区间，直到满足上面的条件。

```ts
function numFriendRequests(ages: number[]): number {
  const n = ages.length;
  ages.sort((a, b) => a - b);
  let left = 0;
  let right = 0;
  let ans = 0;

  for (const age of ages) {
    // 不满足1和2条件
    if (age < 15) continue;
    // (0.5 * age[x] + 7,age[x]]区间
    while (ages[left] <= 0.5 * age + 7) {
      left++;
    }
    while (right + 1 < n && ages[right + 1] <= age) {
      right++;
    }

    ans += right - left;
  }

  return ans;
}
```
