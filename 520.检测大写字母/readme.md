# 题目

我们定义，在以下情况时，单词的大写用法是正确的：

- 全部字母都是大写，比如 "USA" 。
- 单词中所有字母都不是大写，比如 "leetcode" 。
- 如果单词不只含有一个字母，只有首字母大写， 比如 "Google" 。

给你一个字符串 word 。如果大写用法正确，返回 true ；否则，返回 false 。

# 示例

```
输入：word = "USA"
输出：true
```

```
输入：word = "FlaG"
输出：false
```

# 题解

## 模拟

根据题意可以看出：

- 第一个字母小写，那么第二个字母也必须小写
- 剩下的其他的字母必须跟第二个字符大小写相符合

```ts
function detectCapitalUse(word: string): boolean {
  const n = word.length;
  // 判断是否是小写
  const isLowerCase = (char: string): boolean => {
    return char === char.toLowerCase();
  };
  // 第一个字母小写，第二个字母也需要小写
  if (n >= 2 && isLowerCase(word[0]) && !isLowerCase(word[1])) {
    return false;
  }
  // 第一个字母无论大小写，后面所有字母都需要跟第二个字母相符
  for (let i = 2; i < n; i++) {
    if (isLowerCase(word[i]) !== isLowerCase(word[1])) {
      return false;
    }
  }
  return true;
}
```
