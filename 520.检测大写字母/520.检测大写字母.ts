/*
 * @lc app=leetcode.cn id=520 lang=typescript
 *
 * [520] 检测大写字母
 */

// @lc code=start
function detectCapitalUse(word: string): boolean {
  const n = word.length;
  const isLowerCase = (char: string): boolean => {
    return char === char.toLowerCase();
  };
  // 第一个字母小写，第二个字母也需要小写
  if (n >= 2 && isLowerCase(word[0]) && !isLowerCase(word[1])) {
    return false;
  }
  // 第一个字母无论大小写，后面所有字母都需要跟第二个字母相符
  for (let i = 2; i < n; i++) {
    if (isLowerCase(word[i]) !== isLowerCase(word[1])) {
      return false;
    }
  }
  return true;
}
// @lc code=end
