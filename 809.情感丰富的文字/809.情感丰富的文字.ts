/*
 * @lc app=leetcode.cn id=809 lang=typescript
 *
 * [809] 情感丰富的文字
 */

// @lc code=start
function expand(s: string, word: string): boolean {
  let m = s.length;
  let n = word.length;
  let i = 0;
  let j = 0;
  while (i < m && j < n) {
    if (s[i] !== word[j]) return false;
    const letter = s[i];
    // 当前字符在s中出现的连续次数
    let countI = 0;
    while (i < m && s[i] === letter) {
      countI++, i++;
    }
    // 当前字符在word中出现的连续次数
    let countJ = 0;
    while (j < n && word[j] === letter) {
      countJ++, j++;
    }
    // 无法扩张的情况
    if (countI < countJ) return false;
    if (countI !== countJ && countI < 3) return false;
  }
  return i === m && j === n;
}

function expressiveWords(s: string, words: string[]): number {
  let ans = 0;
  for (const word of words) {
    if (expand(s, word)) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
