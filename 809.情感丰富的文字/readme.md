# 题目

有时候人们会用重复写一些字母来表示额外的感受，比如 `"hello" -> "heeellooo", "hi" -> "hiii"`。我们将相邻字母都相同的一串字符定义为相同字母组，例如："h", "eee", "ll", "ooo"。

对于一个给定的字符串 S ，如果另一个单词能够通过将一些字母组扩张从而使其和 S 相同，我们将这个单词定义为可扩张的（stretchy）。扩张操作定义如下：选择一个字母组（包含字母 c ），然后往其中添加相同的字母 c 使其长度达到 3 或以上。

例如，以 "hello" 为例，我们可以对字母组 "o" 扩张得到 "hellooo"，但是无法以同样的方法得到 "helloo" 因为字母组 "oo" 长度小于 3。此外，我们可以进行另一种扩张 "ll" -> "lllll" 以获得 "helllllooo"。如果 `s = "helllllooo"`，那么查询词 "hello" 是可扩张的，因为可以对它执行这两种扩张操作使得 `query = "hello" -> "hellooo" -> "helllllooo" = s`。

输入一组查询单词，输出其中可扩张的单词数量。

提示：

- $1 \leq s.length, words.length \leq 100$
- $1 \leq words[i].length \leq 100$
- s 和所有在 words 中的单词都只由小写字母组成。

# 示例

```
s = "heeellooo"
words = ["hello", "hi", "helo"]
输出：1
解释：
我们能通过扩张 "hello" 的 "e" 和 "o" 来得到 "heeellooo"。
我们不能通过扩张 "helo" 来得到 "heeellooo" 因为 "ll" 的长度小于 3
```

# 题解

## 双指针

根据题意，可以看出扩张的定义：

- s 中的连续字符数量需要大于等于 word 中的连续字符数量
- s 中的连续字符数量需要大于等于 3

我们遍历 words，使用双指针分别指向 s 和 word ，按照以上定义，遍历检查两个字符串，全部遍历完毕，表示可以扩展，更新答案。

```ts
function expand(s: string, word: string): boolean {
  let m = s.length;
  let n = word.length;
  let i = 0;
  let j = 0;
  while (i < m && j < n) {
    if (s[i] !== word[j]) return false;
    const letter = s[i];
    // 当前字符在s中出现的连续次数
    let countI = 0;
    while (i < m && s[i] === letter) {
      countI++, i++;
    }
    // 当前字符在word中出现的连续次数
    let countJ = 0;
    while (j < n && word[j] === letter) {
      countJ++, j++;
    }
    // 无法扩张的情况
    if (countI < countJ) return false;
    if (countI !== countJ && countI < 3) return false;
  }
  return i === m && j === n;
}

function expressiveWords(s: string, words: string[]): number {
  let ans = 0;
  for (const word of words) {
    if (expand(s, word)) {
      ans++;
    }
  }
  return ans;
}
```
