# 题目

给你一个数组 `arr` ，该数组表示一个从 `1` 到 `n` 的数字排列。有一个长度为 `n` 的二进制字符串，该字符串上的所有位最初都设置为 `0` 。

在从 `1` 到 `n` 的每个步骤 `i` 中（假设二进制字符串和 `arr` 都是从 `1` 开始索引的情况下），二进制字符串上位于位置 `arr[i]` 的位将会设为 `1` 。

给你一个整数 `m` ，请你找出二进制字符串上存在长度为 `m` 的一组 `1` 的最后步骤。一组 `1` 是一个连续的、由 `1` 组成的子串，且左右两边不再有可以延伸的 `1` 。

返回存在长度 **恰好** 为 `m` 的 一组 `1` 的最后步骤。如果不存在这样的步骤，请返回 `-1` 。

提示：

- $n \equiv arr.length$
- $1 \leq n \leq 10^5$
- $1 \leq arr[i] \leq n$
- `arr` 中的所有整数 **互不相同**
- $1 \leq m \leq arr.length$

# 示例

```
输入：arr = [3,5,1,2,4], m = 1
输出：4
解释：
步骤 1："00100"，由 1 构成的组：["1"]
步骤 2："00101"，由 1 构成的组：["1", "1"]
步骤 3："10101"，由 1 构成的组：["1", "1", "1"]
步骤 4："11101"，由 1 构成的组：["111", "1"]
步骤 5："11111"，由 1 构成的组：["11111"]
存在长度为 1 的一组 1 的最后步骤是步骤 4 。
```

```
输入：arr = [3,1,5,4,2], m = 2
输出：-1
解释：
步骤 1："00100"，由 1 构成的组：["1"]
步骤 2："10100"，由 1 构成的组：["1", "1"]
步骤 3："10101"，由 1 构成的组：["1", "1", "1"]
步骤 4："10111"，由 1 构成的组：["1", "111"]
步骤 5："11111"，由 1 构成的组：["11111"]
不管是哪一步骤都无法形成长度为 2 的一组 1 。
```

# 题解

我们维护一个 `n+1` 大小的并查集，如果当前位置变为 `1` ，那么就将当前位置和前一个位置合并，这样有两个结点形成的连通分量就表示为单独的 `1` 。依次类推， `k` 个连续的 `1` ，需要有 `k + 1` 个结点连通。

遍历 `arr` , 每次将 `(arr[i], arr[i] - 1)` 合并， 如果并查集中存在长度为 `m + 1` 的连通分量，就更新答案。

## 并查集

```ts
class Union {
  data: number[];
  size: number[];
  // 连通的节点数为i个的分组数量
  cnt: number[];
  constructor(n: number) {
    this.data = new Array(n + 1);
    this.size = new Array(n + 1);
    this.cnt = new Array(n + 2).fill(0);
    this.cnt[1] = n + 1;
    for (let i = 0; i <= n; i++) {
      this.data[i] = i;
      this.size[i] = 1;
    }
  }
  find(x: number) {
    if (this.data[x] === x) return x;
    return (this.data[x] = this.find(this.data[x]));
  }
  merge(x: number, y: number) {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.data[rx] = ry;
    this.cnt[this.size[rx]]--;
    this.cnt[this.size[ry]]--;
    this.size[ry] += this.size[rx];
    this.cnt[this.size[ry]]++;
  }
}

function findLatestStep(arr: number[], m: number): number {
  const n = arr.length;
  const union = new Union(n);
  let ans = -1;
  for (let i = 0; i < n; i++) {
    union.merge(arr[i], arr[i] - 1);
    // 如果连通分量中的节点数有 m+1 个，更新答案
    if (union.cnt[m + 1]) ans = i + 1;
  }

  return ans;
}
```

```cpp
class Union
{
public:
    vector<int> data, size, count;
    Union(int n) : data(n + 1), size(n + 1), count(n + 2, 0)
    {
        for (int i = 0; i <= n; i++)
        {
            data[i] = i;
            size[i] = 1;
        }
        count[1] = n + 1;
    }
    int find(int x)
    {
        if (data[x] == x)
            return x;
        return (data[x] = find(data[x]));
    }
    void merge(int x, int y)
    {
        int rx = find(x), ry = find(y);
        if (rx == ry)
            return;
        if (size[rx] < size[ry])
        {
            swap(rx, ry);
        }
        data[rx] = ry;
        count[size[rx]]--;
        count[size[ry]]--;
        size[ry] += size[rx];
        count[size[ry]]++;
    }
};

class Solution
{
public:
    int findLatestStep(vector<int> &arr, int m)
    {
        int n = arr.size(), ans = -1;
        Union uf(n);
        for (int i = 0; i < n; i++)
        {
            uf.merge(arr[i], arr[i] - 1);
            if (uf.count[m + 1])
                ans = i + 1;
        }
        return ans;
    }
};
```
