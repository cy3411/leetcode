# 题目

给定一个整数 `num`，将其转化为 **7 进制**，并以字符串形式输出。

提示：

- `-107 <= num <= 107`

# 示例

```
输入: num = 100
输出: "202"
```

# 题解

## 进制转换

进制转换，就是把值转换成具体的数字表现形式。

个位数： `num % 7`
剩余的需要处理的值： `num / 7`

我们可以通过上面的逻辑，来逐步转换数字。需要注意的就是负数的情况。

```ts
function convertToBase7(num: number): string {
  if (num === 0) return '0';
  // 处理负数
  const flag = num < 0 ? '-' : '';
  let ans = '';
  while (num !== 0) {
    ans = String(Math.abs(num) % 7) + ans;
    num = (num / 7) >> 0;
  }

  return flag ? flag + ans : ans;
}
```

```cpp
class Solution
{
public:
    string convertToBase7(int num)
    {
        if (num == 0)
        {
            return "0";
        }
        int flag = num < 0 ? -1 : 1;
        num = abs(num);
        string ans;
        while (num)
        {
            ans.push_back(num % 7 + '0');
            num /= 7;
        }
        if (flag == -1)
        {
            ans.push_back('-');
        }
        reverse(ans.begin(), ans.end());
        return ans;
    };
};
```
