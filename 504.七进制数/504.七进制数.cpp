/*
 * @lc app=leetcode.cn id=504 lang=cpp
 *
 * [504] 七进制数
 */

// @lc code=start
class Solution
{
public:
    string convertToBase7(int num)
    {
        if (num == 0)
        {
            return "0";
        }
        int flag = num < 0 ? -1 : 1;
        num = abs(num);
        string ans;
        while (num)
        {
            ans.push_back(num % 7 + '0');
            num /= 7;
        }
        if (flag == -1)
        {
            ans.push_back('-');
        }
        reverse(ans.begin(), ans.end());
        return ans;
    };
};
// @lc code=end
