/*
 * @lc app=leetcode.cn id=504 lang=typescript
 *
 * [504] 七进制数
 */

// @lc code=start
function convertToBase7(num: number): string {
  if (num === 0) return '0';
  // 处理负数
  const flag = num < 0 ? '-' : '';
  let ans = '';
  while (num !== 0) {
    ans = String(Math.abs(num) % 7) + ans;
    num = (num / 7) >> 0;
  }

  return flag ? flag + ans : ans;
}
// @lc code=end
