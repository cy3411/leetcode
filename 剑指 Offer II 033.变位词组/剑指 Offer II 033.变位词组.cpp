class Solution
{
public:
    vector<vector<string>> groupAnagrams(vector<string> &strs)
    {
        unordered_map<string, int> m;
        vector<vector<string>> ans;

        for (auto &str : strs)
        {
            string temp = str;
            sort(temp.begin(), temp.end());
            if (m.count(temp) == 1)
            {
                ans[m[temp]].push_back(str);
            }
            else
            {
                m[temp] = ans.size();
                ans.push_back(vector<string>{str});
            }
        }

        return ans;
    }
};