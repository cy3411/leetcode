// @algorithm @lc id=1000275 lang=typescript
// @title sfvd7V
// @test(["eat","tea","tan","ate","nat","bat"])=[["bat"],["nat","tan"],["ate","eat","tea"]]
function groupAnagrams(strs: string[]): string[][] {
  const base = 'a'.charCodeAt(0);
  const getKey = (count: number[]): string => {
    let res = '';
    for (let i = 0; i < 26; i++) {
      if (count[i] === 0) continue;
      const char = String.fromCharCode(base + i);
      res += char + count[i];
    }
    return res;
  };

  const map = new Map<string, string[]>();
  for (const str of strs) {
    const count = new Array(26).fill(0);
    for (const c of str) {
      count[c.charCodeAt(0) - base]++;
    }
    const key = getKey(count);

    if (!map.has(key)) {
      map.set(key, []);
    }
    map.get(key).push(str);
  }

  return Array.from(map.values());
}
