# 题目

给定一个字符串数组 `strs` ，将 **变位词** 组合在一起。 可以按任意顺序返回结果列表。

注意：若两个字符串中每个字符出现的次数都相同，则称它们互为变位词。

提示：

- $1 \leq strs.length \leq 10^4$
- $0 \leq strs[i].length \leq 100$
- `strs[i]` 仅包含小写字母

注意：[本题与主站 49 题相同](https://leetcode-cn.com/problems/group-anagrams/)

# 示例

```
输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
```

```
输入: strs = [""]
输出: [[""]]
```

# 题解

## 哈希表

统计 `strs` 中每个元素中字符出现的次数，然后使用这个统计次数做为哈希表的 key，将 `strs` 中的元素放入哈希表中。

最后将哈希表中分好组的元素放入结果列表中。

```ts
function groupAnagrams(strs: string[]): string[][] {
  const base = 'a'.charCodeAt(0);
  const getKey = (count: number[]): string => {
    let res = '';
    for (let i = 0; i < 26; i++) {
      if (count[i] === 0) continue;
      const char = String.fromCharCode(base + i);
      res += char + count[i];
    }
    return res;
  };

  const map = new Map<string, string[]>();
  for (const str of strs) {
    const count = new Array(26).fill(0);
    for (const c of str) {
      count[c.charCodeAt(0) - base]++;
    }
    const key = getKey(count);

    if (!map.has(key)) {
      map.set(key, []);
    }
    map.get(key).push(str);
  }

  return Array.from(map.values());
}
```

将 `strs` 中的元素排序后做为 `key`,然后将对应的 `ans` 下标存入哈希表，同步更新 `ans` 中的元素。

如果哈希表表中有，将当前元素插入到对应的 `ans` 数组中；否则，创建一个新的数组，并将当前元素插入到新数组中，并存入 `ans` 中。

```cpp
class Solution
{
public:
    vector<vector<string>> groupAnagrams(vector<string> &strs)
    {
        unordered_map<string, int> m;
        vector<vector<string>> ans;

        for (auto &str : strs)
        {
            string temp = str;
            sort(temp.begin(), temp.end());
            if (m.count(temp) == 1)
            {
                ans[m[temp]].push_back(str);
            }
            else
            {
                m[temp] = ans.size();
                ans.push_back(vector<string>{str});
            }
        }

        return ans;
    }
};
```
