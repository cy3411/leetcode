# 题目

给定一个字符串 `s` ，验证 `s` 是否是 **回文串** ，只考虑字母和数字字符，可以忽略字母的大小写。

本题中，将空字符串定义为有效的 **回文串** 。

提示：

- $1 \leq s.length \leq 2 * 10^5$
- 字符串 `s` 由 `ASCII` 字符组成

注意：[本题与主站 125 题相同](https://leetcode-cn.com/problems/valid-palindrome/)

# 示例

```
输入: s = "A man, a plan, a canal: Panama"
输出: true
解释："amanaplanacanalpanama" 是回文串
```

```
输入: s = "race a car"
输出: false
解释："raceacar" 不是回文串
```

# 题解

## 双指针

```ts
function isPalindrome(s: string): boolean {
  const n = s.length;
  let l = 0;
  let r = n - 1;
  // 判断字符合法性
  const isValidaChars = (c: string) => {
    if (c >= 'a' && c <= 'z') return true;
    if (c >= 'A' && c <= 'Z') return true;
    if (c >= '0' && c <= '9') return true;
    return false;
  };
  while (l < r) {
    // 跳过非法字符
    while (l < n && !isValidaChars(s[l])) l++;
    while (r >= 0 && !isValidaChars(s[r])) r--;
    // 都是非法字符
    if (l === n || r === 0) return true;
    // 比较首尾字符
    if (s[l].toLocaleLowerCase() !== s[r].toLocaleLowerCase()) return false;
    l++, r--;
  }

  return true;
}
```

```cpp
class Solution {
public:
    int isValidaChar (char &c) {
      if (c >= 'A' && c <= 'Z') {
        c += 'a' - 'A';
        return 1;
      }
      if (c >= 'a' && c <= 'z') {
        return 1;
      }
      if (c >= '0' && c <= '9') {
        return 1;
      }
      return 0;
    }
    bool isPalindrome(string s) {
      int n = s.size();
      int l = 0, r = n - 1;
      while (l < r) {
        while (l < n && !isValidaChar(s[l])) {
          l++;
        }
        while (r >= 0 && !isValidaChar(s[r])) {
          r--;
        }
        if (l == n || r == 0)
        {
          return true;
        }
        if (s[l] != s[r]) {
          return false;
        }
        l++, r--;
      }
      return true;
    }
};
```
