class Solution
{
public:
    int isValidaChar(char &c)
    {
        if (c >= 'A' && c <= 'Z')
        {
            c += 'a' - 'A';
            return 1;
        }
        if (c >= 'a' && c <= 'z')
        {
            return 1;
        }
        if (c >= '0' && c <= '9')
        {
            return 1;
        }
        return 0;
    }
    bool isPalindrome(string s)
    {
        int n = s.size();
        int l = 0, r = n - 1;
        while (l < r)
        {
            while (l < n && !isValidaChar(s[l]))
            {
                l++;
            }
            while (r >= 0 && !isValidaChar(s[r]))
            {
                r--;
            }
            if (l == n || r == 0)
            {
                return true;
            }
            if (s[l] != s[r])
            {
                return false;
            }
            l++, r--;
        }
        return true;
    }
};