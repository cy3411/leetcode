# 题目

给定字符串 s 和字符串数组 words, 返回 words[i] 中是 s 的子序列的单词个数 。

字符串的 子序列 是从原始字符串中生成的新字符串，可以从中删去一些字符(可以是 none)，而不改变其余字符的相对顺序。

- 例如， “ace” 是 “abcde” 的子序列。

提示:

- $1 \leq s.length \leq 5 * 10^4$
- $1 \leq words.length \leq 5000$
- $1 \leq words[i].length \leq 50$
- words[i]和 s 都只由小写字母组成。

# 示例

```
输入: s = "abcde", words = ["a","bb","acd","ace"]
输出: 3
解释: 有三个是 s 的子序列的单词: "a", "acd", "ace"。
```

```
输入: s = "dsahjpjauf", words = ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"]
输出: 2
```

# 题解

## 二分搜索

使用二分搜索定位子序列中每个字符的位置是否匹配源字符串，匹配的话表示源字符串的子序列，否则不是。

```ts
function binarysearch(target: number, idxs: number[]): number {
  let l = 0;
  let r = idxs.length - 1;
  let mid: number;
  while (l < r) {
    mid = l + Math.floor((r - l) / 2);
    if (idxs[mid] > target) {
      r = mid;
    } else {
      l = mid + 1;
    }
  }
  return idxs[l];
}

function numMatchingSubseq(s: string, words: string[]): number {
  const map = new Map<string, number[]>();
  const n = s.length;
  // 建立字符和索引的对应关系
  for (let i = 0; i < n; i++) {
    const letter = s[i];
    if (!map.has(letter)) {
      map.set(letter, []);
    }
    map.get(letter)!.push(i);
  }

  let ans = words.length;
  // 遍历words
  for (const word of words) {
    // 单词长度超过 s ，肯定不是 s 的子序列
    if (word.length > s.length) {
      ans--;
      continue;
    }
    const m = word.length;
    let p = -1;
    for (let i = 0; i < m; i++) {
      const idxs = map.get(word[i]);
      // 字符在s中不存在或者s中字符的最大索引小于当前索引,表示不是子序列
      if (!idxs || idxs[idxs.length - 1] <= p) {
        ans--;
        break;
      }
      // 在当前字符的索引数组中找到的第一个位置大于 p 的索引
      p = binarysearch(p, idxs);
    }
  }

  return ans;
}
```
