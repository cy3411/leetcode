/*
 * @lc app=leetcode.cn id=1996 lang=typescript
 *
 * [1996] 游戏中弱角色的数量
 */

// @lc code=start
function numberOfWeakCharacters(properties: number[][]): number {
  // 按照第一个数字升序，第二个数字降序排序
  properties.sort((a, b) => a[0] - b[0] || b[1] - a[1]);

  let ans = 0;
  let max = 0;
  // 逆序遍历，同时维护第二个数字的最大值
  // 当前元素第二个数字小于max，则说明当前元素是弱角色
  for (let i = properties.length - 1; i >= 0; i--) {
    max = Math.max(max, properties[i][1]);
    if (properties[i][1] < max) {
      ans++;
    }
  }

  return ans;
}
// @lc code=end
