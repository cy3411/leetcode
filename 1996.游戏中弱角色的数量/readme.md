# 题目

你正在参加一个多角色游戏，每个角色都有两个主要属性：**攻击** 和 **防御** 。给你一个二维整数数组 `properties` ，其中 `properties[i] = [attacki, defensei]` 表示游戏中第 `i` 个角色的属性。

如果存在一个其他角色的攻击和防御等级 **都严格高于** 该角色的攻击和防御等级，则认为该角色为 **弱角色** 。更正式地，如果认为角色 `i` 弱于 存在的另一个角色 `j` ，那么 $attack_j > attack_i$ 且 $defense_j > defense_i$ 。

返回 弱角色 的数量。

提示：

- $\color{burlywood}2 \leq properties.length \leq 10^5$
- $\color{burlywood}properties[i].length == 2$
- $\color{burlywood}1 \leq attacki, defensei \leq 10^5$

# 示例

```
输入：properties = [[5,5],[6,3],[3,6]]
输出：0
解释：不存在攻击和防御都严格高于其他角色的角色。
```

```
输入：properties = [[2,2],[3,3]]
输出：1
解释：第一个角色是弱角色，因为第二个角色的攻击和防御严格大于该角色。
```

# 题解

我们可以按照角色的攻击值从大到小顺序遍历，同时记录已经遍历的过的防御值的最大值 maxDef。

对于当前角色 a， 如果 a 的防御值严格小于 maxDef，那么存在防御值 a 高的角色 b。如果此时 b 的攻击值也严格大于 a， 那么 a 是弱角色。

我们对 properties 排序，对于攻击值相同的角色，按照防御从降序排列。这就可以保证已经遍历过的最大防御值的角色 b 的 maxDef 严格大于当前角色 a 的防御值，则此时 b 的攻击值也严格大于 a 的攻击值。

## 排序

```ts
function numberOfWeakCharacters(properties: number[][]): number {
  // 按照第一个数字升序，第二个数字降序排序
  properties.sort((a, b) => a[0] - b[0] || b[1] - a[1]);

  let ans = 0;
  let max = 0;
  // 逆序遍历，同时维护第二个数字的最大值
  // 当前元素第二个数字小于max，则说明当前元素是弱角色
  for (let i = properties.length - 1; i >= 0; i--) {
    max = Math.max(max, properties[i][1]);
    if (properties[i][1] < max) {
      ans++;
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int numberOfWeakCharacters(vector<vector<int>> &properties)
    {
        sort(properties.begin(), properties.end(), [](const vector<int> &a, const vector<int> &b)
             { return a[0] == b[0] ? a[1] < b[1] : a[0] > b[0]; });

        int ans = 0;
        int maxDef = 0;

        for (auto prop : properties)
        {
            maxDef = max(maxDef, prop[1]);
            if (prop[1] < maxDef)
            {
                ans++;
            }
        }

        return ans;
    }
};
```
