/*
 * @lc app=leetcode.cn id=1996 lang=cpp
 *
 * [1996] 游戏中弱角色的数量
 */

// @lc code=start
class Solution
{
public:
    int numberOfWeakCharacters(vector<vector<int>> &properties)
    {
        sort(properties.begin(), properties.end(), [](const vector<int> &a, const vector<int> &b)
             { return a[0] == b[0] ? a[1] < b[1] : a[0] > b[0]; });

        int ans = 0;
        int maxDef = 0;

        for (auto prop : properties)
        {
            maxDef = max(maxDef, prop[1]);
            if (prop[1] < maxDef)
            {
                ans++;
            }
        }

        return ans;
    }
};
// @lc code=end
