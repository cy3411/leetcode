# 题目

给你两个单链表的头节点 **headA** 和 **headB** ，请你找出并返回两个单链表相交的起始节点。如果两个链表没有交点，返回 **null** 。

图示两个链表在节点 **c1** 开始相交：

![28haHs.md.png](https://z3.ax1x.com/2021/06/04/28haHs.md.png)

题目数据 保证 整个链式结构中不存在环。

注意，函数返回结果后，链表必须 **保持其原始结构** 。

# 示例

```
输入：intersectVal = 8, listA = [4,1,8,4,5], listB = [5,0,1,8,4,5], skipA = 2, skipB = 3
输出：Intersected at '8'
解释：相交节点的值为 8 （注意，如果两个链表相交则不能为 0）。
从各自的表头开始算起，链表 A 为 [4,1,8,4,5]，链表 B 为 [5,0,1,8,4,5]。
在 A 中，相交节点前有 2 个节点；在 B 中，相交节点前有 3 个节点。
```

# 题解

## 哈希表

使用哈希表存储 `headA` 中所有的节点，然后遍历 `headB` 中节点是否出现在哈希表中，出现的话直接返回 `true`。 因为第一个出现的节点肯定是相交节点。

```ts
function getIntersectionNode(headA: ListNode | null, headB: ListNode | null): ListNode | null {
  const hash: Set<ListNode> = new Set();

  let p = headA;
  while (p !== null) {
    hash.add(p);
    p = p.next;
  }

  // 第一个出现在哈希表中的节点就是相交节点
  p = headB;
  while (p !== null) {
    if (hash.has(p)) return p;
    p = p.next;
  }

  return null;
}
```

## 双指针

定义 `p1`,`p2` 头指针分别指向 `headA` 和 `headB`.

同时遍历两个链表:

- 如果 `p1` 到达末尾，就把 `p1` 指向 `headB`.
- 如果 `p2` 到达末尾，就把 `p2` 指向 `headA`.

这样就会把 2 个链表的所有节点都对比一次，直到 2 个指针都指向 `null`.

```ts
function getIntersectionNode(headA: ListNode | null, headB: ListNode | null): ListNode | null {
  if (headA === null || headB === null) return null;

  let p1 = headA;
  let p2 = headB;
  //第一个满足条件的就是相交节点
  while (p1 !== p2) {
    p1 = p1.next ? p1.next : headB;
    p2 = p2.next ? p2.next : headA;
  }

  return p1;
}
```
