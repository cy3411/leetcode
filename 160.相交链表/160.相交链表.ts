/*
 * @lc app=leetcode.cn id=160 lang=typescript
 *
 * [160] 相交链表
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

function getIntersectionNode(headA: ListNode | null, headB: ListNode | null): ListNode | null {
  if (headA === null || headB === null) return null;

  let p1 = headA;
  let p2 = headB;
  //第一个满足条件的就是相交节点
  while (p1 !== p2) {
    p1 = p1.next ? p1.next : headB;
    p2 = p2.next ? p2.next : headA;
  }

  return p1;
}
// @lc code=end
