/*
 * @lc app=leetcode.cn id=1800 lang=cpp
 *
 * [1800] 最大升序子数组和
 */

// @lc code=start
class Solution {
public:
    int maxAscendingSum(vector<int> &nums) {
        int n = nums.size();
        int ans = 0, i = 0;
        while (i < n) {
            int sum = nums[i++];
            while (i < n && nums[i] > nums[i - 1]) {
                sum += nums[i++];
            }
            ans = max(ans, sum);
        }
        return ans;
    }
};
// @lc code=end
