# 题目

给你一个正整数组成的数组 nums ，返回 nums 中一个 升序 子数组的最大可能元素和。

子数组是数组中的一个连续数字序列。

已知子数组 $[nums_l, nums_{l+1}, ..., nums_{r-1}, nums_r]$ ，若对所有 $i（l\leq i < r）$，$nums_i  < nums{i+1}$ 都成立，则称这一子数组为 升序 子数组。注意，大小为 1 的子数组也视作 升序 子数组。

提示：

- $1 \leq nums.length \leq 100$
- $1 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [10,20,30,5,10,50]
输出：65
解释：[5,10,50] 是元素和最大的升序子数组，最大元素和为 65
```

```
输入：nums = [10,20,30,40,50]
输出：150
解释：[10,20,30,40,50] 是元素和最大的升序子数组，最大元素和为 150 。
```

# 题解

## 遍历

```js
function maxAscendingSum(nums: number[]): number {
  const n = nums.length;
  let ans = 0;
  let sum = 0;

  for (let i = 0; i < n; i++) {
    // 当前是第一个元素或者当前元素非升序，重置元素和sum
    if (i === 0 || nums[i] <= nums[i - 1]) {
      sum = nums[i];
    } else {
      sum += nums[i];
    }
    ans = Math.max(ans, sum);
  }

  return ans;
}
```

## 动态规划

我们设 dp[i] 为 nums[i] 为结尾最大的升序子数组的元素和。那么状态转移为：

$$
dp[i] = \begin{cases}
    dp[i-1] + nums[i] , & nums[i] > nums[i -1] \\
    nums[i], & nums[i] <= nums[i-1]
\end{cases}
$$

由于只需要考虑前一个状态，所以使用滚动数组来优化。

```cpp
class Solution {
public:
    int maxAscendingSum(vector<int> &nums) {
        int n = nums.size();
        int ans = 0, i = 0;
        while (i < n) {
            int sum = nums[i++];
            while (i < n && nums[i] > nums[i - 1]) {
                sum += nums[i++];
            }
            ans = max(ans, sum);
        }
        return ans;
    }
};
```
