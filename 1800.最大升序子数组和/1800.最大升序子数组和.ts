/*
 * @lc app=leetcode.cn id=1800 lang=typescript
 *
 * [1800] 最大升序子数组和
 */

// @lc code=start
function maxAscendingSum(nums: number[]): number {
  const n = nums.length;
  let ans = 0;
  let sum = 0;

  for (let i = 0; i < n; i++) {
    // 当前是第一个元素或者当前元素非升序，重置元素和sum
    if (i === 0 || nums[i] <= nums[i - 1]) {
      sum = nums[i];
    } else {
      sum += nums[i];
    }
    ans = Math.max(ans, sum);
  }

  return ans;
}
// @lc code=end
