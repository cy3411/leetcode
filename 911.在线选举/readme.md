# 题目

给你两个整数数组 persons 和 times 。在选举中，第 i 张票是在时刻为 times[i] 时投给候选人 persons[i] 的。

对于发生在时刻 t 的每个查询，需要找出在 t 时刻在选举中领先的候选人的编号。

在 t 时刻投出的选票也将被计入我们的查询之中。在平局的情况下，最近获得投票的候选人将会获胜。

实现 TopVotedCandidate 类：

- TopVotedCandidate(int[] persons, int[] times) 使用 persons 和 times 数组初始化对象。
- int q(int t) 根据前面描述的规则，返回在时刻 t 在选举中领先的候选人的编号。

提示：

- $1 \leq persons.length \leq 5000$
- $times.length == persons.length$
- $0 \leq persons[i] < persons.length$
- $0 \leq times[i] \leq 10^9$
- `times` 是一个严格递增的有序数组
- $times[0] \leq t \leq 10^9$
- 每个测试用例最多调用 $10^4$ 次 q

# 示例

```
输入：
["TopVotedCandidate", "q", "q", "q", "q", "q", "q"]
[[[0, 1, 1, 0, 0, 1, 0], [0, 5, 10, 15, 20, 25, 30]], [3], [12], [25], [15], [24], [8]]
输出：
[null, 0, 1, 1, 0, 0, 1]

解释：
TopVotedCandidate topVotedCandidate = new TopVotedCandidate([0, 1, 1, 0, 0, 1, 0], [0, 5, 10, 15, 20, 25, 30]);
topVotedCandidate.q(3); // 返回 0 ，在时刻 3 ，票数分布为 [0] ，编号为 0 的候选人领先。
topVotedCandidate.q(12); // 返回 1 ，在时刻 12 ，票数分布为 [0,1,1] ，编号为 1 的候选人领先。
topVotedCandidate.q(25); // 返回 1 ，在时刻 25 ，票数分布为 [0,1,1,0,0,1] ，编号为 1 的候选人领先。（在平局的情况下，1 是最近获得投票的候选人）。
topVotedCandidate.q(15); // 返回 0
topVotedCandidate.q(24); // 返回 0
topVotedCandidate.q(8); // 返回 1
```

# 题解

## 预处理+二分

将每个时间点的 top 预处理。

题目给出 times 是单调递增的，所以可以用二分查找。找到最近一个小于等于 t 的时间点，然后返回预处理的信息。

```ts
class TopVotedCandidate {
  tops: number[];
  times: number[];
  constructor(persons: number[], times: number[]) {
    this.times = times;
    // 预处理每个时间点的最高得票数的人，方便q函数查询
    this.tops = [];
    let voteCount = new Map<number, number>();
    let maxVote = -1;
    voteCount.set(-1, -1);

    for (let person of persons) {
      voteCount.set(person, (voteCount.get(person) || 0) + 1);
      if (voteCount.get(person) >= voteCount.get(maxVote)) {
        maxVote = person;
      }
      this.tops.push(maxVote);
    }
  }

  q(t: number): number {
    let l = 0;
    let r = this.times.length - 1;
    // 二分查找最后一个小于等于t的时间点
    while (l < r) {
      let mid = l + ((r - l + 1) >> 1);
      if (this.times[mid] <= t) {
        l = mid;
      } else {
        r = mid - 1;
      }
    }
    return this.tops[l];
  }
}
```
