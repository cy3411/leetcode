// @algorithm @lc id=100274 lang=typescript
// @title fei-bo-na-qi-shu-lie-lcof
//  @test(0)  result: 1 ,expect: 0
function fib(n: number): number {
  if (n === 0) return 0;
  const mod = 1e9 + 7;
  let a = 0;
  let b = 1;
  for (let i = 2; i <= n; i++) {
    const c = (a + b) % mod;
    a = b;
    b = c;
  }
  return b;
}
