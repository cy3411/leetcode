/*
 * @lc app=leetcode.cn id=349 lang=javascript
 *
 * [349] 两个数组的交集
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersection = function (nums1, nums2) {
  // 排序
  nums1.sort((a, b) => a - b);
  nums2.sort((a, b) => a - b);

  let i = 0,
    j = 0;
  const result = [];

  while (i < nums1.length && j < nums2.length) {
    const [num1, num2] = [nums1[i], nums2[j]];
    if (num1 === num2) {
      // 相等时判断是否存入结果
      if (!result.length || num1 !== result[result.length - 1]) {
        result.push(num1);
      }
      i++;
      j++;
    } else if (num1 < num2) {
      i++;
    } else {
      j++;
    }
  }

  return result;
};
// @lc code=end

/**
 * 时间复杂度：O(mlog⁡m+nlog⁡n)，其中 m 和 n 分别是两个数组的长度。
 * 空间复杂度：O(log⁡m+log⁡n)，其中 m 和 n 分别是两个数组的长度。空间复杂度主要取决于排序使用的额外空间
 */
