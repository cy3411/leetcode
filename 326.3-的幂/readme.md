# 题目

给定一个整数，写一个函数来判断它是否是 `3` 的幂次方。如果是，返回 `true` ；否则，返回 `false` 。

整数 `n` 是 `3` 的幂次方需满足：存在整数 `x` 使得 $\color{#ABA187}n == 3^x$

提示：

- $-2^{31} \leq n \leq 2^{31} - 1$

进阶：

- 你能不使用循环或者递归来完成本题吗？

# 示例

```
输入：n = 27
输出：true
```

```
输入：n = 0
输出：false
```

# 题解

## 试除法

我们不算的将 `n` 除以 `3`，直到 `n=1`。

如果过程中 `n` 不能被 `3` 整除，那么就不是 `3` 的幂。

```ts
function isPowerOfThree(n: number): boolean {
  // 特判
  if (n <= 0) return false;
  // 试除法
  while (n !== 1) {
    if (n % 3 !== 0) return false;
    n = n / 3;
  }

  return true;
}
```

## 判断 n 是否是最大 3 的幂的约数

题目给定了 `n` 的取值范围是 `32` 位有符号整数。

只要判断 `n` 是否是 `32` 位有符号整数中 `3` 的最大幂的约数即可。

```ts
function isPowerOfThree(n: number): boolean {
  // 特判
  if (n <= 0) return false;
  // 32位有符号整数中最大的3的幂
  const max = 3 ** 19;

  return max % n === 0;
}
```
