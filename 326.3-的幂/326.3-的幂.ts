/*
 * @lc app=leetcode.cn id=326 lang=typescript
 *
 * [326] 3的幂
 */

// @lc code=start
function isPowerOfThree(n: number): boolean {
  // 特判
  if (n <= 0) return false;
  // 32位有符号整数中最大的3的幂
  const max = 3 ** 19;

  return max % n === 0;
}
// @lc code=end
