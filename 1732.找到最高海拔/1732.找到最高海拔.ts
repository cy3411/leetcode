/*
 * @lc app=leetcode.cn id=1732 lang=typescript
 *
 * [1732] 找到最高海拔
 */

// @lc code=start
function largestAltitude(gain: number[]): number {
  const n = gain.length;
  const prefixSum = new Array(n + 1).fill(0);

  let ans = 0;
  for (let i = 0; i <= n; i++) {
    if (i !== 0) {
      prefixSum[i] = prefixSum[i - 1] + gain[i - 1];
    }
    ans = Math.max(ans, prefixSum[i]);
  }
  return ans;
}
// @lc code=end
