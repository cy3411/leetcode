#
# @lc app=leetcode.cn id=1732 lang=python3
#
# [1732] 找到最高海拔
#

# @lc code=start
class Solution:
    def largestAltitude(self, gain: List[int]) -> int:
        ans = prefixSum = 0
        for x in gain:
            prefixSum += x
            ans = max(ans, prefixSum)
        return ans
# @lc code=end
