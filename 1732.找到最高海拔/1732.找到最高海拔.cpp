/*
 * @lc app=leetcode.cn id=1732 lang=cpp
 *
 * [1732] 找到最高海拔
 */

// @lc code=start
class Solution {
public:
    int largestAltitude(vector<int> &gain) {
        int ans = 0, prefixSum = 0;
        for (int x : gain) {
            prefixSum = x + prefixSum;
            ans = max(ans, prefixSum);
        }
        return ans;
    }
};
// @lc code=end
