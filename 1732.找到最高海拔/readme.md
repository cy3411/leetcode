# 题目

有一个自行车手打算进行一场公路骑行，这条路线总共由 n + 1 个不同海拔的点组成。自行车手从海拔为 0 的点 0 开始骑行。

给你一个长度为 n 的整数数组 gain ，其中 gain[i] 是点 i 和点 i + 1 的 净海拔高度差（$0 \leq i < n$）。请你返回 最高点的海拔 。

提示：

- $n \equiv gain.length$
- $1 \leq n \leq 100$
- $-100 \leq gain[i] \leq 100$

# 示例

```
输入：gain = [-5,1,5,0,-7]
输出：1
解释：海拔高度依次为 [0,-5,-4,1,1,-6] 。最高海拔为 1 。
```

```
输入：gain = [-4,-3,-2,-1,4,3,2]
输出：0
解释：海拔高度依次为 [0,-4,-7,-9,-10,-6,-3,-1] 。最高海拔为 0 。
```

# 题解

## 前缀和

对 gain 做前缀和操作，取前缀和中的最大值就是最高海拔。

```ts
function largestAltitude(gain: number[]): number {
  const n = gain.length;
  const prefixSum = new Array(n + 1).fill(0);

  let ans = 0;
  for (let i = 0; i <= n; i++) {
    if (i !== 0) {
      prefixSum[i] = prefixSum[i - 1] + gain[i - 1];
    }
    ans = Math.max(ans, prefixSum[i]);
  }
  return ans;
}
```

```cpp
class Solution {
public:
    int largestAltitude(vector<int> &gain) {
        int ans = 0, prefixSum = 0;
        for (int x : gain) {
            prefixSum = x + prefixSum;
            ans = max(ans, prefixSum);
        }
        return ans;
    }
};
```

```py
class Solution:
    def largestAltitude(self, gain: List[int]) -> int:
        ans = prefixSum = 0
        for x in gain:
            prefixSum += x
            ans = max(ans, prefixSum)
        return ans
```
