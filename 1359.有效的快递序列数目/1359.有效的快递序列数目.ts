/*
 * @lc app=leetcode.cn id=1359 lang=typescript
 *
 * [1359] 有效的快递序列数目
 */

// @lc code=start
function countOrders(n: number): number {
  const mod = 10 ** 9 + 7;
  let ans = 1;
  for (let i = 2; i <= n; i++) {
    ans = (ans * (2 * i ** 2 - i)) % mod;
  }

  return ans;
}
// @lc code=end
