# 题目

给你一个 `m` 行 `n` 列的二维网格 `grid` 和一个整数 `k`。你需要将 `grid` 迁移 `k` 次。

每次「迁移」操作将会引发下述活动：

- 位于 `grid[i][j]` 的元素将会移动到 `grid[i][j + 1]`。
- 位于 `grid[i][n - 1]` 的元素将会移动到 `grid[i + 1][0]`。
- 位于 `grid[m - 1][n - 1]` 的元素将会移动到 `grid[0][0]`。

请你返回 k 次迁移操作后最终得到的 二维网格。

提示：

- $m \equiv grid.length$
- $n \equiv grid[i].length$
- $1 \leq m \leq 50$
- $1 \leq n \leq 50$
- $-1000 \leq grid[i][j] \leq 1000$
- $0 \leq k \leq 100$

# 示例

[![jbLVL4.png](https://s1.ax1x.com/2022/07/20/jbLVL4.png)](https://imgtu.com/i/jbLVL4)

```
输入：grid = [[1,2,3],[4,5,6],[7,8,9]], k = 1
输出：[[9,1,2],[3,4,5],[6,7,8]]
```

# 题解

## 一维展开

设 m 和 n 分别为网格的行和列，可以将二维数组展开为一维数组，那么 `grid[i][j]` 对应一维的索引就是 `i * n + j`。 移动 `k` 次，就是将一维数组向右移动 k 个单位，那么移动后的索引就是 `idx = (i * n + j + k) % (m * n)`。

那么此时对应的二维索引就是 `i = idx / n, j = idx % n`。

遍历 grid ，按照上面的索引规则将 gird 的值赋值给新的数组，然后返回新数组。

```ts
function shiftGrid(grid: number[][], k: number): number[][] {
  const m = grid.length;
  const n = grid[0].length;
  const ans = new Array(m).fill(0).map(() => new Array(n).fill(0));
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      // 二维转一维，索引是(i * n + j)，加上位移k
      const idx = (i * n + j + k) % (m * n);
      // 一维转二维后，索引是(idx / n, idx % n)
      ans[(idx / n) >> 0][idx % n] = grid[i][j];
    }
  }
  return ans;
}
```

```cpp
class Solution {
public:
    vector<vector<int>> shiftGrid(vector<vector<int>> &grid, int k) {
        int m = grid.size(), n = grid[0].size();
        vector<vector<int>> ans(m, vector<int>(n));
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int idx = (i * n + j + k) % (m * n);
                ans[idx / n][idx % n] = grid[i][j];
            }
        }
        return ans;
    }
};
```

```py
class Solution:
    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:
        m, n = len(grid), len(grid[0])
        ans = [[0] * n for _ in range(m)]
        for i in range(m):
            for j in range(n):
                idx = (i * n + j + k) % (m * n)
                ans[idx // n][idx % n] = grid[i][j]

        return ans
```
