/*
 * @lc app=leetcode.cn id=1260 lang=typescript
 *
 * [1260] 二维网格迁移
 */

// @lc code=start
function shiftGrid(grid: number[][], k: number): number[][] {
  const m = grid.length;
  const n = grid[0].length;
  const ans = new Array(m).fill(0).map(() => new Array(n).fill(0));
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      // 二维转一维，索引是(i * n + j)，加上位移k
      const idx = (i * n + j + k) % (m * n);
      // 一维转二维后，索引是(idx / n, idx % n)
      ans[(idx / n) >> 0][idx % n] = grid[i][j];
    }
  }
  return ans;
}
// @lc code=end
