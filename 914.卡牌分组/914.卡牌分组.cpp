/*
 * @lc app=leetcode.cn id=914 lang=cpp
 *
 * [914] 卡牌分组
 */

// @lc code=start
class Solution
{
public:
    bool hasGroupsSizeX(vector<int> &deck)
    {
        map<int, int> h;
        for (auto x : deck)
        {

            h[x]++;
        }

        int x = 0;
        for (auto d : h)
        {
            if (x == 0)
            {
                x = d.second;
            }
            else
            {
                x = gcd(x, d.second);
            }
            if (x == 1)
                break;
        }

        return x >= 2;
    }
};
// @lc code=end
