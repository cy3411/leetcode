# 题目

给定一副牌，每张牌上都写着一个整数。

此时，你需要选定一个数字 `X`，使我们可以将整副牌按下述规则分成 `1` 组或更多组：

- 每组都有 `X` 张牌。
- 组内所有的牌上都写着相同的整数。

仅当你可选的 $\color{burlywood}X \geq 2$ 时返回 `true`。

提示：

- $\color{burlywood}1 \leq deck.length \leq 10000$
- $\color{burlywood}0 \leq deck[i] < 10000$

# 示例

```
输入：[1,2,3,4,4,3,2,1]
输出：true
解释：可行的分组是 [1,1]，[2,2]，[3,3]，[4,4]
```

```
输入：[1,1,1,2,2,2,3,3]
输出：false
解释：没有满足要求的分组。
```

# 题解

## 最大公约数

根据题意可知，所有数字的数量都是 `X` 的倍数，所以可以求出所有数字的最大公约数，即为 `X`。

返回 `X` 是否大于 1 即可。

```ts
function hasGroupsSizeX(deck: number[]): boolean {
  const gcd = (a: number, b: number) => (b === 0 ? a : gcd(b, a % b));
  const map = new Map<number, number>();

  for (const num of deck) {
    map.set(num, (map.get(num) ?? 0) + 1);
  }

  let x = -1;
  // 计算所有元素数量的最大公约数
  for (const [_, count] of map) {
    if (x === -1) x = count;
    else x = gcd(x, count);
    if (x === 1) break;
  }
  // 最大公约数大于1即可
  return x > 1;
}
```

```cpp
class Solution
{
public:
    bool hasGroupsSizeX(vector<int> &deck)
    {
        map<int, int> h;
        for (auto x : deck)
        {

            h[x]++;
        }

        int x = 0;
        for (auto d : h)
        {
            if (x == 0)
            {
                x = d.second;
            }
            else
            {
                x = gcd(x, d.second);
            }
            if (x == 1)
                break;
        }

        return x >= 2;
    }
};
```
