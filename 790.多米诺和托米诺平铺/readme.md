# 题目

有两种形状的瓷砖：一种是 2 x 1 的多米诺形，另一种是形如 "L" 的托米诺形。两种形状都可以旋转。

[![zifseA.png](https://s1.ax1x.com/2022/11/12/zifseA.png)](https://imgse.com/i/zifseA)

给定整数 n ，返回可以平铺 2 x n 的面板的方法的数量。返回对 109 + 7 取模 的值。

平铺指的是每个正方形都必须有瓷砖覆盖。两个平铺不同，当且仅当面板上有四个方向上的相邻单元中的两个，使得恰好有一个平铺有一个瓷砖占据两个正方形。

提示：

- 1 <= n <= 1000

# 示例

[![zifTwn.png](https://s1.ax1x.com/2022/11/12/zifTwn.png)](https://imgse.com/i/zifTwn)

```
输入: n = 3
输出: 5
解释: 五种不同的方法如上所示。
```

# 题解

## 递推

定义 f(1) 为平铺 2 x n 矩阵的方案数。尝试计算前几项：

- $f(1) = 1$
- $f(2) = 2$
- $f(3) = f(2) + f(1) + 2$
- $f(4) = f(3) + f(2) + 2 * (f(1) + 1)$
- $f(5) = f(4) + f(3) + 2 * (f(2) + f(1) + 1)$
- -...
- $f(n-1) = f(n - 2) + f(n - 3) + 2 * (f(n - 4) + ... + 1)$
- $f(n) = f(n - 1) + f(n - 2) + 2 * (f(n - 3 + ... + 1))$

得到递归公式：

$$
f(n) - f(n - 1) = f(n - 1) + f(n - 3) \Rightarrow \\
f(n) = 2 * f(n - 1) + f(n - 3) (\text{ n >= 4})
$$

```ts
function numTilings(n: number): number {
  if (n === 1) return 1;
  const mod = 1e9 + 7;
  const f: number[] = new Array(n + 1).fill(0);
  f[0] = f[1] = 1;
  f[2] = 2;
  for (let i = 3; i <= n; i++) {
    f[i] = (f[i - 1] * 2 + f[i - 3]) % mod;
  }
  return f[n];
}
```
