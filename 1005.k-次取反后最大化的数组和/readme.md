# 题目

给你一个整数数组 `nums` 和一个整数 `k` ，按以下方法修改该数组：

选择某个下标 `i` 并将 `nums[i]` 替换为 `-nums[i]` 。
重复这个过程恰好 `k` 次。可以多次选择同一个下标 `i` 。

以这种方式修改数组后，返回数组 **可能的最大和** 。

提示：

- $1 \leq nums.length \leq 10^4$
- $-100 \leq nums[i] \leq 100$
- $1 \leq k \leq 10^4$

# 示例

```
输入：nums = [4,2,3], k = 1
输出：5
解释：选择下标 1 ，nums 变为 [4,-2,3] 。
```

```
输入：nums = [2,-3,-1,5,-4], k = 2
输出：13
解释：选择下标 (1, 4) ，nums 变为 [2,3,-1,5,4] 。
```

# 题解

## 优先队列

要想求可能的最大和，那么每次反转就要去反转最小的那个数，这样就可以保证每次反转后的损失或者收益都是最大的。

使用小顶堆，每次取出最小的数，反转后放入堆中，重复 k 次。

最后累加堆中的所有数即可。

```ts
class MinPQ {
  public heap: number[];
  private N: number;

  constructor(maxN: number) {
    this.heap = new Array(maxN + 1);
    this.N = 0;
  }

  public insert(value: number) {
    this.N++;
    this.heap[this.N] = value;
    this.swim(this.N);
  }

  public delMin() {
    const min = this.heap[1];
    this.exch(1, this.N--);
    this.sink(1);
    this.heap[this.N + 1] = null;
    return min;
  }

  private swim(k: number) {
    while (k > 1 && this.less(k, (k / 2) >> 0)) {
      this.exch(k, (k / 2) >> 0);
      k = (k / 2) >> 0;
    }
  }

  private sink(k: number) {
    while (2 * k <= this.N) {
      let j = 2 * k;
      if (j < this.N && this.less(j + 1, j)) j++;
      if (!this.less(j, k)) break;
      this.exch(k, j);
      k = j;
    }
  }

  private less(i: number, j: number) {
    return this.heap[i] < this.heap[j];
  }

  private exch(i: number, j: number) {
    [this.heap[i], this.heap[j]] = [this.heap[j], this.heap[i]];
  }
}

function largestSumAfterKNegations(nums: number[], k: number): number {
  // 小顶堆
  const minpq = new MinPQ(nums.length);
  const n = nums.length;
  // 元素入堆
  for (let i = 0; i < n; i++) {
    minpq.insert(nums[i]);
  }
  // 取k次反转
  // 每次取最小元素反转
  for (let i = 0; i < k; i++) {
    const min = minpq.delMin();
    minpq.insert(-min);
  }
  // 累加数组结果
  return minpq.heap.reduce((a, b) => a + b, 0);
}
```
