# 题目

给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。

# 示例

```
输入: s = "abcabcbb"
输出: 3
解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
```

```
输入: s = "bbbbb"
输出: 1
解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
```

# 题解

## 二分搜索

枚举 `[1,s.length]` 区间,然后依次去检查当前长度是否是无重复字串。这样做的话时间复杂度会是$O(n^2)$。

需要降低复杂度，我们可以看到枚举长度的过程，就是二分的 `01` 模型。所以可以将枚举过程通过二分来实现。

当前值验证失败，将最大长度减少来缩减范围。

```ts
function lengthOfLongestSubstring(s: string): number {
  // 验证长度为len，能否找到无重复的字串
  const isValida = (len: number, s: string): boolean => {
    const hash = new Array(256).fill(0);
    let k = 0;
    for (let i = 0; i < s.length; i++) {
      const idx = s.charCodeAt(i);
      if (hash[idx] === 0) k++;
      hash[idx] += 1;
      if (i >= len) {
        const idx = s.charCodeAt(i - len);
        hash[idx] -= 1;
        if (hash[idx] === 0) k--;
      }
      if (k === len) return true;
    }
    return false;
  };

  if (s.length === 0) return 0;

  let head = 1;
  let tail = s.length;
  let mid: number;
  while (head < tail) {
    // 二分查找区间长度
    mid = head + ((tail - head + 1) >> 1);
    if (isValida(mid, s)) head = mid;
    else tail = mid - 1;
  }

  return head;
}
```
