# 题目

给定两个单词 `word1` 和 `word2`，找到使得 `word1` 和 `word2` 相同所需的最小步数，每步可以删除任意一个字符串中的一个字符。

提示：

- 给定单词的长度不超过 `500`。
- 给定单词中的字符只含有小写字母。

# 示例

```
输入: "sea", "eat"
输出: 2
解释: 第一步将"sea"变为"ea"，第二步将"eat"变为"ea"
```

# 题解

## 动态规划

给定两个单词，删除掉部分字符后，剩下相同的部分就是公共子序列。

为了使删除的字符最少，那么剩下的部分必然是最长的公共子序列。

因此，可以计算两个单词的最长公共子序列，然后分别计算两个单词和公共子序列的长度之差，两者之和就是答案。

[1143.最长公共子序列](https://gitee.com/cy3411/leetcode/tree/master/1143.%E6%9C%80%E9%95%BF%E5%85%AC%E5%85%B1%E5%AD%90%E5%BA%8F%E5%88%97)

```ts
function minDistance(word1: string, word2: string): number {
  const m = word1.length;
  const n = word2.length;
  // dp[i][j]表示word1中前i个字符和word2中前j个字符的最长公共子序列的长度
  const dp = new Array(m + 1).fill(0).map((_) => new Array(n + 1).fill(0));

  for (let i = 1; i <= m; i++) {
    const a = word1[i - 1];
    for (let j = 1; j <= n; j++) {
      const b = word2[j - 1];
      if (a === b) {
        dp[i][j] = dp[i - 1][j - 1] + 1;
      } else {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }

  // 最长公共子序列长度
  const lsc = dp[m][n];
  // 两个单词减去最长公共子序列长度就是答案
  return m - lsc + n - lsc;
}
```
