# 题目

存在一个不含 0 的 环形 数组 nums ，每个 nums[i] 都表示位于下标 i 的角色应该向前或向后移动的下标个数：

- 如果 nums[i] 是正数，向前 移动 nums[i] 步
- 如果 nums[i] 是负数，向后 移动 nums[i] 步
  因为数组是 环形 的，所以可以假设从最后一个元素向前移动一步会到达第一个元素，而第一个元素向后移动一步会到达最后一个元素。

数组中的 循环 由长度为 k 的下标序列 seq ：

- 遵循上述移动规则将导致重复下标序列 seq[0] -> seq[1] -> ... -> seq[k - 1] -> seq[0] -> ...
- 所有 nums[seq[j]] 应当不是 全正 就是 全负
- k > 1

  如果 nums 中存在循环，返回 true ；否则，返回 false 。

提示：

- 1 <= nums.length <= 5000
- -1000 <= nums[i] <= 1000
- nums[i] != 0

# 示例

```
输入：nums = [2,-1,1,2,2]
输出：true
解释：存在循环，按下标 0 -> 2 -> 3 -> 0 。循环长度为 3 。
```

```
输入：nums = [-1,2]
输出：false
解释：按下标 1 -> 1 -> 1 ... 的运动无法构成循环，因为循环的长度为 1 。根据定义，循环的长度必须大于 1 。
```

# 题解

## 快慢指针

判断是否有环，可以使用快慢指针，看看是否能够相遇，相遇的话就是有环。

题意要求每次移动的序列全正或者全负，移动指针前需要判断一下，slow 和 fast 指针所指向的值是否满足条件。

还有一个限制，就是移动的序列长度要求大于 1，在指针相遇后，判断一下 slow 下一个次移动是否等于 slow 即可。

```ts
function circularArrayLoop(nums: number[]): boolean {
  const m = nums.length;
  const next = (idx: number): number => {
    // 避免负数的情况，保证范围在[0,m)
    return (((idx + nums[idx]) % m) + m) % m;
  };

  for (let i = 0; i < m; i++) {
    let slow = i;
    let fast = next(slow);
    while (nums[slow] * nums[fast] > 0 && nums[slow] * nums[next(fast)] > 0) {
      if (slow === fast) {
        // 如果下一步还是移动回来，表示这个循环区间长度<=1，不满足k>1条件
        if (slow === next(slow)) break;
        return true;
      }
      slow = next(slow);
      fast = next(next(fast));
    }
  }

  return false;
}
```

```cpp
class Solution
{
public:
    int getNext(int idx, int n, vector<int> &nums)
    {
        return ((idx + nums[idx] % n) + n) % n;
    }
    bool circularArrayLoop(vector<int> &nums)
    {
        int n = nums.size();
        for (int i = 0; i < n; i++)
        {
            int slow = i;
            int fast = getNext(i, n, nums);

            while (nums[slow] * nums[fast] > 0 && nums[slow] * nums[getNext(fast, n, nums)] > 0)
            {
                if (slow == fast)
                {
                    if (slow == getNext(slow, n, nums))
                        break;
                    return true;
                }
                slow = getNext(slow, n, nums);
                fast = getNext(getNext(fast, n, nums), n, nums);
            }
        }

        return false;
    }
};
```
