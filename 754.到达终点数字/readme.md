# 题目

在一根无限长的数轴上，你站在 0 的位置。终点在 target 的位置。

每次你可以选择向左或向右移动。第 n 次移动（从 1 开始），可以走 n 步。

返回到达终点需要的最小移动次数。

注意:

- `target` 是在$\color{goldenrod}[-10^9, 10^9]$范围中的非零整数。

# 示例

```
输入: target = 3
输出: 2
解释:
第一次移动，从 0 到 1 。
第二次移动，从 1 到 3 。
```

# 题解

## 模拟

题意就是走 `k` 步，可以到达 `m`， `m>=target`，并且 `m-target` 必须为偶数。

走 `k` 步后，超过了 `target` ，超出的部分为 `delta` 。

那么需要在 `[1,m] `范围内找到一些数字和等于 `delta/2`（就是往回走），所以 `delta` 必须为偶数，即可以达到 `target`。

```ts
function reachNumber(target: number): number {
  target = Math.abs(target);
  // 初始化，最少需要走k步，才能超过target
  let k = Math.floor(Math.sqrt(2 * target));
  // 如果没超过target，则k++
  while ((k * (k + 1)) / 2 < target) k++;
  // 计算走完k步与target的差值
  let delta = (k * (k + 1)) / 2 - target;
  // 如果delta为奇数，继续往后走一步
  while (delta & 1) {
    k++;
    delta += k;
  }

  return k;
}
```
