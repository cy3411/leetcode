/*
 * @lc app=leetcode.cn id=754 lang=typescript
 *
 * [754] 到达终点数字
 */

// @lc code=start
function reachNumber(target: number): number {
  target = Math.abs(target);
  // 初始化，最少需要走k步，才能超过target
  let k = Math.floor(Math.sqrt(2 * target));
  // 如果没超过target，则k++
  while ((k * (k + 1)) / 2 < target) k++;
  // 计算走完k步与target的差值
  let delta = (k * (k + 1)) / 2 - target;
  // 如果delta为奇数，需要多走一步
  while (delta & 1) {
    k++;
    delta += k;
  }

  return k;
}
// @lc code=end
