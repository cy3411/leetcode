# 题目

给你一个 $n * n$ 矩阵 grid ，矩阵由若干 `0` 和 `1` 组成。请你用四叉树表示该矩阵 `grid` 。

你需要返回能表示矩阵的 **四叉树** 的根结点。

注意，当 `isLeaf` 为 `False` 时，你可以把 `True` 或者 `False` 赋值给节点，两种值都会被判题机制 接受 。

四叉树数据结构中，每个内部节点只有四个子节点。此外，每个节点都有两个属性：

- `val`：储存叶子结点所代表的区域的值。`1` 对应 `True`，`0` 对应 `False`；
- `isLeaf`: 当这个节点是一个叶子结点时为 `True`，如果它有 `4` 个子节点则为 `False` 。

```
class Node {
    public boolean val;
    public boolean isLeaf;
    public Node topLeft;
    public Node topRight;
    public Node bottomLeft;
    public Node bottomRight;
}
```

我们可以按以下步骤为二维区域构建四叉树：

- 如果当前网格的值相同（即，全为 `0` 或者全为 `1`），将 isLeaf 设为 True ，将 `val` 设为网格相应的值，并将四个子节点都设为 `Null` 然后停止。
- 如果当前网格的值不同，将 `isLeaf` 设为 `False`， 将 `val` 设为任意值，然后如下图所示，将当前网格划分为四个子网格。
- 使用适当的子网格递归每个子节点。

[![LzAPeS.png](https://s1.ax1x.com/2022/04/30/LzAPeS.png)](https://imgtu.com/i/LzAPeS)

如果你想了解更多关于四叉树的内容，可以参考 [wiki](https://en.wikipedia.org/wiki/Quadtree) 。

**四叉树格式：**

输出为使用层序遍历后四叉树的序列化形式，其中 `null` 表示路径终止符，其下面不存在节点。

它与二叉树的序列化非常相似。唯一的区别是节点以列表形式表示 `[isLeaf, val]` 。

如果 `isLeaf` 或者 `val` 的值为 `True` ，则表示它在列表 `[isLeaf, val]` 中的值为 `1` ；如果 `isLeaf` 或者 `val` 的值为 `False` ，则表示值为 `0` 。

提示：

- $n \equiv grid.length \equiv grid[i].length$
- $n \equiv 2^x$ 其中 $0 \leq x \leq 6$

# 示例

[![LzAAij.png](https://s1.ax1x.com/2022/04/30/LzAAij.png)](https://imgtu.com/i/LzAAij)

```
输入：grid = [[0,1],[1,0]]
输出：[[0,1],[1,0],[1,1],[1,1],[1,0]]
解释：此示例的解释如下：
请注意，在下面四叉树的图示中，0 表示 false，1 表示 True 。
```

[![LzAZzq.png](https://s1.ax1x.com/2022/04/30/LzAZzq.png)](https://imgtu.com/i/LzAZzq)

# 题解

## 递归

根据题意，矩阵中所有节点的值全部相等，就是一个叶子节点。

若不相等，将矩阵划分为同等大小的四个子矩阵，继续上面的判断，并将当前矩阵设为四个子矩阵的父节点。

可以通过递归的方式来处理上面的逻辑。

```ts
function construct(grid: number[][]): Node | null {
  const isCheck = (r0: number, r1: number, c0: number, c1: number): boolean => {
    for (let i = r0; i < r1; i++) {
      for (let j = c0; j < c1; j++) {
        if (grid[r0][c0] !== grid[i][j]) {
          return false;
        }
      }
    }
    return true;
  };
  const dfs = (r0: number, r1: number, c0: number, c1: number): Node | null => {
    const allSame = isCheck(r0, r1, c0, c1);

    // 所有网格值相同，表示是叶子节点
    if (allSame) {
      return new Node(grid[r0][c0] === 1, true);
    }

    const ans = new Node(
      false,
      false,
      dfs(r0, (r0 + r1) >> 1, c0, (c0 + c1) >> 1),
      dfs(r0, (r0 + r1) >> 1, (c0 + c1) >> 1, c1),
      dfs((r0 + r1) >> 1, r1, c0, (c0 + c1) >> 1),
      dfs((r0 + r1) >> 1, r1, (c0 + c1) >> 1, c1)
    );

    return ans;
  };

  const n = grid.length;
  return dfs(0, n, 0, n);
}
```

```cpp
class Solution
{
public:
    int isAllSame(vector<vector<int>> &grid, int r0, int r1, int c0, int c1)
    {
        for (int i = r0; i < r1; i++)
        {
            for (int j = c0; j < c1; j++)
            {
                if (grid[r0][c0] != grid[i][j])
                {
                    return 0;
                }
            }
        }
        return 1;
    }
    Node *dfs(vector<vector<int>> &grid, int r0, int r1, int c0, int c1)
    {
        int allSame = isAllSame(grid, r0, r1, c0, c1);
        if (allSame)
        {
            return new Node(grid[r0][c0], true);
        }
        Node *root = new Node(
            false,
            false,
            dfs(grid, r0, (r0 + r1) >> 1, c0, (c0 + c1) >> 1),
            dfs(grid, r0, (r0 + r1) >> 1, (c0 + c1) >> 1, c1),
            dfs(grid, (r0 + r1) >> 1, r1, c0, (c0 + c1) >> 1),
            dfs(grid, (r0 + r1) >> 1, r1, (c0 + c1) >> 1, c1));

        return root;
    }
    Node *construct(vector<vector<int>> &grid)
    {
        int n = grid.size();
        return dfs(grid, 0, n, 0, n);
    }
};
```
