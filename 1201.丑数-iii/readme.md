# 题目

给你四个整数：`n 、a 、b 、c` ，请你设计一个算法来找出第 n 个丑数。

丑数是可以被 `a` 或 `b` 或 `c` 整除的 正整数 。

# 示例

```
输入：n = 3, a = 2, b = 3, c = 5
输出：4
解释：丑数序列为 2, 3, 4, 5, 6, 8, 9, 10... 其中第 3 个是 4。
```

```
输入：n = 4, a = 2, b = 3, c = 4
输出：6
解释：丑数序列为 2, 3, 4, 6, 8, 9, 10, 12... 其中第 4 个是 6。
```

# 题解

## 二分搜索

我们通过查找某个区间内的数字可以得到多少个丑数，然后通过二分搜索来找到第 n 个丑数。

```ts
function nthUglyNumber(n: number, a: number, b: number, c: number): number {
  // 最大公约数
  const gcd = (a: number, b: number): number => (b === 0 ? a : gcd(b, a % b));
  // 最小公倍数
  const lcm = (a: number, b: number): number => (a * b) / gcd(a, b);
  // 找到x中有多少个丑数
  const find = (x: number): number => {
    let ab = lcm(a, b);
    let ac = lcm(a, c);
    let bc = lcm(b, c);
    let abc = lcm(a, lcm(b, c));
    return (
      Math.floor(x / a) +
      Math.floor(x / b) +
      Math.floor(x / c) -
      Math.floor(x / ab) -
      Math.floor(x / ac) -
      Math.floor(x / bc) +
      Math.floor(x / abc)
    );
  };

  let l = 1;
  let r = n * Math.min(a, b, c);
  let mid: number;
  // 二分查找区间范围内有几个丑数
  // 如果区间范围内有几个丑数等于n，则当前数字就是第n个丑数
  while (l < r) {
    mid = l + Math.floor((r - l) / 2);

    if (find(mid) < n) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }

  return l;
}
```
