# 题目

给你一个仅由字符 '0' 和 '1' 组成的字符串 s 。一步操作中，你可以将任一 '0' 变成 '1' ，或者将 '1' 变成 '0' 。

交替字符串 定义为：如果字符串中不存在相邻两个字符相等的情况，那么该字符串就是交替字符串。例如，字符串 "010" 是交替字符串，而字符串 "0100" 不是。

返回使 s 变成 交替字符串 所需的 最少 操作数。

提示：

- $1 \leq s.length \leq 10^4$
- s[i] 是 '0' 或 '1'

# 示例

```
输入：s = "0100"
输出：1
解释：如果将最后一个字符变为 '1' ，s 就变成 "0101" ，即符合交替字符串定义。
```

```
输入：s = "10"
输出：0
解释：s 已经是交替字符串。
```

# 题解

## 模拟

交替字符串有两种，一种是 1 开头的交替字符串，一种是 0 开头的交替字符串，取两者中变换次数做少的即可。

两种字符串变换次数的和最少等于 s 的长度，所以只需要计算一种变换的次数 cnt。最后返回 cnt 和 字符串长度减去 cnt 次数的最小值。

```ts
function minOperations(s: string): number {
  const n = s.length;
  let cnt = 0;
  for (let i = 0; i < n; i++) {
    const c = s[i];
    // 计算首位是0的交替字符串需要替换的次数
    if (c !== String.fromCharCode('0'.charCodeAt(0) + (i & 1))) {
      cnt++;
    }
  }

  return Math.min(cnt, n - cnt);
}
```

```cpp
class Solution {
public:
    int minOperations(string s) {
        int n = s.size(), cnt = 0;
        for (int i = 0; i < n; i++) {
            char c = s[i];
            if (c != (char)('0' + (i & 1))) {
                cnt++;
            }
        }
        return min(cnt, n - cnt);
    }
};
```

```py
class Solution:
    def minOperations(self, s: str) -> int:
        n, cnt = len(s), 0
        for i, c in enumerate(s):
            if int(c) != (i & 1):
                cnt += 1
        return min(cnt, n - cnt)
```
