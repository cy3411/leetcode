/*
 * @lc app=leetcode.cn id=1758 lang=typescript
 *
 * [1758] 生成交替二进制字符串的最少操作数
 */

// @lc code=start
function minOperations(s: string): number {
  const n = s.length;
  let cnt = 0;
  for (let i = 0; i < n; i++) {
    const c = s[i];
    // 计算首位是0的交替字符串需要替换的次数
    if (c !== String.fromCharCode('0'.charCodeAt(0) + (i & 1))) {
      cnt++;
    }
  }

  return Math.min(cnt, n - cnt);
}
// @lc code=end
