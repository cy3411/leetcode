/*
 * @lc app=leetcode.cn id=189 lang=javascript
 *
 * [189] 旋转数组
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var rotate = function (nums, k) {
  const size = nums.length;
  let visited = -1;
  let i = 0;
  if ((k %= size) === 0) return;

  while (++visited < size) {
    let prev = nums[i];
    let j = i;
    while (i !== (j = (j + k) % size)) {
      const temp = nums[j];
      nums[j] = prev;
      prev = temp;
      visited++;
    }
    nums[i++] = prev;
  }
};
// @lc code=end
