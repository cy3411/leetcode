# 题目

最初记事本上只有一个字符 `'A'` 。你每次可以对这个记事本进行两种操作：

- `Copy All`（复制全部）：复制这个记事本中的所有字符（不允许仅复制部分字符）。
- `Paste`（粘贴）：粘贴 **上一次** 复制的字符。

给你一个数字 `n` ，你需要使用最少的操作次数，在记事本上输出 **恰好** `n` 个 `'A'` 。返回能够打印出 `n` 个 `'A'` 的最少操作次数。

提示：

- `1 <= n <= 1000`

# 示例

```
输入：3
输出：3
解释：
最初, 只有一个字符 'A'。
第 1 步, 使用 Copy All 操作。
第 2 步, 使用 Paste 操作来获得 'AA'。
第 3 步, 使用 Paste 操作来获得 'AAA'。
```

# 题解

## 动态规划

**状态**

`dp[i][j]`表示记事本上有`i`个字符，粘贴板上有`j`个字符时的最小操作次数。

**转移**

最后一步是 `paste`，粘贴板上不会有变化，那么

$$dp[i][j]=dp[i-j][j]+1$$

最后一步是 `copy`，此时记事本和粘贴板字符数量一样($i=j$)，那么

$$dp[i][j]=min(f[i][x]+1), \text{0<=x<i}$$

另外粘贴板上的字符数量不会超过记事本字符数量的一半（每次都是 `copy all` 后的 `paste`），`x`的遍历次数可以减少到`i/2`。

**边界**

$$
\begin{cases}
dp[1][0] = 0, & \text{记事本上1个字符，粘贴板上0个字符}  \\
dp[1][1] = 1，& \text{记事本上1个字符，粘贴板上1个字符}
\end{cases}
$$

```ts
function minSteps(n: number): number {
  // dp[i][j]，记事本上i个字符，粘贴板上j个字符的最小操作次数
  const dp = new Array(n + 1).fill(0).map((_) => new Array(n + 1).fill(1001));
  // 初始化
  dp[1][0] = 0;
  dp[1][1] = 1;
  // 验证i到n的过程
  for (let i = 2; i <= n; i++) {
    let min = 1001;
    for (let j = 0; j <= (i / 2) >> 0; j++) {
      // 最后一次操作是paste
      dp[i][j] = dp[i - j][j] + 1;
      min = Math.min(min, dp[i][j]);
    }
    // 最后一次操作是copy，i必然等于j
    dp[i][i] = min + 1;
  }

  return Math.min(...dp[n]);
}
```
