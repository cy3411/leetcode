# 题目

在一个大小为 `n` 且 `n` 为 **偶数** 的链表中，对于 $\color{burlywood} 0 \leq i \leq (n / 2) - 1$ 的 `i` ，第 `i` 个节点（下标从 `0` 开始）的孪生节点为第 `(n-1-i)` 个节点 。

- 比方说，`n = 4` 那么节点 `0` 是节点 `3` 的孪生节点，节点 `1` 是节点 `2` 的孪生节点。这是长度为 `n = 4` 的链表中所有的孪生节点。

**孪生和** 定义为一个节点和它孪生节点两者值之和。

给你一个长度为偶数的链表的头节点 `head` ，请你返回链表的 `最大孪生和` 。

提示：

- 链表的节点数目是 $\color{burlywood} [2, 10^5]$ 中的 偶数 。
- $\color{burlywood} 1 \leq Node.val \leq 10^5$

# 示例

[![HKLFPA.png](https://s4.ax1x.com/2022/02/07/HKLFPA.png)](https://imgtu.com/i/HKLFPA)

```
输入：head = [5,4,2,1]
输出：6
解释：
节点 0 和节点 1 分别是节点 3 和 2 的孪生节点。孪生和都为 6 。
链表中没有其他孪生节点。
所以，链表的最大孪生和是 6 。
```

[![HKLZKf.png](https://s4.ax1x.com/2022/02/07/HKLZKf.png)](https://imgtu.com/i/HKLZKf)

```
输入：head = [4,2,2,3]
输出：7
解释：
链表中的孪生节点为：
- 节点 0 是节点 3 的孪生节点，孪生和为 4 + 3 = 7 。
- 节点 1 是节点 2 的孪生节点，孪生和为 2 + 2 = 4 。
所以，最大孪生和为 max(7, 4) = 7 。
```

# 题解

根据题意，链表中的孪生节点就是将链表平均分为前后两断，前半段顺序访问，后半段逆序访问，将对应位置求和。

最后取最大值即可。

## 递归

利用递归回溯的特性，我们将后半段放入到递归中，在回溯的时候统计孪生和，同时更新最大结果。

```ts
function pairSum(head: ListNode | null): number {
  let p = head;
  let q = head;
  // 找到中间节点，p为后半段第一个结点
  // 快慢指针
  while (q !== null) {
    p = p.next;
    q = q.next.next;
  }

  let ans = Number.MIN_SAFE_INTEGER;
  // 利用递归回溯的特性，计算孪生和
  const getValue = (p: ListNode | null): void => {
    if (p === null) {
      return;
    }
    getValue(p.next);
    ans = Math.max(ans, p.val + head.val);
    head = head.next;
  };

  getValue(p);

  return ans;
}
```

将后半段链表反转后，再计算孪生和即可。

```cpp
class Solution
{
public:
    ListNode *reverse(ListNode *p)
    {
        ListNode *head = NULL;
        while (p)
        {
            ListNode *next = p->next;
            p->next = head;
            head = p;
            p = next;
        }
        return head;
    }

    int pairSum(ListNode *head)
    {
        ListNode *p = head, *q = head;
        while (q != NULL)
        {
            p = p->next;
            q = q->next->next;
        }

        ListNode *tail = reverse(p);

        // 计算孪生和
        int ans = -1;
        while (tail != NULL)
        {
            ans = max(ans, tail->val + head->val);
            tail = tail->next;
            head = head->next;
        }

        return ans;
    }
};
```
