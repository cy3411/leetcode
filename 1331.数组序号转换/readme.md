# 题目

给你一个整数数组 `arr` ，请你将数组中的每个元素替换为它们排序后的序号。

序号代表了一个元素有多大。序号编号的规则如下：

- 序号从 1 开始编号。
- 一个元素越大，那么序号越大。如果两个元素相等，那么它们的序号相同。
- 每个数字的序号都应该尽可能地小。

提示：

- $0 \leq arr.length \leq 10^5$
- $-10^9 \leq arr[i] \leq 10^9$

# 示例

```
输入：arr = [40,10,20,30]
输出：[4,1,2,3]
解释：40 是最大的元素。 10 是最小的元素。 20 是第二小的数字。 30 是第三小的数字。
```

```
输入：arr = [100,100,100]
输出：[1,1,1]
解释：所有元素有相同的序号。
```

# 题解

## 索引排序

建立一个索引数组，然后按照升序排序。

最后遍历索引数组，按序生成排名。

```ts
function arrayRankTransform(arr: number[]): number[] {
  const n = arr.length;
  // 索引排序
  const idxs = new Array(n);
  for (let i = 0; i < n; i++) {
    idxs[i] = i;
  }
  idxs.sort((a, b) => arr[a] - arr[b]);

  const ans = new Array(n);
  // 上一个排名的数字，如果以下一个数字和它相同，则排名相同
  let pre: number = -1;
  let rank = 1;
  // 根据排序后的索引，计算排名
  for (let i = 0; i < n; i++) {
    if (i === 0 || arr[idxs[i]] !== pre) {
      ans[idxs[i]] = rank++;
      pre = arr[idxs[i]];
    } else {
      ans[idxs[i]] = ans[idxs[i - 1]];
    }
  }

  return ans;
}
```

## 排序+哈希表

拷贝一个新数组，然后按照升序排序。

使用哈希表保存新数组的各元素的序号，然后遍历原数组，按序生成排名。

```cpp
class Solution {
public:
    vector<int> arrayRankTransform(vector<int> &arr) {
        int n = arr.size();
        // 拷贝一个新的数组，升序排序
        vector<int> sortedArr = arr;
        sort(sortedArr.begin(), sortedArr.end());
        // 创建一个map，key是原数组的值，value是排名
        unordered_map<int, int> rankMap;

        for (auto &x : sortedArr) {
            if (rankMap.find(x) == rankMap.end()) {
                rankMap[x] = rankMap.size() + 1;
            }
        }

        vector<int> ans(n);
        for (int i = 0; i < n; i++) {
            ans[i] = rankMap[arr[i]];
        }

        return ans;
    }
};
```

```py
class Solution:
    def arrayRankTransform(self, arr: List[int]) -> List[int]:
        randMap = {v: i for i, v in enumerate(sorted(set(arr)), 1)}
        return [randMap[v] for v in arr]
```
