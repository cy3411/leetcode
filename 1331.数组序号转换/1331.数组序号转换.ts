/*
 * @lc app=leetcode.cn id=1331 lang=typescript
 *
 * [1331] 数组序号转换
 */

// @lc code=start
function arrayRankTransform(arr: number[]): number[] {
  const n = arr.length;
  // 索引排序
  const idxs = new Array(n);
  for (let i = 0; i < n; i++) {
    idxs[i] = i;
  }
  idxs.sort((a, b) => arr[a] - arr[b]);

  const ans = new Array(n);
  // 上一个排名的数字，如果以下一个数字和它相同，则排名相同
  let pre: number = -1;
  let rank = 1;
  // 根据排序后的索引，计算排名
  for (let i = 0; i < n; i++) {
    if (i === 0 || arr[idxs[i]] !== pre) {
      ans[idxs[i]] = rank++;
      pre = arr[idxs[i]];
    } else {
      ans[idxs[i]] = ans[idxs[i - 1]];
    }
  }

  return ans;
}
// @lc code=end
