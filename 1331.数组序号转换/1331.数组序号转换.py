#
# @lc app=leetcode.cn id=1331 lang=python3
#
# [1331] 数组序号转换
#

# @lc code=start
class Solution:
    def arrayRankTransform(self, arr: List[int]) -> List[int]:
        randMap = {v: i for i, v in enumerate(sorted(set(arr)), 1)}
        return [randMap[v] for v in arr]
# @lc code=end
