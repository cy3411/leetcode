/*
 * @lc app=leetcode.cn id=1331 lang=cpp
 *
 * [1331] 数组序号转换
 */

// @lc code=start
class Solution {
public:
    vector<int> arrayRankTransform(vector<int> &arr) {
        int n = arr.size();
        // 拷贝一个新的数组，升序排序
        vector<int> sortedArr = arr;
        sort(sortedArr.begin(), sortedArr.end());
        // 创建一个map，key是原数组的值，value是排名
        unordered_map<int, int> rankMap;

        for (auto &x : sortedArr) {
            if (rankMap.find(x) == rankMap.end()) {
                rankMap[x] = rankMap.size() + 1;
            }
        }

        vector<int> ans(n);
        for (int i = 0; i < n; i++) {
            ans[i] = rankMap[arr[i]];
        }

        return ans;
    }
};
// @lc code=end
