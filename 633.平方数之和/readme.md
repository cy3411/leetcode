# 题目
给定一个非负整数 c ，你要判断是否存在两个整数 `a` 和 `b`，使得 `a2 + b2 = c `。

# 示例
```
输入：c = 5
输出：true
解释：1 * 1 + 2 * 2 = 5
```
```
输入：c = 3
输出：false
```
# 题解
## 暴力
遍历 `a` ,求解 $b=\sqrt{c-a^2}$ 。判断如果 `b` 是一个整数，就是想要的答案。
```js
function judgeSquareSum(c: number): boolean {
  for (let a = 0; a * a <= c; a++) {
    const b = Math.sqrt(c - a * a);
    if (Number.isInteger(b)) return true;
  }
  return false;
};
```

## 双指针
由于 a 和 b 的范围均为 $[0,\sqrt{c}]$，因此我们可以使用「双指针」在 $[0,\sqrt{c}]$ 范围进行扫描：
+ $a^2+b^2 = c$, 返回true
+ $a^2+b^2 < c$, `a++`
+ $a^2+b^2 > c$, `b--`

```js
function judgeSquareSum(c: number): boolean {
  let left = 0;
  let right = Math.sqrt(c) >> 0;

  while (left <= right) {
    const sum = left ** 2 + right ** 2;
    if (sum === c) {
      return true;
    } else if (sum < c) {
      left++;
    } else if (sum > c) {
      right--;
    }
  }
  return false;
}
```

## 费马平方和定理
一个非负整数 `c` 如果能够表示为两个整数的平方和，当且仅当 `c` 的所有形如` 4k+3` 的质因子的幂均为偶数。

证明见[这里](https://wstein.org/edu/124/lectures/lecture21/lecture21/node2.html)。

```js
function judgeSquareSum(c: number): boolean {
  for (let base = 2; base ** 2 <= c; base++) {
    if (c % base !== 0) continue;

    let exp = 0;
    while (c % base === 0) {
      c /= base;
      exp++;
    }
    // 根据平方和定理验证
    if (base % 4 === 3 && exp % 2 !== 0) return false;
  }

  return c % 4 !== 3;
```