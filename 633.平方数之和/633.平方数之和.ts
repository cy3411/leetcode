/*
 * @lc app=leetcode.cn id=633 lang=typescript
 *
 * [633] 平方数之和
 */

// @lc code=start
function judgeSquareSum(c: number): boolean {
  for (let base = 2; base ** 2 <= c; base++) {
    if (c % base !== 0) continue;

    let exp = 0;
    while (c % base === 0) {
      c /= base;
      exp++;
    }
    // 根据平方和定理验证
    if (base % 4 === 3 && exp % 2 !== 0) return false;
  }

  return c % 4 !== 3;
}
// @lc code=end
