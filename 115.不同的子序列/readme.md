# 题目
给定一个字符串 s 和一个字符串 t ，计算在 s 的子序列中 t 出现的个数。

字符串的一个 子序列 是指，通过删除一些（也可以不删除）字符且不干扰剩余字符相对位置所组成的新字符串。（例如，"ACE" 是 "ABCDE" 的一个子序列，而 "AEC" 不是）

题目数据保证答案符合 32 位带符号整数范围。

提示：

+ 0 <= s.length, t.length <= 1000
+ s 和 t 由英文字母组成

# 示例
```
输入：s = "rabbbit", t = "rabbit"
输出：3
解释：
如下图所示, 有 3 种可以从 s 中得到 "rabbit" 的方案。
(上箭头符号 ^ 表示选取的字母)
rabbbit
^^^^ ^^
rabbbit
^^ ^^^^
rabbbit
^^^ ^^^
```
# 方法
问题的意思就是从s串中挑选字符，去匹配t串，有几种方式，重点是这个选。

例如：s='rabbbit',t='rabbit'
+ 末尾字符相同
  + s选择匹配，继续递归子问题'rabbbi'和'rabbi'
  + s不选择匹配，继续递归子问题'rabbbi'和'rabbit'
+ 末尾字符不同
  + s无法选择匹配，继续递归子问题'rabbbi'和'rabbit'

考虑递归的出口:
+ t成空串，s可以匹配，返回1。考虑s和t会同时空串，这个要先返回。
+ s成空串，那肯定匹配不了，返回0

## 自上而下递归
纯递归的方式有重复子问题，提交会超时，需要减枝。
```js
/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var numDistinct = function (s, t) {
  const sLen = s.length;
  const tLen = t.length;
  const memo = new Array(sLen).fill(0).map((_) => new Array(tLen).fill(-1));

  const helper = (i, j) => {
    if (j < 0) {
      return 1;
    }
    if (i < 0) {
      return 0;
    }
    if (memo[i][j] !== -1) {
      return memo[i][j];
    }

    if (s[i] === t[j]) {
      memo[i][j] = helper(i - 1, j) + helper(i - 1, j - 1);
    } else {
      memo[i][j] = helper(i - 1, j);
    }
    return memo[i][j];
  };

  return helper(sLen - 1, tLen - 1);
};
```


## 自下而上的DP
从上面的递归公式，我们可以找出递推关系，
```js
if (s[i] === t[j]) {
      memo[i][j] = helper(i - 1, j) + helper(i - 1, j - 1);
    } else {
      memo[i][j] = helper(i - 1, j);
```
稍作修改就是动态规划
+ dp[i][j]，前i个字符的s串中包含前j个字符的t串的次数
+ 转移方程式
  + s[i - 1] === t[j - 1],dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j]
  + s[i - 1] !== t[j - 1],dp[i][j] = dp[i - 1][j]
+ base case
  + j===0,dp[i][0] = 1
  + i===0,dp[0][j] = 0

```js
/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var numDistinct = function (s, t) {
  const sLen = s.length;
  const tLen = t.length;
  // dp数组，需要有base case，所以都要加1个
  const dp = new Array(sLen + 1).fill(0).map((_) => new Array(tLen + 1).fill(0));

  for (let i = 0; i <= sLen; i++) {
    for (let j = 0; j <= tLen; j++) {
      if (j === 0) {
        dp[i][j] = 1;
      } else if (i === 0) {
        dp[i][j] = 0;
      } else {
        if (s[i - 1] === t[j - 1]) {
          dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
        } else {
          dp[i][j] = dp[i - 1][j];
        }
      }
    }
  }

  return dp[sLen][tLen];
};
```