/*
 * @lc app=leetcode.cn id=1620 lang=typescript
 *
 * [1620] 网络信号最好的坐标
 */

// @lc code=start

function getPointDistance(coordinate: number[], tower: number[]): number {
  return (coordinate[0] - tower[0]) ** 2 + (coordinate[1] - tower[1]) ** 2;
}

function bestCoordinate(towers: number[][], radius: number): number[] {
  let maxX = Number.MIN_SAFE_INTEGER;
  let maxY = Number.MIN_SAFE_INTEGER;
  for (const [x, y, _] of towers) {
    maxX = Math.max(maxX, x);
    maxY = Math.max(maxY, y);
  }

  let tx = 0;
  let ty = 0;
  let maxQuality = 0;
  // 枚举每个坐标
  for (let x = 0; x <= maxX; x++) {
    for (let y = 0; y <= maxY; y++) {
      const coordinate = [x, y];
      let quality = 0;
      // 计算坐标和塔的信号强度和
      for (const tower of towers) {
        const pointDistance = getPointDistance(coordinate, tower);
        if (pointDistance <= radius ** 2) {
          const manhattanDist = Math.sqrt(pointDistance);
          quality += (tower[2] / (1 + manhattanDist)) >> 0;
        }
      }

      if (quality > maxQuality) {
        tx = x;
        ty = y;
        maxQuality = quality;
      }
    }
  }

  return [tx, ty];
}
// @lc code=end
