# 题目

给你一棵二叉树的根节点 root ，树中有 n 个节点，每个节点都有一个不同于其他节点且处于 1 到 n 之间的值。

另给你一个由 n 个值组成的行程序列 voyage ，表示 预期 的二叉树 先序遍历 结果。

通过交换节点的左右子树，可以 翻转 该二叉树中的任意节点。例，翻转节点 1 的效果如下：

[![f263LQ.png](https://z3.ax1x.com/2021/08/15/f263LQ.png)](https://imgtu.com/i/f263LQ)

请翻转 最少 的树中节点，使二叉树的 先序遍历 与预期的遍历行程 voyage 相匹配 。

如果可以，则返回 翻转的 所有节点的值的列表。你可以按任何顺序返回答案。如果不能，则返回列表 [-1]。

提示：

- 树中的节点数目为 n
- n == voyage.length
- 1 <= n <= 100
- 1 <= Node.val, voyage[i] <= n
- 树中的所有值 互不相同
- voyage 中的所有值 互不相同

# 示例

[![f26Yon.png](https://z3.ax1x.com/2021/08/15/f26Yon.png)](https://imgtu.com/i/f26Yon)

```
输入：root = [1,2], voyage = [2,1]
输出：[-1]
解释：翻转节点无法令先序遍历匹配预期行程。
```

# 题解

## 前序遍历

前序遍历 root，遍历过程中比较：

- root.val 不等于 voyage 对应根节点的值，根节点相同，无法翻转匹配
- root.left.val 不等于 voyage 对应左节点的值，尝试翻转，并记录答案

递归左右子树执行相同操作。

```ts
function flipMatchVoyage(root: TreeNode | null, voyage: number[]): number[] {
  const preorder = (root: TreeNode | null): boolean => {
    // 遍历到叶节点，无需交换
    if (root === null) return true;
    // 根节点的值不相同，无法匹配
    if (root.val !== voyage[idx]) {
      ans.length = 0;
      ans.push(-1);
      return false;
    }
    // 找到左子树的根节点
    idx++;
    // 左节点值不同，翻转节点
    if (root.left && root.left.val !== voyage[idx]) {
      const temp = root.left;
      root.left = root.right;
      root.right = temp;
      ans.push(root.val);
    }
    // 左右子树是否相同
    if (!preorder(root.left)) return false;
    if (!preorder(root.right)) return false;
    return true;
  };
  const ans: number[] = [];
  let idx = 0;
  preorder(root);
  return ans;
}
```
