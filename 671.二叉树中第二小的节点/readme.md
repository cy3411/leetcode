# 题目

给定一个非空特殊的二叉树，每个节点都是正数，并且每个节点的子节点数量只能为 2 或 0。如果一个节点有两个子节点的话，那么该节点的值等于两个子节点中较小的一个。

更正式地说，root.val = min(root.left.val, root.right.val) 总成立。

给出这样的一个二叉树，你需要输出所有节点中的第二小的值。如果第二小的值不存在的话，输出 -1 。

提示：

- 树中节点数目在范围 [1, 25] 内
- 1 <= Node.val <= 231 - 1
- 对于树中每个节点 root.val == min(root.left.val, root.right.val)

# 示例

[![W4TZo4.png](https://z3.ax1x.com/2021/07/27/W4TZo4.png)](https://imgtu.com/i/W4TZo4)

```
输入：root = [2,2,5,null,null,5,7]
输出：5
解释：最小的值是 2 ，第二小的值是 5 。
```

# 题解

## 深度优先搜索

根据题意如果一个节点有两个子节点的话，那么该节点的值等于两个子节点中较小的一个。那么我们可以推出节点 val 一定不大于节点下左右子节点的 val。

那么根节点的 val，一定是最小的值。

那么我们只要找到严格大于根节点的值的最小值即可。

```ts
function findSecondMinimumValue(root: TreeNode | null): number {
  let ans = -1;
  // 根节点的值是最小的值
  const rootVal: number = root.val;
  const dfs = (node: TreeNode | null) => {
    if (node === null) return;
    // 这种情况表示node下所有的节点都大于等于ans，直接跳过。
    if (ans !== -1 && node.val >= ans) return;
    // 寻找严格大于根节点的值
    if (node.val > rootVal) ans = node.val;
    dfs(node.left);
    dfs(node.right);
  };

  dfs(root);
  return ans;
}
```
