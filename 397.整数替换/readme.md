# 题目

给定一个正整数 `n` ，你可以做如下操作：

- 如果 `n` 是偶数，则用 `n / 2` 替换 n 。
- 如果 `n` 是奇数，则可以用 `n + 1` 或 `n - 1` 替换 `n` 。

`n` 变为 `1` 所需的**最小替换次数**是多少？

提示：

- $1 \leq n \leq 2^{31} - 1$

# 示例

```
输入：n = 8
输出：3
解释：8 -> 4 -> 2 -> 1
```

```
输入：n = 7
输出：4
解释：7 -> 8 -> 4 -> 2 -> 1
或 7 -> 6 -> 3 -> 2 -> 1
```

# 题解

## 深度优先搜索

使用递归的方式枚举所有可能的替换次数，并记录最小替换次数。

- n 为偶数时，替换次数为 n / 2
- n 为奇数时，替换次数为 n + 1 或 n - 1，从中取最小值

```ts
function integerReplacement(n: number): number {
  // 记忆化搜索
  const hash = new Map<number, number>();
  // 深度优先搜索
  const dfs = (n: number): number => {
    if (n === 1) return 0;
    if (!hash.has(n)) {
      if (n % 2 === 0) {
        hash.set(n, 1 + dfs(n / 2));
      } else {
        // 取最小值
        hash.set(n, 1 + Math.min(dfs(n - 1), dfs(n + 1)));
      }
    }
    return hash.get(n);
  };

  return dfs(n);
}
```
