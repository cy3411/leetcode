# 题目

给你一个整数数组 nums 和一个整数 k ，找出三个长度为 k 、互不重叠、且 3 \* k 项的和最大的子数组，并返回这三个子数组。

以下标的数组形式返回结果，数组中的每一项分别指示每个子数组的起始位置（下标从 0 开始）。如果有多个结果，返回字典序最小的一个。

提示：

- $1 <= nums.length <= 2 * 10^4$
- $1 <= nums[i] < 2^16$
- $1 <= k <= floor(nums.length / 3)$

# 示例

```
输入：nums = [1,2,1,2,6,7,5,1], k = 2
输出：[0,3,5]
解释：子数组 [1, 2], [2, 6], [7, 5] 对应的起始下标为 [0, 3, 5]。
也可以取 [2, 1], 但是结果 [1, 3, 5] 在字典序上更大。
```

```
输入：nums = [1,2,1,2,1,2,1,2,1], k = 2
输出：[0,2,4]
```

# 题解

## 滑动窗口

使用 3 个大小为 k 的滑动窗口。

设 sum1 为第一个窗口的和，该窗口是从[0,k-1]开始的。

设 sum2 为第二个窗口的和，该窗口是从[k,2k-1]开始的。

设 sum3 为第三个窗口的和，该窗口是从[2k,3k-1]开始的。

我们同时向右移动窗口，每次移动一个位置。每次滑动时，计算三个窗口的和，并将最大的和及其相对位置记录下来。

```ts
function maxSumOfThreeSubarrays(nums: number[], k: number): number[] {
  const ans = new Array(3).fill(0);
  let sum1 = 0,
    maxSum1 = 0,
    maxSum1Index = 0;
  let sum2 = 0,
    maxSum12 = 0,
    maxSum12Index1 = 0,
    maxSum12Index2 = 0;
  let sum3 = 0,
    maxSum123 = 0;

  for (let i = 2 * k; i < nums.length; i++) {
    sum1 += nums[i - 2 * k];
    sum2 += nums[i - k];
    sum3 += nums[i];
    // 开始滑动窗口
    if (i >= 3 * k - 1) {
      // 更新第一个子数组的最大和
      if (sum1 > maxSum1) {
        maxSum1 = sum1;
        maxSum1Index = i - 3 * k + 1;
      }
      // 更新第二个子数组的最大和
      if (maxSum1 + sum2 > maxSum12) {
        maxSum12 = maxSum1 + sum2;
        maxSum12Index1 = maxSum1Index;
        maxSum12Index2 = i - 2 * k + 1;
      }
      // 更新第三个子数组的最大和以及答案
      if (maxSum12 + sum3 > maxSum123) {
        maxSum123 = maxSum12 + sum3;
        ans[0] = maxSum12Index1;
        ans[1] = maxSum12Index2;
        ans[2] = i - k + 1;
      }
      // 左边移除一个元素
      sum1 -= nums[i - 3 * k + 1];
      sum2 -= nums[i - 2 * k + 1];
      sum3 -= nums[i - k + 1];
    }
  }

  return ans;
}
```
