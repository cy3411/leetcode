# 题目

给你一个整数数组 `nums` 和一个整数 · ，请你返回数对 `(i, j)` 的数目，满足 `i < j` 且 `|nums[i] - nums[j]| == k` 。

`|x|` 的值定义为：

- 如果 `x >= 0` ，那么值为 `x` 。
- 如果 `x < 0` ，那么值为 `-x` 。

提示：

- $\color{burlywood} 1 \leq nums.length \leq 200$
- $\color{burlywood} 1 \leq nums[i] \leq 100$
- $\color{burlywood} 1 \leq k \leq 99$

# 示例

```
输入：nums = [1,2,2,1], k = 1
输出：4
解释：差的绝对值为 1 的数对为：
- [**1**,**2**,2,1]
- [**1**,2,**2**,1]
- [1,**2**,2,**1**]
- [1,2,**2**,**1**]
```

```
输入：nums = [1,3], k = 3
输出：0
解释：没有任何数对差的绝对值为 3 。
```

# 题解

## 暴力

题目提供的数据范围比较小，可以用暴力解决。

两层遍历，一层遍历 `i`， 一层遍历 `j`，对于每对组合 `(i,j)`，判断差的绝对值是否等于 `k`，统计符合条件的数对数目。

```cpp
class Solution
{
public:
    int countKDifference(vector<int> &nums, int k)
    {
        int n = nums.size();
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                if (abs(nums[i] - nums[j]) == k)
                {
                    ans++;
                }
            }
        }
        return ans;
    }
};
```

## 哈希表

一次遍历，我们对于每个数字 nums[i]，需要知道之前符合条件的数字数量，也就是 `nums[i] - k` 或者 `nums[i] + k` 在数组中出现的次数。

遍历时使用哈希表来维护不同数字的的出现频率，并统计符合条件的数对数目。

```ts
function countKDifference(nums: number[], k: number): number {
  const n = nums.length;
  const hash = new Map<number, number>();
  let ans = 0;
  for (let i = 0; i < n; i++) {
    // 当前数字之前是出现过几次符合题意的数对
    ans += (hash.get(nums[i] - k) || 0) + (hash.get(nums[i] + k) || 0);
    // 当前数字放入hash表
    hash.set(nums[i], (hash.get(nums[i]) || 0) + 1);
  }

  return ans;
}
```
