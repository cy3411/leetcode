/*
 * @lc app=leetcode.cn id=2006 lang=typescript
 *
 * [2006] 差的绝对值为 K 的数对数目
 */

// @lc code=start
function countKDifference(nums: number[], k: number): number {
  const n = nums.length;
  const hash = new Map<number, number>();
  let ans = 0;
  for (let i = 0; i < n; i++) {
    // 当前数字之前是出现过几次符合题意的数对
    ans += (hash.get(nums[i] - k) || 0) + (hash.get(nums[i] + k) || 0);
    // 当前数字放入hash表
    hash.set(nums[i], (hash.get(nums[i]) || 0) + 1);
  }

  return ans;
}
// @lc code=end
