# 题目

给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。

你可以假设数组中无重复元素。

# 示例

```
输入: [1,3,5,6], 5
输出: 2
```

# 题解

## 二分查找

在一个有序列表中查找第一个大于等于 target 的位置。

```ts
function searchInsert(nums: number[], target: number): number {
  const binarySearch = (nums: number[], target: number): number => {
    let head = 0;
    let tail = nums.length - 1;
    let mid = 0;
    while (head <= tail) {
      mid = head + ((tail - head) >> 1);
      if (nums[mid] < target) head = mid + 1;
      else tail = mid - 1;
    }
    return head;
  };

  return binarySearch(nums, target);
}
```
