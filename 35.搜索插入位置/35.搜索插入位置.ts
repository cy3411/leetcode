/*
 * @lc app=leetcode.cn id=35 lang=typescript
 *
 * [35] 搜索插入位置
 */

// @lc code=start
function searchInsert(nums: number[], target: number): number {
  const binarySearch = (nums: number[], target: number): number => {
    let head = 0;
    let tail = nums.length - 1;
    let mid = 0;
    while (head <= tail) {
      mid = head + ((tail - head) >> 1);
      if (nums[mid] < target) head = mid + 1;
      else tail = mid - 1;
    }
    return head;
  };

  return binarySearch(nums, target);
}
// @lc code=end
