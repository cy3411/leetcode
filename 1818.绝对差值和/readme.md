# 题目

给你两个正整数数组 nums1 和 nums2 ，数组的长度都是 n 。

数组 nums1 和 nums2 的 绝对差值和 定义为所有 |nums1[i] - nums2[i]|（0 <= i < n）的 总和（下标从 0 开始）。

你可以选用 nums1 中的 任意一个 元素来替换 nums1 中的 至多 一个元素，以 最小化 绝对差值和。

在替换数组 nums1 中最多一个元素 之后 ，返回最小绝对差值和。因为答案可能很大，所以需要对 109 + 7 取余 后返回。

|x| 定义为：

如果 x >= 0 ，值为 x ，或者
如果 x <= 0 ，值为 -x

提示：

- n == nums1.length
- n == nums2.length
- 1 <= n <= 105
- 1 <= nums1[i], nums2[i] <= 105

# 示例

```
输入：nums1 = [1,7,5], nums2 = [2,3,5]
输出：3
解释：有两种可能的最优方案：
- 将第二个元素替换为第一个元素：[1,7,5] => [1,1,5] ，或者
- 将第二个元素替换为第三个元素：[1,7,5] => [1,5,5]
两种方案的绝对差值和都是 |1-2| + (|1-3| 或者 |5-3|) + |5-5| = 3
```

# 题解

## 二分搜索

如果我们找到 i 的位置，它们的差值是$\lvert nums1[i]-nums2[i]  \lvert$。假设我们将元素 num1[j]替换为 nums1[i]，那么它们的差值就是$\lvert nums1[j]-nums2[i]  \lvert$。

它们对答案总的贡献就是：
$$\lvert nums1[i]-nums2[i]  \lvert-\lvert nums1[j]-nums2[i]  \lvert$$

我们想要求最小化的差值和，那么就尽量最大化该差值。也就是 num1[j]和 num2[i]的值尽量接近。

遍历数组，记录差值和已经当前差值，并用二分取查找最接近 nums2[i]的值，同时记录下最大差值。

最后的答案就差值和减去最大差值。

```ts
function minAbsoluteSumDiff(nums1: number[], nums2: number[]): number {
  const m = nums1.length;
  const mod = 10 ** 9 + 7;
  // 二分查找使用
  const temp = [...nums1];
  temp.sort((a, b) => a - b);
  // 保存最大绝对差值和
  let sum = 0;
  // 保存元素最大的差值
  let max = 0;
  for (let i = 0; i < m; i++) {
    const diff = Math.abs(nums1[i] - nums2[i]);
    sum = (sum + diff) % mod;
    let idx = binarySearch(temp, nums2[i]);
    // 最接近nums2[i]的元素可能是大于它的，也可能是小于它的
    // 所以计算了2次，取较大差值
    if (idx < m) {
      max = Math.max(max, diff - (temp[idx] - nums2[i]));
    }
    if (idx > 0) {
      max = Math.max(max, diff - (nums2[i] - temp[idx - 1]));
    }
  }

  return (sum - max + mod) % mod;

  function binarySearch(temp: number[], target: number): number {
    // 找到第一个大于等于target的下标
    let l = 0;
    let r = temp.length - 1;
    if (temp[r] < target) return r + 1;
    let mid: number;
    while (l < r) {
      mid = l + ((r - l) >> 1);
      if (temp[mid] < target) l = mid + 1;
      else r = mid;
    }
    return l;
  }
}
```
