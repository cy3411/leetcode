/*
 * @lc app=leetcode.cn id=322 lang=javascript
 *
 * [322] 零钱兑换
 */

// @lc code=start
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  const dp = new Array(amount + 1).fill(Number.POSITIVE_INFINITY);
  // base case
  dp[0] = 0;
  for (let i = 0; i < dp.length; i++) {
    for (let coin of coins) {
      // 子问题无解
      if (i - coin < 0) continue;

      dp[i] = Math.min(dp[i], dp[i - coin] + 1);
    }
  }
  return dp[amount] !== Number.POSITIVE_INFINITY ? dp[amount] : -1;
};
// @lc code=end
