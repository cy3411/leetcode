# 题目

给定不同面额的硬币 coins 和一个总金额 amount。编写一个函数来计算可以凑成总金额所需的最少的硬币个数。如果没有任何一种硬币组合能组成总金额，返回 -1。

你可以认为每种硬币的数量是无限的。

提示：

- 1 <= coins.length <= 12
- 1 <= coins[i] <= 231 - 1
- 0 <= amount <= 104

# 示例

```
输入：coins = [1, 2, 5], amount = 11
输出：3
解释：11 = 5 + 5 + 1
```

```
输入：coins = [2], amount = 3
输出：-1
```

# 方法

明确 base case, amout 为 0 的时候返回 0，因为不需要任何硬币都凑出了结果

明确状态，就是会变化的变量，硬币数量无限，硬币的面额是固定的，只有 amount 会不断向 base case 靠近，所以唯一的状态就是 amount

明确选择，就是会影响状态的行为。这里的是否选择硬币，会影响到 amount

## 自顶向下，DP 函数

`dp(n)`的定义就是输入一个目标数，返回凑成目标的硬币数

```js
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  // 备忘录，减枝
  const memo = new Map();
  // 辅助函数
  const helper = (n) => {
    // 有记录，直接返回
    if (memo.has(n)) return memo.get(n);
    // n为0，不需要零钱了
    if (n === 0) return 0;
    // 没有零钱的组合
    if (n < 0) return -1;

    // 结果要取最少
    let res = Number.POSITIVE_INFINITY;
    for (let coin of coins) {
      // 子问题是否有解
      let subProblem = helper(n - coin);
      if (subProblem === -1) continue;
      // 子问题有解的话，就是在子问题的数量上加上本次的选择
      res = Math.min(res, subProblem + 1);
    }
    // 记录备忘录
    memo.set(n, res !== Number.POSITIVE_INFINITY ? res : -1);

    return memo.get(n);
  };

  return helper(amount);
};
```

## 自底向上，dp 数组

`dp[i]`,当目标金额为 i 时，至少需要 dp[i] 枚硬币凑出。

```js
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  const dp = new Array(amount + 1).fill(Number.POSITIVE_INFINITY);
  // base case
  dp[0] = 0;
  for (let i = 0; i < dp.length; i++) {
    for (let coin of coins) {
      // 子问题无解
      if (i - coin < 0) continue;

      dp[i] = Math.min(dp[i], dp[i - coin] + 1);
    }
  }
  return dp[amount] !== Number.POSITIVE_INFINITY ? dp[amount] : -1;
};
```

```ts
function coinChange(coins: number[], amount: number): number {
  // dp[i]表示凑满i的金额需要最少的硬币数量
  // 因为会出现不可能凑出的结果，会有不可达状态
  const dp = new Array(amount + 1).fill(-1);
  // 初始化
  dp[0] = 0;

  for (let i = 1; i <= amount; i++) {
    for (let c of coins) {
      // 当前总金额没硬币金额大
      if (i < c) continue;
      // 没法和当前硬币凑成组合
      if (dp[i - c] === -1) continue;
      // 状态转移
      if (dp[i] === -1 || dp[i] > dp[i - c] + 1) {
        dp[i] = dp[i - c] + 1;
      }
    }
  }

  return dp[amount];
}
```
