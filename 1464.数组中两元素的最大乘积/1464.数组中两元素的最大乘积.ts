/*
 * @lc app=leetcode.cn id=1464 lang=typescript
 *
 * [1464] 数组中两元素的最大乘积
 */

// @lc code=start
function maxProduct(nums: number[]): number {
  const n = nums.length;
  const idxs = new Array(n);
  for (let i = 0; i < n; i++) {
    idxs[i] = i;
  }
  idxs.sort((a: number, b: number): number => {
    return nums[a] - nums[b];
  });

  return (nums[idxs[n - 1]] - 1) * (nums[idxs[n - 2]] - 1);
}
// @lc code=end
