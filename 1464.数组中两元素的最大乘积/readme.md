# 题目

给你一个整数数组 `nums`，请你选择数组的两个不同下标 `i` 和 `j`，使 `(nums[i]-1)*(nums[j]-1)` 取得最大值。

请你计算并返回该式的最大值。

提示：

- $2 \leq nums.length \leq 500$
- $1 \leq nums[i] \leq 10^3$

# 示例

```
输入：nums = [3,4,5,2]
输出：12
解释：如果选择下标 i=1 和 j=2（下标从 0 开始），则可以获得最大值，(nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12 。
```

```
输入：nums = [1,5,4,5]
输出：16
解释：选择下标 i=1 和 j=3（下标从 0 开始），则可以获得最大值 (5-1)*(5-1) = 16 。
```

# 题解

## 索引排序

题意要求最大乘积，那么只要找到数组最大的两个元素即可。

对数组的索引进行排序，然后将最大的两个数字按照题意相乘取得最大值。

```js
function maxProduct(nums: number[]): number {
  const n = nums.length;
  const idxs = new Array(n);
  for (let i = 0; i < n; i++) {
    idxs[i] = i;
  }
  idxs.sort((a: number, b: number): number => {
    return nums[a] - nums[b];
  });

  return (nums[idxs[n - 1]] - 1) * (nums[idxs[n - 2]] - 1);
}
```

## 排序

对数组排序，取最大的两个值按照题意相乘。

```cpp
class Solution {
public:
    int maxProduct(vector<int> &nums) {
        sort(nums.begin(), nums.end());
        int n = nums.size();

        return (nums[n - 1] - 1) * (nums[n - 2] - 1);
    }
};
```

```py
class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        nums.sort()
        n = len(nums)
        return (nums[n-1]-1) * (nums[n-2]-1)
```
