# 题目

给你一个长桌子，桌子上盘子和蜡烛排成一列。给你一个下标从 `0` 开始的字符串 `s` ，它只包含字符 `'_'` 和 `'|'` ，其中 `'_'` 表示一个 **盘子** ，`'|'` 表示一支 **蜡烛** 。

同时给你一个下标从 `0` 开始的二维整数数组 `queries` ，其中 $queries[i] = [left_i, right_i]$ 表示 **子字符串** $s[left_i...right_i]$ （包含左右端点的字符）。对于每个查询，你需要找到 **子字符串中** 在 **两支蜡烛之间** 的盘子的 **数目** 。如果一个盘子在 **子字符串中** 左边和右边 **都** 至少有一支蜡烛，那么这个盘子满足在 **两支蜡烛之间** 。

- 比方说，`s = "||**||**|_"` ，查询 `[3, 8]` ，表示的是子字符串 `"*||**|"` 。子字符串中在两支蜡烛之间的盘子数目为 `2` ，子字符串中右边两个盘子在它们左边和右边 `都` 至少有一支蜡烛。

请你返回一个整数数组 `answer` ，其中 `answer[i]` 是第 `i` 个查询的答案。

提示：

- $3 \leq s.length \leq 10^5$
- `s` 只包含字符 `'*'` 和 `'|'` 。
- $1 \leq queries.length \leq 10^5$
- $queries[i].length \equiv 2$
- $0 \leq lefti \leq righti < s.length$

# 示例

[![bcZiwT.png](https://s1.ax1x.com/2022/03/08/bcZiwT.png)](https://imgtu.com/i/bcZiwT)

```
输入：s = "**|**|***|", queries = [[2,5],[5,9]]
输出：[2,3]
解释：
- queries[0] 有两个盘子在蜡烛之间。
- queries[1] 有三个盘子在蜡烛之间。
```

[![bcZn61.png](https://s1.ax1x.com/2022/03/08/bcZn61.png)](https://imgtu.com/i/bcZn61)

```
输入：s = "***|**|*****|**||**|*", queries = [[1,17],[4,5],[14,17],[5,11],[15,16]]
输出：[9,0,0,0,0]
解释：
- queries[0] 有 9 个盘子在蜡烛之间。
- 另一个查询没有盘子在蜡烛之间。
```

# 题解

## 前缀和+预处理区间

题意就是在给定的一个区间内找到最左边的蜡烛和最右边的蜡烛，返回两个蜡烛之间的盘子数目。

区间之间的数量，显然可以用前缀和来表示。至于区间内左右蜡烛的位置，我们可以预处理出所有位置的左右蜡烛下标，最后查询时可以快速定位到区间内蜡烛下标，然后求区间内盘子数目。

最后计算结果的时候，可能当前位置的左右没有蜡烛，那么当前位置记作 `-1`。当 `left = -1` 或者 `right = -1` 或者 `left >= right` 时，说明当前位置没有蜡烛，直接返回 `0` 。否则返回 $preSum_{left} - preSum{right}$。

```ts
function platesBetweenCandles(s: string, queries: number[][]): number[] {
  const n = s.length;
  // 统计每个位置盘子的前缀和
  const preSum = new Array(n).fill(0);
  for (let i = 0, sum = 0; i < n; i++) {
    if (s[i] === '*') {
      sum++;
    }
    preSum[i] = sum;
  }
  // 计算每个点左边的第一个蜡烛位置
  const left = new Array(n).fill(0);
  for (let i = 0, l = -1; i < n; i++) {
    if (s[i] === '|') {
      l = i;
    }
    left[i] = l;
  }
  // 计算每个点右边的第一个蜡烛位置
  const right = new Array(n).fill(0);
  for (let i = n - 1, r = -1; i >= 0; i--) {
    if (s[i] === '|') {
      r = i;
    }
    right[i] = r;
  }

  const m = queries.length;
  const ans = new Array(m);
  // 枚举查询点，按位置查询两点之间的盘子数
  for (let i = 0; i < m; i++) {
    const [l, r] = queries[i];
    // 查询点最近的两个蜡烛位置
    const leftPoint = right[l];
    const rightPoint = left[r];
    ans[i] =
      leftPoint === -1 || rightPoint === -1 || leftPoint >= rightPoint
        ? 0
        : preSum[rightPoint] - preSum[leftPoint];
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> platesBetweenCandles(string s, vector<vector<int>> &queries)
    {
        int n = s.size();
        int preSum[n];
        for (int i = 0, sum = 0; i < n; i++)
        {
            if (s[i] == '*')
            {
                sum++;
            }
            preSum[i] = sum;
        }

        int left[n];
        for (int i = 0, l = -1; i < n; i++)
        {
            if (s[i] == '|')
            {
                l = i;
            }
            left[i] = l;
        }

        int right[n];
        for (int i = n - 1, r = -1; i >= 0; i--)
        {
            if (s[i] == '|')
            {
                r = i;
            }
            right[i] = r;
        }

        int m = queries.size();
        vector<int> ans(m, 0);
        for (int i = 0; i < m; i++)
        {
            int l = queries[i][0], r = queries[i][1];
            int lp = right[l], rp = left[r];
            if (lp != -1 && rp != -1 && lp <= rp)
            {
                ans[i] = preSum[rp] - preSum[lp];
            }
        }

        return ans;
    }
};
```
