/*
 * @lc app=leetcode.cn id=2055 lang=cpp
 *
 * [2055] 蜡烛之间的盘子
 */

// @lc code=start
class Solution
{
public:
    vector<int> platesBetweenCandles(string s, vector<vector<int>> &queries)
    {
        int n = s.size();
        int preSum[n];
        for (int i = 0, sum = 0; i < n; i++)
        {
            if (s[i] == '*')
            {
                sum++;
            }
            preSum[i] = sum;
        }

        int left[n];
        for (int i = 0, l = -1; i < n; i++)
        {
            if (s[i] == '|')
            {
                l = i;
            }
            left[i] = l;
        }

        int right[n];
        for (int i = n - 1, r = -1; i >= 0; i--)
        {
            if (s[i] == '|')
            {
                r = i;
            }
            right[i] = r;
        }

        int m = queries.size();
        vector<int> ans(m, 0);
        for (int i = 0; i < m; i++)
        {
            int l = queries[i][0], r = queries[i][1];
            int lp = right[l], rp = left[r];
            if (lp != -1 && rp != -1 && lp <= rp)
            {
                ans[i] = preSum[rp] - preSum[lp];
            }
        }

        return ans;
    }
};
// @lc code=end
