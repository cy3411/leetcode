/*
 * @lc app=leetcode.cn id=274 lang=typescript
 *
 * [274] H 指数
 */

// @lc code=start
function hIndex(citations: number[]): number {
  let h = 0;
  // 升序
  citations.sort((a, b) => a - b);
  // 从后向前遍历，判断当前元素合法性，并更新h指数。
  for (let i = citations.length - 1; i >= 0; i--) {
    if (citations[i] <= h) break;
    h++;
  }

  return h;
}
// @lc code=end
