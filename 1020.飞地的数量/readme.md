# 题目

给你一个大小为 `m x n` 的二进制矩阵 `grid` ，其中 `0` 表示一个海洋单元格、`1` 表示一个陆地单元格。

一次 `移动` 是指从一个陆地单元格走到另一个相邻**（上、下、左、右）**的陆地单元格或跨过 `grid` 的边界。

返回网格中 **无法** 在任意次数的移动中离开网格边界的陆地单元格的数量。

提示：

- $\color{burlywood} m \equiv grid.length$
- $\color{burlywood} n \equiv grid[i].length$
- $\color{burlywood} 1 \leq m, n \leq 500$
- `grid[i][j]` 的值为 `0` 或 `1`

# 示例

[![HBmtkF.png](https://s4.ax1x.com/2022/02/12/HBmtkF.png)](https://imgtu.com/i/HBmtkF)

```
输入：grid = [[0,0,0,0],[1,0,1,0],[0,1,1,0],[0,0,0,0]]
输出：3
解释：有三个 1 被 0 包围。一个 1 没有被包围，因为它在边界上。
```

[![HBms0K.png](https://s4.ax1x.com/2022/02/12/HBms0K.png)](https://imgtu.com/i/HBms0K)

```
输入：grid = [[0,1,1,0],[0,0,1,0],[0,0,1,0],[0,0,0,0]]
输出：0
解释：所有 1 都在边界上或可以到达边界。
```

# 题解

飞地的定义就是矩阵中值为 `1`，且不能和边界相连的陆地连通。

我们可以从网格边界上的陆地为起始点开始搜索，搜索完后，所有和边界陆地相连的陆地都会被访问过。

最后矩阵中的陆地，且没有被访问过的就是飞地。

## 深度优先搜索

```ts
function numEnclaves(grid: number[][]): number {
  const m = grid.length;
  const n = grid[0].length;
  const dirs = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  const dfs = (i: number, j: number) => {
    if (i < 0 || i >= m) return;
    if (j < 0 || j >= n) return;
    if (grid[i][j] === 0) return;
    // 已经访问过，标记为0
    grid[i][j] = 0;
    for (const [x, y] of dirs) {
      dfs(i + x, j + y);
    }
  };
  // 从所有的边界开始dfs
  for (let i = 0; i < m; i++) {
    // 第一列
    dfs(i, 0);
    // 最后一列
    dfs(i, n - 1);
  }
  for (let j = 1; j < n - 1; j++) {
    dfs(0, j);
    dfs(m - 1, j);
  }
  let ans = 0;
  // 矩阵中剩下的岛屿就是飞地
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 1) ans++;
    }
  }

  return ans;
}
```

## 广度优先搜索

```cpp
typedef pair<int, int> PII;
class Solution
{
    vector<vector<int>> dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

public:
    int numEnclaves(vector<vector<int>> &grid)
    {
        int m = grid.size(), n = grid[0].size();
        deque<PII> q;

        // 将所有的边界上的陆地加入队列
        for (int i = 0; i < m; i++)
        {
            if (grid[i][0] == 1)
            {
                // 标记访问过了
                grid[i][0] = 0;
                q.push_back(PII(i, 0));
            }
            if (grid[i][n - 1] == 1)
            {
                grid[i][n - 1] = 0;
                q.push_back(PII(i, n - 1));
            }
        }
        for (int j = 1; j < n - 1; j++)
        {
            if (grid[0][j] == 1)
            {
                grid[0][j] = 0;
                q.push_back(PII(0, j));
            }
            if (grid[m - 1][j] == 1)
            {
                grid[m - 1][j] = 0;
                q.push_back(PII(m - 1, j));
            }
        }

        while (!q.empty())
        {
            auto [row, col] = q.front();
            q.pop_front();
            // 遍历四个方向
            for (auto dir : dirs)
            {
                int next_row = row + dir[0], next_col = col + dir[1];
                if (next_row < 0 || next_row >= m || next_col < 0 || next_col >= n || grid[next_row][next_col] != 1)
                {
                    continue;
                }
                q.push_back(PII(next_row, next_col));
                grid[next_row][next_col] = 0;
            }
        }

        int ans = 0;
        for (int i = 1; i < m - 1; i++)
        {
            for (int j = 1; j < n - 1; j++)
            {
                if (grid[i][j] == 1)
                {
                    ans++;
                }
            }
        }

        return ans;
    }
};
```
