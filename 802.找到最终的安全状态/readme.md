# 题目

在有向图中，以某个节点为起始节点，从该点出发，每一步沿着图中的一条有向边行走。如果到达的节点是终点（即它没有连出的有向边），则停止。

对于一个起始节点，如果从该节点出发，无论每一步选择沿哪条有向边行走，最后必然在有限步内到达终点，则将该起始节点称作是 安全 的。

返回一个由图中所有安全的起始节点组成的数组作为答案。答案数组中的元素应当按 升序 排列。

该有向图有 n 个节点，按 0 到 n - 1 编号，其中 n 是 graph 的节点数。图以下述形式给出：graph[i] 是编号 j 节点的一个列表，满足 (i, j) 是图的一条有向边。

提示：

- n == graph.length
- 1 <= n <= 104
- 0 <= graph[i].length <= n
- graph[i] 按严格递增顺序排列。
- 图中可能包含自环。
- 图中边的数目在范围 [1, 4 * 104] 内。

# 示例

[![febWWQ.md.png](https://z3.ax1x.com/2021/08/05/febWWQ.md.png)](https://imgtu.com/i/febWWQ)

```
输入：graph = [[1,2],[2,3],[5],[0],[5],[],[]]
输出：[2,4,5,6]
解释：示意图如上。
```

# 题解

## 深度优先搜索

题目的意思就是判断一个节点是否在环中，是的话就则节点是不安全的，否则就是安全的。

深度搜索每一个节点，搜索过程中对每一个节点进行标记：

- 0 表示未被访问
- 1 表示节点在调用栈中存在或在环中
- 2 表示节点已经访问过了，是安全节点

```ts
function isSafe(graph: number[][], color: number[], v: number): boolean {
  // 如果v在栈中，返回false，否则返回true
  if (color[v] > 0) return color[v] === 2;
  // 入栈
  color[v] = 1;
  for (const x of graph[v]) {
    // 如果x在栈中，表示在环上，非安全状态
    // 提前退出，这样可以让节点仍然为1，继续为其他节点做判断依据
    if (!isSafe(graph, color, x)) return false;
  }
  // 已经访问过了
  color[v] = 2;
  return true;
}

function eventualSafeNodes(graph: number[][]): number[] {
  const m = graph.length;
  const color = new Array(m).fill(0);
  const ans = [];
  // 每个结点都dfs，判断是否有环
  for (let i = 0; i < m; i++) {
    if (isSafe(graph, color, i)) {
      ans.push(i);
    }
  }

  return ans;
}
```
