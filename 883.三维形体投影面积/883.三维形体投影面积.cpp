/*
 * @lc app=leetcode.cn id=883 lang=cpp
 *
 * [883] 三维形体投影面积
 */

// @lc code=start
class Solution
{
public:
    int projectionArea(vector<vector<int>> &grid)
    {
        int n = grid.size(), xy = 0, xz = 0, yz = 0;
        for (int i = 0; i < n; i++)
        {
            int xzMax = 0, yzMax = 0;
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] > 0)
                {
                    xy++;
                }
                xzMax = max(xzMax, grid[i][j]);
                yzMax = max(yzMax, grid[j][i]);
            }
            xz += xzMax;
            yz += yzMax;
        }
        return xy + xz + yz;
    }
};
// @lc code=end
