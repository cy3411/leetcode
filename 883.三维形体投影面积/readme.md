# 题目

在 `n x n` 的网格 `grid` 中，我们放置了一些与 `x，y，z` 三轴对齐的 `1 x 1 x 1` 立方体。

每个值 `v = grid[i][j]` 表示 `v` 个正方体叠放在单元格 `(i, j)` 上。

现在，我们查看这些立方体在 `xy` 、`yz` 和 `zx` 平面上的投影。

**投影** 就像影子，将 **三维** 形体映射到一个 **二维** 平面上。从顶部、前面和侧面看立方体时，我们会看到“影子”。

返回 _所有三个投影的总面积_ 。

提示：

- $n \equiv grid.length \equiv grid[i].length$
- $1 \leq n \leq 50$
- $0 \leq grid[i][j] \leq 50$

# 示例

[![L7hN2n.png](https://s1.ax1x.com/2022/04/26/L7hN2n.png)](https://imgtu.com/i/L7hN2n)

```
输入：[[1,2],[3,4]]
输出：17
解释：这里有该形体在三个轴对齐平面上的三个投影(“阴影部分”)。
```

```
输入：grid = [[2]]
输出：5
```

# 题解

## 模拟

从示例中可以看出，三个视图的阴影面积如下：

- 从顶部看，有 `n * n` 个格子需要统计，当格子高度大于 0，阴影面积加 1
- 从前面看，有 `n` 行需要统计，每一行取最高的格子
- 从侧面看，有 `n` 列需要统计，每一列取最高的格子

最后返回 3 个面积之和即可

```ts
function projectionArea(grid: number[][]): number {
  const n = grid.length;
  let xy = 0;
  let xz = 0;
  let yz = 0;

  for (let i = 0; i < n; i++) {
    let xzMax = 0;
    let yzMax = 0;
    for (let j = 0; j < n; j++) {
      if (grid[i][j] > 0) {
        xy++;
      }
      xzMax = Math.max(xzMax, grid[i][j]);
      yzMax = Math.max(yzMax, grid[j][i]);
    }
    xz += xzMax;
    yz += yzMax;
  }

  return xy + xz + yz;
}
```

```cpp
class Solution
{
public:
    int projectionArea(vector<vector<int>> &grid)
    {
        int n = grid.size(), xy = 0, xz = 0, yz = 0;
        for (int i = 0; i < n; i++)
        {
            int xzMax = 0, yzMax = 0;
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] > 0)
                {
                    xy++;
                }
                xzMax = max(xzMax, grid[i][j]);
                yzMax = max(yzMax, grid[j][i]);
            }
            xz += xzMax;
            yz += yzMax;
        }
        return xy + xz + yz;
    }
};
```
