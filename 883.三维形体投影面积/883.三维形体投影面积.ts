/*
 * @lc app=leetcode.cn id=883 lang=typescript
 *
 * [883] 三维形体投影面积
 */

// @lc code=start
function projectionArea(grid: number[][]): number {
  const n = grid.length;
  let xy = 0;
  let xz = 0;
  let yz = 0;

  for (let i = 0; i < n; i++) {
    let xzMax = 0;
    let yzMax = 0;
    for (let j = 0; j < n; j++) {
      if (grid[i][j] > 0) {
        xy++;
      }
      xzMax = Math.max(xzMax, grid[i][j]);
      yzMax = Math.max(yzMax, grid[j][i]);
    }
    xz += xzMax;
    yz += yzMax;
  }

  return xy + xz + yz;
}
// @lc code=end
