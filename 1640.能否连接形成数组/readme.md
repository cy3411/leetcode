# 题目

给你一个整数数组 `arr` ，数组中的每个整数 **互不相同** 。另有一个由整数数组构成的数组 `pieces`，其中的整数也 **互不相同** 。请你以 **任意顺序** 连接 `pieces` 中的数组以形成 `arr` 。但是，**不允许** 对每个数组 `pieces[i]` 中的整数重新排序。

如果可以连接 `pieces` 中的数组形成 `arr` ，返回 `true` ；否则，返回 `false` 。

提示：

- $1 \leq pieces.length \leq arr.length \leq 100$
- $sum(pieces[i].length) \equiv arr.length$
- $1 \leq pieces[i].length \leq arr.length$
- $1 \leq arr[i], pieces[i][j] \leq 100$
- `arr` 中的整数 **互不相同**
- `pieces` 中的整数 **互不相同**（也就是说，如果将 pieces 扁平化成一维数组，数组中的所有整数互不相同）

# 示例

```
输入：arr = [15,88], pieces = [[88],[15]]
输出：true
解释：依次连接 [15] 和 [88]
```

```
输入：arr = [49,18,16], pieces = [[16,18,49]]
输出：false
解释：即便数字相符，也不能重新排列 pieces[0]
```

# 题解

## 哈希表

使用哈希表记录 pieces 中各个数组首元素和索引的对应关系。

枚举 arr 每个元素，看看是否在哈希表中，如果不在，直接返回 false。

否则，取得当前元素在 pieces 中的索引 j，从当前元素开始往后和 pieces[j] 逐个比较，如果不相同，返回 false。

如果 arr 所有元素都比较成功，返回 true。

```js
function canFormArray(arr: number[], pieces: number[][]): boolean {
  const idxs = new Map<number, number>();
  const m = pieces.length;
  // 将pieces元素中第一个数字和索引放入哈希表
  for (let i = 0; i < m; i++) {
    idxs.set(pieces[i][0], i);
  }

  const n = arr.length;
  // 枚举 arr 中每个元素
  for (let i = 0; i < n; ) {
    if (!idxs.has(arr[i])) return false;
    // 从哈希表中拿到当前元素在 pieces中的索引
    const j = idxs.get(arr[i])!;
    const size = pieces[j].length;
    // arr 和 pieces 比较
    for (let k = 0; k < size; k++) {
      if (arr[i + k] !== pieces[j][k]) return false;
    }
    i += size;
  }

  return true;
}
```

```cpp
class Solution {
public:
    bool canFormArray(vector<int> &arr, vector<vector<int>> &pieces) {
        unordered_map<int, int> idxs;
        int n = pieces.size();
        for (int i = 0; i < n; i++) {
            idxs[pieces[i][0]] = i;
        }
        n = arr.size();
        for (int i = 0; i < n;) {
            if (idxs.find(arr[i]) == idxs.end()) {
                return false;
            }
            int j = idxs[arr[i]], size = pieces[j].size();
            for (int k = 0; k < size; k++) {
                if (arr[i + k] != pieces[j][k]) {
                    return false;
                }
            }
            i += size;
        }

        return true;
    }
};
```

```py
class Solution:
    def canFormArray(self, arr: List[int], pieces: List[List[int]]) -> bool:
        idxs = {p[0]: i for i, p in enumerate(pieces)}
        i = 0
        while (i < len(arr)):
            if arr[i] not in idxs:
                return False
            p = pieces[idxs[arr[i]]]
            n = len(p)
            if arr[i:i+n] != p:
                return False
            i += n
        return True
```
