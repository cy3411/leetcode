#
# @lc app=leetcode.cn id=1640 lang=python3
#
# [1640] 能否连接形成数组
#

# @lc code=start
class Solution:
    def canFormArray(self, arr: List[int], pieces: List[List[int]]) -> bool:
        idxs = {p[0]: i for i, p in enumerate(pieces)}
        i = 0
        while (i < len(arr)):
            if arr[i] not in idxs:
                return False
            p = pieces[idxs[arr[i]]]
            n = len(p)
            if arr[i:i+n] != p:
                return False
            i += n
        return True
# @lc code=end
