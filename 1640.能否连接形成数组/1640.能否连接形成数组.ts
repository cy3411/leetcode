/*
 * @lc app=leetcode.cn id=1640 lang=typescript
 *
 * [1640] 能否连接形成数组
 */

// @lc code=start
function canFormArray(arr: number[], pieces: number[][]): boolean {
  const idxs = new Map<number, number>();
  const m = pieces.length;
  // 将pieces元素中第一个数字和索引放入哈希表
  for (let i = 0; i < m; i++) {
    idxs.set(pieces[i][0], i);
  }

  const n = arr.length;
  // 枚举 arr 中每个元素
  for (let i = 0; i < n; ) {
    if (!idxs.has(arr[i])) return false;
    // 从哈希表中拿到当前元素在 pieces中的索引
    const j = idxs.get(arr[i])!;
    const size = pieces[j].length;
    // arr 和 pieces 比较
    for (let k = 0; k < size; k++) {
      if (arr[i + k] !== pieces[j][k]) return false;
    }
    i += size;
  }

  return true;
}
// @lc code=end
