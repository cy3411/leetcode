/*
 * @lc app=leetcode.cn id=1725 lang=typescript
 *
 * [1725] 可以形成最大正方形的矩形数目
 */

// @lc code=start
function countGoodRectangles(rectangles: number[][]): number {
  // 记录每个矩形的最小边
  const map = new Map<number, number>();
  let ans = 0;
  let max = 0;
  for (const [w, h] of rectangles) {
    const key = Math.min(w, h);
    // 更新最大边以及最大边的数量
    map.set(key, (map.get(key) || 0) + 1);
    if (key >= max) {
      max = key;
      ans = map.get(key);
    }
  }

  return ans;
}
// @lc code=end
