# 题目

给你一个数组 `rectangles` ，其中 $\color{burlywood}rectangles[i] = [l_i, w_i]$ 表示第 `i` 个矩形的长度为 $\color{burlywood}l_i$ 、宽度为 $\color{burlywood}w_i$ 。

如果存在 `k` 同时满足 $\color{burlywood}k \leq l_i$ 和 $\color{burlywood}k \leq w_i$ ，就可以将第 `i` 个矩形切成边长为 `k` 的正方形。例如，矩形 `[4,6]` 可以切成边长最大为 `4` 的正方形。

设 `maxLen` 为可以从矩形数组 `rectangles` 切分得到的 **最大正方形** 的边长。

请你统计有多少个矩形能够切出边长为 `maxLen` 的正方形，并返回矩形 **数目** 。

提示：

- $\color{burlywood}1 \leq rectangles.length \leq 1000$
- $\color{burlywood}rectangles[i].length \equiv 2$
- $\color{burlywood}1 \leq li, wi \leq 109$
- $\color{burlywood}li \not = wi$

# 示例

```
输入：rectangles = [[5,8],[3,9],[5,12],[16,5]]
输出：3
解释：能从每个矩形中切出的最大正方形边长分别是 [5,3,5,5] 。
最大正方形的边长为 5 ，可以由 3 个矩形切分得到。
```

```
输入：rectangles = [[2,3],[3,7],[4,3],[3,7]]
输出：3
```

# 题解

## 哈希表

题意可以得出，每个矩形元素的长宽的最小值就是可以切成正方形的边长。

利用哈希表表统计正方形边长的个数，同时维护 maxLen 来记录最大的边长，一边统计一边更新答案。

```ts
function countGoodRectangles(rectangles: number[][]): number {
  // 记录每个矩形的最小边
  const map = new Map<number, number>();
  let ans = 0;
  let max = 0;
  for (const [w, h] of rectangles) {
    const key = Math.min(w, h);
    // 更新最大边以及最大边的数量
    map.set(key, (map.get(key) || 0) + 1);
    if (key >= max) {
      max = key;
      ans = map.get(key);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int countGoodRectangles(vector<vector<int>> &rectangles)
    {
        unordered_map<int, int> m;
        int ans = 0;
        int maxlen = 0;
        for (auto r : rectangles)
        {
            int key;
            key = r[0] < r[1] ? r[0] : r[1];
            m[key]++;
            if (key >= maxlen)
            {
                maxlen = key;
                ans = m[key];
            }
        }
        return ans;
    }
};
```
