/*
 * @lc app=leetcode.cn id=1725 lang=cpp
 *
 * [1725] 可以形成最大正方形的矩形数目
 */

// @lc code=start
class Solution
{
public:
    int countGoodRectangles(vector<vector<int>> &rectangles)
    {
        unordered_map<int, int> m;
        int ans = 0;
        int maxlen = 0;
        for (auto r : rectangles)
        {
            int key;
            key = r[0] < r[1] ? r[0] : r[1];
            m[key]++;
            if (key >= maxlen)
            {
                maxlen = key;
                ans = m[key];
            }
        }
        return ans;
    }
};
// @lc code=end
