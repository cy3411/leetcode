# 题目

给你一个字符串 s ，请你去除字符串中重复的字母，使得每个字母只出现一次。需保证 返回结果的字典序最小（要求不能打乱其他字符的相对位置）。

提示：

- 1 <= s.length <= 104
- s 由小写英文字母组成

# 示例

```
输入：s = "bcabc"
输出："abc"
```

```
输入：s = "cbacdcbc"
输出："acdb"
```

# 题解

## 单调栈+哈希表

结果要求字典序最小，相对位置不能改变，那么肯定是单调递增的结果。

我们可以先统计每个字符的出现次数，然后遍历每个字符，当栈中没有当前字符的时候，进行单调递增栈的处理。

需要注意的就是出栈的时候需要判断一下栈顶字符在后续的字符串中是否还存在，如果还有就可以出栈，如果没有了，就不能出栈。因为题目还有一个限制，就是每个字符只能出现一次。

```ts
function removeDuplicateLetters(s: string): string {
  // 统计每个字符出现的次数
  const hash: Map<string, number> = new Map();
  for (const c of s) {
    hash.set(c, (hash.get(c) ?? 0) + 1);
  }

  // 单调递增栈，保证字符是按照相对升序
  const stack = [];
  for (const c of s) {
    // 栈中没有当前字符才可以入栈
    if (!stack.includes(c)) {
      while (stack.length && hash.get(stack[stack.length - 1]) && c < stack[stack.length - 1]) {
        stack.pop();
      }
      stack.push(c);
    }
    hash.set(c, hash.get(c) - 1);
  }

  return stack.join('');
}
```
