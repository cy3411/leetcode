/*
 * @lc app=leetcode.cn id=521 lang=c
 *
 * [521] 最长特殊序列 Ⅰ
 */

// @lc code=start

int findLUSlength(char *a, char *b)
{
    int aLen = strlen(a);
    int bLen = strlen(b);
    if (aLen == bLen)
    {
        int i = 0;
        // 遍历字符串，检查a和b是否相等
        while (i < aLen && a[i] == b[i])
        {
            i++;
        }
        if (i == aLen)
        {
            return -1;
        }
        else
        {
            return aLen;
        }
    }
    else
    {
        return aLen > bLen ? aLen : bLen;
    }
}
// @lc code=end
