# 题目

给你两个字符串 `a` 和 `b`，请返回 _这两个字符串中_ **最长的特殊序列** 的长度。如果不存在，则返回 `-1` 。

**「最长特殊序列」** 定义如下：该序列为 某字符串独有的最长子序列（即不能是其他字符串的子序列） 。

字符串 `s` 的子序列是在从 `s` 中删除任意数量的字符后可以获得的字符串。

- 例如，`"abc"` 是 `"aebdc"` 的子序列，因为删除 `"aebdc"` 中斜体加粗的字符可以得到 `"abc"` 。 `"aebdc"` 的子序列还包括 `"aebdc"` 、 `"aeb"` 和 `""` (空字符串)。

提示：

- $1 \leq a.length, b.length \leq 100$
- `a` 和 `b` 由小写英文字母组成

# 示例

```
输入: a = "aba", b = "cdc"
输出: 3
解释: 最长特殊序列可为 "aba" (或 "cdc")，两者均为自身的子序列且不是对方的子序列。
```

```
输入：a = "aaa", b = "bbb"
输出：3
解释: 最长特殊序列是 "aaa" 和 "bbb" 。
```

# 题解

## 脑筋急转弯

如果 a 和 b 两个字符串不相等，那么它们都是各自的最长子序列。那么较长的字符串不可能是较短字符串的子序列。直接返回较长字符串的长度即可。

如果 a 和 b 两个字符串相等，那么独有的最长子序列不存在，返回 -1 即可。

```ts
function findLUSlength(a: string, b: string): number {
  return a !== b ? Math.max(a.length, b.length) : -1;
}
```

```cpp
int findLUSlength(char *a, char *b)
{
    int aLen = strlen(a);
    int bLen = strlen(b);
    if (aLen == bLen)
    {
        int i = 0;
        // 遍历字符串，检查a和b是否相等
        while (i < aLen && a[i] == b[i])
        {
            i++;
        }
        if (i == aLen)
        {
            return -1;
        }
        else
        {
            return aLen;
        }
    }
    else
    {
        return aLen > bLen ? aLen : bLen;
    }
}
```

```cpp
class Solution
{
public:
    int findLUSlength(string a, string b)
    {
        if (a == b)
        {
            return -1;
        }
        return max(a.size(), b.size());
    }
};
```
