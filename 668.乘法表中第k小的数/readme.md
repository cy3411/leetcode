# 题目

几乎每一个人都用 **乘法表**。但是你能在乘法表中快速找到第 `k` 小的数字吗？

给定高度 `m` 、宽度 `n` 的一张 $m * n$ 的乘法表，以及正整数 `k`，你需要返回表中第 `k` 小的数字。

注意：

- `m` 和 `n` 的范围在 `[1, 30000]` 之间。
- `k` 的范围在 `[1, m * n]` 之间。

# 示例

```
输入: m = 3, n = 3, k = 5
输出: 3
解释:
乘法表:
1	2	3
2	4	6
3	6	9

第5小的数字是 3 (1, 2, 2, 3, 3).
```

```
输入: m = 2, n = 3, k = 6
输出: 6
解释:
乘法表:
1	2	3
2	4	6

第6小的数字是 6 (1, 2, 2, 3, 4, 6).
```

# 题解

## 二分查找

由于 m 和 n 的范围在 [1, 30000] 之间，直接求出所有结果，然后找到第 k 大的数字会超时。

所以考虑使用二分查找，查找的范围是 [1, m * n]。每次查找的数字，去计算在乘法表中的位置，如果位置小于 k，左边界向右移动，否则，右边界向左移动。

```ts
function findKthNumber(m: number, n: number, k: number): number {
  const getCount = (num: number): number => {
    let count = 0;
    for (let i = 1; i <= m; i++) {
      count += Math.min(n, Math.floor(num / i));
    }
    return count;
  };

  let l = 1;
  let r = m * n;
  while (l < r) {
    const mid = l + Math.floor((r - l) / 2);
    const count = getCount(mid);
    if (count >= k) {
      r = mid;
    } else {
      l = mid + 1;
    }
  }
  return l;
}
```

```cpp
class Solution {
public:
    // num 在乘法表中的位置
    int getCount(int m, int n, int num) {
        int count = 0;
        for (int i = 1; i <= m; i++) {
            count += min(n, num / i);
        }
        return count;
    }
    int findKthNumber(int m, int n, int k) {
        int l = 1, r = m * n, mid;
        while (l < r) {
            mid = l + (r - l) / 2;
            if (getCount(m, n, mid) >= k) {
                r = mid;
            } else {
                l = mid + 1;
            }
        }
        return l;
    }
};
```
