/*
 * @lc app=leetcode.cn id=668 lang=typescript
 *
 * [668] 乘法表中第k小的数
 */

// @lc code=start
function findKthNumber(m: number, n: number, k: number): number {
  const getCount = (num: number): number => {
    let count = 0;
    for (let i = 1; i <= m; i++) {
      count += Math.min(n, Math.floor(num / i));
    }
    return count;
  };

  let l = 1;
  let r = m * n;
  while (l < r) {
    const mid = l + Math.floor((r - l) / 2);
    const count = getCount(mid);
    if (count >= k) {
      r = mid;
    } else {
      l = mid + 1;
    }
  }
  return l;
}
// @lc code=end
