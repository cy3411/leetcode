/*
 * @lc app=leetcode.cn id=668 lang=cpp
 *
 * [668] 乘法表中第k小的数
 */

// @lc code=start
class Solution {
public:
    // num 在乘法表中的位置
    int getCount(int m, int n, int num) {
        int count = 0;
        for (int i = 1; i <= m; i++) {
            count += min(n, num / i);
        }
        return count;
    }
    int findKthNumber(int m, int n, int k) {
        int l = 1, r = m * n, mid;
        while (l < r) {
            mid = l + (r - l) / 2;
            if (getCount(m, n, mid) >= k) {
                r = mid;
            } else {
                l = mid + 1;
            }
        }
        return l;
    }
};
// @lc code=end
