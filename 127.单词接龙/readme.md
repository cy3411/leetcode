# 题目

字典 `wordList` 中从单词 `beginWord` 和 `endWord` 的 **转换序列** 是一个按下述规格形成的序列 $beginWord -> s1 -> s2 -> ... -> s_k$：

- 每一对相邻的单词只差一个字母。
- 对于 `1 <= i <= k` 时，每个 $s_i$ 都在 `wordList` 中。注意， `beginWord` 不需要在 `wordList` 中。
- $s_k == endWord$

给你两个单词 `beginWord` 和 `endWord` 和一个字典 `wordList` ，返回 从 `beginWord` 到 `endWord` 的 **最短转换序列** 中的 单词数目 。如果不存在这样的转换序列，返回 `0` 。

提示：

- $1 <= beginWord.length <= 10$
- $endWord.length == beginWord.length$
- $1 <= wordList.length <= 5000$
- $wordList[i].length == beginWord.length$
- `beginWord`、`endWord` 和 `wordList[i]` 由小写英文字母组成
- $beginWord != endWord$
- `wordList` 中的所有字符串 **互不相同**

# 示例

```
输入：beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
输出：5
解释：一个最短转换序列是 "hit" -> "hot" -> "dot" -> "dog" -> "cog", 返回它的长度 5。
```

```
输入：beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
输出：0
解释：endWord "cog" 不在字典中，所以无法进行转换。
```

# 题解

## 广度优先搜索

连通性的问题，可以使用广度优先搜索来解决。

beginWord 做为搜索的起点，通过改变当前单词每个字符，来确定搜索的下一个状态。

如果可以在字典中找到下一个状态，则加入队列，同时删除字典中的该单词，以防止重复搜索。

搜索过程中，如果当前单词与目标单词相同，则返回当前转换序列的长度。

```ts
interface WordNode {
  word: string;
  step: number;
}

function ladderLength(beginWord: string, endWord: string, wordList: string[]): number {
  const base = 'a'.charCodeAt(0);
  const hash = new Set(wordList);
  // 字典中不包含终点单词，肯定完成不了转换
  if (!hash.has(endWord)) return 0;
  // 搜索起点
  const queue: WordNode[] = [{ word: beginWord, step: 1 }];
  // 开始搜索
  while (queue.length) {
    let curr = queue.shift();
    // 搜索到终点
    if (curr.word === endWord) {
      return curr.step;
    }
    // 当前单词的所有可能的转换
    // 改变单词的每个字符，从 a - z 枚举
    for (let i = 0; i < curr.word.length; i++) {
      const temp = curr.word.split('');
      for (let j = 0; j < 26; j++) {
        const char = String.fromCharCode(base + j);
        temp[i] = char;
        const word = temp.join('');
        // 如果字典中包含，并且没有被访问过，加入队列
        if (hash.has(word)) {
          // 删除字典中的单词，已访问过
          hash.delete(word);
          queue.push({ word, step: curr.step + 1 });
        }
      }
    }
  }

  return 0;
}
```

```cpp
class Solution
{
public:
    struct WordNode
    {
        string word;
        int step;
    };

    int ladderLength(string beginWord, string endWord, vector<string> &wordList)
    {
        unordered_set<string> dict(wordList.begin(), wordList.end());
        if (dict.find(endWord) == dict.end())
        {
            return 0;
        }
        queue<WordNode> q;
        q.push(WordNode{beginWord, 1});
        while (!q.empty())
        {
            WordNode curr = q.front();
            q.pop();
            if (curr.word == endWord)
            {
                return curr.step;
            }
            for (int i = 0; i < curr.word.size(); i++)
            {
                string temp = curr.word;
                for (int j = 'a'; j <= 'z'; j++)
                {
                    temp[i] = j;
                    if (dict.find(temp) != dict.end())
                    {
                        dict.erase(temp);
                        q.push(WordNode{temp, curr.step + 1});
                    }
                }
            }
        }
        return 0;
    }
};
```
