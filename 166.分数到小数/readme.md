# 题目

给定两个整数，分别表示分数的分子 numerator 和分母 denominator，以 字符串形式返回小数 。

如果小数部分为循环小数，则将循环的部分括在括号内。

如果存在多个答案，只需返回 任意一个 。

对于所有给定的输入，保证 答案字符串的长度小于 104 。

提示：

- $\color{#cda869}-2^{31} \leq numerator, denominator \leq 2^{31} - 1$
- `denominator != 0`

# 示例

```
输入：numerator = 1, denominator = 2
输出："0.5"
```

```
输入：numerator = 4, denominator = 333
输出："0.(012)"
```

# 题解

## 长除法+哈希表

将分数转为小数，就是计算分子和分母相除的结果，可能的结果有三种情况：

- 整数
- 有限小数
- 无限循环小数

如果分子可以被分母整除，将分子除以分母的商转成字符串就是答案。

如果分子不能被分母整除，那么结果可能是有限小数或无限循环小数，需要通过模拟长除法来计算答案。

由于有负数的存在，为了计算方便，先通过分子和分母的正负关系来判断答案的正负，然后将分子和分母转成正整数来计算。

计算长除法，首先计算整数部分，然后将符号位、整数部分和小数点压入到结果中。

然后根据余数计算小数部分。每次将余数乘 `10`，然后计算下一位小数，再将余数乘 `10`，继续计算小数部分。重复操作，知道余数为 `0` 或者找到循环节点。

如何判断是否找到循环节点？对于相同余数的计算，那么计算得到的小数一定相同，因此如果计算过程中出现过相同的小数，表示已经找到了循环节点。

有限小数
[![4LbLLD.md.png](https://z3.ax1x.com/2021/10/03/4LbLLD.md.png)](https://imgtu.com/i/4LbLLD)

无限循环小数
[![4Lqiy8.md.png](https://z3.ax1x.com/2021/10/03/4Lqiy8.md.png)](https://imgtu.com/i/4Lqiy8)

我们可以使用哈希表记录每次计算的小数和下标，如果新的小数出现在哈希表中，表示找到了循环节点，我们将哈希表中数的下标位置插入`(`，在小数位最后插入`)`即可。

```ts
function fractionToDecimal(numerator: number, denominator: number): string {
  // 可以整除
  if (numerator % denominator === 0) {
    return String(numerator / denominator);
  }

  const ans: string[] = [];
  // 结果是否为负数
  // 分子和分母有一个为负数
  if (Number(numerator < 0) ^ Number(denominator < 0)) {
    ans.push('-');
  }

  numerator = Math.abs(numerator);
  denominator = Math.abs(denominator);
  // 整数部分
  const integerPart = Math.floor(numerator / denominator);
  ans.push(String(integerPart));
  ans.push('.');
  // 小数部分
  const fractionPart: string[] = [];
  const remaindIndex = new Map();
  let remaind = numerator % denominator;
  let index = 0;
  while (remaind !== 0 && !remaindIndex.has(remaind)) {
    remaindIndex.set(remaind, index++);
    remaind = remaind * 10;
    fractionPart.push(String(Math.floor(remaind / denominator)));
    remaind = remaind % denominator;
  }
  // 出现循环,需要给循环部分加上小括号
  if (remaind > 0) {
    // 找到第一个出现的小数下标
    const index = remaindIndex.get(remaind);
    fractionPart.splice(index, 0, '(');
    fractionPart.push(')');
  }

  ans.push(...fractionPart);

  return ans.join('');
}
```
