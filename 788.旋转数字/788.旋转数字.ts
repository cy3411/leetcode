/*
 * @lc app=leetcode.cn id=788 lang=typescript
 *
 * [788] 旋转数字
 */

// @lc code=start
function rotatedDigits(n: number): number {
  // 2,5,6,9 至少需要出现一次
  // 不能出现3,4,7
  const check = [0, 0, 1, -1, -1, 1, 1, -1, 0, 1];
  let ans = 0;

  for (let i = 1; i <= n; i++) {
    const num: string = String(i);
    const m = num.length;
    // 是否出现非法数字
    let isValid = true;
    // 是否出现有效数字
    let isDiff = false;
    // 枚举数字的每一位,判断是否符合条件
    for (let j = 0; j < m; j++) {
      const idx = num[j].charCodeAt(0) - '0'.charCodeAt(0);
      if (check[idx] === -1) {
        isValid = false;
      } else if (check[idx] === 1) {
        isDiff = true;
      }
    }

    if (isValid && isDiff) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
