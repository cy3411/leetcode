/*
 * @lc app=leetcode.cn id=434 lang=typescript
 *
 * [434] 字符串中的单词数
 */

// @lc code=start
function countSegments(s: string): number {
  let ans = 0;
  let word = '';
  // 尾部增加一个空格，方便处理
  s += ' ';
  for (let char of s) {
    if (char === ' ') {
      if (word !== '') {
        ans++;
        word = '';
      }
      continue;
    }
    word += char;
  }

  return ans;
}
// @lc code=end
