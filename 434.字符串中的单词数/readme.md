# 题目

统计字符串中的单词个数，这里的单词指的是连续的不是空格的字符。

请注意，你可以假定字符串里不包括任何不可打印的字符。

# 示例

```
输入: "Hello, my name is John"
输出: 5
解释: 这里的单词是指连续的不是空格的字符，所以 "Hello," 算作 1 个单词。
```

# 题解

## 遍历

遍历字符串，如果当前字符是

- 非空格，将字符串存入到变量 word 中
- 空格，如果 word 不为空，将结果加 1，并将 word 置空

可以提前将 s 尾部添加一个空格，相当于哨兵位置，方便遍历到最后位置的代码处理。

```ts
function countSegments(s: string): number {
  let ans = 0;
  let word = '';
  // 尾部增加一个空格，方便处理
  s += ' ';
  for (let char of s) {
    if (char === ' ') {
      if (word !== '') {
        ans++;
        word = '';
      }
      continue;
    }
    word += char;
  }

  return ans;
}
```
