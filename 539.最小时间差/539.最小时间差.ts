/*
 * @lc app=leetcode.cn id=539 lang=typescript
 *
 * [539] 最小时间差
 */

// @lc code=start
function findMinDifference(timePoints: string[]): number {
  const n = timePoints.length;
  // 只有144种时间，超过了，肯定有重复的存在
  if (n > 1440) return 0;
  // 排序，最小时间差就在两个相邻的时间点之间或者首尾之间
  timePoints.sort();
  // 将时间字符串转换为数字用于比较
  const getMinutes = (time: string) => {
    const [hour, minute] = time.split(':');
    return Number(hour) * 60 + Number(minute);
  };

  let ans = Number.POSITIVE_INFINITY;
  // 首部的分钟，最后和尾部计算用
  const headMinutes = getMinutes(timePoints[0]);
  // 上一个元素的分钟，计算相邻之间使用
  let preMinutes = headMinutes;
  // 遍历，计算相邻时间差，更新最小值
  for (let i = 1; i < timePoints.length; i++) {
    const minutes = getMinutes(timePoints[i]);
    ans = Math.min(ans, minutes - preMinutes);
    preMinutes = minutes;
  }
  // 计算首尾时间差
  ans = Math.min(ans, headMinutes + 24 * 60 - preMinutes);

  return ans;
}
// @lc code=end
