# 题目

给定一个 24 小时制（小时:分钟 "**HH:MM**"）的时间列表，找出列表中任意两个时间的最小时间差并以分钟数表示。

提示：

- $\color{burlywood}2 \leq timePoints \leq 2 * 10^4$
- `timePoints[i]` 格式为 **"HH:MM**"

# 示例

```
输入：timePoints = ["23:59","00:00"]
输出：1
```

```
输入：timePoints = ["00:00","23:59","00:00"]
输出：0
```

# 题解

## 遍历

将时间列表排序，那么最小时间差就是相邻两个时间的差值或者首尾时间的差值。

遍历时间列表，计算相邻时间差值，并更新最小时间差。遍历完毕，再计算首尾时间差值，并更新最小时间差。

有一个优化的地方，所有的时间一共有 1440 种，如果时间列表的长度超过了 1440，那么列表中肯定有重复时间，直接返回 0。

```ts
function findMinDifference(timePoints: string[]): number {
  const n = timePoints.length;
  // 只有144种时间，超过了，肯定有重复的存在
  if (n > 1440) return 0;
  // 排序，最小时间差就在两个相邻的时间点之间或者首尾之间
  timePoints.sort();
  // 将时间字符串转换为数字用于比较
  const getMinutes = (time: string) => {
    const [hour, minute] = time.split(':');
    return Number(hour) * 60 + Number(minute);
  };

  let ans = Number.POSITIVE_INFINITY;
  // 首部的分钟，最后和尾部计算用
  const headMinutes = getMinutes(timePoints[0]);
  // 上一个元素的分钟，计算相邻之间使用
  let preMinutes = headMinutes;
  // 遍历，计算相邻时间差，更新最小值
  for (let i = 1; i < timePoints.length; i++) {
    const minutes = getMinutes(timePoints[i]);
    ans = Math.min(ans, minutes - preMinutes);
    preMinutes = minutes;
  }
  // 计算首尾时间差
  ans = Math.min(ans, headMinutes + 24 * 60 - preMinutes);

  return ans;
}
```

```cpp
class Solution
{
public:
    int getMinutes(string &time)
    {
        int h = (time[0] - '0') * 10 + (time[1] - '0');
        int m = (time[3] - '0') * 10 + (time[4] - '0');
        return h * 60 + m;
    };
    int findMinDifference(vector<string> &timePoints)
    {
        int n = timePoints.size();
        if (n > 1440)
        {
            return 0;
        }

        sort(timePoints.begin(), timePoints.end());
        int ans = INT_MAX;
        int headMinutes = getMinutes(timePoints[0]);
        int prevMinutes = headMinutes;

        for (int i = 1; i < n; i++)
        {
            int minutes = getMinutes(timePoints[i]);
            ans = min(ans, minutes - prevMinutes);
            prevMinutes = minutes;
        }

        ans = min(ans, headMinutes + 1440 - prevMinutes);

        return ans;
    }
};
```
