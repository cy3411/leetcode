/*
 * @lc app=leetcode.cn id=539 lang=cpp
 *
 * [539] 最小时间差
 */

// @lc code=start
class Solution
{
public:
    int getMinutes(string &time)
    {
        int h = (time[0] - '0') * 10 + (time[1] - '0');
        int m = (time[3] - '0') * 10 + (time[4] - '0');
        return h * 60 + m;
    };
    int findMinDifference(vector<string> &timePoints)
    {
        int n = timePoints.size();
        if (n > 1440)
        {
            return 0;
        }

        sort(timePoints.begin(), timePoints.end());
        int ans = INT_MAX;
        int headMinutes = getMinutes(timePoints[0]);
        int prevMinutes = headMinutes;

        for (int i = 1; i < n; i++)
        {
            int minutes = getMinutes(timePoints[i]);
            ans = min(ans, minutes - prevMinutes);
            prevMinutes = minutes;
        }

        ans = min(ans, headMinutes + 1440 - prevMinutes);

        return ans;
    }
};
// @lc code=end
