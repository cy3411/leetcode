# 题目

对于某些非负整数 `k` ，如果交换 `s1` 中两个字母的位置恰好 `k` 次，能够使结果字符串等于 `s2` ，则认为字符串 `s1` 和 `s2` 的 相似度为 `k` 。

给你两个字母异位词 `s1` 和 `s2` ，返回 `s1` 和 `s2` 的相似度 `k` 的最小值。

提示：

- $1 \leq s1.length \leq 20$
- $s2.length \equiv s1.length$
- `s1` 和 `s2` 只包含集合 `{'a', 'b', 'c', 'd', 'e', 'f'}` 中的小写字母
- `s2` 是 `s1` 的一个字母异位词

# 示例

```
输入：s1 = "ab", s2 = "ba"
输出：1
```

```
输入：s1 = "abc", s2 = "bca"
输出：2
```

# 题解

## 广度优先搜索

枚举所有可能的交换方案，搜索过程中使用减枝优化搜索效率，最终找到最小的交换方案。

```js
type Data = [string, number];

function swap(cur: string, pos: number, j: number): string {
  const tmp: string[] = [...cur];
  [tmp[pos], tmp[j]] = [tmp[j], tmp[pos]];
  return tmp.join('');
}

function kSimilarity(s1: string, s2: string): number {
  const n = s1.length;
  const visited = new Set<string>();
  const queue: Data[] = [];
  // 初始化
  queue.push([s1, 0]);
  visited.add(s1);
  let step = 0;
  // 广搜
  while (queue.length) {
    const size = queue.length;
    for (let i = 0; i < size; i++) {
      let [cur, pos] = queue.shift()!;
      if (cur === s2) return step;
      // 找到 s1 第一个需要交换的位置
      while (pos < n && cur[pos] === s2[pos]) {
        pos++;
      }
      // 遍历需要交换的字符
      for (let j = pos + 1; j < n; j++) {
        if (cur[j] === s2[j]) continue;
        if (cur[j] === s2[pos]) {
          const next = swap(cur, pos, j);
          if (!visited.has(next)) {
            queue.push([next, pos + 1]);
            visited.add(next);
          }
        }
      }
    }
    step++;
  }

  return step;
}
```
