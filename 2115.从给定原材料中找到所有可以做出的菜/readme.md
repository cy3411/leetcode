# 题目

你有 `n` 道不同菜的信息。给你一个字符串数组 `recipes` 和一个二维字符串数组 `ingredients` 。第 `i` 道菜的名字为 `recipes[i]` ，如果你有它 **所有** 的原材料 `ingredients[i]` ，那么你可以 **做出** 这道菜。一道菜的原材料可能是 **另一道** 菜，也就是说 `ingredients[i]` 可能包含 `recipes` 中另一个字符串。

同时给你一个字符串数组 `supplies` ，它包含你初始时拥有的所有原材料，每一种原材料你都有无限多。

请你返回你可以做出的所有菜。你可以以 **任意顺序** 返回它们。

注意两道菜在它们的原材料中可能互相包含。

提示：

- $\color{burlywood} n \equiv recipes.length \equiv ingredients.length$
- $\color{burlywood} 1 \leq n \leq 100$
- $\color{burlywood} 1 \leq ingredients[i].length, supplies.length \leq 100$
- $\color{burlywood} 1 \leq recipes[i].length, ingredients[i][j].length, supplies[k].length \leq 10$
- `recipes[i]`, `ingredients[i][j]` 和 `supplies[k]` 只包含小写英文字母。
- 所有 `recipes` 和 `supplies` 中的值互不相同。
- `ingredients[i]` 中的字符串互不相同。

# 示例

```
输入：recipes = ["bread"], ingredients = [["yeast","flour"]], supplies = ["yeast","flour","corn"]
输出：["bread"]
解释：
我们可以做出 "bread" ，因为我们有原材料 "yeast" 和 "flour" 。
```

```
输入：recipes = ["bread","sandwich"], ingredients = [["yeast","flour"],["bread","meat"]], supplies = ["yeast","flour","meat"]
输出：["bread","sandwich"]
解释：
我们可以做出 "bread" ，因为我们有原材料 "yeast" 和 "flour" 。
我们可以做出 "sandwich" ，因为我们有原材料 "meat" 且可以做出原材料 "bread" 。
```

# 题解

## 图的入度

我们可以把菜和原材料的关系想象成一张有向图，菜和原材料之间是有边的，菜的入度就是原材料的数量。

默认菜的入度为所需原材料的数量，每提供一种原材料，入度就减一。当菜的入度为 `0` 的时候，表示可以制作这道菜。

题目中，菜也可以作为其他菜的原材料，所以当某个菜的入度为 `0` 的时候，也需要去更新图的信息。

定义哈希表 `indeg` ，用来记录每个菜的入度。

定义哈希表 `graph` 用来维护每个原材料和菜之间的关系。

遍历 `supplies` 数组，每次遍历一个原材料，更新 `indeg`。

最后返回 `indeg` 中入度为 `0` 的菜即可。

```ts
function findAllRecipes(recipes: string[], ingredients: string[][], supplies: string[]): string[] {
  // 原材料数量发生变化，更新对应数据
  const updateGraph = (supply: string): void => {
    if (graph.has(supply)) {
      const supplyGraph = graph.get(supply);
      for (const x of supplyGraph) {
        if (indeg.has(x)) {
          // 菜需要的原材料数量减少1
          indeg.set(x, indeg.get(x) - 1);
          // 当前菜可以制作，可以提供新的原材料
          if (indeg.get(x) == 0) updateGraph(x);
        }
      }
    }
  };

  // 图的入度，表示菜需要的材料数量，如果材料数量为0，则表示可以做出菜
  const indeg = new Map<string, number>();
  // 原材料和菜谱的映射关系
  const graph = new Map<string, Set<string>>();
  // 初始化图
  for (const [i, recipe] of recipes.entries()) {
    indeg.set(recipe, ingredients[i].length);
    for (const x of ingredients[i]) {
      if (!graph.has(x)) {
        graph.set(x, new Set());
      }
      graph.get(x).add(recipe);
    }
  }

  // 遍历supplies,更新数据
  for (const supply of supplies) {
    // 直接提供的材料就是菜
    if (indeg.has(supply)) {
      indeg.set(supply, 0);
    }
    // 使用提供的材料，取更新图
    updateGraph(supply);
  }

  // 计算结果，菜中入度为0的表示材料都齐全，可以制作
  const ans = [];
  for (const [recipe, count] of indeg.entries()) {
    if (count === 0) ans.push(recipe);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    void updateGraph(string x, unordered_map<string, int> &indeg, unordered_map<string, unordered_set<string>> &graph)
    {
        for (auto x : graph[x])
        {
            indeg[x]--;
            if (indeg[x] == 0)
                updateGraph(x, indeg, graph);
        }
    }

    vector<string> findAllRecipes(vector<string> &recipes, vector<vector<string>> &ingredients, vector<string> &supplies)
    {
        unordered_map<string, int> indeg;
        unordered_map<string, unordered_set<string>> graph;
        for (int i = 0; i < recipes.size(); i++)
        {
            indeg[recipes[i]] = ingredients[i].size();
            for (auto x : ingredients[i])
            {
                graph[x].insert(recipes[i]);
            }
        }

        for (auto x : supplies)
        {
            indeg[x] = 0;
            updateGraph(x, indeg, graph);
        }

        vector<string> ans;
        for (auto x : recipes)
        {
            if (indeg[x] == 0)
            {
                ans.push_back(x);
            }
        }
        return ans;
    }
};
```
