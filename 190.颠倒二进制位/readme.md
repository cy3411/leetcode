# 题目

颠倒给定的 32 位无符号整数的二进制位。

提示：

- 请注意，在某些语言（如 Java）中，没有无符号整数类型。在这种情况下，输入和输出都将被指定为有符号整数类型，并且不应影响您的实现，因为无论整数是有符号的还是无符号的，其内部的二进制表示形式都是相同的。
- 在 Java 中，编译器使用二进制补码记法来表示有符号整数。因此，在上面的 示例 2 中，输入表示有符号整数 -3，输出表示有符号整数 -1073741825。

提示：

- 输入是一个长度为 32 的二进制字符串

# 示例

```
输入: 00000010100101000001111010011100
输出: 00111001011110000010100101000000
解释: 输入的二进制串 00000010100101000001111010011100 表示无符号整数 43261596，
     因此返回 964176192，其二进制表示形式为 00111001011110000010100101000000。
```

```
输入：11111111111111111111111111111101
输出：10111111111111111111111111111111
解释：输入的二进制串 11111111111111111111111111111101 表示无符号整数 4294967293，
     因此返回 3221225471 其二进制表示形式为 10111111111111111111111111111111 。
```

# 方法

## 迭代低位

每次使用 n 的最低位去更新结果的最低位，然后将 n 右移 1 位，将结果左移 1 位。

```js
/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
var reverseBits = function (n) {
  let result = 0;

  for (let i = 0; i < 32; i++) {
    // 将答案左移
    result = result << 1;
    // 每次取n的最低位去更新答案的最低位
    result += n & 1;
    // 将n右移动
    n = n >> 1;
  }
  // 消除符号位
  return result >>> 0;
};
```

## 位运算分治

若要翻转一个二进制串，可以将其均分成左右两部分，对每部分递归执行翻转操作，然后将左半部分拼在右半部分的后面，即完成了翻转。

由于左右两部分的计算方式是相似的，利用位掩码和位移运算，我们可以自底向上地完成这一分治流程。

```js
/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
var reverseBits = function (n) {
  const M1 = 0x55555555; // 01010101010101010101010101010101
  const M2 = 0x33333333; // 00110011001100110011001100110011
  const M4 = 0x0f0f0f0f; // 00001111000011110000111100001111
  const M8 = 0x00ff00ff; // 00000000111111110000000011111111

  // 11交换
  n = ((n >>> 1) & M1) | ((n & M1) << 1);
  // 22交换
  n = ((n >>> 2) & M2) | ((n & M2) << 2);
  // 44交换
  n = ((n >>> 4) & M4) | ((n & M4) << 4);
  // 88交换
  n = ((n >>> 8) & M8) | ((n & M8) << 8);
  //  16 16 交换
  return ((n >>> 16) | (n << 16)) >>> 0;
};
```

## 逐位颠倒

```ts
function reverseBits(n: number): number {
  let ans: number = 0;
  // 逐位颠倒，如果n的高位只有0了，可以提前结束循环
  for (let i = 0; i < 32 && n > 0; i++) {
    // n的低位与ans高位
    ans |= (n & 1) << (31 - i);
    // 每次消掉1个低位
    n >>>= 1;
  }
  // 消除符号位
  return ans >>> 0;
}
```
