/*
 * @lc app=leetcode.cn id=190 lang=typescript
 *
 * [190] 颠倒二进制位
 */

// @lc code=start
function reverseBits(n: number): number {
  let ans: number = 0;
  // 逐位颠倒，如果n的高位只有0了，可以提前结束循环
  for (let i = 0; i < 32 && n > 0; i++) {
    // n的低位与ans高位
    ans |= (n & 1) << (31 - i);
    // 每次消掉1个低位
    n >>>= 1;
  }
  return ans >>> 0;
}
// @lc code=end
