/*
 * @lc app=leetcode.cn id=890 lang=cpp
 *
 * [890] 查找和替换模式
 */

// @lc code=start
class Solution {
public:
    vector<string> findAndReplacePattern(vector<string> &words,
                                         string pattern) {
        vector<string> ans;
        int n = pattern.size();
        for (auto word : words) {
            // word对应pattern的映射
            unordered_map<char, char> mp;
            // pattern中字符是否被映射过
            unordered_set<char> uniq;
            // 是否配置标识
            bool flag = true;
            for (int i = 0; i < n; i++) {
                char x = word[i];
                char y = pattern[i];
                if (mp.find(x) != mp.end()) {
                    if (mp[x] != y) {
                        flag = false;
                        break;
                    }
                } else {
                    if (uniq.find(y) != uniq.end()) {
                        flag = false;
                        break;
                    }
                    mp[x] = y;
                    uniq.insert(y);
                }
            }
            if (flag) ans.push_back(word);
        }
        return ans;
    }
};
// @lc code=end
