# 题目

你有一个单词列表 `words` 和一个模式 `pattern` ，你想知道 `words` 中的哪些单词与模式匹配。

如果存在字母的排列 `p` ，使得将模式中的每个字母 `x` 替换为 `p(x)` 之后，我们就得到了所需的单词，那么单词与模式是匹配的。

（回想一下，字母的排列是从字母到字母的双射：每个字母映射到另一个字母，没有两个字母映射到同一个字母。）

返回 `words` 中与给定模式匹配的单词列表。

你可以按任何顺序返回答案。

提示：

- $1 \leq words.length \leq 50$
- $1 \leq pattern.length = words[i].length \leq 20$

# 示例

```
输入：words = ["abc","deq","mee","aqq","dkd","ccc"], pattern = "abb"
输出：["mee","aqq"]
解释：
"mee" 与模式匹配，因为存在排列 {a -> m, b -> e, ...}。
"ccc" 与模式不匹配，因为 {a -> c, b -> c, ...} 不是排列。
因为 a 和 b 映射到同一个字母。
```

# 题解

## 哈希表

利用哈希表来构造 word 到 pattern 的映射字符映射，然后遍历 words 判断是否存在映射。

```ts
function findAndReplacePattern(words: string[], pattern: string): string[] {
  const n = words.length;
  const matchWord = (w1: string, w2: string): boolean => {
    const map = new Map<string, string>();
    for (let i = 0; i < w1.length; i++) {
      const x = w1[i];
      const y = w2[i];
      if (!map.has(x)) {
        map.set(x, y);
      } else if (map.get(x) !== y) {
        return false;
      }
    }
    return true;
  };

  const ans: string[] = [];
  for (let i = 0; i < n; i++) {
    //  双向映射同时匹配
    if (matchWord(words[i], pattern) && matchWord(pattern, words[i])) {
      ans.push(words[i]);
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<string> findAndReplacePattern(vector<string> &words,
                                         string pattern) {
        vector<string> ans;
        int n = pattern.size();
        for (auto word : words) {
            // word对应pattern的映射
            unordered_map<char, char> mp;
            // pattern中字符是否被映射过
            unordered_set<char> uniq;
            // 是否配置标识
            bool flag = true;
            for (int i = 0; i < n; i++) {
                char x = word[i];
                char y = pattern[i];
                if (mp.find(x) != mp.end()) {
                    if (mp[x] != y) {
                        flag = false;
                        break;
                    }
                } else {
                    if (uniq.find(y) != uniq.end()) {
                        flag = false;
                        break;
                    }
                    mp[x] = y;
                    uniq.insert(y);
                }
            }
            if (flag) ans.push_back(word);
        }
        return ans;
    }
};
```
