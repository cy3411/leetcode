# 题目

在英语中，我们有一个叫做 `词根`(root) 的概念，可以词根后面添加其他一些词组成另一个较长的单词——我们称这个词为 `继承词`(successor)。例如，词根 `an`，跟随着单词 `other`(其他)，可以形成新的单词 `another`(另一个)。

现在，给定一个由许多词根组成的词典 `dictionary` 和一个用空格分隔单词形成的句子 `sentence`。你需要将句子中的所有继承词用词根替换掉。如果继承词有许多可以形成它的词根，则用最短的词根替换它。

你需要输出替换之后的句子。

提示：

- $1 \leq dictionary.length \leq 1000$
- $1 \leq dictionary[i].length \leq 100$
- `dictionary[i]` 仅由小写字母组成。
- $1 \leq sentence.length \leq 10^6$
- `sentence` 仅由小写字母和空格组成。
- `sentence` 中单词的总量在范围 `[1, 1000]` 内。
- `sentence` 中每个单词的长度在范围 `[1, 1000]` 内。
- `sentence` 中单词之间由一个空格隔开。
- `sentence` 没有前导或尾随空格。

# 示例

```
输入：dictionary = ["cat","bat","rat"], sentence = "the cattle was rattled by the battery"
输出："the cat was rat by the bat"
```

```
输入：dictionary = ["a","b","c"], sentence = "aadsfasf absbs bbab cadsfafs"
输出："a a b c"
```

# 题解

## 哈希表

利用哈希表存储词根，然后对 sentence 中的单词，从短到长遍历它的前缀，如果前缀在哈希表中，则替换。

```ts
function replaceWords(dictionary: string[], sentence: string): string {
  // 利用哈希表存储词根
  const map = new Set(dictionary);
  const words = sentence.split(' ');

  for (let i = 0; i < words.length; i++) {
    const word = words[i];
    for (let j = 0; j < word.length; j++) {
      const root = word.substring(0, j + 1);
      // 找到词根，替换
      if (map.has(root)) {
        words[i] = root;
        break;
      }
    }
  }

  return words.join(' ');
}
```

## Trie 树

利用 Trie 树存储词根，然后对 sentence 中的单词，从短到长遍历它的前缀，如果前缀在 Trie 树中，则替换。

```cpp
struct TireNode {
    TireNode *next[26];
    bool isEnd;
    string word;
    TireNode() {
        memset(next, 0, sizeof(next));
        isEnd = false;
        word = "";
    }
};

struct Tire {
    TireNode *root;
    Tire() { root = new TireNode(); }
    void insert(string word) {
        TireNode *p = root;
        for (auto c : word) {
            if (p->next[c - 'a'] == NULL) {
                p->next[c - 'a'] = new TireNode();
            }
            p = p->next[c - 'a'];
        }
        p->isEnd = true;
        p->word = word;
    }
    string search(string word) {
        TireNode *p = root;
        for (auto c : word) {
            if (p->next[c - 'a'] == NULL) {
                return "";
            }
            p = p->next[c - 'a'];
            if (p->isEnd) {
                return p->word;
            }
        }
        return "";
    }
};

class Solution {
public:
    string replaceWords(vector<string> &dictionary, string sentence) {
        Tire tire;
        for (auto word : dictionary) {
            tire.insert(word);
        }
        // 分割字符串为数组
        stringstream ss(sentence);
        string temp;
        vector<string> words;
        while (ss >> temp) {
            words.push_back(temp);
        }
        // 替换单词
        for (int i = 0; i < words.size(); i++) {
            string word = words[i];
            string replace = tire.search(word);
            if (replace != "") {
                words[i] = replace;
            }
        }
        // 数组转字符串输出
        return accumulate(words.begin(), words.end(), string{},
                          [](string a, string b) {
                              if (a == "") return b;
                              return a + " " + b;
                          });
    }
};
```
