# 题目

你有一个带有四个圆形拨轮的转盘锁。每个拨轮都有 10 个数字： `'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'` 。每个拨轮可以自由旋转：例如把 `'9'` 变为 `'0'`，`'0'` 变为 `'9'`。每次旋转都只能旋转一个拨轮的一位数字。

锁的初始数字为 `'0000'` ，一个代表四个拨轮的数字的字符串。

列表 `deadends` 包含了一组死亡数字，一旦拨轮的数字和列表里的任何一个元素相同，这个锁将会被永久锁定，无法再被旋转。

字符串 `target` 代表可以解锁的数字，你需要给出最小的旋转次数，如果无论如何不能解锁，返回 -1。

提示：

- 死亡列表 deadends 的长度范围为 [1, 500]。
- 目标数字 target 不会在 deadends 之中。
- 每个 deadends 和 target 中的字符串的数字会在 10,000 个可能的情况 '0000' 到 '9999' 中产生。

# 示例

```
输入：deadends = ["0201","0101","0102","1212","2002"], target = "0202"
输出：6
解释：
可能的移动序列为 "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202"。
注意 "0000" -> "0001" -> "0002" -> "0102" -> "0202" 这样的序列是不能解锁的，
因为当拨动到 "0102" 时这个锁就会被锁定。
```

# 题解

## 广度优先搜索

```ts
class Data {
  constructor(public s: string = '', public l: number = 0) {
    this.s = s;
    this.l = l;
  }
}

function openLock(deadends: string[], target: string): number {
  const deadLock = new Set([...deadends]);
  // 开始位置是否是死锁
  if (deadLock.has('0000')) return -1;

  const getStr = (s: string, i: number, j: number): string => {
    const arr = s.split('');
    let next: number;
    switch (j) {
      case 0:
        next = Number(arr[i]) + 1 > 9 ? 0 : Number(arr[i]) + 1;
        arr[i] = String(next);
        break;
      case 1:
        next = Number(arr[i]) - 1 < 0 ? 9 : Number(arr[i]) - 1;
        arr[i] = String(next);
        break;
    }
    return arr.join('');
  };

  const queue: Data[] = [];
  deadLock.add('0000');
  queue.push(new Data('0000', 0));
  while (queue.length) {
    const data = queue.shift();
    if (data.s === target) return data.l;
    // 搜索其他状态
    // 4位数，每个数字2种状态：加1或者减1
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < 2; j++) {
        const temp = getStr(data.s, i, j);
        if (deadLock.has(temp)) continue;
        deadLock.add(temp);
        queue.push(new Data(temp, data.l + 1));
      }
    }
  }

  return -1;
}
```
