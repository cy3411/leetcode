class Solution
{
public:
    int numSubarrayProductLessThanK(vector<int> &nums, int k)
    {
        if (k <= 1)
        {
            return 0;
        }
        int n = nums.size(), l = 0, sum = 1, ans = 0;
        for (int r = 0; r < n; r++)
        {
            sum *= nums[r];
            while (sum >= k)
            {
                sum /= nums[l];
                l++;
            }
            ans += r - l + 1;
        }
        return ans;
    }
};