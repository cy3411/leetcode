// @algorithm @lc id=1000244 lang=typescript
// @title ZVAVXX
function numSubarrayProductLessThanK(nums: number[], k: number): number {
  if (k <= 1) return 0;
  let l = 0;
  let sum = 1;
  let ans = 0;
  for (let r = 0; r < nums.length; r++) {
    sum *= nums[r];
    while (sum >= k) {
      sum /= nums[l];
      l++;
    }
    ans += r - l + 1;
  }
  return ans;
}
