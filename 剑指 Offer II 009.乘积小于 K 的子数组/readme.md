# 题目

给定一个正整数数组 `nums` 和整数 `k` ，请找出该数组内乘积小于 `k` 的连续的子数组的个数。

提示:

- $1 \leq nums.length \leq 3 * 10^4$
- $1 \leq nums[i] \leq 1000$
- $0 \leq k \leq 10^6$

注意：[本题与主站 713 题相同](https://leetcode-cn.com/problems/subarray-product-less-than-k/)

# 示例

```
输入: nums = [10,5,2,6], k = 100
输出: 8
解释: 8 个乘积小于 100 的子数组分别为: [10], [5], [2], [6], [10,5], [5,2], [2,6], [5,2,6]。
需要注意的是 [10,5,2] 并不是乘积小于100的子数组。
```

```
输入: nums = [1,2,3], k = 0
输出: 0
```

# 题解

## 滑动窗口

利用滑动窗口维护一个区间，每次更新区间的乘积，如果区间乘积小于 `k`，则区间长度加 1，否则区间长度减 1。

遍历过程中，同步更新答案。

```ts
function numSubarrayProductLessThanK(nums: number[], k: number): number {
  if (k <= 1) return 0;
  let l = 0;
  let sum = 1;
  let ans = 0;
  for (let r = 0; r < nums.length; r++) {
    sum *= nums[r];
    while (sum >= k) {
      sum /= nums[l];
      l++;
    }
    ans += r - l + 1;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int numSubarrayProductLessThanK(vector<int> &nums, int k)
    {
        if (k <= 1)
        {
            return 0;
        }
        int n = nums.size(), l = 0, sum = 1, ans = 0;
        for (int r = 0; r < n; r++)
        {
            sum *= nums[r];
            while (sum >= k)
            {
                sum /= nums[l];
                l++;
            }
            ans += r - l + 1;
        }
        return ans;
    }
};
```
