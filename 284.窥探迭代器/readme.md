# 题目

请你设计一个迭代器，除了支持 `hasNext` 和 `next` 操作外，还支持 `peek` 操作。

实现 `PeekingIterator` 类：

- `PeekingIterator(int[] nums)` 使用指定整数数组 `nums` 初始化迭代器。
- `int next()` 返回数组中的下一个元素，并将指针移动到下个元素处。
- `bool hasNext()` 如果数组中存在下一个元素，返回 `true` ；否则，返回 `false` 。
- `int peek()` 返回数组中的下一个元素，但 **不** 移动指针。

提示：

- $\color{burlywood}1 \leq nums.length \leq 1000$
- $\color{burlywood}1 \leq nums[i] \leq 1000$
- 对 `next` 和 `peek` 的调用均有效
- ` next``、hasNext ` 和 `peek` 最多调用 `1000` 次

进阶：你将如何拓展你的设计？使之变得通用化，从而适应所有的类型，而不只是整数型？

# 示例

```
输入：
["PeekingIterator", "next", "peek", "next", "next", "hasNext"]
[[[1, 2, 3]], [], [], [], [], []]
输出：
[null, 1, 2, 2, 3, false]

解释：
PeekingIterator peekingIterator = new PeekingIterator([1, 2, 3]); // [1,2,3]
peekingIterator.next();    // 返回 1 ，指针移动到下一个元素 [1,2,3]
peekingIterator.peek();    // 返回 2 ，指针未发生移动 [1,2,3]
peekingIterator.next();    // 返回 2 ，指针移动到下一个元素 [1,2,3]
peekingIterator.next();    // 返回 3 ，指针移动到下一个元素 [1,2,3]
peekingIterator.hasNext(); // 返回 False
```

# 题解

## 迭代器

跟正常的迭代器相比，题目就多了一个 peek 功能。

定义一个变量，缓存 next 的值，调用 peek 的时候，直接返回这个缓存变量的值就行了，其他的功能和正常的迭代器一样。

```ts
class PeekingIterator {
  cur: number;
  isEnd: boolean;
  constructor(private iterator: Iterator) {
    if (iterator.hasNext()) {
      this.cur = iterator.next();
    } else {
      this.isEnd = true;
    }
  }

  peek(): number {
    return this.cur;
  }

  next(): number {
    if (this.isEnd) return -1;
    const res = this.cur;
    if (this.iterator.hasNext()) {
      this.cur = this.iterator.next();
    } else {
      this.isEnd = true;
    }
    return res;
  }

  hasNext(): boolean {
    return !this.isEnd;
  }
}
```

```cpp
class PeekingIterator : public Iterator
{
private:
	int nextVal;
	int isEnd;

public:
	PeekingIterator(const vector<int> &nums) : Iterator(nums)
	{
		// Initialize any member here.
		// **DO NOT** save a copy of nums and manipulate it directly.
		// You should only use the Iterator interface methods.
		isEnd = false;
		if (Iterator::hasNext())
		{
			nextVal = Iterator::next();
		}
		else
		{
			isEnd = true;
		}
	}

	// Returns the next element in the iteration without advancing the iterator.
	int peek()
	{
		return nextVal;
	}

	// hasNext() and next() should behave the same as in the Iterator interface.
	// Override them if needed.
	int next()
	{
		if (isEnd)
		{

			return -1;
		}
		int ret = nextVal;
		if (Iterator::hasNext())
		{
			nextVal = Iterator::next();
		}
		else
		{
			isEnd = true;
		}
		return ret;
	}

	bool hasNext() const
	{
		return !isEnd;
	}
};
```
