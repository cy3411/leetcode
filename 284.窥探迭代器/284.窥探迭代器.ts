/*
 * @lc app=leetcode.cn id=284 lang=typescript
 *
 * [284] 顶端迭代器
 */

// @lc code=start
/**
 * // This is the Iterator's API interface.
 * // You should not implement it, or speculate about its implementation
 * class Iterator {
 *      hasNext(): boolean {}
 *
 *      next(): number {}
 * }
 */

class PeekingIterator {
  cur: number;
  isEnd: boolean;
  constructor(private iterator: Iterator) {
    if (iterator.hasNext()) {
      this.cur = iterator.next();
    } else {
      this.isEnd = true;
    }
  }

  peek(): number {
    return this.cur;
  }

  next(): number {
    if (this.isEnd) return -1;
    const res = this.cur;
    if (this.iterator.hasNext()) {
      this.cur = this.iterator.next();
    } else {
      this.isEnd = true;
    }
    return res;
  }

  hasNext(): boolean {
    return !this.isEnd;
  }
}

/**
 * Your PeekingIterator object will be instantiated and called as such:
 * var obj = new PeekingIterator(iterator)
 * var param_1 = obj.peek()
 * var param_2 = obj.next()
 * var param_3 = obj.hasNext()
 */
// @lc code=end
