# 题目

给定一个字符串数组 `words`，请计算当两个字符串 `words[i]` 和 `words[j]` 不包含相同字符时，它们长度的乘积的最大值。假设字符串中只包含英语的小写字母。如果没有不包含相同字符的一对字符串，返回 `0`。

提示：

- $2 \leq words.length \leq 1000$
- $1 \leq words[i].length \leq 1000$
- `words[i]` 仅包含小写字母

注意：[本题与主站 318 题相同](https://leetcode-cn.com/problems/maximum-product-of-word-lengths/)

# 示例

```
输入: words = ["abcw","baz","foo","bar","fxyz","abcdef"]
输出: 16
解释: 这两个单词为 "abcw", "fxyz"。它们不包含相同字符，且长度的乘积最大。
```

```
输入: words = ["a","ab","abc","d","cd","bcd","abcd"]
输出: 4
解释: 这两个单词为 "ab", "cd"。
```

# 题解

## 位记录

利用二进制位记录每个字符出现的次数，然后每次比较两个单词的位记录，如果不同，则更新答案。

```ts
function maxProduct(words: string[]): number {
  const n = words.length;
  const bits = new Array(n).fill(0);
  const base = 'a'.charCodeAt(0);
  // 按位记录每个字符是否出现过
  for (let i = 0; i < n; i++) {
    for (const c of words[i]) {
      bits[i] |= 1 << (c.charCodeAt(0) - base);
    }
  }

  let ans = 0;
  for (let i = 0; i < n - 1; i++) {
    for (let j = i + 1; j < n; j++) {
      // 如果两个单词的字符不重复，则更新最大长度
      if ((bits[i] & bits[j]) === 0) {
        ans = Math.max(ans, words[i].length * words[j].length);
      }
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int maxProduct(vector<string>& words) {
      int n = words.size(), i, j;
      vector<int> bits(n, 0);
      for (i = 0 ; i < n; i++) {
        for (auto c : words[i]) {
          bits[i] |= (1 << (c - 'a'));
        }
      }
      int ans = 0;
      for (i = 0; i < n - 1; i++) {
        for (j = i + 1; j < n; j++) {
          if (bits[i] & bits[j]) {
            continue;
          }
          ans = max(ans, (int)(words[i].size() * words[j].size()));
        }
      }
      return ans;
    }
};
```
