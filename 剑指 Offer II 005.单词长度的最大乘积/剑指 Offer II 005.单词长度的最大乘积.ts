// @algorithm @lc id=1000236 lang=typescript
// @title aseY1I
function maxProduct(words: string[]): number {
  const n = words.length;
  const bits = new Array(n).fill(0);
  const base = 'a'.charCodeAt(0);
  // 按位记录每个字符是否出现过
  for (let i = 0; i < n; i++) {
    for (const c of words[i]) {
      bits[i] |= 1 << (c.charCodeAt(0) - base);
    }
  }

  let ans = 0;
  for (let i = 0; i < n - 1; i++) {
    for (let j = i + 1; j < n; j++) {
      // 如果两个单词的字符不重复，则更新最大长度
      if ((bits[i] & bits[j]) === 0) {
        ans = Math.max(ans, words[i].length * words[j].length);
      }
    }
  }

  return ans;
}
