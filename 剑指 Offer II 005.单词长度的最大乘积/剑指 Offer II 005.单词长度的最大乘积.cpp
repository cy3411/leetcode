class Solution
{
public:
  int maxProduct(vector<string> &words)
  {
    int n = words.size(), i, j;
    vector<int> bits(n, 0);
    for (i = 0; i < n; i++)
    {
      for (auto c : words[i])
      {
        bits[i] |= (1 << (c - 'a'));
      }
    }
    int ans = 0;
    for (i = 0; i < n - 1; i++)
    {
      for (j = i + 1; j < n; j++)
      {
        if (bits[i] & bits[j])
        {
          continue;
        }
        ans = max(ans, (int)(words[i].size() * words[j].size()));
      }
    }
    return ans;
  }
};