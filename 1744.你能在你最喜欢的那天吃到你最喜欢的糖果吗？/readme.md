# 题目

给你一个下标从 `0` 开始的正整数数组 ``candiesCount` ，其中 candiesCount[i]`表示你拥有的第`i`类糖果的数目。同时给你一个二维数组`queries`，其中`queries[i] = [favoriteTypei, favoriteDayi, dailyCapi]` 。

你按照如下规则进行一场游戏：

- 你从第 `0` 天开始吃糖果。
- 你在吃完 所有 第 `i - 1` 类糖果之前，不能 吃任何一颗第 `i` 类糖果。
- 在吃完所有糖果之前，你必须每天 **至少** 吃 **一颗** 糖果。
- 请你构建一个布尔型数组 `answer` ，满足 `answer.length == queries.length` 。`answer[i]` 为 `true` 的条件是：在每天吃 **不超过** `dailyCapi` 颗糖果的前提下，你可以在第 `favoriteDayi` 天吃到第 `favoriteTypei` 类糖果；否则 `answer[i] 为 false` 。注意，只要满足上面 3 条规则中的第二条规则，你就可以在同一天吃不同类型的糖果。

请你返回得到的数组 `answer` 。

# 示例

```
输入：candiesCount = [7,4,5,3,8], queries = [[0,2,2],[4,2,4],[2,13,1000000000]]
输出：[true,false,true]
提示：
1- 在第 0 天吃 2 颗糖果(类型 0），第 1 天吃 2 颗糖果（类型 0），第 2 天你可以吃到类型 0 的糖果。
2- 每天你最多吃 4 颗糖果。即使第 0 天吃 4 颗糖果（类型 0），第 1 天吃 4 颗糖果（类型 0 和类型 1），你也没办法在第 2 天吃到类型 4 的糖果。换言之，你没法在每天吃 4 颗糖果的限制下在第 2 天吃到第 4 类糖果。
3- 如果你每天吃 1 颗糖果，你可以在第 13 天吃到类型 2 的糖果。
```

# 题解

## 前缀和

计算 `candiesCount` 的前缀和。

遍历 `queries` ，计算到达 `queries[i][1]` 天需要吃最少和最多的糖果数量。

然后判断糖果的数量是否在前缀和的 `[queries[i][0], queries[i][1]]`区间内即可。

```ts
function canEat(candiesCount: number[], queries: number[][]): boolean[] {
  const size = candiesCount.length;
  const prefixSum = new Array(size + 1).fill(0);
  // 计算前缀和
  for (let i = 1; i <= size; i++) {
    prefixSum[i] = prefixSum[i - 1] + candiesCount[i - 1];
  }
  const result = [];
  // 遍历queries，计算queries[i][1]+1天能吃的最多和最少的糖果。
  // 如果结果在prefixSum[type]和prefixSum[type-1]之间即为true
  for (let [type, day, cap] of queries) {
    let min = (day + 1) * 1;
    let max = (day + 1) * cap;
    let flag = false;
    if (max > prefixSum[type] && min <= prefixSum[type + 1]) flag = true;
    result.push(flag);
  }

  return result;
}
```

```cpp
class Solution
{
public:
    vector<bool> canEat(vector<int> &candiesCount, vector<vector<int>> &queries)
    {
        int n = candiesCount.size(), m = queries.size();
        long long preSum[n + 1];
        preSum[0] = 0;
        for (int i = 1; i <= n; i++)
        {
            preSum[i] = preSum[i - 1] + candiesCount[i - 1];
        }

        vector<bool> ans(m, false);
        for (int i = 0; i < m; i++)
        {
            int t = queries[i][0], d = queries[i][1], c = queries[i][2];
            if (preSum[t + 1] >= (d + 1) && (preSum[t] / c - 1 < d))
            {
                ans[i] = true;
            }
        }
        return ans;
    }
};
```
