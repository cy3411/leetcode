/*
 * @lc app=leetcode.cn id=1744 lang=cpp
 *
 * [1744] 你能在你最喜欢的那天吃到你最喜欢的糖果吗？
 */

// @lc code=start
class Solution
{
public:
    vector<bool> canEat(vector<int> &candiesCount, vector<vector<int>> &queries)
    {
        int n = candiesCount.size(), m = queries.size();
        long long preSum[n + 1];
        preSum[0] = 0;
        for (int i = 1; i <= n; i++)
        {
            preSum[i] = preSum[i - 1] + candiesCount[i - 1];
        }

        vector<bool> ans(m, false);
        for (int i = 0; i < m; i++)
        {
            int t = queries[i][0], d = queries[i][1], c = queries[i][2];
            if (preSum[t + 1] >= (d + 1) && (preSum[t] / c - 1 < d))
            {
                ans[i] = true;
            }
        }
        return ans;
    }
};
// @lc code=end
