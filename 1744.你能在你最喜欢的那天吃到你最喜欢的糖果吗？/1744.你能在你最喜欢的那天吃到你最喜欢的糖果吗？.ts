/*
 * @lc app=leetcode.cn id=1744 lang=typescript
 *
 * [1744] 你能在你最喜欢的那天吃到你最喜欢的糖果吗？
 */

// @lc code=start
function canEat(candiesCount: number[], queries: number[][]): boolean[] {
  const size = candiesCount.length;
  const prefixSum = new Array(size + 1).fill(0);
  // 计算前缀和
  for (let i = 1; i <= size; i++) {
    prefixSum[i] = prefixSum[i - 1] + candiesCount[i - 1];
  }
  const result = [];
  // 遍历queries，计算queries[i][1]+1天能吃的最多和最少的糖果。
  // 如果结果在prefixSum[type]和prefixSum[type-1]之间即为true
  for (let [type, day, cap] of queries) {
    let min = (day + 1) * 1;
    let max = (day + 1) * cap;
    let flag = false;
    if (max > prefixSum[type] && min <= prefixSum[type + 1]) flag = true;
    result.push(flag);
  }

  return result;
}
// @lc code=end
