# 题目
给定一个由表示变量之间关系的字符串方程组成的数组，每个字符串方程 `equations[i]` 的长度为 `4`，并采用两种不同的形式之一：`"a==b"` 或 `"a!=b"`。在这里，a 和 b 是小写字母（不一定不同），表示单字母变量名。

只有当可以将整数分配给变量名，以便满足所有给定的方程时才返回 `true`，否则返回 `false`。 

提示：
+ `1 <= equations.length <= 500`
+ `equations[i].length == 4`
+ `equations[i][0] 和 equations[i][3] 是小写字母`
+ `equations[i][1] 要么是 '='，要么是 '!'`
+ `equations[i][2] 是 '='`

# 示例
```
输入：["a==b","b!=a"]
输出：false
解释：如果我们指定，a = 1 且 b = 1，那么可以满足第一个方程，但无法满足第二个方程。没有办法分配变量同时满足这两个方程。
```

```
输入：["b==a","a==b"]
输出：true
解释：我们可以指定 a = 1 且 b = 1 以满足满足这两个方程。
```
# 题解 
## 并查集
第一次遍历，将等于操作的元素放入并查集。

第二次遍历，判断不等于操作的元素是否在并查集中，返回结果。

```js
class UnionFind {
  parent: number[];
  size: number[];
  count: number;
  constructor(n: number = 0) {
    this.parent = new Array(n).fill(0).map((_, i) => i);
    this.size = new Array(n).fill(1);
    this.count = n;
  }
  find(x: number): number {
    return this.parent[x] === x ? x : (this.parent[x] = this.find(this.parent[x]));
  }
  union(x: number, y: number): void {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.parent[rx] = ry;
    this.size[ry] += this.size[rx];
    this.count--;
  }
  isConnected(x: number, y: number) {
    return this.find(x) === this.find(y);
  }
}
function equationsPossible(equations: string[]): boolean {
  const size = equations.length;
  const unionFind = new UnionFind(26);
  const base = 'a'.charCodeAt(0);
  // 将=操作加入并查集
  for (let i = 0; i < size; i++) {
    const equation = equations[i];
    let x = equation.charCodeAt(0) - base;
    let y = equation.charCodeAt(3) - base;
    if (equation[1] === '!') continue;
    unionFind.union(x, y);
  }
  // 判断!=操作的元素是否在并查集
  for (let i = 0; i < size; i++) {
    const equation = equations[i];
    let x = equation.charCodeAt(0) - base;
    let y = equation.charCodeAt(3) - base;
    if (equation[1] === '=') continue;
    if (unionFind.isConnected(x, y)) return false;
  }

  return true;
}
```