/*
 * @lc app=leetcode.cn id=38 lang=typescript
 *
 * [38] 外观数列
 */

// @lc code=start
function countAndSay(n: number): string {
  let ans = '1';
  for (let i = 2; i <= n; i++) {
    let res = [];
    let left = 0;
    let right = 0;
    // 对一项的描述
    while (right < ans.length) {
      // 计算连续重复数字
      while (right < ans.length && ans[left] === ans[right]) {
        right++;
      }
      res.push(`${right - left}${ans[left]}`);
      left = right;
    }
    ans = res.join('');
  }
  return ans;
}
// @lc code=end
