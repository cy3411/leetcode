# 题目

给定一个字符串 `queryIP` 。如果是有效的 IPv4 地址，返回 `"IPv4"` ；如果是有效的 IPv6 地址，返回 `"IPv6"` ；如果不是上述类型的 IP 地址，返回 `"Neither"` 。

有效的 **IPv4 地址** 是 `“x1.x2.x3.x4”` 形式的 IP 地址。 其中 $0 \leq x_i \leq 255$ 且 $x_i$ 不能包含 **前导零**。例如: `“192.168.1.1”` 、 `“192.168.1.0”` 为有效 IPv4 地址， `“192.168.01.1”` 为无效 IPv4 地址; `“192.168.1.00”` 、 `“192.168@1.1”` 为无效 IPv4 地址。

一个有效的 **IPv6 地址** 是一个格式为 `“x1:x2:x3:x4:x5:x6:x7:x8”` 的 IP 地址，其中:

- $1 \leq x_{i}.length \leq 4$
- $x_i$ 是一个 **十六进制字符串** ，可以包含数字、小写英文字母( `'a'` 到 `'f'` )和大写英文字母( `'A'` 到 `'F'` )。
- 在 $x_i$ 中允许前导零。

例如 `"2001:0db8:85a3:0000:0000:8a2e:0370:7334"` 和 `"2001:db8:85a3:0:0:8A2E:0370:7334"` 是有效的 IPv6 地址，而 `"2001:0db8:85a3::8A2E:037j:7334"` 和 `"02001:0db8:85a3:0000:0000:8a2e:0370:7334"` 是无效的 IPv6 地址。

提示：

- `queryIP` 仅由英文字母，数字，字符 '.' 和 ':' 组成。

# 示例

```
输入：queryIP = "172.16.254.1"
输出："IPv4"
解释：有效的 IPv4 地址，返回 "IPv4"
```

```
输入：queryIP = "2001:0db8:85a3:0:0:8A2E:0370:7334"
输出："IPv6"
解释：有效的 IPv6 地址，返回 "IPv6"
```

# 题解

## 模拟

按照题意，分别对 IPV4 和 IPV6 进行模拟，并判断是否是有效的 IP 地址。

IPV4 验证:

- 包含 4 个部分，分别用 '.' 分割
- 每个部分的长度不超过 3 位
- 每部分只能包含数字
- 每部分的值在 [0,255] 之间
- 每部分不能包含前导零

IPV6 验证:

- 包含 8 个部分，分别用 ':' 分割
- 每部分的长度在 [1,4] 之间
- 每部分只能包含数字、小写英文字母( `'a'` 到 `'f'` )和大写英文字母( `'A'` 到 `'F'` )

```ts
function isIPV4(str: string): boolean {
  const arr = str.split('.');
  const n = arr.length;
  // 长度不为4
  if (n !== 4) return false;

  for (let i = 0; i < n; i++) {
    const item = arr[i];
    if (item === '') return false;
    // 前导零
    if (item.length > 1 && item[0] === '0') return false;
    // 是否有非数字字符
    for (let j = 0; j < item.length; j++) {
      const char = item[j];
      if (char < '0' || char > '9') return false;
    }
    // 超出范围
    if (Number(item) < 0 || Number(item) > 255) return false;
  }
  return true;
}

function isIPV6(str: string): boolean {
  const arr = str.split(':');
  const n = arr.length;
  if (n !== 8) return false;

  for (let i = 0; i < n; i++) {
    const item = arr[i];
    // 长度不正确
    if (item.length > 4 || item.length < 1) return false;
    // 字符是否符合16进制
    for (let j = 0; j < item.length; j++) {
      const char = item[j];
      if (char >= '0' && char <= '9') continue;
      if (char >= 'a' && char <= 'f') continue;
      if (char >= 'A' && char <= 'F') continue;
      return false;
    }
  }
  return true;
}

function validIPAddress(queryIP: string): string {
  if (queryIP.indexOf('.') !== -1) {
    return isIPV4(queryIP) ? 'IPv4' : 'Neither';
  } else {
    return isIPV6(queryIP) ? 'IPv6' : 'Neither';
  }
}
```
