/*
 * @lc app=leetcode.cn id=468 lang=typescript
 *
 * [468] 验证IP地址
 */

// @lc code=start

function isIPV4(str: string): boolean {
  const arr = str.split('.');
  const n = arr.length;
  // 长度不为4
  if (n !== 4) return false;

  for (let i = 0; i < n; i++) {
    const item = arr[i];
    if (item === '') return false;
    // 前导零
    if (item.length > 1 && item[0] === '0') return false;
    // 是否有非数字字符
    for (let j = 0; j < item.length; j++) {
      const char = item[j];
      if (char < '0' || char > '9') return false;
    }
    // 超出范围
    if (Number(item) < 0 || Number(item) > 255) return false;
  }
  return true;
}

function isIPV6(str: string): boolean {
  const arr = str.split(':');
  const n = arr.length;
  if (n !== 8) return false;

  for (let i = 0; i < n; i++) {
    const item = arr[i];
    // 长度不正确
    if (item.length > 4 || item.length < 1) return false;
    // 字符是否符合16进制
    for (let j = 0; j < item.length; j++) {
      const char = item[j];
      if (char >= '0' && char <= '9') continue;
      if (char >= 'a' && char <= 'f') continue;
      if (char >= 'A' && char <= 'F') continue;
      return false;
    }
  }
  return true;
}

function validIPAddress(queryIP: string): string {
  if (queryIP.indexOf('.') !== -1) {
    return isIPV4(queryIP) ? 'IPv4' : 'Neither';
  } else {
    return isIPV6(queryIP) ? 'IPv6' : 'Neither';
  }
}
// @lc code=end
