# 题目

给你一个整数数组 `nums` 以及两个整数 `lower` 和 `upper` 。求数组中，值位于范围 `[lower, upper]` （包含 `lower` 和 `upper）之内的` 区间和的个数 。

区间和 `S(i, j)` 表示在 `nums` 中，位置从 `i` 到 `j` 的元素之和，包含 `i` 和 `j` `(i ≤ j)`。

提示：

- 1 <= nums.length <= 105
- -231 <= nums[i] <= 231 - 1
- -105 <= lower <= upper <= 105
- 题目数据保证答案是一个 32 位 的整数

# 示例

```
输入：nums = [-2,5,-1], lower = -2, upper = 2
输出：3
解释：存在三个区间：[0,0]、[2,2] 和 [0,2] ，对应的区间和分别是：-2 、-1 、2 。
```

```
输入：nums = [0], lower = 0, upper = 0
输出：1
```

# 题解

## 分治

先计算`nums`的区间和，这样就可以方便的获取到每个区间的和。

采用分治思路，先结算左区间的数量，在计算右区间的数量。合并后继续计算。

```ts
function countRangeSum(nums: number[], lower: number, upper: number): number {
  const size = nums.length;

  const prefixSum = new Array(size + 1).fill(0);
  // 计算前缀和
  for (let i = 1; i <= size; i++) {
    prefixSum[i] = prefixSum[i - 1] + nums[i - 1];
  }
  // 分治排。排序过程中，先计算左边有多少区间和，在计算右边有多少区间和，合并后递归计算。
  const mergeSort = (
    sum: number[],
    left: number,
    right: number,
    lower: number,
    upper: number
  ): number => {
    if (left >= right) return 0;
    let result = 0;
    let mid = left + ((right - left) >> 1);
    result += mergeSort(sum, left, mid, lower, upper);
    result += mergeSort(sum, mid + 1, right, lower, upper);
    result += countParts(sum, left, mid, mid + 1, right, lower, upper);

    let temp = new Array(right - left + 1);
    let idx = 0;
    let l1 = left;
    let l2 = mid + 1;
    while (l1 <= mid || l2 <= right) {
      if (l2 > right || (l1 <= mid && sum[l1] <= sum[l2])) {
        temp[idx++] = sum[l1++];
      } else {
        temp[idx++] = sum[l2++];
      }
    }
    for (let i = 0; i < temp.length; i++) {
      sum[left + i] = temp[i];
    }
    return result;
  };

  const countParts = (
    sum: number[],
    l1: number,
    r1: number,
    l2: number,
    r2: number,
    lower: number,
    upper: number
  ): number => {
    let result = 0;
    let i = l1;
    let l = l2;
    let r = l2;
    // 计算区间和数量
    while (i <= r1) {
      while (l <= r2 && sum[l] - sum[i] < lower) l++;
      while (r <= r2 && sum[r] - sum[i] <= upper) r++;
      result += r - l;
      i++;
    }
    return result;
  };
  return mergeSort(prefixSum, 0, size, lower, upper);
}
```
