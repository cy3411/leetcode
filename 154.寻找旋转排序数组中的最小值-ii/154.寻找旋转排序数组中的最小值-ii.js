/*
 * @lc app=leetcode.cn id=154 lang=javascript
 *
 * [154] 寻找旋转排序数组中的最小值 II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMin = function (nums) {
  const size = nums.length;
  if (size === 1) return nums[0];

  let left = 0;
  let right = size - 1;

  while (left < right) {
    let mid = left + ((right - left) >> 1);
    // 恢复二分查找的二段性
    if (nums[left] === nums[mid] && nums[mid] === nums[right]) {
      left++;
      right--;
      continue;
    }

    if (nums[mid] <= nums[right]) {
      right = mid;
    } else {
      left = mid + 1;
    }
  }
  return nums[left];
};
// @lc code=end
