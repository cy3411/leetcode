# 题目

统计所有**小于**非负整数 `n` 的质数的数量。

# 示例

```
输入：n = 10
输出：4
解释：小于 10 的质数一共有 4 个, 它们是 2, 3, 5, 7 。
```

# 题解

## 埃氏筛法

如果一个数 x 为质数，那么 x 的倍数一定不为质数。

遍历 $[2,n)$ 范围，将每个数的倍数做标记。

最后通过标记来判断是否为质数。

```ts
function countPrimes(n: number): number {
  if (n === 0 || n === 1) return 0;
  const primes = new Array(n).fill(1);
  let ans = 0;
  for (let i = 2; i < n; i++) {
    // 如果已经计算过了，就跳过本次计算过去
    if (!primes[i]) continue;
    for (let j = i * i; j < n; j += i) {
      primes[j] = 0;
    }
    if (primes[i]) {
      ans++;
    }
  }
  return ans;
}
```

## 线性筛

```ts
function countPrimes(n: number): number {
  // 标记素数1，合数0
  const isPrimes = new Array(n).fill(1);
  // 记录每一个素数
  const primes = [];
  for (let i = 2; i < n; i++) {
    if (isPrimes[i]) primes.push(i);
    for (let j = 0; j < n && i * primes[j] < n; j++) {
      isPrimes[i * primes[j]] = 0;
      // 保证每个合数被标记一次
      if (i % primes[j] === 0) break;
    }
  }

  return primes.length;
}
```
