/*
 * @lc app=leetcode.cn id=1128 lang=javascript
 *
 * [1128] 等价多米诺骨牌对的数量
 */

// @lc code=start
/**
 * @param {number[][]} dominoes
 * @return {number}
 */
var numEquivDominoPairs = function (dominoes) {
  const size = dominoes.lengt;
  const memo = new Map();
  let result = 0;

  for (dominoe of dominoes) {
    let [x, y] = dominoe;
    if (x > y) {
      [x, y] = [y, x];
    }
    const val = x * 10 + y;
    result += memo.get(val) || 0;
    memo.set(val, (memo.get(val) || 0) + 1);
  }

  return result;
};
// @lc code=end
