# 题目

给你一个字符串 time ，格式为 hh:mm（小时：分钟），其中某几位数字被隐藏（用 ? 表示）。

有效的时间为 00:00 到 23:59 之间的所有时间，包括 00:00 和 23:59 。

替换 time 中隐藏的数字，返回你可以得到的最晚有效时间。

提示：

- time 的格式为 hh:mm
- 题目数据保证你可以由输入的字符串生成有效的时间

# 示例

```
输入：time = "2?:?0"
输出："23:50"
解释：以数字 '2' 开头的最晚一小时是 23 ，以 '0' 结尾的最晚一分钟是 50 。
```

# 题解

## 模拟

```ts
function maximumTime(time: string): string {
  const max = ['2', '3', '-1', '5', '9'];
  let ans = '';
  for (let i = 0; i < time.length; i++) {
    if (time[i] === ':') {
      ans += time[i];
      continue;
    }
    if (time[i] === '?') {
      if (i === 0 && time[1] !== '?' && time[1] >= '4') {
        // 个位超过4的，十位最大是1
        ans += '1';
      } else if (i === 1 && ans[0] !== '2') {
        // 十位是2的，个位最大是9
        ans += '9';
      } else {
        ans += max[i];
      }
    } else {
      ans += time[i];
    }
  }
  return ans;
}
```
