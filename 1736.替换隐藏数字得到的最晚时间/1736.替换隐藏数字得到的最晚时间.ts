/*
 * @lc app=leetcode.cn id=1736 lang=typescript
 *
 * [1736] 替换隐藏数字得到的最晚时间
 */

// @lc code=start
function maximumTime(time: string): string {
  const max = ['2', '3', '-1', '5', '9'];
  let ans = '';
  for (let i = 0; i < time.length; i++) {
    if (time[i] === ':') {
      ans += time[i];
      continue;
    }
    if (time[i] === '?') {
      if (i === 0 && time[1] !== '?' && time[1] >= '4') {
        // 个位超过4的，十位最大是1
        ans += '1';
      } else if (i === 1 && ans[0] !== '2') {
        // 十位是2的，个位最大是9
        ans += '9';
      } else {
        ans += max[i];
      }
    } else {
      ans += time[i];
    }
  }
  return ans;
}
// @lc code=end
