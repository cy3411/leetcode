# 题目

给你一个整数数组 `arr` ，请你删除最小 `5%` 的数字和最大 `5%` 的数字后，剩余数字的平均值。

与 **标准答案** 误差在 $10^{-5}$ 的结果都被视为正确结果。

提示：

- $20 \leq arr.length \leq 1000$
- `arr.length` 是 `20` 的 倍数
- $0 \leq arr[i] \leq 10^5$

# 示例

```
输入：arr = [1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3]
输出：2.00000
解释：删除数组中最大和最小的元素后，所有元素都等于 2，所以平均值为 2 。
```

```
输入：arr = [6,2,7,5,1,2,0,3,10,2,5,0,5,5,0,8,7,6,8,0]
输出：4.00000
```

# 题解

## 遍历

对数组做升序排序。

先计算 `%5` 的偏移量，然后遍历删除最小 5% 的数字和最大 5% 的数字后的结果，对遍历到的元素累加和。最后返回平均值。

```js
function trimMean(arr: number[]): number {
  // 排序
  arr.sort((a, b) => a - b);
  const n = arr.length;
  // 计算 5% 的偏移量
  const offset = (n * 5) / 100;
  const rest = n - 2 * offset;
  let ans = 0;
  // 遍历删除最小和最大5%数字后的数组
  for (let i = offset; i < n - offset; i++) {
    ans += arr[i];
  }
  // 返回平均值
  return ans / rest;
}
```

```cpp
class Solution {
public:
    double trimMean(vector<int> &arr) {
        sort(arr.begin(), arr.end());
        int n = arr.size();
        long offset = n * 5 / 100;

        long ans = 0;
        for (int i = offset; i < n - offset; i++) {
            ans += arr[i];
        }

        return ans / (n * 0.9);
    }
};
```
