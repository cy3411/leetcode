/*
 * @lc app=leetcode.cn id=1619 lang=cpp
 *
 * [1619] 删除某些元素后的数组均值
 */

// @lc code=start
class Solution {
public:
    double trimMean(vector<int> &arr) {
        sort(arr.begin(), arr.end());
        int n = arr.size();
        long offset = n * 5 / 100;

        long ans = 0;
        for (int i = offset; i < n - offset; i++) {
            ans += arr[i];
        }

        return ans / (n * 0.9);
    }
};
// @lc code=end
