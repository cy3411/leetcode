/*
 * @lc app=leetcode.cn id=1619 lang=typescript
 *
 * [1619] 删除某些元素后的数组均值
 */

// @lc code=start
function trimMean(arr: number[]): number {
  // 排序
  arr.sort((a, b) => a - b);
  const n = arr.length;
  // 计算 5% 的偏移量
  const offset = (n * 5) / 100;
  const rest = n - 2 * offset;
  let ans = 0;
  // 遍历删除最小和最大5%数字后的数组
  for (let i = offset; i < n - offset; i++) {
    ans += arr[i];
  }
  // 返回平均值
  return ans / rest;
}
// @lc code=end
