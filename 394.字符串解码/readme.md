# 题目
给定一个经过编码的字符串，返回它解码后的字符串。

编码规则为: `k[encoded_string]`，表示其中方括号内部的 encoded_string 正好重复 k 次。注意 k 保证为正整数。

你可以认为输入字符串总是有效的；输入字符串中没有额外的空格，且输入的方括号总是符合格式要求的。

此外，你可以认为原始数据不包含数字，所有的数字只表示重复的次数 *k* ，例如不会出现像 `3a` 或 `2[4]` 的输入。


# 示例
```
s = "3[a]2[bc]", 返回 "aaabcbc".
s = "3[a2[c]]", 返回 "accaccacc".
s = "2[abc]3[cd]ef", 返回 "abcabccdcdcdef".
```

# 题解
## 递归
观察题目，其实每次都是要处理方括号内的字符串即可。

我们解决最简单的一层嵌套，然后判断是否中间还有中括号，递归处理剩下的字符串即可。

```ts
var decodeString = function (s: string): string {
  let result = '';

  let i = 0;
  while (i < s.length) {
    if (/[a-zA-Z]/.test(s[i])) {
      result += s[i++];
    } else {
      let sum = 0;
      while (/\d/.test(s[i])) {
        sum = sum * 10 + Number(s[i]);
        i++;
      }
      i++;
      let l = i;
      let r = i;
      let cnt = 1;
      // 找到方括号的左右下标
      while (cnt) {
        r++;
        if (s[r] === '[') cnt++;
        else if (s[r] === ']') cnt--;
      }
      // 递归处理方括号内的字符串
      let temp = decodeString(s.substring(l, r));
      while (sum--) result += temp;
      i = r + 1;
    }
  }
  return result;
};
```