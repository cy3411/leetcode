# 题目

给出一个字符串 `s`（仅含有小写英文字母和括号）。

请你按照从括号内到外的顺序，逐层反转每对匹配括号中的字符串，并返回最终的结果。

注意，您的结果中 **不应** 包含任何括号。

提示：

- 0 <= s.length <= 2000
- s 中只有小写英文字母和括号
- 我们确保所有括号都是成对出现的

# 示例

```
输入：s = "(abcd)"
输出："dcba"
```

```
输入：s = "(u(love)i)"
输出："iloveu"
```

# 题解

## 栈

括号匹配问题，可以用栈来解决。

我们遍历字符串，定义`str`来记录小写字符串。对于遍历到的字符：

- 如果是`(`，我们将`str`入栈，并将`str`重置为空。
- 如果是`)`，我们将`str`反转后，将栈顶元素和它拼接上。
- 如果是小写字符，直接凭借到`str`后面。

```ts
function reverseParentheses(s: string): string {
  // 暂存外层括号的字符串
  const stack = [];
  let str = '';
  for (const char of s) {
    if (char === '(') {
      stack.push(str);
      str = '';
    } else if (char === ')') {
      // 最内层字符串反转后，和栈顶字符串拼接
      str = str.split('').reverse().join('');
      str = stack.pop() + str;
    } else {
      str += char;
    }
  }

  return str;
}
```

## 预处理括号位置

遍历字符串，将左右括号下标互换存入数组中。这里做预处理。

遍历字符串，遇到括号就将游标跳到对应下标位置，开始反向扫描。

- 如果是字符串，直接存入结果
- 如果是括号，继续将游标跳到对应下标位置，继续反向扫描

```ts
function reverseParentheses(s: string): string {
  const size = s.length;
  const stack = [];
  const brackers = new Array(size).fill(0);
  // 记录每对括号的下标，将左右括号下标交换
  for (let i = 0; i < size; i++) {
    if (s[i] === '(') {
      stack.push(i);
    } else if (s[i] === ')') {
      let j = stack.pop();
      brackers[i] = j;
      brackers[j] = i;
    }
  }

  const strs = [];
  let idx = 0;
  let step = 1;
  // 遍历的时候出现括号，找到另一个对应括号的位置，然后反向遍历即可
  while (idx < size) {
    if (s[idx] === '(' || s[idx] === ')') {
      idx = brackers[idx];
      step = -step;
    } else {
      strs.push(s[idx]);
    }
    idx += step;
  }

  return strs.join('');
}
```
