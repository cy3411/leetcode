# 题目

给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。

如果数组中不存在目标值 `target，返回` `[-1, -1]`。

进阶：

你可以设计并实现时间复杂度为 `O(log n)` 的算法解决此问题吗？

# 示例

```
输入：nums = [5,7,7,8,8,10], target = 8
输出：[3,4]
```

# 题解

## 二分查找

在一个有序的数组中，找到第一个大于等于 target 的位置。

有序，并且有条件，可以考虑二分查找。

题目需要的是一个区间，找到第一个大于等于 target 的位置后，在找第一个大于等于 target+1 的位置。这样就可以找到一个符合题意的区间。

再做一下边界的处理就可以了。

```ts
function searchRange(nums: number[], target: number): number[] {
  const binarySearch = (nums: number[], target: number): number => {
    let head = 0;
    let tail = nums.length - 1;

    while (head <= tail) {
      let mid = ((tail - head) >> 1) + head;
      if (nums[mid] < target) head = mid + 1;
      else tail = mid - 1;
    }

    return head;
  };

  const result = new Array(2);
  // 先找第一个大于等于targt的元素
  let i = binarySearch(nums, target);
  // 验证元素的合法性
  if (i === nums.length || nums[i] !== target) return [-1, -1];
  // 找第一个大于等于target+1的元素
  // 如果只有一个元素的话，需要将边界合法
  let j = Math.max(0, binarySearch(nums, target + 1) - 1);

  return [i, j];
}
```
