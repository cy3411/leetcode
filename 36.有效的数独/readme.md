# 题目

请你判断一个 `9x9` 的数独是否有效。只需要 根据以下规则 ，验证已经填入的数字是否有效即可。

- 数字 `1-9` 在每一行只能出现一次。
- 数字 `1-9` 在每一列只能出现一次。
- 数字 `1-9` 在每一个以粗实线分隔的 `3x3` 宫内只能出现一次。（请参考示例图）

数独部分空格内已填入了数字，空白格用 `'.'` 表示。

注意：

- 一个有效的数独（部分已被填充）不一定是可解的。
- 只需要根据以上规则，验证已经填入的数字是否有效即可。

提示：

- `board.length == 9`
- `board[i].length == 9`
- `board[i][j]` 是一位数字或者 `'.'`

# 示例

[![4MNeKg.png](https://z3.ax1x.com/2021/09/17/4MNeKg.png)](https://imgtu.com/i/4MNeKg)

```输入：board =
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：true
```

# 题解

## 哈希表

遍历每个元素，如果当前元素出现在行、列和宫格的哈希表中，表示这个数独是无效的。

由于题目只有`1-9`的数字，题解中使用 `9` 位二进制来表示是否访问过。

```ts
function isValidSudoku(board: string[][]): boolean {
  const m = board.length;
  const n = board[0].length;
  // 行
  const X: Map<number, number> = new Map();
  // 列
  const Y: Map<number, number> = new Map();
  // 3*3宫格
  const Z: Map<number, number> = new Map();

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (board[i][j] === '.') continue;
      const num = Number(board[i][j]);
      // 计算当前元素在哪个3*3宫格内
      const cell = ((i / 3) >> 0) * 3 + ((j / 3) >> 0);
      let x = X.get(i) || 0;
      let y = Y.get(j) || 0;
      let z = Z.get(cell) || 0;
      // 同行、列、3*3宫格内出现重复数字
      if (x & (1 << num)) return false;
      if (y & (1 << num)) return false;
      if (z & (1 << num)) return false;
      // 将当前元素放入到哈希中保存
      X.set(i, x | (1 << num));
      Y.set(j, y | (1 << num));
      Z.set(cell, z | (1 << num));
    }
  }

  return true;
}
```
