/*
 * @lc app=leetcode.cn id=1608 lang=typescript
 *
 * [1608] 特殊数组的特征值
 */

// @lc code=start
function specialArray(nums: number[]): number {
  const n = nums.length;
  nums.sort((a: number, b: number): number => b - a);

  for (let i = 1; i <= n; i++) {
    if (nums[i - 1] >= i && (i === n || nums[i] < i)) return i;
  }
  return -1;
}
// @lc code=end
