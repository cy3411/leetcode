# 题目

给你一棵二叉搜索树，请你返回一棵 平衡后 的二叉搜索树，新生成的树应该与原来的树有着相同的节点值。

如果一棵二叉搜索树中，每个节点的两棵子树高度差不超过 1 ，我们就称这棵二叉搜索树是 平衡的 。

如果有多种构造方法，请你返回任意一种。

提示：

- 树节点的数目在 1 到 10^4 之间。
- 树节点的值互不相同，且在 1 到 10^5 之间。

# 示例

[![f3o16U.png](https://z3.ax1x.com/2021/08/09/f3o16U.png)](https://imgtu.com/i/f3o16U)

```
输入：root = [1,null,2,null,3,null,4,null,null]
输出：[2,1,3,null,null,null,4]
解释：这不是唯一的正确答案，[3,1,4,null,2,null,null] 也是一个可行的构造方案。
```

# 题解

## 中序遍历

二叉搜索树的中序遍历是一个升序序列，我们可以先得到这个序列，再去创建平衡二叉搜索树。

想要二叉搜索树平衡，那么左右子树数量要接近。我们可以让序列的中间值做根节点，中间值左边的节点去建左树，中间值右边的节点去建右树。

```ts
function inorder(root: TreeNode | null, seq: TreeNode[]) {
  if (root === null) return;
  inorder(root.left, seq);
  seq.push(root);
  inorder(root.right, seq);
  return seq;
}

function build(seq: TreeNode[], l: number, r: number): TreeNode | null {
  if (l > r) return null;
  // 每次使用中间节点来当作根节点
  let mid = (l + r) >> 1;
  const root = seq[mid];
  // 遍历这个过程
  root.left = build(seq, l, mid - 1);
  root.right = build(seq, mid + 1, r);
  return root;
}

function balanceBST(root: TreeNode | null): TreeNode | null {
  // 获得中序遍历序列
  const inorderSeq = inorder(root, []);

  return build(inorderSeq, 0, inorderSeq.length - 1);
}
```
