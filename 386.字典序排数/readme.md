# 题目

给你一个整数 `n` ，按字典序返回范围 `[1, n]` 内所有整数。

你必须设计一个时间复杂度为 `O(n)` 且使用 `O(1)` 额外空间的算法。

提示：

- $1 \leq n \leq 5 * 10^4$

# 示例

```
输入：n = 13
输出：[1,10,11,12,13,2,3,4,5,6,7,8,9]
```

```
输入：n = 2
输出：[1,2]
```

# 题解

## 排序

生成 n 位数字的数组，然后按照字典序排序。

```ts
function lexicalOrder(n: number): number[] {
  const ans: number[] = new Array(n).fill(0).map((_, i) => i + 1);
  ans.sort((a, b) => {
    let aStr = a.toString();
    let bStr = b.toString();
    return aStr.localeCompare(bStr);
  });

  return ans;
}
```

## 递归

递归求解，每次递归按照字典序增加一位数字，直到超过 n 为止。

```cpp
class Solution
{
public:
    void dfs(int i, int n, vector<int> &ans)
    {
        if (i > n)
        {
            return;
        }
        ans.push_back(i);
        for (int j = 0; j < 10; j++)
        {
            dfs(i * 10 + j, n, ans);
        }
    }

    vector<int> lexicalOrder(int n)
    {
        vector<int> ans;
        for (int i = 1; i <= 9; i++)
        {
            dfs(i, n, ans);
        }

        return ans;
    }
};
```
