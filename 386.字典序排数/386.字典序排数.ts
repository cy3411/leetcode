/*
 * @lc app=leetcode.cn id=386 lang=typescript
 *
 * [386] 字典序排数
 */

// @lc code=start
function lexicalOrder(n: number): number[] {
  const ans: number[] = new Array(n).fill(0).map((_, i) => i + 1);
  ans.sort((a, b) => {
    let aStr = a.toString();
    let bStr = b.toString();
    return aStr.localeCompare(bStr);
  });

  return ans;
}
// @lc code=end
