/*
 * @lc app=leetcode.cn id=1791 lang=typescript
 *
 * [1791] 找出星型图的中心节点
 */

// @lc code=start
function findCenter(edges: number[][]): number {
  const n = edges.length;
  const map = new Map<number, number[]>();
  for (const [u, v] of edges) {
    if (!map.has(u)) {
      map.set(u, []);
    }
    map.get(u).push(v);
    if (!map.has(v)) {
      map.set(v, []);
    }
    map.get(v).push(u);
  }

  let ans = -1;
  for (const [u, vs] of map.entries()) {
    if (vs.length === n) ans = u;
  }

  return ans;
}
// @lc code=end
