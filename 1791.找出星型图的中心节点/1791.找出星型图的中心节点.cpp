/*
 * @lc app=leetcode.cn id=1791 lang=cpp
 *
 * [1791] 找出星型图的中心节点
 */

// @lc code=start
class Solution
{
public:
    int findCenter(vector<vector<int>> &edges)
    {
        int n = edges.size() + 1;
        vector<int> degree(n + 1, 0);
        for (auto &e : edges)
        {
            degree[e[0]]++;
            degree[e[1]]++;
        }

        int ans = -1;
        for (int i = 1; i <= n; i++)
        {
            if (degree[i] == n - 1)
            {
                ans = i;
                break;
            }
        }
        return ans;
    }
};
// @lc code=end
