/*
 * @lc app=leetcode.cn id=1791 lang=c
 *
 * [1791] 找出星型图的中心节点
 */

// @lc code=start

int findCenter(int **edges, int edgesSize, int *edgesColSize)
{
    int n = edgesSize + 1;
    int *degrees = (int *)malloc(sizeof(int) * (n + 1));
    memset(degrees, 0, sizeof(int) * (n + 1));

    for (int i = 0; i < edgesSize; i++)
    {
        degrees[edges[i][0]]++;
        degrees[edges[i][1]]++;
    }

    for (int i = 1;; i++)
    {
        if (degrees[i] == n - 1)
        {
            free(degrees);
            return i;
        }
    }
}
// @lc code=end
