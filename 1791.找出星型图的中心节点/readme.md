# 题目

有一个无向的 **星型** 图，由 `n` 个编号从 `1` 到 `n` 的节点组成。星型图有一个 **中心** 节点，并且恰有 `n - 1` 条边将中心节点与其他每个节点连接起来。

给你一个二维整数数组 `edges` ，其中 $edges[i] = [u_i, v_i]$ 表示在节点 $u_i$ 和 $v_i$ 之间存在一条边。请你找出并返回 `edges` 所表示星型图的中心节点。

提示：

- $3 \leq n \leq 10^5$
- $edges.length \equiv n - 1$
- $edges[i].length \equiv 2$
- $1 \leq u_i, v_i \leq n$
- $u_i \not = v_i$
- 题目数据给出的 `edges` 表示一个有效的星型图

# 示例

[![H7n6wn.png](https://s4.ax1x.com/2022/02/18/H7n6wn.png)](https://imgtu.com/i/H7n6wn)

```
输入：edges = [[1,2],[2,3],[4,2]]
输出：2
解释：如上图所示，节点 2 与其他每个节点都相连，所以节点 2 是中心节点。
```

# 题解

## 图

按照题目中的描述，我们可以把 `edges` 转换成一个无向图，答案就是能引出最多边的节点。

```ts
function findCenter(edges: number[][]): number {
  const n = edges.length;
  const map = new Map<number, number[]>();
  for (const [u, v] of edges) {
    if (!map.has(u)) {
      map.set(u, []);
    }
    map.get(u).push(v);
    if (!map.has(v)) {
      map.set(v, []);
    }
    map.get(v).push(u);
  }

  let ans = -1;
  for (const [u, vs] of map.entries()) {
    if (vs.length === n) ans = u;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int findCenter(vector<vector<int>> &edges)
    {
        int n = edges.size() + 1;
        vector<int> degree(n + 1, 0);
        for (auto &e : edges)
        {
            degree[e[0]]++;
            degree[e[1]]++;
        }

        int ans = -1;
        for (int i = 1; i <= n; i++)
        {
            if (degree[i] == n - 1)
            {
                ans = i;
                break;
            }
        }
        return ans;
    }
};
```

```cpp
int findCenter(int **edges, int edgesSize, int *edgesColSize)
{
    int n = edgesSize + 1;
    int *degrees = (int *)malloc(sizeof(int) * (n + 1));
    memset(degrees, 0, sizeof(int) * (n + 1));

    for (int i = 0; i < edgesSize; i++)
    {
        degrees[edges[i][0]]++;
        degrees[edges[i][1]]++;
    }

    for (int i = 1;; i++)
    {
        if (degrees[i] == n - 1)
        {
            free(degrees);
            return i;
        }
    }
}
```
