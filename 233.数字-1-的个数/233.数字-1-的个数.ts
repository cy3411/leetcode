/*
 * @lc app=leetcode.cn id=233 lang=typescript
 *
 * [233] 数字 1 的个数
 */

// @lc code=start
function countDigitOne(n: number): number {
  let k = 1;
  let ans = 0;
  while (k <= n) {
    ans += ((n / (k * 10)) >> 0) * k + Math.min(Math.max((n % (k * 10)) - k + 1, 0), k);
    k *= 10;
  }
  return ans;
}
// @lc code=end
