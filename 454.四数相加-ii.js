/*
 * @lc app=leetcode.cn id=454 lang=javascript
 *
 * [454] 四数相加 II
 */

// @lc code=start
/**
 * @param {number[]} A
 * @param {number[]} B
 * @param {number[]} C
 * @param {number[]} D
 * @return {number}
 */
var fourSumCount = function (A, B, C, D) {
  const hash = new Map();

  for (let i = 0; i < A.length; i++) {
    for (let j = 0; j < B.length; j++) {
      let key = -(A[i] + B[j]);
      hash.set(key, hash.get(key) ? hash.get(key) + 1 : 1);
    }
  }

  let result = 0;
  for (let i = 0; i < C.length; i++) {
    for (let j = 0; j < D.length; j++) {
      let key = C[i] + D[j];
      if (hash.has(key)) {
        result += hash.get(key);
      }
    }
  }

  return result;
};
// @lc code=end
