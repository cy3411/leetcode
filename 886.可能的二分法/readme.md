# 题目

给定一组 n 人（编号为 1, 2, ..., n）， 我们想把每个人分进任意大小的两组。每个人都可能不喜欢其他人，那么他们不应该属于同一组。

给定整数 n 和数组 dislikes ，其中 $dislikes[i] = [a_i, b_i]$ ，表示不允许将编号为 $a_i$ 和 $b_i$ 的人归入同一组。当可以用这种方法将所有人分进两组时，返回 true；否则返回 false。

提示：

- $1 \leq n \leq 2000$
- $0 \leq dislikes.length \leq 104$
- $dislikes[i].length \equiv 2$
- $1 \leq dislikes[i][j] \leq n$
- $a_i < b_i$
- `dislikes` 中每一组都 不同

# 示例

```
输入：n = 4, dislikes = [[1,2],[1,3],[2,4]]
输出：true
解释：group1 [1,4], group2 [2,3]
```

```
输入：n = 3, dislikes = [[1,2],[1,3],[2,3]]
输出：false
```

```
输入：n = 5, dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]]
输出：false
```

# 题解

## 标记法

遍历数组，如果当前人没有分组，将其标记为第一组，那么这个人不喜欢的人标记为第二组，如果遍历过程中遇到冲突，那么表示无法分为两组。

```js
function possibleBipartition(n: number, dislikes: number[][]): boolean {
  // 值为0表示未标记，1和2分别为不同的组
  const colors: number[] = new Array(n + 1).fill(0);
  // 当前索引保存的是不喜欢的人列表
  const graph: number[][] = new Array(n + 1).fill(0).map((_) => []);
  // 建模
  for (const [first, second] of dislikes) {
    graph[first].push(second);
    graph[second].push(first);
  }

  const dfs = (
    currNode: number,
    currColor: number,
    colors: number[],
    graph: number[][]
  ): boolean => {
    // 标记
    colors[currNode] = currColor;
    for (const nextNode of graph[currNode]) {
      // 下一个节点标记过，但是和当前节点颜色相同，有冲突
      if (colors[nextNode] !== 0 && colors[nextNode] === colors[currNode]) {
        return false;
      }
      // 下一个节点未标记，开始标记为另一组
      if (colors[nextNode] === 0 && !dfs(nextNode, 3 ^ currColor, colors, graph)) {
        return false;
      }
    }
    return true;
  };

  for (let i = 1; i <= n; i++) {
    // 当前节点标记过，跳过
    if (colors[i] !== 0) continue;
    if (!dfs(i, 1, colors, graph)) return false;
  }

  return true;
}
```
