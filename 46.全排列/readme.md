# 题目

给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。

提示：

- 1 <= nums.length <= 6
- -10 <= nums[i] <= 10
- nums 中的所有整数 互不相同

# 示例

```
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
```

# 题解

## 回溯

穷举所有组合，递归索引位置，尝试将当前索引与数组中其它位置交换，递归到边界条件时候更新结果。

回溯回去的时候，将交换的位置还原，然后尝试下一次选择。

```ts
function permute(nums: number[]): number[][] {
  const ans: number[][] = [];
  const backtrack = (idx: number) => {
    if (idx === nums.length) {
      // 因为数组是引用类型，所以要做一次copy
      ans.push([...nums]);
      return;
    }
    for (let i = idx; i < nums.length; i++) {
      // 选择交换位置
      [nums[i], nums[idx]] = [nums[idx], nums[i]];
      // 递归下一个位置
      backtrack(idx + 1);
      // 回溯
      [nums[i], nums[idx]] = [nums[idx], nums[i]];
    }
  };

  backtrack(0);

  return ans;
}
```
