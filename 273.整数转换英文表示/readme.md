# 题目

将非负整数 `num` 转换为其对应的英文表示。

提示：

- $0 <= num <= 2^{31} - 1$

# 示例

```
输入：num = 123
输出："One Hundred Twenty Three"
```

```
输入：num = 1234567891
输出："One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One"
```

# 题解

## 递归

西方数字的划分，是从低位到高位，每 3 位一组，依次是千`Thousand`，百万`Million`，十亿`Billion`。

比如：`1234567890`，划分完了就是 `1 Billion 234 Million 567 Thousand 890`。我们主要就是将 `3` 位的数字转换成字母。

- 小于 20 的数字可以直接用字母表示
- 大于 20 且小于 100 的数，将十位转换成字母，然后递归的将个位转换成字符
- 剩下的首先将百位转换成字母，然后递归的将十位、个位分别转换成字母。

题目给出最大的数字是$2^{31}-1$，最多只有 `10` 位。所以我们最多只要将数字分成 `4` 组，从高到低去处理即可。

```ts
function numberToWords(num: number): string {
  if (num === 0) return 'Zero';

  const singles = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
  const teens = [
    'Ten',
    'Eleven',
    'Twelve',
    'Thirteen',
    'Fourteen',
    'Fifteen',
    'Sixteen',
    'Seventeen',
    'Eighteen',
    'Nineteen',
  ];
  const tens = [
    '',
    'Ten',
    'Twenty',
    'Thirty',
    'Forty',
    'Fifty',
    'Sixty',
    'Seventy',
    'Eighty',
    'Ninety',
  ];
  const thousands = ['', 'Thousand', 'Million', 'Billion'];

  const recursion = (curr: string[], num: number) => {
    if (num === 0) {
      return;
    } else if (num < 10) {
      curr.push(`${singles[num]}`);
    } else if (num < 20) {
      curr.push(`${teens[num - 10]}`);
    } else if (num < 100) {
      curr.push(`${tens[(num / 10) >> 0]}`);
      recursion(curr, num % 10);
    } else {
      curr.push(`${singles[(num / 100) >> 0]} Hundred`);
      recursion(curr, num % 100);
    }
  };

  let unit = 10 ** 9;
  const ans: string[] = [];
  // 从高位开始，3位数字一组开始转换
  for (let i = 3; i >= 0; i--, unit /= 10 ** 3) {
    const currNum = (num / unit) >> 0;
    if (currNum !== 0) {
      num -= currNum * unit;
      const curr: string[] = [];
      recursion(curr, currNum);
      curr.push(`${thousands[i]}`);
      ans.push(...curr);
    }
  }

  return ans.join(' ').trim();
}
```
