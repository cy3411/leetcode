/*
 * @lc app=leetcode.cn id=1823 lang=typescript
 *
 * [1823] 找出游戏的获胜者
 */

// @lc code=start

function findTheWinner(n: number, k: number): number {
  const queue = new Array(n);
  for (let i = 1; i <= n; i++) {
    queue[i - 1] = i;
  }

  while (queue.length > 1) {
    // 取出 k - 1 个元素重新入队列
    for (let i = 1; i < k; i++) {
      const a = queue.shift();
      queue.push(a);
    }
    queue.shift();
  }

  return queue[0];
}
// @lc code=end
