# 题目

共有 `n` 名小伙伴一起做游戏。小伙伴们围成一圈，按 **顺时针顺序** 从 `1` 到 `n` 编号。确切地说，从第 `i` 名小伙伴顺时针移动一位会到达第 `(i+1)` 名小伙伴的位置，其中 $1 \leq i < n$ ，从第 `n` 名小伙伴顺时针移动一位会回到第 `1` 名小伙伴的位置。

游戏遵循如下规则：

- 从第 `1` 名小伙伴所在位置 **开始** 。
- 沿着顺时针方向数 `k` 名小伙伴，计数时需要 **包含** 起始时的那位小伙伴。逐个绕圈进行计数，一些小伙伴可能会被数过不止一次。
- 你数到的最后一名小伙伴需要离开圈子，并视作输掉游戏。
- 如果圈子中仍然有不止一名小伙伴，从刚刚输掉的小伙伴的 顺时针下一位 **小伙伴** 开始，回到步骤 2 继续执行。
- 否则，圈子中最后一名小伙伴赢得游戏。

给你参与游戏的小伙伴总数 n ，和一个整数 k ，返回游戏的获胜者。

提示：

- $1 \leq k \leq n \leq 500$

# 示例

[![OZFAKS.png](https://s1.ax1x.com/2022/05/04/OZFAKS.png)](https://imgtu.com/i/OZFAKS)

```
输入：n = 5, k = 2
输出：3
解释：游戏运行步骤如下：
1) 从小伙伴 1 开始。
2) 顺时针数 2 名小伙伴，也就是小伙伴 1 和 2 。
3) 小伙伴 2 离开圈子。下一次从小伙伴 3 开始。
4) 顺时针数 2 名小伙伴，也就是小伙伴 3 和 4 。
5) 小伙伴 4 离开圈子。下一次从小伙伴 5 开始。
6) 顺时针数 2 名小伙伴，也就是小伙伴 5 和 1 。
7) 小伙伴 1 离开圈子。下一次从小伙伴 3 开始。
8) 顺时针数 2 名小伙伴，也就是小伙伴 3 和 5 。
9) 小伙伴 5 离开圈子。只剩下小伙伴 3 。所以小伙伴 3 是游戏的获胜者。
```

```
输入：n = 6, k = 5
输出：1
解释：小伙伴离开圈子的顺序：5、4、6、2、3 。小伙伴 1 是游戏的获胜者。
```

# 题解

## 队列

约瑟夫环问题，可以用队列来解决。

将所有的小伙伴放入队列中，每次从队列中取出 `k - 1` 个小伙伴后，重新将其入队，将第 `k` 个小伙伴出队，重复上面的操作，知道队列中只剩下一个小伙伴。

```ts
function findTheWinner(n: number, k: number): number {
  const queue = new Array(n);
  for (let i = 1; i <= n; i++) {
    queue[i - 1] = i;
  }

  while (queue.length > 1) {
    // 取出 k - 1 个元素重新入队列
    for (let i = 1; i < k; i++) {
      const a = queue.shift();
      queue.push(a);
    }
    queue.shift();
  }

  return queue[0];
}
```

```cpp
class Solution
{
public:
    int findTheWinner(int n, int k)
    {
        queue<int> q;
        for (int i = 1; i <= n; i++)
        {
            q.push(i);
        }
        while (q.size() > 1)
        {
            for (int i = 1; i < k; i++)
            {
                q.push(q.front());
                q.pop();
            }
            q.pop();
        }
        return q.front();
    }
};
```

```py
class Solution:
    def findTheWinner(self, n: int, k: int) -> int:
        q = deque(range(1, n + 1))
        while (len(q) > 1):
            for _ in range(k-1):
                q.append(q.popleft())
            q.popleft()
        return q[0]
```
