/*
 * @lc app=leetcode.cn id=1200 lang=cpp
 *
 * [1200] 最小绝对差
 */

// @lc code=start
class Solution {
public:
    vector<vector<int>> minimumAbsDifference(vector<int> &arr) {
        sort(arr.begin(), arr.end());

        int min = INT_MAX, n = arr.size();
        vector<vector<int>> ans;
        for (int i = 1; i < n; i++) {
            int diff = arr[i] - arr[i - 1];
            if (diff < min) {
                min = diff;
                ans.clear();
            }
            if (diff == min) {
                ans.push_back({arr[i - 1], arr[i]});
            }
        }
        return ans;
    }
};
// @lc code=end
