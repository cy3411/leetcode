/*
 * @lc app=leetcode.cn id=1200 lang=typescript
 *
 * [1200] 最小绝对差
 */

// @lc code=start
function minimumAbsDifference(arr: number[]): number[][] {
  // 升序
  arr.sort((a, b) => a - b);
  // 最小绝对差
  let min = Infinity;
  const ans: number[][] = [];

  for (let i = 1; i < arr.length; i++) {
    let diff = Math.abs(arr[i] - arr[i - 1]);
    // 更新最小绝对差
    if (diff < min) {
      min = diff;
      ans.length = 0;
    }
    // 更新最小绝对差结果
    if (diff === min) {
      ans.push([arr[i - 1], arr[i]]);
    }
  }

  return ans;
}
// @lc code=end
