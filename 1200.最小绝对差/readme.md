# 题目

给你个整数数组 `arr`，其中每个元素都 **不相同**。

请你找到所有具有最小绝对差的元素对，并且按升序的顺序返回。

提示：

- $2 \leq arr.length \leq 10^5$
- $-10^6 \leq arr[i] \leq 10^6$

# 示例

```
输入：arr = [4,2,1,3]
输出：[[1,2],[2,3],[3,4]]
```

```
输入：arr = [1,3,6,10,15]
输出：[[1,3]]
```

# 题解

## 排序

升序排序后遍历，每次找到下一个元素的最小绝对差，更新结果。

```ts
function minimumAbsDifference(arr: number[]): number[][] {
  // 升序
  arr.sort((a, b) => a - b);
  // 最小绝对差
  let min = Infinity;
  const ans: number[][] = [];

  for (let i = 1; i < arr.length; i++) {
    let diff = Math.abs(arr[i] - arr[i - 1]);
    // 更新最小绝对差
    if (diff < min) {
      min = diff;
      ans.length = 0;
    }
    // 更新最小绝对差结果
    if (diff === min) {
      ans.push([arr[i - 1], arr[i]]);
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<vector<int>> minimumAbsDifference(vector<int> &arr) {
        sort(arr.begin(), arr.end());

        int min = INT_MAX, n = arr.size();
        vector<vector<int>> ans;
        for (int i = 1; i < n; i++) {
            int diff = arr[i] - arr[i - 1];
            if (diff < min) {
                min = diff;
                ans.clear();
            }
            if (diff == min) {
                ans.push_back({arr[i - 1], arr[i]});
            }
        }
        return ans;
    }
};
```
