# 题目
给你一个由 不同 整数组成的数组 `nums` ，和一个目标整数 `target` 。请你从 `nums` 中找出并返回总和为 `target` 的元素组合的个数。

题目数据保证答案符合 32 位整数范围。

提示：
+ 1 <= nums.length <= 200
+ 1 <= nums[i] <= 1000
+ nums 中的所有元素 互不相同
+ 1 <= target <= 1000

# 示例
```
输入：nums = [1,2,3], target = 4
输出：7
解释：
所有可能的组合为：
(1, 1, 1, 1)
(1, 1, 2)
(1, 2, 1)
(1, 3)
(2, 1, 1)
(2, 2)
(3, 1)
请注意，顺序不同的序列被视作不同的组合。
```

# 题解
定义`f(n)`表示从`nums`中挑选数字可以构成`n`的方法总数。

遍历`nums`,判断如果构成`target`，选择`nums[i]`，剩余的`target-nums[i]`数再数组中选择的话需要，会有多少中方法。

题目给出的都是正整数，递归的终止条件自然就是:
+ `target < 0`,无法从数组中挑选数组组成，返回`0`
+ `target = 0`,数组中有一个数等于`target`，返回`1`

## 递归
```js
function combinationSum4(nums: number[], target: number): number {
  let memo = new Map();
  const dp = (nums: number[], target: number): number => {
    if (memo.has(target)) return memo.get(target);
    if (target < 0) return 0;
    if (target === 0) return 1;

    let res = 0;

    for (let num of nums) {
      res += dp(nums, target - num);
    }

    memo.set(target, res);

    return memo.get(target);
  };

  return dp(nums, target);
}
```

## 动态规划
```js
function combinationSum4(nums: number[], target: number): number {
  const size = nums.length;
  const dp = new Array(target + 1).fill(0);
  dp[0] = 1;

  for (let i = 1; i <= target; i++) {
    for (let j = 0; j < size; j++) {
      if (i >= nums[j]) {
        dp[i] += dp[i - nums[j]];
      }
    }
  }
  return dp[target];
}
```