# 题目

存在一个由 n 个节点组成的无向连通图，图中的节点按从 0 到 n - 1 编号。

给你一个数组 graph 表示这个图。其中，graph[i] 是一个列表，由所有与节点 i 直接相连的节点组成。

返回能够访问所有节点的最短路径的长度。你可以在任一节点开始和停止，也可以多次重访节点，并且可以重用边。

提示：

- n == graph.length
- 1 <= n <= 12
- 0 <= graph[i].length < n
- graph[i] 不包含 i
- 如果 graph[a] 包含 b ，那么 graph[b] 也包含 a
- 输入的图总是连通图

# 示例

[![fnLhVI.png](https://z3.ax1x.com/2021/08/06/fnLhVI.png)](https://imgtu.com/i/fnLhVI)

```
输入：graph = [[1,2,3],[0],[0],[0]]
输出：4
解释：一种可能的路径为 [1,0,2,0,3]
```

# 题解

## 广度优先搜索

题目要我们求出访问所有节点**最短路径**的长度，每条边的长度固定为**1**，因此我们可以使用广度优先搜索来求解。

最短路径的前提是**访问了所有节点**，所以节点的状态还要保存每一个节点的是否访问过的状态。

使用三元组来保存当前节点的扩展状态：

- u 表示当前节点编号
- mask 是一个长度为 n 的二进制数，表示每个节点是否经过。mask 所以位数都是 1 的时候，表示访问了所有节点。
- dist 表示访问过的路径长度

```ts
function shortestPathLength(graph: number[][]): number {
  const m = graph.length;
  // 队列，保存格式[节点，mask，dist]
  // mask是二进制格式，每位表示一个节点，值为1的话表示访问过了
  const queue: [number, number, number][] = [];
  // 保存当前节点下mask状态是否被访问过
  const visited = new Array(m).fill(0).map((_) => new Array(1 << m).fill(0));
  // 初始化队列数据
  for (let i = 0; i < m; i++) {
    let mask = 1 << i;
    queue.push([i, mask, 0]);
    visited[i][mask] = 1;
  }

  // bfs
  while (queue.length) {
    const [u, mask, dist] = queue.shift();
    // 如果mask位数都是1的话，表示u开始的位置，已经访问完所有的节点。返回dist
    if (mask === (1 << m) - 1) {
      return dist;
    }
    // 扩展当前节点的状态
    for (const v of graph[u]) {
      const next = mask | (1 << v);
      if (!visited[v][next]) {
        queue.push([v, next, dist + 1]);
        visited[v][next] = 1;
      }
    }
  }
}
```
