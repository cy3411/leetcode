# 描述

给出由小写字母组成的字符串 S，重复项删除操作会选择两个相邻且相同的字母，并删除它们。

在 S 上反复执行重复项删除操作，直到无法继续删除。

在完成所有重复项删除操作后返回最终的字符串。答案保证唯一。

# 示例

```
输入："abbaca"
输出："ca"
解释：
例如，在 "abbaca" 中，我们可以删除 "bb" 由于两字母相邻且相同，这是此时唯一可以执行删除操作的重复项。之后我们得到字符串 "aaca"，其中又只有 "aa" 可以执行重复项删除操作，所以最后的字符串为 "ca"。
```

# 方法

消除一对相邻重复项可能会导致新的相邻重复项出现，因此还需要保存还未被删除的字符。
我们只要遍历字符和栈顶元素，相同即消除，不同则入栈。


```js
var removeDuplicates = function (S) {
  const stack = [];

  for (let char of S) {
    if (char === stack[stack.length - 1]) {
      stack.pop();
    } else {
      stack.push(char);
    }
  }
  return stack.join('');
};
```

```js
var removeDuplicates = function (S) {
  const stack = [];
  // 保存消除状态
  let isDel = false;

  for (let char of S) {
    while (stack.length && char === stack[stack.length - 1]) {
      stack.pop();
      isDel = true;
    }
    if (isDel) {
      isDel = false;
      continue;
    }
    stack.push(char);
  }

  return stack.join('');
};
```