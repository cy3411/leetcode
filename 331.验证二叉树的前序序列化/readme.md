# 描述
序列化二叉树的一种方法是使用前序遍历。当我们遇到一个非空节点时，我们可以记录下这个节点的值。如果它是一个空节点，我们可以使用一个标记值记录，例如 #。
```
     _9_
    /   \
   3     2
  / \   / \
 4   1  #  6
/ \ / \   / \
# # # #   # #
```
例如，上面的二叉树可以被序列化为字符串 "9,3,4,#,#,1,#,#,2,#,6,#,#"，其中 # 代表一个空节点。

给定一串以逗号分隔的序列，验证它是否是正确的二叉树的前序序列化。编写一个在不重构树的条件下的可行算法。

每个以逗号分隔的字符或为一个整数或为一个表示 null 指针的 '#' 。

你可以认为输入格式总是有效的，例如它永远不会包含两个连续的逗号，比如 "1,,3" 。

# 示例
```
输入: "9,3,4,#,#,1,#,#,2,#,6,#,#"
输出: true
```
```
输入: "1,#"
输出: false
```

# 方法
我们可以把二叉树的每个节点位置看作一个slot：
+ 叶子节点，消耗1个slot
+ 非叶子空节点，消耗1个slot，并补充2个slot

## 使用栈来维护slot数的变化
```js
var isValidSerialization = function (preorder) {
  const size = preorder.length;
  const slots = [1];
  let i = 0;
  while (i < size) {
    if (slots.length === 0) {
      return false;
    }
    let char = preorder[i];
    if (char === ',') {
      i++;
    } else if (char === '#') {
      slots[slots.length - 1]--;
      if (slots[slots.length - 1] === 0) {
        slots.pop();
      }
      i++;
    } else {
      // 读取数字，可能是多位数
      while (preorder[i] !== ',' && i < size) {
        i++;
      }
      slots[slots.length - 1]--;
      if (slots[slots.length - 1] === 0) {
        slots.pop();
      }
      slots.push(2);
    }
  }
  return slots.length === 0;
};
```

## 使用常量来维护slot变化
从上面可以看出，如果把栈看作一个元素，只要维护slot的数量（即栈中元素的总和）即可
```js
var isValidSerialization = function (preorder) {
  const size = preorder.length;
  let slots = 1;
  let i = 0;
  while (i < size) {
    if (slots === 0) {
      return false;
    }
    let char = preorder[i];
    if (char === ',') {
      i++;
    } else if (char === '#') {
      slots--;
      i++;
    } else {
      // 读取数字，可能是多位数
      while (preorder[i] !== ',' && i < size) {
        i++;
      }
      slots = slots - 1 + 2;
    }
  }
  return slots === 0;
};
```