# 题目

实现 `strStr()` 函数。

给你两个字符串 `haystack` 和 `needle` ，请你在 `haystack` 字符串中找出 `needle` 字符串出现的第一个位置（下标从 0 开始）。如果不存在，则返回 `-1` 。

说明：

当 `needle` 是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。

对于本题而言，当 `needle` 是空字符串时我们应当返回 0 。这与 C 语言的 `strstr()` 以及 Java 的 `indexOf()` 定义相符。

提示：

- 0 <= haystack.length, needle.length <= 5 \* 104
- haystack 和 needle 仅由小写英文字符组成

# 示例

```
输入：haystack = "hello", needle = "ll"
输出：2
```

# 题解

## 暴力

```js
function strStr(haystack: string, needle: string): number {
  let hsize = haystack.length;
  let nsize = needle.length;
  if (nsize === 0) return 0;

  // 比较同长haystack的字串和needle
  for (let i = 0; i + nsize <= hsize; i++) {
    let match = true;
    for (let j = 0; j < nsize; j++) {
      if (haystack[j + i] !== needle[j]) {
        match = false;
        break;
      }
    }
    if (match) return i;
  }

  return -1;
}
```

```js
function strStr(haystack: string, needle: string): number {
  let hsize = haystack.length;
  let nsize = needle.length;
  if (nsize === 0) return 0;

  // js api
  for (let i = 0; i + nsize <= hsize; i++) {
    if (haystack.slice(i, i + nsize) === needle) {
      return i;
    }
  }

  return -1;
}
```

## KMP

[前缀函数与 KMP 算法](https://oi-wiki.org/string/kmp/)

```js
// 题目给出了最多26个小写字母
// 这里可以使用kmp 状态机的算法
function strStr(haystack: string, needle: string): number {
  let hsize = haystack.length;
  let nsize = needle.length;
  if (nsize === 0) return 0;
  // 状态机
  const R = 26;
  const dfa: number[][] = new Array(R).fill(0).map(() => new Array(nsize).fill(0));
  const base = 'a'.charCodeAt(0);
  dfa[needle.charCodeAt(0) - base][0] = 1;
  for (let i = 1, x = 0; i < nsize; i++) {
    for (let c = 0; c < R; c++) {
      dfa[c][i] = dfa[c][x];
    }
    dfa[needle.charCodeAt(i) - base][i] = i + 1;
    x = dfa[needle.charCodeAt(i) - base][x];
  }

  // 搜索
  let i = 0;
  let j = 0;
  while (i < hsize && j < nsize) {
    j = dfa[haystack.charCodeAt(i) - base][j];
    i++;
  }
  return j === nsize ? i - nsize : -1;
}
```

```js
function strStr(haystack: string, needle: string): number {
  let hsize = haystack.length;
  let nsize = needle.length;
  if (nsize === 0) return 0;

  // 前缀函数
  const nextFunc = (pattern: string, next: number[] = [0]): typeof next => {
    let i = 1;
    while (i < pattern.length) {
      let j = next[i - 1];
      while (j > 0 && pattern[i] !== pattern[j]) j = next[j - 1];
      if (pattern[i] === pattern[j]) j++;
      next[i++] = j;
    }
    return next;
  };

  const next = nextFunc(needle);
  let i = 0;
  let j = 0;
  while (i < hsize && j < nsize) {
    while (j > 0 && haystack[i] !== needle[j]) j = next[j - 1];
    if (haystack[i] === needle[j]) j++;
    i++;
  }

  return j === nsize ? i - j : -1;
}
```

## Sunday 算法

```ts
function strStr(haystack: string, needle: string): number {
  const n = needle.length;
  const m = haystack.length;
  const index = new Array(26).fill(-1);
  const base = 'a'.charCodeAt(0);
  // 记录匹配串的字符下标
  for (let i = 0; i < n; i++) {
    const idx = needle.charCodeAt(i) - base;
    index[idx] = i;
  }
  let i = 0;
  while (i + n <= m) {
    let flag = 1;
    for (let j = 0; j < n; j++) {
      if (haystack[i + j] === needle[j]) continue;
      flag = 0;
      break;
    }
    if (flag === 1) return i;
    const idx = haystack[i + n] ? haystack[i + n].charCodeAt(0) - base : -1;
    i += n - index[idx];
  }

  return -1;
}
```
