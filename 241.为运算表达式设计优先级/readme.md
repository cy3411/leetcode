# 题目

给定一个含有数字和运算符的字符串，为表达式添加括号，改变其运算优先级以求出不同的结果。你需要给出所有可能的组合的结果。有效的运算符号包含 `+`, `-` 以及 `*` 。

# 示例

```
输入: "2-1-1"
输出: [0, 2]
解释:
((2-1)-1) = 0
(2-(1-1)) = 2
```

```
输入: "2*3-4*5"
输出: [-34, -14, -10, -10, 10]
解释:
(2*(3-(4*5))) = -34
((2*3)-(4*5)) = -14
((2*(3-4))*5) = -10
(2*((3-4)*5)) = -10
(((2*3)-4)*5) = 10
```

# 题解

## 分治法

分治法是一种递归算法，它将问题分解为规模较小的子问题，然后递归地解决子问题，最后将子问题的解合并为原问题的解。

利用运算符将问题分解成子问题，然后递归求解。

```ts
function diffWaysToCompute(expression: string): number[] {
  const ans: number[] = [];
  const n = expression.length;
  // 遍历，将每个数字和运算符分开
  for (let i = 0; i < n; i++) {
    const op = expression[i];
    if (op !== '+' && op !== '-' && op !== '*') continue;
    // 左边的数字
    const left: number[] = diffWaysToCompute(expression.substring(0, i));
    // 右边的数字
    const right: number[] = diffWaysToCompute(expression.substring(i + 1));
    // 左右两边的数字，进行运算
    for (let l of left) {
      for (let r of right) {
        switch (op) {
          case '+':
            ans.push(l + r);
            break;
          case '-':
            ans.push(l - r);
            break;
          case '*':
            ans.push(l * r);
            break;
        }
      }
    }
  }
  // 只剩下数字
  if (ans.length === 0) {
    ans.push(Number(expression));
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<int> diffWaysToCompute(string expression) {
        vector<int> ans;
        int n = expression.size();
        for (int i = 0; i < n; i++) {
            char op = expression[i];
            if (op != '+' && op != '-' && op != '*') {
                continue;
            }

            vector<int> left = diffWaysToCompute(expression.substr(0, i));
            vector<int> right = diffWaysToCompute(expression.substr(i + 1));

            for (auto l : left) {
                for (auto r : right) {
                    switch (op) {
                    case '+':
                        ans.push_back(l + r);
                        break;
                    case '-':
                        ans.push_back(l - r);
                        break;
                    case '*':
                        ans.push_back(l * r);
                        break;
                    }
                }
            }
        }

        if (ans.size() == 0) {
            ans.push_back(stoi(expression));
        }

        return ans;
    }
};
```
