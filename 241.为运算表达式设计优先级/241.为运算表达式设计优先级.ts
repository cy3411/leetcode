/*
 * @lc app=leetcode.cn id=241 lang=typescript
 *
 * [241] 为运算表达式设计优先级
 */

// @lc code=start
function diffWaysToCompute(expression: string): number[] {
  const ans: number[] = [];
  const n = expression.length;
  // 遍历，将每个数字和运算符分开
  for (let i = 0; i < n; i++) {
    const op = expression[i];
    if (op !== '+' && op !== '-' && op !== '*') continue;
    // 左边的数字
    const left: number[] = diffWaysToCompute(expression.substring(0, i));
    // 右边的数字
    const right: number[] = diffWaysToCompute(expression.substring(i + 1));
    // 左右两边的数字，进行运算
    for (let l of left) {
      for (let r of right) {
        switch (op) {
          case '+':
            ans.push(l + r);
            break;
          case '-':
            ans.push(l - r);
            break;
          case '*':
            ans.push(l * r);
            break;
        }
      }
    }
  }
  // 只剩下数字
  if (ans.length === 0) {
    ans.push(Number(expression));
  }

  return ans;
}
// @lc code=end
