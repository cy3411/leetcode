# 题目

给你一个整数数组 `nums` 和一个整数 `x` 。每一次操作时，你应当移除数组 `nums` 最左边或最右边的元素，然后从 `x` 中减去该元素的值。请注意，需要 **修改** 数组以供接下来的操作使用。

如果可以将 `x` 恰好 减到 `0` ，返回 **最小操作数** ；否则，返回 `-1` 。

# 示例

```
输入：nums = [1,1,4,2,3], x = 5
输出：2
解释：最佳解决方案是移除后两个元素，将 x 减到 0 。
```

# 题解

## 二分查找

题目的意思就是从数组的头或者尾寻找元素，用 `x` 去减去这个元素，需要多少次操作，才能将 `x` 减少到 `0`，要求返回最小的次数。

这里可以看出都是区间元素之和的操作，所以可以使用前缀和数组。

定义从前向后的前缀和数组和从后向前的前缀数组，然后遍历左边前缀和，使用差值去右边前缀和中寻找，看看是否有结果。

题目给出的数组是一个整数数组，它的前缀和数组一定具有单调性，所以寻找差值可以使用二分查找来实现。

```ts
function minOperations(nums: number[], x: number): number {
  const n = nums.length;

  const left = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    left[i] = left[i - 1] + nums[i - 1];
  }

  const right = new Array(n + 1).fill(0);
  for (let i = n; i > 0; i--) {
    right[n - i + 1] = right[n - i] + nums[i - 1];
  }

  const binarySearch = (nums: number[], target: number): number => {
    let head = 0;
    let tail = nums.length - 1;
    let mid: number;

    while (head <= tail) {
      mid = head + ((tail - head) >> 1);
      if (nums[mid] === target) return mid;
      if (nums[mid] < target) head = mid + 1;
      else tail = mid - 1;
    }

    return -1;
  };

  let result = -1;
  // 遍历左区间和
  for (let i = 0; i < left.length; i++) {
    const target = x - left[i];
    const j = binarySearch(right, target);
    // 没找到
    if (j === -1) continue;
    // 超出范围了
    if (i + j > n) continue;
    // 更新结果
    result = result === -1 ? i + j : Math.min(result, i + j);
  }

  return result;
}
```
