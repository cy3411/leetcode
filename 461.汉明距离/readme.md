# 题目

两个整数之间的`汉明距离`指的是这两个数字对应二进制位不同的位置的数目。

给出两个整数 x 和 y，计算它们之间的汉明距离。

注意：

`0 ≤ x, y < 231`

# 示例

```
输入: x = 1, y = 4

输出: 2

解释:
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑

上面的箭头指出了对应二进制位不同的位置。
```

# 题解

## 位运算

遍历每个二进制位，做比较同时更新结果。

```ts
function hammingDistance(x: number, y: number): number {
  let result = 0;
  // 每个二进制位做比较
  for (let i = 0; i < 31; i++) {
    let carry = 1 << i;
    if ((x & carry) !== (y & carry)) {
      result++;
    }
  }

  return result;
}
```

## 位异或

`x` 和 `y` 的异或结果中 `1` 的个数就是汉明距离.

```ts
function hammingDistance(x: number, y: number): number {
  // 取得异或结果
  let z = x ^ y;
  let ans = 0;
  // 计算有多少个1
  while (z !== 0) {
    z &= z - 1;
    ans++;
  }

  return ans;
}
```
