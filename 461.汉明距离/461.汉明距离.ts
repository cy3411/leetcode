/*
 * @lc app=leetcode.cn id=461 lang=typescript
 *
 * [461] 汉明距离
 */

// @lc code=start
function hammingDistance(x: number, y: number): number {
  // 取得异或结果
  let z = x ^ y;
  let ans = 0;
  // 计算有多少个1
  while (z !== 0) {
    z &= z - 1;
    ans++;
  }

  return ans;
}
// @lc code=end
