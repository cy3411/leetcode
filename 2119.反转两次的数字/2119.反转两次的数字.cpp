/*
 * @lc app=leetcode.cn id=2119 lang=cpp
 *
 * [2119] 反转两次的数字
 */

// @lc code=start
class Solution
{
public:
    long long reverse(long long n)
    {
        long long res = 0;
        while (n)
        {
            res = res * 10 + n % 10;
            n /= 10;
        }
        return res;
    }
    bool isSameAfterReversals(int num)
    {
        return num == reverse(reverse(num));
    }
};
// @lc code=end
