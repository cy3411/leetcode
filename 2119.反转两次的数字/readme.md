# 题目

**反转** 一个整数意味着倒置它的所有位。

- 例如，反转 `2021` 得到 `1202` 。反转 `12300` 得到 `321` ，不保留前导零 。

给你一个整数 `num` ，反转 `num` 得到 `reversed1` ，接着反转 `reversed1` 得到 `reversed2` 。如果 `reversed2` 等于 `num` ，返回 `true` ；否则，返回 `false` 。

提示：

- $\color{burlywood}0 <= num <= 10^6$

# 示例

```
输入：num = 526
输出：true
解释：反转 num 得到 625 ，接着反转 625 得到 526 ，等于 num 。
```

```
输入：num = 1800
输出：false
解释：反转 num 得到 81 ，接着反转 81 得到 18 ，不等于 num 。
```

```
输入：num = 0
输出：true
解释：反转 num 得到 0 ，接着反转 0 得到 0 ，等于 num 。
```

# 题解

## 模拟

根据题意，将 num 反转两次，然后比较两次反转后的数字是否相等。

```ts
function isSameAfterReversals(num: number): boolean {
  const reverse = (x: number): number => {
    let y = 0;
    while (x) {
      y = y * 10 + (x % 10);
      x = (x / 10) >> 0;
    }
    return y;
  };

  return num === reverse(reverse(num));
}
```

```cpp
class Solution
{
public:
    long long reverse(long long n)
    {
        long long res = 0;
        while (n)
        {
            res = res * 10 + n % 10;
            n /= 10;
        }
        return res;
    }
    bool isSameAfterReversals(int num)
    {
        return num == reverse(reverse(num));
    }
};
```
