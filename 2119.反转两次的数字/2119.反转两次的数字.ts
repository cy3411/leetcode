/*
 * @lc app=leetcode.cn id=2119 lang=typescript
 *
 * [2119] 反转两次的数字
 */

// @lc code=start
function isSameAfterReversals(num: number): boolean {
  const reverse = (x: number): number => {
    let y = 0;
    while (x) {
      y = y * 10 + (x % 10);
      x = (x / 10) >> 0;
    }
    return y;
  };

  return num === reverse(reverse(num));
}
// @lc code=end
