# 题目

给定一个字符串数组 `arr`，字符串 `s` 是将 `arr` 某一子序列字符串连接所得的字符串，如果 `s` 中的每一个字符都只出现过一次，那么它就是一个可行解。

请返回所有可行解 `s` 中最长长度。

提示：

- `1 <= arr.length <= 16`
- `1 <= arr[i].length <= 26`
- `arr[i]` 中只含有小写英文字母

# 示例

```
输入：arr = ["un","iq","ue"]
输出：4
解释：所有可能的串联组合是 "","un","iq","ue","uniq" 和 "ique"，最大长度为 4。
```

# 题解

## 深度优先

题目给出只有小写字母，我们可以通过 26 个比特位来判断是否有重复的字符出现。

先将数据中有重复字符的元素删除，然后递归比较即可。

```ts
function maxLength(arr: string[]): number {
  // 保存每个元素的二进制位，来判断是否出现过某个字符
  const masks = [];

  // 删选arr中无重复字符的字符串
  for (const s of arr) {
    let mask = 0;
    for (let i = 0; i < s.length; i++) {
      const code = s.charCodeAt(i) - 'a'.charCodeAt(0);
      if (((mask >> code) & 1) !== 0) {
        mask = 0;
        break;
      }
      mask |= 1 << code;
    }
    if (mask > 0) masks.push(mask);
  }

  const dfs = (masks: number[], i: number, mask: number) => {
    // 找到最后，都没有重复的元素，更新答案
    if (i === masks.length) {
      ans = Math.max(ans, mask.toString(2).split('0').join('').length);
      return;
    }
    // 如果无重复的，继续累加下一个元素检测
    if ((masks[i] & mask) === 0) {
      dfs(masks, i + 1, masks[i] | mask);
    }
    // 有重复的，跳过当前元素，继续开一个检测
    dfs(masks, i + 1, mask);
  };

  let ans = 0;
  dfs(masks, 0, 0);
  return ans;
}
```
