# 题目
给一非空的单词列表，返回前 k 个出现次数最多的单词。

返回的答案应该按单词出现频率由高到低排序。如果不同的单词有相同出现频率，按字母顺序排序。

注意：

+ 假定 k 总为有效值， 1 ≤ k ≤ 集合元素数。
+ 输入的单词均由小写字母组成。
 

扩展练习：

尝试以 O(n log k) 时间复杂度和 O(n) 空间复杂度解决。

# 示例
```
输入: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
输出: ["i", "love"]
解析: "i" 和 "love" 为出现次数最多的两个单词，均为2次。
    注意，按字母顺序 "i" 在 "love" 之前。
```

```
输入: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
输出: ["the", "is", "sunny", "day"]
解析: "the", "is", "sunny" 和 "day" 是出现次数最多的四个单词，
    出现次数依次为 4, 3, 2 和 1 次。
```

# 题解
## 堆
维护一个大小为 `k` 的小顶堆。

先统计单词出现的频率，然后将单词放入堆中，超过`k`的尺寸后将堆顶弹出。

堆中剩下的就是结果。

最后需要将结果按照题意做一次排序。

```js
class PQ {
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;

    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  less(i, j) {
    if (this.mapKeysValue(this.keys[i]) !== this.mapKeysValue(this.keys[j])) {
      return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
    } else {
      return this.keys[i] < this.keys[j];
    }
  }

  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }

  size() {
    return this.keys.length;
  }

  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }

  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }

  peek() {
    return this.keys[0];
  }
}
class MinPQ extends PQ {
  constructor(keys, mapKeysValue = (x) => x) {
    super(keys, (x) => -1 * mapKeysValue(x));
  }
}

/**
 * @param {string[]} words
 * @param {number} k
 * @return {string[]}
 */
var topKFrequent = function (words, k) {
  const map = new Map();
  for (const word of words) {
    map.set(word, (map.get(word) || 0) + 1);
  }

  const minPQ = new MinPQ([], (x) => map.get(x));
  for (const key of map.keys()) {
    minPQ.insert(key);
    if (minPQ.size() > k) minPQ.poll();
  }

  return [...minPQ.keys].sort((a, b) => {
    if (map.get(a) !== map.get(b)) return map.get(b) - map.get(a);

    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
  });
};
```

## 排序
使用哈希表统计出频次，然后将结果按照题意降序排序，最后的结果前 `k` 个。
```ts
function topKFrequent(words: string[], k: number): string[] {
  const hash: Map<string, number> = new Map();

  for (let word of words) {
    hash.set(word, (hash.get(word) || 0) + 1);
  }

  let result = Array.from(hash.entries());
  result.sort((b, a) => {
    if (a[1] < b[1]) {
      return -1;
    } else if (a[1] > b[1]) {
      return 1;
    } else {
      if (a[0] > b[0]) {
        return -1;
      } else if (a[0] < b[0]) {
        return 1;
      } else {
        return 0;
      }
    }
  });

  return result.slice(0, k).map((val) => val[0]);
}
```