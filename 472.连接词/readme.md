# 题目

给你一个 不含重复 单词的字符串数组 `words` ，请你找出并返回 `words` 中的所有 连接词 。

**连接词** 定义为：一个完全由给定数组中的至少两个较短单词组成的字符串。

提示：

- $\color{burlywood}1 \leq words.length \leq 10^4$
- $\color{burlywood}0 \leq words[i].length \leq 1000$
- `words[i]` 仅由小写字母组成
- $\color{burlywood}0 \leq sum(words[i].length) \leq 10^5$

# 示例

```
输入：words = ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]
输出：["catsdogcats","dogcatsdog","ratcatdogcat"]
解释："catsdogcats" 由 "cats", "dog" 和 "cats" 组成;
     "dogcatsdog" 由 "dog", "cats" 和 "dog" 组成;
     "ratcatdogcat" 由 "rat", "cat", "dog" 和 "cat" 组成。
```

```
输入：words = ["cat","dog","catdog"]
输出：["catdog"]
```

# 题解

## Trie 树

为了方便处理，先将数组 `words` 按照字符串的长度升序排序，排序后可以确保当遍历到任意单词时，比该单词短的单词一定都已经遍历过，因此可以根据已经遍历过的全部单词判断当前单词是不是连接词。

我们可以利用 Trie 树，将更短的单词放入树中，这样可以方便的判断当前单词是否是连接词。

```ts
class TrieNode {
  isWord: boolean = false;
  children: TrieNode[] = new Array(26);
}

class Trie {
  base: number = 97;
  root: TrieNode = new TrieNode();
  insert(word: string): void {
    let node = this.root;
    for (let i = 0; i < word.length; i++) {
      const index = word.charCodeAt(i) - this.base;
      if (node.children[index] === void 0) {
        node.children[index] = new TrieNode();
      }
      node = node.children[index];
    }
    node.isWord = true;
  }
}

function findAllConcatenatedWordsInADict(words: string[]): string[] {
  // 将 words 通过长度升序排序
  words.sort((a, b) => a.length - b.length);

  const dfs = (word: string, start: number, tire: Trie): boolean => {
    const n = word.length;
    // 找到结尾了
    if (n === start) return true;

    let node: TrieNode = tire.root;
    for (let i = start; i < n; i++) {
      const index = word.charCodeAt(i) - tire.base;
      node = node.children[index];
      // 字典树中未找到有效结果
      if (node === void 0) return false;
      // 找到结果，继续匹配后续的字符串是否可以找到
      if (node.isWord) {
        if (dfs(word, i + 1, tire)) return true;
      }
    }
    return false;
  };

  const tire = new Trie();
  const ans: string[] = [];
  // 遍历 words ，处理每一个单词
  for (const word of words) {
    if (word === '') continue;
    if (dfs(word, 0, tire)) {
      // 如果能找到较短单词组成，当前元素放入答案
      ans.push(word);
    } else {
      // 否则放入字典树中当做短语
      tire.insert(word);
    }
  }

  return ans;
}
```
