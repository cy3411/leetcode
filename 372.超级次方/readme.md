# 题目

你的任务是计算 $a^b$ 对 `1337` 取模，`a` 是一个正整数，`b` 是一个非常大的正整数且会以数组形式给出。

提示：

- $1 \leq a \leq 2^{31} - 1$
- $1 \leq b.length \leq 2000$
- $0 \leq b[i] \leq 9$
- `b` 不含前导 0

# 示例

```
输入：a = 2, b = [3]
输出：8
```

```
输入：a = 2147483647, b = [2,0,0]
输出：1198
```

# 题解

## 快速幂

比如求解$a^{123}$，可以分解为$(a^1)^3*(a^10)^2*(a^100)^1$。

倒序遍历 b 数组，按照上面公式即可求解。

```ts
function superPow(a: number, b: number[]): number {
  // 快速幂
  const power = (base: number, n: number): number => {
    base = base % mod;
    let res = 1;
    while (n !== 0) {
      if (n & 1) res = (res * base) % mod;
      base = (base * base) % mod;
      n >>= 1;
    }
    return res;
  };

  let mod = 1337;
  let base = a % mod;
  let ans = 1;
  // a^12分解为(a^2)*(a^10)^1
  for (let i = b.length - 1; i >= 0; i--) {
    ans = (ans * power(base, b[i])) % mod;
    base = power(base, 10);
  }

  return ans;
}
```
