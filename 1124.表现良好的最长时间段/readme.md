# 题目
给你一份工作时间表 hours，上面记录着某一位员工每天的工作小时数。

我们认为当员工一天中的工作小时数大于 8 小时的时候，那么这一天就是「劳累的一天」。

所谓「表现良好的时间段」，意味在这段时间内，「劳累的天数」是严格 大于「不劳累的天数」。

请你返回「表现良好时间段」的最大长度。

提示：
+ 1 <= hours.length <= 10000
+ 0 <= hours[i] <= 16

# 示例
```
输入：hours = [9,9,6,0,6,6,9]
输出：3
解释：最长的表现良好时间段是 [9,9,6]。
```
# 方法
员工工作只有两种状态，超过8小时和未超过8小时。

我们先将数组转换：
+ 超过8小时的设为1
+ 未超过8小时的设为-1

然后求解，新数组中最长的子数组长度，其和大于0。

## 暴力破解
```js
/**
 * @param {number[]} hours
 * @return {number}
 */
var longestWPI = function (hours) {
  const size = hours.length;
  const types = new Array(size);

  for (let i = 0; i < size; i++) {
    types[i] = hours[i] > 8 ? 1 : -1;
  }

  let result = 0;
  for (let i = 0; i < size; i++) {
    let sum = 0;
    for (let j = i; j < size; j++) {
      sum += types[j];
      // 「劳累的天数」是严格大于「不劳累的天数」
      if (sum > 0) {
        result = Math.max(result, j - i + 1);
      }
    }
  }

  return result;
};
```

## 前缀和
如果第一天到第n天的和如果大于0，就证明从第一天到第n天这段区间是表现良好的。

所以我们可以计算下从第一天开始到第n天的和，也就是我们的前缀和。

我们要求的区间假设是第i天到第j天。要满足`i<j`并且`preSum[j]-preSum[i]`大于零。所以我们
第i天的前缀和要尽可能的小。

我们单调递减栈来记录i，最后倒序遍历.

```js
/**
 * @param {number[]} hours
 * @return {number}
 */
var longestWPI = function (hours) {
  const size = hours.length;
  // 前缀和
  const presum = new Array(size + 1).fill(0);

  for (let i = 0; i < size; i++) {
    if (hours[i] > 8) {
      presum[i + 1] = presum[i] + 1;
    } else {
      presum[i + 1] = presum[i] - 1;
    }
  }

  // 下标i的前缀和要尽可能的小，这里用单调递减栈
  let stack = [];
  for (let i = 0; i < presum.length; i++) {
    if (stack.length === 0 || presum[stack[stack.length - 1]] > presum[i]) {
      stack.push(i);
    }
  }

  let result = 0;
  let j = size;
  // 倒序遍历，当preSum[j]>preSum[i]的时候，更新结果，直到栈中的i全部遍历完毕
  while (j > result) {
    while (stack.length && presum[stack[stack.length - 1]] < presum[j]) {
      result = Math.max(result, j - stack.pop());
    }
    j--;
  }

  return result;
};
```