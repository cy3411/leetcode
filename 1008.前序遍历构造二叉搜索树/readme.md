# 题目

返回与给定前序遍历 `preorder` 相匹配的二叉搜索树（binary search tree）的根结点。

(回想一下，二叉搜索树是二叉树的一种，其每个节点都满足以下规则，对于 `node.left` 的任何后代，值总 < `node.val`，而 `node.right` 的任何后代，值总 > `node.val`。此外，前序遍历首先显示节点 `node` 的值，然后遍历 `node.left`，接着遍历 `node.right`。）

题目保证，对于给定的测试用例，总能找到满足要求的二叉搜索树。

提示：

- 1 <= preorder.length <= 100
- 1 <= preorder[i] <= 10^8
- preorder 中的值互不相同

# 示例

[![favfQf.png](https://z3.ax1x.com/2021/08/11/favfQf.png)](https://imgtu.com/i/favfQf)

```
输入：[8,5,1,7,10,12]
输出：[8,5,10,1,7,null,12]
```

# 题解

## 前序遍历

前序遍历结果的第一个元素是节点，第二个元素是左节点，第一个大于根节点的元素是右节点。

通过上面的特性，创建节点，然后递归的去创建左右子树节点。

```ts
function bstFromPreorder(preorder: number[]): TreeNode | null {
  const build = (order: number[], l: number, r: number): TreeNode | null => {
    if (l > r) return null;
    let index = l + 1;
    // 找到右子树的位置
    // index不能超过r的位置
    while (index <= r && order[index] < order[l]) index++;
    const root = new TreeNode(order[l]);
    root.left = build(preorder, l + 1, index - 1);
    root.right = build(preorder, index, r);
    return root;
  };

  return build(preorder, 0, preorder.length - 1);
}
```
