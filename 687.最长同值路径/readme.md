# 题目

给定一个二叉树的 root ，返回 最长的路径的长度 ，这个路径中的 每个节点具有相同值 。 这条路径可以经过也可以不经过根节点。

两个节点之间的路径长度 由它们之间的边数表示。

提示:

- 树的节点数的范围是 `[0, 104]`
- $-1000 \leq Node.val \leq 1000$
- 树的深度将不超过 `1000`

# 示例

[![vILDSI.png](https://s1.ax1x.com/2022/09/02/vILDSI.png)](https://imgse.com/i/vILDSI)

```
输入：root = [5,4,5,1,1,5]
输出：2
```

[![vILfYj.png](https://s1.ax1x.com/2022/09/03/vILfYj.png)](https://imgse.com/i/vILfYj)

```
输入：root = [1,4,5,4,4,5]
输出：2
```

# 题解

## 深度优先搜索

对根节点进行深度遍历，对于当前节点，分别获取它左右子树的同值路径长度 `l` 和 `r`。

那么节点左边的最长同值路径就是 $lcount = l + 1$ ， 节点右边的最长同值路径就是 $rcount = r + 1$ 。

我们更新答案 $ans = max(ans, lcount + rcount)$。

同时返回节点对应的最长同值路径 $max(lcount, rcount)$。

```js
function longestUnivaluePath(root: TreeNode | null): number {
  let ans = 0;

  const dfs = (root: TreeNode | null): number => {
    if (!root) return 0;
    // 递归左右子树
    let l = dfs(root.left);
    let r = dfs(root.right);
    // 计算左右同值节点路径长度
    let lcount = 0;
    let rcount = 0;
    // 通过判断左右子树节点值是否跟当前节点相等，计算路径长度
    if (root.left && root.left.val === root.val) {
      lcount = l + 1;
    }
    if (root.right && root.right.val === root.val) {
      rcount = r + 1;
    }
    // 更新答案
    ans = Math.max(ans, lcount + rcount);
    // 返回左右子树最长的路径长度
    return Math.max(lcount, rcount);
  };

  dfs(root);
  return ans;
}
```
