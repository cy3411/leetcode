# 题目

哦，不！你不小心把一个长篇文章中的空格、标点都删掉了，并且大写也弄成了小写。像句子`"I reset the computer. It still didn’t boot!"`已经变成了`"iresetthecomputeritstilldidntboot"`。在处理标点符号和大小写之前，你得先把它断成词语。当然了，你有一本厚厚的词典 `dictionary`，不过，有些词没在词典里。假设文章用 `sentence` 表示，设计一个算法，把文章断开，要求未识别的字符最少，返回未识别的字符数。

注意：本题相对原题稍作改动，只需返回未识别的字符数

提示：

- $\color{burlywood}0 \leq len(sentence) \leq 1000$
- `dictionary` 中总字符数不超过 `150000`。
- 你可以认为 `dictionary` 和 `sentence` 中只包含**小写字母**。

# 示例

```
输入：
dictionary = ["looked","just","like","her","brother"]
sentence = "jesslookedjustliketimherbrother"
输出： 7
解释： 断句后为"jess looked just like tim her brother"，共7个未识别字符。
```

# 题解

## 动态规划

定义 dp[i] 表示以 i 位置为结尾的字串中未识别的字符串，则：

$$
    dp[i] = \begin{cases}
        dp[i-1] + 1, & 当前位置和之前不能拼成单词 \\
        dp[j], & 其中 j < i，且 sentence[j:i] 在 dictionary 中。
    \end{cases}
$$

转移方程式就是：

$$ min(dp[i-1]+1, dp[j])$$

从字符串中判断是否是单词，可以用一个字典来判断。

```ts
function respace(dictionary: string[], sentence: string): number {
  // 将词典中的单词放入Trie树中
  const tree: Trie = new Trie();
  for (let word of dictionary) {
    tree.insert(word);
  }
  // 遍历句子中的字符，如果当前位置找到单词，将单词开始位置的索引放入对应的mark数组中
  // mark[i] = [j] 表示从i到j的字符串是单词
  // 因为有的单词会有多个字典匹配，所以mark[i][j]是一个数组
  // 比如:brother，会有borother和her两个字典匹配，所以mark[7] = [0,4]
  const n = sentence.length;
  const mark: number[][] = new Array(n + 1).fill(0).map((_) => []);
  for (let i = 0; i < n; i++) {
    tree.search(sentence, i, mark);
  }
  // 动态规划
  const dp: number[] = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    dp[i] = dp[i - 1] + 1;
    for (const j of mark[i]) {
      dp[i] = Math.min(dp[i], dp[j]);
    }
  }
  return dp[n];
}

class TrieNode {
  next: TrieNode[] = new Array(26);
  isWord: boolean = false;
}

class Trie {
  root: TrieNode = new TrieNode();
  base: number = 'a'.charCodeAt(0);

  insert(word: string) {
    let p = this.root;
    for (const char of word) {
      const idx = char.charCodeAt(0) - this.base;
      if (!p.next[idx]) {
        p.next[idx] = new TrieNode();
      }
      p = p.next[idx];
    }
    p.isWord = true;
  }
  // 记录sentence中搜索到的单词的开始位置的索引
  search(s: string, pos: number, mark: number[][]) {
    let p = this.root;
    for (let i = pos; s[i]; i++) {
      const idx = s.charCodeAt(i) - this.base;
      p = p.next[idx];
      if (p == void 0) break;
      if (p.isWord) {
        mark[i + 1].push(pos);
      }
    }
  }
}
```
