# 题目

给你一个字符串 sequence ，如果字符串 word 连续重复 k 次形成的字符串是 sequence 的一个子字符串，那么单词 word 的 重复值为 k 。单词 word 的 最大重复值 是单词 word 在 sequence 中最大的重复值。如果 word 不是 sequence 的子串，那么重复值 k 为 0 。

给你一个字符串 sequence 和 word ，请你返回 最大重复值 k

提示：

- $1 \leq sequence.length \leq 100$
- $1 \leq word.length \leq 100$
- sequence 和 word 都只包含小写英文字母。

# 示例

```
输入：sequence = "ababc", word = "ab"
输出：2
解释："abab" 是 "ababc" 的子字符串。
```

```
输入：sequence = "ababc", word = "ba"
输出：1
解释："ba" 是 "ababc" 的子字符串，但 "baba" 不是 "ababc" 的子字符串。
```

# 题解

## 字符串匹配

匹配 word，成功的话答案加 1，继续匹配重复的 word，直到匹配失败。

```js
function maxRepeating(sequence: string, word: string): number {
  let ans = 0;
  let temp = word;
  while (sequence.includes(temp)) {
    ans++;
    temp += word;
  }
  return ans;
}
```
