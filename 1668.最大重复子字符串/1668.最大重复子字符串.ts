/*
 * @lc app=leetcode.cn id=1668 lang=typescript
 *
 * [1668] 最大重复子字符串
 */

// @lc code=start
function maxRepeating(sequence: string, word: string): number {
  let ans = 0;
  let temp = word;
  while (sequence.includes(temp)) {
    ans++;
    temp += word;
  }
  return ans;
}
// @lc code=end
