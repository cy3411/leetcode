/*
 * @lc app=leetcode.cn id=231 lang=typescript
 *
 * [231] 2的幂
 */

// @lc code=start
function isPowerOfTwo(n: number): boolean {
  // 2的冥在二进制中只包含一个1，所以移除低位的1，看结果是否为0就知道了 。
  return n > 0 && (n & -n) === n;
}
// @lc code=end
