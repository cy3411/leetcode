# 题目

有两个容量分别为 x 升 和 y 升 的水壶以及无限多的水。请判断能否通过使用这两个水壶，从而可以得到恰好 z 升 的水？

如果可以，最后请用以上水壶中的一或两个来盛放取得的 z 升 水。

你允许：

装满任意一个水壶
清空任意一个水壶
从一个水壶向另外一个水壶倒水，直到装满或者倒空

# 示例

```
输入: x = 3, y = 5, z = 4
输出: True
```

# 题解

## 广度优先

根据题意，扩展下一次的不同储水状态，直到两个壶中的水等于目标值，或者搜索结束。

```ts
class Data {
  constructor(public x: number, public y: number) {
    this.x = x;
    this.y = y;
  }

  next(type: number, x: number, xmax: number, y: number, ymax: number) {
    switch (type) {
      case 0:
        // 第一个壶灌满
        return new Data(xmax, y);
      case 1:
        // 第二个壶灌满
        return new Data(x, ymax);
      case 2:
        // 第一个壶里的水清空
        return new Data(0, y);
      case 3: {
        // 第一个壶往第二个壶倒入一部分水
        let diff = Math.min(x, ymax - y);
        return new Data(x - diff, y + diff);
      }
      case 4:
        // 第二个壶里的水清空
        return new Data(x, 0);
      case 5: {
        // 第二个壶往第一个壶倒入一部分水
        let diff = Math.min(xmax - x, y);
        return new Data(x + diff, y - diff);
      }
    }
  }
}

function canMeasureWater(
  jug1Capacity: number,
  jug2Capacity: number,
  targetCapacity: number
): boolean {
  const deque = [];
  const visited = new Set();
  deque.push(new Data(0, 0));
  visited.add(`0#0`);
  while (deque.length) {
    const data: Data = deque.shift();
    // 满足目标值
    if (data.x + data.y === targetCapacity) return true;
    // 扩展状态，注意需要将访问过的状态过滤掉
    for (let i = 0; i < 6; i++) {
      const next: Data = data.next(i, data.x, jug1Capacity, data.y, jug2Capacity);
      const key = `${next.x}#${next.y}`;
      if (visited.has(key)) continue;
      visited.add(key);
      deque.push(next);
    }
  }

  return false;
}
```
