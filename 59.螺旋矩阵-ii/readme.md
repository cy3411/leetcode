# 题目
给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。

提示：

+ 1 <= n <= 20
  
# 示例
```
输入：n = 3
输出：[[1,2,3],[8,9,4],[7,6,5]]
```

# 方法
详情查看[54.螺旋矩阵](https://gitee.com/cy3411/leecode/tree/master/54.%E8%9E%BA%E6%97%8B%E7%9F%A9%E9%98%B5)
## 模拟
```js
var generateMatrix = function (n) {
  const result = new Array(n).fill(0).map((_) => new Array(n).fill(-1));
  let current = 1;
  let row = 0;
  let col = 0;
  let directions = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
  ];
  let directionsIndex = 0;

  while (current <= n * n) {
    result[row][col] = current;
    current++;
    const nextRow = row + directions[directionsIndex][0];
    const nextCol = col + directions[directionsIndex][1];

    if (
      nextRow < 0 ||
      nextRow >= n ||
      nextCol < 0 ||
      nextCol >= n ||
      result[nextRow][nextCol] !== -1
    ) {
      directionsIndex = (directionsIndex + 1) % 4;
    }
    row += directions[directionsIndex][0];
    col += directions[directionsIndex][1];
  }

  return result;
};
```

## 逐层扫描
```js
var generateMatrix = function (n) {
  const result = new Array(n).fill(0).map((_) => new Array(n).fill(-1));
  let current = 1;
  let top = 0;
  let bottom = n - 1;
  let left = 0;
  let right = n - 1;

  while (true) {
    for (let i = left; i <= right; i++) {
      result[left][i] = current++;
    }
    if (++top > bottom) break;
    for (let i = top; i <= bottom; i++) {
      result[i][right] = current++;
    }
    if (left > --right) break;
    for (let i = right; i >= left; i--) {
      result[bottom][i] = current++;
    }
    if (top > --bottom) break;
    for (let i = bottom; i >= top; i--) {
      result[i][left] = current++;
    }
    if (++left > right) break;
  }

  return result;
};
```
