# 题目

给定一棵二叉树 `root`，返回所有**重复的子树**。

对于同一类的重复子树，你只需要返回其中任意一棵的根结点即可。

如果两棵树具有**相同的结构**和**相同的结点值**，则它们是重复的。

提示：

- 树中的结点数在$[1,10^4]$范围内。
- $-200 \leq Node.val \leq 200$

# 示例

[![v7PhOe.png](https://s1.ax1x.com/2022/09/05/v7PhOe.png)](https://imgse.com/i/v7PhOe)

```
输入：root = [1,2,3,4,null,2,4,null,null,4]
输出：[[2,4],[4]]
```

[![v7P5eH.png](https://s1.ax1x.com/2022/09/05/v7P5eH.png)](https://imgse.com/i/v7P5eH)

```
输入：root = [2,1,1]
输出：[[1]]
```

# 题解

## 序列化二叉树

序列化二叉树，递归回溯过程中，使用哈希表保存序列化字符串和结点的映射，如果这个序列化字符串在哈希表中出现过，则表示有重复的节点，将其放入答案中。

```js
function findDuplicateSubtrees(root: TreeNode | null): Array<TreeNode | null> {
  // 序列化字符串到结点的映射
  const nodes = new Map<string, TreeNode>();
  // 保存重复的结点
  const repeatNode = new Set<TreeNode>();
  // 递归序列化二叉树
  // x(左子树序列化结果)(右子树序列化结果)
  const dfs = (node: TreeNode | null): string => {
    if (!node) return '';
    let substr = '';
    substr += node.val;
    substr += '(';
    substr += dfs(node.left);
    substr += ')(';
    substr += dfs(node.right);
    substr += ')';

    if (nodes.has(substr)) {
      repeatNode.add(nodes.get(substr));
    } else {
      nodes.set(substr, node);
    }

    return substr;
  };
  dfs(root);
  return [...repeatNode];
}
```

```cpp
class Solution {
private:
    unordered_map<string, TreeNode *> nodes;
    unordered_set<TreeNode *> repeatNodes;

public:
    string dfs(TreeNode *node) {
        if (node == NULL) return "";
        string sub = "";
        sub += to_string(node->val);
        sub += "(";
        sub += dfs(node->left);
        sub += ")(";
        sub += dfs(node->right);
        sub += ")";
        if (nodes.find(sub) == nodes.end()) {
            nodes[sub] = node;
        } else {
            repeatNodes.insert(nodes[sub]);
        }
        return sub;
    }
    vector<TreeNode *> findDuplicateSubtrees(TreeNode *root) {
        dfs(root);
        return {repeatNodes.begin(), repeatNodes.end()};
    }
};
```

```py
class Solution:
    def findDuplicateSubtrees(self, root: Optional[TreeNode]) -> List[Optional[TreeNode]]:
        nodes = dict()
        repeatNodes = set()

        def dfs(node: Optional[TreeNode]) -> str:
            if node is None:
                return ""
            serial = "".join([str(node.val), "(", dfs(node.left),
                              ")(", dfs(node.right), ")"])
            if (tree := nodes.get(serial)):
                repeatNodes.add(tree)
            else:
                nodes[serial] = node

            return serial

        dfs(root)

        return list(repeatNodes)
```
