/*
 * @lc app=leetcode.cn id=652 lang=cpp
 *
 * [652] 寻找重复的子树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left),
 * right(right) {}
 * };
 */
class Solution {
private:
    unordered_map<string, TreeNode *> nodes;
    unordered_set<TreeNode *> repeatNodes;

public:
    string dfs(TreeNode *node) {
        if (node == NULL) return "";
        string sub = "";
        sub += to_string(node->val);
        sub += "(";
        sub += dfs(node->left);
        sub += ")(";
        sub += dfs(node->right);
        sub += ")";
        if (nodes.find(sub) == nodes.end()) {
            nodes[sub] = node;
        } else {
            repeatNodes.insert(nodes[sub]);
        }
        return sub;
    }
    vector<TreeNode *> findDuplicateSubtrees(TreeNode *root) {
        dfs(root);
        return {repeatNodes.begin(), repeatNodes.end()};
    }
};
// @lc code=end
