# 题目

给你一个未排序的整数数组 **nums** ，请你找出其中没有出现的最小的正整数。

请你实现时间复杂度为 **O(n)** 并且只使用常数级别额外空间的解决方案。

提示：

- $1 <= nums.length <= 5 * 105$
- $-231 <= nums[i] <= 231 - 1$

# 示例

```
输入：nums = [1,2,0]
输出：3
```

```
输入：nums = [7,8,9,11,12]
输出：1
```

# 题解

## 交换位置

由题意可知，正确的数组是元素等于索引+1。

我们直接操作源数组，将每个元素交换到正确的索引位置上。

最后遍历数组，第一个不满足条件的索引加 1 就是答案。

```ts
function firstMissingPositive(nums: number[]): number {
  const m = nums.length;
  // 将元素交换到正确的索引位置上
  for (let i = 0; i < m; i++) {
    while (nums[i] !== i + 1) {
      // 值超出区间，不做交换操作
      if (nums[i] <= 0 || nums[i] > m) break;
      let idx = nums[i] - 1;
      // 交换双方相同，不做处理
      if (nums[i] === nums[idx]) break;
      // 交换元素
      [nums[i], nums[idx]] = [nums[idx], nums[i]];
    }
  }
  let ans = 0;
  // 如果当前元素和索引不匹配，跳出循环
  while (ans <= m && nums[ans] === ans + 1) {
    ans++;
  }
  return ans + 1;
}
```
