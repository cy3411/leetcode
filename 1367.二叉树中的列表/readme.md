# 题目

给你一棵以 root 为根的二叉树和一个 head 为第一个节点的链表。

如果在二叉树中，存在一条一直向下的路径，且每个点的数值恰好一一对应以 head 为首的链表中每个节点的值，那么请你返回 True ，否则返回 False 。

一直向下的路径的意思是：从树中某个节点开始，一直连续向下的路径。

提示：

- 二叉树和链表中的每个节点的值都满足 1 <= node.val <= 100 。
- 链表包含的节点数目在 1 到 100 之间。
- 二叉树包含的节点数目在 1 到 2500 之间。

# 示例

[![WvODyR.png](https://z3.ax1x.com/2021/07/31/WvODyR.png)](https://imgtu.com/i/WvODyR)

```
输入：head = [4,2,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
输出：true
解释：树中蓝色的节点构成了与链表对应的子路径。
```

# 题解

## 递归

递归遍历找到 root 和 head 的相同的头节点，然后继续递归寻找该头节点开始位置是否有全匹配路径。

```ts
// 比较是否有全匹配路径
function judge(head: ListNode | null, root: TreeNode | null): boolean {
  // 找到了全匹配路径
  if (head === null) return true;
  // root为null，head还有节点，肯定不是全匹配路径
  if (root === null) return false;
  // 匹配路径过程中，有不同节点
  if (root.val !== head.val) return false;
  // 递归比较左右节点和下一个链表节点
  return judge(head.next, root.left) || judge(head.next, root.right);
}

// 递归寻找相同的第一个节点
function isSubPath(head: ListNode | null, root: TreeNode | null): boolean {
  // head是空链表，肯定能找到路径
  if (head === null) return true;
  // root是空节点，肯定没有路径
  if (root === null) return false;
  // 找到相同的节点，开始匹配是否有相同的路径
  if (root.val === head.val && judge(head, root)) return true;
  // 递归比较root的左右节点
  return isSubPath(head, root.left) || isSubPath(head, root.right);
}
```
