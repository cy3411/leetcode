# 题目
给你一棵二叉搜索树，请你 **按中序遍历** 将其重新排列为一棵递增顺序搜索树，使树中最左边的节点成为树的根节点，并且每个节点没有左子节点，只有一个右子节点。

提示：
+ 树中节点数的取值范围是 [1, 100]
+ 0 <= Node.val <= 1000

# 示例  
![cz3Ijx.md.png](https://z3.ax1x.com/2021/04/25/cz3Ijx.md.png)
```
输入：root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
输出：[1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
```
# 题解
## 模拟
按照题目的要求中序遍历，然后按照结果重新生成一棵递增顺序搜索树。
```js
function increasingBST(root: TreeNode | null): TreeNode | null {
  if (root === null) return null;

  let result: number[] = [];
  const inorder = (root: TreeNode): void => {
    if (root === null) return null;
    inorder(root.left);
    result.push(root.val);
    inorder(root.right);
  };

  inorder(root);

  const dummy = new TreeNode();
  let curr = dummy;

  for (let i = 0; i < result.length; i++) {
    curr.right = new TreeNode(result[i]);
    curr = curr.right;
  }

  curr.right = null;

  return dummy.right;
}
```

也可以在中序遍历的过程中直接处理
```js
function increasingBST(root: TreeNode | null): TreeNode | null {
  if (root === null) return null;

  const dummy = new TreeNode();
  let curr = dummy;

  const inorder = (root: TreeNode): void => {
    if (root === null) return null;
    inorder(root.left);
    // 这里处理新的搜索树节点
    curr.right = root;
    curr = curr.right;
    root.left = null;
    inorder(root.right);
  };

  inorder(root);

  return dummy.right;
}
```