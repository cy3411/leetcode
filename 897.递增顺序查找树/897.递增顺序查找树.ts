/*
 * @lc app=leetcode.cn id=897 lang=typescript
 *
 * [897] 递增顺序查找树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function increasingBST(root: TreeNode | null): TreeNode | null {
  if (root === null) return null;

  const dummy = new TreeNode();
  let curr = dummy;

  const inorder = (root: TreeNode): void => {
    if (root === null) return null;
    inorder(root.left);
    // 这里处理新的搜索树节点
    curr.right = root;
    curr = curr.right;
    root.left = null;
    inorder(root.right);
  };

  inorder(root);

  return dummy.right;
}
// @lc code=end
