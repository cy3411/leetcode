/*
 * @lc app=leetcode.cn id=442 lang=typescript
 *
 * [442] 数组中重复的数据
 */

// @lc code=start
function findDuplicates(nums: number[]): number[] {
  nums.sort();
  const n = nums.length;
  const ans: number[] = [];
  for (let i = 1; i < n; i++) {
    if (nums[i] === nums[i - 1]) {
      ans.push(nums[i]);
    }
  }
  return ans;
}
// @lc code=end
