# 题目

给你一个长度为 `n` 的整数数组 `nums` ，其中 `nums` 的所有整数都在范围 `[1, n]` 内，且每个整数出现 **一次** 或 两**次** 。请你找出所有出现 **两次** 的整数，并以数组形式返回。

你必须设计并实现一个时间复杂度为 `O(n)` 且仅使用常量额外空间的算法解决此问题。

提示：

- $n \equiv nums.length$
- $1 \leq n \leq 10^5$
- $1 \leq nums[i] \leq n$
- `nums` 中的每个元素出现 **一次** 或 **两次**

# 示例

```
输入：nums = [4,3,2,7,8,2,3,1]
输出：[2,3]
```

```
输入：nums = [1,1,2]
输出：[1]
```

```
输入：nums = [1]
输出：[]
```

# 题解

## 排序

将数组排序，然后遍历数组，如果当前数字和前一个数字相同，则记录当前数字。

```ts
function findDuplicates(nums: number[]): number[] {
  nums.sort();
  const n = nums.length;
  const ans: number[] = [];
  for (let i = 1; i < n; i++) {
    if (nums[i] === nums[i - 1]) {
      ans.push(nums[i]);
    }
  }
  return ans;
}
```

## 索引排序

由于给定的元素在 `[1,n]` 的范围内，如果有重复元素，那么就表示有个别元素没有出现过。

我们可以将数组中的元素放在跟索引对应的位置上。由于数组的下标是 `[0,n-1]` ，所以我们要将数字 `i` 放到 `i-1` 的索引位置上。

最后遍历排序完的数组，如果遇到 值`i` 和 索引`i-1` 不相同的数字，则表示该数字出现了两次。

```cpp
class Solution
{
public:
    vector<int> findDuplicates(vector<int> &nums)
    {
        int n = nums.size();
        for (int i = 0; i < n; i++)
        {
            while (nums[i] != nums[nums[i] - 1])
            {
                swap(nums[i], nums[nums[i] - 1]);
            }
        }
        vector<int> ans;
        for (int i = 0; i < n; i++)
        {
            if (nums[i] - 1 != i)
                ans.push_back(nums[i]);
        }

        return ans;
    }
};
```

```python
class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        n = len(nums)
        for i in range(n):
            while (nums[i] != nums[nums[i] - 1]):
                nums[nums[i] - 1], nums[i] = nums[i], nums[nums[i] - 1]

        ans = [num for i, num in enumerate(nums) if (num != i + 1)]

        return ans
```
