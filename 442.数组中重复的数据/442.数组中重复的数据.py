#
# @lc app=leetcode.cn id=442 lang=python3
#
# [442] 数组中重复的数据
#

# @lc code=start
class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        n = len(nums)
        for i in range(n):
            while (nums[i] != nums[nums[i] - 1]):
                nums[nums[i] - 1], nums[i] = nums[i], nums[nums[i] - 1]

        ans = [num for i, num in enumerate(nums) if (num != i + 1)]

        return ans

# @lc code=end
