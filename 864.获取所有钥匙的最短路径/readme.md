# 题目

给定一个二维网格 `grid` ，其中：

- `'.'` 代表一个空房间
- `'#'` 代表一堵
- `'@'` 是起点
- 小写字母代表钥匙
- 大写字母代表锁

我们从起点开始出发，一次移动是指向四个基本方向之一行走一个单位空间。我们不能在网格外面行走，也无法穿过一堵墙。如果途经一个钥匙，我们就把它捡起来。除非我们手里有对应的钥匙，否则无法通过锁。

假设 `k` 为 **钥匙/锁** 的个数，且满足 `1 <= k <= 6`，字母表中的前 `k` 个字母在网格中都有自己对应的一个小写和一个大写字母。换言之，每个锁有唯一对应的钥匙，每个钥匙也有唯一对应的锁。另外，代表钥匙和锁的字母互为大小写并按字母顺序排列。

返回获取所有钥匙所需要的移动的最少次数。如果无法获取所有钥匙，返回 `-1` 。

# 示例

[![bWi9TU.png](https://s1.ax1x.com/2022/03/09/bWi9TU.png)](https://imgtu.com/i/bWi9TU)

```
输入：grid = ["@.a.#","###.#","b.A.B"]
输出：8
解释：目标是获得所有钥匙，而不是打开所有锁。
```

[![bWi3pd.png](https://s1.ax1x.com/2022/03/09/bWi3pd.png)](https://imgtu.com/i/bWi3pd)

```
输入：grid = ["@..aA","..B#.","....b"]
输出：6
```

# 题解

## 广度优先搜索

目标的可达性问题，可以使用广度优先搜索来解决。

题意是获取到所有的钥匙，获取钥匙的状态使用二进制位来表示，题目给出最多 `6` 把钥匙，定义 `6` 个二进制位，每个二进制位表示一个钥匙获取状态。通过判断钥匙的获取状态的二进制是否全部为 `1`，来终止搜索。

这里和普通的广度优先搜索不同的是，每个单元格特定的条件下可以重复被访问。这里通过设置每个单元格的状态，来确定是否被访问过。比如：`visited[1][1][1]`，表示单元格`(1,1)`，在获得 `a` 钥匙的情况下是否被访问过。

```ts
interface Cell {
  x: number;
  y: number;
  step: number;
  status: number;
}

function shortestPathAllKeys(grid: string[]): number {
  const m = grid.length;
  const n = grid[0].length;
  const dirs: number[][] = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  const bitMap = [1, 2, 4, 8, 16, 32, 64];
  // 记录访问状态
  const visited = new Array(m)
    .fill(0)
    .map(() => new Array(n).fill(0).map(() => new Array(bitMap[6]).fill(0)));
  // 搜索队列
  const queue: Cell[] = [];
  let keyNumber = 0;
  // 初始化起点
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === '@') {
        queue.push({ x: i, y: j, step: 0, status: 0 });
        visited[i][j][0] = 1;
      } else if (grid[i][j] >= 'a' && grid[i][j] <= 'f') {
        keyNumber++;
      }
    }
  }
  // 搜索
  while (queue.length) {
    const curr = queue.shift();

    // 所有钥匙都获得
    if (curr.status === bitMap[keyNumber] - 1) {
      return curr.step;
    }
    // 扩展下一个状态
    for (const [dx, dy] of dirs) {
      const x = curr.x + dx;
      const y = curr.y + dy;
      if (x < 0 || x >= m) continue;
      if (y < 0 || y >= n) continue;
      if (grid[x][y] === '#') continue;
      if (visited[x][y][curr.status] === 1) continue;
      if (grid[x][y] === '.' || grid[x][y] === '@') {
        visited[x][y][curr.status] = 1;
        queue.push({ x, y, step: curr.step + 1, status: curr.status });
      } else if (grid[x][y] >= 'a' && grid[x][y] <= 'f') {
        visited[x][y][curr.status] = 1;
        // 获得钥匙后的状态也要记录
        const status = curr.status | bitMap[grid[x][y].charCodeAt(0) - 97];
        visited[x][y][status] = 1;
        queue.push({ x, y, step: curr.step + 1, status });
      } else if (
        grid[x][y] >= 'A' &&
        grid[x][y] <= 'F' &&
        curr.status & bitMap[grid[x][y].charCodeAt(0) - 65]
      ) {
        visited[x][y][curr.status] = 1;
        queue.push({ x, y, step: curr.step + 1, status: curr.status });
      }
    }
  }

  return -1;
}
```

```cpp
class Solution
{
public:
    struct node
    {
        int x, y, step, status;
    };

    int n, m, cnt, mark[35][35][256] = {0};
    int dir[4][2] = {0, 1, 1, 0, 0, -1, -1, 0};
    int b2[10] = {1, 2, 4, 8, 16, 32, 64, 128, 256};

    int shortestPathAllKeys(vector<string> &mmap)
    {
        n = mmap.size(), m = mmap[0].size();
        queue<node> que;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (mmap[i][j] == '@')
                {
                    mmap[i][j] = '.';
                    que.push((node){i, j, 0, 0});
                    mark[i][j][0] = 1;
                }
                else if (mmap[i][j] >= 'a' && mmap[i][j] <= 'z')
                {
                    cnt++;
                }
            }
        }
        while (!que.empty())
        {
            node temp = que.front();
            que.pop();
            if (temp.status == b2[cnt] - 1)
            {
                return temp.step;
            }
            for (int i = 0; i < 4; i++)
            {
                int x = temp.x + dir[i][0];
                int y = temp.y + dir[i][1];
                if (x < 0 || y < 0 || x == n || y == m || mark[x][y][temp.status])
                {
                    continue;
                }
                if (mmap[x][y] == '.')
                {
                    mark[x][y][temp.status] = 1;
                    que.push((node){x, y, temp.step + 1, temp.status});
                }
                else if (mmap[x][y] >= 'a' && mmap[x][y] <= 'z')
                {
                    mark[x][y][temp.status] = 1;
                    mark[x][y][temp.status | b2[mmap[x][y] - 'a']] = 1;
                    que.push((node){x, y, temp.step + 1, temp.status | b2[mmap[x][y] - 'a']});
                }
                else if (mmap[x][y] >= 'A' && mmap[x][y] <= 'Z' && (temp.status & b2[mmap[x][y] - 'A']))
                {
                    mark[x][y][temp.status] = 1;
                    que.push((node){x, y, temp.step + 1, temp.status});
                }
            }
        }
        return -1;
    }
};
```
