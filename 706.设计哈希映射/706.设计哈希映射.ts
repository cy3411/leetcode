/*
 * @lc app=leetcode.cn id=706 lang=typescript
 *
 * [706] 设计哈希映射
 */

// @lc code=start

class LinkNode {
  key: number | null;
  value: number | null;
  next: LinkNode | null;
  prev: LinkNode | null;
  constructor(
    key: number = null,
    value: number = null,
    next: LinkNode = null,
    prev: LinkNode = null
  ) {
    this.key = key;
    this.value = value;
    this.next = next;
    this.prev = prev;
  }
  remove() {
    this.prev.next = this.next;
    this.next.prev = this.prev;
    this.next = this.prev = null;
  }

  insert(node: LinkNode) {
    const p = this.next;
    node.prev = this;
    node.next = p;
    this.next = node;
    p.prev = node;
  }
}

class HashLink {
  head: LinkNode;
  tail: LinkNode;
  constructor() {
    this.head = new LinkNode();
    this.tail = new LinkNode();
    this.head.next = this.tail;
    this.tail.prev = this.head;
  }

  add(key: number, value: number): number {
    if (this.contains(key)) {
      let p = this.head;
      while (p) {
        if (p.key === key) {
          p.value = value;
          return 0;
        }
        p = p.next;
      }
    }
    const node = new LinkNode(key, value);
    this.head.insert(node);
    return 1;
  }

  get(key: number): number {
    if (this.contains(key)) {
      let p = this.head;
      while (p) {
        if (p.key === key) return p.value;
        p = p.next;
      }
    }
    return -1;
  }

  remove(key: number): void {
    let p = this.head;
    while (p) {
      if (p.key === key) {
        p.remove();
      }
      p = p.next;
    }
  }

  contains(key: number): boolean {
    let p = this.head;
    while (p) {
      if (p.key === key) {
        return true;
      }
      p = p.next;
    }
    return false;
  }
}

class MyHashMap {
  count: number;
  capcicy: number;
  data: HashLink[];
  constructor() {
    this.capcicy = 100;
    this.count = 0;
    this.data = new Array(this.capcicy).fill(new HashLink());
  }

  private hash(key: number): number {
    return key % this.capcicy;
  }

  put(key: number, value: number): void {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    this.count += hashlink.add(key, value);
  }

  get(key: number): number {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    return hashlink.get(key);
  }

  remove(key: number): void {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    hashlink.remove(key);
    this.count--;
  }
}

/**
 * Your MyHashMap object will be instantiated and called as such:
 * var obj = new MyHashMap()
 * obj.put(key,value)
 * var param_2 = obj.get(key)
 * obj.remove(key)
 */
// @lc code=end
