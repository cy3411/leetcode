# 描述

不使用任何内建的哈希表库设计一个哈希映射（HashMap）。

实现 MyHashMap 类：

- MyHashMap() 用空映射初始化对象
- void put(int key, int value) 向 HashMap 插入一个键值对 (key, value) 。如果 key 已经存在于映射中，则更新其对应的值 value 。
- int get(int key) 返回特定的 key 所映射的 value ；如果映射中不包含 key 的映射，返回 -1 。
- void remove(key) 如果映射中存在 key 的映射，则移除 key 和它所对应的 value 。

# 示例

```
输入：
["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
[[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
输出：
[null, null, null, 1, -1, null, 1, null, -1]

解释：
MyHashMap myHashMap = new MyHashMap();
myHashMap.put(1, 1); // myHashMap 现在为 [[1,1]]
myHashMap.put(2, 2); // myHashMap 现在为 [[1,1], [2,2]]
myHashMap.get(1);    // 返回 1 ，myHashMap 现在为 [[1,1], [2,2]]
myHashMap.get(3);    // 返回 -1（未找到），myHashMap 现在为 [[1,1], [2,2]]
myHashMap.put(2, 1); // myHashMap 现在为 [[1,1], [2,1]]（更新已有的值）
myHashMap.get(2);    // 返回 1 ，myHashMap 现在为 [[1,1], [2,1]]
myHashMap.remove(2); // 删除键为 2 的数据，myHashMap 现在为 [[1,1]]
myHashMap.get(2);    // 返回 -1（未找到），myHashMap 现在为 [[1,1]]
```

# 方法

## 拉链法

选择一个合适质数作为数组的长度，重复的 hash 放入对应的 hash 下标的数组中（也可以用链表实现）

```js
var MyHashMap = function () {
  this.capcity = 997;
  this.size = 0;
  this.map = new Array(this.capcity).fill(0).map((_) => new Array());
  this.hash = (key) => {
    return key % this.capcity;
  };
};

/**
 * value will always be non-negative.
 * @param {number} key
 * @param {number} value
 * @return {void}
 */
MyHashMap.prototype.put = function (key, value) {
  let keys = this.map[this.hash(key)];

  for (let i = 0; i < keys.length; i++) {
    // 更新
    if (keys[i][0] === key) {
      keys[i][1] = value;
      return;
    }
  }

  this.map[this.hash(key)].push([key, value]);
  this.size++;
};

/**
 * Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
 * @param {number} key
 * @return {number}
 */
MyHashMap.prototype.get = function (key) {
  let keys = this.map[this.hash(key)];

  for (let i = 0; i < keys.length; i++) {
    if (keys[i][0] === key) {
      return keys[i][1];
    }
  }

  return -1;
};

/**
 * Removes the mapping of the specified value key if this map contains a mapping for the key
 * @param {number} key
 * @return {void}
 */
MyHashMap.prototype.remove = function (key) {
  let keys = this.map[this.hash(key)];

  for (let i = 0; i < keys.length; i++) {
    if (keys[i][0] === key) {
      keys.splice(i, 1);
      this.size--;
    }
  }
};
```

## 链表实现拉链法

```ts
class LinkNode {
  key: number | null;
  value: number | null;
  next: LinkNode | null;
  prev: LinkNode | null;
  constructor(
    key: number = null,
    value: number = null,
    next: LinkNode = null,
    prev: LinkNode = null
  ) {
    this.key = key;
    this.value = value;
    this.next = next;
    this.prev = prev;
  }
  remove() {
    this.prev.next = this.next;
    this.next.prev = this.prev;
    this.next = this.prev = null;
  }

  insert(node: LinkNode) {
    const p = this.next;
    node.prev = this;
    node.next = p;
    this.next = node;
    p.prev = node;
  }
}

class HashLink {
  head: LinkNode;
  tail: LinkNode;
  constructor() {
    this.head = new LinkNode();
    this.tail = new LinkNode();
    this.head.next = this.tail;
    this.tail.prev = this.head;
  }

  add(key: number, value: number): number {
    if (this.contains(key)) {
      let p = this.head;
      while (p) {
        if (p.key === key) {
          p.value = value;
          return 0;
        }
        p = p.next;
      }
    }
    const node = new LinkNode(key, value);
    this.head.insert(node);
    return 1;
  }

  get(key: number): number {
    if (this.contains(key)) {
      let p = this.head;
      while (p) {
        if (p.key === key) return p.value;
        p = p.next;
      }
    }
    return -1;
  }

  remove(key: number): void {
    let p = this.head;
    while (p) {
      if (p.key === key) {
        p.remove();
      }
      p = p.next;
    }
  }

  contains(key: number): boolean {
    let p = this.head;
    while (p) {
      if (p.key === key) {
        return true;
      }
      p = p.next;
    }
    return false;
  }
}

class MyHashMap {
  count: number;
  capcicy: number;
  data: HashLink[];
  constructor() {
    this.capcicy = 100;
    this.count = 0;
    this.data = new Array(this.capcicy).fill(new HashLink());
  }

  private hash(key: number): number {
    return key % this.capcicy;
  }

  put(key: number, value: number): void {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    this.count += hashlink.add(key, value);
  }

  get(key: number): number {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    return hashlink.get(key);
  }

  remove(key: number): void {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    hashlink.remove(key);
    this.count--;
  }
}
```

## 线性碰撞

详情见[ 705.设计哈希集合 ](https://gitee.com/cy3411/leecode/tree/master/705.%E8%AE%BE%E8%AE%A1%E5%93%88%E5%B8%8C%E9%9B%86%E5%90%88)
