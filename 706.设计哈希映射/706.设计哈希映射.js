/*
 * @lc app=leetcode.cn id=706 lang=javascript
 *
 * [706] 设计哈希映射
 */

// @lc code=start
/**
 * Initialize your data structure here.
 */
var MyHashMap = function () {
  this.capcity = 997;
  this.size = 0;
  this.map = new Array(this.capcity).fill(0).map((_) => new Array());
  this.hash = (key) => {
    return key % this.capcity;
  };
};

/**
 * value will always be non-negative.
 * @param {number} key
 * @param {number} value
 * @return {void}
 */
MyHashMap.prototype.put = function (key, value) {
  let keys = this.map[this.hash(key)];

  for (let i = 0; i < keys.length; i++) {
    // 更新
    if (keys[i][0] === key) {
      keys[i][1] = value;
      return;
    }
  }

  this.map[this.hash(key)].push([key, value]);
  this.size++;
};

/**
 * Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
 * @param {number} key
 * @return {number}
 */
MyHashMap.prototype.get = function (key) {
  let keys = this.map[this.hash(key)];

  for (let i = 0; i < keys.length; i++) {
    if (keys[i][0] === key) {
      return keys[i][1];
    }
  }

  return -1;
};

/**
 * Removes the mapping of the specified value key if this map contains a mapping for the key
 * @param {number} key
 * @return {void}
 */
MyHashMap.prototype.remove = function (key) {
  let keys = this.map[this.hash(key)];

  for (let i = 0; i < keys.length; i++) {
    if (keys[i][0] === key) {
      keys.splice(i, 1);
      this.size--;
    }
  }
};

/**
 * Your MyHashMap object will be instantiated and called as such:
 * var obj = new MyHashMap()
 * obj.put(key,value)
 * var param_2 = obj.get(key)
 * obj.remove(key)
 */
// @lc code=end
