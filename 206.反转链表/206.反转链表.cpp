/*
 * @lc app=leetcode.cn id=206 lang=cpp
 *
 * [206] 反转链表
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution
{
public:
    ListNode *getReverse(ListNode *head)
    {
        if (head == NULL || head->next == NULL)
        {
            return head;
        }
        // 递归找到最后一个节点当作反转后的头节点
        ListNode *tail = head->next;
        ListNode *last = getReverse(head->next);
        tail->next = head;
        head->next = NULL;
        return last;
    }
    ListNode *reverseList(ListNode *head)
    {
        return getReverse(head);
    }
};
// @lc code=end
