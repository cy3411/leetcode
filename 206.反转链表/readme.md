# 题目

给你单链表的头节点 `head` ，请你反转链表，并返回反转后的链表。

提示：

- 链表中节点的数目范围是 `[0, 5000]`
- $\color{burlywood} -5000 \leq Node.val \leq 5000$

进阶：链表可以选用迭代或递归方式完成反转。你能否用两种方法解决这道题？

# 示例

[![HWm8c6.png](https://s4.ax1x.com/2022/02/15/HWm8c6.png)](https://imgtu.com/i/HWm8c6)

```
输入：head = [1,2,3,4,5]
输出：[5,4,3,2,1]
```

[![HWmdNd.png](https://s4.ax1x.com/2022/02/15/HWmdNd.png)](https://imgtu.com/i/HWmdNd)

```
输入：head = [1,2]
输出：[2,1]
```

# 题解

## 迭代

令 p 指向 head，令 ans 做为反转链表的头节点。

当 p 不为空的时候，将 p 的下一个指针赋给一个临时变量，然后改变 p 的 next 指针，指向 ans，然后将 ans 指向 p，然后将 p 指向临时变量。

最后 ans 就是反转链表的头节点。

```ts
function reverseList(head: ListNode | null): ListNode | null {
  let ans = null;
  let p = head;
  let q = null;
  while (p !== null) {
    q = p.next;
    p.next = ans;
    ans = p;
    p = q;
  }
  return ans;
}
```

## 递归

递归找到最后一个节点 `last` 当作反转后的头节点，然后在回溯的过程中将当前节点和下一个节点反转，当前节点 next 指向 null。返回 `last` 即可。

```cpp
class Solution
{
public:
    ListNode *getReverse(ListNode *head)
    {
        if (head->next == NULL)
        {
            return head;
        }
        // 递归找到最后一个节点当作反转后的头节点
        ListNode *last = getReverse(head->next);
        // 将当前节点和后一个节点反转
        head->next->next = head;
        // 将当前节点的next指向NULL
        head->next = NULL;
        return last;
    }
    ListNode *reverseList(ListNode *head)
    {
        return getReverse(head);
    }
};
```

```cpp
struct ListNode *reverseList(struct ListNode *head)
{
    if (head == NULL || head->next == NULL)
    {
        return head;
    }
    struct ListNode *p = reverseList(head->next);
    head->next->next = head;
    head->next = NULL;
    return p;
}
```
