# 描述
给你一个链表，每 k 个节点一组进行翻转，请你返回翻转后的链表。

k 是一个正整数，它的值小于或等于链表的长度。

如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。

进阶：

+ 你可以设计一个只使用常数额外空间的算法来解决此问题吗？
+ 你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。


# 示例

```
示例 1：

输入：head = [1,2,3,4,5], k = 2
输出：[2,1,4,3,5]
```

# 方法
先实现前k个节点反转，然后按序反转多次k个节点，直到剩下的节点数量少于k即可。

```javascript
var reverseKGroup = function (head, k) {
  if (head === null || head.next === null || k < 2) {
    return head;
  }

  // 反转节点
  const reverseNode = (head, n) => {
    // 判断剩余节点是否小于k
    let cur = head;
    let cnt = n;
    while (--cnt && cur) {
      cur = cur.next;
    }
    if (cur === null) {
      return head;
    }

    // 递归到这里结束
    if (n === 1) {
      return head;
    }
    let tail = head.next;
    let p = reverseNode(head.next, n - 1);
    head.next = tail.next;
    tail.next = head;
    return p;
  };

  const dummp = new ListNode(0, head);
  let pre = dummp;

  while (true) {
    pre.next = reverseNode(pre.next, k);
    // 每次往后走k个节点，pre要保留是反转节点的前一个节点
    for (let i = 0; i < k && pre; i++) {
      pre = pre.next;
    }
    if (!pre) break;
  }

  return dummp.next;
};
```