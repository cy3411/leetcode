# 题目

给你一个整数数组 nums ，其中元素已经按 升序 排列，请你将其转换为一棵 高度平衡 二叉搜索树。

高度平衡 二叉树是一棵满足「每个节点的左右两个子树的高度差的绝对值不超过 1 」的二叉树。

提示：

- 1 <= nums.length <= 104
- -104 <= nums[i] <= 104
- nums 按 严格递增 顺序排列

# 示例

[![f89e7q.png](https://z3.ax1x.com/2021/08/09/f89e7q.png)](https://imgtu.com/i/f89e7q)

```
输入：nums = [-10,-3,0,5,9]
输出：[0,-3,9,-10,null,5]
解释：[0,-10,5,null,-3,null,9] 也将被视为正确答案：
```

# 题解

## 递归

使用中间值创建根节点，递归处理左右子树的创建。

```ts
function build(nums: number[], l: number, r: number): TreeNode | null {
  if (l > r) return null;
  const mid = (l + r) >> 1;
  // 用中间值创建root
  const root = new TreeNode(nums[mid]);
  // 递归处理左右子树
  root.left = build(nums, l, mid - 1);
  root.right = build(nums, mid + 1, r);
  return root;
}

function sortedArrayToBST(nums: number[]): TreeNode | null {
  return build(nums, 0, nums.length - 1);
}
```
