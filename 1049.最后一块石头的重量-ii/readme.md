# 题目

有一堆石头，用整数数组 `stones` 表示。其中 `stones[i]` 表示第 `i` 块石头的重量。

每一回合，从中选出任意两块石头，然后将它们一起粉碎。假设石头的重量分别为 `x` 和 `y`，且 `x <= y`。那么粉碎的可能结果如下：

- 如果 `x == y`，那么两块石头都会被完全粉碎；
- 如果 `x != y`，那么重量为 `x` 的石头将会完全粉碎，而重量为 `y` 的石头新重量为 `y-x`。

最后，最多只会剩下一块 石头。返回此石头 **最小的可能重量** 。如果没有石头剩下，就返回 `0`。

# 示例

```
输入：stones = [2,7,4,1,8,1]
输出：1
解释：
组合 2 和 4，得到 2，所以数组转化为 [2,7,1,8,1]，
组合 7 和 8，得到 1，所以数组转化为 [2,1,1,1]，
组合 2 和 1，得到 1，所以数组转化为 [1,1,1]，
组合 1 和 1，得到 0，所以数组转化为 [1]，这就是最优值。
```

# 题解

## 动态规划

题意就是将数组中的值两两互减，使得最后的值最小。

我们将数组中的元素划分为 2 份，就是求 2 份最小的差值即可。

这样问题就转化成，从数组中选择元素，能够找到最接近数组元素和的一半。

**状态**
定义 `dp[i][j]`，表示前 `i` 个石头，凑成总和不超过 `j` 的最大数。

**basecase**
`dp[0][j]` 都为 0，前 0 个石头，无法满足条件。

**选择**
每个石头都有选择或者不选择两种状态

$$
dp[i][j] = max(dp[i-1][j], dp[i-1][j-stones[i]]+stones[i])
$$

```ts
function lastStoneWeightII(stones: number[]): number {
  const n = stones.length;

  let sum = 0;
  for (const stone of stones) {
    sum += stone;
  }

  // 我们需要找的就是最接近数组元素总和一半的值
  let target = (sum / 2) >> 0;
  const dp: number[][] = new Array(n + 1).fill(0).map(() => new Array(target + 1).fill(0));
  for (let i = 1; i <= n; i++) {
    const stone = stones[i - 1];
    for (let j = 0; j <= target; j++) {
      dp[i][j] = dp[i - 1][j];
      if (j >= stone) {
        dp[i][j] = Math.max(dp[i][j], dp[i - 1][j - stone] + stone);
      }
    }
  }
  return sum - dp[n][target] * 2;
}
```
