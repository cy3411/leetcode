/*
 * @lc app=leetcode.cn id=1089 lang=typescript
 *
 * [1089] 复写零
 */

// @lc code=start
/**
 Do not return anything, modify arr in-place instead.
 */
function duplicateZeros(arr: number[]): void {
  const n = arr.length;
  let i = -1;
  // 复写后的数组长度
  let top = 0;
  // 找到 i 在原数组最后的位置
  while (top < n) {
    i++;
    if (arr[i] !== 0) {
      top++;
    } else {
      top += 2;
    }
  }
  // 如果i最后一位是0，超出了原数组范围
  let j = n - 1;
  if (top === n + 1) {
    arr[j] = 0;
    j--;
    i--;
  }
  // 开始复写
  while (j >= 0) {
    arr[j] = arr[i];
    j--;
    if (arr[i] === 0) {
      arr[j] = arr[i];
      j--;
    }
    i--;
  }
}
// @lc code=end
