# 题目

给你一个长度固定的整数数组 `arr` ，请你将该数组中出现的每个零都复写一遍，并将其余的元素向右平移。

注意：请不要在超过该数组长度的位置写入元素。

要求：请对输入的数组 **就地** 进行上述修改，不要从函数返回任何东西。

提示：

- $1 \leq arr.length \leq 10000$
- $0 \leq arr[i] \leq 9$

# 示例

```
输入：[1,0,2,3,0,4,5,0]
输出：null
解释：调用函数后，输入的数组将被修改为：[1,0,0,2,3,0,0,4]
```

```
输入：[1,2,3]
输出：null
解释：调用函数后，输入的数组将被修改为：[1,2,3]
```

# 题解

## 双指针

定义双指针 i 和 j ， i 为原数组中最多可以复写到位置， j 为数组末尾。

然后模拟复写，将 i 的元素复写到 j ，如果是 0 的话就多复写一次，同时将 i 和 j 左移。

```ts
function duplicateZeros(arr: number[]): void {
  const n = arr.length;
  let i = -1;
  // 复写后的数组长度
  let top = 0;
  // 找到 i 在原数组最后的位置
  while (top < n) {
    i++;
    if (arr[i] !== 0) {
      top++;
    } else {
      top += 2;
    }
  }
  // 如果i最后一位是0，超出了原数组范围
  let j = n - 1;
  if (top === n + 1) {
    arr[j] = 0;
    j--;
    i--;
  }
  // 开始复写
  while (j >= 0) {
    arr[j] = arr[i];
    j--;
    if (arr[i] === 0) {
      arr[j] = arr[i];
      j--;
    }
    i--;
  }
}
```

```cpp
class Solution {
public:
    void duplicateZeros(vector<int> &arr) {
        int n = arr.size(), i = -1, top = 0;
        while (top < n) {
            i++;
            if (arr[i] == 0)
                top += 2;
            else
                top++;
        }
        int j = n - 1;
        if (top == n + 1) {
            arr[j] = 0;
            i--, j--;
        }
        while (j >= 0) {
            arr[j] = arr[i];
            j--;
            if (arr[i] == 0) {
                arr[j] = arr[i];
                j--;
            }
            i--;
        }
    }
};
```

## API

利用 API 将数组复写。

如果当前元素为 0 ，就在当前位置后面插入 0， 并将数组最后一个元素弹出

```py
class Solution:
    def duplicateZeros(self, arr: List[int]) -> None:
        """
        Do not return anything, modify arr in-place instead.
        """
        n = len(arr)
        i = 0
        while i < n:
            if arr[i] == 0:
                arr.insert(i, 0)
                arr.pop()
                i += 2
            else:
                i += 1
```
