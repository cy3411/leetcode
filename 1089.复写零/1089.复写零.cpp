/*
 * @lc app=leetcode.cn id=1089 lang=cpp
 *
 * [1089] 复写零
 */

// @lc code=start
class Solution {
public:
    void duplicateZeros(vector<int> &arr) {
        int n = arr.size(), i = -1, top = 0;
        while (top < n) {
            i++;
            if (arr[i] == 0)
                top += 2;
            else
                top++;
        }
        int j = n - 1;
        if (top == n + 1) {
            arr[j] = 0;
            i--, j--;
        }
        while (j >= 0) {
            arr[j] = arr[i];
            j--;
            if (arr[i] == 0) {
                arr[j] = arr[i];
                j--;
            }
            i--;
        }
    }
};
// @lc code=end
