# 题目
给你一个链表的头节点 head ，旋转链表，将链表每个节点向右移动 k 个位置。

提示：

+ 链表中节点的数目在范围 [0, 500] 内
+ -100 <= Node.val <= 100
+ 0 <= k <= 2 * 109

# 示例
```
输入：head = [1,2,3,4,5], k = 2
输出：[4,5,1,2,3]
```

```
输入：head = [0,1,2], k = 4
输出：[2,0,1]
```
# 方法
先求出链表的长度，方便寻找新链表头节点的位置。

将链表首尾相连，形成环形链表。

找到新的链表头，新链表头之前的链表断开。

```js
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var rotateRight = function (head, k) {
  if (head === null) return head;
  // 获取链表长度
  const getLinkedLength = (head) => {
    let size = 1;
    let temp = head;
    while (temp.next) {
      temp = temp.next;
      size++;
    }
    return size;
  };

  let pre = head;
  let size = getLinkedLength(head);

  // 找到链表尾节点
  while (pre.next) {
    pre = pre.next;
  }
  // 形成环形链表
  pre.next = head;

  // 找到新的链表头的上一个节点 size-k
  // 取模是为了处理k大于size的情况
  let i = 1;
  while (i++ < size - (k % size)) {
    head = head.next;
  }
  // pre指向新的链表头
  pre = head.next;
  // 将环断开
  head.next = null;

  return pre;
};
```