# 题目
给你一个嵌套的整型列表。请你设计一个迭代器，使其能够遍历这个整型列表中的所有整数。

列表中的每一项或者为一个整数，或者是另一个列表。其中列表的元素也可能是整数或是其他列表。


# 示例
```
输入: [[1,1],2,[1,1]]
输出: [1,1,2,1,1]
解释: 通过重复调用 next 直到 hasNext 返回 false，next 返回的元素的顺序应该是: [1,1,2,1,1]。
```

# 方法
这是一个很标准的树形结构，叶子节点对应一个整数，非叶子节点对应一个嵌套列表。

我们可以深度优先搜索该树，将结果存入数组，然后遍历该数组实现hasNext和next方法。

```js
/**
 * @constructor
 * @param {NestedInteger[]} nestedList
 */
var NestedIterator = function (nestedList) {
  const flatDeep = (arr, list = []) => {
    for (const nest of arr) {
      if (nest.isInteger()) {
        list.push(nest.getInteger());
      } else {
        flatDeep(nest.getList(), list);
      }
    }
    return list;
  };
  this.head = 0;
  this.list = flatDeep(nestedList);
};

/**
 * @this NestedIterator
 * @returns {boolean}
 */
NestedIterator.prototype.hasNext = function () {
  return this.list[this.head] !== void 0;
};

/**
 * @this NestedIterator
 * @returns {integer}
 */
NestedIterator.prototype.next = function () {
  return this.list[this.head++];
};
```

```js
/**
 * @constructor
 * @param {NestedInteger[]} nestedList
 */
var NestedIterator = function (nestedList) {
  // js api
  const flatDeep = (arr, d = 1) => {
    return d > 0
      ? arr.reduce(
          (acc, val) =>
            acc.concat(val.isInteger() ? val.getInteger() : flatDeep(val.getList(), d - 1)),
          []
        )
      : arr.slice();
  };
  this.head = 0;
  this.list = flatDeep(nestedList, Infinity);
};

/**
 * @this NestedIterator
 * @returns {boolean}
 */
NestedIterator.prototype.hasNext = function () {
  return this.list[this.head] !== void 0;
};

/**
 * @this NestedIterator
 * @returns {integer}
 */
NestedIterator.prototype.next = function () {
  return this.list[this.head++];
};
```