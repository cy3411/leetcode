# 题目
给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。

提示：

+ 1 <= nums.length <= 10
+ -10 <= nums[i] <= 10

# 示例
```
输入：nums = [1,2,2]
输出：[[],[1],[1,2],[1,2,2],[2],[2,2]]
```

```
输入：nums = [0]
输出：[[],[0]]
```

# 方法
## 回朔
这个题目就是[78. 子集](https://leetcode-cn.com/problems/subsets/)的变种，就是多了重复的元素。

一般看到【所有可能的结果】，就要暴力搜索所有可能解，我们就用到了回溯。

直接套模板即可。
```js
//模板
const result = []
function backtrack(路径，选择列表) {
    if(满足结束条件){
        result.push(路径)
        return
    }
    for(选择 of 选择列表) {
        选择
        backtrack(路径，选择列表)
        取消选择
    }
}
```

```js
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsetsWithDup = function (nums) {
  // 不能包含重复的子集，需要先排序。
  // 类似122和212属于相同子集
  nums.sort((a, b) => a - b);
  const result = [];
  // 保存已经获得的子集
  const memo = new Map();
  /**
   * @description:
   * @param {[number]} nums
   * @param {number} start
   * @param {[number]} track
   * @return {void}
   */
  const backtrack = (nums, start, track) => {
    if (memo.has(track.toString())) {
      return;
    }
    // 这里需要push的track的拷贝
    result.push([...track]);
    memo.set(track.toString(), 1);
    for (let i = start; i < nums.length; i++) {
      // 选择
      track.push(nums[i]);
      // 回溯
      backtrack(nums, i + 1, track);
      // 撤销选择
      track.pop();
    }
  };
  backtrack(nums, 0, []);
  return result;
};
```