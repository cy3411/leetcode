/*
 * @lc app=leetcode.cn id=659 lang=javascript
 *
 * [659] 分割数组为连续子序列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var isPossible = function (nums) {
  const countMap = new Map();
  const endMap = new Map();
  // 统计数字出现次数
  for (let n of nums) {
    countMap.set(n, (countMap.get(n) || 0) + 1);
  }

  for (let n of nums) {
    const count = countMap.get(n) || 0;
    if (count) {
      // n-1的序列数
      const prevEnd = endMap.get(n - 1) || 0;
      if (prevEnd > 0) {
        // 继续添加序列
        endMap.set(n - 1, prevEnd - 1);
        endMap.set(n, (endMap.get(n) || 0) + 1);
        countMap.set(n, count - 1);
      } else {
        // 新增序列
        let n1 = countMap.get(n + 1);
        let n2 = countMap.get(n + 2);
        if (n1 && n2) {
          endMap.set(n + 2, (endMap.get(n + 2) || 0) + 1);
          countMap.set(n, count - 1);
          countMap.set(n + 1, n1 - 1);
          countMap.set(n + 2, n2 - 1);
        } else {
          // 无法组成最小长度为3的序列
          return false;
        }
      }
    }
  }

  return true;
};
// @lc code=end
