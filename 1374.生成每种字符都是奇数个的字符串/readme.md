# 题目

给你一个整数 `n`，请你返回一个含 `n` 个字符的字符串，其中每种字符在该字符串中都恰好出现 **奇数次** 。

返回的字符串必须只含小写英文字母。如果存在多个满足题目要求的字符串，则返回其中任意一个即可。

# 示例

```
输入：n = 4
输出："pppz"
解释："pppz" 是一个满足题目要求的字符串，因为 'p' 出现 3 次，且 'z' 出现 1 次。当然，还有很多其他字符串也满足题目要求，比如："ohhh" 和 "love"。
```

```
输入：n = 2
输出："xy"
解释："xy" 是一个满足题目要求的字符串，因为 'x' 和 'y' 各出现 1 次。当然，还有很多其他字符串也满足题目要求，比如："ag" 和 "ur"。
```

# 题解

## 贪心

判断 n 的奇偶性，如果是奇数，返回 n 个相同字符即可，如果是偶数，返回 n - 1 个相同字符，再加上 1 个其他字符。

```ts
function generateTheString(n: number): string {
  return n % 2 === 0 ? 'a'.repeat(n - 1) + 'b' : 'a'.repeat(n);
}
```

```cpp
class Solution {
public:
    string generateTheString(int n) {
        string ans;
        if (n % 2 == 0) {
            ans = string(n - 1, 'a');
            ans += 'b';
        } else {
            ans = string(n, 'a');
        }

        return ans;
    }
};
```

```py
class Solution:
    def generateTheString(self, n: int) -> str:
        ans = ''
        if n % 2 == 0:
            ans = 'a' * (n - 1) + 'b'
        else:
            ans = 'a' * n
        return ans
```
