#
# @lc app=leetcode.cn id=1374 lang=python3
#
# [1374] 生成每种字符都是奇数个的字符串
#

# @lc code=start
class Solution:
    def generateTheString(self, n: int) -> str:
        ans = ''
        if n % 2 == 0:
            ans = 'a' * (n - 1) + 'b'
        else:
            ans = 'a' * n
        return ans
# @lc code=end
