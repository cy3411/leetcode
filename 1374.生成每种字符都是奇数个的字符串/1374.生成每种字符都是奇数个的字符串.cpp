/*
 * @lc app=leetcode.cn id=1374 lang=cpp
 *
 * [1374] 生成每种字符都是奇数个的字符串
 */

// @lc code=start
class Solution {
public:
    string generateTheString(int n) {
        string ans;
        if (n % 2 == 0) {
            ans = string(n - 1, 'a');
            ans += 'b';
        } else {
            ans = string(n, 'a');
        }

        return ans;
    }
};
// @lc code=end
