# 题目

给定一个段落 (paragraph) 和一个禁用单词列表 (banned)。返回出现次数最多，同时不在禁用列表中的单词。

题目保证至少有一个词不在禁用列表中，而且答案唯一。

禁用列表中的单词用小写字母表示，不含标点符号。段落中的单词不区分大小写。答案都是小写字母。

提示：

- $1 \leq 段落长度 \leq 1000$
- $0 \leq 禁用单词个数 \leq 100$
- $1 \leq 禁用单词长度 \leq 10$
- 答案是唯一的, 且都是小写字母 (即使在 `paragraph` 里是大写的，即使是一些特定的名词，答案都是小写的。)
- `paragraph` 只包含字母、空格和下列标点符号`!?',;`.
- 不存在没有连字符或者带有连字符的单词。
- 单词里只包含字母，不会出现省略号或者其他标点符号。

# 示例

```
输入:
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
banned = ["hit"]
输出: "ball"
解释:
"hit" 出现了3次，但它是一个禁用的单词。
"ball" 出现了2次 (同时没有其他单词出现2次)，所以它是段落里出现次数最多的，且不在禁用列表中的单词。
注意，所有这些单词在段落里不区分大小写，标点符号需要忽略（即使是紧挨着单词也忽略， 比如 "ball,"），
"hit"不是最终的答案，虽然它出现次数更多，但它在禁用单词列表中。
```

# 题解

## 哈希表

使用哈希表记录每个合法单词出现的次数，最后遍历哈希表，取出出现次数最多的单词。

```ts
function mostCommonWord(paragraph: string, banned: string[]): string {
  const isBanned = new Set(banned);
  const map = new Map<string, number>();

  const isLetter = (c: string) => {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
  };

  // 增加哨兵，方便处理
  paragraph += '!';
  const n = paragraph.length;

  let sb = '';
  for (let i = 0; i < n; i++) {
    // 如果是字母，则添加到sb中
    if (isLetter(paragraph[i])) {
      sb += paragraph[i].toLowerCase();
    } else {
      // 如果不是字母，开始处理sb
      if (sb.length > 0) {
        if (!isBanned.has(sb)) {
          map.set(sb, map.has(sb) ? map.get(sb)! + 1 : 1);
        }
        sb = '';
      }
    }
  }

  let ans = '';
  let max = 0;
  // 遍历map，找到出现频率最高的单词
  for (const [word, freq] of map) {
    if (freq > max) {
      max = freq;
      ans = word;
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int isLetter(char c)
    {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
    char toLower(char c)
    {
        if (c >= 'A' && c <= 'Z')
            return c - 'A' + 'a';
        return c;
    }
    string mostCommonWord(string paragraph, vector<string> &banned)
    {
        unordered_map<string, int> m;
        unordered_set<string> s;
        for (auto &b : banned)
        {
            s.insert(b);
        }

        paragraph += ' ';
        int n = paragraph.size();
        string sb;
        for (int i = 0; i < n; i++)
        {
            if (isLetter(paragraph[i]))
            {
                sb += toLower(paragraph[i]);
            }
            else
            {
                if (sb.size() && !s.count(sb))
                {
                    m[sb]++;
                }
                sb.clear();
            }
        }

        int max = 0;
        string ans;
        for (auto &p : m)
        {
            if (p.second > max)
            {
                max = p.second;
                ans = p.first;
            }
        }
        return ans;
    }
};
```
