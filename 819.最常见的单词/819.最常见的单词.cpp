/*
 * @lc app=leetcode.cn id=819 lang=cpp
 *
 * [819] 最常见的单词
 */

// @lc code=start
class Solution
{
public:
    int isLetter(char c)
    {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
    char toLower(char c)
    {
        if (c >= 'A' && c <= 'Z')
            return c - 'A' + 'a';
        return c;
    }
    string mostCommonWord(string paragraph, vector<string> &banned)
    {
        unordered_map<string, int> m;
        unordered_set<string> s;
        for (auto &b : banned)
        {
            s.insert(b);
        }

        paragraph += ' ';
        int n = paragraph.size();
        string sb;
        for (int i = 0; i < n; i++)
        {
            if (isLetter(paragraph[i]))
            {
                sb += toLower(paragraph[i]);
            }
            else
            {
                if (sb.size() && !s.count(sb))
                {
                    m[sb]++;
                }
                sb.clear();
            }
        }

        int max = 0;
        string ans;
        for (auto &p : m)
        {
            if (p.second > max)
            {
                max = p.second;
                ans = p.first;
            }
        }
        return ans;
    }
};
// @lc code=end
