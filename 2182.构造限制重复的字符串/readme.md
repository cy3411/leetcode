# 题目

给你一个字符串 `s` 和一个整数 `repeatLimit` ，用 `s` 中的字符构造一个新字符串 `repeatLimitedString` ，使任何字母 `连续` 出现的次数都不超过 `repeatLimit` 次。你不必使用 `s` 中的全部字符。

返回 **字典序最大的** `repeatLimitedStrin`g 。

如果在字符串 `a` 和 `b` 不同的第一个位置，字符串 `a` 中的字母在字母表中出现时间比字符串 `b` 对应的字母晚，则认为字符串 `a` 比字符串 `b` 字典序更大 。如果字符串中前 `min(a.length, b.length)` 个字符都相同，那么较长的字符串字典序更大。

提示：

- $1 \leq repeatLimit \leq s.length \leq 10^5$
- `s` 由小写英文字母组成

# 示例

```
输入：s = "cczazcc", repeatLimit = 3
输出："zzcccac"
解释：使用 s 中的所有字符来构造 repeatLimitedString "zzcccac"。
字母 'a' 连续出现至多 1 次。
字母 'c' 连续出现至多 3 次。
字母 'z' 连续出现至多 2 次。
因此，没有字母连续出现超过 repeatLimit 次，字符串是一个有效的 repeatLimitedString 。
该字符串是字典序最大的 repeatLimitedString ，所以返回 "zzcccac" 。
注意，尽管 "zzcccca" 字典序更大，但字母 'c' 连续出现超过 3 次，所以它不是一个有效的 repeatLimitedString 。
```

```
输入：s = "aababab", repeatLimit = 2
输出："bbabaa"
解释：
使用 s 中的一些字符来构造 repeatLimitedString "bbabaa"。
字母 'a' 连续出现至多 2 次。
字母 'b' 连续出现至多 2 次。
因此，没有字母连续出现超过 repeatLimit 次，字符串是一个有效的 repeatLimitedString 。
该字符串是字典序最大的 repeatLimitedString ，所以返回 "bbabaa" 。
注意，尽管 "bbabaaa" 字典序更大，但字母 'a' 连续出现超过 2 次，所以它不是一个有效的 repeatLimitedString 。
```

# 题解

## 模拟

想要得到最大的字典序的字符串，那么就需要将字典序最大的字符放在前面，如果连续次数超过了 `repeatLimit` ，那么就用字典序次大的字符来分隔这些连续字符。最后的结果一定是字典序最大的字符串。

```ts
function repeatLimitedString(s: string, repeatLimit: number): string {
  // 将字符串逆序排序
  const sArr = s.split('').sort((a, b) => b.charCodeAt(0) - a.charCodeAt(0));
  // 存储连续相同的字符串
  let temp = '';
  let ans = '';
  let i = 0;
  // 遍历字符串
  block: while (i < sArr.length) {
    temp = '';
    temp += sArr[i];
    i++;
    // 将连续相同的字符串存储到temp
    while (sArr[i] === temp[0]) {
      temp += sArr[i];
      i++;
    }

    // 处理temp，将合法字串拼接到ans
    let j = 0;
    while (temp.length - j > repeatLimit) {
      ans += temp.substring(j, j + repeatLimit);
      // 已经到达字符串末尾，跳出循环
      if (i === sArr.length) break block;
      // ans后面添加一个其他字符
      ans += sArr[i];
      i++;
      // j 向后移动
      j += repeatLimit;
    }
    // 将剩余的temp拼接到ans
    if (j < temp.length) {
      ans += temp.substring(j);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    string repeatLimitedString(string s, int repeatLimit)
    {
        sort(s.begin(), s.end(), [](char a, char b)
             { return a > b; });

        string ans = "", temp;
        int i = 0, n = s.size();
        while (i < n)
        {
            temp = "";
            temp += s[i], i++;
            while (temp[0] == s[i])
            {
                temp += s[i], i++;
            }

            int j = 0;
            while (temp.size() - j > repeatLimit)
            {
                ans += temp.substr(j, repeatLimit);
                if (i >= n)
                    return ans;
                ans += s[i], i++;
                j += repeatLimit;
            }

            if (j < temp.size())
            {
                ans += temp.substr(j);
            }
        }

        return ans;
    }
};
```
