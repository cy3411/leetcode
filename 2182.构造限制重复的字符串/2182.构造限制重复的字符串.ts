/*
 * @lc app=leetcode.cn id=2182 lang=typescript
 *
 * [2182] 构造限制重复的字符串
 */

// @lc code=start
function repeatLimitedString(s: string, repeatLimit: number): string {
  // 将字符串逆序排序
  const sArr = s.split('').sort((a, b) => b.charCodeAt(0) - a.charCodeAt(0));
  // 存储连续相同的字符串
  let temp = '';
  let ans = '';
  let i = 0;
  // 遍历字符串
  block: while (i < sArr.length) {
    temp = '';
    temp += sArr[i];
    i++;
    // 将连续相同的字符串存储到temp
    while (sArr[i] === temp[0]) {
      temp += sArr[i];
      i++;
    }

    // 处理temp，将合法字串拼接到ans
    let j = 0;
    while (temp.length - j > repeatLimit) {
      ans += temp.substring(j, j + repeatLimit);
      // 已经到达字符串末尾，跳出循环
      if (i === sArr.length) break block;
      // ans后面添加一个其他字符
      ans += sArr[i];
      i++;
      // j 向后移动
      j += repeatLimit;
    }
    // 将剩余的temp拼接到ans
    if (j < temp.length) {
      ans += temp.substring(j);
    }
  }

  return ans;
}
// @lc code=end
