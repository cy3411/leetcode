# 题目

给你一个数组 `time` ，其中 `time[i]` 表示第 `i` 辆公交车完成 **一趟旅途** 所需要花费的时间。

每辆公交车可以 **连续** 完成多趟旅途，也就是说，一辆公交车当前旅途完成后，可以 **立马开始** 下一趟旅途。每辆公交车 **独立** 运行，也就是说可以同时有多辆公交车在运行且互不影响。

给你一个整数 **totalTrips** ，表示所有公交车 **总共** 需要完成的旅途数目。请你返回完成 **至少** `totalTrips` 趟旅途需要花费的 **最少** 时间。

提示：

- $1 \leq time.length \leq 10^5$
- $1 \leq time[i], totalTrips \leq 10^7$

# 示例

```
输入：time = [1,2,3], totalTrips = 5
输出：3
解释：
- 时刻 t = 1 ，每辆公交车完成的旅途数分别为 [1,0,0] 。
  已完成的总旅途数为 1 + 0 + 0 = 1 。
- 时刻 t = 2 ，每辆公交车完成的旅途数分别为 [2,1,0] 。
  已完成的总旅途数为 2 + 1 + 0 = 3 。
- 时刻 t = 3 ，每辆公交车完成的旅途数分别为 [3,1,1] 。
  已完成的总旅途数为 3 + 1 + 1 = 5 。
所以总共完成至少 5 趟旅途的最少时间为 3 。
```

```
输入：time = [2], totalTrips = 1
输出：2
解释：
只有一辆公交车，它将在时刻 t = 2 完成第一趟旅途。
所以完成 1 趟旅途的最少时间为 2 。
```

# 题解

## 二分查找

假设时间 x 内可以完成 y 趟旅程，且 y >= totalTrips，那么比 x 更大的时间也可以完成 totalTrips 趟旅程，因此可以用二分查找来求解。

确定二分的边界，最少时间肯定是 0 次，最多时间是 totalTrips 和 最小完成一趟旅途时间的乘积。

```ts
function minimumTime(time: number[], totalTrips: number): number {
  // 使用 n 个时间可以完成多少趟旅程
  const isValidate = (n: number) => {
    let res = 0;
    for (const x of time) {
      res += Math.floor(n / x);
    }
    return res;
  };

  // 初始化二分查找区间
  let l = 0;
  let r = totalTrips * time[0];
  for (const x of time) {
    r = Math.min(r, totalTrips * x);
  }
  // 二分查找
  while (l < r) {
    const mid = l + Math.floor((r - l) / 2);
    if (isValidate(mid) < totalTrips) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }

  return l;
}
```

```cpp
class Solution
{
public:
    long long isValidate(vector<int> &time, long long n)
    {
        long long res = 0;
        for (auto x : time)
        {
            res += n / x;
        }
        return res;
    }
    long long minimumTime(vector<int> &time, int totalTrips)
    {
        long long l = 0, r = 1LL * totalTrips * time[0], mid;
        for (auto x : time)
        {
            r = min(r, 1LL * totalTrips * x);
        }
        while (l < r)
        {
            mid = l + (r - l) / 2;
            if (isValidate(time, mid) < totalTrips)
            {
                l = mid + 1;
            }
            else
            {
                r = mid;
            }
        }

        return l;
    }
};
```
