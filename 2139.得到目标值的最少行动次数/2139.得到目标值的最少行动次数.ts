/*
 * @lc app=leetcode.cn id=2139 lang=typescript
 *
 * [2139] 得到目标值的最少行动次数
 */

// @lc code=start
function minMoves(target: number, maxDoubles: number): number {
  // 加倍后需要加1的次数
  let ones = 0;
  // target 二进制位去掉首位的长度，就是最多可以加倍的次数
  let bits = Math.log2(target) >> 0;
  // 可以加倍最大次数
  bits = Math.min(bits, maxDoubles);
  for (let i = 0; i < bits; i++) {
    if (target & 1) {
      ones++;
    }
    target >>= 1;
  }
  // 递增的次数 + 加倍的次数　＋　加倍后需要递增的次数
  // 这里减1，是因为题目默认是从1开始的。
  return target - 1 + bits + ones;
}
// @lc code=end
