/*
 * @lc app=leetcode.cn id=2139 lang=cpp
 *
 * [2139] 得到目标值的最少行动次数
 */

// @lc code=start
class Solution
{
public:
    int minMoves(int target, int maxDoubles)
    {
        int bits = floor(log2(target)), ones = 0;
        bits = min(maxDoubles, bits);
        for (int i = 0; i < bits; i++)
        {
            if (target & 1)
                ones += 1;
            target >>= 1;
        }
        return target - 1 + bits + ones;
    }
};
// @lc code=end
