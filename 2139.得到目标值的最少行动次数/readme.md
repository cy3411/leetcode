# 题目

你正在玩一个整数游戏。从整数 `1` 开始，期望得到整数 `target` 。

在一次行动中，你可以做下述两种操作之一：

- 递增，将当前整数的值加 1（即， $x = x + 1$）。
- 加倍，使当前整数的值翻倍（即，$x = 2 * x$）。

在整个游戏过程中，你可以使用 **递增** 操作 **任意** 次数。但是只能使用 **加倍** 操作 **至多** `maxDoubles` 次。

给你两个整数 `target` 和 `maxDoubles` ，返回从 `1` 开始得到 `target` 需要的最少行动次数。

提示：

- $\color{burlywood} 1 \leq target \leq 10^9$
- $\color{burlywood} 0 \leq maxDoubles \leq 100$

# 示例

```
输入：target = 5, maxDoubles = 0
输出：4
解释：一直递增 1 直到得到 target 。
```

```
输入：target = 19, maxDoubles = 2
输出：7
解释：最初，x = 1 。
递增 3 次，x = 4 。
加倍 1 次，x = 8 。
递增 1 次，x = 9 。
加倍 1 次，x = 18 。
递增 1 次，x = 19 。
```

# 题解

## 位运算

题意就是使用合适大小的整数，经过最多 `maxDoubles` 次加倍，再进行若干次递增，最后得到 target。

加倍操作，在位运算中就是左移一位，也就是低位补 `0` 的操作。

根据加倍的次数，我们可以将 `target` 的二进制位分成两部分，第一部分是加倍操作的次数，第二部分是递增操作的次数。

需要注意的是，加倍操作后的数字如果原来位置是 1 的话，需要在递增 1 次，这个次数也要计算在内。

最后的结果就是：递增的次数 + 加倍的次数　＋　加倍后需要递增的次数

比如数字 `target=19, maxDoucles = 2`， 二进制是 `10011`。根据加倍的次数分成两部分 `100_11`。第一部分是需要递增的次数，第二部分是加倍的次数，由于加倍的两位是两个 1，所以还需要加倍完后加 1。

所以最后的结果是：`(4-1)+2+2=7` 。(100 的 10 进制是 4)

```ts
function minMoves(target: number, maxDoubles: number): number {
  // 加倍后需要加1的次数
  let ones = 0;
  // target 二进制位去掉首位的长度，就是最多可以加倍的次数
  let bits = Math.log2(target) >> 0;
  // 可以加倍最大次数
  bits = Math.min(bits, maxDoubles);
  for (let i = 0; i < bits; i++) {
    if (target & 1) {
      ones++;
    }
    target >>= 1;
  }
  // 递增的次数 + 加倍的次数　＋　加倍后需要递增的次数
  // 这里减1，是因为题目默认是从1开始的。
  return target - 1 + bits + ones;
}
```

```cpp
class Solution
{
public:
    int minMoves(int target, int maxDoubles)
    {
        int bits = floor(log2(target)), ones = 0;
        bits = min(maxDoubles, bits);
        for (int i = 0; i < bits; i++)
        {
            if (target & 1)
                ones += 1;
            target >>= 1;
        }
        return target - 1 + bits + ones;
    }
};
```
