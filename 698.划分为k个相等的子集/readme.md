# 题目

给定一个整数数组 `nums` 和一个正整数 `k`，找出是否有可能把这个数组分成 `k` 个非空子集，其总和都相等。

提示：

- $1 \leq k \leq len(nums) \leq 16$
- $0 < nums[i] < 10000$
- 每个元素的频率在 `[1,4]` 范围内

# 示例

```
输入： nums = [4, 3, 2, 3, 5, 2, 1], k = 4
输出： True
说明： 有可能将其分成 4 个子集（5），（1,4），（2,3），（2,3）等于总和。
```

```
输入: nums = [1,2,3,4], k = 3
输出: false
```

# 题解

## 递归 + 状态压缩

首先计算 nums 的总和 total，不能 total 不是 k 整倍数，则不能有合法方案，返回 false。

否则，我们需要得到 k 个和为 $per= \frac{total}{k}$ 集合。

我们每次选择一个还在数组中的数字，若选择的数字和等于 per，那么我们就找到一个集合。

数组最长 16 位，我们可以使用状态压缩来表示每个数字的是否可用的状态。然后递归的去求解，当最后的状态为 0 的时候，表示所有数字都被使用了，有合法方案。

```js
function dfs(
  s: number,
  sum: number,
  per: number,
  nums: number[],
  n: number,
  visited: boolean[]
): boolean {
  if (s === 0) return true;
  if (!visited[s]) return visited[s];
  visited[s] = false;
  for (let i = 0; i < n; i++) {
    if (nums[i] + sum > per) break;
    // 当前数字可用
    if (((s >> i) & 1) !== 0) {
      if (dfs(s ^ (1 << i), (nums[i] + sum) % per, per, nums, n, visited)) {
        return true;
      }
    }
  }

  return false;
}

function canPartitionKSubsets(nums: number[], k: number): boolean {
  const total = nums.reduce((acc, cur) => acc + cur);
  // 总和不能整除
  if (total % k !== 0) return false;
  // 平均数
  const n = nums.length;
  const per = total / k;
  nums.sort((a, b) => a - b);
  // 有数字大于平均数，表示不能分割
  if (nums[n - 1] > per) return false;

  // 记忆化搜索
  const visited: boolean[] = new Array(1 << n).fill(true);

  return dfs((1 << n) - 1, 0, per, nums, n, visited);
}
```
