# 题目

将一个给定字符串 `s` 根据给定的行数 `numRows` ，以从上往下、从左到右进行 Z 字形排列。

比如输入字符串为 "`PAYPALISHIRING`" 行数为 `3` 时，排列如下：

```
P   A   H   N
A P L S I I G
Y   I   R
```

之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。

请你实现这个将字符串进行指定行数变换的函数：

```
string convert(string s, int numRows);
```

提示：

- $1 \leq s.length \leq 1000$
- `s` 由英文字母（小写和大写）、'`,`' 和 '`.`' 组成
- $1 \leq numRows \leq 1000$

# 示例

```
输入：s = "PAYPALISHIRING", numRows = 3
输出："PAHNAPLSIIGYIR"
```

```
输入：s = "PAYPALISHIRING", numRows = 4
输出："PINALSIGYAHRPI"
解释：
P     I    N
A   L S  I G
Y A   H R
P     I
```

# 题解

## 模拟

使用二维数组模拟，将变形的字符放入二维数组中，然后将二维数组中的字符按照顺序拼接起来

观察示例中的结果，可以发现，每次变换的周期是 $numRows + numRows - 2，假设为cycle$，每个周期需要占用 $1 + numRows - 1$列，这样我们可以得到二维数字的总列数是

$$
\lceil \frac{n}{cycle} \rceil * (1 + numRows - 2)
$$

遍历字符串并按照 Z 型来填充，初始 $(x,y)=(0,0)$,若当前字符下标 $i$ 满足 $(i \mod cycle) mod  < (numRows - 1)$，则 $x++$,否则$x--,y++$

最后遍历二维数组，按序将字符串拼接返回答案

```ts
function convert(s: string, numRows: number): string {
  const n = s.length;
  if (numRows >= n || numRows === 1) return s;

  // 一个变换周期的长度
  const cycle = numRows + numRows - 2;
  // 变换后凑成的列数
  const cols = Math.ceil(n / cycle) * (1 + numRows - 2);
  // 二维数组，存储变换后的字符串。
  const matrix = new Array(numRows).fill(0).map(() => new Array(cols).fill(''));
  // 按照顺序，将变换后的字符串放入二维数组中。
  for (let i = 0, x = 0, y = 0; i < n; i++) {
    matrix[x][y] = s[i];
    if (i % cycle < numRows - 1) {
      x++;
    } else {
      x--;
      y++;
    }
  }
  // 按序将字符串拼接起来。
  let ans = '';
  for (const row of matrix) {
    for (const char of row) {
      if (char !== '') ans += char;
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    string convert(string s, int numRows)
    {
        int n = s.size();
        if (numRows == 1 || numRows >= n)
        {
            return s;
        }
        int cycle = 2 * numRows - 2;
        int cols = (n + cycle - 1) / cycle * (numRows - 1);
        vector<string> matrix(numRows, string(cols, 0));
        for (int i = 0, x = 0, y = 0; i < n; i++)
        {
            matrix[x][y] = s[i];
            if (i % cycle < numRows - 1)
            {
                x++;
            }
            else
            {
                x--, y++;
            }
        }

        string ans;
        for (auto &row : matrix)
        {
            for (auto ch : row)
            {
                if (ch)
                {
                    ans += ch;
                }
            }
        }

        return ans;
    }
};
```
