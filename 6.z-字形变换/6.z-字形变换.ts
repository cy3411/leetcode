/*
 * @lc app=leetcode.cn id=6 lang=typescript
 *
 * [6] Z 字形变换
 */

// @lc code=start
function convert(s: string, numRows: number): string {
  const n = s.length;
  if (numRows >= n || numRows === 1) return s;

  // 一个变换周期的长度
  const cycle = numRows + numRows - 2;
  // 变换后凑成的列数
  const cols = Math.ceil(n / cycle) * (1 + numRows - 2);
  // 二维数组，存储变换后的字符串。
  const matrix = new Array(numRows).fill(0).map(() => new Array(cols).fill(''));
  // 按照顺序，将变换后的字符串放入二维数组中。
  for (let i = 0, x = 0, y = 0; i < n; i++) {
    matrix[x][y] = s[i];
    if (i % cycle < numRows - 1) {
      x++;
    } else {
      x--;
      y++;
    }
  }
  // 按序将字符串拼接起来。
  let ans = '';
  for (const row of matrix) {
    for (const char of row) {
      if (char !== '') ans += char;
    }
  }

  return ans;
}
// @lc code=end
