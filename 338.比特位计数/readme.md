# 题目描述
给定一个非负整数 num。对于 0 ≤ i ≤ num 范围中的每个数字 i ，计算其二进制数中的 1 的数目并将它们作为数组返回。

# 示例
```
输入: 2
输出: [0,1,1]
```

# 方法

## 扫描数组
 
 从0到num直接计算每个数值的‘1bit数’

 ```javascript
 var countBits = function (num) {
  const result = [];
  for (let i = 0; i <= num; i++) {
    let n = i;
    let countOne = 0;
    while (n !== 0) {
      n = n & (n - 1);
      countOne++;
    }
    result.push(countOne);
  }
  return result;
};
 ```

 ## 动态规划
 如果知道bits[j]的数量，那么bits[i]（0<=i<j）也就比i多了一个1。

$bits[j] = bits[i]+1$

这里还需要最高有效位的情况，就是2的整数次幂(i & (i - 1)) == 0)。
判断是否是最高有效位，记录当前位置。

```javascript
var countBits = function (num) {
  const bits = new Array(num + 1).fill(0);
  let high = 0;

  for (let i = 1; i <= num; i++) {
    if ((i & (i - 1)) === 0) {
      high = i;
    }
    bits[i] = bits[i - high] + 1;
  }

  return bits;
};
```