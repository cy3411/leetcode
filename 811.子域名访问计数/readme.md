# 题目

网站域名 "discuss.leetcode.com" 由多个子域名组成。顶级域名为 "com" ，二级域名为 "leetcode.com" ，最低一级为 "discuss.leetcode.com" 。当访问域名 "discuss.leetcode.com" 时，同时也会隐式访问其父域名 "leetcode.com" 以及 "com" 。

计数配对域名 是遵循 "rep d1.d2.d3" 或 "rep d1.d2" 格式的一个域名表示，其中 rep 表示访问域名的次数，d1.d2.d3 为域名本身。

- 例如，"9001 discuss.leetcode.com" 就是一个 计数配对域名 ，表示 discuss.leetcode.com 被访问了 9001 次。

给你一个 计数配对域名 组成的数组 cpdomains ，解析得到输入中每个子域名对应的 计数配对域名 ，并以数组形式返回。可以按 任意顺序 返回答案。

提示：

- $1 \leq cpdomain.length \leq 100$
- $1 \leq cpdomain[i].length \leq 100$
- `cpdomain[i]` 会遵循 `"repi d1i.d2i.d3i"` 或 `"repi d1i.d2i"` 格式
- `repi` 是范围 $[1, 10^4]$ 内的一个整数
- $d1_i$、$d2_i$ 和 $d3_i$ 由小写英文字母组成

# 示例

```
输入：cpdomains = ["9001 discuss.leetcode.com"]
输出：["9001 leetcode.com","9001 discuss.leetcode.com","9001 com"]
解释：例子中仅包含一个网站域名："discuss.leetcode.com"。
按照前文描述，子域名 "leetcode.com" 和 "com" 都会被访问，所以它们都被访问了 9001 次。
```

```
输入：cpdomains = ["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]
输出：["901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com"]
解释：按照前文描述，会访问 "google.mail.com" 900 次，"yahoo.com" 50 次，"intel.mail.com" 1 次，"wiki.org" 5 次。
而对于父域名，会访问 "mail.com" 900 + 1 = 901 次，"com" 900 + 50 + 1 = 951 次，和 "org" 5 次。
```

# 题解

## 哈希表

使用哈希表记录每个子域名的访问次数。

遍历 cpdomains ，对于每个计数配对域名，获取计数和完整域名，同时更新哈希表中每个子域名的计数。

```js
function subdomainVisits(cpdomains: string[]): string[] {
  const counts = new Map<string, number>();
  // 记录每个域名的出现次数
  for (const x of cpdomains) {
    let [count, domain] = x.split(' ');
    counts.set(domain, (counts.get(domain) || 0) + Number(count));
    const n = domain.length;
    // 记录每个子域名的出现次数
    for (let i = 0; i < n; i++) {
      if (domain[i] === '.') {
        const subdomain = domain.substring(i + 1);
        counts.set(subdomain, (counts.get(subdomain) || 0) + Number(count));
      }
    }
  }

  const ans: string[] = [];
  for (const [subdomain, count] of counts.entries()) {
    ans.push(`${count} ${subdomain}`);
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<string> subdomainVisits(vector<string> &cpdomains) {
        unordered_map<string, int> counts;
        for (auto &x : cpdomains) {
            int idx = x.find(' ');
            int count = stoi(x.substr(0, idx));
            string domain = x.substr(idx + 1);
            counts[domain] += count;
            for (int i = 0; i < domain.size(); i++) {
                if (domain[i] == '.') {
                    string subdomain = domain.substr(i + 1);
                    counts[subdomain] += count;
                }
            }
        }

        vector<string> ans;
        for (auto &[subdomain, count] : counts) {
            ans.push_back(to_string(count) + ' ' + subdomain);
        }

        return ans;
    }
};
```
