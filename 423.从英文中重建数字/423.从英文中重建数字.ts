/*
 * @lc app=leetcode.cn id=423 lang=typescript
 *
 * [423] 从英文中重建数字
 */

// @lc code=start
function originalDigits(s: string): string {
  // 词频统计
  const hash = new Map<string, number>();
  for (const c of s) {
    hash.set(c, (hash.get(c) || 0) + 1);
  }
  // 根据单词字符的唯一性确定每个数字的出现次数
  const cnt = new Array(10).fill(0);
  cnt[0] = hash.get('z') || 0;
  cnt[2] = hash.get('w') || 0;
  cnt[4] = hash.get('u') || 0;
  cnt[6] = hash.get('x') || 0;
  cnt[8] = hash.get('g') || 0;
  cnt[3] = (hash.get('h') || 0) - cnt[8];
  cnt[5] = (hash.get('f') || 0) - cnt[4];
  cnt[7] = (hash.get('v') || 0) - cnt[5];
  cnt[9] = (hash.get('i') || 0) - cnt[5] - cnt[6] - cnt[8];
  cnt[1] = (hash.get('o') || 0) - cnt[0] - cnt[2] - cnt[4];

  const ans = [];
  // 遍历数组，按照数字的顺序，把数字按照顺序放入结果数组
  for (let i = 0; i < 10; i++) {
    for (let j = 0; j < cnt[i]; j++) {
      ans.push(i.toString());
    }
  }

  return ans.join('');
}
// @lc code=end
