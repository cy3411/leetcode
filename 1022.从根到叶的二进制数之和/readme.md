# 题目

给出一棵二叉树，其上每个结点的值都是 `0` 或 `1` 。每一条从根到叶的路径都代表一个从最高有效位开始的二进制数。

例如，如果路径为 `0 -> 1 -> 1 -> 0 -> 1`，那么它表示二进制数 `01101`，也就是 `13` 。
对树上的每一片叶子，我们都要找出从根到该叶子的路径所表示的数字。

返回这些数字之和。题目数据保证答案是一个 **32 位 整数**。

提示：

- 树中的节点数在 `[1, 1000]` 范围内
- `Node.val` 仅为 `0` 或 `1`

# 示例

```
输入：root = [1,0,1,0,1,0,1]
输出：22
解释：(100) + (101) + (110) + (111) = 4 + 5 + 6 + 7 = 22
```

```
输入：root = [0]
输出：0
```

# 题解

## 递归

在 DFS 的过程中记录当前的值，然后递归到下一层，最后把所有的值相加。

```ts
function dfs(root: TreeNode | null, val: number): number {
  if (root === null) return 0;
  //  将当前值左移一位，并将当前节点的值放在最低位
  val = (val << 1) | root.val;
  if (!root.left && !root.right) {
    //   如果是根节点，则返回当前值
    return val;
  }

  let sum = 0;
  sum = dfs(root.left, val) + dfs(root.right, val);
  return sum;
}

function sumRootToLeaf(root: TreeNode | null): number {
  return dfs(root, 0);
}
```
