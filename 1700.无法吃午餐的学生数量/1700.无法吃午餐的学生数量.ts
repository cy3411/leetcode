/*
 * @lc app=leetcode.cn id=1700 lang=typescript
 *
 * [1700] 无法吃午餐的学生数量
 */

// @lc code=start
function countStudents(students: number[], sandwiches: number[]): number {
  // 喜欢吃圆的学生数量
  let s0 = 0;
  // 喜欢吃方的学生数量
  let s1 = 0;
  for (const s of students) {
    if (s === 0) s0++;
    else s1++;
  }

  const n = sandwiches.length;
  for (const s of sandwiches) {
    if (s === 0 && s0 > 0) s0--;
    else if (s === 1 && s1 > 0) s1--;
    else break;
  }

  return s0 + s1;
}
// @lc code=end
