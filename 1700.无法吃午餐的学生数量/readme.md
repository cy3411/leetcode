# 题目

学校的自助午餐提供圆形和方形的三明治，分别用数字 `0` 和 `1` 表示。所有学生站在一个队列里，每个学生要么喜欢圆形的要么喜欢方形的。
餐厅里三明治的数量与学生的数量相同。所有三明治都放在一个 栈 里，每一轮：

- 如果队列最前面的学生 喜欢 栈顶的三明治，那么会 拿走它 并离开队列。
- 否则，这名学生会 放弃这个三明治 并回到队列的尾部。

这个过程会一直持续到队列里所有学生都不喜欢栈顶的三明治为止。

给你两个整数数组 `students` 和 `sandwiches` ，其中 `sandwiches[i]` 是栈里面第 `i​​​​​​` 个三明治的类型（`i = 0` 是栈的顶部）， `students[j]` 是初始队列里第 `j​​​​​​` 名学生对三明治的喜好（`j = 0` 是队列的最开始位置）。请你返回无法吃午餐的学生数量。

# 示例

```
输入：students = [1,1,0,0], sandwiches = [0,1,0,1]
输出：0
解释：
- 最前面的学生放弃最顶上的三明治，并回到队列的末尾，学生队列变为 students = [1,0,0,1]。
- 最前面的学生放弃最顶上的三明治，并回到队列的末尾，学生队列变为 students = [0,0,1,1]。
- 最前面的学生拿走最顶上的三明治，剩余学生队列为 students = [0,1,1]，三明治栈为 sandwiches = [1,0,1]。
- 最前面的学生放弃最顶上的三明治，并回到队列的末尾，学生队列变为 students = [1,1,0]。
- 最前面的学生拿走最顶上的三明治，剩余学生队列为 students = [1,0]，三明治栈为 sandwiches = [0,1]。
- 最前面的学生放弃最顶上的三明治，并回到队列的末尾，学生队列变为 students = [0,1]。
- 最前面的学生拿走最顶上的三明治，剩余学生队列为 students = [1]，三明治栈为 sandwiches = [1]。
- 最前面的学生拿走最顶上的三明治，剩余学生队列为 students = []，三明治栈为 sandwiches = []。
所以所有学生都有三明治吃。
```

```
输入：students = [1,1,1,0,0,1], sandwiches = [1,0,0,0,1,1]
输出：3
```

# 题解

## 模拟

题意可以看出，学生是一个循环队列，因此学生的相对位置不会影响取三明治的过程。

我们可以分别记录喜欢圆形和方形的学生数量(s0 和 s1)，遍历 sandwiches：

- 如果是圆形且 s0 大于 0，那么 s0 数量减 1
- 如果是方形且 s1 大于 0，那么 s1 数量减 1
- 否则，跳出

最后返回 s0 和 s1 的剩余数量即可。

```js
function countStudents(students: number[], sandwiches: number[]): number {
  // 喜欢吃圆的学生数量
  let s0 = 0;
  // 喜欢吃方的学生数量
  let s1 = 0;
  for (const s of students) {
    if (s === 0) s0++;
    else s1++;
  }

  const n = sandwiches.length;
  for (const s of sandwiches) {
    if (s === 0 && s0 > 0) s0--;
    else if (s === 1 && s1 > 0) s1--;
    else break;
  }

  return s0 + s1;
}
```

```cpp
class Solution {
public:
    int countStudents(vector<int> &students, vector<int> &sandwiches) {
        int s1 = accumulate(students.begin(), students.end(), 0);
        int s0 = students.size() - s1;

        for (auto s : sandwiches) {
            if (s == 0 && s0 > 0)
                s0--;
            else if (s == 1 && s1 > 0)
                s1--;
            else
                break;
        }

        return s0 + s1;
    }
};
```

```py
class Solution:
    def countStudents(self, students: List[int], sandwiches: List[int]) -> int:
        s1 = sum(students)
        s0 = len(students) - s1

        for s in sandwiches:
            if s == 0 and s0 > 0:
                s0 -= 1
            elif s == 1 and s1 > 0:
                s1 -= 1
            else:
                break

        return s0 + s1
```
