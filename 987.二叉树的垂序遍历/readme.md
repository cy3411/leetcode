# 题目

给你二叉树的根结点 root ，请你设计算法计算二叉树的 垂序遍历 序列。

对位于 (row, col) 的每个结点而言，其左右子结点分别位于 (row + 1, col - 1) 和 (row + 1, col + 1) 。树的根结点位于 (0, 0) 。

二叉树的 垂序遍历 从最左边的列开始直到最右边的列结束，按列索引每一列上的所有结点，形成一个按出现位置从上到下排序的有序列表。如果同行同列上有多个结点，则按结点的值从小到大进行排序。

返回二叉树的 垂序遍历 序列。

提示：

- 树中结点数目总数在范围 [1, 1000] 内
- 0 <= Node.val <= 1000

# 示例

[![WvWPnH.png](https://z3.ax1x.com/2021/07/31/WvWPnH.png)](https://imgtu.com/i/WvWPnH)

```
输入：root = [3,9,20,null,null,15,7]
输出：[[9],[3,15],[20],[7]]
解释：
列 -1 ：只有结点 9 在此列中。
列  0 ：只有结点 3 和 15 在此列中，按从上到下顺序。
列  1 ：只有结点 20 在此列中。
列  2 ：只有结点 7 在此列中。
```

# 题解

## 深度优先搜索

深度优先遍历每个节点，计算出每个节点的[row,col]值，保存到结果中。

按照 col,row,val 的优先级对遍历后的结果排序。

最后将结果按照 col 分组，输出答案。

```ts
function verticalTraversal(root: TreeNode | null): number[][] {
  const inOrder = (node: TreeNode | null, row = 0, col = 0) => {
    if (node === null) return;
    inOrder(node.left, row + 1, col - 1);
    preOrderArray.push([node, row, col]);
    inOrder(node.right, row + 1, col + 1);
  };
  // 中序遍历，计算出所有节点的[row,col]
  const preOrderArray: [TreeNode, number, number][] = [];
  inOrder(root);
  // 按照col,row,value顺序排序
  preOrderArray.sort((a: [TreeNode, number, number], b: [TreeNode, number, number]): number => {
    if (a[2] === b[2]) {
      if (a[1] === b[1]) {
        return a[0].val - b[0].val;
      } else {
        return a[1] - b[1];
      }
    } else {
      return a[2] - b[2];
    }
  });
  // 将排序结果分组
  const hash: Map<number, number[]> = new Map();
  for (let i = 0; i < preOrderArray.length; i++) {
    const k = preOrderArray[i][2];
    const v = preOrderArray[i][0].val;
    if (hash.has(k)) {
      hash.get(k).push(v);
    } else {
      hash.set(k, [v]);
    }
  }
  return [...hash.values()];
}
```
