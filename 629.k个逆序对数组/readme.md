# 题目

给出两个整数 n 和 k，找出所有包含从 1 到 n 的数字，且恰好拥有 k 个逆序对的不同的数组的个数。

逆序对的定义如下：对于数组的第 i 个和第 j 个元素，如果满 i < j 且 a[i] > a[j]，则其为一个逆序对；否则不是。

由于答案可能很大，只需要返回 答案 mod $10^9 + 7$ 的值。

说明:

- `n` 的范围是 [1, 1000] 并且 `k` 的范围是 [0, 1000]。

# 示例

```
输入: n = 3, k = 0
输出: 1
解释:
只有数组 [1,2,3] 包含了从1到3的整数并且正好拥有 0 个逆序对。
```

```
输入: n = 3, k = 1
输出: 2
解释:
数组 [1,3,2] 和 [2,1,3] 都有 1 个逆序对。
```

# 题解

## 暴力

因为第 n 个数（即 n）肯定比前 n-1 个数大，所以，我们可以把数字 n 插入到 0~n-1 的不同位置：

- 假如前 n-1 个数的数组中正好有 k 个逆序对，那么，我们可以把 n 插入到这个数组的最后一位，得到的新数组正好也是 k 个逆序对，因为，新插入的 n 不会与任何数字组成逆序对，而原来数组中的逆序对保持不变；

- 假如前 n-1 个数的数组中正好有 k-1 个逆序对，那么，我们可以把 n 插入到这个数组的倒数第二位，这样，我们会得到 1 个新的逆序对，即原数组的最后一位与 n 组成的逆序对，而原来数组中的 k-1 个逆序对保持不变，总共会有 k 个逆序对；

- 假如前 n-1 个数的数组中正好有 0(k-k) 个逆序对，那么，我们可以把 n 插入到这个数组的倒数第 k+1 位，这样，我们会得到 k 个新的逆序对，即原数组的最后 k 位与 n 组成的逆序对，而原来数组中的 0 个逆序对保持不变，总共会有 k 个逆序对；

所以，我们可以得出递推公式为：$f(n)(k)=f(n−1)(k)+f(n−1)(k−1)+...+f(n−1)(0)$。

```ts
function kInversePairs(n: number, k: number): number {
  const hash = new Map<string, number>();
  const mod = 10 ** 9 + 7;
  const dfs = (n: number, k: number): number => {
    if (k > (n * (n - 1)) / 2) return 0;
    if (n === 1) return k === 0 ? 1 : 0;
    let key = n + '-' + k;
    if (hash.has(key)) return hash.get(key);
    let ans = 0;
    for (let i = Math.max(0, k - n + 1); i <= k; i++) {
      ans += dfs(n - 1, i);
    }
    ans %= mod;
    hash.set(key, ans);
    return ans;
  };

  return dfs(n, k);
}
```

## 动态规划

```ts
function kInversePairs(n: number, k: number): number {
  const MOD = 1000000007;
  // dp[i][j] 表示前i个数中，逆序对的个数为j的情况下，最小的值
  // 滚动数组
  const f = new Array(2).fill(0).map(() => new Array(k + 1).fill(0));
  f[0][0] = 1;
  for (let i = 1; i <= n; ++i) {
    for (let j = 0; j <= k; ++j) {
      const cur = i & 1,
        prev = cur ^ 1;
      f[cur][j] = (j - 1 >= 0 ? f[cur][j - 1] : 0) - (j - i >= 0 ? f[prev][j - i] : 0) + f[prev][j];
      f[cur][j] = f[cur][j] < 0 ? f[cur][j] + MOD : f[cur][j] % MOD;
    }
  }
  return f[n & 1][k];
}
```
