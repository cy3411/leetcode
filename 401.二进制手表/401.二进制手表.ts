/*
 * @lc app=leetcode.cn id=401 lang=typescript
 *
 * [401] 二进制手表
 */

// @lc code=start
function readBinaryWatch(turnedOn: number): string[] {
  const ans: string[] = [];

  for (let h = 0; h < 12; h++) {
    for (let m = 0; m < 60; m++) {
      let hBits = h.toString(2).split('0').join('').length;
      let mBits = m.toString(2).split('0').join('').length;
      if (hBits + mBits === turnedOn) {
        ans.push(h + ':' + (m < 10 ? '0' : '') + m);
      }
    }
  }

  return ans;
}
// @lc code=end
