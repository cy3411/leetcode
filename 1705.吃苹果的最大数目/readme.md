# 题目

有一棵特殊的苹果树，一连 `n` 天，每天都可以长出若干个苹果。在第 `i` 天，树上会长出 `apples[i]` 个苹果，这些苹果将会在 `days[i]` 天后（也就是说，第 `i + days[i]` 天时）腐烂，变得无法食用。也可能有那么几天，树上不会长出新的苹果，此时用 `apples[i] == 0` 且 `days[i] == 0` 表示。

你打算每天 **最多** 吃一个苹果来保证营养均衡。注意，你可以在这 `n` 天之后继续吃苹果。

给你两个长度为 `n` 的整数数组 `days` 和 `apples` ，返回你可以吃掉的苹果的最大数目。

提示：

- `apples.length == n`
- `days.length == n`
- $\color{goldenrod}1 <= n <= 2 * 10^4$
- $\color{goldenrod}0 <= apples[i], days[i] <= 2 * 10^4$
- 只有在 `apples[i] = 0` 时，`days[i] = 0` 才成立

# 示例

```
输入：apples = [1,2,3,5,2], days = [3,2,1,4,2]
输出：7
解释：你可以吃掉 7 个苹果：
- 第一天，你吃掉第一天长出来的苹果。
- 第二天，你吃掉一个第二天长出来的苹果。
- 第三天，你吃掉一个第二天长出来的苹果。过了这一天，第三天长出来的苹果就已经腐烂了。
- 第四天到第七天，你吃的都是第四天长出来的苹果。
```

```
输入：apples = [3,0,0,0,0,2], days = [3,0,0,0,0,2]
输出：5
解释：你可以吃掉 5 个苹果：
- 第一天到第三天，你吃的都是第一天长出来的苹果。
- 第四天和第五天不吃苹果。
- 第六天和第七天，你吃的都是第六天长出来的苹果。
```

# 题解

## 贪心算法

为了尽可能多的吃苹果，所以我们优先吃过期时间最短的苹果。

这里采用优先队列，优先队列的元素是一个元组，元组的第一个元素是苹果的过期时间，第二个元素是苹果的数量。

遍历数组，将过期天数和苹果数放入优先队列中，然后从优先队列中取出元素，每次取出的元素都是过期时间最短的苹果。累加苹果数量，直到优先队列为空。

```ts
class PQ {
  public heap: number[][] = [];

  constructor() {}

  private swap(i: number, j: number) {
    [this.heap[i], this.heap[j]] = [this.heap[j], this.heap[i]];
  }

  private compare(i: number, j: number): number {
    return this.heap[i][0] - this.heap[j][0];
  }

  public push(val: number[]) {
    this.heap.push(val);
    this.swin(this.heap.length - 1);
  }

  public pop() {
    const ret = this.heap[0];
    this.swap(0, this.heap.length - 1);
    this.heap.pop();
    this.sink(0);
    return ret;
  }

  public peek() {
    return this.heap[0];
  }

  public isEmpty() {
    return this.heap.length === 0;
  }

  private sink(i: number) {
    while (true) {
      let child = 2 * i + 1;
      if (child >= this.heap.length) break;
      const right = child + 1;
      if (right < this.heap.length && this.compare(right, child) < 0) {
        child = right;
      }
      if (this.compare(i, child) <= 0) break;
      this.swap(i, child);
      i = child;
    }
  }

  private swin(i: number) {
    while (i > 0) {
      const parent = Math.floor(i - 1);
      if (this.compare(i, parent) >= 0) break;
      this.swap(i, parent);
      i = parent;
    }
  }
}

function eatenApples(apples: number[], days: number[]): number {
  const minPQ = new PQ();
  let n = apples.length;
  let ans = 0;
  let i = 0;
  for (; i < n; i++) {
    // 将过期的苹果移除
    while (!minPQ.isEmpty() && minPQ.peek()[0] <= i) {
      minPQ.pop();
    }
    // 将过期天数和苹果数放入优先队列
    let rottenDay = i + days[i];
    let countApple = apples[i];
    if (countApple > 0) minPQ.push([rottenDay, countApple]);
    // 当天吃1个苹果
    if (!minPQ.isEmpty()) {
      const top = minPQ.peek();
      ans++;
      top[1]--;
      if (top[1] === 0) minPQ.pop();
    }
  }

  // 将队列中的剩余苹果取出计算
  while (!minPQ.isEmpty()) {
    // 将过期的苹果移除
    while (!minPQ.isEmpty() && minPQ.peek()[0] <= i) {
      minPQ.pop();
    }

    if (minPQ.isEmpty()) break;
    // 计算队列中能吃的苹果数
    const top = minPQ.pop();
    const count = Math.min(top[0] - i, top[1]);
    ans += count;
    i += count;
  }

  return ans;
}
```
