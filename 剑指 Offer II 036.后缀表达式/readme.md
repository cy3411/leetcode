# 题目

根据 (逆波兰表示法)[https://baike.baidu.com/item/%E9%80%86%E6%B3%A2%E5%85%B0%E5%BC%8F/128437]，求该后缀表达式的计算结果。

有效的算符包括 `+`、`-`、`*`、`/` 。每个运算对象可以是整数，也可以是另一个逆波兰表达式。

说明：

- 整数除法只保留整数部分。
- 给定逆波兰表达式总是有效的。换句话说，表达式总会得出有效数值且不存在除数为 0 的情况。

提示：

- $1 \leq tokens.length \leq 10^4$
- `tokens[i]` 要么是一个算符（`"+"`、`"-"`、`"*"` 或 `"/"`），要么是一个在范围 `[-200, 200]` 内的整数

逆波兰表达式：

逆波兰表达式是一种后缀表达式，所谓后缀就是指算符写在后面。

- 平常使用的算式则是一种中缀表达式，如 $( 1 + 2 ) * ( 3 + 4 )$ 。
- 该算式的逆波兰表达式写法为 $( ( 1 2 + ) ( 3 4 + ) * )$ 。

逆波兰表达式主要有以下两个优点：

- 去掉括号后表达式无歧义，上式即便写成 $1 2 + 3 4 + *$ 也可以依据次序计算出正确结果。
- 适合用栈操作运算：遇到数字则入栈；遇到算符则取出栈顶两个数字进行计算，并将结果压入栈中。

注意：(本题与主站 150 题相同)[https://leetcode-cn.com/problems/evaluate-reverse-polish-notation/]

# 示例

```
输入：tokens = ["2","1","+","3","*"]
输出：9
解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9
```

```
输入：tokens = ["4","13","5","/","+"]
输出：6
解释：该算式转化为常见的中缀算术表达式为：(4 + (13 / 5)) = 6
```

# 题解

## 栈

遍历 tokens，将数字入栈，遇到算符则将栈顶两个数字进行计算，并将结果压入栈中。

最后栈中剩下的数字就是答案。

```ts
function evalRPN(tokens: string[]): number {
  const n = tokens.length;
  // 利用栈保存运算对象
  const stack = [];

  for (const token of tokens) {
    switch (token) {
      case '+':
        stack.push(stack.pop() + stack.pop());
        break;
      case '-': {
        let b = stack.pop();
        let a = stack.pop();
        stack.push(a - b);
        break;
      }
      case '*':
        stack.push(stack.pop() * stack.pop());
        break;
      case '/': {
        let b = stack.pop();
        let a = stack.pop();
        stack.push((a / b) >> 0);
        break;
      }
      default:
        stack.push(Number(token));
        break;
    }
  }

  return stack[0];
}
```

```cpp
class Solution
{
public:
    int evalRPN(vector<string> &tokens)
    {
        stack<int> stk;
        for (auto token : tokens)
        {
            if ((token[0] >= '0' && token[0] <= '9') || token.size() > 1)
            {
                stk.push(stoi(token));
            }
            else
            {
                int b = stk.top();
                stk.pop();
                int a = stk.top();
                stk.pop();
                int res;
                if (token == "+")
                {
                    res = a + b;
                }
                else if (token == "-")
                {
                    res = a - b;
                }
                else if (token == "*")
                {
                    res = a * b;
                }
                else
                {
                    res = a / b;
                }
                stk.push(res);
            }
        }
        return stk.top();
    }
};
```
