class Solution
{
public:
    int evalRPN(vector<string> &tokens)
    {
        stack<int> stk;
        for (auto token : tokens)
        {
            if ((token[0] >= '0' && token[0] <= '9') || token.size() > 1)
            {
                stk.push(stoi(token));
            }
            else
            {
                int b = stk.top();
                stk.pop();
                int a = stk.top();
                stk.pop();
                int res;
                if (token == "+")
                {
                    res = a + b;
                }
                else if (token == "-")
                {
                    res = a - b;
                }
                else if (token == "*")
                {
                    res = a * b;
                }
                else
                {
                    res = a / b;
                }
                stk.push(res);
            }
        }
        return stk.top();
    }
};