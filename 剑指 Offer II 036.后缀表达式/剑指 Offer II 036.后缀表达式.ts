// @algorithm @lc id=1000279 lang=typescript
// @title 8Zf90G
// @test(["2","1","+","3","*"])=9
function evalRPN(tokens: string[]): number {
  const n = tokens.length;
  // 利用栈保存运算对象
  const stack = [];

  for (const token of tokens) {
    switch (token) {
      case '+':
        stack.push(stack.pop() + stack.pop());
        break;
      case '-': {
        let b = stack.pop();
        let a = stack.pop();
        stack.push(a - b);
        break;
      }
      case '*':
        stack.push(stack.pop() * stack.pop());
        break;
      case '/': {
        let b = stack.pop();
        let a = stack.pop();
        stack.push((a / b) >> 0);
        break;
      }
      default:
        stack.push(Number(token));
        break;
    }
  }

  return stack[0];
}
