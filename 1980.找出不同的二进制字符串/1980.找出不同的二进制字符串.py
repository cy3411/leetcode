#
# @lc app=leetcode.cn id=1980 lang=python3
#
# [1980] 找出不同的二进制字符串
#

# @lc code=start
class Solution:
    def findDifferentBinaryString(self, nums: List[str]) -> str:
        n = len(nums)
        ans = ''
        for i in range(n):
            ans += nums[i][i] == '0' and '1' or '0'

        return ans
# @lc code=end
