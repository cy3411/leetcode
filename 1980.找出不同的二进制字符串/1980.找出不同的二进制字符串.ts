/*
 * @lc app=leetcode.cn id=1980 lang=typescript
 *
 * [1980] 找出不同的二进制字符串
 */

// @lc code=start
function findDifferentBinaryString(nums: string[]): string {
  const n = nums.length;
  let ans = '';
  for (let i = 0; i < n; i++) {
    ans += nums[i][i] === '0' ? '1' : '0';
  }
  return ans;
}
// @lc code=end
