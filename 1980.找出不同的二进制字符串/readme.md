# 题目

给你一个字符串数组 `nums` ，该数组由 `n` 个 **互不相同** 的二进制字符串组成，且每个字符串长度都是 `n` 。请你找出并返回一个长度为 `n` 且 没有出现 在 `nums` 中的二进制字符串。如果存在多种答案，只需返回 **任意一个** 即可。

提示：

- $n \equiv nums.length$
- $1 \leq n \leq 16$
- $nums[i].length \equiv n$
- `nums[i]` 为 `'0'` 或 `'1'`
- `nums` 中的所有字符串** 互不相同**

# 示例

```
输入：nums = ["01","10"]
输出："11"
解释："11" 没有出现在 nums 中。"00" 也是正确答案。
```

```
输入：nums = ["111","011","001"]
输出："101"
解释："101" 没有出现在 nums 中。"000"、"010"、"100"、"110" 也是正确答案。
```

# 题解

## 遍历

遍历 nums，对每个字符串，取其第 i 位不同的数字，构建一个新的字符串即可。

```ts
function findDifferentBinaryString(nums: string[]): string {
  const n = nums.length;
  let ans = '';
  for (let i = 0; i < n; i++) {
    ans += nums[i][i] === '0' ? '1' : '0';
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    string findDifferentBinaryString(vector<string> &nums)
    {
        int n = nums.size();
        string ans;
        for (int i = 0; i < n; i++)
        {
            ans += nums[i][i] == '0' ? '1' : '0';
        }
        return ans;
    }
};
```

```python
class Solution:
    def findDifferentBinaryString(self, nums: List[str]) -> str:
        n = len(nums)
        ans = ''
        for i in range(n):
            ans += nums[i][i] == '0' and '1' or '0'

        return ans
```
