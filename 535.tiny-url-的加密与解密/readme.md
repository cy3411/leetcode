# 题目

TinyURL 是一种 URL 简化服务， 比如：当你输入一个 URL `https://leetcode.com/problems/design-tinyurl` 时，它将返回一个简化的 URL `http://tinyurl.com/4e9iAk`.

要求：设计一个 TinyURL 的加密 `encode` 和解密 `decode` 的方法。你的加密和解密算法如何设计和运作是没有限制的，你只需要保证一个 URL 可以被加密成一个 TinyURL，并且这个 TinyURL 可以用解密方法恢复成原本的 URL。

# 题解

## 哈希表

计算一个随机短链接，将短链接和长链接的映射关系存入哈希表中即可。

```ts
// 存储短链接到长链接的映射关系
const hash: Map<string, string> = new Map();

// 小写字母、大写字母和数字随机字符
const randChar = (seed: number): string => {
  const base = ['a'.charCodeAt(0), 'A'.charCodeAt(0), '0'.charCodeAt(0)];
  const limit = (62 * seed) >> 0;
  if (limit < 26) {
    return String.fromCharCode(limit + base[0]);
  } else if (limit < 52) {
    return String.fromCharCode(limit - 26 - base[1]);
  } else {
    return String.fromCharCode(limit - 52 + base[1]);
  }
};

// 生成 n 长度的随机串
const randStr = (n: number): string => {
  let s = '';
  do {
    let seed = Math.random();
    s += randChar(seed);
  } while (n--);
  return s;
};
/**
 * Encodes a URL to a shortened URL.
 */
function encode(longUrl: string): string {
  let tinyUrl: string;
  do {
    tinyUrl = randStr(5);
  } while (hash.has(tinyUrl));

  hash.set(tinyUrl, longUrl);
  return tinyUrl;
}

/**
 * Decodes a shortened URL to its original URL.
 */
function decode(shortUrl: string): string {
  return hash.get(shortUrl);
}
```
