/*
 * @lc app=leetcode.cn id=888 lang=javascript
 *
 * [888] 公平的糖果交换
 */

// @lc code=start
/**
 * @param {number[]} A
 * @param {number[]} B
 * @return {number[]}
 */
var fairCandySwap = function (A, B) {
  const sumA = A.reduce((prev, curr) => prev + curr, 0);
  const sumB = B.reduce((prev, curr) => prev + curr, 0);
  const memo = new Set(A);
  const target = Math.floor((sumA + sumB) / 2);

  for (const y of B) {
    const x = target - (sumB - y);
    if (memo.has(x)) {
      return [x, y];
    }
  }
};
// @lc code=end
