# 题目

有一组 `n` 个人作为实验对象，从 `0` 到 `n - 1` 编号，其中每个人都有不同数目的钱，以及不同程度的安静值（`quietness`）。为了方便起见，我们将编号为 `x` 的人简称为 "person `x` "。

给你一个数组 `richer` ，其中 `richer[i] = [ai, bi]` 表示 person $\color{goldenrod}a_i$ 比 person $\color{goldenrod}b_i$ 更有钱。另给你一个整数数组 `quiet` ，其中 `quiet[i]` 是 person `i` 的安静值。`richer` 中所给出的数据 逻辑自恰（也就是说，在 person `x` 比 person `y` 更有钱的同时，不会出现 person `y` 比 person `x` 更有钱的情况 ）。

现在，返回一个整数数组 `answer` 作为答案，其中 `answer[x] = y` 的前提是，在所有拥有的钱肯定不少于 person `x` 的人中，person `y` 是最安静的人（也就是安静值 `quiet[y]` 最小的人）。

提示：

- $\color{goldenrod}n == quiet.length$
- $\color{goldenrod}1 \leq n \leq 500$
- $\color{goldenrod}0 \leq quiet[i] < n$
- `quiet` 的所有值 **互不相同**
- $\color{goldenrod}0 \leq richer.length \leq n * (n - 1) / 2$
- $\color{goldenrod}0 \leq ai, bi < n$
- $\color{goldenrod}ai != bi$
- richer 中的所有数对 **互不相同**
- 对 `richer` 的观察在逻辑上是一致的

# 示例

```
输入：richer = [[1,0],[2,1],[3,1],[3,7],[4,3],[5,3],[6,3]], quiet = [3,2,5,4,6,1,7,0]
输出：[5,5,2,5,4,5,6,7]
解释：
answer[0] = 5，
person 5 比 person 3 有更多的钱，person 3 比 person 1 有更多的钱，person 1 比 person 0 有更多的钱。
唯一较为安静（有较低的安静值 quiet[x]）的人是 person 7，
但是目前还不清楚他是否比 person 0 更有钱。
answer[7] = 7，
在所有拥有的钱肯定不少于 person 7 的人中（这可能包括 person 3，4，5，6 以及 7），
最安静（有较低安静值 quiet[x]）的人是 person 7。
其他的答案也可以用类似的推理来解释。
```

# 题解

## 深度优先搜索

可以根据 richer 构建一张有向图：如果$a_i$比$b_i$更有钱，则$b_i$指向$a_i$。题目保证了 richer 给出的数据逻辑自恰，我们得到一张有向无环图。

假如我们从图中任意一点出发，沿着有向边能访问的所有点，都比起始点更有钱。

我们深度优先搜索图，每次搜索到一个点，我们把它的加到答案中，如果遍历到安静值更小的点，我就更新当前答案。

```ts
function loudAndRich(richer: number[][], quiet: number[]): number[] {
  const n = quiet.length;
  const graph = new Array(n).fill(0).map(() => []);
  // 建图，每个人的邻居
  for (const [a, b] of richer) {
    graph[b].push(a);
  }
  // 深度优先搜索，将quiet值最小的邻居找出来
  const dfs = (x: number, ans: number[]): void => {
    if (ans[x] !== -1) return;
    ans[x] = x;
    for (const y of graph[x]) {
      dfs(y, ans);
      if (quiet[ans[y]] < quiet[ans[x]]) {
        ans[x] = ans[y];
      }
    }
  };

  const ans: number[] = new Array(n).fill(-1);
  for (let i = 0; i < n; i++) {
    dfs(i, ans);
  }

  return ans;
}
```
