# 题目

给你一个整数数组 `nums`，和一个整数 `k` 。

在一个操作中，您可以选择 $0 \leq i < nums.length$ 的任何索引 `i` 。将 `nums[i]` 改为 `nums[i] + x` ，其中 `x` 是一个范围为 `[-k, k]` 的整数。对于每个索引 `i` ，最多 `只能` 应用 **一次** 此操作。

`nums` 的 **分数** 是 `nums` 中最大和最小元素的差值。

在对 `nums` 中的每个索引最多应用一次上述操作后，返回 `nums` 的*最低 分数* 。

提示：

- $1 \leq nums.length \leq 10^4$
- $0 \leq nums[i] \leq 10^4$
- $0 \leq k \leq 10^4$

# 示例

```
输入：nums = [1], k = 0
输出：0
解释：分数是 max(nums) - min(nums) = 1 - 1 = 0。
```

```
输入：nums = [0,10], k = 2
输出：6
解释：将 nums 改为 [2,8]。分数是 max(nums) - min(nums) = 8 - 2 = 6。
```

# 题解

## 模拟

想要差值最小，那么就要让数组中的最大元素变小，让最小元素变大。

找出数组中的最大元素和最小元素，让它们 `-k` 和 `+k`，然后再求差值。

需要注意一下，当 `k` 的区间超过两者的差值，直接返回 0 即可。要不就会有负数的结果。

```ts
function smallestRangeI(nums: number[], k: number): number {
  const n = nums.length;

  // 最大值
  let maxNum = nums[0];
  // 最小值
  let minNum = nums[0];
  for (let i = 1; i < n; i++) {
    maxNum = Math.max(maxNum, nums[i]);
    minNum = Math.min(minNum, nums[i]);
  }
  // k 的范围超过了差值，返回 0
  // 因为 maxNum 和 minNum 可以凑成相等的数字
  if (k * 2 >= maxNum - minNum) return 0;

  // 返回 (maxNum - k) - (minNum + k)
  return maxNum - minNum - 2 * k;
}
```

```cpp
class Solution
{
public:
    int smallestRangeI(vector<int> &nums, int k)
    {
        int n = nums.size(), maxNum = nums[0], minNum = nums[0];
        for (int i = 1; i < n; i++)
        {
            maxNum = max(maxNum, nums[i]);
            minNum = min(minNum, nums[i]);
        }
        return max(0, maxNum - minNum - 2 * k);
    }
};
```
