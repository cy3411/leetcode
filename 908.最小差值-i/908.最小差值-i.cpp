/*
 * @lc app=leetcode.cn id=908 lang=cpp
 *
 * [908] 最小差值 I
 */

// @lc code=start
class Solution
{
public:
    int smallestRangeI(vector<int> &nums, int k)
    {
        int n = nums.size(), maxNum = nums[0], minNum = nums[0];
        for (int i = 1; i < n; i++)
        {
            maxNum = max(maxNum, nums[i]);
            minNum = min(minNum, nums[i]);
        }
        return max(0, maxNum - minNum - 2 * k);
    }
};
// @lc code=end
