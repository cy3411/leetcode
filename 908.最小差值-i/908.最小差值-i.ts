/*
 * @lc app=leetcode.cn id=908 lang=typescript
 *
 * [908] 最小差值 I
 */

// @lc code=start
function smallestRangeI(nums: number[], k: number): number {
  const n = nums.length;

  // 最大值
  let maxNum = nums[0];
  // 最小值
  let minNum = nums[0];
  for (let i = 1; i < n; i++) {
    maxNum = Math.max(maxNum, nums[i]);
    minNum = Math.min(minNum, nums[i]);
  }
  // k 的范围超过了差值，返回 0
  // 因为 maxNum 和 minNum 可以凑成相等的数字
  if (k * 2 >= maxNum - minNum) return 0;

  // 返回 (maxNum - k) - (minNum + k)
  return maxNum - minNum - 2 * k;
}
// @lc code=end
