# 题目

给你一个大小为 `m x n` 的矩阵 `board` 表示甲板，其中，每个单元格可以是一艘战舰 `'X'` 或者是一个空位 `'.'` ，返回在甲板 `board` 上放置的 **战舰** 的数量。

**战舰** 只能水平或者垂直放置在 `board` 上。换句话说，战舰只能按 `1 x k（1 行，k 列）`或 `k x 1（k 行，1 列）`的形状建造，其中 `k` 可以是任意大小。两艘战舰之间至少有一个水平或垂直的空位分隔 （即没有相邻的战舰）。

# 示例

[![TV0aB8.png](https://s4.ax1x.com/2021/12/18/TV0aB8.png)](https://imgtu.com/i/TV0aB8)

```
输入：board = [["X",".",".","X"],[".",".",".","X"],[".",".",".","X"]]
输出：2
```

# 题解

## 深度优先搜索

战舰的定义为：

- 只能水平或者垂直放置在 `board` 上。
- 两艘战舰之间至少有一个水平或垂直的空位分隔 （即没有相邻的战舰）。

遍历每个位置，当前位置如果为'X',表示找到一艘战舰，同时将相邻的'X'改为'.'，并且答案加 1。

```ts
function countBattleships(board: string[][]): number {
  const n = board.length;
  const m = board[0].length;
  let ans = 0;

  // 将board[i][j] === 'X'相连的位置都设置为'.'
  const dfs = (i: number, j: number) => {
    if (i < 0 || i >= n) return;
    if (j < 0 || j >= m) return;
    if (board[i][j] === '.') return;
    board[i][j] = '.';
    dfs(i - 1, j);
    dfs(i + 1, j);
    dfs(i, j - 1);
    dfs(i, j + 1);
  };

  // 对于每一个board[i][j] === 'X'的位置，设置为'.'，表示这个位置是一个统计过的战舰
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      if (board[i][j] === 'X') {
        ans++;
        dfs(i, j);
      }
    }
  }

  return ans;
}
```

## 枚举起点

题目要求的进阶是实现一次扫描，只能使用`O(1)`的空间复杂度，且不能修改原数组。

可以通过扫描每个战舰的起点，来计算战舰的数量。

起点的定义：

- board[i][j] === 'X'，表示当前位置是一个战舰的起点；
- 起点上方为空， board[i-1][j] == '.'；
- 起点左侧为空, board[i][j-1] == '.'；

```cpp
class Solution
{
public:
    int countBattleships(vector<vector<char>> &board)
    {
        int n = board.size(), m = board[0].size();
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                // 枚举战舰的起点
                if (board[i][j] == 'X')
                {
                    if (i > 0 && board[i - 1][j] == 'X')
                        continue;
                    if (j > 0 && board[i][j - 1] == 'X')
                        continue;
                    ans++;
                }
            }
        }
        return ans;
    }
};
```
