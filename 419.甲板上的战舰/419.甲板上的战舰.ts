/*
 * @lc app=leetcode.cn id=419 lang=typescript
 *
 * [419] 甲板上的战舰
 */

// @lc code=start
function countBattleships(board: string[][]): number {
  const n = board.length;
  const m = board[0].length;
  let ans = 0;

  // 将board[i][j] === 'X'相连的位置都设置为'.'
  const dfs = (i: number, j: number) => {
    if (i < 0 || i >= n) return;
    if (j < 0 || j >= m) return;
    if (board[i][j] === '.') return;
    board[i][j] = '.';
    dfs(i - 1, j);
    dfs(i + 1, j);
    dfs(i, j - 1);
    dfs(i, j + 1);
  };

  // 对于每一个board[i][j] === 'X'的位置，设置为'.'，表示这个位置是一个统计过的战舰
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      if (board[i][j] === 'X') {
        ans++;
        dfs(i, j);
      }
    }
  }

  return ans;
}
// @lc code=end
