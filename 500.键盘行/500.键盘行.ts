/*
 * @lc app=leetcode.cn id=500 lang=typescript
 *
 * [500] 键盘行
 */

// @lc code=start
function findWords(words: string[]): string[] {
  const ans = [];
  // 每个字母所在的键盘行号
  const row = '12210111011122000010020202';
  const base = 'a'.charCodeAt(0);
  for (let word of words) {
    // 单词第一个字符的行号
    const rowNumber = row[word[0].toLowerCase().charCodeAt(0) - base];
    let isValida = true;
    // 遍历判断其他字符行号是否和第一个匹配
    for (let i = 1; i < word.length; i++) {
      if (row[word[i].toLowerCase().charCodeAt(0) - base] !== rowNumber) {
        isValida = false;
        break;
      }
    }
    if (isValida) ans.push(word);
  }

  return ans;
}
// @lc code=end
