# 题目

给你一个字符串数组 `words` ，只返回可以使用在 美式键盘 同一行的字母打印出来的单词。键盘如下图所示。

**美式键盘** 中：

- 第一行由字符 "`qwertyuiop`" 组成。
- 第二行由字符 "`asdfghjkl`" 组成。
- 第三行由字符 "`zxcvbnm`" 组成。

提示：

- $1 \leq words.length \leq 20$
- $1 \leq words[i].length \leq 100$
- `words[i]` 由英文字母（小写和大写字母）组成

[![IpBeUg.md.png](https://z3.ax1x.com/2021/10/31/IpBeUg.md.png)](https://imgtu.com/i/IpBeUg)

# 示例

```
输入：words = ["Hello","Alaska","Dad","Peace"]
输出：["Alaska","Dad"]
```

```
输入：words = ["omk"]
输出：[]
```

# 题解

## 哈希表

预处理将每个字母的行号和字母关联。

遍历每个 word，检测每个字符的行号是否一致即可。

键盘大小写字母的位置是一致的，为了方便处理，将所有字符转换成小写字母来处理。

```ts
function findWords(words: string[]): string[] {
  const ans = [];
  // 每个字母所在的键盘行号
  const row = '12210111011122000010020202';
  const base = 'a'.charCodeAt(0);
  for (let word of words) {
    // 单词第一个字符的行号
    const rowNumber = row[word[0].toLowerCase().charCodeAt(0) - base];
    let isValida = true;
    // 遍历判断其他字符行号是否和第一个匹配
    for (let i = 1; i < word.length; i++) {
      if (row[word[i].toLowerCase().charCodeAt(0) - base] !== rowNumber) {
        isValida = false;
        break;
      }
    }
    if (isValida) ans.push(word);
  }

  return ans;
}
```
