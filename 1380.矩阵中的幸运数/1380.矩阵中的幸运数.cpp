/*
 * @lc app=leetcode.cn id=1380 lang=cpp
 *
 * [1380] 矩阵中的幸运数
 */

// @lc code=start
class Solution
{
public:
    vector<int> luckyNumbers(vector<vector<int>> &matrix)
    {
        int m = matrix.size(), n = matrix[0].size();
        vector<int> row_min(m, INT_MAX), col_max(n, INT_MIN);
        int i, j;
        for (i = 0; i < m; i++)
        {
            for (j = 0; j < n; j++)
            {
                row_min[i] = min(row_min[i], matrix[i][j]);
                col_max[j] = max(col_max[j], matrix[i][j]);
            }
        }

        vector<int> ans;
        for (i = 0; i < m; i++)
        {
            for (j = 0; j < n; j++)
            {
                if (row_min[i] == matrix[i][j] && col_max[j] == matrix[i][j])
                {
                    ans.push_back(row_min[i]);
                    break;
                }
            }
        }

        return ans;
    }
};
// @lc code=end
