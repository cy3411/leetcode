/*
 * @lc app=leetcode.cn id=1380 lang=typescript
 *
 * [1380] 矩阵中的幸运数
 */

// @lc code=start
function luckyNumbers(matrix: number[][]): number[] {
  const m = matrix.length;
  const n = matrix[0].length;
  const minRow = new Array(m).fill(Number.MAX_SAFE_INTEGER);
  const maxCol = new Array(n).fill(Number.MIN_SAFE_INTEGER);

  // 预处理每行的最小值和每列的最大值
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      minRow[i] = Math.min(minRow[i], matrix[i][j]);
      maxCol[j] = Math.max(maxCol[j], matrix[i][j]);
    }
  }

  const ans: number[] = [];
  // 计算答案
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      // 当前数字是否在每行的最小值和每列的最大值中
      if (matrix[i][j] === minRow[i] && matrix[i][j] === maxCol[j]) {
        ans.push(matrix[i][j]);
        break;
      }
    }
  }

  return ans;
}
// @lc code=end
