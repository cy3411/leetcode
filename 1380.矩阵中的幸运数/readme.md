# 题目

给你一个 `m * n` 的矩阵，矩阵中的数字 **各不相同** 。请你按 **任意** 顺序返回矩阵中的所有幸运数。

幸运数是指矩阵中满足同时下列两个条件的元素：

- 在同一行的所有元素中最小
- 在同一列的所有元素中最大

提示：

- $\color{burlywood} m \equiv mat.length$
- $\color{burlywood} n \equiv  mat[i].length$
- $\color{burlywood} 1 \leq n, m \leq 50$
- $\color{burlywood} 1 \leq matrix[i][j] \leq 10^5$
- 矩阵中的所有元素都是不同的

# 示例

```
输入：matrix = [[3,7,8],[9,11,13],[15,16,17]]
输出：[15]
解释：15 是唯一的幸运数，因为它是其所在行中的最小值，也是所在列中的最大值。
```

```
输入：matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
输出：[12]
解释：12 是唯一的幸运数，因为它是其所在行中的最小值，也是所在列中的最大值。
```

# 题解

## 预处理

预处理每行的最小值 `minRow` 和每列的最大值 `maxCol`。最后遍历矩阵，如果当前元素 `matrix[i][j]` 等于 `minRow[i]` 和 `maxCol[j]`，则是幸运数。

```ts
function luckyNumbers(matrix: number[][]): number[] {
  const m = matrix.length;
  const n = matrix[0].length;
  const minRow = new Array(m).fill(Number.MAX_SAFE_INTEGER);
  const maxCol = new Array(n).fill(Number.MIN_SAFE_INTEGER);

  // 预处理每行的最小值和每列的最大值
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      minRow[i] = Math.min(minRow[i], matrix[i][j]);
      maxCol[j] = Math.max(maxCol[j], matrix[i][j]);
    }
  }

  const ans: number[] = [];
  // 计算答案
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      // 当前数字是否在每行的最小值和每列的最大值中
      if (matrix[i][j] === minRow[i] && matrix[i][j] === maxCol[j]) {
        ans.push(matrix[i][j]);
        break;
      }
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> luckyNumbers(vector<vector<int>> &matrix)
    {
        int m = matrix.size(), n = matrix[0].size();
        vector<int> row_min(m, INT_MAX), col_max(n, INT_MIN);
        int i, j;
        for (i = 0; i < m; i++)
        {
            for (j = 0; j < n; j++)
            {
                row_min[i] = min(row_min[i], matrix[i][j]);
                col_max[j] = max(col_max[j], matrix[i][j]);
            }
        }

        vector<int> ans;
        for (i = 0; i < m; i++)
        {
            for (j = 0; j < n; j++)
            {
                if (row_min[i] == matrix[i][j] && col_max[j] == matrix[i][j])
                {
                    ans.push_back(row_min[i]);
                    break;
                }
            }
        }

        return ans;
    }
};
```
