# 题目

给你一个整数数组 `nums`，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。你只可以看到在滑动窗口内的 k 个数字。滑动窗口每次只向右移动一位。

返回滑动窗口中的最大值。

提示：

- 1 <= nums.length <= 105
- -104 <= nums[i] <= 104
- 1 <= k <= nums.length

# 示例

```
输入：nums = [1,3,-1,-3,5,3,6,7], k = 3
输出：[3,3,5,5,6,7]
解释：
滑动窗口的位置                最大值
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7
```

# 题解

## 双端队列

遍历数组，如果队尾元素小于当前元素，队尾元素出队，新元素入队，保持队首元素一定是最大的那个。

遍历数组的下标超过 k 时，就可以每次把队首元素放入结果。

```ts
function maxSlidingWindow(nums: number[], k: number): number[] {
  // 双端队列，存储元素下标
  const deque = [];
  const result = [];

  let i = 0;
  let size = nums.length;
  while (i < size) {
    // 如果队首元素下标超出范围，直接出队
    if (deque.length && deque[0] + k <= i) {
      deque.shift();
    }
    // 队首保留当前区间最大的元素
    while (deque.length && nums[deque[deque.length - 1]] < nums[i]) {
      deque.pop();
    }
    deque.push(i);

    i++;
    // 索引大于等于k,开始记录结果
    if (i >= k) {
      result.push(nums[deque[0]]);
    }
  }

  return result;
}
```

```ts
function maxSlidingWindow(nums: number[], k: number): number[] {
  // 单调队列
  const dequeue: number[] = [];
  const ans: number[] = [];
  for (let i = 0; i < nums.length; i++) {
    // 入队
    while (dequeue.length && nums[i] > nums[dequeue[dequeue.length - 1]]) {
      dequeue.pop();
    }
    dequeue.push(i);
    // 超出窗口大小，队首出队
    if (i - dequeue[0] === k) dequeue.shift();
    if (i + 1 < k) continue;

    ans.push(nums[dequeue[0]]);
  }

  return ans;
}
```
