int pivotIndex(int *nums, int numsSize)
{
    int pre_sum[100001] = {0};
    int i;
    for (i = 1; i <= numsSize; i++)
    {
        pre_sum[i] = pre_sum[i - 1] + nums[i - 1];
    }
    for (i = 0; i < numsSize; i++)
    {
        if (pre_sum[i] == pre_sum[numsSize] - pre_sum[i + 1])
        {
            return i;
        }
    }
    return -1;
}