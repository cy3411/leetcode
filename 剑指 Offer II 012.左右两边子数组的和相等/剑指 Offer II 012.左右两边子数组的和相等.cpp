/*
 * @lc app=leetcode.cn id=剑指 Offer II 012 lang=cpp
 *
 * [剑指 Offer II 012] 左右两边子数组的和相等
 */

// @lc code=start

class Solution
{
public:
    int pivotIndex(vector<int> &nums)
    {
        int i;
        int n = nums.size();
        vector<int> presum(n + 1, 0);
        for (i = 1; i <= n; i++)
        {
            presum[i] = presum[i - 1] + nums[i - 1];
        }
        for (i = 0; i < n; i++)
        {
            if (presum[i] == presum[n] - presum[i + 1])
            {
                return i;
            }
        }
        return -1;
    }
};
// @lc code=end