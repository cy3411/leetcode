// @algorithm @lc id=1000248 lang=typescript
// @title tvdfij
function pivotIndex(nums: number[]): number {
  const n = nums.length;
  const presum = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    presum[i] = presum[i - 1] + nums[i - 1];
  }

  for (let i = 0; i < n; i++) {
    if (presum[i] === presum[n] - presum[i + 1]) {
      return i;
    }
  }

  return -1;
}
