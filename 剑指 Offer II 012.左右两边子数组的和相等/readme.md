# 题目

给你一个整数数组 `nums` ，请计算数组的 **中心下标** 。

数组 **中心下标** 是数组的一个下标，其左侧所有元素相加的和等于右侧所有元素相加的和。

如果中心下标位于数组最左端，那么左侧数之和视为 `0` ，因为在下标的左侧不存在元素。这一点对于中心下标位于数组最右端同样适用。

如果数组有多个中心下标，应该返回 **最靠近左边** 的那一个。如果数组不存在中心下标，返回 `-1` 。

提示：

- $1 \leq nums.length \leq 10^4$
- $-1000 \leq nums[i] \leq 1000$

注意：本题与主站 724 题相同： https://leetcode-cn.com/problems/find-pivot-index/

# 示例

```
输入：nums = [1,7,3,6,5,6]
输出：3
解释：
中心下标是 3 。
左侧数之和 sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11 ，
右侧数之和 sum = nums[4] + nums[5] = 5 + 6 = 11 ，二者相等。
```

```
输入：nums = [2, 1, -1]
输出：0
解释：
中心下标是 0 。
左侧数之和 sum = 0 ，（下标 0 左侧不存在元素），
右侧数之和 sum = nums[1] + nums[2] = 1 + -1 = 0 。
```

# 题解

## 前缀和

题目要求的是中间元素左右两边区间和相等，那么我们可以考虑是使用前缀和来解决。

计算前缀和数组后，遍历前缀和数组，如果当前元素的左侧和等于右侧，那么就是中心下标。返回下标即可。

```ts
function pivotIndex(nums: number[]): number {
  const n = nums.length;
  const presum = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    presum[i] = presum[i - 1] + nums[i - 1];
  }

  for (let i = 0; i < n; i++) {
    if (presum[i] === presum[n] - presum[i + 1]) {
      return i;
    }
  }

  return -1;
}
```

```cpp
class Solution
{
public:
    int pivotIndex(vector<int> &nums)
    {
        int i;
        int n = nums.size();
        vector<int> presum(n + 1, 0);
        for (i = 1; i <= n; i++)
        {
            presum[i] = presum[i - 1] + nums[i - 1];
        }
        for (i = 0; i < n; i++)
        {
            if (presum[i] == presum[n] - presum[i + 1])
            {
                return i;
            }
        }
        return -1;
    }
};
```

```cpp
int pivotIndex(int *nums, int numsSize)
{
    int pre_sum[100001] = {0};
    int i;
    for (i = 1; i <= numsSize; i++)
    {
        pre_sum[i] = pre_sum[i - 1] + nums[i - 1];
    }
    for (i = 0; i < numsSize; i++)
    {
        if (pre_sum[i] == pre_sum[numsSize] - pre_sum[i + 1])
        {
            return i;
        }
    }
    return -1;
}
```
