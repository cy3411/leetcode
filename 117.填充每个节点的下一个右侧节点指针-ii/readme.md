# 题目

给定一个二叉树

```
struct Node {
  int val;
  Node *left;
  Node *right;
  Node *next;
}
```

填充它的每个 next 指针，让这个指针指向其下一个右侧节点。如果找不到下一个右侧节点，则将 next 指针设置为 NULL。

初始状态下，所有 next 指针都被设置为 NULL。

进阶：

- 你只能使用常量级额外空间。
- 使用递归解题也符合要求，本题中递归程序占用的栈空间不算做额外的空间复杂度。

提示：

- 树中的节点数小于 6000
- -100 <= node.val <= 100

# 示例

[![ffIbT0.md.png](https://z3.ax1x.com/2021/08/16/ffIbT0.md.png)](https://imgtu.com/i/ffIbT0)

```
输入：root = [1,2,3,4,5,null,7]
输出：[1,#,2,3,#,4,5,7,#]
解释：给定二叉树如图 A 所示，你的函数应该填充它的每个 next 指针，以指向其下一个右侧节点，如图 B 所示。序列化输出按层序遍历顺序（由 next 指针连接），'#' 表示每层的末尾。
```

# 题解

## 迭代，向下处理

如果使用广度优先，但是题目有一个进阶的要求，只能使用常量级额外空间。明显这个办法可行了。

我们可以考虑使用上层节点的信息来处理下层节点的 next 指针，这样可以在源数据上修改，满足进阶的条件。

我们利用当前节点子节点信息，挨个处理左右子节点，并完成 next 指针，返回下一层的头节点信息。

```ts
function connect(root: Node | null): Node | null {
  // 利用本层去处理下一层的next
  const layerConnect = (node: Node | null): Node | null => {
    // 当前层头一个节点指针
    let p = node;
    // 记录当前层上一个访问的节点指针
    let pre = null;
    // 当前层头一个节点
    let head = null;

    while (p) {
      if (p.left) {
        if (pre) pre.next = p.left;
        pre = p.left;
      }
      if (head === null) head = pre;
      if (p.right) {
        if (pre) pre.next = p.right;
        pre = p.right;
      }
      if (head === null) head = pre;
      // 如果有next节点，继续处理当层节点
      p = p.next;
    }

    return head;
  };

  let p: Node = root;
  // 每次返回下一层的完成next指针的头节点
  while (p) {
    p = layerConnect(p);
  }
  return root;
}
```
