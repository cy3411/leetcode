/*
 * @lc app=leetcode.cn id=1600 lang=typescript
 *
 * [1600] 皇位继承顺序
 */

// @lc code=start
class ThroneInheritance {
  edges: Map<string, string[]>;
  deads: Set<string>;
  kingName: string;
  constructor(kingName: string) {
    this.edges = new Map();
    this.deads = new Set();
    this.kingName = kingName;
  }

  birth(parentName: string, childName: string): void {
    if (!this.edges.get(parentName)) {
      this.edges.set(parentName, []);
    }
    this.edges.get(parentName).push(childName);
  }

  death(name: string): void {
    this.deads.add(name);
  }

  getInheritanceOrder(): string[] {
    const ans: string[] = [];
    const preorder = (name: string): void => {
      if (!this.deads.has(name)) {
        ans.push(name);
      }
      if (this.edges.has(name)) {
        for (const childName of this.edges.get(name)) {
          preorder(childName);
        }
      }
    };
    preorder(this.kingName);
    return ans;
  }
}

/**
 * Your ThroneInheritance object will be instantiated and called as such:
 * var obj = new ThroneInheritance(kingName)
 * obj.birth(parentName,childName)
 * obj.death(name)
 * var param_3 = obj.getInheritanceOrder()
 */
// @lc code=end
