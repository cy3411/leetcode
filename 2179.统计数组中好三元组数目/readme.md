# 题目

给你两个下标从 `0` 开始且长度为 `n` 的整数数组 `nums1` 和 `nums2` ，两者都是 `[0, 1, ..., n - 1]` 的 **排列** 。

**好三元组** 指的是 `3` 个 **互不相同** 的值，且它们在数组 `nums1` 和 `nums2` 中出现顺序保持一致。换句话说，如果我们将 $pos1_v$ 记为值 `v` 在 `nums1` 中出现的位置，$pos2_v$ 为值 `v` 在 `nums2` 中的位置，那么一个好三元组定义为 $0 \leq x, y, z \leq n - 1$ ，且 $pos1_x < pos1_y < pos1_z$ 和 $pos2_x < pos2_y < pos2_z$ 都成立的 `(x, y, z)` 。

请你返回好三元组的 总数目 。

提示：

- $n \equiv nums1.length \equiv nums2.length$
- $3 \leq n \leq 10^5$
- $0 \leq nums1[i], nums2[i] \leq n - 1$
- `nums1` 和 `nums2` 是 `[0, 1, ..., n - 1]` 的排列。

# 示例

```
输入：nums1 = [2,0,1,3], nums2 = [0,1,2,3]
输出：1
解释：
总共有 4 个三元组 (x,y,z) 满足 pos1x < pos1y < pos1z ，分别是 (2,0,1) ，(2,0,3) ，(2,1,3) 和 (0,1,3) 。
这些三元组中，只有 (0,1,3) 满足 pos2x < pos2y < pos2z 。所以只有 1 个好三元组。
```

```
输入：nums1 = [4,0,1,3,2], nums2 = [4,1,0,2,3]
输出：4
解释：总共有 4 个好三元组 (4,0,3) ，(4,0,2) ，(4,1,3) 和 (4,1,2) 。
```

# 题解

## 树状数组

枚举 nums1 中的每个元素 `x` ，找到 `x` 在 `nums1` 和 `nums2` 中位置**前**和**后**的相同元素的个数 `a` 和 `b` ，则以 `x` 为中心的好三元组数目为 `a * b` 。累加所有 `x` 对应的好三元组数目即可得到答案。

我们使用前缀和(涉及到区间更新，这里使用树状数组)来记录 `x` 在两个数组中位置前面相同元素的个数。

```
比如：nums1 = [2,0,1,3], nums2 = [0,1,2,3]

初始 preSum = [0,0,0,0,0]

遍历 nums1:

当前元素2，前缀和中位置2+1的值为0，更新 preSum = [0,0,0,1,1]
当前元素0，前缀和中位置0+1的值为0，更新 preSum = [0,1,1,2,2]
当前元素1，前缀和中位置1+1的值为1，更新 preSum = [0,1,1,2,2]

这里可以找到，元素1在两个数组中位置前有1个相同元素
```

定义 x 两个数组中位置前相同元素个数为 a，那么 x 在 nums1 位置前剩下的其他元素一定出现在 nums2 中 x 位置的后面，记作 diff。

那么 x 在 nums2 中位置后面的元素减去 diff，就是 x 在两个数组中位置后面相同元素的个数 b。

```ts
// 树状数组，维护当前元素前有几个元素在num1和num2中出现在当前元素的前面
class BIT {
  data: number[];
  n: number;
  constructor(n: number) {
    this.n = n;
    this.data = new Array(n + 1).fill(0);
  }
  private lowbit(x: number) {
    return x & -x;
  }

  add(x: number, val: number): void {
    while (x <= this.n) {
      this.data[x] += val;
      x += this.lowbit(x);
    }
  }

  query(x: number): number {
    let sum = 0;
    while (x) {
      sum += this.data[x];
      x -= this.lowbit(x);
    }
    return sum;
  }
}

function goodTriplets(nums1: number[], nums2: number[]): number {
  let n = nums1.length;
  // 构建树状数组，维护num2元素某个阶段是否出现，出现的话标记为1
  const ind = new BIT(n);

  // num2 元素的下标的映射关系
  const inxMap = new Map<number, number>();
  for (let i = 0; i < n; i++) {
    inxMap.set(nums2[i], i);
  }

  let ans = 0;
  // 遍历nums1，计算结果
  for (let i = 0; i < n; i++) {
    // 当前元素在nums2中的下标
    const j = inxMap.get(nums1[i]);
    // 当前元素在nums1和nums2中前面出现的相同元素个数
    const cnti = ind.query(j + 1);
    // 当前元素在nums1和nums2中前面出现的相同不同元素个数
    const diff = i - cnti;
    // 当前元素在nums1和nums2中后面出现的相同元素个数
    const cntj = n - j - 1 - diff;
    // 更新答案
    ans += cnti * cntj;
    // 更新树状数组
    ind.add(j + 1, 1);
  }
  return ans;
}
```
