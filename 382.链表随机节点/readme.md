# 题目

给定一个单链表，随机选择链表的一个节点，并返回相应的节点值。保证每个节点被选的概率一样。

进阶:
如果链表十分大且长度未知，如何解决这个问题？你能否使用常数级空间复杂度实现？

# 示例

```
// 初始化一个单链表 [1,2,3].
ListNode head = new ListNode(1);
head.next = new ListNode(2);
head.next.next = new ListNode(3);
Solution solution = new Solution(head);

// getRandom()方法应随机返回1,2,3中的一个，保证每个元素被返回的概率相等。
solution.getRandom();
```

# 题解

## 环形链表

将链表头尾相连，变成环形链表。

随机数访问链表节点。

```ts
class Solution {
  length: number;
  head: ListNode;
  constructor(head: ListNode | null) {
    this.head = head;
    this.length = 1;
    let p = head;
    while (p.next) {
      p = p.next;
      this.length++;
    }
    p.next = head;
  }

  getRandom(): number {
    let rand = (Math.random() * this.length) >> 0;
    let node = this.head;
    while (rand--) {
      node = node.next;
    }
    return node.val;
  }
}
```

## 蓄水池抽样

题目的进阶要求是大数据流中的随机抽样。

我们每次保留一个数，当遇到第 `i` 个数的时候，以 `1/i` 的概率保留它,`(i-1)/i` 的概率保留原来的数。

比如，1-10 个数：

- 遇到 1，只有 1 个数字，随机结果是 1
- 遇到 2，有 2 个数字，概率为 $\frac{1}{2}$，随机结果 1 和 2 各有 $\frac{1}{2}$ 几率
- 遇到 3，有 3 个数字，概率为 $\frac{1}{3}$,$\frac{2}{3}$ 的几率 1(假设之前 1 被保留) 被保留(此时 1 的总概率是 $\frac{2}{3}*\frac{1}{2}=\frac{1}{3}$)
- 遇到 4，有 4 个数字，概率为 $\frac{1}{4}$,$\frac{3}{4}$ 的几率 1(假设之前 1 被保留) 被保留(此时 1 的总概率是 $\frac{3}{4}*\frac{2}{3}*\frac{1}{2}=\frac{1}{4}$)

以此类推，每个数被保留的概率都是 1/N。

```ts
class Solution {
  head: ListNode;
  constructor(head: ListNode | null) {
    this.head = head;
  }

  getRandom(): number {
    let node = this.head;
    let ans: number;
    let i = 0;
    while (node) {
      i++;
      const rand = Math.ceil(Math.random() * i);
      if (rand === i) {
        ans = node.val;
      }
      node = node.next;
    }

    return ans;
  }
}
```

```cpp
class Solution
{
public:
    /** @param head The linked list's head.
        Note that the head is guaranteed to be not null, so it contains at least one node. */

    ListNode *headNode;
    Solution(ListNode *head)
    {
        headNode = head;
    }

    /** Returns a random node's value. */
    int getRandom()
    {
        ListNode *node = headNode;
        int ans;
        int i = 0;
        while (node)
        {
            i++;
            if (rand() % i == 0)
                ans = node->val;
            node = node->next;
        }
        return ans;
    }
};
```
