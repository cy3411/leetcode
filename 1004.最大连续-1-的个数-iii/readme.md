# 题目

给定一个由若干 `0` 和 `1` 组成的数组 `A`，我们最多可以将 `K` 个值从 `0` 变成 `1` 。

返回仅包含 `1` 的最长（连续）子数组的长度。

提示：

- $\color{burlywood}1 \leq A.length \leq 20000$
- $\color{burlywood}0 \leq K \leq A.length$
- `A[i]` 为 `0` 或 `1`

# 示例

```
输入：A = [1,1,1,0,0,0,1,1,1,1,0], K = 2
输出：6
解释：
[1,1,1,0,0,1,1,1,1,1,1]
```

```
输入：A = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], K = 3
输出：10
解释：
[0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
```

# 题解

## 滑动窗口

控制一个滑动窗口

- 如果窗口 0 的数量大于 k ，左边界向右移动，更新 0 的数量
- 如果窗口 0 的数量小于等于 k ，右边界向右移动，更新 0 的数量
- 同时更新窗口的长度，取最大值

```ts
function longestOnes(nums: number[], k: number): number {
  let l = 0;
  let r = -1;
  let n0 = 0;
  let ans = 0;

  while (1) {
    // 计算当前区间内的0个数
    if (n0 <= k) {
      r++;
      if (r >= nums.length) break;
      if (nums[r] === 0) n0++;
    } else {
      if (nums[l] === 0) n0--;
      l++;
    }
    // 如果区间内的0个数小于等于k，则更新答案
    if (n0 <= k) ans = Math.max(ans, r - l + 1);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int longestOnes(vector<int> &nums, int k)
    {
        int l = 0, r = 0, zeros = 0, ans = 0;

        while (r < nums.size())
        {
            if (zeros <= k)
            {
                if (nums[r] == 0)
                    zeros++;
                r++;
            }
            else
            {
                if (nums[l] == 0)
                    zeros--;
                l++;
            }
            if (zeros <= k)
                ans = max(ans, r - l);
        }

        return ans;
    }
};
```
