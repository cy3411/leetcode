/*
 * @lc app=leetcode.cn id=1004 lang=cpp
 *
 * [1004] 最大连续1的个数 III
 */

// @lc code=start
class Solution
{
public:
    int longestOnes(vector<int> &nums, int k)
    {
        int l = 0, r = 0, zeros = 0, ans = 0;

        while (r < nums.size())
        {
            if (zeros <= k)
            {
                if (nums[r] == 0)
                    zeros++;
                r++;
            }
            else
            {
                if (nums[l] == 0)
                    zeros--;
                l++;
            }
            if (zeros <= k)
                ans = max(ans, r - l);
        }

        return ans;
    }
};
// @lc code=end
