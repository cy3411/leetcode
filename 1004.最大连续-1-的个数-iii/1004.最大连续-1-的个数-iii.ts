/*
 * @lc app=leetcode.cn id=1004 lang=typescript
 *
 * [1004] 最大连续1的个数 III
 */

// @lc code=start
function longestOnes(nums: number[], k: number): number {
  let l = 0;
  let r = -1;
  let n0 = 0;
  let ans = 0;

  while (1) {
    // 计算当前区间内的0个数
    if (n0 <= k) {
      r++;
      if (r >= nums.length) break;
      if (nums[r] === 0) n0++;
    } else {
      if (nums[l] === 0) n0--;
      l++;
    }
    // 如果区间内的0个数小于等于k，则更新答案
    if (n0 <= k) ans = Math.max(ans, r - l + 1);
  }

  return ans;
}
// @lc code=end
