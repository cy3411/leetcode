/*
 * @lc app=leetcode.cn id=921 lang=typescript
 *
 * [921] 使括号有效的最少添加
 */

// @lc code=start
function minAddToMakeValid(s: string): number {
  let left = 0;
  let ans = 0;
  for (const x of s) {
    if (x === '(') left++;
    else {
      if (left > 0) left--;
      else ans++;
    }
  }

  return ans + left;
}
// @lc code=end
