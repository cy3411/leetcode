# 题目

给你一个二叉树的根结点 root ，请返回出现次数最多的子树元素和。如果有多个元素出现的次数相同，返回所有出现次数最多的子树元素和（不限顺序）。

一个结点的 「子树元素和」 定义为以该结点为根的二叉树上所有结点的元素之和（包括结点本身）。

# 示例

[![XjoSIg.png](https://s1.ax1x.com/2022/06/19/XjoSIg.png)](https://imgtu.com/i/XjoSIg)

```
输入: root = [5,2,-3]
输出: [2,-3,4]
```

# 题解

## 深度优先搜索

遍历二叉树，计算每个节点的子树元素和，并用哈希表记录每个子树元素和出现的次数。

最后将出现次数最多的子树元素和返回。

```ts
function findFrequentTreeSum(root: TreeNode | null): number[] {
  const cnt = new Map<number, number>();
  let maxCnt = 0;
  const dfs = (root: TreeNode | null): number => {
    if (root === null) return 0;
    const left = dfs(root.left);
    const right = dfs(root.right);
    const sum = root.val + left + right;
    cnt.set(sum, (cnt.get(sum) || 0) + 1);
    maxCnt = Math.max(maxCnt, cnt.get(sum) || 0);
    return sum;
  };
  dfs(root);
  const ans: number[] = [];
  for (const [k, v] of cnt) {
    if (v === maxCnt) ans.push(k);
  }
  return ans;
}
```

```cpp
class Solution {
public:
    unordered_map<int, int> cnt;
    int maxCnt = 0;
    int dfs(TreeNode *root) {
        if (root == NULL) return 0;
        int sum = root->val + dfs(root->left) + dfs(root->right);
        cnt[sum]++;
        maxCnt = max(maxCnt, cnt[sum]);
        return sum;
    }
    vector<int> findFrequentTreeSum(TreeNode *root) {
        dfs(root);
        vector<int> ans;
        for (auto x : cnt) {
            if (x.second == maxCnt) ans.push_back(x.first);
        }
        return ans;
    }
};
```
