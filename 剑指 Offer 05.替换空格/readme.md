# 题目
请实现一个函数，把字符串 `s` 中的每个空格替换成"`%20`"。

限制：
+ 0 <= s 的长度 <= 10000

# 示例
```
输入：s = "We are happy."
输出："We%20are%20happy."
```

# 题解
这个题目看着很简单，可以直接使用遍历拼接字符串或者正则替换，但是这样会生成很多字符串实例，会有内存占有问题。

考虑转换成字符数组来处理，如果是顺序遍历，每次替换空格后，后面的所有字符都会顺序后移，这就就会有时间上的多余消耗。

可以先统计空格数量，然后得到新的数组长度后，从后往前处理字符串，这样就只有`O(n)`的时间复杂度了。

PS:对于javascript来说，因为js的数组其实就是一种特殊的对象。所以不考虑数组所谓的连续内存空间的问题。

## 倒序遍历
```js
/**
 * @param {string} s
 * @return {string}
 */
var replaceSpace = function (s) {
  if (!s || s.length === 0) return '';

  const strArr = s.split('');

  let originLength = strArr.length;
  // 空格数量
  let numberOfBlank = 0;
  let i = 0;
  while (i < originLength) {
    if (strArr[i++] === ' ') numberOfBlank++;
  }
  // 新数组的长度
  let newLength = originLength + numberOfBlank * 2;
  // 扩充数组
  strArr.length = newLength;

  let originIndex = originLength - 1;
  let newIndex = newLength - 1;
  // 倒序复制数组内容
  while (originIndex >= 0 && originIndex < newIndex) {
    if (strArr[originIndex] === ' ') {
      strArr[newIndex--] = '0';
      strArr[newIndex--] = '2';
      strArr[newIndex--] = '%';
    } else {
      strArr[newIndex--] = strArr[originIndex];
    }
    originIndex--;
  }

  return strArr.join('');
};
```