/*
 * @lc app=leetcode.cn id=1470 lang=cpp
 *
 * [1470] 重新排列数组
 */

// @lc code=start
class Solution {
public:
    vector<int> shuffle(vector<int> &nums, int n) {
        int size = nums.size();
        int i = 0, j = n, idx = 0;
        vector<int> ans(size);
        while (j < size && i < j) {
            ans[idx] = nums[i++];
            ans[idx + 1] = nums[j++];
            idx += 2;
        }
        return ans;
    }
};
// @lc code=end
