# 题目

给你一个数组 `nums` ，数组中有 `2n` 个元素，按 $[x_1,x_2,...,x_n,y_1,y_2,...,y_n]$ 的格式排列。

请你将数组按 $[x_1,y_1,x_2,y_2,...,x_n,y_n]$ 格式重新排列，返回重排后的数组。

提示：

- $1 \leq n \leq 500$
- $nums.length \equiv 2n$
- $1 \leq nums[i] \leq 10^3$

# 示例

```
输入：nums = [2,5,1,3,4,7], n = 3
输出：[2,3,5,4,1,7]
解释：由于 x1=2, x2=5, x3=1, y1=3, y2=4, y3=7 ，所以答案为 [2,3,5,4,1,7]
```

```
输入：nums = [1,2,3,4,4,3,2,1], n = 4
输出：[1,4,2,3,3,2,4,1]
```

# 题解

## 双指针

定义双指针 i 和 j ， 分别指向 nums 的开始和中点，将指针指向的元素分别压入答案中，同时将指针后移，知道所有的元素都处理完毕。

```js
function shuffle(nums: number[], n: number): number[] {
  const size = nums.length;
  const ans = new Array(size);
  let idx = 0;
  for (let i = 0, j = n; i < j && j < size; i++, j++) {
    ans[idx] = nums[i];
    ans[idx + 1] = nums[j];
    idx += 2;
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<int> shuffle(vector<int> &nums, int n) {
        int size = nums.size();
        int i = 0, j = n, idx = 0;
        vector<int> ans(size);
        while (j < size && i < j) {
            ans[idx] = nums[i++];
            ans[idx + 1] = nums[j++];
            idx += 2;
        }
        return ans;
    }
};
```

```py
class Solution:
    def shuffle(self, nums: List[int], n: int) -> List[int]:
        size = len(nums)
        ans = [0] * size
        for i in range(n):
            ans[2 * i] = nums[i]
            ans[2 * i + 1] = nums[n + i]
        return ans
```
