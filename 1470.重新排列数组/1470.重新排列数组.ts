/*
 * @lc app=leetcode.cn id=1470 lang=typescript
 *
 * [1470] 重新排列数组
 */

// @lc code=start
function shuffle(nums: number[], n: number): number[] {
  const size = nums.length;
  const ans = new Array(size);
  let idx = 0;
  for (let i = 0, j = n; i < j && j < size; i++, j++) {
    ans[idx] = nums[i];
    ans[idx + 1] = nums[j];
    idx += 2;
  }

  return ans;
}
// @lc code=end
