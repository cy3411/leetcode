## 描述

给你一个字符串 s，请你将 s 分割成一些子串，使每个子串都是回文。

返回符合要求的 最少分割次数 。

## 示例

```
输入：s = "aab"
输出：1
解释：只需一次分割就可将 s 分割成 ["aa","b"] 这样两个回文子串。
```

```
输入：s = "a"
输出：0
```

## 方法

我们定义 dp[i] 为以下标为 i 的字符作为结尾的最小分割次数，那么最终答案为 dp[n - 1]

考虑分割的位置 j :

- 起点 0 到第 j 个字符能形成回文串，那么最小分割次数为 0。此时有 dp[j] = 0
- 起点 0 到第 j 个字符不能能形成回文串

  - 该字符独立消耗一次分割，dp[j] = dp[j-1] + 1
  - 该字符与前面 i 构成了回文，dp[j] = dp[i-1] + 1

```js
var minCut = function (s) {
  const size = s.length;
  // 预处理
  // isPal[i][j]是否为回文串，[i,j]区间的字符串
  const isPal = new Array(size).fill(0).map((_) => new Array(size).fill(true));
  for (let i = size - 1; i >= 0; i--) {
    for (let j = i + 1; j < size; j++) {
      isPal[i][j] = s[i] === s[j] && isPal[i + 1][j - 1];
    }
  }

  // 记录当前位置需要分割几次
  const dp = new Array(size).fill(Number.POSITIVE_INFINITY);
  for (let i = 0; i < size; i++) {
    if (isPal[0][i]) {
      dp[i] = 0;
    } else {
      for (let j = 0; j < i; j++) {
        if (isPal[j + 1][i]) {
          dp[i] = Math.min(dp[i], dp[j] + 1);
        }
      }
    }
  }

  return dp[size - 1];
};
```

```ts
function minCut(s: string): number {
  // 预处理，构建回文串的索引
  const extract = (s: string, i: number, j: number, index: number[][]) => {
    const n = s.length;
    while (i >= 0 && s[i] === s[j]) {
      index[j + 1].push(i);
      i--;
      j++;
    }
  };
  const n = s.length;
  // index[i] 表示以 s[i] 结尾的回文串的起始位置
  // index[3] = [1,2] 表示 [1,3)和[2,3)的区间都能构成回文串
  const index = new Array(n + 1).fill(0).map((_) => []);

  for (let i = 0; i < n; i++) {
    extract(s, i, i, index);
    extract(s, i, i + 1, index);
  }
  // dp[i]表示前i个字符有最少几个回文串
  const dp = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    // 每个字符本身都是回文串
    dp[i] = i;
    for (const j of index[i]) {
      dp[i] = Math.min(dp[i], dp[j] + 1);
    }
  }

  // 要返回分割次数，所以需要减 1
  return dp[n] - 1;
}
```
