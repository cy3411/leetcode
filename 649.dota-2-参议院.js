/*
 * @lc app=leetcode.cn id=649 lang=javascript
 *
 * [649] Dota2 参议院
 */

// @lc code=start
/**
 * @param {string} senate
 * @return {string}
 */
var predictPartyVictory = function (senate) {
  const radiant = [];
  const dire = [];
  const turns = senate.length;
  // 记录索引，表示轮流投票顺序
  for (const [index, char] of Array.from(senate).entries()) {
    if (char === 'R') {
      radiant.push(index);
    } else {
      dire.push(index);
    }
  }

  while (radiant.length && dire.length) {
    // 投过票的改变顺序后重新插入数组
    if (radiant[0] < dire[0]) {
      radiant.push(radiant[0] + turns);
    } else {
      dire.push(dire[0] + turns);
    }
    radiant.shift();
    dire.shift();
  }

  return radiant.length ? 'Radiant' : 'Dire';
};
// @lc code=end
