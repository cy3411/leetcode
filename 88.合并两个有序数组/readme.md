# 题目
给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。

说明:

+ 初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。
+ 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素。


# 示例
```
输入:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

输出: [1,2,2,3,5,6]
```

# 题解
## 合并排序
最直观的方法是先将数组 nums2 放进数组 nums1 的尾部，然后直接对整个数组进行排序。
```js
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  nums1.splice(m, nums1.length - m, ...nums2);
  nums1.sort((a, b) => a - b);
};
```
复杂度分析
+ 时间复杂度：`O((m+n)log⁡(m+n))`
+ 空间复杂度：`O(log⁡(m+n))`


## 双指针
由于两个数组已经排序了，我们将两个数组看作队列，每次取两个数组头部元素比较，较小的放入结果。
```js
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  const temp = nums1.slice(0, m);
  nums1.length = 0;

  let i = 0;
  let j = 0;

  while (i < m && j < n) {
    if (temp[i] < nums2[j]) {
      nums1.push(temp[i]);
      i++;
    } else {
      nums1.push(nums2[j]);
      j++;
    }
  }
  // 将剩余的元素直接插入到结果数组中
  if (i < m) nums1.push(...temp.slice(i));
  if (j < n) nums1.push(...nums2.slice(j));
};
```
复杂度分析
+ 时间复杂度：`O(m+n)`
+ 空间复杂度：`O(m+n)`


## 逆向双指针
题目还给出了假设nums1有足够的空间，也就是说num1数组的后半部分是空的，可以直接覆盖，不影响结果。

因此可以逆序遍历，将比较的较大值放入nums1的后面。

```js
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  let i = m - 1;
  let j = n - 1;
  let tail = m + n - 1;
  let curr;

  while (i >= 0 || j >= 0) {
    if (i === -1) {
      // nums1处理完了，把nums2剩余的存入结果
      curr = nums2[j--];
    } else if (j === -1) {
      // nums2处理完了，把nums1剩余的存入结果
      curr = nums1[i--];
    } else if (nums1[i] > nums2[j]) {
      // 双方比较，较大值放入结果
      curr = nums1[i--];
    } else {
      curr = nums2[j--];
    }
    // 逆向改变结果数组的值
    nums1[tail--] = curr;
  }
};
```
复杂度分析
+ 时间复杂度：`O(m+n)`
+ 空间复杂度：`O(1)`

