/*
 * @lc app=leetcode.cn id=88 lang=javascript
 *
 * [88] 合并两个有序数组
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  let i = m - 1;
  let j = n - 1;
  let tail = m + n - 1;
  let curr;

  while (i >= 0 || j >= 0) {
    if (i === -1) {
      // nums1处理完了，把nums2剩余的存入结果
      curr = nums2[j--];
    } else if (j === -1) {
      // nums2处理完了，把nums1剩余的存入结果
      curr = nums1[i--];
    } else if (nums1[i] > nums2[j]) {
      // 双方比较，较大值放入结果
      curr = nums1[i--];
    } else {
      curr = nums2[j--];
    }
    // 逆向改变结果数组的值
    nums1[tail--] = curr;
  }
};
// @lc code=end
