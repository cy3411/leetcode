# 题目

小朋友 A 在和 ta 的小伙伴们玩传信息游戏，游戏规则如下：

有 n 名玩家，所有玩家编号分别为 0 ～ n-1，其中小朋友 A 的编号为 0
每个玩家都有固定的若干个可传信息的其他玩家（也可能没有）。传信息的关系是单向的（比如 A 可以向 B 传信息，但 B 不能向 A 传信息）。
每轮信息必须需要传递给另一个人，且信息可重复经过同一个人
给定总玩家数 n，以及按 [玩家编号,对应可传递玩家编号] 关系组成的二维数组 relation。返回信息从小 A (编号 0 ) 经过 k 轮传递到编号为 n-1 的小伙伴处的方案数；若不能到达，返回 0。

# 示例

```
输入：n = 5, relation = [[0,2],[2,1],[3,4],[2,3],[1,4],[2,0],[0,4]], k = 3

输出：3

解释：信息从小 A 编号 0 处开始，经 3 轮传递，到达编号 4。共有 3 种方案，分别是 0->2->0->4， 0->2->1->4， 0->2->3->4。
```

# 题解

## 回溯

其实用 0 为条件，选择符合条件的下一个编号，递归，继续选择。

如果选择的编号等于 k，并且最后一个编号等于 n-1，表示是一个合法的结果，更新答案。

如果没有合适的，就回溯回去。找其他选择。

这里要注意的就是 relation 里是可以重复选择的。

```js
/**
 * @param {number} n
 * @param {number[][]} relation
 * @param {number} k
 * @return {number}
 */
var numWays = function (n, relation, k) {
  const m = relation.length;
  const path = [];
  const backtrack = (code) => {
    if (path.length === k) {
      // 满足条件，更新答案
      if (code === n - 1) ans++;
      return;
    }
    for (let i = 0; i < m; i++) {
      let [a, b] = relation[i];
      if (a !== code) continue;
      // 选择下一个编号
      path.push(b);
      backtrack(b);
      // 取消选择
      path.pop();
    }
  };
  let ans = 0;

  backtrack(0);

  return ans;
};
```
