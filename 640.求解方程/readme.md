# 题目

求解一个给定的方程，将 `x` 以字符串 `"x=#value"` 的形式返回。该方程仅包含 `'+'` ， `'-'` 操作，变量 `x` 和其对应系数。

如果方程没有解，请返回 "No solution" 。如果方程有无限解，则返回 “Infinite solutions” 。

题目保证，如果方程中只有一个解，则 `'x'` 的值是一个整数。

提示:

- $3 <= equation.length <= 1000$
- `equation` 只有一个 `'='`.
- `equation` 方程由整数组成，其绝对值在 `[0, 100]` 范围内，不含前导零和变量 `'x'` 。

# 示例

```
输入: equation = "x+5-3+x=6+x-2"
输出: "x=2"
```

```
输入: equation = "x=x"
输出: "Infinite solutions"
```

# 题解

## 模拟

我们可以模拟将等式右项移动到左项，然后将同类项合并，最后求解。

定义 factor 和 constant 分别保存等式的系数和常数。

如果最后 factor 为 0 ， 那么 x 对方程式没有影响，再判断 constant 为否为 0，如果为 0，则方程式有无数解，返回 "Infinite solutions"，否则返回 "No solution"。其他情况，返回 $\frac{-constant}{factor}$。

```js
function solveEquation(equation: string): string {
  // 因数
  let factor = 0;
  // 常数
  let constant = 0;
  let n = equation.length;
  let idx = 0;
  // 默认等号左边符号
  let sign = 1;
  while (idx < n) {
    // 等号右边，默认符号位负
    if (equation[idx] === '=') {
      sign = -1;
      idx++;
      continue;
    }

    // 临时符号，处理数字正负
    let signTemp = sign;
    let num = 0;
    // 处理是否是数字
    let isNumber = false;
    if (equation[idx] === '-' || equation[idx] === '+') {
      signTemp = equation[idx] === '-' ? -sign : sign;
      idx++;
    }
    // 处理数字
    while (idx < n && equation[idx] >= '0' && equation[idx] <= '9') {
      num = num * 10 + Number(equation[idx]);
      idx++;
      isNumber = true;
    }
    // 处理是否是因数
    if (idx < n && equation[idx] === 'x') {
      factor += isNumber ? signTemp * num : signTemp;
      idx++;
    } else {
      constant += signTemp * num;
    }
  }

  if (factor === 0) {
    return constant === 0 ? 'Infinite solutions' : 'No solution';
  }

  return `x=${-constant / factor}`;
}
```
