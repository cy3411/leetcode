# 题目

有 n 个网络节点，标记为 1 到 n。

给你一个列表 times，表示信号经过 有向 边的传递时间。 times[i] = (ui, vi, wi)，其中 ui 是源节点，vi 是目标节点， wi 是一个信号从源节点传递到目标节点的时间。

现在，从某个节点 K 发出一个信号。需要多久才能使所有节点都收到信号？如果不能使所有节点收到信号，返回 -1 。

提示：

- 1 <= k <= n <= 100
- 1 <= times.length <= 6000
- times[i].length == 3
- 1 <= ui, vi <= n
- ui != vi
- 0 <= wi <= 100
- 所有 (ui, vi) 对都 互不相同（即，不含重复边）

# 示例

[![fC1J2t.png](https://z3.ax1x.com/2021/08/02/fC1J2t.png)](https://imgtu.com/i/fC1J2t)

```
输入：times = [[2,1,1],[2,3,1],[3,4,1]], n = 4, k = 2
输出：2
```

# 题解

## 最短路径(Dijkstra 算法)

根据题意，从 k 点到达 x 点的时间就是 k 点到 x 点的最短路径。因此我们需要求出 k 点到所有节点的最短路径，其中的最大值就是答案。如果存在 k 点无法到达的节点，就返回-1。

详情可以看这里：[Dijkstra 算法](https://oi-wiki.org/graph/shortest-path/#dijkstra)

```ts
function networkDelayTime(times: number[][], n: number, k: number): number {
  const graph = new Array(n).fill(0).map((_) => new Array(n).fill(Number.POSITIVE_INFINITY));
  // 建模
  for (let [from, to, time] of times) {
    graph[from - 1][to - 1] = time;
  }

  const visited = new Array(n).fill(0);
  const dist = new Array(n).fill(Number.POSITIVE_INFINITY);
  // 起点
  dist[k - 1] = 0;

  for (let i = 0; i < n; i++) {
    let x = -1;
    // 先找到最小的顶点
    for (let y = 0; y < n; y++) {
      if (!visited[y] && (x === -1 || dist[y] < dist[x])) {
        x = y;
      }
    }
    visited[x] = 1;
    // 更新所有跟最小顶点连接的点的边的权重
    for (let y = 0; y < n; y++) {
      dist[y] = Math.min(dist[y], dist[x] + graph[x][y]);
    }
  }
  // 所有最小边中的最大值
  const ans = Math.max(...dist);

  return isFinite(ans) ? ans : -1;
}
```
