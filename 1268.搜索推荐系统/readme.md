# 题目

给你一个产品数组 `products` 和一个字符串 `searchWord` ，`products` 数组中每个产品都是一个字符串。

请你设计一个推荐系统，在依次输入单词 `searchWord` 的每一个字母后，推荐 `products` 数组中前缀与 `searchWord` 相同的最多三个产品。如果前缀相同的可推荐产品超过三个，请按**字典序**返回最小的三个。

请你以二维列表的形式，返回在输入 `searchWord` 每个字母后相应的推荐产品的列表。

提示：

- $1 \leq products.length \leq 1000$
- $1 \leq Σ products[i].length \leq 2 * 10^4$
- `products[i]` 中所有的字符都是小写英文字母。
- $1 \leq searchWord.length \leq 1000$
- `searchWord` 中所有字符都是小写英文字母。

# 示例

```
输入：products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
输出：[
["mobile","moneypot","monitor"],
["mobile","moneypot","monitor"],
["mouse","mousepad"],
["mouse","mousepad"],
["mouse","mousepad"]
]
解释：按字典序排序后的产品列表是 ["mobile","moneypot","monitor","mouse","mousepad"]
输入 m 和 mo，由于所有产品的前缀都相同，所以系统返回字典序最小的三个产品 ["mobile","moneypot","monitor"]
输入 mou， mous 和 mouse 后系统都返回 ["mouse","mousepad"]
```

```
输入：products = ["havana"], searchWord = "havana"
输出：[["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
```

# 题解

## Tire 树

利用 `Tire` 树将 `products` 中的元素全部插入，同时将遍历的 `products` 元素保存 `Trie` 树中节点中，并按照题意字典序排序且最多 `3` 个。

最后使用 `searchWord` 去 `Trie` 树中搜索，将匹配节点中的 `products` 元素放入答案。

```ts
class TireNode {
  isWord: boolean;
  next: Map<string, any>;
  // 保存当前前缀的products
  products: string[];
  constructor() {
    this.isWord = false;
    this.next = new Map();
    this.products = [];
  }
}

class Tire {
  root: TireNode;
  constructor() {
    this.root = new TireNode();
  }

  insert(word: string) {
    let node = this.root;
    for (let char of word) {
      if (!node.next.has(char)) {
        node.next.set(char, new TireNode());
      }
      node = node.next.get(char);
      node.products.push(word);
      node.products.sort();
      // 维持最多3个product
      if (node.products.length > 3) node.products.pop();
    }
    if (node.isWord) return false;
    node.isWord = true;
    return true;
  }

  search(word: string): string[][] {
    let node = this.root;
    const res: string[][] = [];
    for (let char of word) {
      if (node.next.has(char)) {
        node = node.next.get(char);
        res.push(node.products);
      } else {
        // 树中没有后续字符了
        // 方便后面继续生成空数组
        node = new TireNode();
        res.push([]);
      }
    }
    return res;
  }
}

function suggestedProducts(products: string[], searchWord: string): string[][] {
  const tire = new Tire();
  for (let product of products) {
    tire.insert(product);
  }

  return tire.search(searchWord);
}
```
