#
# @lc app=leetcode.cn id=1759 lang=python3
#
# [1759] 统计同构子字符串的数目
#

# @lc code=start
class Solution:
    def countHomogenous(self, s: str) -> int:
        mod = 1e9+7
        ans = 0
        for _, g in groupby(s):
            n = len(list(g))
            ans += n * (n + 1) // 2 % mod

        return int(ans)
# @lc code=end
