# 题目

给你一个字符串 `s` ，返回 `s` 中 **同构子字符串** 的数目。由于答案可能很大，只需返回对 $10^9 + 7$ **取余** 后的结果。

**同构字符串** 的定义为：如果一个字符串中的所有字符都相同，那么该字符串就是同构字符串。

**子字符串** 是字符串中的一个连续字符序列。

提示：

- $1 \leq s.length \leq 10^5$
- `s` 由小写字符串组成

# 示例

```
输入：s = "abbcccaa"
输出：13
解释：同构子字符串如下所列：
"a"   出现 3 次。
"aa"  出现 1 次。
"b"   出现 2 次。
"bb"  出现 1 次。
"c"   出现 3 次。
"cc"  出现 2 次。
"ccc" 出现 1 次。
3 + 1 + 2 + 1 + 3 + 2 + 1 = 13
```

```
输入：s = "xy"
输出：2
解释：同构子字符串是 "x" 和 "y" 。
```

# 题解

## 数学

假设一个同构字符串的长度为 n , 那么它的子字符串的数量为 $\frac{n*(n+1)}{2}$。

遍历字符串，我们统计连续相同字符串的长度，根据上述公式计算同构子串的数量。

```ts
function countHomogenous(s: string): number {
  const mod = 1e9 + 7;
  let prev = s[0];
  let cnt = 0;
  let ans = 0;
  for (const c of s) {
    if (prev === c) {
      cnt++;
    } else {
      ans += ((cnt * (cnt + 1)) / 2) % mod;
      prev = c;
      cnt = 1;
    }
  }
  // 处理最后的字串
  ans += ((cnt * (cnt + 1)) / 2) % mod;

  return ans;
}
```

```py
class Solution:
    def countHomogenous(self, s: str) -> int:
        mod = 1e9+7
        ans = 0
        for _, g in groupby(s):
            n = len(list(g))
            ans += n * (n + 1) // 2 % mod

        return int(ans)
```
