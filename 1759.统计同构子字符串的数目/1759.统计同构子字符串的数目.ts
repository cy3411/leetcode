/*
 * @lc app=leetcode.cn id=1759 lang=typescript
 *
 * [1759] 统计同构子字符串的数目
 */

// @lc code=start
function countHomogenous(s: string): number {
  const mod = 1e9 + 7;
  let prev = s[0];
  let cnt = 0;
  let ans = 0;
  for (const c of s) {
    if (prev === c) {
      cnt++;
    } else {
      ans += ((cnt * (cnt + 1)) / 2) % mod;
      prev = c;
      cnt = 1;
    }
  }
  // 处理最后的字串
  ans += ((cnt * (cnt + 1)) / 2) % mod;

  return ans;
}
// @lc code=end
