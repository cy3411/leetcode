# 题目

给定一个包含 `n` 个整数的数组 `nums`，判断 `nums` 中是否存在三个元素 `a` ，`b` ，`c` ，使得 `a + b + c = 0` 。请找出所有和为 `0` 且 **不重复** 的三元组。

提示：

- $0 \leq nums.length \leq 3000$
- $-10^5 \leq nums[i] \leq 10^5$

注意：[本题与主站 15 题相同](https://leetcode-cn.com/problems/3sum/)

# 示例

```
输入：nums = [-1,0,1,2,-1,-4]
输出：[[-1,-1,2],[-1,0,1]]
```

```
输入：nums = []
输出：[]
```

# 题解

## 排序+双指针

nums 升序排序， 枚举 nums 中的每个元素 `a`，然后在剩下的元素中使用双指针指向数组的两端，枚举 `b` 和 `c`，使得 `b + c = 0 - a`。

- 若 `b + c > 0 - a`，b 向右移动一位
- 若 `b + c < 0 - a`，c 向左移动一位

枚举 `a` 和 查找 `b`、 `c` 的过程中，需要去重，保证答案的不重复性。

```ts
function threeSum(nums: number[]): number[][] {
  nums.sort((a, b) => a - b);
  const n = nums.length;
  const ans = [];
  for (let i = 0; i < n && nums[i] <= 0; i++) {
    // 去重
    if (i > 0 && nums[i] === nums[i - 1]) continue;
    const target = 0 - nums[i];
    let l = i + 1;
    let r = n - 1;
    while (l < r) {
      if (nums[l] + nums[r] === target) {
        ans.push([nums[i], nums[l], nums[r]]);
        // 去重
        while (l < r && nums[l] === nums[l + 1]) {
          l++;
        }
        l++;
      } else if (nums[l] + nums[r] > target) {
        r--;
      } else {
        l++;
      }
    }
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    vector<vector<int>> threeSum(vector<int> &nums)
    {
        sort(nums.begin(), nums.end());
        vector<vector<int>> ans;
        int n = nums.size();
        for (int i = 0; i < n && nums[i] <= 0; i++)
        {
            if (i > 0 && nums[i] == nums[i - 1])
            {
                continue;
            }
            int target = 0 - nums[i];
            int l = i + 1, r = n - 1;
            while (l < r)
            {
                int sum = nums[l] + nums[r];
                if (sum == target)
                {
                    ans.push_back(vector<int>{nums[i], nums[l], nums[r]});
                    while (l < r && nums[l] == nums[l + 1])
                    {
                        l++;
                    }
                    l++;
                }
                else if (sum > target)
                {
                    r--;
                }
                else
                {
                    l++;
                }
            }
        }
        return ans;
    }
};
```
