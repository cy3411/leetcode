# 描述
实现一个基本的计算器来计算一个简单的字符串表达式 s 的值。
# 示例
```
输入：s = "1 + 1"
输出：2
```
```
输入：s = "(1+(4+5+2)-3)+(6+8)"
输出：23
```
# 方法
字符串除了数字和括号外，只有加号和减号两种运算符。
取值{-1,1}来表示当前的符号。

## 括号展开+栈的方式
如果当前位置处于一系列括号之内，则也与这些括号前面的运算符有关：每当遇到一个以 - 号开头的括号，则意味着此后的符号都要被「翻转」。
```js
var calculate = function (s) {
  const operator = [1];
  let sign = 1;
  let result = 0;
  let i = 0;

  while (i < s.length) {
    let c = s[i];
    if (/\s/.test(c)) {
      i++;
    } else if (c === '+') {
      sign = operator[operator.length - 1];
      i++;
    } else if (c === '-') {
      sign = -operator[operator.length - 1];
      i++;
    } else if (c === '(') {
      operator.push(sign);
      i++;
    } else if (c === ')') {
      operator.pop();
      i++;
    } else {
      let num = 0;
      while (s[i] !== ' ' && i < s.length && !isNaN(Number(s[i]))) {
        let c = s[i];
        num = num * 10 + Number(c);
        i++;
      }
      result += num * sign;
    }
  }
  return result;
};
```

## 括号不展开+栈
将括号内的结果和相关符号压入栈中，遇到')'后处理结果
```js
var calculate = function (s) {
  const stack = [];
  let sign = 1;
  let num = 0;
  let result = 0;

  for (let c of s) {
    if (/\d/.test(c)) {
      num = num * 10 + Number(c);
    } else if (c === '+' || c === '-') {
      result += num * sign;
      num = 0;
      sign = c === '+' ? 1 : -1;
    } else if (c === '(') {
      stack.push(result);
      stack.push(sign);
      result = 0;
      sign = 1;
    } else if (c === ')') {
      result += num * sign;
      result *= stack.pop();
      result += stack.pop();
      num = 0;
    }
  }
  // 最后处理结尾不是括号的内容
  result += num * sign;

  return result;
};
```