/*
 * @lc app=leetcode.cn id=1796 lang=cpp
 *
 * [1796] 字符串中第二大的数字
 */

// @lc code=start
class Solution {
public:
    int secondHighest(string s) {
        int fir = -1, sec = -1;
        for (char &c : s) {
            if (c >= 'a' && c <= 'z') continue;
            int x = c - '0';
            if (x > fir) {
                sec = fir;
                fir = x;
            } else if (sec < x && x < fir) {
                sec = x;
            }
        }
        return sec;
    }
};
// @lc code=end
