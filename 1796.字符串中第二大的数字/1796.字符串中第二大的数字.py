#
# @lc app=leetcode.cn id=1796 lang=python3
#
# [1796] 字符串中第二大的数字
#

# @lc code=start
class Solution:
    def secondHighest(self, s: str) -> int:
        fir, sec = -1, -1
        for c in s:
            if c.isdigit():
                x = int(c)
                if x > fir:
                    sec = fir
                    fir = x
                elif x > sec and x < fir:
                    sec = x
        return sec
# @lc code=end

