/*
 * @lc app=leetcode.cn id=1796 lang=typescript
 *
 * [1796] 字符串中第二大的数字
 */

// @lc code=start
function secondHighest(s: string): number {
  // 最大数字
  let fir = -1;
  // 次大数字
  let sec = -1;
  for (const c of s) {
    if (c >= 'a' && c <= 'z') continue;
    const x = Number(c);
    if (x > fir) {
      sec = fir;
      fir = x;
    } else if (x < fir && x > sec) {
      sec = x;
    }
  }
  return sec;
}
// @lc code=end
