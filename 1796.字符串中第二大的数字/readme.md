# 题目

给你一个混合字符串 `s` ，请你返回 `s` 中 **第二大** 的数字，如果不存在第二大的数字，请你返回 `-1` 。

混合字符串 由小写英文字母和数字组成。

提示：

- $1 <= s.length <= 500$
- `s` 只包含小写英文字母和（或）数字。

# 示例

```
输入：s = "dfa12321afd"
输出：2
解释：出现在 s 中的数字包括 [1, 2, 3] 。第二大的数字是 2 。
```

```
输入：s = "abc1111"
输出：-1
解释：出现在 s 中的数字只包含 [1] 。没有第二大的数字。
```

# 题解

## 遍历

定义 fir 和 sec 分别保存最大和次大的数字，遍历 s ，遇到数字 x:

- 如果 x > fir ，x 是最大的数字，更新 sec = fir, fir = x;
- 如果 sec < x < fir，最大数字是 fir， 次大是 x，更新 sec = x;

最后返回 sec。

```ts
function secondHighest(s: string): number {
  // 最大数字
  let fir = -1;
  // 次大数字
  let sec = -1;
  for (const c of s) {
    if (c >= 'a' && c <= 'z') continue;
    const x = Number(c);
    if (x > fir) {
      sec = fir;
      fir = x;
    } else if (x < fir && x > sec) {
      sec = x;
    }
  }
  return sec;
}
```

```cpp
class Solution {
public:
    int secondHighest(string s) {
        int fir = -1, sec = -1;
        for (char &c : s) {
            if (c >= 'a' && c <= 'z') continue;
            int x = c - '0';
            if (x > fir) {
                sec = fir;
                fir = x;
            } else if (sec < x && x < fir) {
                sec = x;
            }
        }
        return sec;
    }
};
```

```py
class Solution:
    def secondHighest(self, s: str) -> int:
        fir, sec = -1, -1
        for c in s:
            if c.isdigit():
                x = int(c)
                if x > fir:
                    sec = fir
                    fir = x
                elif x > sec and x < fir:
                    sec = x
        return sec
```
