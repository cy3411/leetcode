/*
 * @lc app=leetcode.cn id=513 lang=cpp
 *
 * [513] 找树左下角的值
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left),
 * right(right) {}
 * };
 */
class Solution {
public:
    int maxDepth = -1;
    void dfs(TreeNode *root, int depth, int &ans) {
        if (root == NULL) return;
        if (depth > maxDepth) {
            ans = root->val;
            maxDepth = depth;
        }
        dfs(root->left, depth + 1, ans);
        dfs(root->right, depth + 1, ans);
    }

    int findBottomLeftValue(TreeNode *root) {
        int ans = 0;
        dfs(root, 0, ans);
        return ans;
    }
};
// @lc code=end
