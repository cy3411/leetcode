# 题目

给定一个二叉树，在树的最后一行找到最左边的值。

# 示例

```
输入:

    2
   / \
  1   3

输出:
1
```

# 题解

遍历树的每一个节点，只记录每层节点的第一个值。

## 广度优先

```ts
function findBottomLeftValue(root: TreeNode | null): number {
  const queue = [root];
  let ans: number;
  while (queue.length) {
    let n = queue.length;
    let first = true;
    while (n--) {
      let node = queue.shift();
      // 每次记录每层的第一个值
      if (first) {
        ans = node.val;
        first = false;
      }
      node.left && queue.push(node.left);
      node.right && queue.push(node.right);
    }
  }
  return ans;
}
```

```py
class Solution:
    def findBottomLeftValue(self, root: Optional[TreeNode]) -> int:
        que = collections.deque([root])
        ans = root.val
        while que:
            n = len(que)
            flag = True
            for i in range(n):
                node = que.popleft()
                if flag:
                    ans = node.val
                    flag = False
                if node.left:
                    que.append(node.left)
                if node.right:
                    que.append(node.right)

        return ans
```

## 深度优先

```ts
function findBottomLeftValue(root: TreeNode | null): number {
  let maxDeepth = -1;
  let ans = 0;

  const dfs = (root: TreeNode | null, deepth: number): void => {
    if (root === null) return;
    // 第一次进入，记录val
    if (deepth > maxDeepth) {
      maxDeepth = deepth;
      ans = root.val;
    }
    dfs(root.left, deepth + 1);
    dfs(root.right, deepth + 1);
  };

  dfs(root, 0);
  return ans;
}
```

```cpp
class Solution {
public:
    int maxDepth = -1;
    void dfs(TreeNode *root, int depth, int &ans) {
        if (root == NULL) return;
        if (depth > maxDepth) {
            ans = root->val;
            maxDepth = depth;
        }
        dfs(root->left, depth + 1, ans);
        dfs(root->right, depth + 1, ans);
    }

    int findBottomLeftValue(TreeNode *root) {
        int ans = 0;
        dfs(root, 0, ans);
        return ans;
    }
};
```
