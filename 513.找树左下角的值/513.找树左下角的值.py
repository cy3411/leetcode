#
# @lc app=leetcode.cn id=513 lang=python3
#
# [513] 找树左下角的值
#

# @lc code=start
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def findBottomLeftValue(self, root: Optional[TreeNode]) -> int:
        que = collections.deque([root])
        ans = root.val
        while que:
            n = len(que)
            flag = True
            for i in range(n):
                node = que.popleft()
                if flag:
                    ans = node.val
                    flag = False
                if node.left:
                    que.append(node.left)
                if node.right:
                    que.append(node.right)

        return ans

# @lc code=end
