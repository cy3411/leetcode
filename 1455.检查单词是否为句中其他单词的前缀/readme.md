# 题目

给你一个字符串 `sentence` 作为句子并指定检索词为 `searchWord` ，其中句子由若干用 **单个空格** 分隔的单词组成。请你检查检索词 `searchWord` 是否为句子 `sentence` 中任意单词的前缀。

如果 `searchWord` 是某一个单词的前缀，则返回句子 `sentence` 中该单词所对应的下标（下标从 1 开始）。如果 `searchWord` 是多个单词的前缀，则返回匹配的第一个单词的下标（最小下标）。如果 `searchWord` 不是任何单词的前缀，则返回 `-1` 。

字符串 `s` 的 **前缀** 是 `s` 的任何前导连续子字符串。

提示：

- $1 \leq sentence.length \leq 100$
- $1 \leq searchWord.length \leq 10$
- sentence 由小写英文字母和空格组成。
- searchWord 由小写英文字母组成。

# 示例

```
输入：sentence = "i love eating burger", searchWord = "burg"
输出：4
解释："burg" 是 "burger" 的前缀，而 "burger" 是句子中第 4 个单词。
```

```
输入：sentence = "this problem is an easy problem", searchWord = "pro"
输出：2
解释："pro" 是 "problem" 的前缀，而 "problem" 是句子中第 2 个也是第 6 个单词，但是应该返回最小下标 2 。
```

# 题解

## 枚举

枚举每一个单词，检查是否为 `searchWord` 的前缀。是的话中断循环，返回下标。

```js
function isPrefixOfWord(sentence: string, searchWord: string): number {
  const words = sentence.split(' ');
  const n = words.length;
  for (let i = 0; i < n; i++) {
    if (words[i].startsWith(searchWord)) {
      return i + 1;
    }
  }
  return -1;
}
```

```cpp
class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {
        vector<string> words;
        stringstream ss(sentence);
        string word;
        while (ss >> word) {
            words.push_back(word);
        }
        int n = words.size();
        for (int i = 0; i < n; i++) {
            if (words[i].find(searchWord) == 0) {
                return i + 1;
            }
        }
        return -1;
    }
};
```

## 双指针

定义 start 和 end 两个指针，分别记录单词的起始和结束位置。

我们遍历 sentence 且同时不断移动 start 和 end 指针，然后判断 [start, end) 区间内的单词是否有前缀是 searchWord。

```py
class Solution:
    def isPrefixOfWord(self, sentence: str, searchWord: str) -> int:
        i, index, n = 0, 1, len(sentence)
        while i < n:
            start = i
            while i < n and sentence[i] != ' ':
                i += 1
            end = i
            if sentence[start:end].startswith(searchWord):
                return index
            index += 1
            i += 1
        return -1
```
