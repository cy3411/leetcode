/*
 * @lc app=leetcode.cn id=1455 lang=typescript
 *
 * [1455] 检查单词是否为句中其他单词的前缀
 */

// @lc code=start
function isPrefixOfWord(sentence: string, searchWord: string): number {
  const words = sentence.split(' ');
  const n = words.length;
  for (let i = 0; i < n; i++) {
    if (words[i].startsWith(searchWord)) {
      return i + 1;
    }
  }
  return -1;
}
// @lc code=end
