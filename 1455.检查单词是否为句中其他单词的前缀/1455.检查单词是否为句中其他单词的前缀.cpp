/*
 * @lc app=leetcode.cn id=1455 lang=cpp
 *
 * [1455] 检查单词是否为句中其他单词的前缀
 */

// @lc code=start
class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {
        vector<string> words;
        stringstream ss(sentence);
        string word;
        while (ss >> word) {
            words.push_back(word);
        }
        int n = words.size();
        for (int i = 0; i < n; i++) {
            if (words[i].find(searchWord) == 0) {
                return i + 1;
            }
        }
        return -1;
    }
};
// @lc code=end
