# 题目

给你一个由小写字母组成的字符串 `s` ，以及一个整数 `k` 。

首先，用字母在字母表中的位置替换该字母，将 `s` 转化 为一个整数（也就是，`'a'` 用 `1` 替换，`'b'` 用 `2` 替换，... `'z'` 用 `26` 替换）。接着，将整数 **转换** 为其 **各位数字之和** 。共重复 **转换** 操作 `k` 次 。

例如，如果 `s = "zbax"` 且 `k = 2` ，那么执行下述步骤后得到的结果是整数 `8` ：

- 转化：`"zbax" ➝ "(26)(2)(1)(24)" ➝ "262124" ➝ 262124`
- 转换 #1：`262124 ➝ 2 + 6 + 2 + 1 + 2 + 4 ➝ 17`
- 转换 #2：`17 ➝ 1 + 7 ➝ 8`

返回执行上述操作后得到的结果整数。

提示：

- $1 \leq s.length \leq 100$
- $1 \leq k \leq 10$
- `s` 由小写英文字母组成

# 示例

```
输入：s = "iiii", k = 1
输出：36
解释：操作如下：
- 转化："iiii" ➝ "(9)(9)(9)(9)" ➝ "9999" ➝ 9999
- 转换 #1：9999 ➝ 9 + 9 + 9 + 9 ➝ 36
因此，结果整数为 36 。
```

```
输入：s = "leetcode", k = 2
输出：6
解释：操作如下：
- 转化："leetcode" ➝ "(12)(5)(5)(20)(3)(15)(4)(5)" ➝ "12552031545" ➝ 12552031545
- 转换 #1：12552031545 ➝ 1 + 2 + 5 + 5 + 2 + 0 + 3 + 1 + 5 + 4 + 5 ➝ 33
- 转换 #2：33 ➝ 3 + 3 ➝ 6
因此，结果整数为 6 。
```

# 题解

## 模拟

按照题意将字符转换成对应的数字，然后执行 k 次位数累加即可。

```ts
function getNumber(s: string): string {
  const base = 'a'.charCodeAt(0);
  let res: string = '';
  for (const c of s) {
    const d = c.charCodeAt(0) - base + 1;
    res += String(d);
  }
  return res;
}

function getLucky(s: string, k: number): number {
  let numstr = getNumber(s);
  let ans: number = 0;
  while (k--) {
    ans = 0;
    for (const n of numstr) {
      ans += Number(n);
    }
    numstr = String(ans);
  }

  return ans!;
}
```
