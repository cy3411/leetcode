/*
 * @lc app=leetcode.cn id=1945 lang=typescript
 *
 * [1945] 字符串转化后的各位数字之和
 */

// @lc code=start

function getNumber(s: string): string {
  const base = 'a'.charCodeAt(0);
  let res: string = '';
  for (const c of s) {
    const d = c.charCodeAt(0) - base + 1;
    res += String(d);
  }
  return res;
}

function getLucky(s: string, k: number): number {
  let numstr = getNumber(s);
  let ans: number = 0;
  while (k--) {
    ans = 0;
    for (const n of numstr) {
      ans += Number(n);
    }
    numstr = String(ans);
  }

  return ans!;
}
// @lc code=end
