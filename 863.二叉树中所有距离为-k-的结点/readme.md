# 题目

给定一个二叉树（具有根结点 root）， 一个目标结点 target ，和一个整数值 K 。

返回到目标结点 target 距离为 K 的所有结点的值的列表。 答案可以以任何顺序返回。

提示：

- 给定的树是非空的。
- 树上的每个结点都具有唯一的值 0 <= node.val <= 500 。
- 目标结点 target 是树上的结点。
- 0 <= K <= 1000.

# 示例

```
输入：root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2
输出：[7,4,1]
解释：
所求结点为与目标结点（值为 5）距离为 2 的结点，
值分别为 7，4，以及 1
```

[![W7BIeA.md.png](https://z3.ax1x.com/2021/07/28/W7BIeA.md.png)](https://imgtu.com/i/W7BIeA)

注意，输入的 "root" 和 "target" 实际上是树上的结点。
上面的输入仅仅是对这些对象进行了序列化描述。

# 题解

## 深度优先搜索

使用哈希保存每个节点的父节点，然后 dfs 子节点和父节点，找到深度为 k 的节点。

题目给出每个节点的值都是唯一的，我们可以直接使用节点值来建立哈希映射。

dfs 的时候，我们还需要判断一下来源，是否跟将要递归的节点相同，不同的话才可以继续递归。

```ts
function distanceK(root: TreeNode | null, target: TreeNode | null, k: number): number[] {
  const parents: Map<number, TreeNode> = new Map();
  const findParent = (node: TreeNode) => {
    if (node === null) return;
    if (node.left !== null) {
      parents.set(node.left.val, node);
      findParent(node.left);
    }
    if (node.right !== null) {
      parents.set(node.right.val, node);
      findParent(node.right);
    }
  };
  findParent(root);

  const ans: number[] = [];
  const findAns = (node: TreeNode | null, from: TreeNode | null, depth: number) => {
    // 哈希中没有找到结果返回的是undefined，这里需要特判一下
    if (node === null || node === undefined) return;
    if (depth === k) {
      ans.push(node.val);
      return;
    }
    if (node.left !== from) {
      findAns(node.left, node, depth + 1);
    }
    if (node.right !== from) {
      findAns(node.right, node, depth + 1);
    }
    if (parents.get(node.val) !== from) {
      findAns(parents.get(node.val), node, depth + 1);
    }
  };
  findAns(target, null, 0);

  return ans;
}
```
