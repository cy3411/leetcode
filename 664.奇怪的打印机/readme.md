# 题目
有台奇怪的打印机有以下两个特殊要求：

打印机每次只能打印由 同一个字符 组成的序列。
每次可以在任意起始和结束位置打印新字符，并且会覆盖掉原来已有的字符。
给你一个字符串 `s` ，你的任务是计算这个打印机打印它需要的最少打印次数。

# 示例
```
输入：s = "aaabbb"
输出：2
解释：首先打印 "aaa" 然后打印 "bbb"。
```

# 题解
## 动态规划
**状态**

定义`dp[i][j]`为打印区间`[i][j]`的最少操作次数。

**选择**

+ `s[i]=s[j]`，区间两端字符相同。那么当打印左边字符`s[i]`，右边字符`s[j]`可以顺便打印。这样我们只需要考虑如何尽快打印`s[i][j-1]`区间即可。
+ `s[i]!=s[j]`,区间两端字符不相同。那么我们需要完成区间左右两部分的打印。我们记两个部分的区间为`[i][k]`和`[k+1,j]`,(`i<=k<j`)。

$$
dp[i][j]= \begin{cases} dp[i][j-1], & s[i] \equiv s[j] \\ min_{k=i}^{j-1} dp[i][k] + dp[k+1][j], & s[i] \neq s[j] \end{cases} 
$$

**Basecase**

边界条件为 `dp[i][i]=1`，对于长度为 1 的区间，需要打印 1 次。

最后的答案为 `dp[0][n-1]`

```ts
function strangePrinter(s: string): number {
  let size = s.length;
  const dp = new Array(size).fill(0).map(() => new Array(size).fill(0));

  for (let i = size - 1; i >= 0; i--) {
    // 边界，长度为1的字符最多打印1次
    dp[i][i] = 1;
    for (let j = i + 1; j < size; j++) {
      if (s[i] === s[j]) {
        dp[i][j] = dp[i][j - 1];
      } else {
        let min = Number.POSITIVE_INFINITY;
        for (let k = i; k < j; k++) {
          min = Math.min(min, dp[i][k] + dp[k + 1][j]);
        }
        dp[i][j] = min;
      }
    }
  }

  return dp[0][size - 1];
}
```