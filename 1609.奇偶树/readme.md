# 题目

如果一棵二叉树满足下述几个条件，则可以称为 **奇偶树** ：

- 二叉树根节点所在层下标为 `0` ，根的子节点所在层下标为 `1` ，根的孙节点所在层下标为 `2` ，依此类推。
- 偶数下标 层上的所有节点的值都是 **奇** 整数，从左到右按顺序 **严格递增**
- 奇数下标 层上的所有节点的值都是 **偶** 整数，从左到右按顺序 **严格递减**

给你二叉树的根节点，如果二叉树为 **奇偶树** ，则返回 **true** ，否则返回 **false** 。

提示：

- 树中节点数在范围 $\color{golenrod}[1, 10^5]$ 内
- $\color{golenrod}1 \leq Node.val \leq 10^6$

# 示例

[![TdmF0A.png](https://s4.ax1x.com/2021/12/25/TdmF0A.png)](https://imgtu.com/i/TdmF0A)

```
输入：root = [1,10,4,3,null,7,9,12,8,6,null,null,2]
输出：true
解释：每一层的节点值分别是：
0 层：[1]
1 层：[10,4]
2 层：[3,7,9]
3 层：[12,8,6,2]
由于 0 层和 2 层上的节点值都是奇数且严格递增，而 1 层和 3 层上的节点值都是偶数且严格递减，因此这是一棵奇偶树。
```

[![TdmEkt.png](https://s4.ax1x.com/2021/12/25/TdmEkt.png)](https://imgtu.com/i/TdmEkt)

```
输入：root = [5,4,2,3,3,7]
输出：false
解释：每一层的节点值分别是：
0 层：[5]
1 层：[4,2]
2 层：[3,3,7]
2 层上的节点值不满足严格递增的条件，所以这不是一棵奇偶树。
```

# 题解

## 广度优先搜索

广度优先搜索二叉树，每次从队列中取出一个节点，检查其值是否满足奇偶树的条件，如果满足，则将其子节点放入队列中，否则返回 false。

奇偶树的条件：

- 偶数层上的所有节点的值都是奇整数，从左到右按顺序严格递增
- 奇数层上的所有节点的值都是偶整数，从左到右按顺序严格递减

```ts
class Node1609 {
  constructor(public node: TreeNode, public level: number) {
    this.node = node;
    this.level = level;
  }
}

function isEvenOddTree(root: TreeNode | null): boolean {
  if (root === null) return true;
  const deque = new Array<Node1609 | null>();
  deque.push(new Node1609(root, 0));

  while (deque.length) {
    const top = deque.shift();
    // 节点值与层数奇偶性不符
    if (top.node.val % 2 === top.level % 2) return false;
    // 偶数层要严格递增
    if (top.level % 2 === 0 && deque.length) {
      if (top.node.val >= deque[0].node.val && top.level === deque[0].level) return false;
    }
    // 奇数层要严格递减
    if (top.level % 2 === 1 && deque.length) {
      if (top.node.val <= deque[0].node.val && top.level === deque[0].level) return false;
    }
    if (top.node.left) deque.push(new Node1609(top.node.left, top.level + 1));
    if (top.node.right) deque.push(new Node1609(top.node.right, top.level + 1));
  }

  return true;
}
```
