/*
 * @lc app=leetcode.cn id=7 lang=typescript
 *
 * [7] 整数反转
 */

// @lc code=start
function reverse(x: number): number {
  let result = 0;
  while (x !== 0) {
    let pop = x % 10;
    result = result * 10 + pop;
    if (result > 2 ** 31 - 1 || result < (-2) ** 31) {
      return 0;
    }
    x = (x / 10) >> 0;
  }

  return result;
}
// @lc code=end
