# 题目
给你一个 32 位的有符号整数 `x` ，返回将 `x` 中的数字部分反转后的结果。

如果反转后整数超过 32 位的有符号整数的范围 `[−231,  231 − 1]` ，就返回 0。

**假设环境不允许存储 64 位整数（有符号或无符号）。**

提示：

+ -231 <= x <= 231 - 1

# 示例
```
输入：x = 123
输出：321
```

# 题解
## 数学计算
每次取出尾数，保存到结果中即可。

注意题目给出的限制，超过32位有符号整数返回0.

```js
function reverse(x: number): number {
  let result = 0;
  while (x !== 0) {
    let pop = x % 10;
    result = result * 10 + pop;
    if (result > 2 ** 31 - 1 || result < (-2) ** 31) {
      return 0;
    }
    x = (x / 10) >> 0;
  }

  return result;
}
```