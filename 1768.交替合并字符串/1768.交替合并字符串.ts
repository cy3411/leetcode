/*
 * @lc app=leetcode.cn id=1768 lang=typescript
 *
 * [1768] 交替合并字符串
 */

// @lc code=start
function mergeAlternately(word1: string, word2: string): string {
  const m = word1.length;
  const n = word2.length;
  let i = 0;
  let j = 0;
  let ans = '';
  while (i < m && j < n) {
    ans += word1[i++];
    ans += word2[j++];
  }

  while (i < m) ans += word1[i++];
  while (j < n) ans += word2[j++];

  return ans;
}
// @lc code=end
