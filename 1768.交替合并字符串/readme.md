# 题目

给你两个字符串 `word1` 和 `word2` 。请你从 `word1` 开始，通过交替添加字母来合并字符串。如果一个字符串比另一个字符串长，就将多出来的字母追加到合并后字符串的末尾。

返回合并后的字符串 。

提示：

- $1 \leq word1.length, word2.length \leq 100$
- `word1` 和 `word2` 由小写英文字母组成

# 示例

```
输入：word1 = "abc", word2 = "pqr"
输出："apbqcr"
解释：字符串合并情况如下所示：
word1：  a   b   c
word2：    p   q   r
合并后：  a p b q c r
```

```
输入：word1 = "ab", word2 = "pqrs"
输出："apbqrs"
解释：注意，word2 比 word1 长，"rs" 需要追加到合并后字符串的末尾。
word1：  a   b
word2：    p   q   r   s
合并后：  a p b q   r   s
```

# 题解

## 双指针

定义 i,j 分别指向两个字符串的开始位置，在遍历过程中，分别将答案加入结果中，同时将指针后移。

```js
function mergeAlternately(word1: string, word2: string): string {
  const m = word1.length;
  const n = word2.length;
  let i = 0;
  let j = 0;
  let ans = '';
  while (i < m && j < n) {
    ans += word1[i++];
    ans += word2[j++];
  }

  while (i < m) ans += word1[i++];
  while (j < n) ans += word2[j++];

  return ans;
}
```

```cpp
class Solution {
public:
    string mergeAlternately(string word1, string word2) {
        int m = word1.size(), n = word2.size();
        int i = 0, j = 0;
        string ans;
        while (i < m || j < n) {
            if (i < m) {
                ans += word1[i++];
            }
            if (j < n) {
                ans += word2[j++];
            }
        }
        return ans;
    }
};
```

```py
class Solution:
    def mergeAlternately(self, word1: str, word2: str) -> str:
        m, n = len(word1), len(word2)
        i = j = 0
        ans = list()
        while i < m or j < n:
            if i < m:
                ans.append(word1[i])
                i += 1
            if j < n:
                ans.append(word2[j])
                j += 1
        return "".join(ans)
```
