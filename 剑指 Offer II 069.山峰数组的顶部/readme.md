# 题目

符合下列属性的数组 `arr` 称为 **山峰数组（山脉数组）** ：

- `arr.length >= 3`
- 存在 `i（0 < i < arr.length - 1）`使得：
  - `arr[0] < arr[1] < ... arr[i-1] < arr[i]`
  - `arr[i] > arr[i+1] > ... > arr[arr.length - 1]`

给定由整数组成的山峰数组 `arr` ，返回任何满足 `arr[0] < arr[1] < ... arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1]` 的下标 `i` ，即山峰顶部。

提示：

- $\color{#CDA869} 3 \leq arr.length \leq 10^4$
- $\color{#CDA869} 0 \leq arr[i] \leq 10^6$
- 题目数据保证 `arr` 是一个山脉数组

进阶：很容易想到时间复杂度 `O(n)` 的解决方案，你可以设计一个 `O(log(n))` 的解决方案吗？

# 示例

```
输入：arr = [0,1,0]
输出：1
```

```
输入：arr = [24,69,100,99,79,78,67,36,26,19]
输出：2
```

# 题解

## 二分搜索

峰顶的特点就是比前一个数大，比后一个数小。

二分搜索

- 如果中间数符合峰顶特点，证明我们找到了答案，直接返回。
- 如果中间数比前一个数大，那么表示峰顶在后面，`left=middle`
- 如果中间数比后一个大，那么表示峰顶在前面，`right=middle`

```ts
function peakIndexInMountainArray(arr: number[]): number {
  let left = 0;
  let right = arr.length;
  let middle: number;

  // 二分查找比前后都大的元素位置
  while (left <= right) {
    middle = left + (((right - left) / 2) >> 0);
    if (arr[middle] > arr[middle - 1] && arr[middle] > arr[middle + 1]) {
      return middle;
    } else if (arr[middle] > arr[middle - 1]) {
      left = middle;
    } else if (arr[middle] > arr[middle + 1]) {
      right = middle;
    }
  }
}
```
