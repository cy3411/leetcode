// @algorithm @lc id=1000333 lang=typescript
// @title B1IidL
function peakIndexInMountainArray(arr: number[]): number {
  let left = 0;
  let right = arr.length;
  let middle: number;

  // 二分查找比前后都大的元素位置
  while (left <= right) {
    middle = left + (((right - left) / 2) >> 0);
    if (arr[middle] > arr[middle - 1] && arr[middle] > arr[middle + 1]) {
      return middle;
    } else if (arr[middle] > arr[middle - 1]) {
      left = middle;
    } else if (arr[middle] > arr[middle + 1]) {
      right = middle;
    }
  }
}
