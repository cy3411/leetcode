# 题目

给定一个整数数组 `arr` ，以及一个整数 `target` 作为目标值，返回满足 `i < j < k` 且 `arr[i] + arr[j] + arr[k] == target` 的元组 `i, j, k` 的数量。

由于结果会非常大，请返回 $10^9 + 7$ 的模。

提示：

- $3 \leq arr.length \leq 3000$
- $0 \leq arr[i] \leq 100$
- $0 \leq target \leq 300$

# 示例

```
输入：arr = [1,1,2,2,3,3,4,4,5,5], target = 8
输出：20
解释：
按值枚举(arr[i], arr[j], arr[k])：
(1, 2, 5) 出现 8 次；
(1, 3, 4) 出现 8 次；
(2, 2, 4) 出现 2 次；
(2, 3, 3) 出现 2 次。
```

```
输入：arr = [1,1,2,2,2,2], target = 5
输出：12
解释：
arr[i] = 1, arr[j] = arr[k] = 2 出现 12 次：
我们从 [1,1] 中选择一个 1，有 2 种情况，
从 [2,2,2,2] 中选出两个 2，有 6 种情况。
```

# 题解

## 枚举+双指针

枚举第一个数字 `arr[i]`，然后求取两数之和为 `target - arr[i]`的组合数量。

```ts
function threeSumMulti(arr: number[], target: number): number {
  const twoSumMulti = (arr: number[], l: number, r: number, target: number): number => {
    let res = 0;
    while (l < r) {
      const sum = arr[l] + arr[r];
      if (sum < target) {
        l++;
      } else if (sum > target) {
        r--;
      } else {
        // 前后相等，就是从n个数字中取2个数字的组合数量
        // 表示这是最后一个区间，所以直接返回
        if (arr[l] === arr[r]) {
          const n = r - l + 1;
          res += (n * (n - 1)) / 2;
          res %= modNum;
          break;
        }
        let lcnt = 1;
        let rcnt = 1;
        // arr[l] 相同的数字数量
        while (l < r && arr[l] === arr[l + 1]) {
          l++;
          lcnt++;
        }
        // arr[r] 相同的数字数量
        while (l < r && arr[r] === arr[r - 1]) {
          r--;
          rcnt++;
        }
        // 组合数量
        res += lcnt * rcnt;
        res %= modNum;
        l++, r--;
      }
    }
    return res;
  };
  arr.sort((a, b) => a - b);
  const n = arr.length;
  const modNum = 1e9 + 7;
  let ans = 0;
  // 枚举第一个数，因为后面最少要留两个数字，所以结束条件是n-2
  for (let i = 0; i < n - 2; i++) {
    ans += twoSumMulti(arr, i + 1, n - 1, target - arr[i]);
    ans %= modNum;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    const int modNum = 1e9 + 7;

    int twoSumMulti(vector<int> &arr, int target, int l, int r)
    {
        int res = 0;
        while (l < r)
        {
            int sum = arr[l] + arr[r];
            if (sum < target)
            {
                l++;
            }
            else if (sum > target)
            {
                r--;
            }
            else
            {
                if (arr[l] == arr[r])
                {
                    int count = r - l + 1;
                    res += count * (count - 1) / 2;
                    res %= modNum;
                    break;
                }
                int lcnt = 1, rcnt = 1;
                while (l < r && arr[l] == arr[l + 1])
                {
                    lcnt++;
                    l++;
                }
                while (l < r && arr[r] == arr[r - 1])
                {
                    rcnt++;
                    r--;
                }
                res += lcnt * rcnt;
                res %= modNum;
                l++, r--;
            }
        }
        return res;
    }

    int threeSumMulti(vector<int> &arr, int target)
    {
        sort(arr.begin(), arr.end());
        int n = arr.size(), ans = 0;

        for (int i = 0; i < n - 1; i++)
        {
            ans += twoSumMulti(arr, target - arr[i], i + 1, n - 1);
            ans %= modNum;
        }

        return ans;
    }
};
```
