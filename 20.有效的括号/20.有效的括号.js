/*
 * @lc app=leetcode.cn id=20 lang=javascript
 *
 * [20] 有效的括号
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
  const stack = [];
  const hash = new Map([
    [')', '('],
    ['}', '{'],
    [']', '['],
  ]);

  for (const char of s) {
    // 判断是否是右括号
    if (hash.has(char)) {
      // 当找到右括号时，栈为空或者当前由括号和栈顶左括号不匹配
      if (!stack.length || hash.get(char) !== stack[stack.length - 1]) {
        return false;
      }
      stack.pop();
    } else {
      stack.push(char);
    }
  }

  return !stack.length;
};
// @lc code=end
