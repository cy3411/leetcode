# 题目
给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。

有效字符串需满足：

+ 左括号必须用相同类型的右括号闭合。
+ 左括号必须以正确的顺序闭合。

提示：

+ 1 <= s.length <= 104
+ s 仅由括号 '()[]{}' 组成

# 示例
```
输入：s = "()"
输出：true
```

```
输入：s = "()[]{}"
输出：true
```
# 方法
匹配有包含关系，所以我们使用栈来操作数据。
```js
/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
  const stack = [];
  const hash = new Map([
    [')', '('],
    ['}', '{'],
    [']', '['],
  ]);

  for (const char of s) {
    // 判断是否是右括号
    if (hash.has(char)) {
      // 当找到右括号时，栈为空或者当前由括号和栈顶左括号不匹配
      if (!stack.length || hash.get(char) !== stack[stack.length - 1]) {
        return false;
      }
      stack.pop();
    } else {
      stack.push(char);
    }
  }

  return !stack.length;
};
```