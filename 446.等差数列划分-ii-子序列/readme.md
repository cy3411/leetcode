# 题目

给你一个整数数组 nums ，返回 nums 中所有 等差子序列 的数目。

如果一个序列中 至少有三个元素 ，并且任意两个相邻元素之差相同，则称该序列为等差序列。

- 例如，[1, 3, 5, 7, 9]、[7, 7, 7, 7] 和 [3, -1, -5, -9] 都是等差序列。
- 再例如，[1, 1, 2, 5, 7] 不是等差序列。

数组中的子序列是从数组中删除一些元素（也可能不删除）得到的一个序列。

- 例如，[2,5,10] 是 [1,2,1,2,4,1,5,10] 的一个子序列。

题目数据保证答案是一个 32-bit 整数。

提示：

- `1 <= nums.length <= 1000`
- `-231 <= nums[i] <= 231 - 1`

# 示例

```
输入：nums = [2,4,6,8,10]
输出：7
解释：所有的等差子序列为：
[2,4,6]
[4,6,8]
[6,8,10]
[2,4,6,8]
[4,6,8,10]
[2,4,6,8,10]
[2,6,10]
```

# 题解

## 动态规划

**状态**

定义`dp[i][d]`,表示`[0,i]`的子串中，有多少个公差为`d`的序列数量。由于公差的数量可能有很多，这里的使用哈希来保存多个公差的数量。

**转换**

$dp[i][d]+=dp[j][d]+1$

**Base Case**
初始化都为 0

```ts
function numberOfArithmeticSlices(nums: number[]): number {
  const m = nums.length;
  // dp[i][d]，[0,i]字串中，差值为d的序列的数量
  const dp: Map<number, number>[] = new Array(m).fill(0).map((_) => new Map());

  let ans = 0;
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < i; j++) {
      const diff = nums[i] - nums[j];
      const count = dp[j].get(diff) ?? 0;
      dp[i].set(diff, (dp[i].get(diff) ?? 0) + count + 1);
      ans += count;
    }
  }
  return ans;
}
```
