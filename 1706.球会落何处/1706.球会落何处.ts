/*
 * @lc app=leetcode.cn id=1706 lang=typescript
 *
 * [1706] 球会落何处
 */

// @lc code=start
function findBall(grid: number[][]): number[] {
  const n = grid[0].length;
  const ans = new Array(n);
  // 枚举每个小球的位置，当前行是否可以左右移动
  for (let j = 0; j < n; j++) {
    let col = j;
    for (const row of grid) {
      // 当前列需要移动的的左右方向
      const dir = row[col];
      // 移动到哪一列
      col += dir;
      // 在侧边上或者和下一个位置形成了V字形
      if (col < 0 || col >= n || row[col] !== dir) {
        col = -1;
        break;
      }
    }
    ans[j] = col;
  }

  return ans;
}
// @lc code=end
