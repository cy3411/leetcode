/*
 * @lc app=leetcode.cn id=1706 lang=c
 *
 * [1706] 球会落何处
 */

// @lc code=start

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *findBall(int **grid, int gridSize, int *gridColSize, int *returnSize)
{
    int n = *gridColSize;
    int *ans = (int *)malloc(sizeof(int) * n);

    for (int j = 0; j < n; j++)
    {
        int col = j;
        for (int i = 0; i < gridSize; i++)
        {
            int dir = grid[i][col];
            col += dir;
            if (col < 0 || col >= n || grid[i][col] != dir)
            {
                col = -1;
                break;
            }
        }
        ans[j] = col;
    }
    *returnSize = n;
    return ans;
}
// @lc code=end
