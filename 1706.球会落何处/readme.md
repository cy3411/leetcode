# 题目

用一个大小为 `m x n` 的二维网格 `grid` 表示一个箱子。你有 `n` 颗球。箱子的顶部和底部都是开着的。

箱子中的每个单元格都有一个对角线挡板，跨过单元格的两个角，可以将球导向左侧或者右侧。

- 将球导向右侧的挡板跨过左上角和右下角，在网格中用 `1` 表示。
- 将球导向左侧的挡板跨过右上角和左下角，在网格中用 `-1` 表示。

在箱子每一列的顶端各放一颗球。每颗球都可能卡在箱子里或从底部掉出来。如果球恰好卡在两块挡板之间的 `"V"` 形图案，或者被一块挡导向到箱子的任意一侧边上，就会卡住。

返回一个大小为 `n` 的数组 `answer` ，其中 `answer[i]` 是球放在顶部的第 `i` 列后从底部掉出来的那一列对应的下标，如果球卡在盒子里，则返回 `-1` 。

提示：

- $m \equiv grid.length$
- $n \equiv grid[i].length$
- $1 \leq m, n \leq 100$
- `grid[i][j]` 为 `1` 或 `-1`

# 示例

[![bFN4C8.png](https://s4.ax1x.com/2022/02/24/bFN4C8.png)](https://imgtu.com/i/bFN4C8)

```
输入：grid = [[1,1,1,-1,-1],[1,1,1,-1,-1],[-1,-1,-1,1,1],[1,1,1,1,-1],[-1,-1,-1,-1,-1]]
输出：[1,-1,-1,-1,-1]
解释：示例如图：
b0 球开始放在第 0 列上，最终从箱子底部第 1 列掉出。
b1 球开始放在第 1 列上，会卡在第 2、3 列和第 1 行之间的 "V" 形里。
b2 球开始放在第 2 列上，会卡在第 2、3 列和第 0 行之间的 "V" 形里。
b3 球开始放在第 3 列上，会卡在第 2、3 列和第 0 行之间的 "V" 形里。
b4 球开始放在第 4 列上，会卡在第 2、3 列和第 1 行之间的 "V" 形里。
```

# 题解

## 模拟

遍历每个小球，然后枚举每行小球是否可以左右移动，如果可以，就把移动后的位置记录下来。

```ts
function findBall(grid: number[][]): number[] {
  const n = grid[0].length;
  const ans = new Array(n);
  // 枚举每个小球的位置，当前行是否可以左右移动
  for (let j = 0; j < n; j++) {
    let col = j;
    for (const row of grid) {
      // 当前列需要移动的的左右方向
      const dir = row[col];
      // 下一列
      col += dir;
      // 在侧边上或者和下一个位置形成了V字形
      if (col < 0 || col >= n || row[col] !== dir) {
        col = -1;
        break;
      }
    }
    ans[j] = col;
  }

  return ans;
}
```

```cpp
int *findBall(int **grid, int gridSize, int *gridColSize, int *returnSize)
{
    int n = *gridColSize;
    int *ans = (int *)malloc(sizeof(int) * n);

    for (int j = 0; j < n; j++)
    {
        int col = j;
        for (int i = 0; i < gridSize; i++)
        {
            int dir = grid[i][col];
            col += dir;
            if (col < 0 || col >= n || grid[i][col] != dir)
            {
                col = -1;
                break;
            }
        }
        ans[j] = col;
    }
    *returnSize = n;
    return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> findBall(vector<vector<int>> &grid)
    {
        int n = grid[0].size();
        vector<int> ans(n);
        for (int j = 0; j < n; j++)
        {
            int col = j;
            for (auto &row : grid)
            {
                int dir = row[col];
                col += dir;
                if (col < 0 || col >= n || row[col] != dir)
                {
                    col = -1;
                    break;
                }
            }
            ans[j] = col;
        }

        return ans;
    }
};

```
