# 题目

给你一个 **正** 整数 `num` ，输出它的补数。补数是对该数的二进制表示取反。

提示：

- 给定的整数 `num` 保证在 `32` 位带符号整数的范围内。
- `num >= 1`
- 你可以假定二进制数不包含前导零位。

# 示例

```
输入：num = 5
输出：2
解释：5 的二进制表示为 101（没有前导零位），其补数为 010。所以你需要输出 2 。
```

```
输入：num = 1
输出：0
解释：1 的二进制表示为 1（没有前导零位），其补数为 0。所以你需要输出 0 。
```

# 题解

## 位运算

首先我们要找到 `num` 二进制最高位的 `1` 的位置，然后对这个 `1` 到最低位进行取反。

```ts
function findComplement(num: number): number {
  let highestBit = 0;
  for (let i = 0; i <= 30; i++) {
    if (num >= 2 ** i) {
      highestBit = i;
    } else {
      break;
    }
  }
  // 构建不包括前导0的，所有位值为1的掩码
  const mask = highestBit === 30 ? 0x7fffffff : (1 << (highestBit + 1)) - 1;

  // 异或
  return mask ^ num;
}
```
