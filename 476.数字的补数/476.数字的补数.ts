/*
 * @lc app=leetcode.cn id=476 lang=typescript
 *
 * [476] 数字的补数
 */

// @lc code=start
function findComplement(num: number): number {
  let highestBit = 0;
  for (let i = 0; i <= 30; i++) {
    if (num >= 2 ** i) {
      highestBit = i;
    } else {
      break;
    }
  }
  // 构建不包括前导0的，所有位值为1的掩码
  const mask = highestBit === 30 ? 0x7fffffff : (1 << (highestBit + 1)) - 1;

  // 异或
  return mask ^ num;
}
// @lc code=end
