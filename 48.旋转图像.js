/*
 * @lc app=leetcode.cn id=48 lang=javascript
 *
 * [48] 旋转图像
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var rotate = function (matrix) {
  const size = matrix.length;
  // 水平翻转
  for (let i = 0; i < Math.floor(size / 2); i++) {
    for (let j = 0; j < size; j++) {
      [matrix[i][j], matrix[size - i - 1][j]] = [
        matrix[size - i - 1][j],
        matrix[i][j],
      ];
    }
  }
  // 对角线翻转
  for (let i = 0; i < size; i++) {
    for (let j = 0; j < i; j++) {
      [matrix[i][j], matrix[j][i]] = [matrix[j][i], matrix[i][j]];
    }
  }
};
// @lc code=end\
