/*
 * @lc app=leetcode.cn id=458 lang=typescript
 *
 * [458] 可怜的小猪
 */

// @lc code=start
function poorPigs(buckets: number, minutesToDie: number, minutesToTest: number): number {
  let n = minutesToTest / minutesToDie + 1;
  return Math.ceil(Math.log(buckets) / Math.log(n));
}
// @lc code=end
