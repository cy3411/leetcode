# 题目

有 `buckets` 桶液体，其中 正好 有一桶含有毒药，其余装的都是水。它们从外观看起来都一样。为了弄清楚哪只水桶含有毒药，你可以喂一些猪喝，通过观察猪是否会死进行判断。不幸的是，你只有 `minutesToTest` 分钟时间来确定哪桶液体是有毒的。

喂猪的规则如下：

- 选择若干活猪进行喂养
- 可以允许小猪同时饮用任意数量的桶中的水，并且该过程不需要时间。
- 小猪喝完水后，必须有 `minutesToDie` 分钟的冷却时间。在这段时间里，你只能观察，而不允许继续喂猪。
- 过了 `minutesToDie` 分钟后，所有喝到毒药的猪都会死去，其他所有猪都会活下来。
- 重复这一过程，直到时间用完。

给你桶的数目 `buckets` `，minutesToDie` 和 `minutesToTest` ，返回在规定时间内判断哪个桶有毒所需的 **最小** 猪数。

提示：

- `1 <= buckets <= 1000`
- `1 <= minutesToDie <= minutesToTest <= 100`

# 示例

```
输入：buckets = 1000, minutesToDie = 15, minutesToTest = 60
输出：5
```

```
输入：buckets = 4, minutesToDie = 15, minutesToTest = 15
输出：2
```

# 题解

## 数学

题目可以转换为给 x 个小猪，最多可以尝试 y 次，最多可以检测多少桶水？

假设 x 个小猪同时检测，检测 y 次，也就是 y+1 桶水，因为 y 检测完后，最后一桶水就不需要检测也可以知道结果。

因此可以最多检测$(y+1)^x$桶水。

题目给出了桶的数量和最多检测次数 y，求最少的 pig 数量 x，可知$(y+1)^x \geq buckets$.

由公式可得：$x = \lceil \log_{y+1}buckets \rceil$

```ts
function poorPigs(buckets: number, minutesToDie: number, minutesToTest: number): number {
  let n = minutesToTest / minutesToDie + 1;
  return Math.ceil(Math.log(buckets) / Math.log(n));
}
```
