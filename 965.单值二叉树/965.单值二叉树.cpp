/*
 * @lc app=leetcode.cn id=965 lang=cpp
 *
 * [965] 单值二叉树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left),
 * right(right) {}
 * };
 */
class Solution {
public:
    bool preorder(TreeNode *root, TreeNode *pre) {
        if (root == nullptr) {
            return true;
        }
        if (pre != nullptr && root->val != pre->val) {
            return false;
        }
        pre = root;

        return preorder(root->left, pre) && preorder(root->right, pre);
    }
    bool isUnivalTree(TreeNode *root) {
        TreeNode *pre = nullptr;

        return preorder(root, pre);
    }
};
// @lc code=end
