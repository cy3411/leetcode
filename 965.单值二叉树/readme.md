# 题目

如果二叉树每个节点都具有相同的值，那么该二叉树就是单值二叉树。

只有给定的树是单值二叉树时，才返回 true；否则返回 false。

提示：

- 给定树的节点数范围是 `[1, 100]`。
- 每个节点的值都是整数，范围为 `[0, 99]` 。

# 示例

```
输入：[1,1,1,1,1,null,1]
输出：true
```

```
输入：[2,2,2,5,2]
输出：false
```

# 题解

## 递归

递归访问每个节点，同时记录上一个节点值，如果当前节点值与上一个节点值不同，则返回 false；否则返回 true

```ts
function isUnivalTree(root: TreeNode | null): boolean {
  let pre: TreeNode | null = null;

  const preorder = (root: TreeNode | null) => {
    if (root === null) return true;
    if (pre !== null && pre.val !== root.val) return false;
    pre = root;

    return preorder(root.left) && preorder(root.right);
  };

  return preorder(root);
}
```

```cpp
class Solution {
public:
    bool preorder(TreeNode *root, TreeNode *pre) {
        if (root == nullptr) {
            return true;
        }
        if (pre != nullptr && root->val != pre->val) {
            return false;
        }
        pre = root;

        return preorder(root->left, pre) && preorder(root->right, pre);
    }
    bool isUnivalTree(TreeNode *root) {
        TreeNode *pre = nullptr;

        return preorder(root, pre);
    }
};
```

```py
class Solution:
    def isUnivalTree(self, root: TreeNode) -> bool:
        if not root:
            return True
        if root.left:
            if root.val != root.left.val or not self.isUnivalTree(root.left):
                return False
        if root.right:
            if root.val != root.right.val or not self.isUnivalTree(root.right):
                return False
        return True
```
