/*
 * @lc app=leetcode.cn id=965 lang=typescript
 *
 * [965] 单值二叉树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function isUnivalTree(root: TreeNode | null): boolean {
  let pre: TreeNode | null = null;

  const preorder = (root: TreeNode | null) => {
    if (root === null) return true;
    if (pre !== null && pre.val !== root.val) return false;
    pre = root;

    return preorder(root.left) && preorder(root.right);
  };

  return preorder(root);
}
// @lc code=end
