# 题目

给定一个 `24` 小时制（小时:分钟 "HH:MM"）的时间列表，找出列表中任意两个时间的最小时间差并以分钟数表示。

提示：

- $2 \leq timePoints \leq 2 * 10^4$
- `timePoints[i]` 格式为 "HH:MM"

注意：(本题与主站 539 题相同)[https://leetcode-cn.com/problems/minimum-time-difference/]

# 示例

```
输入：timePoints = ["23:59","00:00"]
输出：1
```

```
输入：timePoints = ["00:00","23:59","00:00"]
输出：0
```

# 题解

## 排序

最小的时间差，肯定是相邻的两个时间点的差值。

将时间点按照时间升序排序，然后遍历每两个时间点的差值，取最小值即可。

这里还有一种是跨天的情况，初始化的时候可以先计算跨天的时间差，然后再遍历每两个时间点的差值，取最小值即可。

有一个优化点，时间点最多有 1440 种，代码开始可以特判一下，超过 1440 种的时间点肯定会有重复的，直接返回 0。

```ts
function findMinDifference(timePoints: string[]): number {
  const getTime = (time: string): number => {
    const [h, m] = time.split(':').map(Number);
    return h * 60 + m;
  };

  // 顺序排序
  const n = timePoints.length;
  if (n > 1440) {
    return 0;
  }
  timePoints.sort((a, b) => getTime(a) - getTime(b));
  // 初始化为首尾相接的时间差
  let ans = getTime(timePoints[0]) + 24 * 60 - getTime(timePoints[n - 1]);

  for (let i = 1; i < n; i++) {
    ans = Math.min(ans, getTime(timePoints[i]) - getTime(timePoints[i - 1]));
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int getTime(string &time)
    {
        return (time[0] - 0) * 10 * 60 + (time[1] - 0) * 60 + (time[3] - 0) * 10 + (time[4] - 0);
    }
    int findMinDifference(vector<string> &timePoints)
    {
        int n = timePoints.size();
        if (n > 1440)
        {
            return 0;
        }
        sort(timePoints.begin(), timePoints.end());
        int ans = getTime(timePoints[0]) + 24 * 60 - getTime(timePoints[n - 1]);
        for (int i = 1; i < n; i++)
        {
            ans = min(ans, getTime(timePoints[i]) - getTime(timePoints[i - 1]));
        }
        return ans;
    }
};
```
