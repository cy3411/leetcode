// @algorithm @lc id=1000278 lang=typescript
// @title 569nqc
// @test(["23:59","00:00"])=1
function findMinDifference(timePoints: string[]): number {
  const getTime = (time: string): number => {
    const [h, m] = time.split(':').map(Number);
    return h * 60 + m;
  };

  // 顺序排序
  timePoints.sort((a, b) => getTime(a) - getTime(b));
  const n = timePoints.length;
  if (n > 1440) {
    return 0;
  }
  // 初始化为首尾相接的时间差
  let ans = getTime(timePoints[0]) + 24 * 60 - getTime(timePoints[n - 1]);

  for (let i = 1; i < n; i++) {
    ans = Math.min(ans, getTime(timePoints[i]) - getTime(timePoints[i - 1]));
  }

  return ans;
}
