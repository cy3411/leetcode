class Solution
{
public:
    int getTime(string &time)
    {
        return (time[0] - 0) * 10 * 60 + (time[1] - 0) * 60 + (time[3] - 0) * 10 + (time[4] - 0);
    }
    int findMinDifference(vector<string> &timePoints)
    {
        int n = timePoints.size();
        if (n > 1440)
        {
            return 0;
        }
        sort(timePoints.begin(), timePoints.end());
        int ans = getTime(timePoints[0]) + 24 * 60 - getTime(timePoints[n - 1]);
        for (int i = 1; i < n; i++)
        {
            ans = min(ans, getTime(timePoints[i]) - getTime(timePoints[i - 1]));
        }
        return ans;
    }
};