# 题目
给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 最多出现两次 ，返回删除后数组的新长度。

不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。

提示：

+ 1 <= nums.length <= 3 * 104
+ -104 <= nums[i] <= 104
+ nums 已按升序排列

# 示例
```
输入：nums = [1,1,1,2,2,3]
输出：5, nums = [1,1,2,2,3]
解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3 。 不需要考虑数组中超出新长度后面的元素。
```

```
输入：nums = [0,0,1,1,1,1,2,3,3]
输出：7, nums = [0,0,1,1,2,3,3]
解释：函数应返回新长度 length = 7, 并且原数组的前五个元素被修改为 0, 0, 1, 1, 2, 3, 3 。 不需要考虑数组中超出新长度后面的元素。
```

# 题解
## 双指针
题目要求在原地修改，那么肯定需要一个指针指向修改的位置，另一个指针遍历所有的元素，所以需要使用双指针来解决这个问题。

数据已经排序，而且需要最多2个重复元素，我们可以从第3个位置开始遍历。

定义slow和fast两个指针，每次对比slow-2位置和fast位置。
+ 相同的话，fast后移
+ 不同的话，slow位置数据重置成fast位置的值，同时后移

```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function (nums) {
  const size = nums.length;
  if (size <= 2) return size;

  let slow = 2;
  for (let fast = 2; fast < size; fast++) {
    if (nums[slow - 2] !== nums[fast]) {
      nums[slow] = nums[fast];
      slow++;
    }
  }

  return slow;
};
```