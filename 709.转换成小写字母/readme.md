# 题目

给你一个字符串 `s` ，将该字符串中的大写字母转换成相同的小写字母，返回新的字符串。

提示：

- `1 <= s.length <= 100`
- `s 由 ASCII 字符集中的可打印字符组成

# 示例

```
输入：s = "Hello"
输出："hello"
```

```
输入：s = "here"
输出："here"
```

# 题解

## 遍历

遍历字符串，如果字符是大写字母，则转换成小写字母。

```ts
function toLowerCase(s: string): string {
  let ans = '';
  for (let char of s) {
    // 如果是大写字符，则转换为小写字符
    if (char >= 'A' && char <= 'Z') {
      ans += String.fromCharCode(char.charCodeAt(0) + 32);
    } else {
      ans += char;
    }
  }

  return ans;
}
```
