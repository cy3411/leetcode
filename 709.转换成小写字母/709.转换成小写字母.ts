/*
 * @lc app=leetcode.cn id=709 lang=typescript
 *
 * [709] 转换成小写字母
 */

// @lc code=start
function toLowerCase(s: string): string {
  let ans = '';
  for (let char of s) {
    // 如果是大写字符，则转换为小写字符
    if (char >= 'A' && char <= 'Z') {
      ans += String.fromCharCode(char.charCodeAt(0) + 32);
    } else {
      ans += char;
    }
  }

  return ans;
}
// @lc code=end
