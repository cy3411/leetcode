# 题目

给定一个非负整数 ·，反复将各个位上的数字相加，直到结果为一位数。返回这个结果。

提示：

- $0 \leq num \leq 2^{31} - 1$

进阶：你可以不使用循环或者递归，在 `O(1)` 时间复杂度内解决这个问题吗？

# 示例

```
输入: num = 38
输出: 2
解释: 各位相加的过程为：
38 --> 3 + 8 --> 11
11 --> 1 + 1 --> 2
由于 2 是一位数，所以返回 2。
```

```
输入: num = 0
输出: 0
```

# 题解

## 模拟

模拟各位相加的过程，直到结果为一位数

每次计算当前整数除以 10 的余数得到个位，将最低位加到总和中，然后将当前整数除以 10 的结果赋值给当前整数。重复上面的操作，直到当前整数为 0。

重复上面的操作，直到 num 小于 10

```cpp
class Solution
{
public:
    int addDigits(int num)
    {
        while (num >= 10)
        {
            int sum = 0;
            while (num > 0)
            {
                sum += num % 10;
                num /= 10;
            }
            num = sum;
        }
        return num;
    }
};
```

```cpp
int addDigits(int num)
{
    while (num > 9)
    {
        int sum = 0;
        while (num)
        {
            sum += num % 10;
            num /= 10;
        }
        num = sum;
    }
    return num;
}
```

## 数学

假设 num 的十进制位有 n 位，从低位到高位依次是 $a_0$ 到 $a_{n-1}$，则 num 有以下公式：

$$
     \begin{aligned}
        num
        & = \sum_{i=0}^{n-1}{a_i * 10^i} \\
        & = \sum_{i=0}^{n-1}{a_i * (10^i - 1 + 1)} \\
        & = \sum_{i=0}^{n-1}{a_i * (10^i - 1) + (\sum_{i=0}^{n-1}{a_i} \quad \text{各位相加的结果})} \\
    \end{aligned}
$$

公式可以看出，$10^i -1$ 都是 9 的倍数，由此可以得知 num 和 各位相加的和与 9 同余。可得：

- num = 0 的时候，根数为 0
- num 是 9 的倍数， 根数为 9
- num 不是 9 的倍数，根数为 num % 9

```ts
function addDigits(num: number): number {
  if (num === 0) return 0;
  if (num % 9 === 0) return 9;
  return num % 9;
}
```
