/*
 * @lc app=leetcode.cn id=258 lang=c
 *
 * [258] 各位相加
 */

// @lc code=start

int addDigits(int num)
{
    while (num > 9)
    {
        int sum = 0;
        while (num)
        {
            sum += num % 10;
            num /= 10;
        }
        num = sum;
    }
    return num;
}
// @lc code=end
