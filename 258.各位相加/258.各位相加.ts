/*
 * @lc app=leetcode.cn id=258 lang=typescript
 *
 * [258] 各位相加
 */

// @lc code=start
function addDigits(num: number): number {
  if (num === 0) return 0;
  if (num % 9 === 0) return 9;
  return num % 9;
}
// @lc code=end
