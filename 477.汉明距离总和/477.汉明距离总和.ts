/*
 * @lc app=leetcode.cn id=477 lang=typescript
 *
 * [477] 汉明距离总和
 */

// @lc code=start
function totalHammingDistance(nums: number[]): number {
  let result = 0;
  const size = nums.length;

  // 遍历每一个二进制位，统计每个二进制位的汉明距离
  for (let i = 0; i < 31; i++) {
    // 统计当前位为1的数量
    let count = 0;
    for (const num of nums) {
      count += (num >> i) & 1;
    }
    // 当前位为1的数量乘上当前位为0的数量，当前位的汉明距离
    result += count * (size - count);
  }

  return result;
}
// @lc code=end
