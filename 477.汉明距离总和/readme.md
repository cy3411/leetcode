# 题目

两个整数的 `汉明距离` 指的是这两个数字的二进制数对应位不同的数量。

计算一个数组中，任意两个数之间汉明距离的总和。

注意:

- 数组中元素的范围为从 0 到 10^9。
- 数组的长度不超过 10^4。

# 示例

```
输入: 4, 14, 2

输出: 6

解释: 在二进制表示中，4表示为0100，14表示为1110，2表示为0010。（这样表示是为了体现后四位之间关系）
所以答案为：
HammingDistance(4, 14) + HammingDistance(4, 2) + HammingDistance(14, 2) = 2 + 2 + 2 = 6.
```

# 题解

遍历每个二进制位，计算数组中的元素在当前位共有多少个 1 和多少个 0，数量相乘就是结果。

```ts
function totalHammingDistance(nums: number[]): number {
  let result = 0;
  const size = nums.length;

  // 遍历每一个二进制位，统计每个二进制位的汉明距离
  for (let i = 0; i < 31; i++) {
    // 统计当前位为1的数量
    let count = 0;
    for (const num of nums) {
      count += (num >> i) & 1;
    }
    // 当前位为1的数量乘上当前位为0的数量，当前位的汉明距离
    result += count * (size - count);
  }

  return result;
}
```
