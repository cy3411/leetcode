# 题目

给你一个字符串 `s` ，如果可以将它分割成三个 **非空** 回文子字符串，那么返回 `true` ，否则返回 `false` 。

当一个字符串正着读和反着读是一模一样的，就称其为 **回文字符串** 。

提示：

- $3 \leq s.length \leq 2000$
- `s`​​​​​​ 只包含小写英文字母。

# 示例

```
输入：s = "abcbdd"
输出：true
解释："abcbdd" = "a" + "bcb" + "dd"，三个子字符串都是回文的。
```

```
输入：s = "bcbddxy"
输出：false
解释：s 没办法被分割成 3 个回文子字符串。
```

# 题解

## 哈希表

假设字符串 `(i,j)` 是回文串， 那么 `(0,i-1)` 和 `(j+1,n-1)` 也必须是回文串。

我们可以预处理所有下标 `0` 开始的回文串和下标 `n-1` 结束的回文串，然后遍历字符串，如果字符串 `(i,j)` 是回文串，去查询预处理信息中是否存在 `(0,i-1)` 和 `(j+1,n-1)` 的回文。是的话，满足题意，返回 `true`

```ts
function checkPartitioning(s: string): boolean {
  const n = s.length;
  // 以0为起点，构成回文串的结束下标
  const hash0 = new Set<number>();
  // 以 n -1 为终点，构成回文串的开始下标
  const hashn = new Set<number>();
  // 处理回文串
  const extract = (i: number, j: number) => {
    while (i >= 0 && j < n && s[i] === s[j]) {
      if (i === 0) hash0.add(j);
      if (j === n - 1) hashn.add(i);
      i--, j++;
    }
  };
  // 判断是否有满足题意的三段回文串
  const isCheck = (i: number, j: number): boolean => {
    while (s[i] === s[j]) {
      if (hash0.has(i - 1) && hashn.has(j + 1)) return true;
      i--, j++;
    }
    return false;
  };
  // 预处理
  for (let i = 0; i < n; i++) {
    extract(i, i);
    extract(i, i + 1);
  }

  // 判断是否可以分割
  for (let i = 1; i < n - 1; i++) {
    if (isCheck(i, i) || isCheck(i, i + 1)) return true;
  }

  return false;
}
```

```cpp
class Solution
{
public:
    unordered_set<int> hash0, hashn;
    int n;
    void extract(string &s, int i, int j)
    {
        while (i >= 0 && j < n && s[i] == s[j])
        {
            if (i == 0)
                hash0.insert(j);
            if (j == n - 1)
                hashn.insert(i);
            i--, j++;
        }
    }
    bool isCheck(string &s, int i, int j)
    {
        while (i >= 0 && j < n && s[i] == s[j])
        {
            if (hash0.count(i - 1) && hashn.count(j + 1))
                return true;
            i--, j++;
        }
        return false;
    }
    bool checkPartitioning(string s)
    {
        n = s.size();
        for (int i = 0; i < n; i++)
        {
            extract(s, i, i);
            extract(s, i, i + 1);
        }

        for (int i = 1; i < n - 1; i++)
        {
            if (isCheck(s, i, i) || isCheck(s, i, i + 1))
                return true;
        }

        return false;
    }
};
```
