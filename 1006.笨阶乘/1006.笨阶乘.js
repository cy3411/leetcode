/*
 * @lc app=leetcode.cn id=1006 lang=javascript
 *
 * [1006] 笨阶乘
 */

// @lc code=start
/**
 * @param {number} N
 * @return {number}
 */
var clumsy = function (N) {
  const stack = [N];
  // 控制操作符
  let idx = 0;
  while (--N > 0) {
    // 乘法
    if (idx % 4 === 0) {
      stack.push(N * stack.pop());
      // 除法
    } else if (idx % 4 === 1) {
      let num = stack.pop();
      // 有负数的存在，要判断是向上取整还是向下取整
      stack.push(num > 0 ? Math.floor(num / N) : Math.ceil(num / N));
      // 加法
    } else if (idx % 4 == 2) {
      stack.push(N);
      // 减法
    } else if (idx % 4 === 3) {
      stack.push(-N);
    }
    idx++;
  }
  let result = stack.reduce((acc, curr) => acc + curr);

  return result;
};
// @lc code=end
