# 题目

给你一个仅包含小写英文字母和 `'?'` 字符的字符串 `s`，请你将所有的 `'?'` 转换为若干小写字母，使最终的字符串不包含任何 **连续重复** 的字符。

注意：你 **不能** 修改非 `'?'` 字符。

题目测试用例保证 除 `'?'` 字符 之外，不存在连续重复的字符。

在完成所有转换（可能无需转换）后返回最终的字符串。如果有多个解决方案，请返回其中任何一个。可以证明，在给定的约束条件下，答案总是存在的。

提示：

- $\color{burlywood}1 \leq s.length \leq 100$
- `s` 仅包含小写英文字母和 `'?'` 字符

# 示例

```
输入：s = "?zs"
输出："azs"
解释：该示例共有 25 种解决方案，从 "azs" 到 "yzs" 都是符合题目要求的。只有 "z" 是无效的修改，因为字符串 "zzs" 中有连续重复的两个 'z' 。
```

```
输入：s = "ubv?w"
输出："ubvaw"
解释：该示例共有 24 种解决方案，只有替换成 "v" 和 "w" 不符合题目要求。因为 "ubvvw" 和 "ubvww" 都包含连续重复的字符。
```

# 题解

## 枚举

枚举每一个字符，当字符为 `'?'` 时，判断当前位置的两边是否和将要填入的字符相同，如果不同，则填入字符，否则不填入，选择下一个字符继续尝试填入。

由于只需要判断连续 3 位的字符，所以选择填入的字符只需要 3 个不同的字符就可以满足题意的连续不重复性。

```ts
function modifyString(s: string): string {
  const ans = s.split('');

  for (let i = 0; i < ans.length; i++) {
    if (ans[i] !== '?') continue;
    // 只需要比较连续3个字母，所以3个字母就可以解决不重复问题
    // js数组越界的话会返回undefined,所以这里没处理越界情况
    if (ans[i - 1] !== 'a' && ans[i + 1] !== 'a') {
      ans[i] = 'a';
    } else if (ans[i - 1] !== 'b' && ans[i + 1] !== 'b') {
      ans[i] = 'b';
    } else {
      ans[i] = 'c';
    }
  }

  return ans.join('');
}
```

```cpp
class Solution
{
public:
    string modifyString(string s)
    {
        for (int i = 0; i < s.size(); i++)
        {
            if (s[i] != '?')
            {

                continue;
            }

            for (char c = 'a'; c <= 'c'; c++)
            {
                if (i > 0 && s[i - 1] == c)
                {
                    continue;
                }
                if (i < s.size() - 1 && s[i + 1] == c)
                {
                    continue;
                }
                s[i] = c;
                break;
            }
        }

        return s;
    }
};
```
