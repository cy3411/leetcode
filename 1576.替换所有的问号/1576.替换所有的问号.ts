/*
 * @lc app=leetcode.cn id=1576 lang=typescript
 *
 * [1576] 替换所有的问号
 */

// @lc code=start
function modifyString(s: string): string {
  const ans = s.split('');

  for (let i = 0; i < ans.length; i++) {
    if (ans[i] !== '?') continue;
    // 只需要比较连续3个字母，所以3个字母就可以解决不重复问题
    if (ans[i - 1] !== 'a' && ans[i + 1] !== 'a') {
      ans[i] = 'a';
    } else if (ans[i - 1] !== 'b' && ans[i + 1] !== 'b') {
      ans[i] = 'b';
    } else {
      ans[i] = 'c';
    }
  }

  return ans.join('');
}
// @lc code=end
