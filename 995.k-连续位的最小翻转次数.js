/*
 * @lc app=leetcode.cn id=995 lang=javascript
 *
 * [995] K 连续位的最小翻转次数
 */

// @lc code=start
/**
 * @param {number[]} A
 * @param {number} K
 * @return {number}
 */
var minKBitFlips = function (A, K) {
  const size = A.length;
  let result = 0;

  for (let i = 0; i < size - K + 1; i++) {
    if (A[i] === 1) {
      continue;
    }
    for (let j = 0; j < K; j++) {
      A[i + j] ^= 1;
    }
    result++;
  }

  for (let i = 0; i < size; i++) {
    if (A[i] === 0) return -1;
  }

  return result;
};
// @lc code=end
