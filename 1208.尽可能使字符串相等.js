/*
 * @lc app=leetcode.cn id=1208 lang=javascript
 *
 * [1208] 尽可能使字符串相等
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @param {number} maxCost
 * @return {number}
 */
var equalSubstring = function (s, t, maxCost) {
  let i = 0;
  let j = 0;
  let result = 0;
  let sum = 0;

  while (j < s.length) {
    sum += Math.abs(s.charCodeAt(j) - t.charCodeAt(j));
    while (sum > maxCost) {
      sum -= Math.abs(s.charCodeAt(i) - t.charCodeAt(i));
      i++;
    }
    result = Math.max(result, j - i + 1);
    j++;
  }

  return result;
};
// @lc code=end
