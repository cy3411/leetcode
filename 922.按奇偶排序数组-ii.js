/*
 * @lc app=leetcode.cn id=922 lang=javascript
 *
 * [922] 按奇偶排序数组 II
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortArrayByParityII = function (A) {
  const size = A.length;
  const result = new Array(size);

  let i = 0;
  for (const num of A) {
    if (!(num & 1)) {
      result[i] = num;
      i += 2;
    }
  }

  i = 1;
  for (const num of A) {
    if (num & 1) {
      result[i] = num;
      i += 2;
    }
  }

  return result;
};
// @lc code=end
