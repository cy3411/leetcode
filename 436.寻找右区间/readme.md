# 题目

给你一个区间数组 `intervals` ，其中 $intervals[i] = [start_i, end_i]$ ，且每个 $start_i$ 都 不同 。

区间 `i` 的 右侧区间 可以记作区间 `j` ，并满足 $start_j \geq end_i$ ，且 $start_j$ 最小化 。

返回一个由每个区间 `i` 的 右侧区间 在 `intervals` 中对应下标组成的数组。如果某个区间 `i` 不存在对应的 **右侧区间** ，则下标 `i` 处的值设为 `-1` 。

提示：

- $1 \leq intervals.length \leq 2 * 10^4$
- $intervals[i].length \equiv 2$
- $-106 \leq starti \leq endi \leq 10^6$
- 每个间隔的起点都 **不相同**

# 示例

```
输入：intervals = [[1,2]]
输出：[-1]
解释：集合中只有一个区间，所以输出-1。
```

```
输入：intervals = [[3,4],[2,3],[1,2]]
输出：[-1,0,1]
解释：对于 [3,4] ，没有满足条件的“右侧”区间。
对于 [2,3] ，区间[3,4]具有最小的“右”起点;
对于 [1,2] ，区间[2,3]具有最小的“右”起点。
```

# 题解

## 二分查找

题意就是找到离当前区间右边界最近的左边界，然后返回其索引。

我们可以将所有区间的左边界按照升序排序后，放入 startIdx 数组中，然后遍历区间数组，对于每个区间，我们可以使用二分查找的方法找到离其右边界最近的左边界，然后返回其索引。

```ts
function findRightInterval(intervals: number[][]): number[] {
  const n = intervals.length;
  const startIdx: number[][] = new Array(n).fill(0).map((_) => new Array(2).fill(0));
  // 将每个区间的起始点和下标存入数组
  for (let i = 0; i < n; i++) {
    startIdx[i][0] = intervals[i][0];
    startIdx[i][1] = i;
  }
  // 排序
  startIdx.sort((a: number[], b: number[]) => a[0] - b[0]);

  // 二分查找，找到第一个大于等于当前右区间的下标
  const ans = new Array(n).fill(-1);
  for (let i = 0; i < n; i++) {
    let l = 0;
    let r = n - 1;
    let mid: number;
    let res = -1;
    while (l <= r) {
      mid = l + ((r - l) >> 1);
      if (startIdx[mid][0] >= intervals[i][1]) {
        res = startIdx[mid][1];
        r = mid - 1;
      } else {
        l = mid + 1;
      }
    }
    ans[i] = res;
  }
  return ans;
}
```
