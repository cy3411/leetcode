/*
 * @lc app=leetcode.cn id=116 lang=javascript
 *
 * [116] 填充每个节点的下一个右侧节点指针
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
  if (!root) {
    return root
  }

  const que = [root]

  while (que.length) {
    const size = que.length
    for (let i = 0; i < size; i++) {
      let node = que.shift()
      node.next = i === size - 1 ? null : que[0]

      if (node.left) {
        que.push(node.left)
      }

      if (node.right) {
        que.push(node.right)
      }
    }
  }

  return root
};
// @lc code=end

