/*
 * @lc app=leetcode.cn id=1614 lang=typescript
 *
 * [1614] 括号的最大嵌套深度
 */

// @lc code=start
function maxDepth(s: string): number {
  const stack = [];
  let max = 0;
  for (let i = 0; i < s.length; i++) {
    if (s[i] !== '(' && s[i] !== ')') continue;
    if (s[i] === '(') {
      stack.push(s[i]);
    } else {
      stack.pop();
    }

    max = Math.max(max, stack.length);
  }
  return max;
}
// @lc code=end
