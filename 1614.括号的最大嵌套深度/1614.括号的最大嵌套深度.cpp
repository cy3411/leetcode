/*
 * @lc app=leetcode.cn id=1614 lang=cpp
 *
 * [1614] 括号的最大嵌套深度
 */

// @lc code=start
class Solution
{
public:
    int maxDepth(string s)
    {
        stack<char> stk;
        int ans = 0;
        for (char c : s)
        {
            if (c != '(' && c != ')')
            {

                continue;
            }

            if (c == '(')
            {
                stk.push(c);
            }
            else
            {
                stk.pop();
            }
            ans = max(ans, (int)stk.size());
        }

        return ans;
    }
};
// @lc code=end
