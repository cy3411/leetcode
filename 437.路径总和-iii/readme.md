# 题目

给定一个二叉树的根节点 root ，和一个整数 targetSum ，求该二叉树里节点值之和等于 targetSum 的 路径 的数目。

路径 不需要从根节点开始，也不需要在叶子节点结束，但是路径方向必须是向下的（只能从父节点到子节点）。

提示:

- 二叉树的节点个数的范围是 [0,1000]
- -109 <= Node.val <= 109
- -1000 <= targetSum <= 1000

# 示例

[![fpDOV1.png](https://z3.ax1x.com/2021/08/02/fpDOV1.png)](https://imgtu.com/i/fpDOV1)

```
输入：root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
输出：3
解释：和等于 8 的路径有 3 条，如图所示。
```

# 题解

## 深度优先搜索

递归遍历每一条边，遍历过程中，累加每条边访问过的节点，将其计入哈希表中。

同时查询哈希表中是否有(sum-targetSum)元素，有的话，表示找到值为 targetSum 的路径和。

比如示例中，我们计算最右边的路径，递归过程中，我们分别将[10,7,18]累加和存入哈希表中，当我们递归到值为 11 的节点，在哈希表中找到了(18-8=10)这个 key，这就表示找到一个值为 8 的路径和。

这里的解题思路跟 [1.两数之和](https://gitee.com/cy3411/leetcode/tree/master/1.%E4%B8%A4%E6%95%B0%E4%B9%8B%E5%92%8C) 哈希解法一样。

```ts
function count(
  node: TreeNode | null,
  targetSum: number,
  sum: number,
  map: Map<number, number>
): number {
  if (node === null) return 0;
  // 累加路径上所有节点
  sum += node.val;
  // 路径和与目标的差值是否出现过，出现过的话就找到匹配
  let ans = map.get(sum - targetSum) || 0;
  // 将当前累加和存入哈希
  map.set(sum, (map.get(sum) || 0) + 1);
  // 计算左右子节点有几个匹配
  ans += count(node.left, targetSum, sum, map);
  ans += count(node.right, targetSum, sum, map);
  // 当前节点相关路径都计算过了，从哈希中删除
  map.set(sum, map.get(sum) - 1);
  return ans;
}

function pathSum(root: TreeNode | null, targetSum: number): number {
  const map: Map<number, number> = new Map();
  // 初始化，累加和等于targetSum的时候也能找到匹配数
  map.set(0, 1);
  // 递归寻找每条路径是否有匹配结果
  return count(root, targetSum, 0, map);
}
```
