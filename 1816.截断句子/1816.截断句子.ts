/*
 * @lc app=leetcode.cn id=1816 lang=typescript
 *
 * [1816] 截断句子
 */

// @lc code=start
function truncateSentence(s: string, k: number): string {
  let ans = '';
  // 记录统计的word数量
  let count = 0;
  for (let i = 0; i < s.length; i++) {
    let word = '';
    // 记录单词
    while (s[i] !== ' ' && i < s.length) {
      word += s[i];
      i++;
    }
    count++;
    // 如果word数量大于k，则返回ans
    if (count <= k) {
      ans += count === k ? word : word + ' ';
    } else {
      break;
    }
  }

  return ans;
}
// @lc code=end
