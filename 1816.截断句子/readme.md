# 题目

句子 是一个单词列表，列表中的单词之间用单个空格隔开，且不存在前导或尾随空格。每个单词仅由大小写英文字母组成（不含标点符号）。

- 例如，`"Hello World"、"HELLO"` 和 `"hello world hello world"` 都是句子。

给你一个句子 `s​​​​​​` 和一个整数 `k​​​​​​` ，请你将 `s​​` 截断 ​，​​​ 使截断后的句子仅含 前 `k​​​​​​` 个单词。返回 截断 `s​​​​​​` 后得到的句子。

# 示例

```
输入：s = "Hello how are you Contestant", k = 4
输出："Hello how are you"
解释：
s 中的单词为 ["Hello", "how" "are", "you", "Contestant"]
前 4 个单词为 ["Hello", "how", "are", "you"]
因此，应当返回 "Hello how are you"
```

# 题解

## API

将句子分割成单词，然后取前 k 个单词，再将单词拼接成句子。

```ts
function truncateSentence(s: string, k: number): string {
  return s.split(' ').splice(0, k).join(' ');
}
```

## 遍历

```ts
function truncateSentence(s: string, k: number): string {
  let ans = '';
  // 记录统计的word数量
  let count = 0;
  for (let i = 0; i < s.length; i++) {
    let word = '';
    // 记录单词
    while (s[i] !== ' ' && i < s.length) {
      word += s[i];
      i++;
    }
    count++;
    // 如果word数量大于k，则返回ans
    if (count <= k) {
      ans += count === k ? word : word + ' ';
    } else {
      break;
    }
  }

  return ans;
}
```
