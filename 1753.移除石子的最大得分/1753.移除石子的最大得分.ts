/*
 * @lc app=leetcode.cn id=1753 lang=typescript
 *
 * [1753] 移除石子的最大得分
 */

// @lc code=start
function maximumScore(a: number, b: number, c: number): number {
  let [x, y, z] = [a, b, c].sort((a, b) => a - b);
  let ans = 0;

  let count = Math.min(x, z - y);
  x -= count;
  z -= count;
  ans += count;

  if (x > 0) {
    if (x & 1) x -= 1;
    y -= x / 2;
    z -= x / 2;
    ans += x;
  }

  ans += y;

  return ans;
}
// @lc code=end
