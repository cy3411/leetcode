/*
 * @lc app=leetcode.cn id=1753 lang=javascript
 *
 * [1753] 移除石子的最大得分
 */

// @lc code=start
/**
 * @param {number} a
 * @param {number} b
 * @param {number} c
 * @return {number}
 */
var maximumScore = function (a, b, c) {
  // 升序排序，从小到大拿到3堆石子
  let [x, y, z] = [a, b, c].sort((a, b) => a - b);
  let result = 0;

  // 第一步，操作最大堆和最小堆，将最大堆的数量减少和次大堆的数量一样
  let count = Math.min(z - y, x);
  x -= count;
  z -= count;
  result += count;
  // 第二步，将最小堆的数量平分，让最大堆和次大堆同时较少
  if (x !== 0) {
    if ((x & 1) === 1) x -= 1;
    y -= x / 2;
    z -= x / 2;
    result += x;
  }
  // 第三步，最大堆和次大堆数量一样，可以同时操作
  result += y;

  return result;
};
// @lc code=end
