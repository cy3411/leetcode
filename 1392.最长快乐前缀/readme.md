# 题目

「快乐前缀」是在原字符串中既是 非空 前缀也是后缀（不包括原字符串自身）的字符串。

给你一个字符串 `s`，请你返回它的 最长快乐前缀。

如果不存在满足题意的前缀，则返回一个空字符串。

提示：

- $\color{#cda869} 1 \leq s.length \leq 10^5$
- `s` 只含有小写英文字母

# 示例

```
输入：s = "level"
输出："l"
解释：不包括 s 自己，一共有 4 个前缀（"l", "le", "lev", "leve"）和 4 个后缀（"l", "el", "vel", "evel"）。最长的既是前缀也是后缀的字符串是 "l" 。
```

```
输入：s = "ababab"
输出："abab"
解释："abab" 是最长的既是前缀也是后缀的字符串。题目允许前后缀在原字符串中重叠。
```

# 题解

## KMP

利用 KMP 的 next 数组，可以计算出字符串的最长公共前后缀对应的下标。

然后利用下标切割出对应的子串返回。

```ts
function longestPrefix(s: string): string {
  const n = s.length;
  const next = new Array(n).fill(-1);
  let j = -1;
  for (let i = 1; i < n; i++) {
    while (j !== -1 && s[i] !== s[j + 1]) {
      j = next[j];
    }
    if (s[i] === s[j + 1]) {
      j++;
    }
    next[i] = j;
  }

  // 返回公共前缀长度
  return s.substring(0, next[n - 1] + 1);
}
```
