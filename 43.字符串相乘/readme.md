# 题目

给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。

说明：

- num1 和 num2 的长度小于 110。
- num1 和 num2 只包含数字 0-9。
- num1 和 num2 均不以零开头，除非是数字 0 本身。
- 不能使用任何标准库的大数类型（比如 BigInteger）或直接将输入转换为整数来处理。

# 示例

```
输入: num1 = "123", num2 = "456"
输出: "56088"
```

# 题解

## 模拟乘法术式

将 num1 和 num2 按位倒序存入数组中，遍历数组，将每位的相乘结果存入结果数组中。

对结果数组中的每位数字进行进位处理。

最后注意的就是要处理结果数组中的前导零，因为 num1 或者 num2 会有 0 的出现。

```ts
function multiply(num1: string, num2: string): string {
  const m = num1.length;
  const n = num2.length;
  const a = new Array(m).fill(0);
  const b = new Array(n).fill(0);
  //保存乘法结果
  const c = new Array(m + n - 1).fill(0);

  for (let i = 0; i < m; i++) {
    a[m - 1 - i] = Number(num1[i]);
  }
  for (let i = 0; i < n; i++) {
    b[n - 1 - i] = Number(num2[i]);
  }

  // 处理相乘
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      c[i + j] += a[i] * b[j];
    }
  }

  // 处理进位
  for (let i = 0; i < c.length; i++) {
    if (c[i] < 10) continue;
    // 防止高位需要进位
    if (i === c.length - 1) c.push(0);
    c[i + 1] += (c[i] / 10) >> 0;
    c[i] = c[i] % 10;
  }

  // 处理前导零
  while (c.length > 1 && c[c.length - 1] === 0) c.pop();

  // 更新结果
  let ans = '';
  for (let i = c.length - 1; i >= 0; i--) {
    ans += String(c[i]);
  }

  return ans;
}
```
