/*
 * @lc app=leetcode.cn id=599 lang=cpp
 *
 * [599] 两个列表的最小索引总和
 */

// @lc code=start
class Solution
{
public:
    vector<string> findRestaurant(vector<string> &list1, vector<string> &list2)
    {
        unordered_map<string, int> m;
        for (int i = 0; i < list1.size(); i++)
        {
            m[list1[i]] = i;
        }
        vector<string> ans;
        int minSum = INT_MAX;
        for (int i = 0; i < list2.size(); i++)
        {
            string name = list2[i];
            if (m.find(name) == m.end())
            {
                continue;
            }
            int sum = i + m[name];
            if (minSum > sum)
            {
                minSum = sum;
                ans.clear();
                ans.push_back(name);
            }
            else if (minSum == sum)
            {
                ans.push_back(name);
            }
        }
        return ans;
    }
};
// @lc code=end
