# 题目

假设 Andy 和 Doris 想在晚餐时选择一家餐厅，并且他们都有一个表示最喜爱餐厅的列表，每个餐厅的名字用字符串表示。

你需要帮助他们用**最少的索引**和找出他们共同喜爱的餐厅。 如果答案不止一个，则输出所有答案并且不考虑顺序。 你可以假设答案总是存在。

提示:

- $1 <= list1.length, list2.length <= 1000$
- $1 <= list1[i].length, list2[i].length <= 30 $
- `list1[i]` 和 `list2[i]` 由空格 `' '` 和英文字母组成。
- `list1` 的所有字符串都是 **唯一** 的。
- `list2` 中的所有字符串都是 **唯一** 的。

# 示例

```
输入: list1 = ["Shogun", "Tapioca Express", "Burger King", "KFC"]，list2 = ["Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"]
输出: ["Shogun"]
解释: 他们唯一共同喜爱的餐厅是“Shogun”。
```

```
输入:list1 = ["Shogun", "Tapioca Express", "Burger King", "KFC"]，list2 = ["KFC", "Shogun", "Burger King"]
输出: ["Shogun"]
解释: 他们共同喜爱且具有最小索引和的餐厅是“Shogun”，它有最小的索引和1(0+1)。
```

# 题解

## 哈希表

使用哈希表记录 list1 中元素的索引，然后遍历 list2 ，查找哈希表中是否存在该元素，如果存在，则更新索引和。同时更新答案。

```ts
function findRestaurant(list1: string[], list2: string[]): string[] {
  const map = new Map<string, number>();
  // 记录list1每个餐厅的索引
  for (let i = 0; i < list1.length; i++) {
    map.set(list1[i], i);
  }
  // 遍历list2，找出最小索引和
  const ans = [];
  let minSum = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < list2.length; i++) {
    const name = list2[i];
    if (!map.has(name)) continue;
    const sum = i + map.get(name);
    if (sum < minSum) {
      minSum = sum;
      ans.length = 0;
      ans.push(name);
    } else if (sum === minSum) {
      ans.push(name);
    }
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    vector<string> findRestaurant(vector<string> &list1, vector<string> &list2)
    {
        unordered_map<string, int> m;
        for (int i = 0; i < list1.size(); i++)
        {
            m[list1[i]] = i;
        }
        vector<string> ans;
        int minSum = INT_MAX;
        for (int i = 0; i < list2.size(); i++)
        {
            string name = list2[i];
            if (m.find(name) == m.end())
            {
                continue;
            }
            int sum = i + m[name];
            if (minSum > sum)
            {
                minSum = sum;
                ans.clear();
                ans.push_back(name);
            }
            else if (minSum == sum)
            {
                ans.push_back(name);
            }
        }
        return ans;
    }
};
```
