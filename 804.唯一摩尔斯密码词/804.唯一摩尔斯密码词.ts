/*
 * @lc app=leetcode.cn id=804 lang=typescript
 *
 * [804] 唯一摩尔斯密码词
 */

// @lc code=start
function uniqueMorseRepresentations(words: string[]): number {
  const morse = [
    '.-',
    '-...',
    '-.-.',
    '-..',
    '.',
    '..-.',
    '--.',
    '....',
    '..',
    '.---',
    '-.-',
    '.-..',
    '--',
    '-.',
    '---',
    '.--.',
    '--.-',
    '.-.',
    '...',
    '-',
    '..-',
    '...-',
    '.--',
    '-..-',
    '-.--',
    '--..',
  ];
  const set = new Set<string>();
  const base = 'a'.charCodeAt(0);
  for (const word of words) {
    let str = '';
    for (const char of word) {
      str += morse[char.charCodeAt(0) - base];
    }
    set.add(str);
  }

  return set.size;
}
// @lc code=end
