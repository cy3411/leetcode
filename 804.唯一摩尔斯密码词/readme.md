# 题目

国际摩尔斯密码定义一种标准编码方式，将每个字母对应于一个由一系列点和短线组成的字符串， 比如:

- `'a'` 对应 `".-"` ，
- `'b'` 对应 `"-..."` ，
- `'c'` 对应 `"-.-."` ，以此类推。

为了方便，所有 `26` 个英文字母的摩尔斯密码表如下：

```
[".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]
```

给你一个字符串数组 `words` ，每个单词可以写成每个字母对应摩尔斯密码的组合。

- 例如，`"cab"` 可以写成 `"-.-..--..."` ，(即 `"-.-."` + `".-"` + `"-..."` 字符串的结合)。我们将这样一个连接过程称作 **单词翻译** 。

对 `words` 中所有单词进行单词翻译，返回不同 **单词翻译** 的数量。

提示：

- $1 \leq words.length \leq 100$
- $1 \leq words[i].length \leq 12$
- `words[i]` 由小写英文字母组成

# 示例

```
输入: words = ["gin", "zen", "gig", "msg"]
输出: 2
解释:
各单词翻译如下:
"gin" -> "--...-."
"zen" -> "--...-."
"gig" -> "--...--."
"msg" -> "--...--."

共有 2 种不同翻译, "--...-." 和 "--...--.".
```

```
输入：words = ["a"]
输出：1
```

# 题解

## 哈希表

将每个翻译完的单词放入 set 中，最后返回 set 的长度。

```ts
function uniqueMorseRepresentations(words: string[]): number {
  const morse = [
    '.-',
    '-...',
    '-.-.',
    '-..',
    '.',
    '..-.',
    '--.',
    '....',
    '..',
    '.---',
    '-.-',
    '.-..',
    '--',
    '-.',
    '---',
    '.--.',
    '--.-',
    '.-.',
    '...',
    '-',
    '..-',
    '...-',
    '.--',
    '-..-',
    '-.--',
    '--..',
  ];
  const set = new Set<string>();
  const base = 'a'.charCodeAt(0);
  for (const word of words) {
    let str = '';
    for (const char of word) {
      str += morse[char.charCodeAt(0) - base];
    }
    set.add(str);
  }

  return set.size;
}
```

```cpp
class Solution
{
public:
    string morse[26] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
    string converToMorse(string s)
    {
        string res;
        for (auto &c : s)
        {
            res += morse[c - 'a'];
        }
        return res;
    }
    int uniqueMorseRepresentations(vector<string> &words)
    {
        unordered_set<string> set;
        for (auto &word : words)
        {
            set.insert(converToMorse(word));
        }
        return set.size();
    }
};
```
