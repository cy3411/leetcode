/*
 * @lc app=leetcode.cn id=804 lang=cpp
 *
 * [804] 唯一摩尔斯密码词
 */

// @lc code=start
class Solution
{
public:
    string morse[26] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
    string converToMorse(string s)
    {
        string res;
        for (auto &c : s)
        {
            res += morse[c - 'a'];
        }
        return res;
    }
    int uniqueMorseRepresentations(vector<string> &words)
    {
        unordered_set<string> set;
        for (auto &word : words)
        {
            set.insert(converToMorse(word));
        }
        return set.size();
    }
};
// @lc code=end
