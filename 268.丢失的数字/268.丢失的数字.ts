/*
 * @lc app=leetcode.cn id=268 lang=typescript
 *
 * [268] 丢失的数字
 */

// @lc code=start
function missingNumber(nums: number[]): number {
  const hash = new Set<number>();
  // 记录数组中的数字
  for (const num of nums) {
    hash.add(num);
  }
  // 找出数组中没有的数字
  for (let i = 0; i <= nums.length; i++) {
    if (!hash.has(i)) {
      return i;
    }
  }
}
// @lc code=end
