# 题目

给定一个包含 [0, n] 中 n 个数的数组 nums ，找出 [0, n] 这个范围内没有出现在数组中的那个数。

提示：

- n == nums.length
- $1 <= n <= 10^4$
- 0 <= nums[i] <= n
- `nums` 中的所有数字都 **独一无二**

进阶：你能否实现线性时间复杂度、仅使用额外常数空间的算法解决此问题?

# 示例

```
输入：nums = [3,0,1]
输出：2
解释：n = 3，因为有 3 个数字，所以所有的数字都在范围 [0,3] 内。2 是丢失的数字，因为它没有出现在 nums 中。
```

```
输入：nums = [0,1]
输出：2
解释：n = 2，因为有 2 个数字，所以所有的数字都在范围 [0,2] 内。2 是丢失的数字，因为它没有出现在 nums 中。
```

# 题解

## 位运算

异或运算满足交换律和结合律，且对于任何整数 `x` 都满足于 `x^0=x`，`x^x=0`。

根据数字出现的奇偶性，可以通过异或获得丢失的数字。

我们对数组整数进行 `2n+1` 次异或运算，得到的结果即为丢失的数字。

```ts
function missingNumber(nums: number[]): number {
  let xor = 0;
  const n = nums.length;
  // 先求出数组的异或值
  for (let i = 0; i < n; i++) {
    xor ^= i ^ nums[i];
  }

  return xor ^ n;
}
```

## 哈希表

使用哈希表记录数组中的数字，然后遍历数组，检查哈希表中是否存在丢失的数字。

```ts
function missingNumber(nums: number[]): number {
  const hash = new Set<number>();
  // 记录数组中的数字
  for (const num of nums) {
    hash.add(num);
  }
  // 找出数组中没有的数字
  for (let i = 0; i <= nums.length; i++) {
    if (!hash.has(i)) {
      return i;
    }
  }
}
```
