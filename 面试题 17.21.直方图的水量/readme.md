# 题目
给定一个直方图(也称柱状图)，假设有人从上面源源不断地倒水，最后直方图能存多少水量?直方图的宽度为 1。

![cmGF2D.png](https://z3.ax1x.com/2021/04/02/cmGF2D.png)

上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的直方图，在这种情况下，可以接 6 个单位的水（蓝色部分表示水）。 感谢 Marcos 贡献此图。

# 示例
```
输入: [0,1,0,2,1,0,1,3,2,1,2,1]
输出: 6
```

# 方法
每个位置能接多少雨水，很容易想到「木桶效应」，即是由两边最短的木板限制的。

那么直观思路就是，对于每个位置，向左右找最高的木板；当前位置能放的水量是：左右两边最高木板的最低高度 - 当前高度。

![cmJSyQ.md.png](https://z3.ax1x.com/2021/04/02/cmJSyQ.md.png)

## 暴力
```js
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  let res = 0;
  // 第一个位置和最后一个位置不能蓄水，不用计算
  for (let i = 1; i < height.length - 1; i++) {
    let maxLeftHeight = Math.max(...height.slice(0, i));
    let maxRightHeight = Math.max(...height.slice(i + 1));
    let h = Math.min(maxLeftHeight, maxRightHeight) - height[i];
    if (h > 0) res += h;
  }

  return res;
};
```
+ 时间复杂度：O(N^2)
+ 空间复杂度：O(1)

## 动态规划
上面代码每次计算左边最高值和右边最高值的时候都有重复计算。

直观点的优化就是提前求出左边和右边最高值，结果保存起来，就可以避免for循环中的计算了。

```js
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  const size = height.length;
  // 小于3个柱子，不能蓄水
  if (size <= 2) return 0;
  // 计算每个柱子左边最高柱子
  const maxLeft = new Array(size);
  maxLeft[0] = height[0];
  for (let i = 1; i < size; i++) {
    maxLeft[i] = Math.max(maxLeft[i - 1], height[i]);
  }
  // 计算每个柱子右边最高柱子
  const maxRight = new Array(size);
  maxRight[size - 1] = height[size - 1];
  for (let i = size - 2; i >= 0; i--) {
    maxRight[i] = Math.max(maxRight[i + 1], height[i]);
  }
  // 计算蓄水量
  let result = 0;
  for (let i = 1; i < size - 1; i++) {
    const h = Math.min(maxLeft[i], maxRight[i]) - height[i];
    if (h > 0) {
      result += h;
    }
  }

  return result;
};
```

+ 时间复杂度：O(N)
+ 空间复杂度：O(N)


## 单调栈
维护一个单调栈，栈底到栈顶递减，栈内保存数组下标。

遍历数组，遍历到下标i时，如果栈内有最少2个元素，栈顶记做mid，栈顶的下一个元素是left，则一定有 `height[left]>=height[mid]<height[i]` ,则可以得到一个凹槽用于接水。

我们计算凹槽面积，宽度 = `i-left+1-2`,高度=`min(height[left],height[i])-height[mid]`。

```js
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  /**
   * 单调栈
   * @param {number[]} height
   * @return {number}
   */
  var trap = function (height) {
    const size = height.length;

    if (size < 3) return 0;
    // 栈底到栈顶的单调递增，存储下标
    const stack = [];
    let result = 0;
    for (let i = 0; i < size; i++) {
      // 栈顶小于当前元素时候，如果栈内元素最少为2个，height[栈顶-1]>height[栈顶]<height[当前元素]
      // 这样才会形成一个凹槽，来接水
      while (stack.length && height[i] > height[stack[stack.length - 1]]) {
        let mid = stack.pop();
        if (stack.length) {
          let left = stack[stack.length - 1];
          let w = i - left + 1 - 2;
          let h = Math.min(height[left], height[i]) - height[mid];
          result += w * h;
        }
      }
      stack.push(i);
    }

    return result;
  };
};
```

+ 时间复杂度：O(N)
+ 空间复杂度：O(N)