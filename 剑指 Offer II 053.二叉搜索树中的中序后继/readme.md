# 题目

给定一棵二叉搜索树和其中的一个节点 p ，找到该节点在树中的中序后继。如果节点没有中序后继，请返回 null 。

节点 p 的后继是值比 p.val 大的节点中键值最小的节点，即按中序遍历的顺序节点 p 的下一个节点。

提示：

- 树中节点的数目在范围 [1, 104] 内。
- -105 <= Node.val <= 105
- 树中各节点的值均保证唯一。

# 示例

[![ff7BSU.png](https://z3.ax1x.com/2021/08/16/ff7BSU.png)](https://imgtu.com/i/ff7BSU)

```
输入：root = [2,1,3], p = 1
输出：2
解释：这里 1 的中序后继是 2。请注意 p 和返回值都应是 TreeNode 类型。
```

# 题解

## 中序遍历

对二叉树进行中序遍历，遍历过程中，如果前一个节点是 p，那么当前节点就是 p 的后继节点。

所以我们需要定义 pre 记录前一个节点，遍历过程中，`pre=p`，当前遍历的节点就是答案。

```ts
function inorderSuccessor(root: TreeNode | null, p: TreeNode | null): TreeNode | null {
  // 中序遍历
  const inorder = (node: TreeNode | null) => {
    if (node === null) return;
    inorder(node.left);
    if (pre === p) ans = node;
    pre = node;
    inorder(node.right);
  };
  let pre = null;
  let ans = null;
  inorder(root);
  return ans;
}
```

如果找到答案，提前退出递归。

```ts
function inorderSuccessor(root: TreeNode | null, p: TreeNode | null): TreeNode | null {
  // 中序遍历
  const inorder = (node: TreeNode | null) => {
    if (node === null) return null;
    let res = inorder(node.left);
    if (res) return res;
    if (pre === p) return node;
    pre = node;
    res = inorder(node.right);
    if (res) return res;
    return null;
  };
  let pre = null;
  return inorder(root);
}
```
