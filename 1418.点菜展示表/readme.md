# 题目

给你一个数组 orders，表示客户在餐厅中完成的订单，确切地说， orders[i]=[customerNamei,tableNumberi,foodItemi] ，其中 customerNamei 是客户的姓名，tableNumberi 是客户所在餐桌的桌号，而 foodItemi 是客户点的餐品名称。

请你返回该餐厅的 点菜展示表 。在这张表中，表中第一行为标题，其第一列为餐桌桌号 “Table” ，后面每一列都是按字母顺序排列的餐品名称。接下来每一行中的项则表示每张餐桌订购的相应餐品数量，第一列应当填对应的桌号，后面依次填写下单的餐品数量。

注意：客户姓名不是点菜展示表的一部分。此外，表中的数据行应该按餐桌桌号升序排列。

提示：

- 1 <= orders.length <= 5 \* 10^4
- orders[i].length == 3
- 1 <= customerNamei.length, foodItemi.length <= 20
- customerNamei 和 foodItemi 由大小写英文字母及空格字符 ' ' 组成。
- tableNumberi 是 1 到 500 范围内的整数。

# 示例

```
输入：orders = [["David","3","Ceviche"],["Corina","10","Beef Burrito"],["David","3","Fried Chicken"],["Carla","5","Water"],["Carla","5","Ceviche"],["Rous","3","Ceviche"]]
输出：[["Table","Beef Burrito","Ceviche","Fried Chicken","Water"],["3","0","2","1","0"],["5","0","1","0","1"],["10","1","0","0","0"]]
解释：
点菜展示表如下所示：
Table,Beef Burrito,Ceviche,Fried Chicken,Water
3    ,0           ,2      ,1            ,0
5    ,0           ,1      ,0            ,1
10   ,1           ,0      ,0            ,0
对于餐桌 3：David 点了 "Ceviche" 和 "Fried Chicken"，而 Rous 点了 "Ceviche"
而餐桌 5：Carla 点了 "Water" 和 "Ceviche"
餐桌 10：Corina 点了 "Beef Burrito"
```

# 题解

## 哈希表

利用哈希表保存 name 和 table 对应的餐饮。

分别排序后，将结果遍历更新答案。

```ts
function displayTable(orders: string[][]): string[][] {
  const temp: Set<string> = new Set();
  const hash: Map<number, Map<string, number>> = new Map();

  // 将name和food保存到哈希表中
  for (let order of orders) {
    const name = order[2];
    const table = Number(order[1]);
    temp.add(name);
    let food: Map<string, number> = hash.has(table) ? hash.get(table) : new Map();
    food.set(name, (food.get(name) || 0) + 1);
    hash.set(table, food);
  }
  // name排序
  const names = [...temp].sort();

  const ans = [['Table', ...names]];

  const tables = Array.from(hash);
  tables.sort((a, b) => a[0] - b[0]);

  // 遍历，计算出每桌的食物数量
  for (let [table, food] of tables) {
    const temp = [];
    temp.push(String(table));
    for (let i = 1; i < ans[0].length; i++) {
      const name = ans[0][i];
      temp.push(food.has(name) ? String(food.get(name)) : '0');
    }
    ans.push(temp);
  }

  return ans;
}
```
