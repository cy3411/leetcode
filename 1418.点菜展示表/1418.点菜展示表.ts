/*
 * @lc app=leetcode.cn id=1418 lang=typescript
 *
 * [1418] 点菜展示表
 */

// @lc code=start
function displayTable(orders: string[][]): string[][] {
  const temp: Set<string> = new Set();
  const hash: Map<number, Map<string, number>> = new Map();

  // 将name和food保存到哈希表中
  for (let order of orders) {
    const name = order[2];
    const table = Number(order[1]);
    temp.add(name);
    let food: Map<string, number> = hash.has(table) ? hash.get(table) : new Map();
    food.set(name, (food.get(name) || 0) + 1);
    hash.set(table, food);
  }
  // name排序
  const names = [...temp].sort();

  const ans = [['Table', ...names]];

  const tables = Array.from(hash);
  tables.sort((a, b) => a[0] - b[0]);

  // 遍历，计算出每桌的食物数量
  for (let [table, food] of tables) {
    const temp = [];
    temp.push(String(table));
    for (let i = 1; i < ans[0].length; i++) {
      const name = ans[0][i];
      temp.push(food.has(name) ? String(food.get(name)) : '0');
    }
    ans.push(temp);
  }

  return ans;
}
// @lc code=end
