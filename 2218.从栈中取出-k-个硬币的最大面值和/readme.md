# 题目

一张桌子上总共有 `n` 个硬币 **栈** 。每个栈有 **正整数** 个带面值的硬币。

每一次操作中，你可以从任意一个栈的 **顶**部 **取出** `1` 个硬币，从栈中移除它，并放入你的钱包里。

给你一个列表 `piles` ，其中 `piles[i]` 是一个整数数组，分别表示第 `i` 个栈里 **从顶到底** 的硬币面值。同时给你一个正整数 `k` ，请你返回在 **恰好** 进行 `k` 次操作的前提下，你钱包里硬币面值之和 **最大为多少** 。

提示：

- $n \equiv piles.length$
- $1 \leq n \leq 1000$
- $1 \leq piles[i][j] \leq 10^5$
- $1 \leq k \leq sum(piles[i].length) \leq 2000$

# 示例

[![L7LBlD.png](https://s1.ax1x.com/2022/04/26/L7LBlD.png)](https://imgtu.com/i/L7LBlD)

```
输入：piles = [[1,100,3],[7,8,9]], k = 2
输出：101
解释：
上图展示了几种选择 k 个硬币的不同方法。
我们可以得到的最大面值为 101 。
```

```
输入：piles = [[100],[100],[100],[100],[100],[100],[1,1,1,1,1,1,700]], k = 7
输出：706
解释：
如果我们所有硬币都从最后一个栈中取，可以得到最大面值和。
```

# 题解

## 动态规划

我们将栈转换一下，假设栈为 {1,100,3}， 栈中元素消耗的操作和取值分别为(1，1)、(2, 101)、(3, 104)，这样就把问题转换成分组背包问题。

定义状态：

$$
    dp[i][j] 表示前 i 个栈中，操作 j 次可以得到的最大面值
$$

状态转移：

$$
    dp[i][j] = max(dp[i - 1][j], dp[i - 1][j -w] + v)
$$

初始化

$$
    dp[0][0] = 0
$$

```ts
function maxValueOfCoins(piles: number[][], k: number): number {
  const n = piles.length;
  // dp[i][j] 表示前 i 个栈，能取 j 次的最大值
  const dp: number[][] = new Array(n + 1).fill(0).map(() => new Array(k + 1).fill(0));

  for (let i = 1; i <= n; i++) {
    for (let j = 1; j <= k; j++) {
      // 当前栈不选择
      dp[i][j] = dp[i - 1][j];
      let val = 0;
      // 当前栈选择硬币，遍历选择一个值最大的且不超过 j 次
      for (let x = 1; x <= piles[i - 1].length; x++) {
        val += piles[i - 1][x - 1];
        if (x > j) break;
        dp[i][j] = Math.max(dp[i][j], dp[i - 1][j - x] + val);
      }
    }
  }

  return dp[n][k];
}
```

```cpp
class Solution
{
public:
    int maxValueOfCoins(vector<vector<int>> &piles, int k)
    {
        int n = piles.size();
        vector<vector<int>> dp(n + 1, vector<int>(k + 1, 0));

        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= k; j++)
            {
                dp[i][j] = dp[i - 1][j];
                int x = 0, y = 0;
                for (auto t : piles[i - 1])
                {
                    x++, y += t;
                    if (x > j)
                        break;
                    dp[i][j] = max(dp[i][j], dp[i - 1][j - x] + y);
                }
            }
        }

        return dp[n][k];
    }
};
```
