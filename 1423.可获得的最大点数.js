/*
 * @lc app=leetcode.cn id=1423 lang=javascript
 *
 * [1423] 可获得的最大点数
 */

// @lc code=start
/**
 * @param {number[]} cardPoints
 * @param {number} k
 * @return {number}
 */
var maxScore = function (cardPoints, k) {
  let result = 0;
  for (let i = 0; i < k; i++) {
    result += cardPoints[i];
  }

  let sum = result;
  for (let i = 0; i < k; i++) {
    sum -= cardPoints[k - i - 1];
    sum += cardPoints[cardPoints.length - 1 - i];
    result = Math.max(result, sum);
  }

  return result;
};
// @lc code=end
