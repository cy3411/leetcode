/*
 * @lc app=leetcode.cn id=977 lang=javascript
 *
 * [977] 有序数组的平方
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortedSquares = function (A) {
  const size = A.length
  const res = new Array(size - 1).fill(0)

  let i = 0, j = size - 1, pos = size - 1

  while (i <= j) {
    if (A[i] * A[i] > A[j] * A[j]) {
      res[pos] = A[i] * A[i]
      i++
    } else {
      res[pos] = A[j] * A[j]
      j--
    }
    pos--
  }

  return res
};
// @lc code=end

