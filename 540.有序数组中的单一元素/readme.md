# 题目

给你一个仅由整数组成的有序数组，其中每个元素都会出现两次，唯有一个数只会出现一次。

请你找出并返回只出现一次的那个数。

你设计的解决方案必须满足 `O(log n)` 时间复杂度和 `O(1)` 空间复杂度。

提示:

- $\color{burlywood} 1 \leq nums.length \leq 10^5$
- $\color{burlywood} 0 \leq nums[i] \leq 10^5$

# 示例

```
输入: nums = [1,1,2,3,3,4,4,8,8]
输出: 2
```

```
输入: nums =  [3,3,7,7,10,11,11]
输出: 10
```

# 题解

## 二分查找

数组是一个有序数组，而且题目要求的时间复杂度是 `O(log n)`，所以我们可以使用二分查找。

有序数组长度一定是奇数，因为每个数出现两次，只有一个数出现一次。

假设 mid 的位置在目标值的左，它有如下特性：

- mid 为奇数的话，nums[mid] === nums[mid - 1]
- mid 为偶数的话，nums[mid] === nums[mid + 1]

如果 mid 的位置在目标值的右边，特性跟上面刚好相反。

我们可以通过上面的特性，来收缩左右边界，直到左右边界相遇。

```ts
function singleNonDuplicate(nums: number[]): number {
  // mid 是否在目标的左边
  const isLeft = (mid: number): boolean => {
    if (mid % 2 === 0 && nums[mid] === nums[mid + 1]) {
      return true;
    } else if (mid % 2 === 1 && nums[mid] === nums[mid - 1]) {
      return true;
    }
    return false;
  };

  let l = 0;
  let r = nums.length - 1;
  while (l < r) {
    let mid = l + ((r - l) >> 1);
    if (isLeft(mid)) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }

  return nums[l];
}
```

```cpp
class Solution
{
public:
    bool isLeft(vector<int> &nums, int mid)
    {
        if (mid % 2 == 0 && nums[mid] == nums[mid + 1])
        {
            return true;
        }
        if (mid % 2 == 1 && nums[mid] == nums[mid - 1])
        {
            return true;
        }
        return false;
    }

    int singleNonDuplicate(vector<int> &nums)
    {
        int l = 0, r = nums.size() - 1, mid;
        while (l < r)
        {
            mid = l + (r - l) / 2;
            if (isLeft(nums, mid))
            {
                l = mid + 1;
            }
            else
            {
                r = mid;
            }
        }
        return nums[l];
    }
};
```
