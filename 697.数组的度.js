/*
 * @lc app=leetcode.cn id=697 lang=javascript
 *
 * [697] 数组的度
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findShortestSubArray = function (nums) {
  const freq = new Map();
  // 计算度数
  for (const [i, num] of nums.entries()) {
    if (freq.has(num)) {
      let state = freq.get(num);
      state[0]++;
      state[2] = i;
    } else {
      freq.set(num, [1, i, i]);
    }
  }
  let maxCount = 0;
  let minLength = 0;
  // 计算结果
  for (const [count, left, right] of freq.values()) {
    if (maxCount < count) {
      maxCount = count;
      minLength = right - left + 1;
    } else if (maxCount === count) {
      minLength = Math.min(minLength, right - left + 1);
    }
  }
  return minLength;
};
// @lc code=end
