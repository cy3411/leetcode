# 题目描述
请你仅使用两个栈实现先入先出队列。队列应当支持一般队列的支持的所有操作（push、pop、peek、empty）：

实现 MyQueue 类：

+ void push(int x) 将元素 x 推到队列的末尾
+ int pop() 从队列的开头移除并返回元素
+ int peek() 返回队列开头的元素
+ boolean empty() 如果队列为空，返回 true ；否则，返回 false

# 示例

```
输入：
["MyQueue", "push", "push", "peek", "pop", "empty"]
[[], [1], [2], [], [], []]
输出：
[null, null, null, 1, 1, false]

解释：
MyQueue myQueue = new MyQueue();
myQueue.push(1); // queue is: [1]
myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
myQueue.peek(); // return 1
myQueue.pop(); // return 1, queue is [2]
myQueue.empty(); // return false
```

# 方法
2个栈，1个存push的数据，1个存pop的数据，pop栈为空时，将push栈里的数据全部放入pop栈即可。

```javascript
/**
 * Initialize your data structure here.
 */
var MyQueue = function () {
  this.pushStack = [];
  this.popStack = [];
  this.size = 0;
};

/**
 * Push element x to the back of queue.
 * @param {number} x
 * @return {void}
 */
MyQueue.prototype.push = function (x) {
  this.pushStack.push(x);
  this.size++;
};

/**
 * Removes the element from in front of queue and returns that element.
 * @return {number}
 */
MyQueue.prototype.pop = function () {
  if (this.popStack.length === 0) {
    let pushStackSize = this.pushStack.length;
    while (pushStackSize--) {
      this.popStack.push(this.pushStack.pop());
    }
  }
  this.size--;
  return this.popStack.pop();
};

/**
 * Get the front element.
 * @return {number}
 */
MyQueue.prototype.peek = function () {
  if (this.popStack.length === 0) {
    let pushStackSize = this.pushStack.length;
    while (pushStackSize--) {
      this.popStack.push(this.pushStack.pop());
    }
  }
  return this.popStack[this.popStack.length - 1];
};

/**
 * Returns whether the queue is empty.
 * @return {boolean}
 */
MyQueue.prototype.empty = function () {
  return this.size === 0;
};
```