# 题目
给定一个无序的数组，找出数组在排序之后，相邻元素之间最大的差值。

如果数组元素个数小于 `2`，则返回 `0`。

说明:
+ 你可以假设数组中所有元素都是非负整数，且数值在 32 位有符号整数范围内。
+ 请尝试在线性时间复杂度和空间复杂度的条件下解决此问题。

# 示例
```
输入: [3,6,9,1]
输出: 3
解释: 排序后的数组是 [1,3,6,9], 其中相邻元素 (3,6) 和 (6,9) 之间都存在最大差值 3。
```

# 题解
## 基数排序
题目要求线性复杂度的情况下排序，而且排序的元素都是非负整数，在 `32` 位有符号整数范围内。

使用基数排序，先排序低16位，再排序高16位。最后计算最大差值。

```ts
function maximumGap(nums: number[]): number {
  // 将数组元素先低位排序，再高位排序
  const base = 2 ** 16;
  const count = new Array(base).fill(0);
  const temp = new Array(nums.length);

  // 低16位排序
  for (let num of nums) {
    count[num & 0xffff] += 1;
  }
  for (let i = 1; i < count.length; i++) {
    count[i] += count[i - 1];
  }
  for (let i = nums.length - 1; i >= 0; i--) {
    let num = nums[i];
    temp[--count[num & 0xffff]] = num;
  }

  // 重置count
  count.fill(0);

  // 高16位排序
  for (let num of nums) {
    count[(num & 0xffff0000) >> 16] += 1;
  }
  for (let i = 1; i < count.length; i++) {
    count[i] += count[i - 1];
  }
  for (let i = nums.length - 1; i >= 0; i--) {
    let num = temp[i];
    nums[--count[(num & 0xffff0000) >> 16]] = num;
  }

  // 计算结果
  let result = 0;
  for (let i = 1; i < nums.length; i++) {
    result = Math.max(result, nums[i] - nums[i - 1]);
  }
  return result;
}
```