# 题目

让我们一起来玩扫雷游戏！

给你一个大小为 m x n 二维字符矩阵 board ，表示扫雷游戏的盘面，其中：

- 'M' 代表一个 未挖出的 地雷，
- 'E' 代表一个 未挖出的 空方块，
- 'B' 代表没有相邻（上，下，左，右，和所有 4 个对角线）地雷的 已挖出的 空白方块，
- 数字（'1' 到 '8'）表示有多少地雷与这块 已挖出的 方块相邻，
- 'X' 则表示一个 已挖出的 地雷。

给你一个整数数组 click ，其中 click = [clickr, clickc] 表示在所有 未挖出的 方块（'M' 或者 'E'）中的下一个点击位置（clickr 是行下标，clickc 是列下标）。

根据以下规则，返回相应位置被点击后对应的盘面：

- 如果一个地雷（'M'）被挖出，游戏就结束了- 把它改为 'X' 。
- 如果一个 没有相邻地雷 的空方块（'E'）被挖出，修改它为（'B'），并且所有和其相邻的 未挖出 方块都应该被递归地揭露。
- 如果一个 至少与一个地雷相邻 的空方块（'E'）被挖出，修改它为数字（'1' 到 '8' ），表示相邻地雷的数量。

如果在此次点击中，若无更多方块可被揭露，则返回盘面。

提示：

- $m \equiv board.length$
- $n \equiv board[i].length$
- $1 \leq m, n \leq 50$
- `board[i][j]` 为 `'M'`、`'E'`、`'B'` 或数字 `'1'` 到 `'8'` 中的一个
- $click.length \equiv 2$
- $0 \leq clickr < m$
- $0 \leq clickc < n$
- `board[clickr][clickc]` 为 `'M'` 或 `'E'`

# 示例

[![briAeA.png](https://s1.ax1x.com/2022/03/06/briAeA.png)](https://imgtu.com/i/briAeA)

```ts
输入：board = [["E","E","E","E","E"],["E","E","M","E","E"],["E","E","E","E","E"],["E","E","E","E","E"]], click = [3,0]
输出：[["B","1","E","1","B"],["B","1","M","1","B"],["B","1","1","1","B"],["B","B","B","B","B"]]
```

# 题解

## 广度优先搜索

地图的连通性问题，可以用广度优先搜索来解决。

click 就是搜索的起点，按照 8 个方向将状态扩展下去：如果下一个是空方块，就修改状态，并加入队列。

如果当前搜索的方块附近有地雷的话，就修改状态为地雷的数量，跳出循环，继续下一个方块。(附近有地雷的方块无法被扩展)

```ts
interface Cell {
  x: number;
  y: number;
}

function updateBoard(board: string[][], click: number[]): string[][] {
  // 查找当前位置周围是否有雷，并返回数量
  const findMine = (x: number, y: number, dirs: number[][]): number => {
    let res = 0;
    for (const [dx, dy] of dirs) {
      const nx = x + dx;
      const ny = y + dy;
      if (nx < 0 || nx >= m || ny < 0 || ny >= n) continue;
      if (board[nx][ny] === 'M') res++;
    }
    return res;
  };
  // 如果是雷，修改状态后直接返回
  if (board[click[0]][click[1]] === 'M') {
    board[click[0]][click[1]] = 'X';
    return board;
  }

  const m = board.length;
  const n = board[0].length;
  const dirs = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, 1],
  ];
  const queue: Cell[] = [];
  // 初始化状态
  queue.push({ x: click[0], y: click[1] });
  board[click[0]][click[1]] = 'B';
  // 开始搜索
  while (queue.length) {
    const cur = queue.shift();
    const mines = findMine(cur.x, cur.y, dirs);
    if (mines > 0) {
      board[cur.x][cur.y] = String(mines);
      continue;
    }
    for (const [dx, dy] of dirs) {
      const x = cur.x + dx;
      const y = cur.y + dy;
      if (x < 0 || x >= m) continue;
      if (y < 0 || y >= n) continue;
      if (board[x][y] !== 'E') continue;
      board[x][y] = 'B';
      queue.push({ x, y });
    }
  }

  return board;
}
```

```cpp
class Solution
{
public:
    struct Cell
    {
        int x, y;
    };

    int dirs[8][2] = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

    int getMines(vector<vector<char>> &board, int x, int y)
    {
        int mines = 0;
        for (int i = 0; i < 8; i++)
        {
            int nx = x + dirs[i][0];
            int ny = y + dirs[i][1];
            if (nx >= 0 && nx < board.size() && ny >= 0 && ny < board[0].size())
            {
                if (board[nx][ny] == 'M')
                    mines++;
            }
        }
        return mines;
    }

    vector<vector<char>> updateBoard(vector<vector<char>> &board, vector<int> &click)
    {
        if (board[click[0]][click[1]] == 'M')
        {
            board[click[0]][click[1]] = 'X';
            return board;
        }
        int m = board.size(), n = board[0].size();
        queue<Cell> q;
        q.push({click[0], click[1]});
        board[click[0]][click[1]] = 'B';
        while (!q.empty())
        {
            Cell cur = q.front();
            q.pop();
            int mines = getMines(board, cur.x, cur.y);
            if (mines > 0)
            {
                board[cur.x][cur.y] = '0' + mines;
                continue;
            }
            for (auto [dx, dy] : dirs)
            {
                int x = cur.x + dx, y = cur.y + dy;
                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'E')
                {
                    q.push({x, y});
                    board[x][y] = 'B';
                }
            }
        }
        return board;
    }
};
```
