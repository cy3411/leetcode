/*
 * @lc app=leetcode.cn id=1518 lang=typescript
 *
 * [1518] 换酒问题
 */

// @lc code=start
function numWaterBottles(numBottles: number, numExchange: number): number {
  // 初始化
  let ans = numBottles;
  while (numBottles >= numExchange) {
    // 换酒
    const bottles = (numBottles / numExchange) >> 0;
    ans += bottles;
    // 换酒后的剩余
    numBottles = bottles + (numBottles % numExchange);
  }
  return ans;
}
// @lc code=end
