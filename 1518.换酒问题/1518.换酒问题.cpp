/*
 * @lc app=leetcode.cn id=1518 lang=cpp
 *
 * [1518] 换酒问题
 */

// @lc code=start
class Solution
{
public:
    int numWaterBottles(int numBottles, int numExchange)
    {
        int bottle = numBottles, ans = numBottles;
        while (bottle >= numExchange)
        {
            bottle -= numExchange;
            bottle++;
            ans++;
        }

        return ans;
    }
};
// @lc code=end
