# 题目

小区便利店正在促销，用 `numExchange` 个空酒瓶可以兑换一瓶新酒。你购入了 `numBottles` 瓶酒。

如果喝掉了酒瓶中的酒，那么酒瓶就会变成空的。

请你计算 **最多** 能喝到多少瓶酒。

提示：

- $\color{goldenrod}1 \leq numBottles \leq 100$
- $\color{goldenrod}2 \leq numExchange \leq 100$

# 示例

[![TFnNSP.png](https://s4.ax1x.com/2021/12/17/TFnNSP.png)](https://imgtu.com/i/TFnNSP)

```
输入：numBottles = 9, numExchange = 3
输出：13
解释：你可以用 3 个空酒瓶兑换 1 瓶酒。
所以最多能喝到 9 + 3 + 1 = 13 瓶酒。
```

[![TFnBwQ.md.png](https://s4.ax1x.com/2021/12/17/TFnBwQ.md.png)](https://imgtu.com/i/TFnBwQ)

```
输入：numBottles = 15, numExchange = 4
输出：19
解释：你可以用 4 个空酒瓶兑换 1 瓶酒。
所以最多能喝到 15 + 3 + 1 = 19 瓶酒。
```

# 题解

## 模拟

首先我们一定可以喝到 `numBottles` 瓶酒，因为我们有 `numBottles` 个空酒瓶。

接下来我们需要模拟 `numBottles` 个空酒瓶可以兑换$\color{goldenrod}\lfloor \frac{numBottles}{numExchange} \rfloor$瓶酒。

将结果累计到答案中，然后更新`numBottles`,直到不能兑换为止。

```ts
function numWaterBottles(numBottles: number, numExchange: number): number {
  // 初始化
  let ans = numBottles;
  while (numBottles >= numExchange) {
    // 换酒
    const bottles = (numBottles / numExchange) >> 0;
    ans += bottles;
    // 换酒后的剩余
    numBottles = bottles + (numBottles % numExchange);
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int numWaterBottles(int numBottles, int numExchange)
    {
        int bottle = numBottles, ans = numBottles;
        while (bottle >= numExchange)
        {
            bottle -= numExchange;
            bottle++;
            ans++;
        }

        return ans;
    }
};
```
