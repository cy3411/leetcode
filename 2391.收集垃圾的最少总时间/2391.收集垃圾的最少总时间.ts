/*
 * @lc app=leetcode.cn id=2391 lang=typescript
 *
 * [2391] 收集垃圾的最少总时间
 */

// @lc code=start
function garbageCollection(garbage: string[], travel: number[]): number {
  // 记录垃圾车到达每个垃圾最后位置的总时间
  const last = new Map<string, number>();
  // 前缀和，记录垃圾车到达每个垃圾的总时间
  let curDis = 0;
  let ans = 0;
  for (let i = 0; i < garbage.length; i++) {
    // 每个垃圾的处理时间
    ans += garbage[i].length;
    if (i > 0) {
      curDis += travel[i - 1];
    }
    for (const c of garbage[i]) {
      last.set(c, curDis);
    }
  }
  for (const [_, v] of last) {
    ans += v;
  }
  return ans;
}
// @lc code=end
