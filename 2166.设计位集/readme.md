# 题目

位集 `Bitset` 是一种能以紧凑形式存储位的数据结构。

请你实现 `Bitset` 类。

- `Bitset(int size)` 用 `size` 个位初始化 `Bitset` ，所有位都是 `0` 。
- `void fix(int idx)` 将下标为 `idx` 的位上的值更新为 `1` 。如果值已经是 `1` ，则不会发生任何改变。
- `void unfix(int idx)` 将下标为 `idx` 的位上的值更新为 `0` 。如果值已经是 `0` ，则不会发生任何改变。
- `void flip()` 翻转 `Bitset` 中每一位上的值。换句话说，所有值为 `0` 的位将会变成 `1` ，反之亦然。
- `boolean all()` 检查 `Bitset` 中 每一位 的值是否都是 `1` 。如果满足此条件，返回 `true` ；否则，返回 `false` 。
- `boolean one()` 检查 `Bitset` 中 **是否至少一位** 的值是 `1` 。如果满足此条件，返回 `true` ；否则，返回 `false` 。
- `int count()` 返回 `Bitset` 中值为 `1` 的位的 `总数` 。
- `String toString()` 返回 `Bitset` 的当前组成情况。注意，在结果字符串中，第 `i` 个下标处的字符应该与 `Bitset` 中的第 `i` 位一致。

提示：

- $1 <= size <= 10^5$
- $0 <= idx <= size - 1$
- 至多调用 `fix`、`unfix`、`flip`、`all`、`one`、`count` 和 `toString` 方法 总共 $10^5$ 次
- 至少调用 `all`、`one`、`count` 或 `toString` 方法一次
- 至多调用 `toString` 方法 5 次

# 示例

```
输入
["Bitset", "fix", "fix", "flip", "all", "unfix", "flip", "one", "unfix", "count", "toString"]
[[5], [3], [1], [], [], [0], [], [], [0], [], []]
输出
[null, null, null, null, false, null, null, true, null, 2, "01010"]

解释
Bitset bs = new Bitset(5); // bitset = "00000".
bs.fix(3);     // 将 idx = 3 处的值更新为 1 ，此时 bitset = "00010" 。
bs.fix(1);     // 将 idx = 1 处的值更新为 1 ，此时 bitset = "01010" 。
bs.flip();     // 翻转每一位上的值，此时 bitset = "10101" 。
bs.all();      // 返回 False ，bitset 中的值不全为 1 。
bs.unfix(0);   // 将 idx = 0 处的值更新为 0 ，此时 bitset = "00101" 。
bs.flip();     // 翻转每一位上的值，此时 bitset = "11010" 。
bs.one();      // 返回 True ，至少存在一位的值为 1 。
bs.unfix(0);   // 将 idx = 0 处的值更新为 0 ，此时 bitset = "01010" 。
bs.count();    // 返回 2 ，当前有 2 位的值为 1 。
bs.toString(); // 返回 "01010" ，即 bitset 的当前组成情况。
```

# 题解

## 位运算

分组保存，每组保存 30 个二进制位。

```ts
interface Bits {
  // 第几组
  x: number;
  // 第几位
  y: number;
}

class Bitset {
  // 基数，每组保存30个二进制位
  base: number = 30;
  // 1的个数
  ones: number = 0;
  data: number[];
  n: number;
  size: number;
  constructor(size: number) {
    this.size = size;
    // 可以分多少组
    this.n = Math.ceil(size / this.base);
    this.data = new Array(this.n).fill(0);
  }

  private _getBits(idx: number): Bits {
    return {
      x: Math.floor(idx / this.base),
      y: idx % this.base,
    };
  }

  fix(idx: number): void {
    const { x, y } = this._getBits(idx);
    if ((this.data[x] & (1 << y)) == 0) {
      this.ones++;
      this.data[x] |= 1 << y;
    }
  }

  unfix(idx: number): void {
    const { x, y } = this._getBits(idx);
    if ((this.data[x] & (1 << y)) != 0) {
      this.ones--;
      this.data[x] ^= 1 << y;
    }
  }

  flip(): void {
    for (let i = 0; i < this.n; i++) {
      this.data[i] = ~this.data[i];
    }
    this.ones = this.size - this.ones;
  }

  all(): boolean {
    return this.ones === this.size;
  }

  one(): boolean {
    return this.ones > 0;
  }

  count(): number {
    return this.ones;
  }

  toString(): string {
    let m = (this.size / this.base) >> 0;
    let rest = this.size % this.base;
    let str = '';
    for (let i = 0; i < m; i++) {
      for (let j = 0; j < this.base; j++) {
        str += this.data[i] & (1 << j) ? '1' : '0';
      }
    }
    for (let j = 0; j < rest; j++) {
      str += this.data[this.n - 1] & (1 << j) ? '1' : '0';
    }

    return str;
  }
}
```

```cpp
class Bitset
{
    typedef pair<int, int> PII;
    int size, base, ones, *data, n;

    PII _getBits(int idx)
    {
        int x = idx / base, y = idx % base;
        return {x, y};
    }

public:
    Bitset(int size) : size(size), base(30), ones(0)
    {
        // 向上取整
        n = size / base + (size % base != 0);
        data = new int[n];
        memset(data, 0, sizeof(int) * n);
    }

    void fix(int idx)
    {
        auto [x, y] = _getBits(idx);
        if ((data[x] & (1 << y)) == 0)
        {
            ones += 1;
            data[x] |= (1 << y);
        }
    }

    void unfix(int idx)
    {
        auto [x, y] = _getBits(idx);
        if ((data[x] & (1 << y)))
        {
            ones -= 1;
            data[x] ^= (1 << y);
        }
    }

    void flip()
    {
        for (int i = 0; i < n; i++)
        {
            data[i] = ~data[i];
        }
        ones = size - ones;
    }

    bool all()
    {
        return ones == size;
    }

    bool one()
    {
        return ones != 0;
    }

    int count()
    {
        return ones;
    }

    string toString()
    {
        int m = size / base, rest = size % base;
        string res = "";
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < base; j++)
            {
                if ((data[i] & (1 << j)))
                {
                    res += "1";
                }
                else
                {
                    res += "0";
                }
            }
        }
        for (int j = 0; j < rest; j++)
        {
            if ((data[m] & (1 << j)))
            {
                res += "1";
            }
            else
            {
                res += "0";
            }
        }

        return res;
    }
};
```
