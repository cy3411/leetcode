# 题目

给定一个正整数，检查它的二进制表示是否总是 0、1 交替出现：换句话说，就是二进制表示中相邻两位的数字永不相同。

提示：

$1 <= n <= 2^{31} - 1$

# 示例

```
输入：n = 5
输出：true
解释：5 的二进制表示是：101
```

```
输入：n = 7
输出：false
解释：7 的二进制表示是：111.
```

# 题解

## 位运算

记录前一位的值，枚举当前位的值，如果当前位和前一位相同，则返回 false，否则返回 true。

```ts
function hasAlternatingBits(n: number): boolean {
  // 记录前一位的值
  let prev = n & 1;
  n >>= 1;
  while (n) {
    const curr = n & 1;
    // 如果当前位和前一位相同，返回false
    if (prev === curr) return false;
    // 否则更新前一位的值
    prev = curr;
    n >>= 1;
  }
  return true;
}
```
