/*
 * @lc app=leetcode.cn id=693 lang=typescript
 *
 * [693] 交替位二进制数
 */

// @lc code=start
function hasAlternatingBits(n: number): boolean {
  // 记录前一位的值
  let prev = n & 1;
  n >>= 1;
  while (n) {
    const curr = n & 1;
    // 如果当前位和前一位相同，返回false
    if (prev === curr) return false;
    // 否则更新前一位的值
    prev = curr;
    n >>= 1;
  }
  return true;
}
// @lc code=end
