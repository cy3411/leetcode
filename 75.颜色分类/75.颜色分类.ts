/*
 * @lc app=leetcode.cn id=75 lang=typescript
 *
 * [75] 颜色分类
 */

// @lc code=start
/**
 Do not return anything, modify nums in-place instead.
 */
function sortColors(nums: number[]): void {
  let l = 0;
  let p = l;
  let r = nums.length - 1;
  let mid = 1;
  while (p <= r) {
    if (nums[p] === mid) {
      p++;
    } else if (nums[p] < mid) {
      [nums[l], nums[p]] = [nums[p], nums[l]];
      p++;
      l++;
    } else {
      [nums[p], nums[r]] = [nums[r], nums[p]];
      r--;
    }
  }
}
// @lc code=end
