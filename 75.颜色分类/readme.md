# 题目
给定一个包含红色、白色和蓝色，一共 `n` 个元素的数组，**原地**对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。

此题中，我们使用整数 `0`、 `1` 和 `2` 分别表示红色、白色和蓝色。

提示：
+ n == nums.length
+ 1 <= n <= 300
+ nums[i] 为 0、1 或 2

# 示例
```
输入：nums = [2,0,2,1,1,0]
输出：[0,0,1,1,2,2]
```

# 题解
## 三指针
题目给出了原地排序的限制，自然就是多指针的思路。

定义 `l=p=0` ,定义 `r=nums.length-1`：
+ nums[p]=1，p++
+ nums[p]<1,交换nums[l]和nums[p],l++,p++
+ nums[p]>1,交换num[p]和nums[r],r--

有点类似三路快排的思路。

```js
function sortColors(nums: number[]): void {
  let l = 0;
  let p = l;
  let r = nums.length - 1;
  let mid = 1;
  while (p <= r) {
    if (nums[p] === mid) {
      p++;
    } else if (nums[p] < mid) {
      [nums[l], nums[p]] = [nums[p], nums[l]];
      p++;
      l++;
    } else {
      [nums[p], nums[r]] = [nums[r], nums[p]];
      r--;
    }
  }
}
```
