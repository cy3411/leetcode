/*
 * @lc app=leetcode.cn id=450 lang=typescript
 *
 * [450] 删除二叉搜索树中的节点
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function getPrecursor(root: TreeNode): TreeNode {
  let node = root.left;
  while (node.right) node = node.right;
  return node;
}

function deleteNode(root: TreeNode | null, key: number): TreeNode | null {
  if (root === null) return root;
  if (key < root.val) {
    root.left = deleteNode(root.left, key);
  } else if (key > root.val) {
    root.right = deleteNode(root.right, key);
  } else {
    if (root.left === null || root.right === null) {
      // 删除度为1或0的节点
      const node: TreeNode | null = root.left ? root.left : root.right;
      return node;
    } else {
      // 删除度为2的节点
      const node = getPrecursor(root);
      // 用前驱节点替换要删除节点
      root.val = node.val;
      // 再删除当前节点的前驱节点
      root.left = deleteNode(root.left, node.val);
    }
  }
  return root;
}
// @lc code=end
