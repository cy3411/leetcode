# 题目

给定一个二叉搜索树的根节点 root 和一个值 key，删除二叉搜索树中的 key 对应的节点，并保证二叉搜索树的性质不变。返回二叉搜索树（有可能被更新）的根节点的引用。

一般来说，删除节点可分为两个步骤：

- 首先找到需要删除的节点；
- 如果找到了，删除它。

说明： 要求算法时间复杂度为 O(h)，h 为树的高度。

# 示例

```
root = [5,3,6,2,4,null,7]
key = 3

    5
   / \
  3   6
 / \   \
2   4   7

给定需要删除的节点值是 3，所以我们首先找到 3 这个节点，然后删除它。

一个正确的答案是 [5,4,6,2,null,null,7], 如下图所示。

    5
   / \
  4   6
 /     \
2       7

另一个正确答案是 [5,2,6,null,4,null,7]。

    5
   / \
  2   6
   \   \
    4   7
```

# 题解

## 二叉树操作

删除节点分三种情况，判断需要删除节点的度：

- 为 0，返回 null
- 为 1，返回非空节点
- 为 2，将需删除节点的前驱或者后继节点的覆盖需删除节点，这样将删除节点的情况转化为上面的情况，再删除前驱或者后继节点。

前驱节点就是当前节点前值最大的节点
后驱节点就是当前节点后值最小的节点

这样替换删除节点后，不影响二叉搜索树的特性。

```ts
function getPrecursor(root: TreeNode): TreeNode {
  let node = root.left;
  while (node.right) node = node.right;
  return node;
}

function deleteNode(root: TreeNode | null, key: number): TreeNode | null {
  if (root === null) return root;
  if (key < root.val) {
    root.left = deleteNode(root.left, key);
  } else if (key > root.val) {
    root.right = deleteNode(root.right, key);
  } else {
    // 删除度为1或0的节点
    if (root.left === null || root.right === null) {
      const node: TreeNode | null = root.left ? root.left : root.right;
      return node;
      // 删除度为2的节点
    } else {
      // 找到当前节点的前驱或者后继节点，这里找前驱
      const node = getPrecursor(root);
      // 用前驱节点替换要删除节点
      root.val = node.val;
      // 再删除当前节点的前驱节点
      root.left = deleteNode(root.left, node.val);
    }
  }
  return root;
}
```

```cpp
class Solution {
public:
    TreeNode *getPrecursor(TreeNode *root) {
        TreeNode *node = root->left;
        while (node->right != NULL) {
            node = node->right;
        }
        return node;
    }
    TreeNode *deleteNode(TreeNode *root, int key) {
        if (root == NULL) {
            return root;
        }
        if (key > root->val) {
            root->right = deleteNode(root->right, key);
        } else if (key < root->val) {
            root->left = deleteNode(root->left, key);
        } else {
            if (root->left == NULL || root->right == NULL) {
                TreeNode *temp = root->left ? root->left : root->right;
                return temp;
            } else {
                TreeNode *temp = getPrecursor(root);
                root->val = temp->val;
                root->left = deleteNode(root->left, temp->val);
            }
        }
        return root;
    }
};
```
