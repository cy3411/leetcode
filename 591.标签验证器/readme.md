# 题目

给定一个表示代码片段的字符串，你需要实现一个验证器来解析这段代码，并返回它是否合法。合法的代码片段需要遵守以下的所有规则：

- 代码必须被**合法的闭合标签**包围。否则，代码是无效的。
- 闭合标签（不一定合法）要严格符合格式：`<TAG_NAME>TAG_CONTENT</TAG_NAME>`。其中，`<TAG_NAME>`是起始标签，`</TAG_NAME>`是结束标签。起始和结束标签中的 `TAG_NAME` 应当相同。当且仅当 `TAG_NAME` 和 `TAG_CONTENT` 都是合法的，闭合标签才是合法的。
- 合法的 `TAG_NAME` 仅含有大写字母，长度在范围 `[1,9]` 之间。否则，该 `TAG_NAME` 是不合法的。
- 合法的 `TAG_CONTENT` 可以包含其他合法的闭合标签，`cdata` （请参考规则 7）和任意字符（注意参考规则 1）**除了**不匹配的`<`、不匹配的起始和结束标签、不匹配的或带有不合法 `TAG_NAME` 的闭合标签。否则，`TAG_CONTENT` 是不合法的。
- 一个起始标签，如果没有具有相同 `TAG_NAME` 的结束标签与之匹配，是不合法的。反之亦然。不过，你也需要考虑标签嵌套的问题。
- 一个`<`，如果你找不到一个后续的`>`与之匹配，是不合法的。并且当你找到一个`<`或`</`时，所有直到下一个`>`的前的字符，都应当被解析为 `TAG_NAME`（不一定合法）。
- `cdata` 有如下格式：`<![CDATA[CDATA_CONTENT]]>`。`CDATA_CONTENT` 的范围被定义成 `<![CDATA[ 和后续的第一个 ]]>`之间的字符。
- `CDATA_CONTENT` 可以包含任意字符。`cdata` 的功能是阻止验证器解析 `CDATA_CONTENT`，所以即使其中有一些字符可以被解析为标签（无论合法还是不合法），也应该将它们视为**常规字符**。

注意:

- 为简明起见，你可以假设输入的代码（包括提到的任意字符）只包含`数字`, `字母`, `'<'`,`'>'`,`'/'`,`'!'`,`'[',']'`和`' '`。

# 示例

合法代码的例子:

```
输入: "<DIV>This is the first line <![CDATA[<div>]]></DIV>"

输出: True

解释:

代码被包含在了闭合的标签内： <DIV> 和 </DIV> 。

TAG_NAME 是合法的，TAG_CONTENT 包含了一些字符和 cdata 。

即使 CDATA_CONTENT 含有不匹配的起始标签和不合法的 TAG_NAME，它应该被视为普通的文本，而不是标签。

所以 TAG_CONTENT 是合法的，因此代码是合法的。最终返回True。


输入: "<DIV>>>  ![cdata[]] <![CDATA[<div>]>]]>]]>>]</DIV>"

输出: True

解释:

我们首先将代码分割为： start_tag|tag_content|end_tag 。

start_tag -> "<DIV>"

end_tag -> "</DIV>"

tag_content 也可被分割为： text1|cdata|text2 。

text1 -> ">>  ![cdata[]] "

cdata -> "<![CDATA[<div>]>]]>" ，其中 CDATA_CONTENT 为 "<div>]>"

text2 -> "]]>>]"


start_tag 不是 "<DIV>>>" 的原因参照规则 6 。
cdata 不是 "<![CDATA[<div>]>]]>]]>" 的原因参照规则 7 。
```

不合法代码的例子:

```
输入: "<A>  <B> </A>   </B>"
输出: False
解释: 不合法。如果 "<A>" 是闭合的，那么 "<B>" 一定是不匹配的，反之亦然。

输入: "<DIV>  div tag is not closed  <DIV>"
输出: False

输入: "<DIV>  unmatched <  </DIV>"
输出: False

输入: "<DIV> closed tags with invalid tag name  <b>123</b> </DIV>"
输出: False

输入: "<DIV> unmatched tags with invalid tag name  </1234567890> and <CDATA[[]]>  </DIV>"
输出: False

输入: "<DIV>  unmatched start tag <B>  and unmatched end tag </C>  </DIV>"
输出: False
```

# 题解

## 栈

涉及到标签的闭合问题，因此我们考虑使用栈来存储开始标签，方便后面验证标签的闭合。

遍历字符串：

- 是开始标签，判断标签名称是否合法，如果是压入栈中，否则返回 False。
- 是结束标签，判断是否和栈顶标签匹配，如果匹配，则弹出栈顶元素，否则返回 False。栈顶元素弹出后，如果栈为空且当前字符串还有字符，也返回 False。(非嵌套标签也是不合法的)
- 是 cdata，判断结尾是否为 `"]]>"`，如果是，则继续，否则返回 False。
- 其他字符，继续。(需要判断一下栈中是否有标签包裹当前内容，如果没有，返回 False)

```ts
function isValid(code: string): boolean {
  const n = code.length;
  const stack = [];
  // 验证标签名是否合法
  const isValidateTagName = (tagname: string): boolean => {
    if (tagname.length < 1) return false;
    if (tagname.length > 9) return false;
    for (const c of tagname) {
      if (c < 'A' || c > 'Z') return false;
    }
    return true;
  };
  let i = 0;
  while (i < n) {
    if (code[i] === '<') {
      if (i === n - 1) return false;

      if (code[i + 1] === '/') {
        // 结束标签
        const j = code.indexOf('>', i);
        if (j < 0) return false;
        const tagName = code.slice(i + 2, j);
        // 栈为空，没有匹配的开始标签
        if (stack.length === 0) return false;
        // 开始标签和结束标签不匹配
        if (stack[stack.length - 1] !== tagName) return false;
        // 匹配成功，弹出栈顶元素
        stack.pop();
        i = j + 1;
        // 如果不是嵌套标签，也不是合法的标签
        if (stack.length === 0 && i !== n) return false;
      } else if (code[i + 1] === '!') {
        // CDATA
        if (stack.length === 0) return false;
        if (i + 9 > n) return false;
        const cdata = code.slice(i, i + 9);
        if (cdata !== '<![CDATA[') return false;
        const j = code.indexOf(']]>', i);
        if (j < 0) return false;
        i = j + 1;
      } else {
        // 开始标签
        const j = code.indexOf('>', i);
        if (j < 0) return false;
        const tagName = code.slice(i + 1, j);
        // 判断标签名是否合法
        if (!isValidateTagName(tagName)) return false;
        // 将合法开始标签压入栈中
        stack.push(tagName);
        i = j + 1;
      }
    } else {
      // 最少需要一个标签包裹
      if (stack.length === 0) return false;
      i++;
    }
  }
  return stack.length === 0;
}
```

```cpp
class Solution
{
public:
    bool isValidTagName(string tagName)
    {
        int n = tagName.size();
        if (n < 1 || n > 9)
            return false;
        for (int i = 0; i < n; i++)
        {
            if (tagName[i] < 'A' || tagName[i] > 'Z')
                return false;
        }
        return true;
    }
    bool isValid(string code)
    {
        int n = code.size(), i = 0;
        stack<string> st;
        while (i < n)
        {
            if (code[i] == '<')
            {
                if (i + 1 == n)
                    return false;
                if (code[i + 1] == '/')
                {
                    if (st.empty())
                        return false;
                    string tagName = "";
                    i += 2;
                    while (i < n && code[i] != '>')
                    {
                        tagName += code[i];
                        i++;
                    }
                    if (tagName != st.top())
                        return false;
                    st.pop();
                    i++;
                    if (st.empty() && i != n)
                        return false;
                }
                else if (code[i + 1] == '!')
                {
                    if (st.empty())
                        return false;
                    string cdata = code.substr(i, 9);
                    if (cdata != "<![CDATA[")
                        return false;
                    int j = code.find("]]>", i);
                    if (j == string::npos)
                        return false;
                    i = j + 1;
                }
                else
                {
                    string tagName;
                    i++;
                    while (i < n && code[i] != '>')
                    {
                        tagName += code[i];
                        i++;
                    }
                    if (!isValidTagName(tagName))
                        return false;
                    st.push(tagName);
                    i++;
                }
            }
            else
            {
                if (st.empty())
                    return false;
                i++;
            }
        }
        return st.empty();
    }
};
```
