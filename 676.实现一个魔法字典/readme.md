# 题目

设计一个使用单词列表进行初始化的数据结构，单词列表中的单词 互不相同 。 如果给出一个单词，请判定能否只将这个单词中一个字母换成另一个字母，使得所形成的新单词存在于你构建的字典中。

实现 `MagicDictionary` 类：

- `MagicDictionary()` 初始化对象
- `void buildDict(String[] dictionary)` 使用字符串数组 `dictionary` `设定该数据结构，dictionary` 中的字符串互不相同
- `bool search(String searchWord)` 给定一个字符串 `searchWord` ，判定能否只将字符串中 **一个** 字母换成另一个字母，使得所形成的新字符串能够与字典中的任一字符串匹配。如果可以，返回 true ；否则，返回 false 。

提示：

- 1 <= dictionary.length <= 100
- 1 <= dictionary[i].length <= 100
- dictionary[i] 仅由小写英文字母组成
- dictionary 中的所有字符串 互不相同
- 1 <= searchWord.length <= 100
- searchWord 仅由小写英文字母组成
- buildDict 仅在 search 之前调用一次
- 最多调用 100 次 search

# 示例

```
输入
["MagicDictionary", "buildDict", "search", "search", "search", "search"]
[[], [["hello", "leetcode"]], ["hello"], ["hhllo"], ["hell"], ["leetcoded"]]
输出
[null, null, false, true, false, false]

解释
MagicDictionary magicDictionary = new MagicDictionary();
magicDictionary.buildDict(["hello", "leetcode"]);
magicDictionary.search("hello"); // 返回 False
magicDictionary.search("hhllo"); // 将第二个 'h' 替换为 'e' 可以匹配 "hello" ，所以返回 True
magicDictionary.search("hell"); // 返回 False
magicDictionary.search("leetcoded"); // 返回 False
```

# 题解

## Tire Tree

使用 tire 树来初始化单词列表。

search 的时候递归处理 searchWord，保证 n 个字符不同，且树中有单词的时候返回 true。

```ts
class TireNode676 {
  public children: Map<string, TireNode676>;
  public isWord: boolean;
  constructor() {
    this.children = new Map();
    this.isWord = false;
  }
}

class Tire676 {
  private root: TireNode676;
  constructor() {
    this.root = new TireNode676();
  }

  insert(word: string) {
    let node = this.root;
    for (let char of word) {
      if (!node.children.has(char)) {
        node.children.set(char, new TireNode676());
      }
      node = node.children.get(char);
    }
    node.isWord = true;
  }
  private _search(word: string, pos: number, n: number, node: TireNode676): boolean {
    if (pos === word.length) {
      return node.isWord && n === 0;
    }
    // 当前位置存在字符
    if (
      node.children.has(word[pos]) &&
      this._search(word, pos + 1, n, node.children.get(word[pos]))
    ) {
      return true;
    }
    // 当前位置不存在字符，则全部查找
    if (n > 0) {
      const keys = node.children.keys();
      for (let key of keys) {
        if (word[pos] === key) continue;
        if (this._search(word, pos + 1, n - 1, node.children.get(key))) return true;
      }
    }
    return false;
  }
  search(word: string, n: number): boolean {
    return this._search(word, 0, n, this.root);
  }
}
class MagicDictionary {
  public tire: Tire676;
  constructor() {
    this.tire = new Tire676();
  }

  buildDict(dictionary: string[]): void {
    for (let word of dictionary) {
      this.tire.insert(word);
    }
  }

  search(searchWord: string): boolean {
    return this.tire.search(searchWord, 1);
  }
}
```
