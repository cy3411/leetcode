/*
 * @lc app=leetcode.cn id=812 lang=typescript
 *
 * [812] 最大三角形面积
 */

// @lc code=start
function largestTriangleArea(points: number[][]): number {
  const n = points.length;

  let ans = 0;
  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      for (let k = j + 1; k < n; k++) {
        const x1 = points[i][0];
        const y1 = points[i][1];
        const x2 = points[j][0];
        const y2 = points[j][1];
        const x3 = points[k][0];
        const y3 = points[k][1];
        // 行列式公式
        const area = Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) * 0.5;
        ans = Math.max(ans, area);
      }
    }
  }

  return ans;
}
// @lc code=end
