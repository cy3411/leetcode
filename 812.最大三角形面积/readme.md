# 题目

给定包含多个点的集合，从其中取三个点组成三角形，返回能组成的最大三角形的面积。

注意:

- $3 \leq points.length \leq 50$
- 不存在重复的点。
- $-50 \leq points[i][j] \leq 50$
- 结果误差值在 $10^-6$ 以内都认为是正确答案。

# 示例

```
示例:
输入: points = [[0,0],[0,1],[1,0],[0,2],[2,0]]
输出: 2
解释:
这五个点如下图所示。组成的橙色三角形是最大的，面积为2。
```

# 题解

## 枚举

枚举所有的三角形，然后返回最大的面积。

面积的计算公式的行列式：

$$
    area = \frac{1}{2} \lVert \begin{matrix}
        x1 & y1 & 1 \\
        x2 & y2 & 1 \\
        x3 & y3 & 1 \\
    \end{matrix} \rVert
        = \frac{1}{2}\lvert(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))\rvert
$$

```ts
function largestTriangleArea(points: number[][]): number {
  const n = points.length;

  let ans = 0;
  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      for (let k = j + 1; k < n; k++) {
        const x1 = points[i][0];
        const y1 = points[i][1];
        const x2 = points[j][0];
        const y2 = points[j][1];
        const x3 = points[k][0];
        const y3 = points[k][1];
        // 行列式公式
        const area = Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) * 0.5;
        ans = Math.max(ans, area);
      }
    }
  }

  return ans;
}
```
