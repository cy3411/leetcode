/*
 * @lc app=leetcode.cn id=129 lang=javascript
 *
 * [129] 求根到叶子节点数字之和
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumNumbers = function (root) {
  if (root === null) {
    return 0;
  }

  let sum = 0;
  const nodeQue = [root];
  const numQue = [root.val];

  while (nodeQue.length) {
    let node = nodeQue.shift();
    let num = numQue.shift();
    let left = node.left;
    let right = node.right;

    if (left === null && right === null) {
      sum += num;
    } else {
      if (left !== null) {
        nodeQue.push(left);
        numQue.push(num * 10 + left.val);
      }
      if (right !== null) {
        nodeQue.push(right);
        numQue.push(num * 10 + right.val);
      }
    }
  }

  return sum;
};
// @lc code=end

/**
 * 时间复杂度：O(n)，其中 n 是二叉树的节点个数。对每个节点访问一次
 * 空间复杂度：O(n)，其中 n 是二叉树的节点个数。空间复杂度主要取决于队列，每个队列中的元素个数不会超过 n
 */
