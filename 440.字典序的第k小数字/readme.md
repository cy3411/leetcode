# 题目

给定整数 `n` 和 `k`，返回 `[1, n]` 中字典序第 `k` 小的数字。

提示:

- $1 \leq k \leq n \leq 10^9$

# 示例

```
输入: n = 13, k = 2
输出: 10
解释: 字典序的排列是 [1, 10, 11, 12, 13, 2, 3, 4, 5, 6, 7, 8, 9]，所以第二小的数字是 10。
```

```
输入: n = 1, k = 1
输出: 1
```

# 题解

## 字典树

```ts
function findKthNumber(n: number, k: number): number {
  // 当前数字所能表示的范围
  const getSteps = (num: number, n: number): number => {
    let steps = 0;
    let lastNum = num;
    let firstNum = num;
    while (firstNum <= n) {
      // 所能表示的范围，最大不能超过n
      steps += Math.min(lastNum, n) - firstNum + 1;
      firstNum *= 10;
      lastNum = lastNum * 10 + 9;
    }
    return steps;
  };

  let ans = 1;
  k--;
  while (k > 0) {
    const steps = getSteps(ans, n);
    if (steps <= k) {
      k -= steps;
      ans++;
    } else {
      ans *= 10;
      k--;
    }
  }

  return ans;
}
```
