/*
 * @lc app=leetcode.cn id=2110 lang=cpp
 *
 * [2110] 股票平滑下跌阶段的数目
 */

// @lc code=start
class Solution
{
public:
    long long getDescentPeriods(vector<int> &prices)
    {
        long long fi = 0, pre = 0, ans = 0;
        for (int p : prices)
        {
            if (p + 1 == pre)
            {
                fi = fi + 1;
            }
            else
            {
                fi = 1;
            }
            ans += fi;
            pre = p;
        }
        return ans;
    }
};
// @lc code=end
