# 题目

给你一个整数数组 `prices` ，表示一支股票的历史每日股价，其中 `prices[i]` 是这支股票第 `i` 天的价格。

一个 **平滑下降的阶段** 定义为：对于 **连续一天或者多天** ，每日股价都比 **前一日股价恰好少** `1` ，这个阶段第一天的股价没有限制。

请你返回 **平滑下降阶段** 的数目。

提示：

- $\color{burlywood}1 \leq prices.length \leq 10^5$
- $\color{burlywood}1 \leq prices[i] \leq 10^5$

# 示例

```
输入：prices = [3,2,1,4]
输出：7
解释：总共有 7 个平滑下降阶段：
[3], [2], [1], [4], [3,2], [2,1] 和 [3,2,1]
注意，仅一天按照定义也是平滑下降阶段。
```

```
输入：prices = [8,6,7,7]
输出：4
解释：总共有 4 个连续平滑下降阶段：[8], [6], [7] 和 [7]
由于 8 - 6 ≠ 1 ，所以 [8,6] 不是平滑下降阶段。
```

```
输入：prices = [1]
输出：1
解释：总共有 1 个平滑下降阶段：[1]
```

# 题解

## 递推

定义 `f(i)` 为从开始到当前 i 位置的平滑下降阶段数目，则有

$$
    f(i) = \begin{cases}
        f(i-1) + 1, & 当前位置可以继续下降； \\
        1, & 当前位置不能继续下降；
    \end{cases}
$$

状态的转移只和前一个位置有关系，所以可以用使用一个变量来保存前一个位置的状态。遍历的过程的中持续更新就可以了。

```ts
function getDescentPeriods(prices: number[]): number {
  let fi = 0;
  let pre = 0;
  let ans = 0;

  for (const p of prices) {
    // 当前价格可以和前一个价格形成平滑下降阶段
    if (p + 1 === pre) {
      fi = fi + 1;
    } else {
      fi = 1;
    }
    ans += fi;
    pre = p;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    long long getDescentPeriods(vector<int> &prices)
    {
        long long fi = 0, pre = 0, ans = 0;
        for (int p : prices)
        {
            if (p + 1 == pre)
            {
                fi = fi + 1;
            }
            else
            {
                fi = 1;
            }
            ans += fi;
            pre = p;
        }
        return ans;
    }
};
```
