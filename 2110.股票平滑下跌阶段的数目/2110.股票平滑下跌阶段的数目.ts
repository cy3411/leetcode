/*
 * @lc app=leetcode.cn id=2110 lang=typescript
 *
 * [2110] 股票平滑下跌阶段的数目
 */

// @lc code=start
function getDescentPeriods(prices: number[]): number {
  let fi = 0;
  let pre = 0;
  let ans = 0;

  for (const p of prices) {
    // 当前价格可以和前一个价格形成平滑下降阶段
    if (p + 1 === pre) {
      fi = fi + 1;
    } else {
      fi = 1;
    }
    ans += fi;
    pre = p;
  }

  return ans;
}
// @lc code=end
