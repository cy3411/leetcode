/*
 * @lc app=leetcode.cn id=777 lang=typescript
 *
 * [777] 在LR字符串中交换相邻字符
 */

// @lc code=start
function canTransform(start: string, end: string): boolean {
  const n = start.length;
  let i = 0;
  let j = 0;
  while (i < n && j < n) {
    // 找到非X字符的下标
    while (i < n && start[i] === 'X') {
      i++;
    }
    while (j < n && end[j] === 'X') {
      j++;
    }

    if (i < n && j < n) {
      // 是否一致
      if (start[i] !== end[j]) return false;
      // 下标位置是否正确
      if ((start[i] === 'L' && i < j) || (start[i] === 'R' && i > j)) return false;
      i++, j++;
    }
  }

  // 如果对比结束，还有剩余，只能是 X
  while (i < n) {
    if (start[i] !== 'X') return false;
    i++;
  }

  while (j < n) {
    if (end[j] !== 'X') return false;
    j++;
  }

  return true;
}
// @lc code=end
