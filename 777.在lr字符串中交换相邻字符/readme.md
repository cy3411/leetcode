# 题目

在一个由 'L' , 'R' 和 'X' 三个字符组成的字符串（例如"RXXLRXRXL"）中进行移动操作。

一次移动操作指用一个"LX"替换一个"XL"，或者用一个"XR"替换一个"RX"。

现给定起始字符串 start 和结束字符串 end，请编写代码，当且仅当存在一系列移动操作使得 start 可以转换成 end 时， 返回 True。

提示：

- $1 \leq len(start) \equiv len(end) \leq 10000。$
- start 和 end 中的字符串仅限于'L', 'R'和'X'。

# 示例

```
输入: start = "RXXLRXRXL", end = "XRLXXRRLX"
输出: True
解释:
我们可以通过以下几步将start转换成end:
RXXLRXRXL ->
XRXLRXRXL ->
XRLXRXRXL ->
XRLXXRRXL ->
XRLXXRRLX
```

# 题解

## 双指针

根据题意可以看出:

- 'LX' 中 L 是向右边移动得到 'XL'，那么 end 中 L 的索引一定比 start 大。
- 'XR' 中 R 是想左边移动得到 'RX'，那么 end 中 R 的索引一定比 start 小。

使用双指针分别指向 start 和 end 中第一个非 'X' 的字符，按照如上规则比较，如果不符合，直接返回 false 。否则，移动指针到下一个非 'X' 字符，继续上述操作。

最后比较完毕，如果某个字符串还有剩余，还需要判断是否只剩下 'X'。

```js
function canTransform(start: string, end: string): boolean {
  const n = start.length;
  let i = 0;
  let j = 0;
  while (i < n && j < n) {
    // 找到非X字符的下标
    while (i < n && start[i] === 'X') {
      i++;
    }
    while (j < n && end[j] === 'X') {
      j++;
    }

    if (i < n && j < n) {
      // 是否一致
      if (start[i] !== end[j]) return false;
      // 下标位置是否正确
      if ((start[i] === 'L' && i < j) || (start[i] === 'R' && i > j)) return false;
      i++, j++;
    }
  }

  // 如果对比结束，还有剩余，只能是 X
  while (i < n) {
    if (start[i] !== 'X') return false;
    i++;
  }

  while (j < n) {
    if (end[j] !== 'X') return false;
    j++;
  }

  return true;
}
```
