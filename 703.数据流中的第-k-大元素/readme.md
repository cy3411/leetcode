# 题目
设计一个找到数据流中第 `k` 大元素的类（class）。注意是排序后的第 `k` 大元素，不是第 `k` 个不同的元素。

请实现 `KthLargest` 类：

`KthLargest(int k, int[] nums)` 使用整数 `k` 和整数流 `nums` 初始化对象。
`int add(int val)` 将 `val` 插入数据流 `nums` 后，返回当前数据流中第 `k` 大的元素。

提示：
+ 1 <= k <= 104
+ 0 <= nums.length <= 104
+ -104 <= nums[i] <= 104
+ -104 <= val <= 104
+ 最多调用 add 方法 104 次
+ 题目数据保证，在查找第 k 大元素时，数组中至少有 k 个元素

# 示例
```
输入：
["KthLargest", "add", "add", "add", "add", "add"]
[[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
输出：
[null, 4, 5, 5, 8, 8]

解释：
KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
kthLargest.add(3);   // return 4
kthLargest.add(5);   // return 5
kthLargest.add(10);  // return 5
kthLargest.add(9);   // return 8
kthLargest.add(4);   // return 8
```

# 题解

堆的代码可以查看：[ 1046.最后一块石头的重量 ](https://gitee.com/cy3411/leecode/tree/master/1046.%E6%9C%80%E5%90%8E%E4%B8%80%E5%9D%97%E7%9F%B3%E5%A4%B4%E7%9A%84%E9%87%8D%E9%87%8F)

维护一个小顶堆，堆的大小大于 `k` 的时候，把堆顶弹出，堆顶就是第 `k` 大的值。

```js
/**
 * @param {number} k
 * @param {number[]} nums
 */
var KthLargest = function (k, nums) {
  this.minPQ = new MinPQ(nums);
  this.k = k;
};

/**
 * @param {number} val
 * @return {number}
 */
KthLargest.prototype.add = function (val) {
  this.minPQ.insert(val);
  while (this.minPQ.size() > this.k) {
    this.minPQ.poll();
  }
  return this.minPQ.peek();
};
```