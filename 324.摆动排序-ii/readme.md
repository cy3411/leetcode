# 题目

给你一个整数数组 `nums`，将它重新排列成 `nums[0] < nums[1] > nums[2] < nums[3]...` 的顺序。

你可以假设所有输入数组都可以得到满足题目要求的结果。

提示：

- $1 \leq nums.length \leq 5 * 10^4$
- $0 \leq nums[i] \leq 5000$
- 题目数据保证，对于给定的输入 `nums` ，总能产生满足题目要求的结果

# 示例

```
输入：nums = [1,5,1,1,6,4]
输出：[1,6,1,5,1,4]
解释：[1,4,1,5,1,6] 同样是符合题目要求的结果，可以被判题程序接受。
```

```
输入：nums = [1,3,2,2,3,1]
输出：[2,3,1,3,1,2]
```

# 题解

## 排序+双指针

将 nums 按照升序排序，定义两个指针 `j` 和 `k`，分别指向 `nums` 的中间和结尾。

遍历 nums， 更新 i 元素为 j， i + 1 元素为 k。

```ts
function wiggleSort(nums: number[]): void {
  const arr = [...nums];
  arr.sort((a, b) => a - b);
  const n = arr.length;
  for (let i = 0, j = Math.floor((n + 1) / 2) - 1, k = n - 1; i < n; i += 2, j--, k--) {
    nums[i] = arr[j];
    if (i + 1 < n) {
      nums[i + 1] = arr[k];
    }
  }
}
```
