# 题目

Alice 有 `n` 枚糖，其中第 `i` 枚糖的类型为 `candyType[i]` 。Alice 注意到她的体重正在增长，所以前去拜访了一位医生。

医生建议 Alice 要少摄入糖分，只吃掉她所有糖的 `n / 2` 即可（n 是一个偶数）。Alice 非常喜欢这些糖，她想要在遵循医生建议的情况下，尽可能吃到最多不同种类的糖。

给你一个长度为 `n` 的整数数组 `candyType` ，返回： Alice 在仅吃掉 `n / 2` 枚糖的情况下，可以吃到糖的最多种类数。

提示：

- `n == candyType.length`
- $2 \leq n \leq 10^4$
- `n` 是一个偶数
- $-10^5 \leq candyType[i] \leq 10^5$

# 示例

```
输入：candyType = [1,1,2,2,3,3]
输出：3
解释：Alice 只能吃 6 / 2 = 3 枚糖，由于只有 3 种糖，她可以每种吃一枚。
```

```
输入：candyType = [1,1,2,3]
输出：2
解释：Alice 只能吃 4 / 2 = 2 枚糖，不管她选择吃的种类是 [1,2]、[1,3] 还是 [2,3]，她只能吃到两种不同类的糖。
```

# 题解

## 哈希表

使用哈希表记录糖果的种类数，最后取糖果种类和一半糖果数量的最小值就是答案。

```ts
function distributeCandies(candyType: number[]): number {
  const hash = new Map();
  // 统计糖果种类
  for (const candy of candyType) {
    if (hash.has(candy)) {
      hash.set(candy, hash.get(candy) + 1);
    } else {
      hash.set(candy, 1);
    }
  }
  // 返回一半糖果数量和糖果种类中的最小值
  return Math.min(hash.size, candyType.length / 2);
}
```
