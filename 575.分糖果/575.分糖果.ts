/*
 * @lc app=leetcode.cn id=575 lang=typescript
 *
 * [575] 分糖果
 */

// @lc code=start
function distributeCandies(candyType: number[]): number {
  const hash = new Map();
  // 统计糖果种类
  for (const candy of candyType) {
    if (hash.has(candy)) {
      hash.set(candy, hash.get(candy) + 1);
    } else {
      hash.set(candy, 1);
    }
  }
  // 返回一半糖果数量和糖果种类中的最小值
  return Math.min(hash.size, candyType.length / 2);
}
// @lc code=end
