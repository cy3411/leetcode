/*
 * @lc app=leetcode.cn id=150 lang=javascript
 *
 * [150] 逆波兰表达式求值
 */

// @lc code=start
/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function (tokens) {
  const calc = (stack, token) => {
    let b = Number(stack.pop());
    let a = Number(stack.pop());
    switch (token) {
      case '+':
        stack.push(a + b);
        break;
      case '-':
        stack.push(a - b);
        break;
      case '*':
        stack.push(a * b);
        break;
      case '/':
        stack.push((a / b) >> 0);
        break;
    }
  };
  const stack = [];
  for (let token of tokens) {
    switch (token) {
      case '+':
        calc(stack, token);
        break;
      case '-':
        calc(stack, token);
        break;

      case '*':
        calc(stack, token);
        break;

      case '/':
        calc(stack, token);
        break;
      default:
        stack.push(token);
        break;
    }
  }
  return stack[0];
};
// @lc code=end
