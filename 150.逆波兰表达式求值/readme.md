# 题目
根据 逆波兰表示法，求表达式的值。

有效的算符包括 +、-、*、/ 。每个运算对象可以是整数，也可以是另一个逆波兰表达式。

说明：

+ 整数除法只保留整数部分。
+ 给定逆波兰表达式总是有效的。换句话说，表达式总会得出有效数值且不存在除数为 0 的情况。

提示：

+ 1 <= tokens.length <= 104
+ tokens[i] 要么是一个算符（"+"、"-"、"*" 或 "/"），要么是一个在范围 [-200, 200] 内的整数

逆波兰表达式：

逆波兰表达式是一种后缀表达式，所谓后缀就是指算符写在后面。

+ 平常使用的算式则是一种中缀表达式，如 ( 1 + 2 ) * ( 3 + 4 ) 。
+ 该算式的逆波兰表达式写法为 ( ( 1 2 + ) ( 3 4 + ) * ) 。


逆波兰表达式主要有以下两个优点：

+ 去掉括号后表达式无歧义，上式即便写成 1 2 + 3 4 + * 也可以依据次序计算出正确结果。
+ 适合用栈操作运算：遇到数字则入栈；遇到算符则取出栈顶两个数字进行计算，并将结果压入栈中。


# 示例
```
输入：tokens = ["2","1","+","3","*"]
输出：9
解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9
```

# 方法
把操作数压入栈，遇到操作符执行计算，把结果再次压入栈就可以了。
```js
/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function (tokens) {
  const calc = (stack, token) => {
    let b = Number(stack.pop());
    let a = Number(stack.pop());
    switch (token) {
      case '+':
        stack.push(a + b);
        break;
      case '-':
        stack.push(a - b);
        break;
      case '*':
        stack.push(a * b);
        break;
      case '/':
        stack.push((a / b) >> 0);
        break;
    }
  };
  const stack = [];
  for (let token of tokens) {
    switch (token) {
      case '+':
        calc(stack, token);
        break;
      case '-':
        calc(stack, token);
        break;
      case '*':
        calc(stack, token);
        break;

      case '/':
        calc(stack, token);
        break;
      default:
        stack.push(token);
        break;
    }
  }
  return stack[0];
};
```