# 题目

给你一个下标从 `0` 开始的数组 `nums` ，该数组由 `n` 个正整数组成。

如果满足下述条件，则数组 `nums` 是一个 **交替数组** ：

- $nums[i - 2] \equiv nums[i]$ ，其中 $2 \leq i \leq n - 1$ 。
- $nums[i - 1] \not = nums[i]$ ，其中 $1 \leq i \leq n - 1$ 。

在一步 **操作** 中，你可以选择下标 `i` 并将 `nums[i]` **更改** 为 **任一** 正整数。

返回使数组变成交替数组的 **最少操作数** 。

提示：

- $1 \leq nums.length \leq 10^5$
- $1 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [3,1,3,2,4,3]
输出：3
解释：
使数组变成交替数组的方法之一是将该数组转换为 [3,1,3,1,3,1] 。
在这种情况下，操作数为 3 。
可以证明，操作数少于 3 的情况下，无法使数组变成交替数组。
```

```
输入：nums = [1,2,2,2,2]
输出：2
解释：
使数组变成交替数组的方法之一是将该数组转换为 [1,2,1,2,1].
在这种情况下，操作数为 2 。
注意，数组不能转换成 [2,2,2,2,2] 。因为在这种情况下，nums[0] == nums[1]，不满足交替数组的条件。
```

# 题解

## 计数

定义 `n0` 和 `n1` 分别为数组的偶数位和奇数位的个数

统计偶数位和奇数位出现次数最多的数字的数量分别为 `x` 和 `y`，那么最少的操作数就是 $(n0-x)+(n1-y)$

由于题意要求 x 和 y 的值不能相同，所以还要分别统计次多出现的数字数量 `x'` 和 `y'`。当 $x \equiv y$ 时，取 `(x', y)` 和 `(x, y')` 中较小的操作数。

```ts
interface NumStatus {
  num: number;
  count: number;
}

function minimumOperations(nums: number[]): number {
  const n = nums.length;
  if (n === 1) return 0;
  // 计算偶数位和奇数位最多和次多出现的数字和次数
  const getMaxNum = (p: number, z1: NumStatus, z2: NumStatus) => {
    const hash = new Map<number, number>();
    for (let i = p; i < n; i += 2) {
      hash.set(nums[i], (hash.get(nums[i]) ?? 0) + 1);
    }
    for (const [num, count] of hash) {
      if (count > z1.count) {
        z2.num = z1.num;
        z2.count = z1.count;
        z1.num = num;
        z1.count = count;
      } else if (count > z2.count) {
        z2.num = num;
        z2.count = count;
      }
    }
  };

  // 出现最多次数的偶数位的值和数量
  const x1: NumStatus = { num: 0, count: 0 };
  // 出现次多次数的偶数位的值和数量
  const x2: NumStatus = { num: 0, count: 0 };
  // 出现最多次数的奇数位的值和数量
  const y1: NumStatus = { num: 0, count: 0 };
  // 出现次多次数的奇数位的值和数量
  const y2: NumStatus = { num: 0, count: 0 };

  getMaxNum(0, x1, x2);
  getMaxNum(1, y1, y2);
  // 共有多少个偶数位
  let n0 = ((n + 1) / 2) | 0;
  // 共有多少个奇数位
  let n1 = n - n0;

  // (偶数位 - 偶数为出现最多次的数字的次数) + (奇数位 - 奇数为出现最多次的数字的次数)
  if (x1.num !== y1.num) return n0 - x1.count + (n1 - y1.count);
  // 如果偶数位和奇数位出现最多次的数字相同，那么就在出现次多的组合中取最小的变化次数
  return Math.min(n0 - x1.count + (n1 - y2.count), n0 - x2.count + (n1 - y1.count));
}
```

```cpp
class Solution
{
public:
    typedef pair<int, int> PII;
    void getMaxNum(vector<int> &nums, int p, PII &z1, PII &z2)
    {
        unordered_map<int, int> m;
        int n = nums.size();
        for (int i = p; i < n; i += 2)
        {
            m[nums[i]]++;
        }
        for (auto &x : m)
        {
            if (x.second > z1.second)
            {
                z2 = z1;
                z1 = x;
            }
            else if (x.second > z2.second)
            {
                z2 = x;
            }
        }
    }
    int minimumOperations(vector<int> &nums)
    {
        int n = nums.size();
        if (n == 1)
            return 0;

        PII x1 = {0, 0}, x2 = {0, 0}, y1 = {0, 0}, y2 = {0, 0};
        getMaxNum(nums, 0, x1, x2);
        getMaxNum(nums, 1, y1, y2);

        int n0 = (n + 1) / 2, n1 = n - n0;
        if (x1.first != y1.first)
        {
            return (n0 - x1.second) + (n1 - y1.second);
        }

        return min(
            (n0 - x1.second) + (n1 - y2.second),
            (n0 - x2.second) + (n1 - y1.second));
    }
};
```
