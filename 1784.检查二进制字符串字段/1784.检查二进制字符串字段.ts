/*
 * @lc app=leetcode.cn id=1784 lang=typescript
 *
 * [1784] 检查二进制字符串字段
 */

// @lc code=start
function checkOnesSegment(s: string): boolean {
  return s.indexOf('01') === -1;
}
// @lc code=end
