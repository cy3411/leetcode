/*
 * @lc app=leetcode.cn id=1784 lang=cpp
 *
 * [1784] 检查二进制字符串字段
 */

// @lc code=start
class Solution {
public:
    bool checkOnesSegment(string s) {
        // 标记 0 是否出现过
        int isFlag = 0;
        for (auto x : s) {
            if (x == '0') isFlag = true;
            if (x == '1' && isFlag) return false;
        }
        return true;
    }
};
// @lc code=end
