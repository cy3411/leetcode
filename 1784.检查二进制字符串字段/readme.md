# 题目

给你一个二进制字符串 `s` ，该字符串 **不含前导零** 。

如果 `s` 包含 零个或一个由连续的 `'1'` 组成的字段 ，返回 `true​​​` 。否则，返回 `false` 。

如果 `s` 中 由连续若干个 `'1'` 组成的字段 数量不超过 `1`，返回 `true​​​` 。否则，返回 `false` 。

提示：

- 1 <= s.length <= 100
- s[i]​​​​ 为 '0' 或 '1'
- s[0] 为 '1'

# 示例

```
输入：s = "1001"
输出：false
解释：由连续若干个 '1' 组成的字段数量为 2，返回 false
```

```
输入：s = "110"
输出：true
```

# 题解

## 寻找‘01’串

根据题意给出的条件可知：

- 二进制串全部为 0
- 二进制串只能包含一个连续 1 组成的串，由于没有前导零，只能表示为 '1...000'

所以我们只需要判断字符串中是否含有'01'字串即可。

```js
function checkOnesSegment(s: string): boolean {
  return s.indexOf('01') === -1;
}
```

```cpp
class Solution {
public:
    bool checkOnesSegment(string s) {
        // 标记 0 是否出现过
        int isFlag = 0;
        for (auto x : s) {
            if (x == '0') isFlag = true;
            if (x == '1' && isFlag) return false;
        }
        return true;
    }
};
```
