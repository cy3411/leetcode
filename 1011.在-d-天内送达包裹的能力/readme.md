# 题目
传送带上的包裹必须在 `D` 天内从一个港口运送到另一个港口。

传送带上的第 `i` 个包裹的重量为 `weights[i]`。每一天，我们都会按给出重量的顺序往传送带上装载包裹。我们装载的重量不会超过船的最大运载重量。

返回能在 `D` 天内将传送带上的所有包裹送达的船的**最低运载能力**。

提示：

+ 1 <= D <= weights.length <= 50000
+ 1 <= weights[i] <= 500

# 示例
```
输入：weights = [1,2,3,4,5,6,7,8,9,10], D = 5
输出：15
解释：
船舶最低载重 15 就能够在 5 天内送达所有包裹，如下所示：
第 1 天：1, 2, 3, 4, 5
第 2 天：6, 7
第 3 天：8
第 4 天：9
第 5 天：10

请注意，货物必须按照给定的顺序装运，因此使用载重能力为 14 的船舶并将包装分成 (2, 3, 4, 5), (1, 6, 7), (8), (9), (10) 是不允许的。 
```
# 题解
## 二分查找
假设运载能力为`x`，使得包裹总重量除于`x`大于等于D，这个`x`就是答案。

船舶最低载重肯定是大于等于包裹里的最大重量，因为包裹不能拆分。

船舶最低载重并且小于等于所有的包裹的总重量。

这样我们就是在一个数值范围区间内求结果。`[包裹里的最大重量, 所有的包裹的总重量]`

这显然是一个升序的结果。

```js
function shipWithinDays(weights: number[], D: number): number {
  let left = Math.max(...weights);
  let right = weights.reduce((prev, curr) => prev + curr);

  while (left < right) {
    const mid = ((right - left) >> 1) + left;
    let needDays = 1;
    let count = 0;
    // 计算当前x需要几天可以运输完毕
    for (let w of weights) {
      if (count + w > mid) {
        needDays++;
        count = 0;
      }
      count += w;
    }

    if (needDays <= D) {
      right = mid;
    } else {
      left = mid + 1;
    }
  }

  return left;
}
```
