# 题目

根据 百度百科 ，生命游戏，简称为生命，是英国数学家约翰·何顿·康威在 1970 年发明的细胞自动机。

给定一个包含 m × n 个格子的面板，每一个格子都可以看成是一个细胞。每个细胞都具有一个初始状态：1 即为活细胞（live），或 0 即为死细胞（dead）。每个细胞与其八个相邻位置（水平，垂直，对角线）的细胞都遵循以下四条生存定律：

- 如果活细胞周围八个位置的活细胞数少于两个，则该位置活细胞死亡；
- 如果活细胞周围八个位置有两个或三个活细胞，则该位置活细胞仍然存活；
- 如果活细胞周围八个位置有超过三个活细胞，则该位置活细胞死亡；
- 如果死细胞周围正好有三个活细胞，则该位置死细胞复活；

下一个状态是通过将上述规则同时应用于当前状态下的每个细胞所形成的，其中细胞的出生和死亡是同时发生的。给你 `m x n` 网格面板 `board` 的当前状态，返回下一个状态。

提示：

- `m == board.length`
- `n == board[i].length`
- `1 <= m, n <= 25`
- `board[i][j]` 为 `0` 或 `1`

# 示例

[![TF3yHf.png](https://s4.ax1x.com/2021/12/17/TF3yHf.png)](https://imgtu.com/i/TF3yHf)

```
输入：board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
输出：[[0,0,0],[1,0,1],[0,1,1],[0,1,0]]
```

# 题解

## 模拟

遍历每个细胞，记录每个细胞周围的活细胞数，然后根据生存规则进行更新。

- 如果活细胞周围八个位置有两个或三个活细胞，则该位置活细胞仍然存活,否则死亡
- 如果死细胞周围正好有三个活细胞，则该位置死细胞复活,否则死亡

```ts
function gameOfLife(board: number[][]): void {
  const n = board.length;
  const m = board[0].length;
  const directions = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1],
  ];
  const cnt = new Array(n).fill(0).map(() => new Array(m).fill(0));

  // 计算每个点周围的活细胞数
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      let count = 0;
      for (const [x, y] of directions) {
        const newX = i + x;
        const newY = j + y;
        if (newX < 0 || newX >= n) continue;
        if (newY < 0 || newY >= m) continue;
        if (board[newX][newY] !== 0) count++;
      }
      cnt[i][j] = count;
    }
  }
  // 根据活细胞数重新设置细胞状态
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      if (board[i][j]) {
        board[i][j] = cnt[i][j] < 2 || cnt[i][j] > 3 ? 0 : 1;
      } else {
        board[i][j] = cnt[i][j] === 3 ? 1 : 0;
      }
    }
  }
}
```
