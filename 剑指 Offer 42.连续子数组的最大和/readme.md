# 题目

输入一个整型数组，数组中的一个或连续多个整数组成一个子数组。求所有子数组的和的最大值。

要求时间复杂度为 O(n)。

提示：

- 1 <= arr.length <= 10^5
- -100 <= arr[i] <= 100

# 示例

```
输入: nums = [-2,1,-3,4,-1,2,1,-5,4]
输出: 6
解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。
```

# 题解

## 前缀和

区间和，自然就用到了前缀和。

遍历 nums，计算前缀和的时候，同时并记录最小的前缀和的值。

每次用新的前缀和值减去最小的前缀和值，取里面最大的结果就是答案。

```js
var maxSubArray = function (nums) {
  const m = nums.length;
  // 前缀和的值
  let sum = 0;
  // 最小的前缀和
  let min = 0;
  let ans = Number.NEGATIVE_INFINITY;
  for (let i = 0; i < m; i++) {
    sum += nums[i];
    ans = Math.max(ans, sum - min);
    if (sum < min) min = sum;
  }
  return ans;
};
```
