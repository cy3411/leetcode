// @test([-2,1,-3,4,-1,2,1,-5,4])=6
// @algorithm @lc id=100304 lang=javascript
// @title lian-xu-zi-shu-zu-de-zui-da-he-lcof
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function (nums) {
  const m = nums.length;
  // 前缀和的值
  let sum = 0;
  // 最小的前缀和
  let min = 0;
  let ans = Number.NEGATIVE_INFINITY;
  for (let i = 0; i < m; i++) {
    sum += nums[i];
    ans = Math.max(ans, sum - min);
    if (sum < min) min = sum;
  }
  return ans;
};
