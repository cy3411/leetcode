/*
 * @lc app=leetcode.cn id=119 lang=typescript
 *
 * [119] 杨辉三角 II
 */

// @lc code=start
function getRow(rowIndex: number): number[] {
  const m = rowIndex + 1;
  // 滚动数组，默认值为1
  const f = new Array(2).fill(0).map((_) => new Array(m).fill(1));
  for (let i = 1; i < m; i++) {
    let currIdx = i % 2;
    let preIdx = Number(!currIdx);
    // 计算每个位置之和
    for (let j = 1; j <= i; j++) {
      f[currIdx][j] = f[preIdx][j] + f[preIdx][j - 1];
    }
  }

  return f[m % 2];
}
// @lc code=end
