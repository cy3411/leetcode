# 题目

给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。

在「杨辉三角」中，每个数是它左上方和右上方的数的和。

[![hT3SnP.gif](https://z3.ax1x.com/2021/09/07/hT3SnP.gif)](https://imgtu.com/i/hT3SnP)

提示:

- `0 <= rowIndex <= 33`

进阶：

你可以优化你的算法到 O(rowIndex) 空间复杂度吗？

# 示例

```
输入: rowIndex = 3
输出: [1,3,3,1]
```

# 题解

## 递推

**状态**

定义 `f(x)` 为 `x` 行的杨辉三角。

**转移**

根据题意可知，每行中的元素都是上一行的部分元素的和组成。

$$f[x][j] = f[x-1][j]+f[x-1][j-11]$$

由于状态转移只依赖于前一个状态，我们使用滚动数组来优化。

```ts
function getRow(rowIndex: number): number[] {
  const m = rowIndex + 1;
  // 滚动数组，初始化为1
  const f = new Array(2).fill(0).map((_) => new Array(m).fill(1));
  for (let i = 1; i < m; i++) {
    let currIdx = i % 2;
    let preIdx = Number(!currIdx);
    // 计算每个位置之和
    for (let j = 1; j <= i; j++) {
      f[currIdx][j] = f[preIdx][j] + f[preIdx][j - 1];
    }
  }

  return f[m % 2];
}
```
