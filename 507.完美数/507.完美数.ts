/*
 * @lc app=leetcode.cn id=507 lang=typescript
 *
 * [507] 完美数
 */

// @lc code=start
function checkPerfectNumber(num: number): boolean {
  if (num <= 1) return false;

  let sum = 1;
  for (let i = 2; i * i <= num; i++) {
    // 如果可以整除，就将两个正因子加入到 sum 中
    if (num % i === 0) {
      sum += i + num / i;
    }
  }

  return sum === num;
}
// @lc code=end
