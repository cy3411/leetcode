# 题目

对于一个 正整数，如果它和除了它自身以外的所有 **正因子** 之和相等，我们称它为 「完美数」。

给定一个 整数 `n` ， 如果是完美数，返回 `true` ，否则返回 `false`

提示：

- $\color{burlywood}1 \leq num \leq 10^8$

# 示例

```
输入：num = 28
输出：true
解释：28 = 1 + 2 + 4 + 7 + 14
1, 2, 4, 7, 和 14 是 28 的所有正因子。
```

```
输入：num = 6
输出：true
```

# 题解

## 枚举

枚举[2, num/2]，如果可以和 num 整除，则将因子累加，最后判断累加之和是否等于 num

```ts
function checkPerfectNumber(num: number): boolean {
  if (num <= 1) return false;

  let sum = 1;
  for (let i = 2; i * i <= num; i++) {
    // 如果可以整除，就将两个正因子加入到 sum 中
    if (num % i === 0) {
      sum += i + num / i;
    }
  }

  return sum === num;
}
```

```cpp
class Solution
{
public:
    bool checkPerfectNumber(int num)
    {
        if (num <= 1)
        {

            return false;
        }

        int sum = 1;
        for (int i = 2; i * i <= num; i++)
        {
            if (num % i == 0)
            {
                sum += i + num / i;
            }
        }

        return sum == num;
    }
};
```
