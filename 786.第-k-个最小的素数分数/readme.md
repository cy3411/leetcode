# 题目

给你一个按递增顺序排序的数组 `arr` 和一个整数 `k` 。数组 `arr` 由 `1` 和若干 素数 组成，且其中所有整数互不相同。

对于每对满足 `0 < i < j < arr.length` 的 `i` 和 `j` ，可以得到分数 `arr[i] / arr[j]` 。

那么第 `k` 个最小的分数是多少呢? 以长度为 `2` 的整数数组返回你的答案, 这里 `answer[0] == arr[i]` 且 `answer[1] == arr[j]` 。

提示：

- $2 \leq arr.length \leq 1000$
- $1 \leq arr[i] \leq 3 * 10^4$
- $arr[0] == 1$
- `arr[i]` 是一个 素数 ，`i > 0`
- `arr` 中的所有数字 **互不相同** ，且按 **严格递增** 排序
- $1 \leq k \leq arr.length * (arr.length - 1) / 2$

# 示例

```
输入：arr = [1,2,3,5], k = 3
输出：[2,5]
解释：已构造好的分数,排序后如下所示:
1/5, 1/3, 2/5, 1/2, 3/5, 2/3
很明显第三个最小的分数是 2/5
```

# 题解

## 自定义排序

将所有的分数放入数组，自定义数组的 sort 排序。

当比较两个分数 $\frac{a}{b}$和$\frac{c}{d}$,会产生浮点计算，这会影响效率和精度。所以转换成乘法来比较：$a*d<b*c$。

```ts
function kthSmallestPrimeFraction(arr: number[], k: number): number[] {
  const frac = [];
  const n = arr.length;

  // 初始化所有的分数
  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      frac.push([arr[i], arr[j]]);
    }
  }

  // 自定义比较函数
  // 由于除法运算会产生浮点，所所这里转换成乘法运算
  frac.sort((a, b) => a[0] * b[1] - a[1] * b[0]);
  // 返回第 k 个最小的分数
  return frac[k - 1];
}
```
