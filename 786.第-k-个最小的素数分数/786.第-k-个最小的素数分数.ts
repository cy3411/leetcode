/*
 * @lc app=leetcode.cn id=786 lang=typescript
 *
 * [786] 第 K 个最小的素数分数
 */

// @lc code=start
function kthSmallestPrimeFraction(arr: number[], k: number): number[] {
  const frac = [];
  const n = arr.length;

  // 初始化所有的分数
  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      frac.push([arr[i], arr[j]]);
    }
  }

  // 自定义比较函数
  // 由于除法运算会产生浮点，所所这里转换成乘法运算
  frac.sort((a, b) => a[0] * b[1] - a[1] * b[0]);

  return frac[k - 1];
}
// @lc code=end
