/*
 * @lc app=leetcode.cn id=448 lang=javascript
 *
 * [448] 找到所有数组中消失的数字
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findDisappearedNumbers = function (nums) {
  const size = nums.length;
  for (const [_, n] of nums.entries()) {
    const x = (n - 1) % size;
    nums[x] += size;
  }
  const result = [];
  for (const [i, n] of nums.entries()) {
    if (n <= size) {
      result.push(i + 1);
    }
  }

  return result;
};
// @lc code=end
