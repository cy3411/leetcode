/*
 * @lc app=leetcode.cn id=1143 lang=javascript
 *
 * [1143] 最长公共子序列
 */

// @lc code=start
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  let size1 = text1.length;
  let size2 = text2.length;
  // dp数组,dp[i][j]表示text1[0,i]和text2[0,j]的最长公共子序列
  const dp = new Array(size1 + 1).fill(0).map((_) => new Array(size2 + 1).fill(0));

  for (let i = 1; i <= size1; i++) {
    for (let j = 1; j <= size2; j++) {
      if (text1[i - 1] === text2[j - 1]) {
        dp[i][j] = dp[i - 1][j - 1] + 1;
      } else {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }

  return dp[size1][size2];
};
// @lc code=end
