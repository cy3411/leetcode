# 题目
给定两个字符串 text1 和 text2，返回这两个字符串的最长公共子序列的长度。

一个字符串的 子序列 是指这样一个新的字符串：它是由原字符串在不改变字符的相对顺序的情况下删除某些字符（也可以不删除任何字符）后组成的新字符串。
例如，"ace" 是 "abcde" 的子序列，但 "aec" 不是 "abcde" 的子序列。两个字符串的「公共子序列」是这两个字符串所共同拥有的子序列。

若这两个字符串没有公共子序列，则返回 0。

提示:

+ `1 <= text1.length <= 1000`
+ `1 <= text2.length <= 1000`
+ 输入的字符串只含有小写英文字符。

# 示例
```
输入：text1 = "abcde", text2 = "ace" 
输出：3  
解释：最长公共子序列是 "ace"，它的长度为 3。
```

```
输入：text1 = "abc", text2 = "abc"
输出：3
解释：最长公共子序列是 "abc"，它的长度为 3。
```

# 方法
## 递归
自顶向下，逐个字符匹配：
+ 字符相同，继续匹配减去匹配字符后text1和减去匹配字符后text2
+ 字符不同，下面的选择取较大值
  + 继续匹配减去匹配字符后的text1和text2
  + 继续匹配text1和减去匹配字符后text2

```js
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  // text1[size1]和text2[size2]是否匹配
  const getSequence = (text1, size1, text2, size2) => {
    if (size1 < 0 || size2 < 0) return 0;

    if (text1[size1] === text2[size2]) {
      // 去掉匹配字母的下一组字符串匹配结果+1
      return getSequence(text1, size1 - 1, text2, size2 - 1) + 1;
    } else {
      // texe1去掉匹配结果和text2继续比较匹配
      // texe2去掉匹配结果和text1继续比较匹配
      // 取最大值，因为要获取最长公共子序列
      return Math.max(
        getSequence(text1, size1 - 1, text2, size2),
        getSequence(text1, size1, text2, size2 - 1)
      );
    }
  };
  let size1 = text1.length;
  let size2 = text2.length;

  return getSequence(text1, size1 - 1, text2, size2 - 1);
};
```

## 动态规划

明确base case: 

i=0或者j=0的时候字符串为空，最长公共子序列长度为0

明确状态：

这里字符串都是固定的，唯一会变化的状态就是最长公共子序列长度

明确选择，当i>0&&j>0时候，考虑dp[i][j]的计算：

+ `text1[i-1]等于text2[j-1]`，考虑之前的长度，在加上当前相同的公共字符，就可以得到新的长度。`dp[i][j] = dp[i-1][j-1]+1`
+ 当`text1[i-1]不等于text2[j-1]`，考虑下面2项:
  
  + text1[0,i-1]和text2[0,j]的最长公共子序列长
  + text2[0,i]和text2[0,j-1]的最长公共子序列长

    要得到text1[0,i]和text2[0,j]的最长公共子序列长，应取上面两项的最大值。

    `dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1])`


```js
/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */
var longestCommonSubsequence = function (text1, text2) {
  let size1 = text1.length;
  let size2 = text2.length;
  // dp数组,dp[i][j]表示text1[0,i]和text2[0,j]的最长公共子序列
  const dp = new Array(size1 + 1).fill(0).map((_) => new Array(size2 + 1).fill(0));

  for (let i = 1; i <= size1; i++) {
    for (let j = 1; j <= size2; j++) {
        // 比较i-1和j-1是因为dp数组本身要多一位做base case
      if (text1[i - 1] === text2[j - 1]) {
        dp[i][j] = dp[i - 1][j - 1] + 1;
      } else {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }

  return dp[size1][size2];
};
```
+ 时间复杂度O(m*n)
+ 空间复杂度O(m*n)