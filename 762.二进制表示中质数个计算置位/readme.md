# 题目

给你两个整数 `left` 和 `right` ，在闭区间 `[left, right]` 范围内，统计并返回 **计算置位位数为质数** 的整数个数。

**计算置位位数** 就是二进制表示中 `1` 的个数。

- 例如， `21` 的二进制表示 `10101` 有 `3` 个计算置位。

提示：

- $1 \leq left \leq right \leq 10^6$
- $0 \leq right - left \leq 10^4$

# 示例

```
输入：left = 6, right = 10
输出：4
解释：
6 -> 110 (2 个计算置位，2 是质数)
7 -> 111 (3 个计算置位，3 是质数)
9 -> 1001 (2 个计算置位，2 是质数)
10-> 1010 (2 个计算置位，2 是质数)
共计 4 个计算置位为质数的数字。
```

```
输入：left = 10, right = 15
输出：5
解释：
10 -> 1010 (2 个计算置位, 2 是质数)
11 -> 1011 (3 个计算置位, 3 是质数)
12 -> 1100 (2 个计算置位, 2 是质数)
13 -> 1101 (3 个计算置位, 3 是质数)
14 -> 1110 (3 个计算置位, 3 是质数)
15 -> 1111 (4 个计算置位, 4 不是质数)
共计 5 个计算置位为质数的数字。
```

# 题解

## 位运算

遍历 `left` 到 `right` 之间的所有数字，对每个数字 `n` 做如下操作：

- 计算 `n` 二进制中 `1` 的个数
- 判断 `1` 的个数是否为质数

```ts
function countPrimeSetBits(left: number, right: number): number {
  // 二进制位1的个数
  const getOneCount = (n: number) => {
    let res = 0;
    while (n > 0) {
      res++;
      n &= n - 1;
    }
    return res;
  };
  // 是否是质数
  const isPrime = (n: number) => {
    if (n < 2) return false;
    for (let i = 2; i * i <= n; i++) {
      if (n % i === 0) return false;
    }
    return true;
  };

  let ans = 0;
  for (let i = left; i <= right; i++) {
    if (isPrime(getOneCount(i))) ans++;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int getOnes(int n)
    {
        int res = 0;
        while (n)
        {
            n &= (n - 1);
            res++;
        }
        return res;
    }
    bool isPrime(int n)
    {
        if (n < 2)
        {
            return false;
        }
        for (int i = 2; i * i <= n; i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }
    int countPrimeSetBits(int left, int right)
    {
        int ans = 0;
        for (int i = left; i <= right; i++)
        {
            if (isPrime(getOnes(i)))
            {
                ans++;
            }
        }
        return ans;
    }
};
```
