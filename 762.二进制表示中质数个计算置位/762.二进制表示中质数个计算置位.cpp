/*
 * @lc app=leetcode.cn id=762 lang=cpp
 *
 * [762] 二进制表示中质数个计算置位
 */

// @lc code=start
class Solution
{
public:
    int getOnes(int n)
    {
        int res = 0;
        while (n)
        {
            n &= (n - 1);
            res++;
        }
        return res;
    }
    bool isPrime(int n)
    {
        if (n < 2)
        {
            return false;
        }
        for (int i = 2; i * i <= n; i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }
    int countPrimeSetBits(int left, int right)
    {
        int ans = 0;
        for (int i = left; i <= right; i++)
        {
            if (isPrime(getOnes(i)))
            {
                ans++;
            }
        }
        return ans;
    }
};
// @lc code=end
