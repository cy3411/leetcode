/*
 * @lc app=leetcode.cn id=762 lang=typescript
 *
 * [762] 二进制表示中质数个计算置位
 */

// @lc code=start
function countPrimeSetBits(left: number, right: number): number {
  // 二进制位1的个数
  const getOneCount = (n: number) => {
    let res = 0;
    while (n > 0) {
      res++;
      n &= n - 1;
    }
    return res;
  };
  // 是否是质数
  const isPrime = (n: number) => {
    if (n < 2) return false;
    for (let i = 2; i * i <= n; i++) {
      if (n % i === 0) return false;
    }
    return true;
  };

  let ans = 0;
  for (let i = left; i <= right; i++) {
    if (isPrime(getOneCount(i))) ans++;
  }

  return ans;
}
// @lc code=end
