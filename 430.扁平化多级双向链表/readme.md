# 题目

多级双向链表中，除了指向下一个节点和前一个节点指针之外，它还有一个子链表指针，可能指向单独的双向链表。这些子列表也可能会有一个或多个自己的子项，依此类推，生成多级数据结构，如下面的示例所示。

给你位于列表第一级的头节点，请你扁平化列表，使所有结点出现在单级双链表中。

# 示例

```
输入：head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
输出：[1,2,3,7,8,11,12,9,10,4,5,6]
```

输入的多级列表如下图所示：

[![2qo4aT.md.png](https://z3.ax1x.com/2021/06/15/2qo4aT.md.png)](https://imgtu.com/i/2qo4aT)

扁平化后的链表如下图：

[![2qo5IU.md.png](https://z3.ax1x.com/2021/06/15/2qo5IU.md.png)](https://imgtu.com/i/2qo5IU)

# 题解

## 链表

标准链表的插入操作，需要注意的就是处理完 child 中的链表，需要将之清空。

```ts
function flatten(head: Node | null): Node | null {
  if (head === null) return null;
  let p = head;
  while (p) {
    if (p.child !== null) {
      let q = p.child;
      // 找到子链表的最后一个节点
      while (q.next) {
        q = q.next;
      }
      q.next = p.next;
      //判断p是否是最后一个节点
      p.next && (p.next.prev = q);
      p.child.prev = p;
      p.next = p.child;
      p.child = null;
    }

    p = p.next;
  }

  return head;
}
```
