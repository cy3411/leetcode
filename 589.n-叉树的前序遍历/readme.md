# 题目

给定一个 `N` 叉树，返回其节点值的 **前序遍历** 。

`N` 叉树 在输入中按层序遍历进行序列化表示，每组子节点由空值 `null` 分隔（请参见示例）。

进阶：

递归法很简单，你可以使用迭代法完成此题吗?

提示：

- `N` 叉树的高度小于或等于 `1000`
- 节点总数在范围 $[0, 10^4]$ 内

# 示例

```
输入：root = [1,null,3,2,4,null,5,6]
输出：[1,3,5,6,2,4]
```

```
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：[1,2,3,6,7,11,14,4,8,12,5,9,13,10]
```

# 方法

## 递归

先将当前节点加入答案，递归访问孩子节点。

```js
/**
 * @param {Node} root
 * @return {number[]}
 */
var preorder = function (root) {
  const _preorder = (root, result) => {
    if (root === null) return;
    result.push(root.val);
    for (const child of root.children) {
      _preorder(child, result);
    }
  };

  const result = [];
  _preorder(root, result);
  return result;
};
```

```ts
function preorder(root: Node | null): number[] {
  const ans = [];
  const dfs = (node: Node | null) => {
    if (node === null) return;
    ans.push(node.val);
    for (const child of node.children) {
      dfs(child);
    }
  };
  dfs(root);
  return ans;
}
```

## 迭代

用栈模拟，将栈顶节点加入答案后，将 children 逆序入栈。

```js
/**
 * @param {Node} root
 * @return {number[]}
 */
var preorder = function (root) {
  const result = [];
  if (root === null) return result;

  const stack = [root];

  while (stack.length) {
    let node = stack.pop();
    result.push(node.val);
    // 将children逆序入栈，保证出栈的顺序是前序的
    for (let i = node.children.length - 1; i >= 0; i--) {
      stack.push(node.children[i]);
    }
  }

  return result;
};
```

```cpp
class Solution
{
public:
    vector<int> preorder(Node *root)
    {
        vector<int> ans;
        if (root == NULL)
        {
            return ans;
        }
        stack<Node *> stk;
        stk.push(root);
        while (!stk.empty())
        {
            Node *curr = stk.top();
            stk.pop();
            ans.push_back(curr->val);
            for (int i = curr->children.size() - 1; i >= 0; i--)
            {
                stk.push(curr->children[i]);
            }
        }
        return ans;
    }
};
```
