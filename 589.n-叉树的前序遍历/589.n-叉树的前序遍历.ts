/*
 * @lc app=leetcode.cn id=589 lang=typescript
 *
 * [589] N 叉树的前序遍历
 */

// @lc code=start
/**
 * Definition for node.
 * class Node {
 *     val: number
 *     children: Node[]
 *     constructor(val?: number) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.children = []
 *     }
 * }
 */

function preorder(root: Node | null): number[] {
  const ans = [];
  const dfs = (node: Node | null) => {
    if (node === null) return;
    ans.push(node.val);
    for (const child of node.children) {
      dfs(child);
    }
  };
  dfs(root);
  return ans;
}
// @lc code=end
