# 题目

如果序列 $X_1, X_2, ..., X_n$ 满足下列条件，就说它是 **斐波那契式** 的：

- $n \geq 3$
- 对于所有 $i + 2 \leq n$，都有 $X_i + X_{i+1} = X_{i+2}$

给定一个严格递增的正整数数组形成序列 `arr` ，找到 `arr` 中最长的斐波那契式的子序列的长度。如果一个不存在，返回 `0` 。

（回想一下，子序列是从原序列 `arr` 中派生出来的，它从 `arr` 中删掉任意数量的元素（也可以不删），而不改变其余元素的顺序。例如， `[3, 5, 8]` 是 `[3, 4, 5, 6, 7, 8]` 的一个子序列）

提示：

- $3 \leq arr.length \leq 1000$
- $1 \leq arr[i] < arr[i + 1] \leq 10^9$

# 示例

```
输入: arr = [1,2,3,4,5,6,7,8]
输出: 5
解释: 最长的斐波那契式子序列为 [1,2,3,5,8] 。
```

```
输入: arr = [1,3,7,11,12,14,18]
输出: 3
解释: 最长的斐波那契式子序列有 [1,11,12]、[3,11,14] 以及 [7,11,18] 。
```

# 题解

## 动态规划

定义 dp[i][j] 表示以 arr[i], arr[j] (i < j)为结尾的斐波那契式子序列的最大长度。

只要在 i 之前找到一个 k 位，同时满足 $arr[k] + arr[i] = arr[j]$。

那么状态转移方程为：

$$
    dp[i][j] = max(dp[i][j], dp[k][i] + 1)
$$

为了方便找到 k 位置的元素，可以使用哈希表，其中 key 为 arr[i]，value 为 i 位置。

```ts
function lenLongestFibSubseq(arr: number[]): number {
  const n = arr.length;
  const idxMap = new Map<number, number>();
  // 创建值与索引的映射
  for (let i = 0; i < n; i++) {
    idxMap.set(arr[i], i);
  }

  // 创建dp数组
  // 初始化长度为2的子序列
  const dp = new Array(n).fill(0).map(() => new Array(n).fill(2));

  let ans = 0;
  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      const diff = arr[j] - arr[i];
      if (idxMap.has(diff)) {
        const idx: number = idxMap.get(diff)!;
        if (idx < i) {
          dp[i][j] = Math.max(dp[i][j], dp[idx][i] + 1);
        }
      }
      ans = Math.max(ans, dp[i][j]);
    }
  }

  return ans > 2 ? ans : 0;
}
```
