# 题目

有两种特殊字符：

- 第一种字符可以用一个比特 `0` 来表示
- 第二种字符可以用两个比特(`10` 或 `11`)来表示

给定一个以 `0` 结尾的二进制数组 `bits` ，如果最后一个字符必须是一位字符，则返回 `true` 。

提示:

- $1 \leq bits.length \leq 1000$
- `bits[i] == 0 or 1`

# 示例

```
输入: bits = [1, 0, 0]
输出: true
解释: 唯一的编码方式是一个两比特字符和一个一比特字符。
所以最后一个字符是一比特字符。
```

```
输入: bits = [1, 1, 1, 0]
输出: false
解释: 唯一的编码方式是两比特字符和两比特字符。
所以最后一个字符不是一比特字符。
```

# 题解

## 正序遍历

根据题意，第一种字符是 `0` 开头， 第二种字符是 `1` 开头。

遍历 `bits`， 如果 $bits[i] \equiv 0$，将 i 的值加 `1`，否则将 `i` 的值加 `2`。

最后判断 `i` 的值是否在最后一位。

```ts
function isOneBitCharacter(bits: number[]): boolean {
  const n = bits.length;
  let i = 0;
  // 逢1加2，逢0加1
  while (i < n - 1) {
    if (bits[i] === 1) i += 2;
    else i++;
  }

  return i === n - 1;
}
```

```cpp
bool isOneBitCharacter(int *bits, int bitsSize)
{
    int i = 0;
    while (i < bitsSize - 1)
    {
        if (bits[i] == 1)
        {
            i += 2;
        }
        else
        {
            i++;
        }
    }
    return i == bitsSize - 1;
}
```

## 倒序遍历

根据题意，`0` 一定是一个字符的结尾。

我们可以找到 `bits` 倒数第 `2` 个 `0` 的位置，记作 `i` 。那么 `bits[i + 1]` 一定是一个字符的开头，且 `bit[i + 1]` 到 `bit [n -2]` 这 `n - 2 - i` 个比特位都是 `1`。

- 如果 `n - 2 - i` 为偶数，则最后一个 0 一定是第一种字符。
- 如果 `n - 2 - i` 为奇数，则最后一个 0 一定是第二种字符。

```cpp
class Solution
{
public:
    bool isOneBitCharacter(vector<int> &bits)
    {
        int n = bits.size();
        int i = n - 2;
        while (i >= 0 && bits[i] == 1)
        {
            i--;
        }

        return (n - 2 - i) % 2 == 0;
    }
};
```
