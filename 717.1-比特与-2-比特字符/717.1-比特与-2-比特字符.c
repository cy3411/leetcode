/*
 * @lc app=leetcode.cn id=717 lang=c
 *
 * [717] 1比特与2比特字符
 */

// @lc code=start

bool isOneBitCharacter(int *bits, int bitsSize)
{
    int i = 0;
    while (i < bitsSize - 1)
    {
        if (bits[i] == 1)
        {
            i += 2;
        }
        else
        {
            i++;
        }
    }
    return i == bitsSize - 1;
}
// @lc code=end
