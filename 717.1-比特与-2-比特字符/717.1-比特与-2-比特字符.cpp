/*
 * @lc app=leetcode.cn id=717 lang=cpp
 *
 * [717] 1比特与2比特字符
 */

// @lc code=start
class Solution
{
public:
    bool isOneBitCharacter(vector<int> &bits)
    {
        int n = bits.size();
        int i = n - 2;
        while (i >= 0 && bits[i] == 1)
        {
            i--;
        }

        return (n - 2 - i) % 2 == 0;
    }
};
// @lc code=end
