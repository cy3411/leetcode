/*
 * @lc app=leetcode.cn id=717 lang=typescript
 *
 * [717] 1比特与2比特字符
 */

// @lc code=start
function isOneBitCharacter(bits: number[]): boolean {
  const n = bits.length;
  let i = 0;
  // 逢1加2，逢0加1
  while (i < n - 1) {
    if (bits[i] === 1) i += 2;
    else i++;
  }

  return i === n - 1;
}
// @lc code=end
