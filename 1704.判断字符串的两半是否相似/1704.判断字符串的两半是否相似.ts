/*
 * @lc app=leetcode.cn id=1704 lang=typescript
 *
 * [1704] 判断字符串的两半是否相似
 */

// @lc code=start
function halvesAreAlike(s: string): boolean {
  const map = new Set<string>(['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']);
  let i = 0;
  let j = s.length - 1;
  let lcount = 0;
  let rcount = 0;
  // 统计左右两部分元音的数量
  while (i < j) {
    lcount += map.has(s[i]) ? 1 : 0;
    rcount += map.has(s[j]) ? 1 : 0;
    i++;
    j--;
  }

  return lcount === rcount;
}
// @lc code=end
