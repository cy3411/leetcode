/*
 * @lc app=leetcode.cn id=1704 lang=cpp
 *
 * [1704] 判断字符串的两半是否相似
 */

// @lc code=start
class Solution {
public:
    bool halvesAreAlike(string s) {
        string h = "aeiouAEIOU";
        int i = 0, j = s.size() - 1;
        int lcount = 0, rcount = 0;
        while (i < j) {
            if (h.find_first_of(s[i]) != string::npos) {
                lcount++;
            }
            if (h.find_first_of(s[j]) != string::npos) {
                rcount++;
            }
            i++, j--;
        }

        return lcount == rcount;
    }
};
// @lc code=end
