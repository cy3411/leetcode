# 题目

给你一个偶数长度的字符串 s 。将其拆分成长度相同的两半，前一半为 a ，后一半为 b 。

两个字符串 相似 的前提是它们都含有相同数目的元音（'a'，'e'，'i'，'o'，'u'，'A'，'E'，'I'，'O'，'U'）。注意，s 可能同时含有大写和小写字母。

如果 a 和 b 相似，返回 true ；否则，返回 false 。

提示：

- $2 \leq s.length \leq 1000$
- s.length 是偶数
- s 由 大写和小写 字母组成

# 示例

```
输入：s = "book"
输出：true
解释：a = "bo" 且 b = "ok" 。a 中有 1 个元音，b 也有 1 个元音。所以，a 和 b 相似。
```

```
输入：s = "textbook"
输出：false
解释：a = "text" 且 b = "book" 。a 中有 1 个元音，b 中有 2 个元音。因此，a 和 b 不相似。
注意，元音 o 在 b 中出现两次，记为 2 个。
```

# 题解

## 遍历

使用头尾双指针同时开始遍历 s，遍历过程中统计两部分元音的数量，最后比较两者统计的数量是否相等。

为了方便判断是否是元音，可以使用哈希表将元音存储起来，共后续判断使用。

```ts
function halvesAreAlike(s: string): boolean {
  const map = new Set<string>(['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']);
  let i = 0;
  let j = s.length - 1;
  let lcount = 0;
  let rcount = 0;
  // 统计左右两部分元音的数量
  while (i < j) {
    lcount += map.has(s[i]) ? 1 : 0;
    rcount += map.has(s[j]) ? 1 : 0;
    i++;
    j--;
  }

  return lcount === rcount;
}
```

```cpp
class Solution {
public:
    bool halvesAreAlike(string s) {
        string h = "aeiouAEIOU";
        int i = 0, j = s.size() - 1;
        int lcount = 0, rcount = 0;
        while (i < j) {
            if (h.find_first_of(s[i]) != string::npos) {
                lcount++;
            }
            if (h.find_first_of(s[j]) != string::npos) {
                rcount++;
            }
            i++, j--;
        }

        return lcount == rcount;
    }
};
```

```py
class Solution:
    def halvesAreAlike(self, s: str) -> bool:
        h = 'aeiouAEIOU'
        n = len(s)
        half = n // 2
        a, b = s[:half], s[half:]
        return sum(c in h for c in a) == sum(c in h for c in b)
```
