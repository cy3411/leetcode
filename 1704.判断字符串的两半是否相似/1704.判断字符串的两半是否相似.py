#
# @lc app=leetcode.cn id=1704 lang=python3
#
# [1704] 判断字符串的两半是否相似
#

# @lc code=start
class Solution:
    def halvesAreAlike(self, s: str) -> bool:
        h = 'aeiouAEIOU'
        n = len(s)
        half = n // 2
        a, b = s[:half], s[half:]
        return sum(c in h for c in a) == sum(c in h for c in b)
# @lc code=end
