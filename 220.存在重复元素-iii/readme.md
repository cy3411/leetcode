# 题目

给你一个整数数组 `nums` 和两个整数 `k` 和 `t` 。请你判断是否存在 两个不同下标 `i` 和 `j`，使得 `abs(nums[i] - nums[j]) <= t` ，同时又满足 `abs(i - j) <= k` 。

如果存在则返回 `true，不存在返回` `false。`

提示：

- `0 <= nums.length <= 2 * 104`
- `-231 <= nums[i] <= 231 - 1`
- `0 <= k <= 104`
- `0 <= t <= 231 - 1`

# 示例

```
输入：nums = [1,2,3,1], k = 3, t = 0
输出：true
```

```
输入：nums = [1,5,9,1,5,9], k = 2, t = 3
输出：false
```

# 题解

## 遍历

遍历元素，当前遍历到的元素和其后最多`k`个元素比较是否符合`abs(nums[i] - nums[j]) <= t `

```js
function containsNearbyAlmostDuplicate(nums: number[], k: number, t: number): boolean {
  const size = nums.length;

  for (let i = 0; i < size; i++) {
    // i和i后的最多k个位置比较
    for (let j = i + 1; j <= i + k && j < size; j++) {
      if (Math.abs(nums[i] - nums[j]) <= t) return true;
    }
  }

  return false;
}
```

## 桶排序+滑动窗口

题目给出了`k`的范围，自然就想到可以使用滑动窗口来维护这个范围。

现在就是在这个范围内取符合题意的一对值，我们这里采用类似桶排序的思路。

由于`t`的值有可能为 0，所以我们使用`t+1`来进行分桶计算。

同一个桶中的元素它们之间的绝对值肯定是符合题意的，相邻的桶中元素也有可能符合题意，需要单独判断一次。

如果遍历到有 2 个元素的桶 id 一样，就直接返回结果了，所以这里不需要用桶来存储所有的数据。

```js
function containsNearbyAlmostDuplicate(nums: number[], k: number, t: number): boolean {
  const bucket: Map<number, number> = new Map();

  const getBucketId = (x: number, size: number): number => {
    return x < 0 ? Math.ceil(x / size) - 1 : Math.floor(x / size);
  };

  for (let i = 0; i < nums.length; i++) {
    const x = nums[i];
    const id = getBucketId(x, t + 1);

    if (bucket.has(id)) {
      return true;
    }
    if (bucket.has(id - 1) && Math.abs(nums[i] - bucket.get(id - 1)) <= t) {
      return true;
    }
    if (bucket.has(id + 1) && Math.abs(nums[i] - bucket.get(id + 1)) <= t) {
      return true;
    }
    bucket.set(id, x);
    // 维护窗口大小
    if (i >= k) {
      bucket.delete(getBucketId(nums[i - k], t + 1));
    }
  }

  return false;
}
```
