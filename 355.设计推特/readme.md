# 题目
设计一个简化版的推特(Twitter)，可以让用户实现发送推文，关注/取消关注其他用户，能够看见关注人（包括自己）的最近十条推文。你的设计需要支持以下的几个功能：

1. **postTweet(userId, tweetId)**: 创建一条新的推文
2. **getNewsFeed(userId)**: 检索最近的十条推文。每个推文都必须是由此用户关注的人或者是用户自己发出的。推文必须按照时间顺序由最近的开始排序。
3. **follow(followerId, followeeId)**: 关注一个用户
4. **unfollow(followerId, followeeId)**: 取消关注一个用户


# 示例
```
Twitter twitter = new Twitter();

// 用户1发送了一条新推文 (用户id = 1, 推文id = 5).
twitter.postTweet(1, 5);

// 用户1的获取推文应当返回一个列表，其中包含一个id为5的推文.
twitter.getNewsFeed(1);

// 用户1关注了用户2.
twitter.follow(1, 2);

// 用户2发送了一个新推文 (推文id = 6).
twitter.postTweet(2, 6);

// 用户1的获取推文应当返回一个列表，其中包含两个推文，id分别为 -> [6, 5].
// 推文id6应当在推文id5之前，因为它是在5之后发送的.
twitter.getNewsFeed(1);

// 用户1取消关注了用户2.
twitter.unfollow(1, 2);

// 用户1的获取推文应当返回一个列表，其中包含一个id为5的推文.
// 因为用户1已经不再关注用户2.
twitter.getNewsFeed(1);
```

# 题解
题意就是模拟一个数据库存储和检索的功能。
```js
// 用户
class User {
  // 发送推文的时间戳
  static timestamp = new Date().getTime();
  constructor(userID) {
    this.id = userID;
    // 关注的用户
    this.follows = new Set();
    // 发送的推文
    this.tweets = [];
  }

  /**
   * @description: 发送推文
   * @param {number} tweetID
   * @return {void}
   */
  post(tweetID) {
    const tweet = new Tweet(tweetID, User.timestamp++);
    this.tweets.push(tweet);
  }
  /**
   * @description: 关注用户
   * @param {number} userID
   * @return {void}
   */
  follow(userID) {
    this.follows.add(userID);
  }
  /**
   * @description: 取消关注
   * @param {number} userID
   * @return {void}
   */
  unfollow(userID) {
    this.follows.delete(userID);
  }
}

// 推文
class Tweet {
  constructor(tweetID, timestamp) {
    this.id = tweetID;
    this.timestamp = timestamp;
  }
}

/**
 * Initialize your data structure here.
 */
var Twitter = function () {
  // 模拟数据库存储的用户
  this.users = new Map();
};

/**
 * Compose a new tweet.
 * @param {number} userId
 * @param {number} tweetId
 * @return {void}
 */
Twitter.prototype.postTweet = function (userId, tweetId) {
  // 如果库中没有用户，新建一个
  if (!this.users.has(userId)) {
    this.users.set(userId, new User(userId));
  }
  const user = this.users.get(userId);
  user.post(tweetId);
};

/**
 * Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent.
 * @param {number} userId
 * @return {number[]}
 */
Twitter.prototype.getNewsFeed = function (userId) {
  // 库中没查询到用户
  if (!this.users.has(userId)) {
    return [];
  }
  // 当前用户和他关注的用户
  const userIDs = [userId, ...this.users.get(userId).follows];
  // 取出用户列表中所有用户发送的tweet
  const twwers = [];
  for (let idx of userIDs) {
    let user = this.users.get(idx);
    twwers.push(...user.tweets);
  }
  // 放入大顶堆，按照时间从大到小
  const maxPQ = new MaxPQ(twwers, (x) => x.timestamp);
  const result = [];
  // 取前10条最新的数据
  while (result.length < 10 && maxPQ.size()) {
    result.push(maxPQ.poll().id);
  }

  return result;
};

/**
 * Follower follows a followee. If the operation is invalid, it should be a no-op.
 * @param {number} followerId
 * @param {number} followeeId
 * @return {void}
 */
Twitter.prototype.follow = function (followerId, followeeId) {
  if (!this.users.has(followerId)) {
    this.users.set(followerId, new User(followerId));
  }
  if (!this.users.has(followeeId)) {
    this.users.set(followeeId, new User(followeeId));
  }
  const user = this.users.get(followerId);
  user.follow(followeeId);
};

/**
 * Follower unfollows a followee. If the operation is invalid, it should be a no-op.
 * @param {number} followerId
 * @param {number} followeeId
 * @return {void}
 */
Twitter.prototype.unfollow = function (followerId, followeeId) {
  if (this.users.has(followerId)) {
    const user = this.users.get(followerId);
    user.unfollow(followeeId);
  }
};
```


```js
// 堆base
class PQ {
  /**
   * @description: PQ基类构造函数
   * @param {[any]} keys
   * @param {function} mapKeysValue
   * @return {void}
   */
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;
    // 初始化一下keys的排序
    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  /**
   * @description: 比较i元素是否小于j元素
   * @param {number} i
   * @param {number} j
   * @return {boolean}
   */
  less(i, j) {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }
  /**
   * @description: 交换i和j元素
   * @param {number} i
   * @param {number} j
   * @return {void}
   */
  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  /**
   * @description: 元素上浮
   * @param {number} index 需要上浮元素的索引
   * @return {void}
   */
  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  /**
   * @description: 元素下沉
   * @param {number} index 需要下沉元素的索引
   * @return {void}
   */
  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }
  /**
   * @description: 堆的大小
   * @return {number}
   */
  size() {
    return this.keys.length;
  }
  /**
   * @description: 插入操作
   * @param {any} key
   * @return {void}
   */
  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }
  /**
   * @description: 删除顶部元素
   * @return {any}
   */
  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }
  /**
   * @description: 查看顶部元素
   * @return {any}
   */
  peek() {
    return this.keys[0];
  }
}
// 大顶堆
class MaxPQ extends PQ {}
```

