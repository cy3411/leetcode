# 题目

给你一个长度为 `n` 的二维整数数组 `groups` ，同时给你一个整数数组 `nums` 。

你是否可以从 `nums` 中选出 `n` 个 **不相交** 的子数组，使得第 `i` 个子数组与 `groups[i]` （下标从 `0` 开始）完全相同，且如果 `i > 0` ，那么第 `(i-1)` 个子数组在 `nums` 中出现的位置在第 `i` 个子数组前面。（也就是说，这些子数组在 `nums` 中出现的顺序需要与 `groups` 顺序相同）

如果你可以找出这样的 `n` 个子数组，请你返回 `true` ，否则返回 `false` 。

如果不存在下标为 `k` 的元素 `nums[k]` 属于不止一个子数组，就称这些子数组是 **不相交** 的。子数组指的是原数组中连续元素组成的一个序列。

提示：

- $groups.length \equiv n$
- $1 \leq n \leq 10^3$
- $1 \leq groups[i].length, sum(groups[i].length) \leq 10^3$
- $1 \leq nums.length \leq 10^3$
- $-10^7 \leq groups[i][j], nums[k] \leq 10^7$

# 示例

```
输入：groups = [[1,-1,-1],[3,-2,0]], nums = [1,-1,0,1,-1,-1,3,-2,0]
输出：true
解释：你可以分别在 nums 中选出第 0 个子数组 [1,-1,0,1,-1,-1,3,-2,0] 和第 1 个子数组 [1,-1,0,1,-1,-1,3,-2,0] 。
这两个子数组是不相交的，因为它们没有任何共同的元素。
```

```
输入：groups = [[10,-2],[1,2,3,4]], nums = [1,2,3,4,10,-2]
输出：false
解释：选择子数组 [1,2,3,4,10,-2] 和 [1,2,3,4,10,-2] 是不正确的，因为它们出现的顺序与 groups 中顺序不同。
[10,-2] 必须出现在 [1,2,3,4] 之前。
```

# 题解

## 模拟

定义 i 为 nums 指针， j 为 groups 指针。

如果 groups[j] 中全部元素匹配，则继续匹配下一个组，i 指针跳过 groups[j]的长度。 否则 i 加 1，继续匹配当前组。

最后检查所有的 groups 元素全部匹配即可，也就是 `j === groups.length`

```ts
function isChecked(group: number[], nums: number[], i: number): boolean {
  const n = group.length;
  const m = nums.length;
  // 当前组的长度超过的 nums 长度
  if (i + n > m) return false;
  for (let k = 0; k < n; k++) {
    if (group[k] !== nums[k + i]) return false;
  }
  return true;
}

function canChoose(groups: number[][], nums: number[]): boolean {
  const n = groups.length;
  let i = 0;
  let j = 0;
  while (i < nums.length && j < n) {
    // 当前组的元素都匹配
    if (isChecked(groups[j], nums, i)) {
      i += groups[j].length;
      j++;
    } else {
      i++;
    }
  }
  return j === n;
}
```
