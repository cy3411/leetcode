/*
 * @lc app=leetcode.cn id=1764 lang=typescript
 *
 * [1764] 通过连接另一个数组的子数组得到一个数组
 */

// @lc code=start

function isChecked(group: number[], nums: number[], i: number): boolean {
  const n = group.length;
  const m = nums.length;
  // 当前组的长度超过的 nums 长度
  if (i + n > m) return false;
  for (let k = 0; k < n; k++) {
    if (group[k] !== nums[k + i]) return false;
  }
  return true;
}

function canChoose(groups: number[][], nums: number[]): boolean {
  const n = groups.length;
  let i = 0;
  let j = 0;
  while (i < nums.length && j < n) {
    // 当前组的元素都匹配
    if (isChecked(groups[j], nums, i)) {
      i += groups[j].length;
      j++;
    } else {
      i++;
    }
  }
  return j === n;
}
// @lc code=end
