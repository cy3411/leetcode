# 题目

大餐 是指 恰好包含两道不同餐品 的一餐，其美味程度之和等于 2 的幂。

你可以搭配 任意 两道餐品做一顿大餐。

给你一个整数数组 deliciousness ，其中 deliciousness[i] 是第 i​​​​​​​​​​​​​​ 道餐品的美味程度，返回你可以用数组中的餐品做出的不同 大餐 的数量。结果需要对 109 + 7 取余。

注意，只要餐品下标不同，就可以认为是不同的餐品，即便它们的美味程度相同。

提示：

- 1 <= deliciousness.length <= 105
- 0 <= deliciousness[i] <= 220

# 示例

```
输入：deliciousness = [1,3,5,7,9]
输出：4
解释：大餐的美味程度组合为 (1,3) 、(1,7) 、(3,5) 和 (7,9) 。
它们各自的美味程度之和分别为 4 、8 、8 和 16 ，都是 2 的幂。
```

# 题解

## 哈希表

美味程度指的任意两个值的和，且和等于 2 的幂。所以，美味程度肯定不会超过数组中最大值的倍数。

我们可是使用类似[1.两数之和](https://gitee.com/cy3411/leetcode/tree/master/1.%E4%B8%A4%E6%95%B0%E4%B9%8B%E5%92%8C)的题解。

遍历数组，然后去哈希表中查找 2 的幂减去当前元素的值，如果存在，就表示找到一对符合提议的结果。

```ts
function countPairs(deliciousness: number[]): number {
  const maxValue = Math.max(...deliciousness);
  // 当前数组所能获得最大的幂
  const maxSum = maxValue * 2;
  // 统计数组中数字出现的次数
  const hash: Map<number, number> = new Map();
  const m = deliciousness.length;
  const mod = 10 ** 9 + 7;
  let ans = 0;
  for (let i = 0; i < m; i++) {
    const value = deliciousness[i];
    // 遍历[1,maxSum]中所有2的幂，查看哈希表中是否有能凑合和的另一个数的数量
    for (let k = 1; k <= maxSum; k <<= 1) {
      const count = hash.get(k - value) || 0;
      ans = (ans + count) % mod;
    }
    hash.set(value, (hash.get(value) || 0) + 1);
  }
  return ans;
}
```
