# 题目

给定一个表示数据的整数数组 `data` ，返回它是否为有效的 `UTF-8` 编码。

`UTF-8` 中的一个字符可能的长度为 `1` 到 `4` 字节，遵循以下的规则：

- 对于 `1` 字节 的字符，字节的第一位设为 `0` ，后面 `7` 位为这个符号的 `unicode` 码。
- 对于 `n` 字节 的字符 `(n > 1)`，第一个字节的前 `n` 位都设为 `1`，第 `n+1` 位设为 `0` ，后面字节的前两位一律设为 `10` 。剩下的没有提及的二进制位，全部为这个符号的 `unicode` 码。

这是 `UTF-8` 编码的工作方式：

```
 Char. number range  |        UTF-8 octet sequence
      (hexadecimal)    |              (binary)
   --------------------+---------------------------------------------
   0000 0000-0000 007F | 0xxxxxxx
   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
```

注意：输入是整数数组。只有每个整数的 **最低 8 个有效位** 用来存储数据。这意味着每个整数只表示 1 字节的数据。

提示:

- $1 \leq data.length \leq 2 * 10^4$
- $0 \leq data[i] \leq 255$

# 示例

```
输入：data = [197,130,1]
输出：true
解释：数据表示字节序列:11000101 10000010 00000001。
这是有效的 utf-8 编码，为一个 2 字节字符，跟着一个 1 字节字符。
```

```
输入：data = [235,140,4]
输出：false
解释：数据表示 8 位的序列: 11101011 10001100 00000100.
前 3 位都是 1 ，第 4 位为 0 表示它是一个 3 字节字符。
下一个字节是开头为 10 的延续字节，这是正确的。
但第二个延续字节不以 10 开头，所以是不符合规则的。
```

# 题解

## 遍历+位运算

遍历 data，检查每个元素(定义 n 为 data 的长度, idx 为访问元素的下标)：

- 判断首字节是否合法，，如果合法，则根据首字节可以计算出一共几个字节(bytes)
- 根据返回的字节数，判断后面的其他字节是否合法
- idx += bytes
- 继续遍历，直到 idx 大于 n

```ts
function validUtf8(data: number[]): boolean {
  const MASK1 = 1 << 7;
  const MASK2 = MASK1 + (1 << 6);
  // 根据首字节高位1的数量返回编码长度
  const getBytes = (num: number): number => {
    // 1位编码
    if ((num & MASK1) === 0) return 1;
    let n = 0;
    let mask = MASK1;
    // 当高位是1的时候，继续循环
    while ((num & mask) !== 0) {
      n++;
      // 超过4位属于不合法的编码
      if (n > 4) return -1;
      mask >>= 1;
    }

    return n < 2 ? -1 : n;
  };
  // 验证其他字节是否合法（就是高位必须10开头）
  const isValid = (num: number): boolean => {
    return (num & MASK2) === MASK1;
  };

  const n = data.length;
  let idx = 0;
  while (idx < n) {
    const num = data[idx];
    const bytes = getBytes(num);
    // 编码长度不合法或者超出数组长度
    if (bytes < 0 || idx + bytes > n) return false;
    // 遍历bytes - 1个字节
    for (let i = 1; i < bytes; i++) {
      // 字节不合法
      if (!isValid(data[idx + i])) return false;
    }
    // 寻找下一个字节
    idx += bytes;
  }

  return true;
}
```

```cpp
class Solution
{
    int MASK1 = 0x80, MASK2 = 0x80 + 0x40;

public:
    int getBytes(int num)
    {
        if ((num & MASK1) == 0)
        {
            return 1;
        }

        int n = 0;
        int mask = MASK1;
        while ((num & mask) != 0)
        {
            n++;
            if (n > 4)
            {
                return -1;
            }
            mask >>= 1;
        }
        return n < 2 ? -1 : n;
    }

    bool isValid(int num)
    {
        return (num & MASK2) == MASK1;
    }

    bool validUtf8(vector<int> &data)
    {
        int n = data.size();
        int idx = 0;
        while (idx < n)
        {
            int num = data[idx];
            int bytes = getBytes(num);
            if (bytes < 0 || idx + bytes > n)
            {
                return false;
            }
            for (int i = 1; i < bytes; i++)
            {
                if (!isValid(data[idx + i]))
                {
                    return false;
                }
            }
            idx += bytes;
        }
        return true;
    }
};
```
