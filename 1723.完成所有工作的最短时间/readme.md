# 题目

给你一个整数数组 `jobs` ，其中 `jobs[i]` 是完成第 `i` 项工作要花费的时间。

请你将这些工作分配给 `k` 位工人。所有工作都应该分配给工人，且每项工作只能分配给一位工人。工人的 **工作时间** 是完成分配给他们的所有工作花费时间的总和。请你设计一套最佳的工作分配方案，使工人的 **最大工作时间** 得以 **最小化** 。

返回分配方案中尽可能 **最小** 的 最大工作时间 。

提示：

- $\color{burlywood}1 \leq k \leq jobs.length \leq 12$
- $\color{burlywood}1 \leq jobs[i] \leq 10^7$

# 示例

```
输入：jobs = [3,2,3], k = 3
输出：3
解释：给每位工人分配一项工作，最大工作时间是 3 。
```

```
输入：jobs = [1,2,4,7,8], k = 2
输出：11
解释：按下述方式分配工作：
1 号工人：1、2、8（工作时间 = 1 + 2 + 8 = 11）
2 号工人：4、7（工作时间 = 4 + 7 = 11）
最大工作时间是 11 。
```

# 题解

## 回溯

穷举每一种分配组合，取中间的最大工作时间的最小值。

```ts
function minimumTimeRequired(jobs: number[], k: number): number {
  let ans = Number.POSITIVE_INFINITY;
  const slot = new Array(k).fill(0);
  const backtrack = (jobs: number[], idx: number, slot: number[], maxNum: number) => {
    if (idx === jobs.length) {
      // 如果已经遍历完所有的工作，则更新答案
      ans = maxNum;
      return;
    }
    const k = slot.length;
    // 给工人分配下标为idx的任务
    for (let i = 0; i < k; i++) {
      // 当前工人的工作总时长大于结果，当前工人不需要分配工作，换下一个工人
      if (slot[i] + jobs[idx] > ans) continue;
      slot[i] += jobs[idx];
      backtrack(jobs, idx + 1, slot, Math.max(maxNum, slot[i]));
      slot[i] -= jobs[idx];
      // 当前工人只有一个工作，已经处理过了，可以不用再分配了
      // 减枝
      if (slot[i] === 0) break;
    }
  };
  backtrack(jobs, 0, slot, 0);
  return ans;
}
```

```cpp
class Solution
{
public:
    void backtrack(vector<int> &jobs, int idx, vector<int> &slot, int maxNum, int &ans)
    {
        if (idx == jobs.size())
        {
            ans = maxNum;
            return;
        }

        for (int i = 0; i < slot.size(); i++)
        {
            if (slot[i] + jobs[idx] > ans)
                continue;
            slot[i] += jobs[idx];
            backtrack(jobs, idx + 1, slot, max(maxNum, slot[i]), ans);
            slot[i] -= jobs[idx];
            if (slot[i] == 0)
                break;
        }
    };

    int minimumTimeRequired(vector<int> &jobs, int k)
    {
        vector<int> slot(k, 0);
        int ans = INT_MAX;
        backtrack(jobs, 0, slot, 0, ans);
        return ans;
    };
};
```
