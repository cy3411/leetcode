# 题目

编写一个函数来查找字符串数组中的最长公共前缀。

如果不存在公共前缀，返回空字符串 ""。

提示：

- $1 \leq strs.length \leq 200$
- $0 \leq strs[i].length \leq 200$
- `strs[i]` 仅由小写英文字母组成

# 示例

```
输入：strs = ["flower","flow","flight"]
输出："fl"
```

```
输入：strs = ["dog","racecar","car"]
输出：""
解释：输入不存在公共前缀。
```

# 题解

## 遍历

比较前两个元素的最长公共前缀，再拿这个前缀和后面的元素依次比较，因为后面的元素最长公共前缀的长度不能超过前两个元素的长度。所以最后比较结束，返回结果。

```ts
function longestCommonPrefix(strs: string[]): string {
  // 将前缀和字符串比较，返回二者最长公共前缀
  const compare = (prefix: string, str: string): string => {
    let res = '';
    const n = prefix.length;
    for (let i = 0; i < n; i++) {
      if (i === str.length || prefix[i] !== str[i]) return res;
      res += prefix[i];
    }
    return res;
  };

  // 初始化第一个串为最长公共前缀
  let ans = strs[0];
  const n = strs.length;
  for (let i = 1; i < n; i++) {
    // 每次比较返回公共前缀
    ans = compare(ans, strs[i]);
  }

  return ans;
}
```
