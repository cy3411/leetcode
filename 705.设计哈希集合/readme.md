# 描述

不使用任何内建的哈希表库设计一个哈希集合（HashSet）。

实现 MyHashSet 类：

- void add(key) 向哈希集合中插入值 key 。
- bool contains(key) 返回哈希集合中是否存在这个值 key 。
- void remove(key) 将给定值 key 从哈希集合中删除。如果哈希集合中没有这个值，什么也不做。

提示：

- 0 <= key <= 10\*\*6
- 最多调用 10\*\*4 次 add、remove 和 contains 。

# 示例

```
输入：
["MyHashSet", "add", "add", "contains", "contains", "add", "contains", "remove", "contains"]
[[], [1], [2], [1], [3], [2], [2], [2], [2]]
输出：
[null, null, null, true, false, null, true, null, false]

解释：
MyHashSet myHashSet = new MyHashSet();
myHashSet.add(1);      // set = [1]
myHashSet.add(2);      // set = [1, 2]
myHashSet.contains(1); // 返回 True
myHashSet.contains(3); // 返回 False ，（未找到）
myHashSet.add(2);      // set = [1, 2]
myHashSet.contains(2); // 返回 True
myHashSet.remove(2);   // set = [1]
myHashSet.contains(2); // 返回 False ，（已移除）
```

# 题解

## 简单版本

题目给出的提示最多 10\*\*4 次的调用，简单点的就是用 10\*\*4 大小的数组实现

```js
var MyHashSet = function () {
  this.st = new Array(10 ** 4);
};

/**
 * @param {number} key
 * @return {void}
 */
MyHashSet.prototype.add = function (key) {
  this.st[key] = true;
};

/**
 * @param {number} key
 * @return {void}
 */
MyHashSet.prototype.remove = function (key) {
  this.st[key] = void 0;
};

/**
 * Returns true if this set contains the specified element
 * @param {number} key
 * @return {boolean}
 */
MyHashSet.prototype.contains = function (key) {
  return this.st[key] !== void 0;
};
```

## 线性碰撞法

```js
var MyHashSet = function (capacity = 4) {
  this.capacity = capacity;
  this.size = 0;
  this.st = new Array(capacity);
  this.resize = (capacity) => {
    const hash = new MyHashSet(capacity);
    for (let i = 0; i < this.capacity; i++) {
      if (this.st[i] !== void 0) {
        hash.add(this.st[i]);
      }
    }
    this.st = hash.st;
    this.capacity = hash.capacity;
  };
  this.hash = (key) => {
    return key % this.capacity;
  };
};

/**
 * @param {number} key
 * @return {void}
 */
MyHashSet.prototype.add = function (key) {
  // 扩容hash table
  if (this.size >= this.capacity / 2) {
    this.resize(this.capacity * 2);
  }
  let i;
  for (i = this.hash(key); this.st[i] !== void 0; i = (i + 1) % this.capacity) {
    // 原来有值就更新
    if (this.st[i] === key) {
      this.st[i] = key;
      return;
    }
  }
  this.st[i] = key;
  this.size++;
};

/**
 * @param {number} key
 * @return {void}
 */
MyHashSet.prototype.remove = function (key) {
  if (!this.contains(key)) return;

  // 找到对应hash
  let i = this.hash(key);
  while (this.st[i] !== key) {
    i = (i + 1) % this.capacity;
  }
  this.st[i] = void 0;
  this.size--;

  // 剩下重复的hash重新排序
  i = (i + 1) % this.capacity;
  while (this.st[i] !== void 0) {
    let key = this.st[i];
    this.st[i] = void 0;
    this.size--;
    this.add(key);
    i = (i + 1) % this.capacity;
  }

  // 收缩hash table
  if (this.size > 0 && this.size < this.capacity / 8) {
    this.resize(this.capacity / 2);
  }
};

/**
 * Returns true if this set contains the specified element
 * @param {number} key
 * @return {boolean}
 */
MyHashSet.prototype.contains = function (key) {
  for (let i = this.hash(key); this.st[i] !== void 0; i = (i + 1) % this.capacity) {
    if (this.st[i] === key) {
      return true;
    }
  }
  return false;
};
```

## 拉链法

数据存储单元存储的是链表，如果有冲突的元素，就添加到当前索引下链表中。

```ts
class LinkNode {
  value: number | null;
  next: LinkNode | null;
  prev: LinkNode | null;
  constructor(value: number = null, next: LinkNode = null, prev: LinkNode = null) {
    this.value = value;
    this.next = next;
    this.prev = prev;
  }
  remove() {
    this.prev.next = this.next;
    this.next.prev = this.prev;
    this.next = this.prev = null;
  }

  insert(node: LinkNode) {
    const p = this.next;
    node.prev = this;
    node.next = p;
    this.next = node;
    p.prev = node;
  }
}

class HashLink {
  head: LinkNode;
  tail: LinkNode;
  constructor() {
    this.head = new LinkNode();
    this.tail = new LinkNode();
    this.head.next = this.tail;
    this.tail.prev = this.head;
  }

  add(key: number): void {
    const node = new LinkNode(key);
    this.head.insert(node);
  }

  remove(key: number): void {
    let p = this.head;
    while (p) {
      if (p.value === key) {
        p.remove();
      }
      p = p.next;
    }
  }

  contains(key: number): boolean {
    let p = this.head;
    while (p) {
      if (p.value === key) {
        return true;
      }
      p = p.next;
    }
    return false;
  }
}

class MyHashSet {
  data: HashLink[];
  capcity: number;
  constructor() {
    this.capcity = 100;
    this.data = new Array(this.capcity).fill(new HashLink());
  }

  private hash(key: number): number {
    return key % this.capcity;
  }

  add(key: number): void {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    if (hashlink.contains(key)) return;
    hashlink.add(key);
  }

  remove(key: number): void {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    if (!hashlink.contains(key)) return;
    hashlink.remove(key);
  }

  contains(key: number): boolean {
    const idx = this.hash(key);
    const hashlink = this.data[idx];
    return hashlink.contains(key);
  }
}
```
