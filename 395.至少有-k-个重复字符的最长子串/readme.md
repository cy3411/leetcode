# 题目

给你一个字符串 s 和一个整数 k ，请你找出 s 中的最长子串， 要求该子串中的每一字符出现次数都不少于 k 。返回这一子串的长度。

提示：

- 1 <= s.length <= 104
- s 仅由小写英文字母组成
- 1 <= k <= 105

# 示例

```
输入：s = "aaabb", k = 3
输出：3
解释：最长子串为 "aaa" ，其中 'a' 重复了 3 次。
```

# 题解

## 递归

符合题意的子串里面肯定不包含出现次数小于 k 的字符，我们可以以出现次数小于 k 次的字符为分割点，将字符串分割成多个字串，然后递归求解相同问题。

```ts
function longestSubstring(s: string, k: number): number {
  const m = s.length;
  // 记录每个字符出现的次数
  const hash: Map<string, number> = new Map();
  for (const char of s) {
    hash.set(char, (hash.get(char) || 0) + 1);
  }
  // 记录分割点
  const delimit: number[] = [];
  for (let i = 0; i < m; i++) {
    if (hash.get(s[i]) < k) delimit.push(i);
  }
  // 最后加入哨兵点，方便计算
  delimit.push(m);
  // 如果分割点只有最后一个，表示当前字符串符合题意
  if (delimit.length === 1) return m;

  // 递归求解子串
  let pre = 0;
  let ans = 0;
  for (const idx of delimit) {
    // 字串长度
    let len = idx - pre;
    // 取最大长度
    ans = Math.max(ans, longestSubstring(s.substr(pre, len), k));
    pre = idx + 1;
  }
  return ans;
}
```
