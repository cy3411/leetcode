# 题目

给你一个数组 `nums` ，它包含 `n` 个正整数。你需要计算所有非空连续子数组的和，并将它们按升序排序，得到一个新的包含 $n * (n + 1) / 2$ 个数字的数组。

请你返回在新数组中下标为 `left` 到 `right` （下标从 `1` 开始）的所有数字和（包括左右端点）。由于答案可能很大，请你将它对 $10^9 + 7$ 取模后返回。

提示：

- $1 \leq nums.length \leq 10^3$
- $nums.length \equiv n$
- $1 \leq nums[i] \leq 100$
- $1 \leq left \leq right \leq n * (n + 1) / 2$

# 示例

```
输入：nums = [1,2,3,4], n = 4, left = 1, right = 5
输出：13
解释：所有的子数组和为 1, 3, 6, 10, 2, 5, 9, 3, 7, 4 。将它们升序排序后，我们得到新的数组 [1, 2, 3, 3, 4, 5, 6, 7, 9, 10] 。下标从 le = 1 到 ri = 5 的和为 1 + 2 + 3 + 3 + 4 = 13 。
```

```
输入：nums = [1,2,3,4], n = 4, left = 3, right = 4
输出：6
解释：给定数组与示例 1 一样，所以新数组为 [1, 2, 3, 3, 4, 5, 6, 7, 9, 10] 。下标从 le = 3 到 ri = 4 的和为 3 + 3 = 6 。
```

# 题解

## 优先队列

题意就是从 nums 中挑选出一个子数组，求子数组的和，升序排序后，取 [left, right] 区间的总和。

假设 nums = [1,2,3,4]，那么它的子数组就是：

- [0,0],[0,1],[0,2],[0,3]
- [1,1],[1,2],[1,3]
- [2,2],[2,3]
- [3,3]

我们可以使用多路快排的思想，使用小顶堆，每次取出最小的元素，然后将取出元素的下一个子数组和放入堆中，直到取了 [left, right] 区间的和。

```ts
interface RangeUnit {
  // 区间开始位置
  i: number;
  // 区间结束位置
  j: number;
  // 区间和
  sum: number;
}

class PQ {
  heap: RangeUnit[];
  size: number;
  constructor() {
    this.heap = [];
    this.size = 0;
  }
  private less(i: number, j: number): boolean {
    return this.heap[i].sum < this.heap[j].sum;
  }
  private swap(i: number, j: number) {
    [this.heap[i], this.heap[j]] = [this.heap[j], this.heap[i]];
  }
  private siftDown(i: number) {
    while (i * 2 + 1 < this.size) {
      let j = i * 2 + 1;
      if (j + 1 < this.size && this.less(j + 1, j)) j++;
      if (this.less(i, j)) break;
      this.swap(i, j);
      i = j;
    }
  }
  private siftUp(i: number) {
    while (i > 0 && this.less(i, (i - 1) >> 1)) {
      this.swap(i, (i - 1) >> 1);
      i = (i - 1) >> 1;
    }
  }
  insert(val: RangeUnit) {
    this.heap.push(val);
    this.size++;
    this.siftUp(this.size - 1);
  }
  pop(): RangeUnit | null {
    if (this.size === 0) return null;
    const val = this.heap[0];
    this.swap(0, this.size - 1);
    this.heap.pop();
    this.size--;
    this.siftDown(0);
    return val;
  }
}

function rangeSum(nums: number[], n: number, left: number, right: number): number {
  const minpq = new PQ();
  for (let i = 0; i < n; i++) {
    minpq.insert({ i, j: i, sum: nums[i] });
  }

  let ans = 0;
  const mod = 1e9 + 7;
  for (let i = 1; i <= right; i++) {
    const curr: RangeUnit = minpq.pop()!;
    // 答案区间开始位置
    if (i >= left) {
      ans += curr.sum;
      ans %= mod;
    }
    // 更新队列，
    if (curr.j + 1 < n) {
      minpq.insert({
        i: curr.i,
        j: curr.j + 1,
        sum: (curr.sum + nums[curr.j + 1]) % mod,
      });
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    struct RangeUnit
    {
        RangeUnit(int i, int j, int sum) : i(i), j(j), sum(sum) {}

        int i, j, sum;
    };

    struct cmp
    {
        bool operator()(const RangeUnit &a, const RangeUnit &b)
        {
            return a.sum > b.sum;
        }
    };

    int rangeSum(vector<int> &nums, int n, int left, int right)
    {
        priority_queue<RangeUnit, vector<RangeUnit>, cmp> pq;
        int modNum = 1e9 + 7;
        for (int i = 0; i < n; i++)
        {
            pq.push(RangeUnit(i, i, nums[i]));
        }

        int ans = 0;
        for (int i = 1; i <= right; i++)
        {
            RangeUnit curr = pq.top();
            pq.pop();
            if (i >= left)
            {
                ans += curr.sum;
                ans %= modNum;
            }
            if (curr.j + 1 < n)
            {
                pq.push(RangeUnit(curr.i, curr.j + 1, curr.sum + nums[curr.j + 1]));
            }
        }
        return ans;
    };
};
```
