# 题目

给你一个大小为 `rows x cols` 的矩阵 `mat`，其中 `mat[i][j]` 是 `0` 或 `1`，请返回 **矩阵** `mat` 中特殊位置的数目 。

**特殊位置** 定义：如果 $mat[i][j] \equiv 1$ 并且第 `i` 行和第 `j` 列中的所有其他元素均为 `0`（行和列的下标均 从 `0` 开始 ），则位置 `(i, j)` 被称为特殊位置。

提示：

- $rows \equiv mat.length$
- $cols \equiv mat[i].length$
- $1 \leq rows, cols \leq 100$
- `mat[i][j]` 是 `0` 或 `1`

# 示例

```
输入：mat = [[1,0,0],
            [0,0,1],
            [1,0,0]]
输出：1
解释：(1,2) 是一个特殊位置，因为 mat[1][2] == 1 且所处的行和列上所有其他元素都是 0
```

```
输入：mat = [[1,0,0],
            [0,1,0],
            [0,0,1]]
输出：3
解释：(0,0), (1,1) 和 (2,2) 都是特殊位置
```

# 题解

## 预处理

遍历 mat ，枚举每一个元素，如果当前元素为 1 ，判断其行列其他元素是否都为 0 ， 是的话答案加 1 。

由于需要多次判断，我们可以预处理每个元素的行列的和是否为 1，方便处理。

```js
function numSpecial(mat: number[][]): number {
  const m = mat.length;
  const n = mat[0].length;
  // 预处理
  const rowNumber = new Array(m).fill(0);
  const colNumber = new Array(n).fill(0);
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      rowNumber[i] += mat[i][j];
      colNumber[j] += mat[i][j];
    }
  }
  // 计算答案
  let ans = 0;
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (mat[i][j] && rowNumber[i] === 1 && colNumber[j] === 1) {
        ans++;
      }
    }
  }
  return ans;
}
```
