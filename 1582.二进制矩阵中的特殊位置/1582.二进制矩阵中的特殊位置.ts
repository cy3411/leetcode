/*
 * @lc app=leetcode.cn id=1582 lang=typescript
 *
 * [1582] 二进制矩阵中的特殊位置
 */

// @lc code=start
function numSpecial(mat: number[][]): number {
  const m = mat.length;
  const n = mat[0].length;
  // 预处理
  const rowNumber = new Array(m).fill(0);
  const colNumber = new Array(n).fill(0);
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      rowNumber[i] += mat[i][j];
      colNumber[j] += mat[i][j];
    }
  }
  // 计算答案
  let ans = 0;
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (mat[i][j] && rowNumber[i] === 1 && colNumber[j] === 1) {
        ans++;
      }
    }
  }
  return ans;
}
// @lc code=end
