/*
 * @lc app=leetcode.cn id=720 lang=typescript
 *
 * [720] 词典中最长的单词
 */

// @lc code=start
function longestWord(words: string[]): string {
  // 按照单词长度升序、字典序降序
  // 确保遍历的时候，比当前单词长度短或者字典序比当前单词大的已经遍历了
  words.sort((a, b) => {
    if (a.length !== b.length) {
      return a.length - b.length;
    }
    return b.localeCompare(a);
  });

  let ans = '';
  const map = new Set<string>();
  // 一个字母的前缀是空字符串，这里做哨兵使用
  map.add('');
  for (const word of words) {
    // 如果当前单词的前缀在map中，则更新ans
    if (map.has(word.slice(0, word.length - 1))) {
      ans = word;
      map.add(word);
    }
  }

  return ans;
}
// @lc code=end
