# 题目

给出一个字符串数组 `words` 组成的一本英语词典。返回 `words` 中最长的一个单词，该单词是由 `words` 词典中其他单词逐步添加一个字母组成。

若其中有多个可行的答案，则返回答案中字典序最小的单词。若无答案，则返回空字符串。

提示：

- $1 \leq words.length \leq 1000$
- $1 \leq words[i].length \leq 30$
- 所有输入的字符串 `words[i]` 都只包含小写字母。

# 示例

```
输入：words = ["w","wo","wor","worl", "world"]
输出："world"
解释： 单词"world"可由"w", "wo", "wor", 和 "worl"逐步添加一个字母组成。
```

```
输入：words = ["a", "banana", "app", "appl", "ap", "apply", "apple"]
输出："apple"
解释："apply" 和 "apple" 都能由词典中的单词组成。但是 "apple" 的字典序小于 "apply"
```

# 题解

## 排序+哈希表

对 `words` 进行排序：

- 如果单词长度不相等，按照单词长度升序排序
- 如果单词长度相等，按照字典序降序排序

这样可以保证当遍历到当前单词的时候，单词的前缀都已经访问过了，并且当前一定是字典序最小的单词。

我们使用哈希表记录之前访问过的单词，每次检查当前单词的前缀是否在哈希表中，是的话表示当前单词可以添加一个字母组成，则更新答案，并将当前单词加入哈希表。

```ts
function longestWord(words: string[]): string {
  // 按照单词长度升序、字典序降序
  // 确保遍历的时候，比当前单词长度短或者字典序比当前单词大的已经遍历了
  words.sort((a, b) => {
    if (a.length !== b.length) {
      return a.length - b.length;
    }
    return b.localeCompare(a);
  });

  let ans = '';
  const map = new Set<string>();
  // 一个字母的前缀是空字符串，这里做哨兵使用
  map.add('');
  for (const word of words) {
    // 如果当前单词的前缀在map中，则更新ans
    if (map.has(word.slice(0, word.length - 1))) {
      ans = word;
      map.add(word);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    string longestWord(vector<string> &words)
    {
        sort(words.begin(), words.end(), [](const string &a, const string &b)
             { return a.size() == b.size() ? a > b : a.size() < b.size(); });
        unordered_set<string> dict = {""};
        string ans;
        for (auto &word : words)
        {
            if (dict.find(word.substr(0, word.size() - 1)) != dict.end())
            {
                dict.insert(word);
                ans = word;
            }
        }
        return ans;
    }
};
```

## 前缀树

将所有的单词放入字典树中，遍历 `words` ，判断每个单词是否符合要求并更新答案。如果是符合要求的单词，则对比单词和答案，如果单词的长度大于答案，或者单词在字典序中更小，则更新答案。

```cpp
class Tire
{
    vector<Tire *> children;
    bool isWord;

public:
    Tire() : isWord(false)
    {
        children = vector<Tire *>(26, nullptr);
    }

    void insert(string &s)
    {
        Tire *node = this;
        for (char c : s)
        {
            if (node->children[c - 'a'] == nullptr)
            {
                node->children[c - 'a'] = new Tire();
            }
            node = node->children[c - 'a'];
        }
        node->isWord = true;
    }

    bool search(const string &s)
    {
        Tire *node = this;
        for (char c : s)
        {
            // 没有这个单词或者没有单词的前缀，直接返回false
            if (node->children[c - 'a'] == nullptr || !node->children[c - 'a']->isWord)
            {
                return false;
            }
            node = node->children[c - 'a'];
        }
        return (node != nullptr && node->isWord);
    }
};

class Solution
{
public:
    string longestWord(vector<string> &words)
    {
        Tire *tire = new Tire();
        for (string &word : words)
        {
            tire->insert(word);
        }

        string ans;
        for (const string &word : words)
        {
            if (tire->search(word))
            {
                if (word.size() > ans.size() || (word.size() == ans.size() && word < ans))
                {
                    ans = word;
                }
            }
        }

        return ans;
    }
};
```
