/*
 * @lc app=leetcode.cn id=1221 lang=typescript
 *
 * [1221] 分割平衡字符串
 */

// @lc code=start
function balancedStringSplit(s: string): number {
  // 统计LR字符出现的次数
  let r = 0;
  let l = 0;
  let ans = 0;

  for (const char of s) {
    if (char === 'L') l++;
    else r++;
    // 出现次数相等，表示是平衡字符串
    if (r === l && r > 0 && l > 0) {
      ans++;
      r = 0;
      l = 0;
    }
  }

  return ans;
}
// @lc code=end
