/*
 * @lc app=leetcode.cn id=492 lang=typescript
 *
 * [492] 构造矩形
 */

// @lc code=start
function constructRectangle(area: number): number[] {
  let W = Math.sqrt(area) >> 0;

  while (area % W) {
    W--;
  }

  const L = area / W;

  return [L, W];
}
// @lc code=end
