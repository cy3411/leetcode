# 题目

给定一个正整数 n，返回 连续正整数满足所有数字之和为 n 的组数 。

# 示例

```
输入: n = 5
输出: 2
解释: 5 = 2 + 3，共有两组连续整数([5],[2,3])求和后为 5。
```

```
输入: n = 9
输出: 3
解释: 9 = 4 + 5 = 2 + 3 + 4
```

# 题解

## 数学

```ts
function consecutiveNumbersSum(n: number): number {
  let ans = 0;
  const bound = 2 * n;
  const isK = (k: number): boolean => {
    if (k % 2) {
      return n % k === 0;
    } else {
      return n % k !== 0 && (2 * n) % k === 0;
    }
  };
  for (let k = 1; k * (k + 1) <= bound; k++) {
    if (isK(k)) {
      ans++;
    }
  }
  return ans;
}
```
