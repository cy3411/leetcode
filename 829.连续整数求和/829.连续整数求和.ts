/*
 * @lc app=leetcode.cn id=829 lang=typescript
 *
 * [829] 连续整数求和
 */

// @lc code=start
function consecutiveNumbersSum(n: number): number {
  let ans = 0;
  const bound = 2 * n;
  const isK = (k: number): boolean => {
    if (k % 2) {
      return n % k === 0;
    } else {
      return n % k !== 0 && (2 * n) % k === 0;
    }
  };
  for (let k = 1; k * (k + 1) <= bound; k++) {
    if (isK(k)) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
