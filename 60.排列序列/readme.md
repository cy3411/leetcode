# 题目

给出集合 [1,2,3,...,n]，其所有元素共有 `n!` 种排列。

按大小顺序列出所有排列情况，并一一标记，当 `n = 3` 时, 所有排列如下：

- "123"
- "132"
- "213"
- "231"
- "312"
- "321"

给定 `n` 和 `k`，返回第 `k` 个排列。

提示：

- $\color{goldenrod}1 \leq n \leq 9$
- $\color{goldenrod}1 \leq k \leq n!$

# 示例

```
输入：n = 3, k = 3
输出："213"
```

```
输入：n = 4, k = 9
输出："2314"
```

# 题解

## 康托展开

题目可以看出，排列按照首位数字相同分组的话，可以分成 `n` 个组，每个组中的排列数量为 `n-1!`。

这样我们可以通过 k 来找到首位数字的索引:$\color{goldenrod}1+\frac{k-1}{n-1!}$。

假设索引为 index，然后通过这个索引去 n 中找第 index 个未被使用的数字。

持续上面的操作，缩小范围，找下一位。

从高位到低位，依次求出每一位的数字，即可得到第 `k` 个排列。

```ts
function getPermutation(n: number, k: number): string {
  // [1,n-1]的阶乘
  let base = 1;
  for (let i = 1; i < n; i++) {
    base *= i;
  }
  // 标识某个数字是否被使用过
  const mark = new Array(n + 1).fill(0);
  // 选择第n个未被使用的数字
  const pick = (n: number): number => {
    let i = 0;
    let cnt = 0;
    while (cnt < n) {
      i += 1;
      cnt += mark[i] === 0 ? 1 : 0;
    }
    mark[i] = 1;
    return i;
  };

  let ans = '';
  for (let i = n; i >= 1; i--) {
    // 当前位置的索引
    const index = 1 + Math.floor((k - 1) / base);
    ans += pick(index).toString();
    // 缩小范围
    k -= (index - 1) * base;
    base /= i - 1;
  }

  return ans;
}
```
