# 题目

给你一个整数数组 `arr` ，你一开始在数组的第一个元素处（下标为 0）。

每一步，你可以从下标 `i` 跳到下标：

- `i + 1` 满足：`i + 1 < arr.length`
- `i - 1` 满足：`i - 1 >= 0`
- `j` 满足：`arr[i] == arr[j]` 且 `i != j`

请你返回到达数组最后一个元素的下标处所需的 **最少操作次数** 。

注意：任何时候你都不能跳到数组外面。

提示：

- $\color{burlywood}1 <= arr.length <= 5 * 10^4$
- $\color{burlywood}-10^8 <= arr[i] <= 10^8$

# 示例

```
输入：arr = [100,-23,-23,404,100,23,23,23,3,404]
输出：3
解释：那你需要跳跃 3 次，下标依次为 0 --> 4 --> 3 --> 9 。下标 9 为数组的最后一个元素的下标。
```

```
输入：arr = [7,6,9,6,9,6,9,7]
输出：1
解释：你可以直接从下标 0 处跳到下标 7 处，也就是数组的最后一个元素处。
```

# 题解

## 广度优先搜索

题目描述的数组可以抽象成一个无向图，相邻的元素之间有一条无向边，元素相同的节点之间也有一条无向边。

求从第一个元素到最后一个元素的最短路径，可以使用广度优先搜索。

里面有一个优化，就是值相同的元素之间的边只需要访问一次即可，因为重复访问对求最短路径没有影响。

```ts
function minJumps(arr: number[]): number {
  const n = arr.length;
  // 记录相同元素的索引
  const hash = new Map<number, number[]>();
  for (let i = 0; i < n; i++) {
    const key = arr[i];
    if (!hash.has(key)) hash.set(key, []);
    hash.get(key).push(i);
  }

  const visited = new Set<number>();
  visited.add(0);
  const deque = new Array<number[]>();
  deque.push([0, 0]);

  while (deque.length) {
    let [idx, step] = deque.shift();
    if (idx === n - 1) {
      return step;
    }

    step++;
    // 将相同元素的索引放入队列，避免后面重复计算
    const val = arr[idx];
    if (hash.has(val)) {
      for (const i of hash.get(val)) {
        if (!visited.has(i)) {
          visited.add(i);
          deque.push([i, step]);
        }
      }
      hash.delete(val);
    }
    // 是否可以往后走一步
    if (idx + 1 < n && !visited.has(idx + 1)) {
      visited.add(idx + 1);
      deque.push([idx + 1, step]);
    }
    // 是否可以往前走一步
    if (idx - 1 >= 0 && !visited.has(idx - 1)) {
      visited.add(idx - 1);
      deque.push([idx - 1, step]);
    }
  }

  return -1;
}
```

```cpp
class Solution
{
public:
    typedef pair<int, int> PII;

    int minJumps(vector<int> &arr)
    {
        unordered_map<int, vector<int>> idxSameVal;
        int n = arr.size();
        for (int i = 0; i < n; i++)
        {
            idxSameVal[arr[i]].push_back(i);
        }

        unordered_set<int> visited;
        queue<PII> q;
        visited.emplace(0);
        q.push(PII(0, 0));

        while (!q.empty())
        {
            auto [idx, step] = q.front();
            q.pop();

            if (idx == n - 1)
            {
                return step;
            }

            step++;

            int val = arr[idx];
            if (idxSameVal.count(val))
            {
                for (int i : idxSameVal[val])
                {
                    if (visited.count(i))
                    {
                        continue;
                    }
                    visited.emplace(i);
                    q.push(PII(i, step));
                }
                idxSameVal.erase(val);
            };

            if (idx + 1 < n && !visited.count(idx + 1))
            {
                visited.emplace(idx + 1);
                q.push(PII(idx + 1, step));
            };

            if (idx - 1 >= 0 && !visited.count(idx - 1))
            {
                visited.emplace(idx - 1);
                q.push(PII(idx - 1, step));
            };
        }

        return -1;
    }
};
```
