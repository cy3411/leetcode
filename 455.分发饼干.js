/*
 * @lc app=leetcode.cn id=455 lang=javascript
 *
 * [455] 分发饼干
 */

// @lc code=start
/**
 * @param {number[]} g
 * @param {number[]} s
 * @return {number}
 */
var findContentChildren = function (g, s) {
  g.sort((a, b) => a - b);
  s.sort((a, b) => a - b);
  let result = 0;
  let i = 0;
  let j = 0;
  while (i < g.length && j < s.length) {
    while (g[i] > s[j] && j < s.length) {
      j++;
    }
    if (j < s.length) {
      result++;
    }
    i++;
    j++;
  }
  return result;
};
// @lc code=end
