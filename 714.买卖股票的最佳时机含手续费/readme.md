# 题目

给定一个整数数组 `prices`，其中第 `i` 个元素代表了第 `i` 天的股票价格 ；整数 `fee` 代表了交易股票的手续费用。

你可以无限次地完成交易，但是你每笔交易都需要付手续费。如果你已经购买了一个股票，在卖出它之前你就不能再继续购买股票了。

返回获得利润的最大值。

注意：这里的一笔交易指买入持有并卖出股票的整个过程，每笔交易你只需要为支付一次手续费。

提示：

- $1 \leq prices.length \leq 5 * 10^4$
- $1 \leq prices[i] < 5 * 10^4$
- $0 \leq fee < 5 * 10^4$

# 示例

```
输入：prices = [1, 3, 2, 8, 4, 9], fee = 2
输出：8
解释：能够达到的最大利润:
在此处买入 prices[0] = 1
在此处卖出 prices[3] = 8
在此处买入 prices[4] = 4
在此处卖出 prices[5] = 9
总利润: ((8 - 1) - 2) + ((9 - 4) - 2) = 8
```

# 题解

## 动态规划

**状态**

定义`dp[i][0]`为第 `i` 天没有持有股票的最大收益
定义`dp[i][1]`为第 `i` 天持有股票的最大收益

**转移**

当前未持有股票的状态：

- 前一天持有股票的状态，今天卖出
- 前一天未持有股票的状态

当前持有股票的状态：

- 前一天未持有股票的状态，今天买入
- 前一天持有股票的状态

$$
\begin{cases}
    dp[i][0] = max(dp[i-1][0], dp[i-1][1] +prices[i] - fee)
    \\
    dp[i][1] = max(dp[i-1][1], dp[i-1][0] - prices[i])
\end{cases}
$$

**边界**

```ts
function maxProfit(prices: number[], fee: number): number {
  const n = prices.length;
  // 第i天持有股票和没有股票的最大收益
  const dp = new Array(n).fill(0).map((_) => new Array(2));
  // 初始化
  dp[0][0] = 0;
  dp[0][1] = -prices[0];

  for (let i = 1; i < n; i++) {
    // 当前未持有股票
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i] - fee);
    // 当前持有股票
    dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
  }

  return Math.max(...dp[n - 1]);
}
```
