/*
 * @lc app=leetcode.cn id=27 lang=typescript
 *
 * [27] 移除元素
 */

// @lc code=start
function removeElement(nums: number[], val: number): number {
  const size = nums.length;
  // 边界
  if (size === 0) return 0;

  let left: number = 0;
  let right: number = size - 1;

  while (left <= right) {
    if (nums[left] === val) {
      nums[left] = nums[right--];
    } else {
      left++;
    }
  }
  return left;
}
// @lc code=end
