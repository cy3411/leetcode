# 题目
给你一个数组 `nums` 和一个值 `val`，你需要 **原地** 移除所有数值等于 `val` 的元素，并返回移除后数组的新长度。

不要使用额外的数组空间，你必须仅使用 `O(1)` 额外空间并 **原地** 修改输入数组。

元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

提示：

+ 0 <= nums.length <= 100
+ 0 <= nums[i] <= 50
+ 0 <= val <= 100

# 示例
```
输入：nums = [3,2,2,3], val = 3
输出：2, nums = [2,2]
解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
```

# 题解
## 双指针
题目要求原地修改，可以使用双指针：
+ slow指针指向要赋值的位置
+ fast指针指向当前要处理的元素

当`fast`指针指向的元素不等于`val`，将`fast`指针指向的元素复制到'slow'指针的位置，2个指针同时后移。

当`fast`指针指向的元素等于`val`，它不能在输出结果中，'slow'指针不动，'fast'指针后移。

整个过程保持不变的性质是：区间 $[0,\textit{left})$ 中的元素都不等于 $\textit{val}$。

```js
function removeElement(nums: number[], val: number): number {
  const size = nums.length;
  // 边界
  if (size === 0) return 0;

  let slow = 0;
  for (let i = 0; i < size; i++) {
    // 如果快指针不等于目标，两个指针同时后移
    if (nums[i] !== val) {
      nums[slow] = nums[i];
      slow++;
    }
  }

  return slow;
}
```


上面方法，如果数组中的第一个元素是需要移除的元素，那么后续所有的元素都要前移一位。

题目也说明了，元素的顺序可以改变，所以可以考虑首尾指针，往中间遍历来实现。

```js
function removeElement(nums: number[], val: number): number {
  const size = nums.length;
  // 边界
  if (size === 0) return 0;

  let left: number = 0;
  let right: number = size - 1;

  while (left <= right) {
    if (nums[left] === val) {
      nums[left] = nums[right--];
    } else {
      left++;
    }
  }
  return left;
}
```