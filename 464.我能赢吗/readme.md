# 题目

在 "100 game" 这个游戏中，两名玩家轮流选择从 1 到 10 的任意整数，累计整数和，先使得累计整数和达到或超过 100 的玩家，即为胜者。

如果我们将游戏规则改为 “玩家不能重复使用整数” 呢？

例如，两个玩家可以轮流从公共整数池中抽取从 1 到 15 的整数（不放回），直到累计整数和 >= 100。

给定一个整数 maxChoosableInteger （整数池中可选择的最大数）和另一个整数 desiredTotal（累计和），判断先出手的玩家是否能稳赢（假设两位玩家游戏时都表现最佳）？

你可以假设 maxChoosableInteger 不会大于 20， desiredTotal 不会大于 300。

# 示例

```
输入：
maxChoosableInteger = 10
desiredTotal = 11

输出：
false

解释：
无论第一个玩家选择哪个整数，他都会失败。
第一个玩家可以选择从 1 到 10 的整数。
如果第一个玩家选择 1，那么第二个玩家只能选择从 2 到 10 的整数。
第二个玩家可以通过选择整数 10（那么累积和为 11 >= desiredTotal），从而取得胜利.
同样地，第一个玩家选择任意其他整数，第二个玩家都会赢。
```

# 题解

## 回溯

我要赢那么对立条件就是对方必须输。

递归去判断当前 desiredTotal 情况下，对方是否会输，如果输，那么就我就能赢。

```ts
function dfs(mask: number, n: number, total: number, hash: Map<number, boolean>): boolean {
  // 记忆化
  if (hash.has(mask)) return hash.get(mask);
  for (let i = 1; i <= n; i++) {
    if (mask & (1 << i)) continue;
    if (i >= total) return true;
    // 我选择完了，递归去计算对方的结果
    // 如果我要赢，那对方必须输才可以
    if (!dfs(mask | (1 << i), n, total - i, hash)) {
      hash.set(mask, true);
      return true;
    }
  }
  hash.set(mask, false);
  return false;
}

function canIWin(maxChoosableInteger: number, desiredTotal: number): boolean {
  // 如果maxChoosableInteger累加和小于desiredTotal，第一个肯定输
  if (((maxChoosableInteger + 1) * maxChoosableInteger) / 2 < desiredTotal) return false;
  // 使用位操作记录用过的数字
  const mask = 0;
  // 记忆化，mask自变量，当作key
  const hash: Map<number, boolean> = new Map();
  return dfs(mask, maxChoosableInteger, desiredTotal, hash);
}
```

```cpp
class Solution {
public:
    bool dfs(int maxChoosableInteger, int desiredTotal,
             unordered_map<int, bool> &m, int mask) {
        if (m.find(mask) != m.end()) return m[mask];
        for (int i = 1; i <= maxChoosableInteger; i++) {
            if (mask & (1 << i)) continue;
            if (i >= desiredTotal) return true;
            if (!dfs(maxChoosableInteger, desiredTotal - i, m,
                     mask | (1 << i))) {
                m[mask] = true;
                return true;
            }
        }
        m[mask] = false;
        return false;
    }
    bool canIWin(int maxChoosableInteger, int desiredTotal) {
        if ((1 + maxChoosableInteger) * maxChoosableInteger / 2 < desiredTotal)
            return false;

        unordered_map<int, bool> m;
        int mask = 0;

        return dfs(maxChoosableInteger, desiredTotal, m, mask);
    }
};
```
