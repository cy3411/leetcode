# 题目

给你一个整数数组 `arr` 和一个整数值 `target` 。

请你在 `arr` 中找 **两个互不重叠的子数组** 且它们的和都等于 `target` 。可能会有多种方案，请你返回满足要求的两个子数组长度和的 **最小值** 。

请返回满足要求的最小长度和，如果无法找到这样的两个子数组，请返回 `-1` 。

提示：

- $\color{burlywood}1 <= arr.length <= 10^5$
- $\color{burlywood}1 <= arr[i] <= 1000$
- $\color{burlywood}1 <= target <= 10^8$

# 示例

```
输入：arr = [3,2,2,4,3], target = 3
输出：2
解释：只有两个子数组和为 3 （[3] 和 [3]）。它们的长度和为 2 。
```

```
输入：arr = [7,3,4,7], target = 7
输出：2
解释：尽管我们有 3 个互不重叠的子数组和为 7 （[7], [3,4] 和 [7]），但我们会选择第一个和第三个子数组，因为它们的长度和 2 是最小值。
```

# 题解

## 滑动窗口

遍历数组，使用滑动窗口统计出区间和等于 `target` 的区间,并记录区间的左右索引。

题目要求互不重叠的子数组，那么相邻的两个子数组，第一个区间的右索引一定小于第二个区间的左索引。

遍历统计的区间数组，每次更新当前区间之前不相交区间的位置，并记录之前的最小长度。同时更新答案，就是当前区间长度和之前的最小长度的较小值。

```ts
function minSumOfLengths(arr: number[], target: number): number {
  const n = arr.length;
  // 保存区间和为target的左右索引：[l,r]
  const ranges: number[][] = [];
  let l = 0;
  let sum = 0;
  // 统计区间和为target的区间
  for (let r = 0; r < n; r++) {
    sum += arr[r];
    while (sum > target && l <= r) {
      sum -= arr[l];
      l++;
    }
    if (sum === target) {
      ranges.push([l, r]);
    }
  }

  let ans = -1;
  let preIdx = -1;
  let preMin = Number.POSITIVE_INFINITY;
  for (const [l, r] of ranges) {
    // 找到当前元素之前与之不相交的区间
    while (ranges[preIdx + 1][1] < l) {
      preIdx++;
      preMin = Math.min(preMin, ranges[preIdx][1] - ranges[preIdx][0] + 1);
    }
    if (preIdx === -1) continue;
    // 更新答案
    if (ans === -1 || ans > preMin + r - l + 1) {
      ans = preMin + r - l + 1;
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    typedef pair<int, int> pii;
    int minSumOfLengths(vector<int> &arr, int target)
    {
        vector<pii> ranges;
        int n = arr.size(), l = 0, sum = 0;
        for (int r = 0; r < n; r++)
        {
            sum += arr[r];
            while (sum > target && l <= r)
            {
                sum -= arr[l++];
            }
            if (sum == target)
            {
                ranges.push_back(pii(l, r));
            }
        }

        int ans = -1, preIdx = -1, preMin = n + 1;
        for (auto range : ranges)
        {
            while (ranges[preIdx + 1].second < range.first)
            {
                preIdx++;
                preMin = min(preMin, ranges[preIdx].second - ranges[preIdx].first + 1);
            }
            if (preIdx == -1)
                continue;
            if (ans == -1 || ans > preMin + range.second - range.first + 1)
            {
                ans = preMin + range.second - range.first + 1;
            }
        }

        return ans;
    }
};
```
