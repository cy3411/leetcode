# 题目

峰值元素是指其值严格大于左右相邻值的元素。

给你一个整数数组 `nums`，找到峰值元素并返回其索引。数组可能包含多个峰值，在这种情况下，返回 任何一个峰值 所在位置即可。

你可以假设 `nums[-1] = nums[n] = -∞` 。

你必须实现时间复杂度为 `O(log n)` 的算法来解决此问题。

提示：

- `1 <= nums.length <= 1000`
- `-2**31 <= nums[i] <= 2**31 - 1`
- 对于所有有效的 `i` 都有 `nums[i] != nums[i + 1]`

# 示例

```
输入：nums = [1,2,3,1]
输出：2
解释：3 是峰值元素，你的函数应该返回其索引 2。
```

# 题解

## 二分搜索

题意要求时间复杂度为`O(log n)`，自然就想到了二分搜索。

如果 `nums[i]<nums[i+1]`,我们知道位置 `i+1` 以及其右侧的位置中一定有一个峰值,那么:

- 下标 `i` 是峰值，我们返回 `i`
- `nums[i]<nums[i+1]`，那么我们抛弃 `[l,i]` 的范围，在剩余 `[i+1,r]` 的范围内继续随机选取下标
- `nums[i]>nums[i+1]`，那么我们抛弃 `[i,r]` 的范围，在剩余 `[l,i−1]` 的范围内继续随机选取下标

```ts
function findPeakElement(nums: number[]): number {
  // 处理边界，返回当前索引的值
  const getNum = (idx: number): number => {
    if (idx < 0 || idx === n) return Number.NEGATIVE_INFINITY;
    return nums[idx];
  };
  // 返回比较结果
  const compare = (idx1: number, idx2: number): number => {
    const a = getNum(idx1);
    const b = getNum(idx2);

    if (a === b) return 0;

    return a < b ? -1 : 1;
  };

  const n = nums.length;
  let head = 0;
  let tail = n - 1;

  while (head <= tail) {
    let mid = head + ((tail - head) >> 1);
    // 符合峰值条件，直接返回
    if (compare(mid - 1, mid) < 0 && compare(mid, mid + 1) > 0) {
      return mid;
    }
    if (compare(mid, mid + 1) < 0) {
      // 当前小于下一个值，表示峰值在后面
      head = mid + 1;
    } else {
      // 峰值在前面
      tail = mid - 1;
    }
  }

  return -1;
}
```
