# 题目

给定两个以升序排列的整形数组 nums1 和 nums2, 以及一个整数 k。

定义一对值 (u,v)，其中第一个元素来自 nums1，第二个元素来自 nums2。

找到和最小的 k 对数字 (u1,v1), (u2,v2) ... (uk,vk)。

# 示例

```
输入: nums1 = [1,7,11], nums2 = [2,4,6], k = 3
输出: [1,2],[1,4],[1,6]
解释: 返回序列中的前 3 对数：
     [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]
```

# 题解

堆的代码可以查看：[ 1046.最后一块石头的重量 ](https://gitee.com/cy3411/leecode/tree/master/1046.%E6%9C%80%E5%90%8E%E4%B8%80%E5%9D%97%E7%9F%B3%E5%A4%B4%E7%9A%84%E9%87%8D%E9%87%8F)

维护大小为 `k` 的一个大顶堆，超过大小就把堆顶弹出，最后的堆内元素就是结果。

## 堆

```js
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @param {number} k
 * @return {number[][]}
 */
var kSmallestPairs = function (nums1, nums2, k) {
  const maxPQ = new MaxPQ([], (mapKeysValue = (x) => x.reduce((acc, cur) => acc + cur)));

  for (let i = 0; i < nums1.length; i++) {
    for (let j = 0; j < nums2.length; j++) {
      // 当2个数的和大于堆顶元素，这2个数就不需要入堆
      if (maxPQ.size() === k && nums1[i] + nums2[j] > maxPQ.peek()[0] + maxPQ.peek()[1]) {
        continue;
      }
      maxPQ.insert([nums1[i], nums2[j]]);
      if (maxPQ.size() > k) {
        maxPQ.poll();
      }
    }
  }

  return [...maxPQ.keys];
};
```

上面的方法在数据量大的时候会超时，所以需要优化。

我们注意到数组都是升序排列，所以最小的元素一定是 `nums1[0] + nums2[0]`。

但是次小的元素是 `nums1[0] + nums2[1]` 还是 `nums1[1] + nums2[0]` 呢？

这里使用优先队列小顶堆，初始化将 `num1` 的索引和 `nums2` 第一个索引入队，出队 `[0,0]`，然后将 nums2 出队的索引加 1 后，再次入队，知道比较 k 次后，出队的元素就是结果。

```ts
type GetCompareKey<T> = (x: T) => number;

class PQ<T> {
  keys: T[];
  mapKeysValue: GetCompareKey<T>;
  constructor(mapKeysValue: GetCompareKey<T>) {
    this.keys = [];
    this.mapKeysValue = mapKeysValue;
  }
  insert(key: T) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }
  peek() {
    if (this.size() === 0) return void 0;
    return this.keys[0];
  }
  poll() {
    if (this.size() === 0) return void 0;
    const key = this.keys[0];
    this.swap(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return key;
  }
  less(i: number, j: number): boolean {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }
  swap(i: number, j: number) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }
  size() {
    return this.keys.length;
  }
  swin(i: number) {
    let parent = (i - 1) >> 1;
    while (parent >= 0 && this.less(parent, i)) {
      this.swap(parent, i);
      i = parent;
      parent = (i - 1) >> 1;
    }
  }
  sink(i: number) {
    let child = i * 2 + 1;
    while (child < this.size()) {
      if (child + 1 < this.size() && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, i)) break;
      this.swap(child, i);
      i = child;
      child = i * 2 + 1;
    }
  }
  isEmpty() {
    return this.size() === 0;
  }
}

class MINPQ<T> extends PQ<T> {
  less(i: number, j: number): boolean {
    return this.mapKeysValue(this.keys[i]) > this.mapKeysValue(this.keys[j]);
  }
}

function kSmallestPairs(nums1: number[], nums2: number[], k: number): number[][] {
  const mapKeysValue = (x: number[]) => nums1[x[0]] + nums2[x[1]];
  const minpq = new MINPQ<number[]>(mapKeysValue);

  // 初始化，将num1的元素下标和num2的第一个元素下标放入minpq
  for (let i = 0; i < Math.min(k, nums1.length); i++) {
    minpq.insert([i, 0]);
  }

  const ans: number[][] = [];
  // 遍历k次或者minpq为空
  while (k > 0 && !minpq.isEmpty()) {
    const [i, j] = minpq.poll();
    ans.push([nums1[i], nums2[j]]);
    // 继续将nums2的下一个元素下标放入minpq
    if (j + 1 < nums2.length) {
      minpq.insert([i, j + 1]);
    }
    k--;
  }

  return ans;
}
```
