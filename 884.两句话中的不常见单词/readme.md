# 题目

句子 是一串由空格分隔的单词。每个 **单词** 仅由小写字母组成。

如果某个单词在其中一个句子中恰好出现一次，在另一个句子中却 没有出现 ，那么这个单词就是 **不常见的** 。

给你两个 句子 `s1` 和 `s2` ，返回所有 **不常用单词** 的列表。返回列表中单词可以按 **任意顺序** 组织。

提示：

- $\color{burlywood}1 \leq s1.length, s2.length \leq 200$
- `s1` 和 `s2` 由小写英文字母和空格组成
- `s1` 和 `s2` 都不含前导或尾随空格
- `s1` 和 `s2` 中的所有单词间均由单个空格分隔

# 示例

```
输入：s1 = "this apple is sweet", s2 = "this apple is sour"
输出：["sweet","sour"]
```

```
输入：s1 = "apple apple", s2 = "banana"
输出：["banana"]
```

# 题解

## 哈希表

题意就是在两个句子中找出只出现 `1` 次的单词。

使用哈希表记录句子中单词出现的次数，然后遍历哈希表，找出出现 `1` 次的单词。

```ts
function uncommonFromSentences(s1: string, s2: string): string[] {
  const map = new Map<string, number>();
  const arr = [...s1.split(' '), ...s2.split(' ')];
  for (const word of arr) {
    map.set(word, (map.get(word) ?? 0) + 1);
  }

  const ans: string[] = [];

  for (const entiry of map.entries()) {
    if (entiry[1] === 1) {
      ans.push(entiry[0]);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    void insert(const string &s, unordered_map<string, int> &freq)
    {
        stringstream ss(s);
        string word;
        while (ss >> word)
        {
            freq[word]++;
        }
    }
    vector<string> uncommonFromSentences(string s1, string s2)
    {
        unordered_map<string, int> freq;
        insert(s1, freq);
        insert(s2, freq);

        vector<string> ans;
        for (auto &p : freq)
        {
            if (p.second == 1)
            {
                ans.push_back(p.first);
            }
        }

        return ans;
    }
};
```

```cpp
class Solution
{
public:
    void insert(const string &s, unordered_map<string, int> &freq)
    {
        int n = s.size();
        int l = 0, r = 0;
        while (r < n)
        {
            while (r < n && s[r] >= 'a' && s[r] <= 'z')
            {
                r++;
            }
            if (r - l > 0)
            {
                freq[s.substr(l, r - l)]++;
            }
            l = r + 1;
            r = l;
        }
    }
    vector<string> uncommonFromSentences(string s1, string s2)
    {
        unordered_map<string, int> freq;
        insert(s1, freq);
        insert(s2, freq);

        vector<string> ans;
        for (auto &p : freq)
        {
            if (p.second == 1)
            {
                ans.push_back(p.first);
            }
        }

        return ans;
    }
};
```
