/*
 * @lc app=leetcode.cn id=884 lang=typescript
 *
 * [884] 两句话中的不常见单词
 */

// @lc code=start
function uncommonFromSentences(s1: string, s2: string): string[] {
  const map = new Map<string, number>();
  const arr = [...s1.split(' '), ...s2.split(' ')];
  for (const word of arr) {
    map.set(word, (map.get(word) ?? 0) + 1);
  }

  const ans: string[] = [];

  for (const entiry of map.entries()) {
    if (entiry[1] === 1) {
      ans.push(entiry[0]);
    }
  }

  return ans;
}
// @lc code=end
