/*
 * @lc app=leetcode.cn id=884 lang=cpp
 *
 * [884] 两句话中的不常见单词
 */

// @lc code=start
class Solution
{
public:
    void insert(const string &s, unordered_map<string, int> &freq)
    {
        int n = s.size();
        int l = 0, r = 0;
        while (r < n)
        {
            while (r < n && s[r] >= 'a' && s[r] <= 'z')
            {
                r++;
            }
            if (r - l > 0)
            {
                freq[s.substr(l, r - l)]++;
            }
            l = r + 1;
            r = l;
        }
    }
    vector<string> uncommonFromSentences(string s1, string s2)
    {
        unordered_map<string, int> freq;
        insert(s1, freq);
        insert(s2, freq);

        vector<string> ans;
        for (auto &p : freq)
        {
            if (p.second == 1)
            {
                ans.push_back(p.first);
            }
        }

        return ans;
    }
};
// @lc code=end
