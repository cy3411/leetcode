# 题目

有 `n` 个人被分成数量未知的组。每个人都被标记为一个从 `0` 到 `n - 1` 的唯一 ID 。

给定一个整数数组 `groupSizes` ，其中 `groupSizes[i]` 是第 `i` 个人所在的组的大小。例如，如果 `groupSizes[1] = 3` ，则第 `1` 个人必须位于大小为 `3` 的组中。

返回一个组列表，使每个人 `i` 都在一个大小为 `groupSizes[i]` 的组中。

每个人应该 **恰好只** 出现在 一个组 中，并且每个人必须在一个组中。如果有多个答案，返回其中 **任何 一个**。可以 **保证** 给定输入 **至少有一个** 有效的解。

提示：

- $groupSizes.length \equiv n$
- $1 \leq n \leq 500$
- $1 \leq groupSizes[i] \leq n$

# 示例

```
输入：groupSizes = [3,3,3,3,3,1,3]
输出：[[5],[0,1,2],[3,4,6]]
解释：
第一组是 [5]，大小为 1，groupSizes[5] = 1。
第二组是 [0,1,2]，大小为 3，groupSizes[0] = groupSizes[1] = groupSizes[2] = 3。
第三组是 [3,4,6]，大小为 3，groupSizes[3] = groupSizes[4] = groupSizes[6] = 3。
其他可能的解决方案有 [[2,1,6],[5],[0,4,3]] 和 [[5],[0,6,2],[4,3,1]]。
```

# 题解

## 哈希表

题目保证至少有一个有效的解，因此对于数组元素 x ，那么它出现的次数一定能被 x 整除。

使用哈希表，元素出现的次数作为 key ， 元素所在的列表作为 value。

最后遍历哈希表，将对应的列表切割成满足题意的组即可。

```js
function groupThePeople(groupSizes: number[]): number[][] {
  const groups = new Map<number, number[]>();
  const n = groupSizes.length;
  // 使用哈希表，key为组的大小，value为组的索引列表
  for (let i = 0; i < n; i++) {
    const size = groupSizes[i];
    if (!groups.has(size)) {
      groups.set(size, []);
    }
    groups.get(size)!.push(i);
  }

  const ans: number[][] = [];
  // 遍历哈希表，将每个组的索引放入答案中
  for (const [size, group] of groups.entries()) {
    const groupSize = group.length / size;
    for (let i = 0; i < groupSize; i++) {
      const offset = i * size;
      const temp = [];
      for (let j = 0; j < size; j++) {
        const idx: number = offset + j;
        temp.push(group[idx]);
      }
      ans.push(temp);
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<vector<int>> groupThePeople(vector<int> &groupSizes) {
        unordered_map<int, vector<int>> m;
        int n = groupSizes.size();
        for (int i = 0; i < n; i++) {
            m[groupSizes[i]].push_back(i);
        }

        vector<vector<int>> ans;
        for (auto &x : m) {
            int groupSize = x.second.size() / x.first;
            for (int i = 0; i < groupSize; i++) {
                vector<int> group(x.first);
                for (int j = 0; j < x.first; j++) {
                    group[j] = x.second[i * x.first + j];
                }
                ans.push_back(group);
            }
        }

        return ans;
    }
};
```
