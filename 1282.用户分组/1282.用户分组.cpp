/*
 * @lc app=leetcode.cn id=1282 lang=cpp
 *
 * [1282] 用户分组
 */

// @lc code=start
class Solution {
public:
    vector<vector<int>> groupThePeople(vector<int> &groupSizes) {
        unordered_map<int, vector<int>> m;
        int n = groupSizes.size();
        for (int i = 0; i < n; i++) {
            m[groupSizes[i]].push_back(i);
        }

        vector<vector<int>> ans;
        for (auto &x : m) {
            int groupSize = x.second.size() / x.first;
            for (int i = 0; i < groupSize; i++) {
                vector<int> group(x.first);
                for (int j = 0; j < x.first; j++) {
                    group[j] = x.second[i * x.first + j];
                }
                ans.push_back(group);
            }
        }

        return ans;
    }
};
// @lc code=end
