# 题目

设计一个支持在平均 时间复杂度 O(1) 下，执行以下操作的数据结构。

- insert(val)：当元素 val 不存在时，向集合中插入该项。
- remove(val)：元素 val 存在时，从集合中移除该项。
- getRandom：随机返回现有集合中的一项。每个元素应该有相同的概率被返回。

# 示例

```
// 初始化一个空的集合。
RandomizedSet randomSet = new RandomizedSet();

// 向集合中插入 1 。返回 true 表示 1 被成功地插入。
randomSet.insert(1);

// 返回 false ，表示集合中不存在 2 。
randomSet.remove(2);

// 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
randomSet.insert(2);

// getRandom 应随机返回 1 或 2 。
randomSet.getRandom();

// 从集合中移除 1 ，返回 true 。集合现在包含 [2] 。
randomSet.remove(1);

// 2 已在集合中，所以返回 false 。
randomSet.insert(2);

// 由于 2 是集合中唯一的数字，getRandom 总是返回 2 。
randomSet.getRandom();
```

# 题解

## 哈希表

题目需要所有操作的时间复杂度都是 O(1)，肯定考虑哈希表。

唯一要注意的是 getRandom，这里是用数组实现，数组只能对队尾的元素的插入和删除有 O(1)的时间复杂度，所以 insert 和 remove 的时候，需要从队尾操作。(push 和 pop)

```ts
class RandomizedSet {
  data: Map<number, number>;
  rand: number[];
  constructor() {
    this.data = new Map();
    this.rand = [];
  }

  insert(val: number): boolean {
    if (this.data.has(val)) return false;
    this.data.set(val, this.rand.length);
    this.rand.push(val);
    return true;
  }

  remove(val: number): boolean {
    if (!this.data.has(val)) return false;
    let idx = this.data.get(val);
    let m = this.rand.length - 1;
    // 将需要删除的元素交换到数组末尾
    [this.rand[m], this.rand[idx]] = [this.rand[idx], this.rand[m]];
    // 交换完，要将哈希中的下标位置更新
    this.data.set(this.rand[m], m);
    this.data.set(this.rand[idx], idx);
    // 删除
    this.data.delete(val);
    this.rand.pop();
    return true;
  }

  getRandom(): number {
    let idx = (this.rand.length * Math.random()) >> 0;
    return this.rand[idx];
  }
}
```
