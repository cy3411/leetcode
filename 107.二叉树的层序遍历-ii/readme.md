# 题目
给定一个二叉树，返回其节点值自底向上的层次遍历。 （即按从叶子节点所在层到根节点所在的层，逐层从左向右遍历）

# 示例
```
给定二叉树 [3,9,20,null,null,15,7]:
    3
   / \
  9  20
    /  \
   15   7

返回其自底向上的层次遍历为：
[
  [15,7],
  [9,20],
  [3]
]
```

# 方法
## 递归
正常层序遍历获取结果，然后将结果反转就可以了。
```js
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrderBottom = function (root) {
  // 获取当前层次的节点，并放入到结果中
  const getResult = (root, k, result) => {
    if (root === null) return;
    // 判断当前层次是否有初始空数组
    if (k === result.length) result.push([]);
    result[k].push(root.val);

    getResult(root.left, k + 1, result);
    getResult(root.right, k + 1, result);
  };
  // 翻转结果
  const reverse = (result) => {
    for (let i = 0, j = result.length - 1; i < j; i++, j--) {
      [result[i], result[j]] = [result[j], result[i]];
    }
  };

  const result = [];
  getResult(root, 0, result);
  reverse(result);
  return result;
};
```

## 迭代 BFS
```js
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrderBottom = function (root) {
  // BFS，按层存储结果
  const getResult = (root, result) => {
    const queue = [root];
    while (queue.length) {
      let size = queue.length;
      result.push([]);
      while (--size >= 0) {
        let node = queue.shift();
        result[result.length - 1].push(node.val);
        if (node.left) queue.push(node.left);
        if (node.right) queue.push(node.right);
      }
    }
  };
  // 翻转结果
  const reverse = (result) => {
    for (let i = 0, j = result.length - 1; i < j; i++, j--) {
      [result[i], result[j]] = [result[j], result[i]];
    }
  };

  const result = [];

  if (root === null) return result;

  getResult(root, result);
  reverse(result);
  return result;
};
```