# 题目

给定一个头结点为 root 的链表, 编写一个函数以将链表分隔为 k 个连续的部分。

每部分的长度应该尽可能的相等: 任意两部分的长度差距不能超过 1，也就是说可能有些部分为 null。

这 k 个部分应该按照在链表中出现的顺序进行输出，并且排在前面的部分的长度应该大于或等于后面的长度。

返回一个符合上述规则的链表的列表。

举例： 1->2->3->4, k = 5 // 5 结果 [ [1], [2], [3], [4], null ]

提示:

- root 的长度范围： [0, 1000].
- 输入的每个节点的大小范围：[0, 999].
- k 的取值范围： [1, 50].

# 示例

```
输入:
root = [1, 2, 3], k = 5
输出: [[1],[2],[3],[],[]]
解释:
输入输出各部分都应该是链表，而不是数组。
例如, 输入的结点 root 的 val= 1, root.next.val = 2, \root.next.next.val = 3, 且 root.next.next.next = null。
第一个输出 output[0] 是 output[0].val = 1, output[0].next = null。
最后一个元素 output[4] 为 null, 它代表了最后一个部分为空链表。
```

# 方法

先计算出链表节点的数量，定义`items`为分组中节点的数量，定义`reaminder`为余数，需要分摊到前面的分组里。

循环分组，每次找 items+1 个 reaminder 数量的节点，放入分组中，将头节点后移，然后通过当前位置把原链表断开。

```js
/**
 * @param {ListNode} root
 * @param {number} k
 * @return {ListNode[]}
 */
var splitListToParts = function (root, k) {
  // 计算链表长度
  const getLinkedSize = (head) => {
    let size = 0;
    let temp = head;
    while (temp) {
      temp = temp.next;
      size++;
    }
    return size;
  };

  let result = new Array(k);
  let size = getLinkedSize(root);
  // 每组的节点数
  let items = (size / k) >> 0;
  // 余数节点
  let remainder = size % k;
  // 记录余数使用数量
  let m = 0;
  // 虚拟头节点
  const dummy = new ListNode(-1);
  dummy.next = root;
  let prev = dummy;
  //
  for (let i = 0; i < k; i++) {
    let curr = prev;
    // 计算每组几个节点
    let j = 0;
    while (j++ < items) {
      curr = curr ? curr.next : null;
    }
    // 余数的节点分给前面的组
    if (m++ < remainder) {
      curr = curr ? curr.next : null;
    }
    // 存入结果
    result[i] = prev.next;
    // 头指针后移
    prev.next = curr.next;
    // 将链表断开
    if (curr) curr.next = null;
  }

  return result;
};
```

```ts
function splitListToParts(head: ListNode | null, k: number): Array<ListNode | null> {
  // 特判
  if (head === null) return new Array(k).fill(null);

  let n = 0;
  // 链表指针，当前指向的位置
  let p: ListNode | null = head;
  // 计算链表长度
  while (p !== null) {
    p = p.next;
    n++;
  }

  let avg = (n / k) >> 0;
  let mod = n % k;

  // 返回分割后的链表头，并重置p指针位置为下一组第一个节点
  const countLink = (k: number): ListNode | null => {
    if (p === null) return null;
    let head = p;
    let q = p;
    while (q && --k) {
      q = q.next;
    }

    if (q) {
      p = q.next;
      q.next = null;
    }
    return head;
  };
  let ans: Array<ListNode | null> = [];
  p = head;
  // 分隔链表
  for (let i = 0; i < k; i++) {
    let tmp = avg;
    if (mod > 0) {
      tmp++;
      mod--;
    }
    const res = countLink(tmp);
    ans.push(res);
  }

  return ans;
}
```
