/*
 * @lc app=leetcode.cn id=2028 lang=typescript
 *
 * [2028] 找出缺失的观测数据
 */

// @lc code=start
function missingRolls(rolls: number[], mean: number, n: number): number[] {
  const m = rolls.length;
  // 总的观测数
  let missNumber = mean * (m + n);
  // 计算遗失的观测数据
  for (const x of rolls) {
    missNumber -= x;
  }

  const ans = [];
  // 数值不在n个6面骰子范围内
  if (missNumber < n || missNumber > n * 6) {
    return ans;
  }

  // 将missNumber分解为n个值
  const q = (missNumber / n) >> 0;
  // 剩余的数值
  const r = missNumber % n;
  // 将剩余的值分散到每个骰子上
  for (let i = 0; i < n; i++) {
    ans[i] = q + (i < r ? 1 : 0);
  }

  return ans;
}
// @lc code=end
