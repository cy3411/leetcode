# 题目

现有一份 `n + m` 次投掷单个 **六面** 骰子的观测数据，骰子的每个面从 `1` 到 `6` 编号。观测数据中缺失了 `n` 份，你手上只拿到剩余 `m` 次投掷的数据。幸好你有之前计算过的这 `n + m` 次投掷数据的 **平均值** 。

给你一个长度为 `m` 的整数数组 `rolls` ，其中 `rolls[i]` 是第 `i` 次观测的值。同时给你两个整数 `mean` 和 `n` 。

返回一个长度为 `n` 的数组，包含所有缺失的观测数据，且满足这 `n + m` 次投掷的 **平均值** 是 `mean` 。如果存在多组符合要求的答案，只需要返回其中任意一组即可。如果不存在答案，返回一个空数组。

`k` 个数字的 **平均值** 为这些数字求和后再除以 `k` 。

注意 `mean` 是一个整数，所以 `n + m` 次投掷的总和需要被 `n + m` 整除。

提示：

- $m \equiv rolls.length$
- $1 \leq n, m \leq 10^5$
- $1 \leq rolls[i], mean \leq 6$

# 示例

```
输入：rolls = [3,2,4,3], mean = 4, n = 2
输出：[6,6]
解释：所有 n + m 次投掷的平均值是 (3 + 2 + 4 + 3 + 6 + 6) / 6 = 4 。
```

```
输入：rolls = [1,5,6], mean = 3, n = 4
输出：[2,3,2,2]
解释：所有 n + m 次投掷的平均值是 (1 + 5 + 6 + 2 + 3 + 2 + 2) / 7 = 3 。
```

# 题解

## 模拟

根据题意可知，总的观察数据为 `n + m`个， `mean` 为平均值，因此所有观察数据的总和为 `(n + m) * mean` 。

根据所有观察数的总和和数组 rolls 中 m 个观察数据，那么可知已缺失的 n 个观察数据总和，记作 `missNumber`。

将 `missNumber` 分解为 `n` 个值，分别为缺失的观察数据，剩余的数值分摊到每个组中。

需要注意一点，当 `missNumber` 不在 `n` 个骰子的范围内时，说明缺失的观察数据不在骰子的范围内，返回空数组。

```ts
function missingRolls(rolls: number[], mean: number, n: number): number[] {
  const m = rolls.length;
  // 总的观测数
  let missNumber = mean * (m + n);
  // 计算遗失的观测数据
  for (const x of rolls) {
    missNumber -= x;
  }

  const ans = [];
  // 数值不在n个6面骰子范围内
  if (missNumber < n || missNumber > n * 6) {
    return ans;
  }

  // 将missNumber分解为n个值
  const q = (missNumber / n) >> 0;
  // 剩余的数值
  const r = missNumber % n;
  // 将剩余的值分散到每个骰子上
  for (let i = 0; i < n; i++) {
    ans[i] = q + (i < r ? 1 : 0);
  }

  return ans;
}
```
