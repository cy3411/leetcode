# 题目
你的面前有一堵矩形的、由 `n` 行砖块组成的砖墙。这些砖块高度相同（也就是一个单位高）但是宽度不同。每一行砖块的宽度之和应该相等。

你现在要画一条 **自顶向下** 的、穿过 **最少** 砖块的垂线。如果你画的线只是从砖块的边缘经过，就不算穿过这块砖。**你不能沿着墙的两个垂直边缘之一画线，这样显然是没有穿过一块砖的。**

给你一个二维数组 wall ，该数组包含这堵墙的相关信息。其中，`wall[i]` 是一个代表从左至右每块砖的宽度的数组。你需要找出怎样画才能使这条线 **穿过的砖块数量最少** ，并且返回 **穿过的砖块数量** 。


# 示例
![geGYoF.png](https://z3.ax1x.com/2021/05/02/geGYoF.png)
```
输入：wall = [[1,2,2,1],[3,1,2],[1,3,2],[2,4],[3,1,2],[1,3,1,1]]
输出：2
```
# 题解
砖墙的高度的是固定的，最少穿过砖块的数量，也就是砖墙的高度减去最多穿过缝隙的数量。

遍历每一行，用`hashMap`记录每行缝隙出现的次数。

这里使用前缀和记录每次累加后值，也就是缝隙出现的位置。  
由于不能从墙的两边画线，所以最后一块砖不需要统计。

最后返回砖墙的高度减去最多穿过缝隙的数量的结果。
## hash+前缀和
```js
function leastBricks(wall: number[][]): number {
  // 记录每行砖块的宽度前缀和，也就是缝隙的位置
  const map: Map<number, number> = new Map();
  for (const width of wall) {
    let sum = 0;
    // 遍历每行砖块宽度，计算前缀和，加入到map中
    // 题目给出不能沿着墙的2边划线，所以最后一块砖不需要遍历
    for (let i = 0; i < width.length - 1; i++) {
      sum += width[i];
      map.set(sum, (map.get(sum) || 0) + 1);
    }
  }

  let maxPrefixSum = 0;
  for (const [_, val] of map.entries()) {
    maxPrefixSum = Math.max(maxPrefixSum, val);
  }
  // 墙的高度减去出现次数最多的前缀和就是结果
  return wall.length - maxPrefixSum;
}
```