# 题目

给定两个字符串 `a` 和 `b`，寻找重复叠加字符串 `a` 的最小次数，使得字符串 `b` 成为叠加后的字符串 `a` 的子串，如果不存在则返回 `-1`。

注意：字符串 `"abc"` 重复叠加 0 次是 `""`，重复叠加 1 次是 `"abc"`，重复叠加 2 次是 `"abcabc"`。

提示：

- $\color{goldenrod}1 \leq a.length \leq 10^4$
- $\color{goldenrod}1 \leq b.length \leq 10^4$
- `a` 和 `b` 由小写英文字母组成

# 示例

```
输入：a = "abcd", b = "cdabcdab"
输出：3
解释：a 重复叠加三遍后为 "abcdabcdabcd", 此时 b 是其子串。
```

```
输入：a = "a", b = "aa"
输出：2
```

# 题解

## KMP 算法

题意就是在重复的字符串中找到子串，这里的子串是指重复的字符串的子串。

使用 KMP 算法，只是在匹配的过程中，需要需要对 a 中的字符进行取模，达到重复字符串的目的。

直到找到匹配字串的位置或者重复字串的长度大于 b 的长度，这时候就可以结束了。、

最后返回需要重复的次数。就是 b 的长度加上匹配索引除以 a 的长度。

```ts
function repeatedStringMatch(a: string, b: string): number {
  // next数组
  const getNext = (patten: string): number[] => {
    const m = patten.length;
    const res: number[] = new Array(m).fill(-1);
    for (let i = 1, j = -1; i < m; i++) {
      while (j !== -1 && patten[j + 1] !== patten[i]) {
        j = res[j];
      }
      if (patten[j + 1] === patten[i]) {
        j++;
      }
      res[i] = j;
    }
    return res;
  };
  // KMP算法
  const kpm = (text: string, patten: string): number => {
    const next = getNext(patten);
    const n = text.length;
    const m = patten.length;
    // i和j的差值不能超过n
    for (let i = 0, j = -1; i - (j + 1) < n; i++) {
      while (j !== -1 && text[i % n] !== patten[j + 1]) {
        j = next[j];
      }
      if (text[i % n] === patten[j + 1]) {
        j++;
      }
      console.log(i, j);
      if (j === m - 1) {
        return i - j;
      }
    }
    return -1;
  };

  const n = a.length;
  const m = b.length;
  const index = kpm(a, b);

  if (index === -1) {
    return -1;
  }
  // 返回结果
  return Math.ceil((m + index) / n);
}
```
