# 题目

Alice 手中有一把牌，她想要重新排列这些牌，分成若干组，使每一组的牌数都是 `groupSize` ，并且由 `groupSize` 张连续的牌组成。

给你一个整数数组 `hand` 其中 `hand[i]` 是写在第 `i` 张牌，和一个整数 `groupSize` 。如果她可能重新排列这些牌，返回 `true` ；否则，返回 `false` 。

提示：

- $\color{burlywood}1 \leq hand.length \leq 10^4$
- $\color{burlywood}0 \leq hand[i] \leq 10^9$
- $\color{burlywood}1 \leq groupSize \leq hand.length$

# 示例

```
输入：hand = [1,2,3,6,2,3,4,7,8], groupSize = 3
输出：true
解释：Alice 手中的牌可以被重新排列为 [1,2,3]，[2,3,4]，[6,7,8]。
```

```
输入：hand = [1,2,3,4,5], groupSize = 4
输出：false
解释：Alice 手中的牌无法被重新排列成几个大小为 4 的组。
```

# 题解

## 贪心算法

题目要求将 hand 中的牌分组，使得每组的数量为 groupSize。假设 n 为 hand 的长度，只有当 $\color{burlywood}n \mod groupSize \neq 0$ 才可以完成分组。因此 $\color{burlywood}n \mod groupSize \neq 0$ 时，直接返回 false 。

每张牌都需要分到不同的组中，因此可以用贪心算法。假设 x 为当前未分组的牌中的最小数字，如果存在符合分组要求的，那么 x 一定是某个组中的最小数字，且该组的数字范围在 $\color{burlywood}[x, x+groupSize-1]$ 中。

x 完成分组后，继续使用该策略对为分组的牌分组，直到分组结束或者无法继续完成分组。

- 对 hand 升序排序，保证最小的数字遍历到
- 将 hand 中的数字出现的次数保存到哈希表
- 遍历 hand ，对每个数字计算是否有顺子

```ts
function isNStraightHand(hand: number[], groupSize: number): boolean {
  const n = hand.length;
  if (n % groupSize !== 0) return false;
  // 排序
  hand.sort((a, b) => a - b);
  // 将所有数字的出现次数放入哈希表
  const hash: Map<number, number> = new Map();
  for (const num of hand) {
    hash.set(num, (hash.get(num) ?? 0) + 1);
  }
  // 计算是否有顺子
  for (const num of hand) {
    // 已经被其他顺子使用了，继续遍历其他数字
    if (!hash.has(num)) continue;
    // 连续的数字，区间是[num, num+groupSize-1]
    for (let i = 0; i < groupSize; i++) {
      const x = num + i;
      if (!hash.has(x)) return false;
      hash.set(x, hash.get(x) - 1);
      // 移除无效的哈希
      if (hash.get(x) === 0) hash.delete(x);
    }
  }

  return true;
}
```
