/*
 * @lc app=leetcode.cn id=846 lang=typescript
 *
 * [846] 一手顺子
 */

// @lc code=start
function isNStraightHand(hand: number[], groupSize: number): boolean {
  const n = hand.length;
  if (n % groupSize !== 0) return false;
  // 排序
  hand.sort((a, b) => a - b);
  // 将所有数字的出现次数放入哈希表
  const hash: Map<number, number> = new Map();
  for (const num of hand) {
    hash.set(num, (hash.get(num) ?? 0) + 1);
  }
  // 计算是否有顺子
  for (const num of hand) {
    // 已经被其他顺子使用了，继续遍历其他数字
    if (!hash.has(num)) continue;
    // 连续的数字，区间是[num, num+groupSize-1]
    for (let i = 0; i < groupSize; i++) {
      const x = num + i;
      if (!hash.has(x)) return false;
      hash.set(x, hash.get(x) - 1);
      // 移除无效的哈希
      if (hash.get(x) === 0) hash.delete(x);
    }
  }

  return true;
}
// @lc code=end
