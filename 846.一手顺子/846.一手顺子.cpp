
/*
 * @lc app=leetcode.cn id=846 lang=cpp
 *
 * [846] 一手顺子
 */

// @lc code=start

class Solution
{
public:
    bool isNStraightHand(vector<int> &hand, int groupSize)
    {
        int n = hand.size();
        if (n % groupSize != 0)
            return false;

        sort(hand.begin(), hand.end());

        unordered_map<int, int> hash;
        for (auto x : hand)
        {

            hash[x]++;
        }

        for (auto x : hand)
        {
            if (hash[x] == 0)
                continue;

            for (int i = 0; i < groupSize; i++)
            {
                if (hash[x + i] == 0)
                    return false;
                hash[x + i]--;
            }
        }

        return true;
    }
};
// @lc code=end
