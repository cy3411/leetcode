# 题目

有一个 `m × n` 的长方形岛屿，与 **太平洋** 和 **大西洋** 相邻。 “太平洋” 处于大陆的左边界和上边界，而 “大西洋” 处于大陆的右边界和下边界。

这个岛被分割成一个个方格网格。给定一个 `m x n` 的整数矩阵 `heights` ， `heights[r][c]` 表示坐标 `(r, c)` 上单元格 高于海平面的高度 。

岛上雨水较多，如果相邻小区的高度 **小于或等于** 当前小区的高度，雨水可以直接向北、南、东、西流向相邻小区。水可以从海洋附近的任何细胞流入海洋。

**返回** 网格坐标 **result** 的 **2D** 列表 ，其中 `result[i] = [ri, ci]` 表示雨水可以从单元格 `(ri, ci)` 流向 **太平洋和大西洋** 。

提示：

- $m \equiv heights.length$
- $n \equiv heights[r].length$
- $1 \leq m, n \leq 200$
- $0 \leq heights[r][c] \leq 10^5$

# 示例

[![bBpmgs.png](https://s4.ax1x.com/2022/03/05/bBpmgs.png)](https://imgtu.com/i/bBpmgs)

```
输入: heights = [[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]
输出: [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]
```

# 题解

## 深度优先搜索

题意就是找到岛屿上的某几个点，这几点的雨水可以同时流向太平洋和大西洋。

正常搜索的话，需要遍历每一个点，来判断当前点是否满足题意，复杂度较高。

我们可以反过来搜索，从终点出发，去找到和终点能连通的最高点。如果这个最高点同时跟太平洋和大西洋相连，那么这个点就是答案。

```ts
function pacificAtlantic(heights: number[][]): number[][] {
  const m = heights.length;
  const n = heights[0].length;
  const dirs = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  // 标记访问过的点
  const visited = Array(m)
    .fill(0)
    .map(() => Array(n).fill(0));

  // type表示 1: pacific, 2: atlantic
  const dfs = (i: number, j: number, type: number) => {
    // 表示已经被当前type的洋流访问过了
    if ((visited[i][j] & type) !== 0) return;
    visited[i][j] |= type;
    // 两个洋流都能访问到，放入答案中
    if (visited[i][j] === 3) {
      ans.push([i, j]);
    }
    for (const [di, dj] of dirs) {
      const x = i + di;
      const y = j + dj;
      // 越界
      if (x < 0 || x >= m || y < 0 || y >= n) continue;
      // 我们是从终点往起点搜索，所以是从低向高找
      if (heights[x][y] < heights[i][j]) continue;
      dfs(x, y, type);
    }
  };
  const ans: number[][] = [];

  // 从每个边界点开始深搜
  for (let i = 0; i < m; i++) {
    // 第一列
    dfs(i, 0, 1);
    // 最后一列
    dfs(i, n - 1, 2);
  }
  for (let j = 0; j < n; j++) {
    // 第一行
    dfs(0, j, 1);
    // 最后一行
    dfs(m - 1, j, 2);
  }

  return ans;
}
```

## 广度优先搜索

```cpp
class Solution
{
public:
    struct Point
    {
        int x, y, type;
        Point(int x, int y, int type) : x(x), y(y), type(type) {}
    };
    int visited[205][205] = {0};
    int dirs[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    vector<vector<int>> pacificAtlantic(vector<vector<int>> &heights)
    {
        int m = heights.size(), n = heights[0].size();
        vector<vector<int>> ans;
        queue<Point> q;
        for (int i = 0; i < m; i++)
        {
            q.push(Point(i, 0, 1));
            visited[i][0] |= 1;
            q.push(Point(i, n - 1, 2));
            visited[i][n - 1] |= 2;
        }
        for (int j = 0; j < n; j++)
        {
            q.push(Point(0, j, 1));
            visited[0][j] |= 1;
            q.push(Point(m - 1, j, 2));
            visited[m - 1][j] |= 2;
        }

        while (!q.empty())
        {
            Point curr = q.front();
            q.pop();
            // 向四个方向搜索
            for (auto [dx, dy] : dirs)
            {
                int x = curr.x + dx, y = curr.y + dy;
                if (x < 0 || x >= m || y < 0 || y >= n || (visited[x][y] & curr.type) != 0)
                {
                    continue;
                }
                if (heights[x][y] < heights[curr.x][curr.y])
                {
                    continue;
                }
                visited[x][y] |= curr.type;
                q.push(Point(x, y, curr.type));
            }
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (visited[i][j] == 3)
                {
                    ans.push_back({i, j});
                }
            }
        }
        return ans;
    }
};
```
