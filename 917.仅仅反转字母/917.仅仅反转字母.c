/*
 * @lc app=leetcode.cn id=917 lang=c
 *
 * [917] 仅仅反转字母
 */

// @lc code=start

char *reverseOnlyLetters(char *s)
{
    int len = strlen(s);
    int i = 0, j = len - 1;
    while (i < j)
    {
        while (i < j && !isalpha(s[i]))
        {
            i++;
        }
        while (i < j && !isalpha(s[j]))
        {
            j--;
        }
        if (i < j)
        {
            char temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            i++;
            j--;
        }
    }
    return s;
}
// @lc code=end
