# 题目

给你一个字符串 `s` ，根据下述规则反转字符串：

- 所有非英文字母保留在原有位置。
- 所有英文字母（小写或大写）位置反转。

返回反转后的 `s` 。

提示

- $1 \leq s.length \leq 100$
- `s` 仅由 `ASCII` 值在范围 `[33, 122]` 的字符组成
- `s` 不含 `'\"'` 或 `'\\'`

# 示例

```
输入：s = "ab-cd"
输出："dc-ba"
```

```
输入：s = "a-bC-dEf-ghIj"
输出："j-Ih-gfE-dCba"
```

# 题解

## 双指针

定义双指针指向字符串的首尾，如果指针指向的字符不是字母，则移动到下一个指针，如果是字母，则交换指针指向的字符。

```ts
function reverseOnlyLetters(s: string): string {
  let i = 0;
  let j = s.length - 1;
  const arr = s.split('');
  const isLetter = (c: string) => {
    const code = c.charCodeAt(0);
    return (code >= 65 && code <= 90) || (code >= 97 && code <= 122);
  };
  while (i < j) {
    if (!isLetter(arr[i])) {
      i++;
    } else if (!isLetter(arr[j])) {
      j--;
    } else {
      [arr[i], arr[j]] = [arr[j], arr[i]];
      i++;
      j--;
    }
  }

  return arr.join('');
}
```

```cpp
char *reverseOnlyLetters(char *s)
{
    int len = strlen(s);
    int i = 0, j = len - 1;
    while (i < j)
    {
        while (i < j && !isalpha(s[i]))
        {
            i++;
        }
        while (i < j && !isalpha(s[j]))
        {
            j--;
        }
        if (i < j)
        {
            char temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            i++;
            j--;
        }
    }
    return s;
}
```

```cpp
char *reverseOnlyLetters(char *s)
{
    int len = strlen(s);
    int i = 0, j = len - 1;
    while (i < j)
    {
        while (i < j && !isalpha(s[i]))
        {
            i++;
        }
        while (i < j && !isalpha(s[j]))
        {
            j--;
        }
        if (i < j)
        {
            char temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            i++;
            j--;
        }
    }
    return s;
}
```
