/*
 * @lc app=leetcode.cn id=917 lang=typescript
 *
 * [917] 仅仅反转字母
 */

// @lc code=start
function reverseOnlyLetters(s: string): string {
  let i = 0;
  let j = s.length - 1;
  const arr = s.split('');
  const isLetter = (c: string) => {
    const code = c.charCodeAt(0);
    return (code >= 65 && code <= 90) || (code >= 97 && code <= 122);
  };
  while (i < j) {
    if (!isLetter(arr[i])) {
      i++;
    } else if (!isLetter(arr[j])) {
      j--;
    } else {
      [arr[i], arr[j]] = [arr[j], arr[i]];
      i++;
      j--;
    }
  }

  return arr.join('');
}
// @lc code=end
