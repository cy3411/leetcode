/*
 * @lc app=leetcode.cn id=384 lang=typescript
 *
 * [384] 打乱数组
 */

// @lc code=start
class Solution {
  m: number;
  constructor(public nums: number[]) {
    this.nums = nums;
    this.m = nums.length;
  }

  reset(): number[] {
    return this.nums;
  }

  shuffle(): number[] {
    const shuffle = [...this.nums];
    for (let i = 0; i < this.m; i++) {
      const idx = (this.m * Math.random()) >> 0;
      [shuffle[i], shuffle[idx]] = [shuffle[idx], shuffle[i]];
    }
    return shuffle;
  }
}

/**
 * Your Solution object will be instantiated and called as such:
 * var obj = new Solution(nums)
 * var param_1 = obj.reset()
 * var param_2 = obj.shuffle()
 */
// @lc code=end
