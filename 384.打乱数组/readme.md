# 题目

给你一个整数数组 nums ，设计算法来打乱一个没有重复元素的数组。

实现 Solution class:

- Solution(int[] nums) 使用整数数组 nums 初始化对象
- int[] reset() 重设数组到它的初始状态并返回
- int[] shuffle() 返回数组随机打乱后的结果

提示：

- 1 <= nums.length <= 200
- -106 <= nums[i] <= 106
- nums 中的所有元素都是 唯一的
- 最多可以调用 5 \* 104 次 reset 和 shuffle

# 示例

```
输入
["Solution", "shuffle", "reset", "shuffle"]
[[[1, 2, 3]], [], [], []]
输出
[null, [3, 1, 2], [1, 2, 3], [1, 3, 2]]

解释
Solution solution = new Solution([1, 2, 3]);
solution.shuffle();    // 打乱数组 [1,2,3] 并返回结果。任何 [1,2,3]的排列返回的概率应该相同。例如，返回 [3, 1, 2]
solution.reset();      // 重设数组到它的初始状态 [1, 2, 3] 。返回 [1, 2, 3]
solution.shuffle();    // 随机返回数组 [1, 2, 3] 打乱后的结果。例如，返回 [1, 3, 2]
```

# 题解

## 数组

reset:返回数组的初始状态
shuffle:复制数组，然后打乱后返回

```ts
class Solution {
  m: number;
  constructor(public nums: number[]) {
    this.nums = nums;
    this.m = nums.length;
  }

  reset(): number[] {
    return this.nums;
  }

  shuffle(): number[] {
    const shuffle = [...this.nums];
    for (let i = 0; i < this.m; i++) {
      const idx = (this.m * Math.random()) >> 0;
      [shuffle[i], shuffle[idx]] = [shuffle[idx], shuffle[i]];
    }
    return shuffle;
  }
}
```
