# 题目

把字符串 `s` 看作是 `“abcdefghijklmnopqrstuvwxyz”` 的无限环绕字符串，所以 `s` 看起来是这样的：

- `"...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd...."` .

现在给定另一个字符串 `p` 。返回 `s` 中 **唯一** 的 `p` 的 **非空子串** 的数量 。

提示:

- $1 \leq p.length \leq 10^5$
- `p` 由小写英文字母构成

# 示例

```
输入: p = "a"
输出: 1
解释: 字符串 s 中只有一个"a"子字符。
```

```
输入: p = "cac"
输出: 2
解释: 字符串 s 中的字符串“cac”只有两个子串“a”、“c”。.
```

# 题解

## 动态规划

定义 dp[k] 为 s 中以 p[i] 为结尾的符合题意的最长子串的长度。知道了最长长度，也就知道了不同子串的个数。

最后的答案即为：

$$
\sum _{k='a'} ^{'z'} dp[k]
$$

```ts
function findSubstringInWraproundString(p: string): number {
  const dp = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  let max = 0;
  for (let i = 0; i < p.length; i++) {
    if (i > 0 && (p[i].charCodeAt(0) - p[i - 1].charCodeAt(0) + 26) % 26 === 1) {
      max++;
    } else {
      max = 1;
    }
    dp[p[i].charCodeAt(0) - base] = Math.max(dp[p[i].charCodeAt(0) - base], max);
  }

  return dp.reduce((a, b) => a + b);
}
```
