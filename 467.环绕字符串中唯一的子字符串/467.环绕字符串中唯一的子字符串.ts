/*
 * @lc app=leetcode.cn id=467 lang=typescript
 *
 * [467] 环绕字符串中唯一的子字符串
 */

// @lc code=start
function findSubstringInWraproundString(p: string): number {
  const dp = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  let max = 0;
  for (let i = 0; i < p.length; i++) {
    if (i > 0 && (p[i].charCodeAt(0) - p[i - 1].charCodeAt(0) + 26) % 26 === 1) {
      max++;
    } else {
      max = 1;
    }
    dp[p[i].charCodeAt(0) - base] = Math.max(dp[p[i].charCodeAt(0) - base], max);
  }

  return dp.reduce((a, b) => a + b);
}
// @lc code=end
