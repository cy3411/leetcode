/*
 * @lc app=leetcode.cn id=172 lang=typescript
 *
 * [172] 阶乘后的零
 */

// @lc code=start
function trailingZeroes(n: number): number {
  // 找有多少个因子为5
  let base = 5;
  let ans = 0;
  while ((n / base) >> 0) {
    ans += (n / base) >> 0;
    // 5的幂次方每次会多一个5，所以还要判断是否还有幂次方的因子
    base *= 5;
  }

  return ans;
}
// @lc code=end
