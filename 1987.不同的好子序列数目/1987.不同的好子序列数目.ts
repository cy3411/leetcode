/*
 * @lc app=leetcode.cn id=1987 lang=typescript
 *
 * [1987] 不同的好子序列数目
 */

// @lc code=start
function numberOfUniqueGoodSubsequences(binary: string): number {
  const n = binary.length;
  const modNum = 1e9 + 7;
  let dp0 = 0;
  let dp1 = 0;
  let hasZero = 0;
  for (let i = n - 1; i >= 0; i--) {
    if (binary[i] === '0') {
      hasZero = 1;
      dp0 = (dp0 + dp1 + 1) % modNum;
    } else {
      dp1 = (dp0 + dp1 + 1) % modNum;
    }
  }

  return (dp1 + hasZero) % modNum;
}
// @lc code=end
