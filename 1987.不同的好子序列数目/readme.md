# 题目

给你一个二进制字符串 `binary` 。 `binary` 的一个 **子序列** 如果是 **非空** 的且没有 **前导** `0` （除非数字是 `"0"` 本身），那么它就是一个 **好** 的子序列。

请你找到 `binary` **不同好子序列** 的数目。

- 比方说，如果 `binary = "001"` ，那么所有 **好** 子序列为 `["0", "0", "1"]` ，所以 **不同** 的好子序列为 `"0"` 和 `"1"` 。 注意，子序列 `"00"` ，`"01"` 和 `"001"` 不是好的，因为它们有前导 `0` 。

请你返回 `binary` 中 **不同好子序列** 的数目。由于答案可能很大，请将它对 $10^9 + 7$ 取余 后返回。

一个 **子序列** 指的是从原数组中删除若干个（可以一个也不删除）元素后，不改变剩余元素顺序得到的序列。

提示：

- $1 \leq binary.length \leq 10^5$
- `binary` 只含有 `'0'` 和 `'1'` 。

# 示例

```
输入：binary = "001"
输出：2
解释：好的二进制子序列为 ["0", "0", "1"] 。
不同的好子序列为 "0" 和 "1" 。
```

```
输入：binary = "11"
输出：2
解释：好的二进制子序列为 ["1", "1", "11"] 。
不同的好子序列为 "1" 和 "11" 。
```

# 题解

## 递推

定义状态：

- `f[i][0]` binary 从 i 开始的子串中，以 0 开头的子序列的个数
- `f[i][1]` binary 从 i 开始的子串中，以 1 开头的子序列的个数

状态转移：

我们逆序遍历 binary，当:

- `binary[i] = 1` 的时候，
  - `f[i][1]` 这样求解：
    - 这个 1 可以添加到之前所有子序列的前面，共有 $f[i+1][1]+f[i+1][0]$ 个不同子序列
    - 1 本身也是一个子序列，这里数量为 1
    - 还有之前以 1 开头的子序列个数 $f[i+1][1]$，但是这里跟前面的子序列之间重复了。
    - 综上，这里的结果是 $f[i+1][1]+f[i+1][0]+1$
  - `f[i][0]` 保持不变，$f[i][0]=f[i+1][0]$
- `binary[i] = 0` 的时候，跟上面刚好相反
  - $f[i][0] = f[i+1][1]+f[i+1][0]+1$
  - $f[i][1] = f[i+1][1]$

因为不允许有前导零，求得最终答案就是$f[0][1]$，如果 `binary` 中出现过 `0` 的话，需要在最终结果中加上 `1`，题目给出单独的 `0` 可以是一个好的子序列

```cpp
class Solution
{
public:
    int numberOfUniqueGoodSubsequences(string binary)
    {
        int n = binary.size(), mod_num = (int)(1e9 + 7);
        int f[n + 1][2], flag = 0;
        f[n][0] = f[n][1] = 0;

        for (int i = n - 1; i >= 0; i--)
        {
            if (binary[i] == '0')
            {
                flag = 1;
                f[i][0] = f[i + 1][0] + f[i + 1][1] + 1;
                f[i][1] = f[i + 1][1];
            }
            else
            {
                f[i][1] = f[i + 1][0] + f[i + 1][1] + 1;
                f[i][0] = f[i + 1][0];
            }
            f[i][0] %= mod_num;
            f[i][1] %= mod_num;
        }

        return f[0][1] + flag;
    }
};
```

```ts
function numberOfUniqueGoodSubsequences(binary: string): number {
  const n = binary.length;
  const modNum = 1e9 + 7;
  let dp0 = 0;
  let dp1 = 0;
  let hasZero = 0;
  for (let i = n - 1; i >= 0; i--) {
    if (binary[i] === '0') {
      hasZero = 1;
      dp0 = (dp0 + dp1 + 1) % modNum;
    } else {
      dp1 = (dp0 + dp1 + 1) % modNum;
    }
  }

  return (dp1 + hasZero) % modNum;
}
```
