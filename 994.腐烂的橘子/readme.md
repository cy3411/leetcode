# 题目

在给定的 `m x n` 网格 `grid` 中，每个单元格可以有以下三个值之一：

- 值 `0` 代表空单元格；
- 值 `1` 代表新鲜橘子；
- 值 `2` 代表腐烂的橘子。

每分钟，**腐烂的橘子** **周围 `4` 个方向上相邻** 的新鲜橘子都会腐烂。

返回 直到单元格中没有新鲜橘子为止所必须经过的最小分钟数。如果不可能，返回 `-1` 。

提示：

- $m \equiv grid.length$
- $n \equiv grid[i].length$
- $1 \leq m, n \leq 10$
- `grid[i][j]` 仅为 `0`、`1` 或 `2`

# 示例

[![b04NPU.png](https://s4.ax1x.com/2022/03/05/b04NPU.png)](https://imgtu.com/i/b04NPU)

```
输入：grid = [[2,1,1],[1,1,0],[0,1,1]]
输出：4
```

# 题解

## 广度优先搜索

题意就是网格中某个单元是否可达的问题，因此可以使用广度优先搜索。

将每个腐烂的橘子都放入队列中，并记录当前腐烂的橘子的位置，然后每次从队列中取出一个腐烂的橘子，并将周围的新鲜橘子改变状态放入队列中。直到队列为空，则说明所有的可达的点都被访问过了。

最后判断可达的点中是否包含所有的新鲜橘子，如果包含，则返回当前的时间，否则返回 -1。

```ts
interface Cell {
  i: number;
  j: number;
  step: number;
}

function orangesRotting(grid: number[][]): number {
  const m = grid.length;
  const n = grid[0].length;
  const queue: Cell[] = [];
  let fresh = 0;
  // 遍历所有的橘子，将腐烂的橘子入队，统计新鲜橘子数量
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 2) {
        queue.push({ i, j, step: 0 });
      } else if (grid[i][j] === 1) {
        fresh++;
      }
    }
  }

  const dirs = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  let ans = 0;
  while (queue.length) {
    const curr = queue.shift()!;
    ans = curr.step;
    // 向四个方向扩散，找到新鲜橘子
    for (const dir of dirs) {
      const i = curr.i + dir[0];
      const j = curr.j + dir[1];
      if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] !== 1) {
        continue;
      }
      grid[i][j] = 2;
      fresh--;
      queue.push({ i, j, step: curr.step + 1 });
    }
  }

  // 搜索完毕，如果还有新鲜橘子，说明不可达
  return fresh ? -1 : ans;
}
```

```cpp
class Solution
{
public:
    struct Cell
    {
        int i, j, step;
        Cell(int i, int j, int step) : i(i), j(j), step(step) {}
    };

    int orangesRotting(vector<vector<int>> &grid)
    {
        int m = grid.size(), n = grid[0].size(), fresh = 0, ans = 0;
        queue<Cell> q;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == 2)
                    q.push(Cell(i, j, 0));
                else if (grid[i][j] == 1)
                    fresh++;
            }
        }
        int dirs[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
        while (!q.empty())
        {
            Cell curr = q.front();
            q.pop();
            ans = curr.step;
            for (auto [di, dj] : dirs)
            {
                int i = curr.i + di, j = curr.j + dj;
                if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] != 1)
                {
                    continue;
                }
                grid[i][j] = 2;
                fresh--;
                q.push(Cell(i, j, curr.step + 1));
            }
        }

        return fresh == 0 ? ans : -1;
    }
};
```
