# 描述
给你一个链表和一个特定值 x ，请你对链表进行分隔，使得所有小于 x 的节点都出现在大于或等于 x 的节点之前。

你应当保留两个分区中每个节点的初始相对位置。

# 示例
```
输入：head = 1->4->3->2->5->2, x = 3
输出：1->2->2->4->3->5
```

# 方法
创建2个虚拟头节点，1个保存小于x的节点，1个保存大于等于x的节点，最后合并。
```js
var partition = function (head, x) {
  if (head === null) return head;

  const smallLink = new ListNode(-1, null);
  const largeLink = new ListNode(-1, null);
  let sp = smallLink;
  let lp = largeLink;

  let p = head;
  while (p) {
    let q = p.next;
    if (p.val < x) {
      p.next = sp.next;
      sp.next = p;
      sp = p;
    } else {
      p.next = lp.next;
      lp.next = p;
      lp = p;
    }
    p = q;
  }

  sp.next = largeLink.next;

  return smallLink.next;
};
```