#
# @lc app=leetcode.cn id=1403 lang=python3
#
# [1403] 非递增顺序的最小子序列
#

# @lc code=start


class Solution:
    def minSubsequence(self, nums: List[int]) -> List[int]:
        nums.sort(reverse=True)

        total = sum(nums)
        s = 0
        ans = []
        for num in nums:
            s += num
            ans.append(num)
            if (s > total - s):
                break
        return ans
# @lc code=end
