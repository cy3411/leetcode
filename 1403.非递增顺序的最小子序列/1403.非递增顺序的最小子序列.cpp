/*
 * @lc app=leetcode.cn id=1403 lang=cpp
 *
 * [1403] 非递增顺序的最小子序列
 */

// @lc code=start
class Solution {
public:
    vector<int> minSubsequence(vector<int> &nums) {
        sort(nums.begin(), nums.end(), greater<int>());

        int total = 0;
        for (auto x : nums) total += x;

        int sum = 0;
        vector<int> ans;
        for (auto x : nums) {
            sum += x;
            ans.push_back(x);
            if (sum > total - sum) break;
        }

        return ans;
    }
};
// @lc code=end
