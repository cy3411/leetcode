/*
 * @lc app=leetcode.cn id=1403 lang=typescript
 *
 * [1403] 非递增顺序的最小子序列
 */

// @lc code=start
function minSubsequence(nums: number[]): number[] {
  nums.sort((a, b) => b - a);

  let total = nums.reduce((a, b) => a + b, 0);

  const ans: number[] = [];
  let sum = 0;
  for (const num of nums) {
    sum += num;
    ans.push(num);
    // 子序列的和大于等于总和的一半，则不需要继续添加
    if (total - sum < sum) break;
  }

  return ans;
}
// @lc code=end
