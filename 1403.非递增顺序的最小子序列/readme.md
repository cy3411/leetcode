# 题目

给你一个数组 `nums`，请你从中抽取一个子序列，满足该子序列的元素之和 严格 大于未包含在该子序列中的各元素之和。

如果存在多个解决方案，只需返回 **长度最小** 的子序列。如果仍然有多个解决方案，则返回 **元素之和最大** 的子序列。

与子数组不同的地方在于，「数组的子序列」不强调元素在原数组中的连续性，也就是说，它可以通过从数组中分离一些（也可能不分离）元素得到。

注意，题目数据保证满足所有约束条件的解决方案是 **唯一** 的。同时，返回的答案应当按 **非递增顺序** 排列。

提示：

- $1 \leq nums.length \leq 500$
- $1 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [4,3,10,9,8]
输出：[10,9]
解释：子序列 [10,9] 和 [10,8] 是最小的、满足元素之和大于其他各元素之和的子序列。但是 [10,9] 的元素之和最大。
```

```
输入：nums = [4,4,7,6,7]
输出：[7,7,6]
解释：子序列 [7,7] 的和为 14 ，不严格大于剩下的其他元素之和（14 = 4 + 4 + 6）。因此，[7,6,7] 是满足题意的最小子序列。注意，元素按非递增顺序返回。
```

# 题解

## 贪心

题意要求子序列之和严格大于剩余元素之和，且满足要求的元素数量最小。

要满足以上条件，那么需要取出的元素要尽可能的大。所以使用贪心策略。

对 nums 排序，从大到小取出元素，直到取出的元素之和大于剩余元素之和。

```ts
function minSubsequence(nums: number[]): number[] {
  nums.sort((a, b) => b - a);

  let total = nums.reduce((a, b) => a + b, 0);

  const ans: number[] = [];
  let sum = 0;
  for (const num of nums) {
    sum += num;
    ans.push(num);
    // 子序列的和大于等于总和的一半，则不需要继续添加
    if (total - sum < sum) break;
  }

  return ans;
}
```

```cpp
class Solution {
public:
    vector<int> minSubsequence(vector<int> &nums) {
        sort(nums.begin(), nums.end(), greater<int>());

        int total = 0;
        for (auto x : nums) total += x;

        int sum = 0;
        vector<int> ans;
        for (auto x : nums) {
            sum += x;
            ans.push_back(x);
            if (sum > total - sum) break;
        }

        return ans;
    }
};
```

```py
class Solution:
    def minSubsequence(self, nums: List[int]) -> List[int]:
        nums.sort(reverse=True)

        total = sum(nums)
        s = 0
        ans = []
        for num in nums:
            s += num
            ans.append(num)
            if (s > total - s):
                break
        return ans
```
