# 题目

假如有一排房子，共 `n` 个，每个房子可以被粉刷成红色、蓝色或者绿色这三种颜色中的一种，你需要粉刷所有的房子并且使其相邻的两个房子颜色不能相同。

当然，因为市场上不同颜色油漆的价格不同，所以房子粉刷成不同颜色的花费成本也是不同的。每个房子粉刷成不同颜色的花费是以一个 `n x 3` 的正整数矩阵 `costs` 来表示的。

例如，`costs[0][0]` 表示第 `0` 号房子粉刷成红色的成本花费；`costs[1][2]` 表示第 `1` 号房子粉刷成绿色的花费，以此类推。

请计算出粉刷完所有房子最少的花费成本。

提示:

- `costs.length == n`
- `costs[i].length == 3`
- `1 <= n <= 100`
- `1 <= costs[i][j] <= 20`

# 示例

```
输入: costs = [[17,2,17],[16,16,5],[14,3,19]]
输出: 10
解释: 将 0 号房子粉刷成蓝色，1 号房子粉刷成绿色，2 号房子粉刷成蓝色。
     最少花费: 2 + 5 + 3 = 10。
```

# 题解

## 动态规划

**状态**

定义 `dp[i][j]` 是以 `j` 颜色结尾的第 `i` 间房子的最小花费。

**转移**

当前状态肯定是前一个状态中的最小值和当前消耗的和。

题目给出颜色不能连续，那么当前颜色是 `0` 的话，前一个状态的颜色只能选择 `1` 或者 `2` 中的最小值来转移。

$$dp[i][j] = min(dp[i-1][k],dp[i-1][l]) + costs[i][j], [j,k,l]一个集合$$

从公式可以看出，状态的转移只依赖于前一个状态，我们这里采用滚动数组的方式来优化。

```ts
function minCost(costs: number[][]): number {
  const m = costs.length;
  // dp[i][j]表示第i间房子且以j为颜色的粉刷最少花费
  // 滚动数组，状态转移只依赖于前一个状态
  const dp: number[][] = new Array(2).fill(0).map((_) => new Array(3).fill(0));
  // 初始化状态
  dp[0] = [...costs[0]];

  for (let i = 1; i < m; i++) {
    const currIdx = i % 2;
    const preIdx = Number(!currIdx);
    // 相邻颜色不能相同，
    // 比如：0颜色的房子，那么它只能从1或者2颜色转移而来
    dp[currIdx][0] = Math.min(dp[preIdx][1], dp[preIdx][2]) + costs[i][0];
    dp[currIdx][1] = Math.min(dp[preIdx][0], dp[preIdx][2]) + costs[i][1];
    dp[currIdx][2] = Math.min(dp[preIdx][1], dp[preIdx][0]) + costs[i][2];
  }
  const idx = (m - 1) % 2;
  // 结果取不同颜色结尾的粉刷成本的最小值
  return Math.min(...dp[idx]);
}
```

```cpp
class Solution {
public:
    int minCost(vector<vector<int>> &costs) {
        int n = costs.size();
        vector<int> dp(3);
        for (int j = 0; j < 3; j++) {
            dp[j] = costs[0][j];
        }

        for (int i = 1; i < n; i++) {
            vector<int> nextDP(3);
            for (int j = 0; j < 3; j++) {
                nextDP[j] = costs[i][j] + min(dp[(j + 1) % 3], dp[(j + 2) % 3]);
            }
            dp = nextDP;
        }
        return min(dp[0], min(dp[1], dp[2]));
    }
};
```
