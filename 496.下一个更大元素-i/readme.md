# 题目

给你两个 没有重复元素 的数组 nums1 和 nums2 ，其中 nums1 是 nums2 的子集。

请你找出 nums1 中每个元素在 nums2 中的下一个比其大的值。

nums1 中数字 x 的下一个更大元素是指 x 在 nums2 中对应位置的右边的第一个比 x 大的元素。如果不存在，对应位置输出 -1 。

提示：

- 1 <= nums1.length <= nums2.length <= 1000
- 0 <= nums1[i], nums2[i] <= 104
- nums1 和 nums2 中所有整数 互不相同
- nums1 中的所有整数同样出现在 nums2 中

# 示例

```
输入: nums1 = [4,1,2], nums2 = [1,3,4,2].
输出: [-1,3,-1]
解释:
    对于 num1 中的数字 4 ，你无法在第二个数组中找到下一个更大的数字，因此输出 -1 。
    对于 num1 中的数字 1 ，第二个数组中数字1右边的下一个较大数字是 3 。
    对于 num1 中的数字 2 ，第二个数组中没有下一个更大的数字，因此输出 -1 。
```

# 题解

## 单调栈+哈希表

利用单调递减栈的特性，计算出 nums2 的每个元素的下一个更大元素，放入哈希中保存。

遍历 nums1，取哈希中寻找对应的元素，没有的话，返回-1。

```ts
function nextGreaterElement(nums1: number[], nums2: number[]): number[] {
  const hash: Map<number, number> = new Map();
  const stack = [];
  // 计算num2中所有元素的下一个更大元素，放入hash中保存
  for (const num of nums2) {
    while (stack.length && num > stack[stack.length - 1]) {
      hash.set(stack[stack.length - 1], num);
      stack.pop();
    }
    stack.push(num);
  }

  const ans = [];
  // 从hash中得到下一个更大元素
  for (const num of nums1) {
    ans.push(hash.get(num) || -1);
  }

  return ans;
}
```
