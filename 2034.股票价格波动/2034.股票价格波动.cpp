/*
 * @lc app=leetcode.cn id=2034 lang=cpp
 *
 * [2034] 股票价格波动
 */

// @lc code=start
class StockPrice
{
private:
    int maxTimestamp;
    unordered_map<int, int> priceMap;
    multiset<int> priceSet;

public:
    StockPrice()
    {
        maxTimestamp = 0;
    }

    void update(int timestamp, int price)
    {
        maxTimestamp = max(maxTimestamp, timestamp);
        // 是否需要更新
        int prevPrice = priceMap.count(timestamp) ? priceMap[timestamp] : 0;
        priceMap[timestamp] = price;
        // 有更新，需要将老的价格删除
        if (prevPrice > 0)
        {
            auto it = priceSet.find(prevPrice);
            if (it != priceSet.end())
            {
                priceSet.erase(it);
            }
        }
        priceSet.emplace(price);
    }

    int current()
    {
        return priceMap[maxTimestamp];
    }

    int maximum()
    {
        return *priceSet.rbegin();
    }

    int minimum()
    {
        return *priceSet.begin();
    }
};

/**
 * Your StockPrice object will be instantiated and called as such:
 * StockPrice* obj = new StockPrice();
 * obj->update(timestamp,price);
 * int param_2 = obj->current();
 * int param_3 = obj->maximum();
 * int param_4 = obj->minimum();
 */
// @lc code=end
