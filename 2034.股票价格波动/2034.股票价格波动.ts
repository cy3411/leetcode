/*
 * @lc app=leetcode.cn id=2034 lang=typescript
 *
 * [2034] 股票价格波动
 */

// @lc code=start
class StockPrice {
  data: number[] = [];
  priceMap: Map<number, number> = new Map();
  maxTimestamp: number = 0;
  constructor() {}

  update(timestamp: number, price: number): void {
    this.maxTimestamp = Math.max(this.maxTimestamp, timestamp);
    const prevPrice = this.priceMap.get(timestamp) ?? -1;
    this.priceMap.set(timestamp, price);
    if (prevPrice !== -1) {
      const idx = this.data.findIndex((p) => p === prevPrice);
      idx !== -1 && this.data.splice(idx, 1);
    }
    this.data.push(price);
    // data可以处理成有序集合的数据结构，红黑树
    this.data.sort((a, b) => a - b);
  }

  current(): number {
    return this.priceMap.get(this.maxTimestamp) ?? -1;
  }

  maximum(): number {
    return this.data[this.data.length - 1];
  }

  minimum(): number {
    return this.data[0];
  }
}

/**
 * Your StockPrice object will be instantiated and called as such:
 * var obj = new StockPrice()
 * obj.update(timestamp,price)
 * var param_2 = obj.current()
 * var param_3 = obj.maximum()
 * var param_4 = obj.minimum()
 */
// @lc code=end
