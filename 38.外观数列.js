/*
 * @lc app=leetcode.cn id=38 lang=javascript
 *
 * [38] 外观数列
 */

// @lc code=start
/**
 * @param {number} n
 * @return {string}
 */
var countAndSay = function (n) {
  if (n === 1) {
    return '1'
  }
  const s = countAndSay(n - 1)

  let i = 0
  let res = ''
  for (let j = 0; j < s.length; j++) {
    if (s[j] !== s[i]) {
      res += j - i + s[i]
      i = j
    }
  }
  res += s.length - i + s[s.length - 1]
  return res
};
// @lc code=end

