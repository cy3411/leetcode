/*
 * @lc app=leetcode.cn id=2044 lang=typescript
 *
 * [2044] 统计按位或能得到最大值的子集数目
 */

// @lc code=start
function countMaxOrSubsets(nums: number[]): number {
  const n = nums.length;
  let maxVal = 0;
  let ans = 0;
  // 枚举每一种组合
  for (let i = 1; i < 1 << n; i++) {
    let curVal = 0;
    for (let j = 0; j < n; j++) {
      // 当前组合有这个元素
      if (((i >> j) & 1) === 1) {
        curVal |= nums[j];
      }
    }
    // 统计最大或值
    if (curVal > maxVal) {
      maxVal = curVal;
      ans = 1;
    } else if (curVal === maxVal) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
