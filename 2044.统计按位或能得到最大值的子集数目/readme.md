# 题目

给你一个整数数组 `nums` ，请你找出 `nums` 子集 **按位或** 可能得到的 **最大值** ，并返回按位或能得到最大值的 **不同非空子集的数目** 。

如果数组 `a` 可以由数组 `b` 删除一些元素（或不删除）得到，则认为数组 `a` 是数组 `b` 的一个 **子集** 。如果选中的元素下标位置不一样，则认为两个子集 **不同** 。

对数组 `a` 执行 **按位或** ，结果等于 `a[0] OR a[1] OR ... OR a[a.length - 1]`（下标从 `0` 开始）。

提示：

- $1 \leq nums.length \leq 16$
- $1 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [3,1]
输出：2
解释：子集按位或能得到的最大值是 3 。有 2 个子集按位或可以得到 3 ：
- [3]
- [3,1]
```

```
输入：nums = [2,2,2]
输出：7
解释：[2,2,2] 的所有非空子集的按位或都可以得到 2 。总共有 23 - 1 = 7 个子集。
```

# 题解

## 枚举+位运算

枚举每一种组合，统计最大或值，并记录出现次数。

`nums` 中的每个元素只有选择或者不选择两种状态，所以一共有 $2^n$ 个状态。除去 `0`， 有 $2^n - 1$ 种组合。

我们利用二进制位来表示状态，每个位代表一个元素是否被选择。枚举 $[1, 2^n - 1]$ 所有的组合，并统计按位或最大值。

```ts
function countMaxOrSubsets(nums: number[]): number {
  const n = nums.length;
  let maxVal = 0;
  let ans = 0;
  // 枚举每一种组合
  for (let i = 1; i < 1 << n; i++) {
    let curVal = 0;
    for (let j = 0; j < n; j++) {
      // 当前组合有这个元素
      if (((i >> j) & 1) === 1) {
        curVal |= nums[j];
      }
    }
    // 统计最大或值
    if (curVal > maxVal) {
      maxVal = curVal;
      ans = 1;
    } else if (curVal === maxVal) {
      ans++;
    }
  }
  return ans;
}
```

## 递归

```cpp
class Solution
{
    int n;

public:
    void dfs(vector<int> &nums, int idx, int orVal, int &ans, int &maxOrVal)
    {
        if (idx == n)
        {
            if (orVal > maxOrVal)
            {
                maxOrVal = orVal;
                ans = 1;
            }
            else if (orVal == maxOrVal)
            {
                ans++;
            }
            return;
        }
        dfs(nums, idx + 1, orVal | nums[idx], ans, maxOrVal);
        dfs(nums, idx + 1, orVal, ans, maxOrVal);
    };
    int countMaxOrSubsets(vector<int> &nums)
    {
        int ans = 0, maxOrVal = 0;
        n = nums.size();
        dfs(nums, 0, 0, ans, maxOrVal);
        return ans;
    }
};
```
