# 题目

神奇字符串 s 仅由 '1' 和 '2' 组成，并需要遵守下面的规则：

- 神奇字符串 s 的神奇之处在于，串联字符串中 '1' 和 '2' 的连续出现次数可以生成该字符串。

s 的前几个元素是 s = "1221121221221121122……" 。如果将 s 中连续的若干 1 和 2 进行分组，可以得到 "1 22 11 2 1 22 1 22 11 2 11 22 ......" 。每组中 1 或者 2 的出现次数分别是 "1 2 2 1 1 2 1 2 2 1 2 2 ......" 。上面的出现次数正是 s 自身。

给你一个整数 n ，返回在神奇字符串 s 的前 n 个数字中 1 的数目。

提示：

- $1 \leq n \leq 10^5$

# 示例

```
输入：n = 6
输出：3
解释：神奇字符串 s 的前 6 个元素是 “122112”，它包含三个 1，因此返回 3 。
```

```
输入：n = 1
输出：1
```

# 题解

## 双指针

初始化起始的字符串 122 ，后面每组数字的规则如下：

- 每组数字出现的次数为按序遍历字符串最后一位的数字，如果为数字 1，则出现 1 次
- 每组数字中的数字为字符串最后一位相反的数字，如果为数字 1，则出现数字 2

定义双指针 i,j，i 按序向后，j 则一直指向最后一位，然后按照上面规则拼接。

```js
function magicalString(n: number): number {
  if (n < 4) return 1;
  // 构建初始状态：122
  const s: number[] = new Array(n).fill(0).map((_, i) => {
    if (i === 0) return 1;
    else if (i === 1 || i === 2) return 2;
    else return 0;
  });

  let ans = 1;
  // s[i]，处理每组的数字出现几次
  let i = 2;
  // s[j]，处理每组的数字是什么
  let j = 3;
  while (j < n) {
    let size = s[i];
    const num = 3 - s[j - 1];
    // 拼接数字
    while (j < n && size) {
      // 记录 1 出现的次数
      if (num === 1) ans++;
      s[j++] = num;
      size--;
    }
    i++;
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int magicalString(int n) {
        if (n < 4) return 1;
        int *s = new int[n];
        for (int i = 0; i < n; i++) {
            if (i == 0)
                s[0] = 1;
            else if (i == 1 || i == 2)
                s[i] = 2;
            else
                s[i] = 0;
        }
        int ans = 1, i = 2, j = 3;
        while (j < n) {
            int size = s[i], num = 3 - s[j - 1];
            while (size && j < n) {
                if (num == 1) ans++;
                s[j++] = num;
                size--;
            }
            i++;
        }
        delete[] s;
        return ans;
    }
};
```

```py
class Solution:
    def magicalString(self, n: int) -> int:
        if n < 4:
            return 1
        s = [0] * n
        s[:3] = [1, 2, 2]
        ans = 1
        i, j = 2, 3
        while j < n:
            size = s[i]
            num = 3 - s[j - 1]
            while size and j < n:
                if num == 1:
                    ans += 1
                s[j] = num
                j += 1
                size -= 1
            i += 1
        return ans
```
