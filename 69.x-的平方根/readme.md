# 题目

实现 `int sqrt(int x)` 函数。

计算并返回 x 的平方根，其中 x 是非负整数。

由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。

# 示例

```
输入: 8
输出: 2
说明: 8 的平方根是 2.82842...,
     由于返回类型是整数，小数部分将被舍去。
```

# 题解

## 二分查找

题目的意思就是在[0,x]区间内,找一个数字 y,确保 y 的平方最接近 x.

我可以通过二分来控制区间的左右边界，确保需要查找的值在边界内即可。

```ts
function mySqrt(x: number): number {
  let head = 0;
  let tail = x;
  let result = 0;

  while (head <= tail) {
    let mid = ((tail - head) >> 1) + head;
    if (mid * mid <= x) {
      head = mid + 1;
      result = mid;
    } else {
      tail = mid - 1;
    }
  }

  return result;
}
```

```ts
function mySqrt(x: number): number {
  let head = 0;
  let tail = x;
  let result = 0;

  let count = 32;

  while (count--) {
    let mid = ((tail - head) >> 1) + head;
    if (mid * mid <= x) {
      head = mid + 1;
      result = mid;
    } else {
      tail = mid - 1;
    }
  }

  return result;
}
```
