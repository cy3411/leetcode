/*
 * @lc app=leetcode.cn id=69 lang=typescript
 *
 * [69] x 的平方根
 */

// @lc code=start
function mySqrt(x: number): number {
  let head = 0;
  let tail = x;
  let result = 0;

  let count = 32;

  while (count--) {
    let mid = ((tail - head) >> 1) + head;
    if (mid * mid <= x) {
      head = mid + 1;
      result = mid;
    } else {
      tail = mid - 1;
    }
  }

  return result;
}
// @lc code=end
