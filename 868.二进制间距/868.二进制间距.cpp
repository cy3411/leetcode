/*
 * @lc app=leetcode.cn id=868 lang=cpp
 *
 * [868] 二进制间距
 */

// @lc code=start
class Solution
{
public:
    int binaryGap(int n)
    {
        int pre = -1, ans = 0, i = 0;
        while (n)
        {
            if (n & 1)
            {
                if (pre != -1)
                {
                    ans = max(ans, i - pre);
                }
                pre = i;
            }
            n >>= 1;
            i++;
        }
        return ans;
    }
};
// @lc code=end
