/*
 * @lc app=leetcode.cn id=1052 lang=javascript
 *
 * [1052] 爱生气的书店老板
 */

// @lc code=start
/**
 * @param {number[]} customers
 * @param {number[]} grumpy
 * @param {number} X
 * @return {number}
 */
var maxSatisfied = function (customers, grumpy, X) {
  const N = customers.length;
  let result = 0;
  for (let i = 0; i < N; i++) {
    if (grumpy[i] === 0) {
      result += customers[i];
    }
  }
  let increase = 0;
  let maxIncrease = increase;
  let left = 0;
  let right = 0;
  while (right < N) {
    increase += customers[right] * grumpy[right];
    while (right - left >= X) {
      increase -= customers[left] * grumpy[left];
      left++;
    }
    maxIncrease = Math.max(maxIncrease, increase);
    right++;
  }
  return result + maxIncrease;
};
// @lc code=end
