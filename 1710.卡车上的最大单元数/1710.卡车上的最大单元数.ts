/*
 * @lc app=leetcode.cn id=1710 lang=typescript
 *
 * [1710] 卡车上的最大单元数
 */

// @lc code=start
function maximumUnits(boxTypes: number[][], truckSize: number): number {
  // 按照箱子数量降序排序
  boxTypes.sort((a: number[], b: number[]) => {
    return b[1] - a[1];
  });

  let ans = 0;
  // 贪心从单元数最大的开始装载
  for (const [boxs, cells] of boxTypes) {
    if (truckSize === 0) break;
    let maxUnits = Math.min(truckSize, boxs);
    ans += maxUnits * cells;
    truckSize -= maxUnits;
  }
  return ans;
}
// @lc code=end
