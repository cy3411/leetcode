# 题目

请你将一些箱子装在 一辆卡车 上。给你一个二维数组 boxTypes ，其中 boxTypes[i] = $[numberOfBoxes_i, numberOfUnitsPerBox_i]$ ：

- $numberOfBoxes_i$ 是类型 i 的箱子的数量。
- $numberOfUnitsPerBox_i$ 是类型 i 每个箱子可以装载的单元数量。

整数 truckSize 表示卡车上可以装载 箱子 的 最大数量 。只要箱子数量不超过 truckSize ，你就可以选择任意箱子装到卡车上。

返回卡车可以装载 单元 的 最大 总数。

提示：

- $1 \leq boxTypes.length \leq 1000$
- $1 \leq numberOfBoxesi, numberOfUnitsPerBoxi \leq 1000$
- $1 \leq truckSize \leq 10^6$

# 示例

```
输入：boxTypes = [[1,3],[2,2],[3,1]], truckSize = 4
输出：8
解释：箱子的情况如下：
- 1 个第一类的箱子，里面含 3 个单元。
- 2 个第二类的箱子，每个里面含 2 个单元。
- 3 个第三类的箱子，每个里面含 1 个单元。
可以选择第一类和第二类的所有箱子，以及第三类的一个箱子。
单元总数 = (1 * 3) + (2 * 2) + (1 * 1) = 8
```

```
输入：boxTypes = [[5,10],[2,5],[4,7],[3,9]], truckSize = 10
输出：91
```

# 题解

## 贪心

题意要求装载单元的最大数量，那么我们可以优先将单元最大的箱子装上卡车，其次优先次大的，直到满足 truckSize 。

一开始将 boxTypes 可以按照装载单元的数量降序排序，然后遍历 boxTypes。依次装载箱子，直到 truckSize 为 0。

```ts
function maximumUnits(boxTypes: number[][], truckSize: number): number {
  // 按照箱子数量降序排序
  boxTypes.sort((a: number[], b: number[]) => {
    return b[1] - a[1];
  });

  let ans = 0;
  // 贪心从单元数最大的开始装载
  for (const [boxs, cells] of boxTypes) {
    if (truckSize === 0) break;
    let maxUnits = Math.min(truckSize, boxs);
    ans += maxUnits * cells;
    truckSize -= maxUnits;
  }
  return ans;
}
```
