/*
 * @lc app=leetcode.cn id=228 lang=javascript
 *
 * [228] 汇总区间
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {string[]}
 */
var summaryRanges = function (nums) {
  nums.push(void 0);
  const result = [];
  let start = 0;

  nums.reduce((prev, curr, i) => {
    if (curr - prev !== 1) {
      result.push(`${nums[start]}${start === i - 1 ? '' : '->' + prev}`);
      start = i;
    }
    return curr;
  });
  return result;
};
// @lc code=end
