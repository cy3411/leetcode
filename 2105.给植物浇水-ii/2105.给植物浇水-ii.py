#
# @lc app=leetcode.cn id=2105 lang=python3
#
# [2105] 给植物浇水 II
#

# @lc code=start
class Solution:
    def minimumRefill(self, plants: List[int], capacityA: int, capacityB: int) -> int:
        a, b = capacityA, capacityB
        i, j = 0, len(plants)-1
        res = 0
        while i < j:
            if plants[i] > a:
                a = capacityA
                res += 1
            a -= plants[i]
            i += 1

            if plants[j] > b:
                b = capacityB
                res += 1
            b -= plants[j]
            j -= 1

        res += i == j and max(a, b) < plants[i]
        return res

# @lc code=end
