/*
 * @lc app=leetcode.cn id=1765 lang=cpp
 *
 * [1765] 地图中的最高点
 */

// @lc code=start
class Solution
{
public:
    vector<vector<int>> highestPeak(vector<vector<int>> &isWater)
    {
        int m = isWater.size(), n = isWater[0].size();
        vector<vector<int>> ans(m, vector<int>(n, -1));
        int directions[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        queue<pair<int, int>> q;

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (isWater[i][j] == 1)
                {
                    ans[i][j] = 0;
                    q.push({i, j});
                }
            }
        }

        while (!q.empty())
        {
            auto p = q.front();
            q.pop();
            for (auto dir : directions)
            {
                int x = p.first + dir[0], y = p.second + dir[1];
                if (x < 0 || x >= m)
                    continue;
                if (y < 0 || y >= n)
                    continue;
                if (ans[x][y] != -1)
                    continue;
                ans[x][y] = ans[p.first][p.second] + 1;
                q.push({x, y});
            }
        }

        return ans;
    }
};
// @lc code=end
