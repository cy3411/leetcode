# 题目

给你一个大小为 `m x n` 的整数矩阵 `isWater` ，它代表了一个由 **陆地** 和 **水域** 单元格组成的地图。

- 如果 `isWater[i][j] == 0` ，格子 `(i, j)` 是一个 **陆地** 格子。
- 如果 `isWater[i][j] == 1` ，格子 `(i, j)` 是一个 **水域** 格子。

你需要按照如下规则给每个单元格安排高度：

- 每个格子的高度都必须是非负的。
- 如果一个格子是是 水域 ，那么它的高度必须为 `0` 。
- 任意相邻的格子高度差 **至多** 为 `1` 。当两个格子在正东、南、西、北方向上相互紧挨着，就称它们为相邻的格子。（也就是说它们有一条公共边）

找到一种安排高度的方案，使得矩阵中的最高高度值 **最大** 。

请你返回一个大小为 `m x n` 的整数矩阵 `height` ，其中 `height[i][j]` 是格子 `(i, j)` 的高度。如果有多种解法，请返回 **任意一个** 。

提示：

- `m == isWater.length`
- `n == isWater[i].length`
- $\color{burlywood}1 \leq m, n \leq 1000$
- `isWater[i][j]` 要么是 `0` ，要么是 `1` 。
- 至少有 `1` 个水域格子。

# 示例

[![HpjxfA.png](https://s4.ax1x.com/2022/01/29/HpjxfA.png)](https://imgtu.com/i/HpjxfA)

```
输入：isWater = [[0,1],[0,0]]
输出：[[1,0],[2,1]]
解释：上图展示了给各个格子安排的高度。
蓝色格子是水域格，绿色格子是陆地格。
```

[![Hpv96P.png](https://s4.ax1x.com/2022/01/29/Hpv96P.png)](https://imgtu.com/i/Hpv96P)

```
输入：isWater = [[0,0,1],[1,0,0],[0,0,0]]
输出：[[1,1,0],[0,1,1],[1,2,2]]
解释：所有安排方案中，最高可行高度为 2 。
任意安排方案中，只要最高高度为 2 且符合上述规则的，都为可行方案。
```

# 题解

## 广度优先搜索

题目规定的水域的高度必须是 `0`，因此水域的高度是确定的值，我们可以从水域出发，推到出其他格子的高度。

先计算跟水域相连的格子高度，题目给出相邻格子高度差为 `1`，因此相邻格子的最大高度就是水域的高度加 `1`。

接着计算与高度为 `1` 的格子相邻的且未被计算过的格子，这些相邻格子的高度就是 `2`。

依次类推，直到所有格子都被计算过。

```ts
function highestPeak(isWater: number[][]): number[][] {
  const m = isWater.length;
  const n = isWater[0].length;
  // -1表示未安排高度的格子
  const ans: number[][] = new Array(m).fill(0).map(() => new Array(n).fill(-1));
  const queue: number[][] = [];
  // 将所有的水域坐标放入队列
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (isWater[i][j] === 1) {
        ans[i][j] = 0;
        queue.push([i, j]);
      }
    }
  }
  const directions = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
  ];
  while (queue.length) {
    const [x, y] = queue.shift()!;
    for (const [dx, dy] of directions) {
      const nx = x + dx;
      const ny = y + dy;
      if (nx < 0 || nx >= m) continue;
      if (ny < 0 || ny >= n) continue;
      if (ans[nx][ny] !== -1) continue;
      ans[nx][ny] = ans[x][y] + 1;
      queue.push([nx, ny]);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<vector<int>> highestPeak(vector<vector<int>> &isWater)
    {
        int m = isWater.size(), n = isWater[0].size();
        vector<vector<int>> ans(m, vector<int>(n, -1));
        int directions[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        queue<pair<int, int>> q;

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (isWater[i][j] == 1)
                {
                    ans[i][j] = 0;
                    q.push({i, j});
                }
            }
        }

        while (!q.empty())
        {
            auto p = q.front();
            q.pop();
            for (auto dir : directions)
            {
                int x = p.first + dir[0], y = p.second + dir[1];
                if (x < 0 || x >= m)
                    continue;
                if (y < 0 || y >= n)
                    continue;
                if (ans[x][y] != -1)
                    continue;
                ans[x][y] = ans[p.first][p.second] + 1;
                q.push({x, y});
            }
        }

        return ans;
    }
};
```
