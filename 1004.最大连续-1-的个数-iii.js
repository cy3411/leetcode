/*
 * @lc app=leetcode.cn id=1004 lang=javascript
 *
 * [1004] 最大连续1的个数 III
 */

// @lc code=start
/**
 * @param {number[]} A
 * @param {number} K
 * @return {number}
 */
var longestOnes = function (A, K) {
  const size = A.length;
  let zeros = 0;
  let result = 0;
  let left = 0;
  let right = 0;
  while (right < size) {
    if (A[right] === 0) {
      zeros++;
    }
    while (zeros > K) {
      if (A[left] === 0) zeros--;
      left++;
    }
    result = Math.max(result, right - left + 1);
    right++;
  }

  return result;
};
// @lc code=end
