# 题目

给你一个整数数组 `nums` ，其中总是存在 **唯一的** 一个最大整数 。

请你找出数组中的最大元素并检查它是否 **至少是数组中每个其他数字的两倍** 。如果是，则返回 **最大元素的下标** ，否则返回 `-1` 。

提示：

- $\color{burlywood}1 \leq nums.length \leq 50$
- $\color{burlywood}0 \leq nums[i] \leq 100$
- `nums` 中的最大元素是唯一的

# 示例

```
输入：nums = [3,6,1,0]
输出：1
解释：6 是最大的整数，对于数组中的其他整数，6 大于数组中其他元素的两倍。6 的下标是 1 ，所以返回 1 。
```

```
输入：nums = [1,2,3,4]
输出：-1
解释：4 没有超过 3 的两倍大，所以返回 -1 。
```

# 题解

## 枚举

维护最大值和次大值，遍历的过程中更新最大值和次大值。

最后比较最大值和次大值是否满足条件。

```ts
function dominantIndex(nums: number[]): number {
  const n = nums.length;

  // 最大数字和下标
  let first = -1;
  let index = -1;
  // 次大数字和下标
  let second = -1;

  for (let i = 0; i < n; i++) {
    if (nums[i] > first) {
      second = first;
      first = nums[i];
      index = i;
    } else if (nums[i] > second) {
      second = nums[i];
    }
  }
  // 如果最大数字大于等于次大数字的两倍，则返回最大数字的下标
  return first >= 2 * second ? index : -1;
}
```

```cpp
class Solution
{
public:
    int dominantIndex(vector<int> &nums)
    {
        int m1 = -1, m2 = -1;
        int ans = -1;

        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] > m1)
            {
                m2 = m1;
                m1 = nums[i];
                ans = i;
            }
            else if (nums[i] > m2)
            {
                m2 = nums[i];
            }
        }

        return m1 >= m2 * 2 ? ans : -1;
    }
};
```
