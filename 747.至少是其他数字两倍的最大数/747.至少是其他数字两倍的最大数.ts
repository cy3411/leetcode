/*
 * @lc app=leetcode.cn id=747 lang=typescript
 *
 * [747] 至少是其他数字两倍的最大数
 */

// @lc code=start
function dominantIndex(nums: number[]): number {
  const n = nums.length;

  // 最大数字和下标
  let first = -1;
  let index = -1;
  // 次大数字和下标
  let second = -1;

  for (let i = 0; i < n; i++) {
    if (nums[i] > first) {
      second = first;
      first = nums[i];
      index = i;
    } else if (nums[i] > second) {
      second = nums[i];
    }
  }
  // 如果最大数字大于等于次大数字的两倍，则返回最大数字的下标
  return first >= 2 * second ? index : -1;
}
// @lc code=end
