/*
 * @lc app=leetcode.cn id=747 lang=cpp
 *
 * [747] 至少是其他数字两倍的最大数
 */

// @lc code=start
class Solution
{
public:
    int dominantIndex(vector<int> &nums)
    {
        int m1 = -1, m2 = -1;
        int ans = -1;

        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] > m1)
            {
                m2 = m1;
                m1 = nums[i];
                ans = i;
            }
            else if (nums[i] > m2)
            {
                m2 = nums[i];
            }
        }

        return m1 >= m2 * 2 ? ans : -1;
    }
};
// @lc code=end
