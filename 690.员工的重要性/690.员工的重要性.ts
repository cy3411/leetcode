/*
 * @lc app=leetcode.cn id=690 lang=typescript
 *
 * [690] 员工的重要性
 */

// @lc code=start

// Definition for Employee.
/* class Employee {
  id: number;
  importance: number;
  subordinates: number[];
  constructor(id: number, importance: number, subordinates: number[]) {
    this.id = id === undefined ? 0 : id;
    this.importance = importance === undefined ? 0 : importance;
    this.subordinates = subordinates === undefined ? [] : subordinates;
  }
} */

function GetImportance(employees: Employee[], id: number): number {
  const map = new Map();
  for (let employe of employees) {
    map.set(employe.id, employe);
  }

  const dfs = (id: number): number => {
    const employe = map.get(id);
    let result = employe.importance;
    for (let subordinat of employe.subordinates) {
      result += dfs(subordinat);
    }
    return result;
  };

  return dfs(id);
}
// @lc code=end
