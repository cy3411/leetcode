/*
 * @lc app=leetcode.cn id=49 lang=javascript
 *
 * [49] 字母异位词分组
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function (strs) {
  const hash = new Map();
  for (const str of strs) {
    const count = new Array(26).fill(0);
    for (const c of str) {
      count[c.charCodeAt() - 'a'.charCodeAt()]++;
    }
    const key = count.toString();
    if (hash.has(key)) {
      hash.get(key).push(str);
    } else {
      hash.set(key, [str]);
    }
  }
  return Array.from(hash.values());
};
// @lc code=end
