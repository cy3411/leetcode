# 题目

给你一个字符串 `s`，以及该字符串中的一些「索引对」数组 `pairs`，其中 `pairs[i] = [a, b]` 表示字符串中的两个索引（编号从 0 开始）。

你可以 任意多次交换 在 `pairs` 中任意一对索引处的字符。

返回在经过若干次交换后，`s` 可以变成的按字典序最小的字符串。

提示：

- 1 <= s.length <= 10^5
- 0 <= pairs.length <= 10^5
- 0 <= pairs[i][0], pairs[i][1] < s.length
- s 中只含有小写英文字母

# 示例

```
输入：s = "dcab", pairs = [[0,3],[1,2]]
输出："bacd"
解释：
交换 s[0] 和 s[3], s = "bcad"
交换 s[1] 和 s[2], s = "bacd"
```

```
输入：s = "dcab", pairs = [[0,3],[1,2],[0,2]]
输出："abcd"
解释：
交换 s[0] 和 s[3], s = "bcad"
交换 s[0] 和 s[2], s = "acbd"
交换 s[1] 和 s[2], s = "abcd"
```

# 题解
## 并查集
我们将索引对放入并查集中，如果它们是连通的，那么连通的索引对之间都可以互换位置。因为交换关系具有传递性。

我们可以将连通的元素放入一个集合中降序排序或者优先队列，然后按照顺序取出最小的即可。

```ts
// 并查集
class UnionFind {
  parent: number[];
  size: number[];
  count: number;
  constructor(n: number) {
    this.parent = new Array(n).fill(0).map((_, idx) => idx);
    this.size = new Array(n).fill(1);
    this.count = n;
  }

  find(x: number): number {
    if (x === this.parent[x]) return x;
    this.parent[x] = this.find(this.parent[x]);
    return this.parent[x];
  }

  union(x: number, y: number): void {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.parent[rx] = ry;
    this.size[ry] += this.size[rx];
    this.count--;
  }
}
// 小顶堆
class MinPQ {
  pq: string[];
  constructor() {
    this.pq = [];
  }
  size(): number {
    return this.pq.length;
  }
  less(i: number, j: number): boolean {
    return this.pq[i].charCodeAt(0) < this.pq[j].charCodeAt(0);
  }
  swap(i: number, j: number): void {
    [this.pq[i], this.pq[j]] = [this.pq[j], this.pq[i]];
  }
  swin(i: number): void {
    let parentIdx = (i - 1) >> 1;
    while (parentIdx >= 0 && this.less(i, parentIdx)) {
      this.swap(i, parentIdx);
      i = parentIdx;
      parentIdx = (i - 1) >> 1;
    }
  }
  sink(i: number): void {
    let childIdx = i * 2 + 1;
    while (childIdx < this.size()) {
      if (childIdx + 1 < this.size() && this.less(childIdx + 1, childIdx)) {
        childIdx++;
      }
      if (this.less(i, childIdx)) return;
      this.swap(i, childIdx);
      i = childIdx;
      childIdx = i * 2 + 1;
    }
  }
  push(x: string) {
    this.pq.push(x);
    this.swin(this.size() - 1);
  }
  pop(): string {
    const min = this.pq[0];
    this.swap(0, this.size() - 1);
    this.pq.pop();
    this.sink(0);
    return min;
  }
}

function smallestStringWithSwaps(s: string, pairs: number[][]): string {
  const size = s.length;
  const unionFind = new UnionFind(size);

  // 将索引对放入并查集
  for (let [x, y] of pairs) {
    unionFind.union(x, y);
  }

  // 将连通的元素放在root为下标的小顶堆中
  const help = new Array(size).fill(0).map(() => new MinPQ());
  for (let i = 0; i < size; i++) {
    const idx = unionFind.find(i);
    help[idx].push(s[i]);
  }

  // 將同一根下的元素从小顶堆中取出
  let result = '';
  for (let i = 0; i < size; i++) {
    const idx = unionFind.find(i);
    result += help[idx].pop();
  }

  return result;
}
```
