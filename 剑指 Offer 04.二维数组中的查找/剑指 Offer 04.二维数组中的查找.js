// @algorithm @lc id=100276 lang=javascript
// @title er-wei-shu-zu-zhong-de-cha-zhao-lcof
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var findNumberIn2DArray = function (matrix, target) {
  let m = matrix.length;
  if (m === 0) return false;
  let n = matrix[0].length;

  let row = m - 1;
  let col = 0;
  while (row >= 0 && col < n) {
    if (matrix[row][col] === target) {
      return true;
    }

    if (matrix[row][col] > target) {
      row--;
    } else if (matrix[row][col] < target) {
      col++;
    }
  }

  return false;
};
