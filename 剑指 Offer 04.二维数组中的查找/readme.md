# 题目
在一个 `n * m` 的二维数组中，每一行都按照**从左到右递增**的顺序排序，每一列都按照**从上到下递增**的顺序排序。请完成一个高效的函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。

限制：

+ 0 <= n <= 1000
+ 0 <= m <= 1000
# 示例
```
现有矩阵 matrix 如下：

[
  [1,   4,  7, 11, 15],
  [2,   5,  8, 12, 19],
  [3,   6,  9, 16, 22],
  [10, 13, 14, 17, 24],
  [18, 21, 23, 26, 30]
]

给定 target = 5，返回 true。

给定 target = 20，返回 false。
```
# 题解
## 二分查找
```js
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var findNumberIn2DArray = function (matrix, target) {
  let m = matrix.length;
  if (m === 0) return false;
  let n = matrix[0].length;

  let row = m - 1;
  let col = 0;
  while (row >= 0 && col < n) {
    if (matrix[row][col] === target) {
      return true;
    }

    if (matrix[row][col] > target) {
      row--;
    } else if (matrix[row][col] < target) {
      col++;
    }
  }

  return false;
};
```