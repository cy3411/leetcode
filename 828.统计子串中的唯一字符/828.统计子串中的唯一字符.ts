/*
 * @lc app=leetcode.cn id=828 lang=typescript
 *
 * [828] 统计子串中的唯一字符
 */

// @lc code=start
var uniqueLetterString = function (s: string): number {
  // 记录每个字符出现的位置
  const index = new Map<string, number[]>();
  const n = s.length;
  for (let i = 0; i < n; i++) {
    const c = s[i];
    if (!index.has(c)) {
      index.set(c, [-1]);
    }
    index.get(c)!.push(i);
  }
  let res = 0;
  for (const [_, arr] of index.entries()) {
    arr.push(s.length);
    const n = arr.length;
    // 计算 i 位置字符可以贡献的次数
    for (let i = 1; i < n - 1; i++) {
      res += (arr[i] - arr[i - 1]) * (arr[i + 1] - arr[i]);
    }
  }
  return res;
};

// @lc code=end
