# 题目

我们定义了一个函数 `countUniqueChars(s)` 来统计字符串 `s` 中的唯一字符，并返回唯一字符的个数。

例如：`s = "LEETCODE"` ，则其中 `"L"`, `"T"`,`"C"`,`"O"`,`"D"` 都是唯一字符，因为它们只出现一次，所以 `countUniqueChars(s) = 5` 。

本题将会给你一个字符串 `s` ，我们需要返回 `countUniqueChars(t)` 的总和，其中 `t` 是 `s` 的子字符串。输入用例保证返回值为 32 位整数。

注意，某些子字符串可能是重复的，但你统计时也必须算上这些重复的子字符串（也就是说，你必须统计 `s` 的所有子字符串中的唯一字符）。

提示：

- $1 <= s.length <= 10^5$
- `s` 只包含大写英文字符

# 示例

```
输入: s = "ABC"
输出: 10
解释: 所有可能的子串为："A","B","C","AB","BC" 和 "ABC"。
     其中，每一个子串都由独特字符构成。
     所以其长度总和为：1 + 1 + 1 + 2 + 2 + 3 = 10
```

```
输入: s = "ABA"
输出: 8
解释: 除了 countUniqueChars("ABA") = 1 之外，其余与示例 1 相同。
```

# 题解

## 哈希表

题意就是将 s 分割成一些子串，计算所有字符唯一的子串的累加长度。

当一个字符只在一个子串中出现了一次，它就会对该子串同意唯一字符有贡献。我们只需要计算有多少子串包含该字符即可。

使用哈希表统计每个字符出现的下标，最后遍历每个字符的下标，$(当前字符位置 - 前一个相同字符出现的位置)*(下一个相同字符出现的位置 - 当前字符位置)$就可以得到每个字符贡献的次数，最后返回累加结果。

比如，示例中的 "ABC"，B 的下标是 1，默认前一个位置是 -1 ，后面的位置是 3，那么 B 可以共享的不重复子串数量为 $(1+1)*(3-1)=4$

```js
var uniqueLetterString = function (s: string): number {
  // 记录每个字符出现的位置
  const index = new Map<string, number[]>();
  const n = s.length;
  for (let i = 0; i < n; i++) {
    const c = s[i];
    if (!index.has(c)) {
      index.set(c, [-1]);
    }
    index.get(c)!.push(i);
  }
  let res = 0;
  for (const [_, arr] of index.entries()) {
    arr.push(s.length);
    const n = arr.length;
    // 计算 i 位置字符可以贡献的次数
    for (let i = 1; i < n - 1; i++) {
      res += (arr[i] - arr[i - 1]) * (arr[i + 1] - arr[i]);
    }
  }
  return res;
};
```
