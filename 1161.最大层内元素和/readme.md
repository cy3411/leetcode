# 题目

给你一个二叉树的根节点 `root` 。设根节点位于二叉树的第 `1` 层，而根节点的子节点位于第 `2` 层，依此类推。

请返回层内元素之和 **最大** 的那几层（可能只有一层）的层号，并返回其中 **最小** 的那个。

提示：

- 树中的节点数在 `[1, 10^4]` 范围内
- $-10^5 \leq Node.val \leq 10^5$

# 示例

[![vkNkUx.png](https://s1.ax1x.com/2022/07/31/vkNkUx.png)](https://imgtu.com/i/vkNkUx)

```
输入：root = [1,7,0,7,-8,null,null]
输出：2
解释：
第 1 层各元素之和为 1，
第 2 层各元素之和为 7 + 0 = 7，
第 3 层各元素之和为 7 + -8 = -1，
所以我们返回第 2 层的层号，它的层内元素之和最大。
```

```
输入：root = [989,null,10250,98693,-89388,null,null,null,-32127]
输出：2
```

# 题解

## 广度优先搜索

使用广度优先搜索，每次搜索一层，记录每层的和，并且记录当前层数。

当前层的和如果大于最大和，则更新最大和和层数。

最后返回层数。

```ts
function maxLevelSum(root: TreeNode | null): number {
  const queue: TreeNode[] = [root];
  // 最大元素和
  let maxSum = root.val;
  // 最大元素和的层数
  let maxLevel = 1;
  let ans = 1;
  while (queue.length) {
    const size = queue.length;
    let sum = 0;
    // 计算当前层的元素和
    for (let i = 0; i < size; i++) {
      const node = queue.shift();
      sum += node.val;
      if (node.left) {
        queue.push(node.left);
      }
      if (node.right) {
        queue.push(node.right);
      }
    }
    // 当前层的元素和超过最大元素和，更新最大元素和和最大元素和的层数
    if (sum > maxSum) {
      maxSum = sum;
      ans = maxLevel;
    }
    // 增加层数
    maxLevel++;
  }
  return ans;
}
```

## 深度优先搜索

定义 sum 数组，用来保存每层的元素和。

深度遍历每一个节点，如果当前层数第一个访问，则将节点值放入 sum , 否则累加到 sum 的 level 索引处。

最后返回 sum 中最大的和的层数。

```cpp
class Solution {
private:
    vector<int> sum;

public:
    void dfs(TreeNode *root, int level) {
        if (level == sum.size()) {
            sum.push_back(root->val);
        } else {
            sum[level] += root->val;
        }

        if (root->left) {
            dfs(root->left, level + 1);
        }
        if (root->right) {
            dfs(root->right, level + 1);
        }
    }
    int maxLevelSum(TreeNode *root) {
        dfs(root, 0);
        int ans = 0;
        for (int i = 0; i < sum.size(); i++) {
            if (sum[i] > sum[ans]) {
                ans = i;
            }
        }
        return ans + 1;
    }
};
```
