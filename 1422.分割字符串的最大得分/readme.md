# 题目

给你一个由若干 0 和 1 组成的字符串 `s` ，请你计算并返回将该字符串分割成两个 **非空** 子字符串（即 **左** 子字符串和 **右** 子字符串）所能获得的最大得分。

「分割字符串的得分」为 **左** 子字符串中 0 的数量加上 **右** 子字符串中 1 的数量。

提示：

- $2 \leq s.length \leq 500$
- 字符串 `s` 仅由字符 `'0'` 和 `'1'` 组成。

# 示例

```
输入：s = "011101"
输出：5
解释：
将字符串 s 划分为两个非空子字符串的可行方案有：
左子字符串 = "0" 且 右子字符串 = "11101"，得分 = 1 + 4 = 5
左子字符串 = "01" 且 右子字符串 = "1101"，得分 = 1 + 3 = 4
左子字符串 = "011" 且 右子字符串 = "101"，得分 = 1 + 2 = 3
左子字符串 = "0111" 且 右子字符串 = "01"，得分 = 1 + 1 = 2
左子字符串 = "01110" 且 右子字符串 = "1"，得分 = 2 + 1 = 3
```

```
输入：s = "00111"
输出：5
解释：当 左子字符串 = "00" 且 右子字符串 = "111" 时，我们得到最大得分 = 2 + 3 = 5
```

# 题解

## 枚举

枚举每个分割点，计算左右字串的分数，取最大值。

```js
function maxScore(s: string): number {
  const n = s.length;
  let ans = 0;
  // 遍历分割点
  for (let i = 1; i < n; i++) {
    let score = 0;
    // 左边的得分
    for (let j = 0; j < i; j++) {
      if (s[j] === '0') score++;
    }
    // 右边的得分
    for (let j = i; j < n; j++) {
      if (s[j] === '1') score++;
    }
    // 更新最大得分
    ans = Math.max(ans, score);
  }

  return ans;
}
```

## 两次循环

初始化第一个分割点的分数，遍历字符串，将元素按序放入到左边字串中：

- 如果字符为 '0' ，那么分数加 1
- 如果字符为 '1' ，那么分数减 1

最后取最大值。

```cpp
class Solution {
public:
    int maxScore(string s) {
        int score = 0, n = s.size();
        // 初始化得分
        if (s[0] == '0') score++;
        for (int i = 1; i < n; i++) {
            if (s[i] == '1') score++;
        }

        int ans = score;
        // 遍历到最后剩下一个字符
        for (int i = 1; i < n - 1; i++) {
            if (s[i] == '0')
                score++;
            else
                score--;
            ans = max(ans, score);
        }
        return ans;
    }
};
```

```py
class Solution:
    def maxScore(self, s: str) -> int:
        ans = score = (s[0] == '0') + (s[1:].count('1'))
        for c in s[1:-1]:
            score += 1 if c == '0' else -1
            ans = max(ans, score)
        return ans
```
