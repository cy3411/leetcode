/*
 * @lc app=leetcode.cn id=1422 lang=typescript
 *
 * [1422] 分割字符串的最大得分
 */

// @lc code=start
function maxScore(s: string): number {
  const n = s.length;
  let ans = 0;
  // 遍历分割点
  for (let i = 1; i < n; i++) {
    let score = 0;
    // 左边的得分
    for (let j = 0; j < i; j++) {
      if (s[j] === '0') score++;
    }
    // 右边的得分
    for (let j = i; j < n; j++) {
      if (s[j] === '1') score++;
    }
    // 更新最大得分
    ans = Math.max(ans, score);
  }

  return ans;
}
// @lc code=end
