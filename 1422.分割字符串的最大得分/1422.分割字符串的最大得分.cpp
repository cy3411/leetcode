/*
 * @lc app=leetcode.cn id=1422 lang=cpp
 *
 * [1422] 分割字符串的最大得分
 */

// @lc code=start
class Solution {
public:
    int maxScore(string s) {
        int score = 0, n = s.size();
        // 初始化得分
        if (s[0] == '0') score++;
        for (int i = 1; i < n; i++) {
            if (s[i] == '1') score++;
        }

        int ans = score;
        // 遍历到最后剩下一个字符
        for (int i = 1; i < n - 1; i++) {
            if (s[i] == '0')
                score++;
            else
                score--;
            ans = max(ans, score);
        }
        return ans;
    }
};
// @lc code=end
