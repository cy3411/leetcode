/*
 * @lc app=leetcode.cn id=2216 lang=typescript
 *
 * [2216] 美化数组的最少删除数
 */

// @lc code=start
function minDeletion(nums: number[]): number {
  const n = nums.length;
  // 共有多少对美丽数组
  let count = 0;
  // 一对数字的前一位
  let preNum = nums[0];
  // 计算一共可以生成多少对美丽数组
  for (let i = 1; i < n; i++) {
    if (nums[i] === preNum) continue;
    count++;
    // 下一个数字越界
    if (i + 1 === n) break;
    // 更新下一对的第一个数字
    preNum = nums[i + 1];
  }

  return n - count * 2;
}
// @lc code=end
