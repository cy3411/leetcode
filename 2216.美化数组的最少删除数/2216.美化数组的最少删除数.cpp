/*
 * @lc app=leetcode.cn id=2216 lang=cpp
 *
 * [2216] 美化数组的最少删除数
 */

// @lc code=start
class Solution
{
public:
    int minDeletion(vector<int> &nums)
    {
        int n = nums.size(), count = 0, preSum = nums[0];

        for (int i = 1; i < n; i++)
        {
            if (nums[i] == preSum)
                continue;
            count++;
            if (i + 1 == n)
                break;
            preSum = nums[i + 1];
        }

        return n - 2 * count;
    }
};
// @lc code=end
