# 题目

给你一个下标从 `0` 开始的整数数组 `nums` ，如果满足下述条件，则认为数组 `nums` 是一个 美丽数组 ：

- `nums.length` 为偶数
- 对所有满足 `i % 2 == 0` 的下标 `i` ，`nums[i] != nums[i + 1]` 均成立

注意，空数组同样认为是美丽数组。

你可以从 `nums` 中删除任意数量的元素。当你删除一个元素时，被删除元素右侧的所有元素将会向左移动一个单位以填补空缺，而左侧的元素将会保持 `不变` 。

返回使 `nums` 变为美丽数组所需删除的 `最少` 元素数目。

提示：

- $1 \leq nums.length \leq 10^5$
- $0 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [1,1,2,3,5]
输出：1
解释：可以删除 nums[0] 或 nums[1] ，这样得到的 nums = [1,2,3,5] 是一个美丽数组。可以证明，要想使 nums 变为美丽数组，至少需要删除 1 个元素。
```

```
输入：nums = [1,1,2,2,3,3]
输出：2
解释：可以删除 nums[0] 和 nums[5] ，这样得到的 nums = [1,2,2,3] 是一个美丽数组。可以证明，要想使 nums 变为美丽数组，至少需要删除 2 个元素。
```

# 题解

## 贪心

要想得到更少的删除数，那么我们就需要去匹配更多的美丽数组对。

遍历数组，统计最多可以匹配多少对美丽数组，遍历结束后没有匹配上的元素就是需要删除的元素。

```ts
function minDeletion(nums: number[]): number {
  const n = nums.length;
  // 共有多少对美丽数组
  let count = 0;
  // 一对数字的前一位
  let preNum = nums[0];
  // 计算一共可以生成多少对美丽数组
  for (let i = 1; i < n; i++) {
    if (nums[i] === preNum) continue;
    count++;
    // 下一个数字越界
    if (i + 1 === n) break;
    // 更新下一对的第一个数字
    preNum = nums[i + 1];
  }

  return n - count * 2;
}
```

```cpp
class Solution
{
public:
    int minDeletion(vector<int> &nums)
    {
        int n = nums.size(), count = 0, preSum = nums[0];

        for (int i = 1; i < n; i++)
        {
            if (nums[i] == preSum)
                continue;
            count++;
            if (i + 1 == n)
                break;
            preSum = nums[i + 1];
        }

        return n - 2 * count;
    }
};
```
