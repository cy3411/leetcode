/*
 * @lc app=leetcode.cn id=593 lang=typescript
 *
 * [593] 有效的正方形
 */

// @lc code=start

function help(p1: number[], p2: number[], p3: number[], p4: number[]): boolean {
  const v1 = [p1[0] - p2[0], p1[1] - p2[1]];
  const v2 = [p3[0] - p4[0], p3[1] - p4[1]];
  if (!validMid(p1, p2, p3, p4)) return false;
  if (!validLength(v1, v2)) return false;
  if (!validAngle(v1, v2)) return false;
  return true;
}

// 斜边的中点是否相等
function validMid(p1: number[], p2: number[], p3: number[], p4: number[]): boolean {
  return p1[0] + p2[0] === p3[0] + p4[0] && p1[1] + p2[1] === p3[1] + p4[1];
}
// 斜边的长度是否相等
function validLength(v1: number[], v2: number[]): boolean {
  return v1[0] ** 2 + v1[1] ** 2 === v2[0] ** 2 + v2[1] ** 2;
}
// 斜边的角度是否为90度
function validAngle(v1: number[], v2: number[]): boolean {
  return v1[0] * v2[0] + v1[1] * v2[1] === 0;
}
// 两点是否相等
function isEqual(a: number[], b: number[]): boolean {
  return a.toString() === b.toString();
}

function validSquare(p1: number[], p2: number[], p3: number[], p4: number[]): boolean {
  if (isEqual(p1, p2)) return false;
  // p1, p2 对角线
  if (help(p1, p2, p3, p4)) return true;
  if (isEqual(p1, p3)) return false;
  // p1, p3 对角线
  if (help(p1, p3, p2, p4)) return true;
  if (isEqual(p1, p4)) return false;
  // p1, p4 对角线
  if (help(p1, p4, p2, p3)) return true;
  return false;
}
// @lc code=end
