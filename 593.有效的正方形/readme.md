# 题目

给定 2D 空间中四个点的坐标 `p1`, `p2`, `p3` 和 `p4`，如果这四个点构成一个正方形，则返回 `true` 。

点的坐标 `pi` 表示为 `[xi, yi]` 。输入 **不是** 按任何顺序给出的。

一个 **有效的正方形** 有四条等边和四个等角(90 度角)。

提示:

- $p1.length \equiv p2.length \equiv p3.length \equiv p4.length \equiv 2$
- $-10^4 \leq xi, yi \leq 10^4$

# 示例

```
输入: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
输出: True
```

```
输入：p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,12]
输出：false
```

# 题解

## 数学

利用 [正方形判定定理](https://leetcode.cn/link/?target=https%3A%2F%2Fbaike.baidu.com%2Fitem%2F%E6%AD%A3%E6%96%B9%E5%BD%A2%E5%88%A4%E5%AE%9A%E5%AE%9A%E7%90%86%2F5599805)来判断是否为一个正方形。

- 先判断是否为平行四边形，斜边的中点是否相等，如果相等，则为平行四边形。
- 满足上面条件判断是否为长方形，斜边的长度相等，如果相等，则为长方形。
- 满足上面条件判断是否为正方形，两条斜边是否垂直，如果是，则为正方形。

```ts
function help(p1: number[], p2: number[], p3: number[], p4: number[]): boolean {
  const v1 = [p1[0] - p2[0], p1[1] - p2[1]];
  const v2 = [p3[0] - p4[0], p3[1] - p4[1]];
  if (!validMid(p1, p2, p3, p4)) return false;
  if (!validLength(v1, v2)) return false;
  if (!validAngle(v1, v2)) return false;
  return true;
}

// 斜边的中点是否相等
function validMid(p1: number[], p2: number[], p3: number[], p4: number[]): boolean {
  return p1[0] + p2[0] === p3[0] + p4[0] && p1[1] + p2[1] === p3[1] + p4[1];
}
// 斜边的长度是否相等
function validLength(v1: number[], v2: number[]): boolean {
  return v1[0] ** 2 + v1[1] ** 2 === v2[0] ** 2 + v2[1] ** 2;
}
// 斜边的角度是否为90度
function validAngle(v1: number[], v2: number[]): boolean {
  return v1[0] * v2[0] + v1[1] * v2[1] === 0;
}
// 两点是否相等
function isEqual(a: number[], b: number[]): boolean {
  return a.toString() === b.toString();
}

function validSquare(p1: number[], p2: number[], p3: number[], p4: number[]): boolean {
  if (isEqual(p1, p2)) return false;
  // p1, p2 对角线
  if (help(p1, p2, p3, p4)) return true;
  if (isEqual(p1, p3)) return false;
  // p1, p3 对角线
  if (help(p1, p3, p2, p4)) return true;
  if (isEqual(p1, p4)) return false;
  // p1, p4 对角线
  if (help(p1, p4, p2, p3)) return true;
  return false;
}
```
