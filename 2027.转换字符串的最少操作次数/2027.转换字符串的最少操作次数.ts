/*
 * @lc app=leetcode.cn id=2027 lang=typescript
 *
 * [2027] 转换字符串的最少操作次数
 */

// @lc code=start
function minimumMoves(s: string): number {
  let ans = 0;
  // 连续字符数量
  let cnt = 0;

  for (const c of s) {
    if (c === 'X' || (c === 'O' && cnt)) {
      cnt++;
    }
    // 连续3个字符做一次操作
    if (cnt === 3) {
      ans++;
      cnt = 0;
    }
  }

  // 剩余的X也需要处理一下
  if (cnt) ans++;

  return ans;
}
// @lc code=end
