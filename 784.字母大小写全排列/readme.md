# 题目

给定一个字符串 s ，通过将字符串 s 中的每个字母转变大小写，我们可以获得一个新的字符串。

返回 所有可能得到的字符串集合 。以 任意顺序 返回输出。

提示:

- $1 \leq s.length \leq 12$
- s 由小写英文字母、大写英文字母和数字组成

# 示例

```
输入：s = "a1b2"
输出：["a1b2", "a1B2", "A1b2", "A1B2"]
```

```
输入: s = "3z4"
输出: ["3z4","3Z4"]
```

# 题解

## BFS

遍历字符串，当遍历到字符 c：

- c 是数字，将队列中所有的序列后都拼接上
- c 是字符，将队列中所有的序列后都加上大写和小写的字符
- 如果队列的序列长度和 s 一致，表示完成搜索，将其压入答案列表中

```js
function isLetter(ch: string): boolean {
  return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
}

function swapChar(ch: string): string {
  return String.fromCharCode(ch.charCodeAt(0) ^ 32);
}

function letterCasePermutation(s: string): string[] {
  const ans: string[] = [];
  const queue: string[] = [''];
  while (queue.length) {
    let curr = queue[0];
    // 需要处理的位置
    const pos = curr.length;
    if (pos === s.length) {
      // 得到一个合法结果
      ans.push(curr);
      queue.shift();
    } else {
      // 如果是字符，将下一个状态压入队列
      if (isLetter(s[pos])) {
        queue.push(curr + swapChar(s[pos]));
      }
      // 处理队首
      queue[0] += s[pos];
    }
  }

  return ans;
}
```
