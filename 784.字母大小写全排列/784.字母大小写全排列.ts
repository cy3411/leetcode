/*
 * @lc app=leetcode.cn id=784 lang=typescript
 *
 * [784] 字母大小写全排列
 */

// @lc code=start

function isLetter(ch: string): boolean {
  return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
}

function swapChar(ch: string): string {
  return String.fromCharCode(ch.charCodeAt(0) ^ 32);
}

function letterCasePermutation(s: string): string[] {
  const ans: string[] = [];
  const queue: string[] = [''];
  while (queue.length) {
    let curr = queue[0];
    const pos = curr.length;
    if (pos === s.length) {
      // 得到一个合法结果
      ans.push(curr);
      queue.shift();
    } else {
      // 如果是字符，将下一个状态压入队列
      if (isLetter(s[pos])) {
        queue.push(curr + swapChar(s[pos]));
      }
      // 处理队首
      queue[0] += s[pos];
    }
  }

  return ans;
}
// @lc code=end
