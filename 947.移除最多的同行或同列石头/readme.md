# 题目
`n` 块石头放置在二维平面中的一些整数坐标点上。每个坐标点上最多只能有一块石头。

如果一块石头的 同行或者同列 上有其他石头存在，那么就可以移除这块石头。

给你一个长度为 `n` 的数组 `stones` ，其中 `stones[i] = [xi, yi]` 表示第 `i` 块石头的位置，返回 **可以移除的石子** 的最大数量。

 
# 示例
```
输入：stones = [[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]]
输出：5
解释：一种移除 5 块石头的方法如下所示：
1. 移除石头 [2,2] ，因为它和 [2,1] 同行。
2. 移除石头 [2,1] ，因为它和 [0,1] 同列。
3. 移除石头 [1,2] ，因为它和 [1,0] 同行。
4. 移除石头 [1,0] ，因为它和 [0,0] 同列。
5. 移除石头 [0,1] ，因为它和 [0,0] 同行。
石头 [0,0] 不能移除，因为它没有与另一块石头同行/列。
```

# 题解
## 并查集
同行同列出现多个是否才可以移除，我们可以将同行同列的石头在并查集中连通，这样最后的集合数就是剩余的石头数量。(统一集合中的石头移除到最后必然要留一块，因为最后一块没有任何石头连通)

最后结果就是总的石头数减去剩余的石头数量。

```js
class UnionFind {
  parent: number[];
  size: number[];
  count: number;
  constructor(n: number = 0) {
    this.parent = new Array(n).fill(0).map((_, i) => i);
    this.size = new Array(n).fill(1);
    this.count = n;
  }
  find(x: number): number {
    return (this.parent[x] = this.parent[x] === x ? x : this.find(this.parent[x]));
  }
  union(x: number, y: number): void {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.size[ry] += this.size[rx];
    this.parent[rx] = ry;
    this.count--;
  }
}

function removeStones(stones: number[][]): number {
  const size = stones.length;
  const unionFind = new UnionFind(size);
  // 记录石头的坐标
  const mapX = new Map();
  const mapY = new Map();

  for (let i = 0; i < size; i++) {
    let [x, y] = stones[i];
    // 当前行出现过，表示有石头，就连通
    if (mapX.has(x)) {
      unionFind.union(i, mapX.get(x));
    }
    // 当前列出现过，连通
    if (mapY.has(y)) {
      unionFind.union(i, mapY.get(y));
    }
    mapX.set(x, i);
    mapY.set(y, i);
  }
  // 石头的总数减去集合数就是答案
  return size - unionFind.count;
}
```