/*
 * @lc app=leetcode.cn id=1588 lang=typescript
 *
 * [1588] 所有奇数长度子数组的和
 */

// @lc code=start
function sumOddLengthSubarrays(arr: number[]): number {
  const m = arr.length;
  const sum = new Array(m + 1).fill(0);

  // 计算前缀和
  for (let i = 1; i <= m; i++) {
    sum[i] = sum[i - 1] + arr[i - 1];
  }

  let ans = 0;

  // 枚举区间，如果区间大小是奇数，就累加结果
  for (let i = 0; i <= m; i++) {
    for (let j = i + 1; j <= m; j++) {
      if ((j - i) % 2 !== 0) ans += sum[j] - sum[i];
    }
  }

  return ans;
}
// @lc code=end
