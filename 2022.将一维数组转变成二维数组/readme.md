# 题目

给你一个下标从 **0** 开始的一维整数数组 `original` 和两个整数 `m` 和 `n` 。你需要使用 `original` 中 **所有** 元素创建一个 `m` 行 `n` 列的二维数组。

`original` 中下标从 `0` 到 `n - 1` （都 **包含** ）的元素构成二维数组的第一行，下标从 `n` 到 `2 * n - 1` （都 **包含** ）的元素构成二维数组的第二行，依此类推。

请你根据上述过程返回一个 `m x n` 的二维数组。如果无法构成这样的二维数组，请你返回一个空的二维数组。

# 示例

[![TIXGQI.png](https://s4.ax1x.com/2022/01/01/TIXGQI.png)](https://imgtu.com/i/TIXGQI)

```
输入：original = [1,2,3,4], m = 2, n = 2
输出：[[1,2],[3,4]]
解释：
构造出的二维数组应该包含 2 行 2 列。
original 中第一个 n=2 的部分为 [1,2] ，构成二维数组的第一行。
original 中第二个 n=2 的部分为 [3,4] ，构成二维数组的第二行。
```

# 题解

## 枚举

将一维数组的下标枚举出来，按照 n 进行分组，每组 n 个元素，构成一行。

- `row = i / n`
- `col = i % n`

```ts
function construct2DArray(original: number[], m: number, n: number): number[][] {
  // 源数组和目标数组大小不一致
  if (m * n !== original.length) return [];
  // 构建目标数组
  const ans = new Array(m).fill(0).map(() => new Array(n));
  // 将源数组中的元素按照行列放入目标数组
  for (let i = 0; i < original.length; i++) {
    const row = (i / n) >> 0;
    const col = i % n;
    ans[row][col] = original[i];
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<vector<int>> construct2DArray(vector<int> &original, int m, int n)
    {
        if (original.size() != m * n)
            return {};

        vector<vector<int>> ans(m, vector<int>(n));

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                ans[i][j] = original[i * n + j];
            }
        }

        return ans;
    }
};
```
