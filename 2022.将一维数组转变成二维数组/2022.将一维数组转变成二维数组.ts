/*
 * @lc app=leetcode.cn id=2022 lang=typescript
 *
 * [2022] 将一维数组转变成二维数组
 */

// @lc code=start
function construct2DArray(original: number[], m: number, n: number): number[][] {
  // 源数组和目标数组大小不一致
  if (m * n !== original.length) return [];
  // 构建目标数组
  const ans = new Array(m).fill(0).map(() => new Array(n));
  // 将源数组中的元素按照行列放入目标数组
  for (let i = 0; i < original.length; i++) {
    const row = (i / n) >> 0;
    const col = i % n;
    ans[row][col] = original[i];
  }

  return ans;
}
// @lc code=end
