# 题目

给定一个字符串 `s` 和一个整数 `k`，从字符串开头算起，每 `2k` 个字符反转前 `k` 个字符。

- 如果剩余字符少于 `k` 个，则将剩余字符全部反转。
- 如果剩余字符小于 `2k` 但大于或等于 `k` 个，则反转前 `k` 个字符，其余字符保持原样。

提示：

- `1 <= s.length <= 104`
- `s 仅由小写英文组成`
- `1 <= k <= 104`

# 示例

```
输入：s = "abcdefg", k = 2
输出："bacdfeg"
```

# 题解

## 双指针

题意就是在一个有效的区间内反转字符串。

每 `2k` 个字符反转前 `k` 个字符，首先要确定区间，遍历字符串，找个分割点：`[0,2k,4k,...,nk]`,然后计算出有效区间:`[0,k-1],[2k,3k-1],...,[nk,min(m-1,(n+1)k-1)]`，在区间中反转字符串即可。

```ts
function reverseStr(s: string, k: number): string {
  const strs = s.split('');
  const m = strs.length;
  // 反转区间内的字符串
  const reverse = (idx: number) => {
    let l = idx;
    let r = Math.min(idx + k - 1, m - 1);
    while (l < r) {
      [strs[l], strs[r]] = [strs[r], strs[l]];
      l++, r--;
    }
  };
  // 分割出需要反转的区间
  for (let i = 0; i < m; i += 2 * k) {
    reverse(i);
  }

  return strs.join('');
}
```
