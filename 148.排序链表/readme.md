# 题目
给你链表的头结点 `head` ，请将其按 **升序** 排列并返回 排序后的链表 。

进阶：  
你可以在 O(n log n) 时间复杂度和常数级空间复杂度下，对链表进行排序吗？

提示：
+ 链表中节点的数目在范围 [0, 5 * 104] 内
+ -105 <= Node.val <= 105

# 示例
[![gNW7Je.png](https://z3.ax1x.com/2021/05/10/gNW7Je.png)](https://imgtu.com/i/gNW7Je)
```
输入：head = [4,2,1,3]
输出：[1,2,3,4]
```

# 题解
## 递归
先取到原链表中最小数 `l` 和最大数 `r`，定义 `mid=(r+l)/2`。

每次将原链表中小于 `mid` 的节点放入到 `h1` 链表中, 剩下的放入 `h2` 链表中，最后合并 `h1` 和 `h2`。

递归操作上面2步，直到链表有序。

```ts
function sortList(head: ListNode | null): ListNode | null {
  if (head === null) return head;

  let p = head;
  let q = null;
  // 小于mid的链表
  let h1 = null;
  // 大于mid的链表
  let h2 = null;

  let l = p.val;
  let r = p.val;
  while (p) {
    l = Math.min(l, p.val);
    r = Math.max(r, p.val);
    p = p.next;
  }
  // 节点的值都一样，无需排序
  if (l === r) return head;

  let mid = l + (r - l) / 2;

  p = head;
  // 将链表分成2份，小于mid和大于等于mid
  while (p) {
    q = p.next;
    if (p.val < mid) {
      p.next = h1;
      h1 = p;
    } else {
      p.next = h2;
      h2 = p;
    }
    p = q;
  }

  h1 = sortList(h1);
  h2 = sortList(h2);
  p = h1;
  // 将h2接到h1后面，形成完整链表
  while (p.next) {
    p = p.next;
  }
  p.next = h2;

  return h1;
}
```

## 归并排序
计算获得节点的总数，将节点按照数量分成两组。剩下的按照归并排序的方法，分组合并排序即可。
```ts
function sortList(head: ListNode | null): ListNode | null {
  if (head === null) return head;
  // 节点的数量
  let n = 0;
  let p = head;
  while (p) {
    p = p.next;
    n++;
  }

  const merge = (head: ListNode | null, n: number): ListNode | null => {
    if (n <= 1) return head;
    let lCount = n >> 1;
    let rCount = n - lCount;

    let p = head;
    // 找到左边的最后一个节点，需要将左右分开
    for (let i = 1; i < lCount; i++) {
      p = p.next;
    }
    let lNode = head;
    let rNode = null;
    // 分成左右2组节点
    rNode = p.next;
    p.next = null;

    // 分治
    lNode = merge(lNode, lCount);
    rNode = merge(rNode, rCount);

    // 合并
    const dumpy = new ListNode();
    p = dumpy;
    while (lNode || rNode) {
      if (rNode === null || (lNode && lNode.val <= rNode.val)) {
        p.next = lNode;
        p = p.next;
        lNode = lNode.next;
      } else {
        p.next = rNode;
        p = p.next;
        rNode = rNode.next;
      }
    }
    return dumpy.next;
  };

  return merge(head, n);
}
**```**