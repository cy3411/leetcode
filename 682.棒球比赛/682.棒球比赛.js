/*
 * @lc app=leetcode.cn id=682 lang=javascript
 *
 * [682] 棒球比赛
 */

// @lc code=start
/**
 * @param {string[]} ops
 * @return {number}
 */
var calPoints = function (ops) {
  const stack = [];
  for (let i = 0; i < ops.length; i++) {
    const char = ops[i];
    switch (char) {
      case '+':
        stack.push(stack[stack.length - 1] + stack[stack.length - 2]);
        break;
      case 'D':
        stack.push(stack[stack.length - 1] * 2);
        break;
      case 'C':
        stack.pop();
        break;
      default:
        stack.push(Number(char));
        break;
    }
  }
  return stack.reduce((prev, curr) => prev + curr);
};
// @lc code=end
