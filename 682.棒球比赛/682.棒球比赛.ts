/*
 * @lc app=leetcode.cn id=682 lang=typescript
 *
 * [682] 棒球比赛
 */

// @lc code=start
function calPoints(ops: string[]): number {
  const n = ops.length;
  const stk = [];
  for (const op of ops) {
    if (op === 'C') {
      stk.pop();
    } else if (op === 'D') {
      stk.push(stk[stk.length - 1] * 2);
    } else if (op === '+') {
      stk.push(stk[stk.length - 1] + stk[stk.length - 2]);
    } else {
      stk.push(Number(op));
    }
  }

  let ans = 0;
  for (const n of stk) {
    ans += n;
  }

  return ans;
}
// @lc code=end
