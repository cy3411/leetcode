/*
 * @lc app=leetcode.cn id=682 lang=cpp
 *
 * [682] 棒球比赛
 */

// @lc code=start
class Solution
{
public:
    int calPoints(vector<string> &ops)
    {
        int n = ops.size();
        stack<int> s;
        for (auto op : ops)
        {
            if (op == "+")
            {
                int a = s.top();
                s.pop();
                int b = s.top();
                s.pop();
                s.push(b);
                s.push(a);
                s.push(a + b);
            }
            else if (op == "C")
            {
                s.pop();
            }
            else if (op == "D")
            {
                s.push(s.top() * 2);
            }
            else
            {
                s.push(stoi(op));
            }
        }

        int ans = 0;
        while (!s.empty())
        {
            int n = s.top();
            s.pop();
            ans += n;
        }

        return ans;
    }
};
// @lc code=end
