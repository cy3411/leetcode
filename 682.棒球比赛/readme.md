# 题目

你现在是一场采用特殊赛制棒球比赛的记录员。这场比赛由若干回合组成，过去几回合的得分可能会影响以后几回合的得分。

比赛开始时，记录是空白的。你会得到一个记录操作的字符串列表 ops，其中 ops[i] 是你需要记录的第 i 项操作，ops 遵循下述规则：

- 整数 x - 表示本回合新获得分数 x
- "+" - 表示本回合新获得的得分是前两次得分的总和。题目数据保证记录此操作时前面总是存在两个有效的分数。
- "D" - 表示本回合新获得的得分是前一次得分的两倍。题目数据保证记录此操作时前面总是存在一个有效的分数。
- "C" - 表示前一次得分无效，将其从记录中移除。题目数据保证记录此操作时前面总是存在一个有效的分数。
  请你返回记录中所有得分的总和。

提示：

- 1 <= ops.length <= 1000
- ops[i] 为 "C"、"D"、"+"，或者一个表示整数的字符串。整数范围是 [-3 * 104, 3 * 104]
- 对于 "+" 操作，题目数据保证记录此操作时前面总是存在两个有效的分数
- 对于 "C" 和 "D" 操作，题目数据保证记录此操作时前面总是存在一个有效的分数

# 示例

```
输入：ops = ["5","2","C","D","+"]
输出：30
解释：
"5" - 记录加 5 ，记录现在是 [5]
"2" - 记录加 2 ，记录现在是 [5, 2]
"C" - 使前一次得分的记录无效并将其移除，记录现在是 [5].
"D" - 记录加 2 * 5 = 10 ，记录现在是 [5, 10].
"+" - 记录加 5 + 10 = 15 ，记录现在是 [5, 10, 15].
所有得分的总和 5 + 10 + 15 = 30
```

# 方法

由题可以看出，我们需要根据操作符性质对操作符之前的分数计算，所以可以用栈来实现。

```js
/**
 * @param {string[]} ops
 * @return {number}
 */
var calPoints = function (ops) {
  const stack = [];
  for (let i = 0; i < ops.length; i++) {
    const char = ops[i];
    switch (char) {
      case '+':
        stack.push(stack[stack.length - 1] + stack[stack.length - 2]);
        break;
      case 'D':
        stack.push(stack[stack.length - 1] * 2);
        break;
      case 'C':
        stack.pop();
        break;
      default:
        stack.push(Number(char));
        break;
    }
  }
  return stack.reduce((prev, curr) => prev + curr);
};
```

```ts
function calPoints(ops: string[]): number {
  const n = ops.length;
  const stk = [];
  for (const op of ops) {
    if (op === 'C') {
      stk.pop();
    } else if (op === 'D') {
      stk.push(stk[stk.length - 1] * 2);
    } else if (op === '+') {
      stk.push(stk[stk.length - 1] + stk[stk.length - 2]);
    } else {
      stk.push(Number(op));
    }
  }

  let ans = 0;
  for (const n of stk) {
    ans += n;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int calPoints(vector<string> &ops)
    {
        int n = ops.size();
        stack<int> s;
        for (auto op : ops)
        {
            if (op == "+")
            {
                int a = s.top();
                s.pop();
                int b = s.top();
                s.pop();
                s.push(b);
                s.push(a);
                s.push(a + b);
            }
            else if (op == "C")
            {
                s.pop();
            }
            else if (op == "D")
            {
                s.push(s.top() * 2);
            }
            else
            {
                s.push(stoi(op));
            }
        }

        int ans = 0;
        while (!s.empty())
        {
            int n = s.top();
            s.pop();
            ans += n;
        }

        return ans;
    }
};
```
