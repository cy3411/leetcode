/*
 * @lc app=leetcode.cn id=168 lang=typescript
 *
 * [168] Excel表列名称
 */

// @lc code=start
function convertToTitle(columnNumber: number): string {
  const ans = [];

  while (columnNumber > 0) {
    // 进位前，需要对columnNumber减1
    const a = ((columnNumber - 1) % 26) + 1;
    ans.push(String.fromCharCode(a - 1 + 'A'.charCodeAt(0)));
    columnNumber = Math.floor((columnNumber - a) / 26);
  }

  return ans.reverse().join('');
}
// @lc code=end
