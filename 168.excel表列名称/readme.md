# 题目

给你一个整数 `columnNumber` ，返回它在 Excel 表中相对应的列名称。

例如：

```
A -> 1
B -> 2
C -> 3
...
Z -> 26
AA -> 27
AB -> 28
...
```

# 示例

```
输入：columnNumber = 1
输出："A"
```

```
输入：columnNumber = 28
输出："AB"
```

# 题解

## 数学计算

题目就是 26 进制的计算题，只是这里是从 1 开始，所以进制转换前，我们需要对 columnNumber 进行减 1 的操作，从而实现整体偏移。

```ts
function convertToTitle(columnNumber: number): string {
  const ans = [];

  while (columnNumber > 0) {
    // 进位前，需要对columnNumber减1
    const a = ((columnNumber - 1) % 26) + 1;
    ans.push(String.fromCharCode(a - 1 + 'A'.charCodeAt(0)));
    columnNumber = Math.floor((columnNumber - a) / 26);
  }

  return ans.reverse().join('');
}
```
