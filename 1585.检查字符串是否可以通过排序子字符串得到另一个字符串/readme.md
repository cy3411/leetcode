# 题目

给你两个字符串 `s` 和 `t` ，请你通过若干次以下操作将字符串 `s` 转化成字符串 `t` ：

- 选择 `s` 中一个 **非空** 子字符串并将它包含的字符就地 **升序** 排序。

比方说，对下划线所示的子字符串进行操作可以由 `"14234"` 得到 `"12344"` 。

如果可以将字符串 `s` 变成 `t` ，返回 `true` 。否则，返回 `false` 。

一个 **子字符串** 定义为一个字符串中连续的若干字符。

提示：

- $s.length \equiv t.length$
- $1 \leq s.length \leq 10^5$
- `s` 和 `t` 都只包含数字字符，即 `'0'` 到 `'9'` 。

# 示例

```
输入：s = "84532", t = "34852"
输出：true
解释：你可以按以下操作将 s 转变为 t ：
"84532" （从下标 2 到下标 3）-> "84352"
"84352" （从下标 0 到下标 2） -> "34852"
```

```
输入：s = "34521", t = "23415"
输出：true
解释：你可以按以下操作将 s 转变为 t ：
"34521" -> "23451"
"23451" -> "23415"
```

```
输入：s = "12345", t = "12435"
输出：false
```

# 题解

## 模拟

验证字符串是否可以通过排序子字符串得到另一个字符串，判断条件如下：

- t 中的字符串长度与 s 中的字符串长度相同
- 题目要求升序排序来进行转化，那么 t 的高位的数字，在 s 中的位置之前不能出现比它小的数字

我们预处理 s 中每个数字出现的下标。然后遍历 t，当前数字在 s 中，如果它前面出现了比它小的数字，就返回 false

```ts
function isTransformable(s: string, t: string): boolean {
  // s 字符串中每个数字的下标
  const pos: number[][] = new Array(10).fill(0).map(() => []);
  for (let i = 0; i < s.length; i++) {
    pos[Number(s[i])].push(i);
  }

  // 检查 t 中每个数字是否可以通过排序 s 中的数字得到
  for (let i = 0; i < t.length; i++) {
    const num = Number(t[i]);
    // 数字不在 s 中
    if (pos[num].length === 0) {
      return false;
    }
    // 数字在 s 中的下标
    const p = pos[num].shift();
    // 检查当前数字前在 s 中是否还有比它小的数字
    for (let j = 0; j < num; j++) {
      if (pos[j].length && pos[j][0] < p) return false;
    }
  }

  return true;
}
```

```cpp
class Solution
{
public:
    bool isTransformable(string s, string t)
    {
        vector<queue<int>> pos(10);
        for (int i = 0; s[i]; i++)
        {
            pos[s[i] - '0'].push(i);
        }

        for (int i = 0; t[i]; i++)
        {
            int num = t[i] - '0';
            if (pos[num].empty())
            {
                return false;
            }
            int p = pos[num].front();
            for (int j = 0; j < num; j++)
            {
                if (!pos[j].empty() && pos[j].front() < p)
                {
                    return false;
                }
            }
            pos[num].pop();
        }

        return true;
    }
};
```
