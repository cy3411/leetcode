# 题目

给你一个链表的头节点 `head` 和一个整数 `val` ，请你删除链表中所有满足 `Node.val == val` 的节点，并返回 **新的头节点** 。

# 示例

[![2agrgP.md.png](https://z3.ax1x.com/2021/06/06/2agrgP.md.png)](https://imgtu.com/i/2agrgP)

```
输入：head = [1,2,6,3,4,5,6], val = 6
输出：[1,2,3,4,5]
```

# 题解

## 链表

定义 `p1` 指向新的链表头，定义 `p2` 指向原链表头。

遍历原链表，如果 `p2` 的值和 `val` 相等，表示需要删除此节点。`p1` 的 `next` 的指针就跳过此节点，继续遍历。

遍历完毕，将 p1 的下一个指针指向 null 即可。

考虑到原链表头有可能会被修改，使用虚拟头节点，减少边界问题。

```ts
function removeElements(head: ListNode | null, val: number): ListNode | null {
  if (head === null) return head;
  const dummy = new ListNode(0, head);
  let p1 = dummy;
  let p2 = head;
  while (p2) {
    if (p2.val !== val) {
      p1.next = p2;
      p1 = p1.next;
    }
    p2 = p2.next;
  }

  // 遍历结束，将p1的下一个指针指向null
  p1.next = null;

  return dummy.next;
}
```
