// @test([3,2,3])=3
// @algorithm @lc id=1000038 lang=javascript
// @title find-majority-element-lcci
// @test([1,2,5,9,5,9,5,5,5])=5
// @test([3,2])=-1
// @test([2,2,1,1,1,2,2])=2
/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function (nums) {
  let curr = -1;
  let count = 0;
  // 每次消除数组中两2个不同的元素，最后剩下的最有可能是主要元素
  for (let num of nums) {
    if (count === 0) {
      curr = num;
    }
    if (curr === num) {
      count++;
    } else {
      count--;
    }
  }
  // 检查最后剩下的是否是主要元素
  count = 0;
  for (let num of nums) {
    if (curr === num) {
      count++;
    }
  }

  return 2 * count > nums.length ? curr : -1;
};
