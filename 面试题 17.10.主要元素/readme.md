# 题目

数组中占比超过一半的元素称之为主要元素。给你一个 整数 数组，找出其中的主要元素。若没有，返回 -1 。请设计时间复杂度为 O(N) 、空间复杂度为 O(1) 的解决方案。

# 示例

```
输入：[1,2,5,9,5,9,5,5,5]
输出：5
```

# 题解

## 哈希表

利用哈希表统计所有数字出现的次数，最后遍历哈希表，更新答案。

```ts
var majorityElement = function (nums) {
  const m = nums.length;
  const half = m >> 1;
  const hash = new Map();
  // 统计数量
  for (let num of nums) {
    hash.set(num, (hash.get(num) || 0) + 1);
  }
  // 更新答案
  let ans = -1;
  for (let [num, count] of Array.from(hash)) {
    if (count > half) {
      max = count;
      ans = num;
    }
  }

  return ans;
};
```

## 摩尔投票

题目要求是 O(1)的复杂度，所以这里不能使用哈希表来实现。

我们可以考虑每个过程，将数组中 2 个不同的元素删除，最后剩下的元素最后肯定是主要元素。

遍历数组，统计最后剩下元素的数量，超过数组长度，就是答案。

```ts
var majorityElement = function (nums) {
  let curr = -1;
  let count = 0;
  // 每次消除数组中两2个不同的元素，最后剩下的最有可能是主要元素
  for (let num of nums) {
    if (count === 0) {
      curr = num;
    }
    if (curr === num) {
      count++;
    } else {
      count--;
    }
  }
  // 检查最后剩下的是否是主要元素
  count = 0;
  for (let num of nums) {
    if (curr === num) {
      count++;
    }
  }

  return 2 * count > nums.length ? curr : -1;
};
```
