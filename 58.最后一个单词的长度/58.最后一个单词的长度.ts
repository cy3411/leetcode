/*
 * @lc app=leetcode.cn id=58 lang=typescript
 *
 * [58] 最后一个单词的长度
 */

// @lc code=start
function lengthOfLastWord(s: string): number {
  // 分割字符串
  const words = s.split(' ');
  let ans = 0;

  const n = words.length;
  // 计算最后一个非空的字符串的长度
  for (let i = n - 1; i >= 0; i--) {
    if (words[i] === '') continue;
    ans = words[i].length;
    break;
  }

  return ans;
}
// @lc code=end
