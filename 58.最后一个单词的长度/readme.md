# 题目

给你一个字符串 `s`，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中最后一个单词的长度。

**单词** 是指仅由字母组成、不包含任何空格字符的最大子字符串。

提示：

- `1 <= s.length <= 104`
- `s` 仅有英文字母和空格 `' '` 组成
- `s` 中至少存在一个单词

# 示例

```
输入：s = "   fly me   to   the moon  "
输出：4
```

# 题解

## 统计

分割字符串，返回最后一个非空字符串的长度。

```ts
function lengthOfLastWord(s: string): number {
  // 分割字符串
  const words = s.split(' ');
  let ans = 0;

  const n = words.length;
  // 计算最后一个非空的字符串的长度
  for (let i = n - 1; i >= 0; i--) {
    if (words[i] === '') continue;
    ans = words[i].length;
    break;
  }

  return ans;
}
```
