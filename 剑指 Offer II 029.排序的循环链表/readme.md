# 题目

给定循环单调非递减列表中的一个点，写一个函数向这个列表中插入一个新元素 insertVal ，使这个列表仍然是循环升序的。

给定的可以是这个列表中任意一个顶点的指针，并不一定是这个列表中最小元素的指针。

如果有多个满足条件的插入位置，可以选择任意一个位置插入新的值，插入后整个列表仍然保持有序。

如果列表为空（给定的节点是 null），需要创建一个循环有序列表并返回这个节点。否则。请返回原先给定的节点。

提示：

- $\color{burlywood}0 \leq Number of Nodes \leq 5 * 10^4$
- $\color{burlywood}-10^6 \leq Node.val \leq 10^6$
- $\color{burlywood}-10^6 \leq insertVal \leq 10^6$

# 示例

[![7QfCX8.png](https://s4.ax1x.com/2022/01/13/7QfCX8.png)](https://imgtu.com/i/7QfCX8)

```
输入：head = [3,4,1], insertVal = 2
输出：[3,4,1,2]
解释：在上图中，有一个包含三个元素的循环有序列表，你获得值为 3 的节点的指针，我们需要向表中插入元素 2 。新插入的节点应该在 1 和 3 之间，插入之后，整个列表如下图所示，最后返回节点 3
```

[![7QfucV.png](https://s4.ax1x.com/2022/01/13/7QfucV.png)](https://imgtu.com/i/7QfucV)

# 题解

## 链表插入

链表插入需要先找到当前插入位置的前驱节点，然后插入新节点。

遍历整个链表，令 p 为当前节点，找到 `p.val <= insertVal && insertVal <= p.next.val` 的节点，即为插入位置。

如果遍历一圈后都没找到，表示 insertVal 大于所有节点或者小于所有节点，由于是一个循环队列，直接插入到最后即可。

```ts
function insert(head: Node | null, insertVal: number): Node | null {
  let p = head;
  let maxNode = head;

  while (p) {
    // 找到最大的节点
    if (p.val > p.next.val) {
      maxNode = p;
    }
    // 找到合适的插入位置
    if (p.val <= insertVal && insertVal <= p.next.val) {
      break;
    }
    // 找了一圈，列表中间没有合适的插入位置，插入到最后
    if (p.next === head) {
      p = maxNode;
      break;
    }
    p = p.next;
  }

  if (p) {
    // 在p后面构建节点
    p.next = new Node(insertVal, p.next);
  } else {
    // head为null，构建节点
    head = new Node(insertVal);
    head.next = head;
  }

  return head;
}
```
