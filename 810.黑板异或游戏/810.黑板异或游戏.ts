/*
 * @lc app=leetcode.cn id=810 lang=typescript
 *
 * [810] 黑板异或游戏
 */

// @lc code=start
function xorGame(nums: number[]): boolean {
  if (nums.length % 2 === 0) return true;
  let xor = 0;
  for (let num of nums) {
    xor ^= num;
  }

  return xor === 0;
}
// @lc code=end
