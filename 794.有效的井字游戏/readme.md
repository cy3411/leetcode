# 题目

给你一个字符串数组 `board` 表示井字游戏的棋盘。当且仅当在井字游戏过程中，棋盘有可能达到 `board` 所显示的状态时，才返回 `true` 。

井字游戏的棋盘是一个 `3 x 3` 数组，由字符 `' '`，`'X'` 和 `'O'` 组成。字符 `' '` 代表一个空位。

以下是井字游戏的规则：

- 玩家轮流将字符放入空位（`' '`）中。
- 玩家 1 总是放字符 `'X'` ，而玩家 2 总是放字符 `'O'` 。
- `'X'` 和 `'O'` 只允许放置在空位中，不允许对已放有字符的位置进行填充。
- 当有 3 个相同（且非空）的字符填充任何行、列或对角线时，游戏结束。
- 当所有位置非空时，也算为游戏结束。
- 如果游戏结束，玩家不允许再放置字符。

提示：

- `board.length == 3`
- `board[i].length == 3`
- `board[i][j]` 为 `'X'`、`'O'` 或 `' '`

# 示例

[![oII5NQ.png](https://s1.ax1x.com/2021/12/10/oII5NQ.png)](https://imgtu.com/i/oII5NQ)

```
输入：board = ["O  ","   ","   "]
输出：false
解释：玩家 1 总是放字符 "X" 。
```

[![oIIXHU.png](https://s1.ax1x.com/2021/12/10/oIIXHU.png)](https://imgtu.com/i/oIIXHU)

```
输入：board = ["XOX"," X ","   "]
输出：false
解释：玩家应该轮流放字符。
```

# 题解

## 分类讨论

提议就是判断最后的 `board` 上的状态是否合法。

状态是否合法， 可以分为三种情况：

- 'X' 先手，所以最后'X'的数量肯定能够等于'O'的数量或者等于'O'的数量+1
- 如果 'X' 获胜，那么棋盘上肯定有连续的 3 个'X',并且'X'的数量肯定等于'O'的数量+1
- 如果'O'获胜，那么棋盘上肯定有联系的 3 个'O',并且'O'的数量肯定等于'X'的数量

```ts
function validTicTacToe(board: string[]): boolean {
  const n = board.length;
  let xCount = 0;
  let oCount = 0;
  const isWin = (char: string): boolean => {
    // 横竖连续3个相等，表示字符是char的一方获胜
    for (let i = 0; i < n; i++) {
      if (board[0][i] === char && board[1][i] === char && board[2][i] === char) return true;

      if (board[i][0] === char && board[i][1] === char && board[i][2] === char) return true;
    }
    // 斜线连续3个相等，表示字符是char的一方获胜
    if (board[0][0] === char && board[1][1] === char && board[2][2] === char) return true;
    if (board[0][2] === char && board[1][1] === char && board[2][0] === char) return true;
    return false;
  };
  // 计算棋盘x和o的个数
  // x先手，所以x>=o
  for (let row of board) {
    for (let char of row) {
      if (char === 'X') {
        xCount++;
      } else if (char === 'O') {
        oCount++;
      }
    }
  }
  // 判断x和o的个数是否合法
  if (xCount !== oCount && xCount !== oCount + 1) return false;
  // 如果x获胜，那么棋盘上x比o多一个
  if (isWin('X') && xCount !== oCount + 1) return false;
  // 如果o获胜，那么棋盘上x和o数量相等
  if (isWin('O') && xCount !== oCount) return false;

  return true;
}
```
