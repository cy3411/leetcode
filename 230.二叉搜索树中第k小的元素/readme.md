# 题目

给定一个二叉搜索树的根节点 `root` ，和一个整数 `k` ，请你设计一个算法查找其中第 `k` 个最小元素（从 `1` 开始计数）。

# 示例

```
输入：root = [3,1,4,null,2], k = 1
输出：1
```

# 题解

二叉树的中序遍历结果是递增序列。

中序遍历二叉树，将第 `k` 次访问的节点值放入答案。

```ts
function kthSmallest(root: TreeNode | null, k: number): number {
  let ans: number;
  const inorder = (root: TreeNode | null) => {
    if (root === null) return;
    inorder(root.left);
    if (--k === 0) {
      ans = root.val;
    }
    inorder(root.right);
  };

  inorder(root);
  return ans;
}
```
