/*
 * @lc app=leetcode.cn id=334 lang=cpp
 *
 * [334] 递增的三元子序列
 */

// @lc code=start
class Solution
{
public:
    bool increasingTriplet(vector<int> &nums)
    {
        int first = INT_MAX, second = INT_MAX;

        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] <= first)
            {
                first = nums[i];
            }
            else if (nums[i] <= second)
            {
                second = nums[i];
            }
            else
            {
                return true;
            }
        }

        return false;
    }
};
// @lc code=end
