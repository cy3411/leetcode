/*
 * @lc app=leetcode.cn id=334 lang=typescript
 *
 * [334] 递增的三元子序列
 */

// @lc code=start
function increasingTriplet(nums: number[]): boolean {
  let first = Number.POSITIVE_INFINITY;
  let second = Number.POSITIVE_INFINITY;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] <= first) {
      // 如果当前数小于等于first，则first取当前数
      first = nums[i];
    } else if (nums[i] <= second) {
      // 如果当前数小于等于second，则second取当前数
      second = nums[i];
    } else {
      // 如果当前数大于second，则返回true
      return true;
    }
  }

  return false;
}
// @lc code=end
