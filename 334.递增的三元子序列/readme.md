# 题目

给你一个整数数组 `nums` ，判断这个数组中是否存在长度为 `3` 的递增子序列。

如果存在这样的三元组下标 `(i, j, k)` 且满足 `i < j < k` ，使得 `nums[i] < nums[j] < nums[k]` ，返回 `true` ；否则，返回 `false` 。

提示：

- $\color{burlywood}1 \leq nums.length \leq 5 * 10^5$
- $\color{burlywood}-2^{31} \leq nums[i] \leq 2^{31} - 1$

进阶：你能实现时间复杂度为 `O(n)` ，空间复杂度为 `O(1)` 的解决方案吗？

# 示例

```
输入：nums = [1,2,3,4,5]
输出：true
解释：任何 i < j < k 的三元组都满足题意
```

```
输入：nums = [5,4,3,2,1]
输出：false
解释：不存在满足题意的三元组
```

```
输入：nums = [2,1,5,0,4,6]
输出：true
解释：三元组 (3, 4, 5) 满足题意，因为 nums[3] == 0 < nums[4] == 4 < nums[5] == 6
```

# 题解

## 贪心

遍历数组，过程中维护两个变量 `first` 和 `second`，分别表示三元组中的第一个和第二个数字。

first 和 second 初始化为 $+\infty$，遍历过程中，

- 如果 $\color{burlywood}nums[i] \leq first$，则 `first = nums[i]`
- 否则 $\color{burlywood}nums[i] \leq second$，则 `second = nums[i]`
- 否则，三元组满足题意，返回 `true`

```ts
function increasingTriplet(nums: number[]): boolean {
  let first = Number.POSITIVE_INFINITY;
  let second = Number.POSITIVE_INFINITY;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] <= first) {
      // 如果当前数小于等于first，则first取当前数
      first = nums[i];
    } else if (nums[i] <= second) {
      // 如果当前数小于等于second，则second取当前数
      second = nums[i];
    } else {
      // 如果当前数大于second，则返回true
      return true;
    }
  }

  return false;
}
```

```cpp
class Solution
{
public:
    bool increasingTriplet(vector<int> &nums)
    {
        int first = INT_MAX, second = INT_MAX;

        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] <= first)
            {
                first = nums[i];
            }
            else if (nums[i] <= second)
            {
                second = nums[i];
            }
            else
            {
                return true;
            }
        }

        return false;
    }
};
```
