# 题目

给定一个整数数组 `nums` ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

# 示例

```
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
```

```
输入：nums = [1]
输出：1
```

# 题解

## 前缀和

求数组的最大区间和，我们可以使用前缀和数组。

最大的区间和，必然是左边的最小值和右边的最大值的相减结果。

定义 `pre = 0`，动态更新迭代扫描过的最小前缀和，持续更新结果，取最大值即可。

```ts
function maxSubArray(nums: number[]): number {
  const size = nums.length;
  if (size === 1) return nums[0];
  const preSum = new Array(size + 1).fill(0);
  // 计算前缀和
  for (let i = 0; i < size; i++) {
    preSum[i + 1] = nums[i] + preSum[i];
  }

  let result = preSum[1];
  // 前一个的最小值
  let pre = 0;
  for (let i = 1; i <= size; i++) {
    result = Math.max(result, preSum[i] - pre);
    pre = Math.min(pre, preSum[i]);
  }

  return result;
}
```

```ts
function maxSubArray(nums: number[]): number {
  const m = nums.length;
  // 前缀和
  for (let i = 1; i < m; i++) {
    nums[i] = nums[i] + nums[i - 1];
  }

  // 记录前面最小的值
  let pre = 0;
  let ans = Number.NEGATIVE_INFINITY;
  for (let num of nums) {
    ans = Math.max(ans, num - pre);
    pre = Math.min(pre, num);
  }

  return ans;
}
```

## 动态规划

**状态**  
定义 `dp[i]` 为 `i` 个数为结尾的最大子序和。那么我们的答案就是 `dp[i]` 中的最大值。

**选择**  
$dp[i] = max(dp[]i-1], dp[i-1] + num [i -1])$

**BaseCase**  
`dp[0]` 给一个极小的值。

```ts
function maxSubArray(nums: number[]): number {
  const size = nums.length;
  if (size === 1) return nums[0];

  const dp = new Array(size + 1).fill((-10) ** 5 - 1);
  for (let i = 1; i < dp.length; i++) {
    dp[i] = Math.max(nums[i - 1], dp[i - 1] + nums[i - 1]);
  }

  return Math.max(...dp);
}
```

上面的代码可以看出，每次转移的时候只用到了 `dp[i-1]`。我们可以使用一个变量 `pre` 来维护这个值，可以降低空间复杂度。

```ts
function maxSubArray(nums: number[]): number {
  let pre = 0;
  let result = nums[0];

  for (let num of nums) {
    pre = Math.max(pre + num, num);
    result = Math.max(result, pre);
  }

  return result;
}
```
