/*
 * @lc app=leetcode.cn id=53 lang=typescript
 *
 * [53] 最大子序和
 */

// @lc code=start
function maxSubArray(nums: number[]): number {
  const m = nums.length;
  // 前缀和
  for (let i = 1; i < m; i++) {
    nums[i] = nums[i] + nums[i - 1];
  }

  // 记录前面最小的值
  let pre = 0;
  let ans = Number.NEGATIVE_INFINITY;
  for (let num of nums) {
    ans = Math.max(ans, num - pre);
    pre = Math.min(pre, num);
  }

  return ans;
}
// @lc code=end
