/*
 * @lc app=leetcode.cn id=861 lang=javascript
 *
 * [861] 翻转矩阵后的得分
 */

// @lc code=start
/**
 * @param {number[][]} A
 * @return {number}
 */
var matrixScore = function (A) {
  let m = A.length;
  let n = A[0].length;
  // 最优情况下，第一位数字都为1
  let result = m * (1 << (n - 1));

  for (let j = 1; j < n; j++) {
    // 统计其他列出现1的次数
    let count = 0;
    for (let i = 0; i < m; i++) {
      if (A[i][0] === 1) {
        count += A[i][j];
      } else {
        count += 1 - A[i][j];
      }
    }
    // 出现1的次数和反转后1的次数取最大值
    const k = Math.max(count, m - count);
    result += k * (1 << (n - j - 1));
  }

  return result;
};
// @lc code=end
