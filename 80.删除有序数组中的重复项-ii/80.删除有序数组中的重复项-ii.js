/*
 * @lc app=leetcode.cn id=80 lang=javascript
 *
 * [80] 删除有序数组中的重复项 II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function (nums) {
  const size = nums.length;
  if (size <= 2) return size;

  let slow = 2;
  for (let i = 2; i < size; i++) {
    if (nums[slow - 2] !== nums[i]) {
      nums[slow] = nums[i];
      slow++;
    }
  }

  return slow;
};
// @lc code=end
