# 题目

给定正整数 `n`，找到若干个完全平方数（比如 `1, 4, 9, 16, ...`）使得它们的和等于 `n`。你需要让组成和的完全平方数的个数最少。

给你一个整数 `n` ，返回和为 `n` 的完全平方数的 **最少数量** 。

**完全平方数** 是一个整数，其值等于另一个整数的平方；换句话说，其值等于一个整数自乘的积。例如，`1、4、9` 和 `16` 都是完全平方数，而 `3` 和 `11` 不是。

# 示例

```
输入：n = 12
输出：3
解释：12 = 4 + 4 + 4
```

# 题解

## 动态规划

可以先将小于等于 `n` 的完全平方数提取出来。这样问题就转换成给出若干**完全平方数**，每个数字的使用**次数不限制**，求凑成 `n` 的数字的**最小数量**。

**状态**

定义 `dp[i][j]`，表示前 `i` 个数组能凑出和为 `j`，所需要用的最小数字数量

**base case**

- `dp[0][0]=0`，`0` 个数字能凑出和为 0 所需要的数字为 `0` 个。
- `dp[0][j]=INF`，`0` 个数字不可能凑出和为 `j` 的方案，都是无效的。

**选择**
对于第 i 个数字(假设为 t)，我们可以如下选择：

- 不选择，`dp[i][j] = dp[i-1][j]`
- 选择 1 次，`dp[i][j] = dp[i-1][j-t]+1`
- 选择 2 次，`dp[i][j] = dp[i-1][j-t*2]+2`
- ...
- 选择 k 次，`dp[i][j] = dp[i-1][j-t*k]+k`

$$
dp[i][j] = min(dp[i][j], dp[i-1][j-t*k]+k) \text{,0<=k<=j}
$$

```ts
function numSquares(n: number): number {
  // 将平方数取出
  const squareNums: number[] = [];
  let t = 1;
  while (t * t <= n) {
    squareNums.push(t * t);
    t++;
  }

  const m = squareNums.length;
  // 前i个物品凑成j的最小方案数
  const dp: number[][] = new Array(m + 1).fill(0).map(() => new Array(n + 1).fill(0));
  // 0个物品凑成0个物品的方案数为0，其他都是无效的
  dp[0].fill(Number.POSITIVE_INFINITY);
  dp[0][0] = 0;

  for (let i = 1; i <= m; i++) {
    let x = squareNums[i - 1];
    for (let j = 0; j <= n; j++) {
      dp[i][j] = dp[i - 1][j];
      for (let k = 1; k * x <= j; k++) {
        // 这里需要判断之前的选择是否合法
        if (dp[i - 1][j - k * x] !== Number.POSITIVE_INFINITY) {
          dp[i][j] = Math.min(dp[i][j], dp[i - 1][j - k * x] + k);
        }
      }
    }
  }

  return dp[m][n];
}
```

## 广度优先

我们可以将计算过程抽象成一颗 `n` 叉数，每个节点的值都是根节点到当前节点的累加和。

这样我们一层一层计算累加下去，直到碰到第一个满足条件的结果，那么当前层数就是结果。

```ts
function numSquares(n: number): number {
  // 从0开始，逐行搜索下去，直到找到和为n的层数
  const queue: number[] = [0];
  // 记录访问过的节点
  const hash: Set<number> = new Set([0]);

  let ans = 0;
  while (queue.length) {
    ans++;
    const size = queue.length;
    for (let i = 0; i < size; i++) {
      // 拿出值来计算
      const num = queue.shift();
      for (let j = 1; j <= n; j++) {
        // value始终是平方和
        const value = num + j * j;
        // 等于n就是找到了，直接返回
        if (value === n) return ans;
        // 大于n了，当前数字不适合，终止循环
        if (value > n) break;
        // 记录到已经计算过的值，hash防止重复计算
        if (!hash.has(value)) {
          queue.push(value);
          hash.add(value);
        }
      }
    }
  }

  return ans;
}
```
