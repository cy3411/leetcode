/*
 * @lc app=leetcode.cn id=2100 lang=typescript
 *
 * [2100] 适合打劫银行的日子
 */

// @lc code=start
function goodDaysToRobBank(security: number[], time: number): number[] {
  const n = security.length;
  // 当前元素左边连续非递增的元素个数
  const ldp = new Array(n).fill(0);
  // 当前元素右边连续非递减的元素个数
  const rdp = new Array(n).fill(0);
  for (let i = 1; i < n; i++) {
    // 顺序更新ldp
    if (security[i] <= security[i - 1]) {
      ldp[i] = ldp[i - 1] + 1;
    }
    // 逆序更新rdp
    if (security[n - 1 - i] <= security[n - i]) {
      rdp[n - 1 - i] = rdp[n - i] + 1;
    }
  }

  const ans = [];
  // 答案在[time, n-time)区间
  for (let i = time; i < n - time; i++) {
    if (ldp[i] >= time && rdp[i] >= time) {
      ans.push(i);
    }
  }

  return ans;
}
// @lc code=end
