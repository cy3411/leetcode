# 题目

你和一群强盗准备打劫银行。给你一个下标从 `0` 开始的整数数组 `security` ，其中 `security[i]` 是第 `i` 天执勤警卫的数量。日子从 `0` 开始编号。同时给你一个整数 `time` 。

如果第 `i` 天满足以下所有条件，我们称它为一个适合打劫银行的日子：

- 第 `i` 天前和后都分别至少有 `time` 天。
- 第 `i` 天前连续 `time` 天警卫数目都是非递增的。
- 第 `i` 天后连续 `time` 天警卫数目都是非递减的。

更正式的，第 `i` 天是一个合适打劫银行的日子当且仅当：$security[i - time] >= security[i - time + 1] >= ... >= security[i] \leq ... \leq security[i + time - 1] \leq security[i + time]$.

请你返回一个数组，包含 **所有** 适合打劫银行的日子（下标从 `0` 开始）。返回的日子可以 **任意** 顺序排列。

提示：

- $1 \leq security.length \leq 10^5$
- $0 \leq security[i], time \leq 10^5$

# 示例

```
输入：security = [5,3,3,3,5,6,2], time = 2
输出：[2,3]
解释：
第 2 天，我们有 security[0] >= security[1] >= security[2] <= security[3] <= security[4] 。
第 3 天，我们有 security[1] >= security[2] >= security[3] <= security[4] <= security[5] 。
没有其他日子符合这个条件，所以日子 2 和 3 是适合打劫银行的日子。
```

```
输入：security = [1,1,1,1,1], time = 0
输出：[0,1,2,3,4]
解释：
因为 time 等于 0 ，所以每一天都是适合打劫银行的日子，所以返回每一天。
```

# 题解

## 动态规划

题目中第 i 天适合打劫的条件是：第 i 天前连续 time 天警卫数目都是非递减的，并且第 i 天后连续 time 天警卫数目都是非递增的。

我们只需要预先计算出第 i 天前连续非递减的警卫数目(假设为：$left_i$)，第 i 天后连续非递增的警卫数目(假设为：$right_i$)。当第 i 天同时满足 $left_i >= time \And right_i >= time$，则第 i 天是适合打劫银行的日子。

如何计算$left_i$：

> 当第 i 天执勤警卫的数量小于等于第 i - 1 天执勤警卫的数量时，第 i 天前连续非递减的警卫数目为第 i 天执勤警卫的数量加 1，否则为 0。

计算$right_i$同上，只是逆序遍历。

```ts
function goodDaysToRobBank(security: number[], time: number): number[] {
  const n = security.length;
  // 当前元素左边连续非递增的元素个数
  const ldp = new Array(n).fill(0);
  // 当前元素右边连续非递减的元素个数
  const rdp = new Array(n).fill(0);
  for (let i = 1; i < n; i++) {
    // 顺序更新ldp
    if (security[i] <= security[i - 1]) {
      ldp[i] = ldp[i - 1] + 1;
    }
    // 逆序更新rdp
    if (security[n - 1 - i] <= security[n - i]) {
      rdp[n - 1 - i] = rdp[n - i] + 1;
    }
  }

  const ans = [];
  // 答案在[time, n-time)区间
  for (let i = time; i < n - time; i++) {
    if (ldp[i] >= time && rdp[i] >= time) {
      ans.push(i);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> goodDaysToRobBank(vector<int> &security, int time)
    {
        int n = security.size();
        vector<int> ldp(n, 0), rdp(n, 0);
        for (int i = 1; i < n; i++)
        {
            if (security[i] <= security[i - 1])
            {
                ldp[i] = ldp[i - 1] + 1;
            }
            if (security[n - i - 1] <= security[n - i])
            {
                rdp[n - i - 1] = rdp[n - i] + 1;
            }
        }

        vector<int> ans;
        for (int i = 0; i < n; i++)
        {
            if (ldp[i] >= time && rdp[i] >= time)
            {
                ans.push_back(i);
            }
        }
        return ans;
    }
};
```
