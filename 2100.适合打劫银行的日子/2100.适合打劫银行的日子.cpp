/*
 * @lc app=leetcode.cn id=2100 lang=cpp
 *
 * [2100] 适合打劫银行的日子
 */

// @lc code=start
class Solution
{
public:
    vector<int> goodDaysToRobBank(vector<int> &security, int time)
    {
        int n = security.size();
        vector<int> ldp(n, 0), rdp(n, 0);
        for (int i = 1; i < n; i++)
        {
            if (security[i] <= security[i - 1])
            {
                ldp[i] = ldp[i - 1] + 1;
            }
            if (security[n - i - 1] <= security[n - i])
            {
                rdp[n - i - 1] = rdp[n - i] + 1;
            }
        }

        vector<int> ans;
        for (int i = 0; i < n; i++)
        {
            if (ldp[i] >= time && rdp[i] >= time)
            {
                ans.push_back(i);
            }
        }
        return ans;
    }
};
// @lc code=end
