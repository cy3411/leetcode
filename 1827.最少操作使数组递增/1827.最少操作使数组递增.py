#
# @lc app=leetcode.cn id=1827 lang=python3
#
# [1827] 最少操作使数组递增
#

# @lc code=start
class Solution:
    def minOperations(self, nums: List[int]) -> int:
        ans, pre = 0, nums[0] - 1
        for num in nums:
            pre = max(pre + 1, num)
            ans += pre - num
        return ans
# @lc code=end
