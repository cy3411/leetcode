# 题目

给你一个整数数组 `nums` （下标从 0 开始）。每一次操作中，你可以选择数组中一个元素，并将它增加 `1` 。

比方说，如果 `nums = [1,2,3]` ，你可以选择增加 `nums[1]` 得到 `nums = [1,3,3]` 。
请你返回使 `nums` 严格递增 的 最少 操作次数。

我们称数组 `nums` 是 严格递增的 ，当它满足对于所有的 `0 <= i < nums.length - 1` 都有 `nums[i] < nums[i+1]` 。一个长度为 `1` 的数组是严格递增的一种特殊情况。

提示：

- $1 <= nums.length <= 5000$
- $1 <= nums[i] <= 10^4$

# 示例

```
输入：nums = [1,1,1]
输出：3
解释：你可以进行如下操作：
1) 增加 nums[2] ，数组变为 [1,1,2] 。
2) 增加 nums[1] ，数组变为 [1,2,2] 。
3) 增加 nums[2] ，数组变为 [1,2,3] 。
```

```
输入：nums = [1,5,2,4,1]
输出：14
```

```
输入：nums = [8]
输出：0
```

# 题解

## 贪心

遍历数组，定义 pre 来记录下一个满足严格递增的数字，同时更新答案。

```ts
function minOperations(nums: number[]): number {
  let ans = 0;
  // 记录遍历过程中最大的值
  let pre = nums[0];
  for (let i = 1; i < nums.length; i++) {
    if (nums[i] > pre) {
      pre = nums[i];
    } else {
      ans += pre - nums[i] + 1;
      pre++;
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int minOperations(vector<int> &nums) {
        int ans = 0, pre = nums[0] - 1;
        for (int num : nums) {
            pre = max(pre + 1, num);
            ans += pre - num;
        }
        return ans;
    }
};
```

```py
class Solution:
    def minOperations(self, nums: List[int]) -> int:
        ans, pre = 0, nums[0] - 1
        for num in nums:
            pre = max(pre + 1, num)
            ans += pre - num
        return ans
```
