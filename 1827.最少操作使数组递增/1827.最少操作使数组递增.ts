/*
 * @lc app=leetcode.cn id=1827 lang=typescript
 *
 * [1827] 最少操作使数组递增
 */

// @lc code=start
function minOperations(nums: number[]): number {
  let ans = 0;
  // 记录遍历过程中最大的值
  let pre = nums[0];
  for (let i = 1; i < nums.length; i++) {
    if (nums[i] > pre) {
      pre = nums[i];
    } else {
      ans += pre - nums[i] + 1;
      pre++;
    }
  }

  return ans;
}
// @lc code=end
