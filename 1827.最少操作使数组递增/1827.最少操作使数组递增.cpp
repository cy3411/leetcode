/*
 * @lc app=leetcode.cn id=1827 lang=cpp
 *
 * [1827] 最少操作使数组递增
 */

// @lc code=start
class Solution {
public:
    int minOperations(vector<int> &nums) {
        int ans = 0, pre = nums[0] - 1;
        for (int num : nums) {
            pre = max(pre + 1, num);
            ans += pre - num;
        }
        return ans;
    }
};
// @lc code=end
