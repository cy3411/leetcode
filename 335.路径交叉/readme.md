# 题目

给你一个整数数组 `distance` 。

从 **X-Y** 平面上的点 `(0,0)` 开始，先向北移动 `distance[0]` 米，然后向西移动 `distance[1]` 米，向南移动 `distance[2]` 米，向东移动 `distance[3]` 米，持续移动。也就是说，每次移动后你的方位会发生逆时针变化。

判断你所经过的路径是否相交。如果相交，返回 `true` ；否则，返回 `false` 。

提示：

- $1 \leq distance.length \leq 10^5$
- $1 \leq distance[i] \leq 10^5$

# 示例

[![5vKB5j.png](https://z3.ax1x.com/2021/10/29/5vKB5j.png)](https://imgtu.com/i/5vKB5j)

```
输入：distance = [2,1,1,2]
输出：true
```

# 题解

## 归纳法

```ts
function isSelfCrossing(distance: number[]): boolean {
  const n = distance.length;

  for (let i = 3; i < n; i++) {
    // 第一类交叉
    if (distance[i] >= distance[i - 2] && distance[i - 3] >= distance[i - 1]) {
      return true;
    }
    // 第二类交叉
    if (i === 4 && distance[3] === distance[1] && distance[4] >= distance[2] - distance[0]) {
      return true;
    }
    // 第三类交叉
    if (
      i >= 5 &&
      distance[i - 3] - distance[i - 5] <= distance[i - 1] &&
      distance[i - 1] <= distance[i - 3] &&
      distance[i] >= distance[i - 2] - distance[i - 4] &&
      distance[i - 2] > distance[i - 4]
    ) {
      return true;
    }
  }

  return false;
}
```
