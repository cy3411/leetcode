/*
 * @lc app=leetcode.cn id=335 lang=typescript
 *
 * [335] 路径交叉
 */

// @lc code=start
function isSelfCrossing(distance: number[]): boolean {
  const n = distance.length;

  for (let i = 3; i < n; i++) {
    // 第一类交叉
    if (distance[i] >= distance[i - 2] && distance[i - 3] >= distance[i - 1]) {
      return true;
    }
    // 第二类交叉
    if (i === 4 && distance[3] === distance[1] && distance[4] >= distance[2] - distance[0]) {
      return true;
    }
    // 第三类交叉
    if (
      i >= 5 &&
      distance[i - 3] - distance[i - 5] <= distance[i - 1] &&
      distance[i - 1] <= distance[i - 3] &&
      distance[i] >= distance[i - 2] - distance[i - 4] &&
      distance[i - 2] > distance[i - 4]
    ) {
      return true;
    }
  }

  return false;
}
// @lc code=end
