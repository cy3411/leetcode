# 题目描述
给定一些标记了宽度和高度的信封，宽度和高度以整数对形式 (w, h) 出现。当另一个信封的宽度和高度都比这个信封大的时候，这个信封就可以放进另一个信封里，如同俄罗斯套娃一样。

请计算最多能有多少个信封能组成一组“俄罗斯套娃”信封（即可以把一个信封放到另一个信封里面）。


# 示例
```
输入: envelopes = [[5,4],[6,4],[6,7],[2,3]]
输出: 3 
解释: 最多信封的个数为 3, 组合为: [2,3] => [5,4] => [6,7]。
```

# 方法

按w为第一个关键字升序，h为第2关键字降序，求出按照h排序的最长递增子序列即可。

## 动态规划
```javascript
var maxEnvelopes = function (envelopes) {
  const size = envelopes.length;

  if (size === 0) return 0;

  envelopes.sort((a, b) => {
    if (a[0] !== b[0]) {
      return a[0] - b[0];
    } else {
      return b[1] - a[1];
    }
  });

  const memo = new Array(size).fill(1);
  for (let i = 1; i < size; i++) {
    for (let j = 0; j < i; j++) {
      if (envelopes[j][1] < envelopes[i][1]) {
        memo[i] = Math.max(memo[i], memo[j] + 1);
      }
    }
  }
  return Math.max(...memo);
};
```

## 二分查找
查找出递增子序列中末尾元素的最小值，用新元素替换
```javascript
var maxEnvelopes = function (envelopes) {
  const size = envelopes.length;
  if (size === 0) return 0;
  const binaySearch = (subs, target) => {
    let low = 0;
    let high = subs.length - 1;
    while (low < high) {
      let mid = low + ((high - low) >> 1);
      if (subs[mid] < target) {
        low = mid + 1;
      } else {
        high = mid;
      }
    }
    // 返回匹配的索引
    return low;
  };

  envelopes.sort((a, b) => {
    if (a[0] !== b[0]) {
      return a[0] - b[0];
    } else {
      return b[1] - a[1];
    }
  });

  const subs = [envelopes[0][1]];

  for (let i = 1; i < size; i++) {
    let height = envelopes[i][1];
    let subMaxHeight = subs[subs.length - 1];
    if (height > subMaxHeight) {
      subs.push(height);
    } else {
      const idx = binaySearch(subs, height);
      subs[idx] = height;
    }
  }
  return subs.length;
};
```

