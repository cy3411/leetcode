# 题目

假设 力扣（LeetCode）即将开始 IPO 。为了以更高的价格将股票卖给风险投资公司，力扣 希望在 IPO 之前开展一些项目以增加其资本。 由于资源有限，它只能在 IPO 之前完成最多 `k` 个不同的项目。帮助 力扣 设计完成最多 k 个不同项目后得到最大总资本的方式。

给你 `n` 个项目。对于每个项目 `i` ，它都有一个纯利润 `profits[i]` ，和启动该项目需要的最小资本 `capital[i]` 。

最初，你的资本为 `w` 。当你完成一个项目时，你将获得纯利润，且利润将被添加到你的总资本中。

总而言之，从给定项目中选择 最多 `k` 个不同项目的列表，以 最大化最终资本 ，并输出最终可获得的最多资本。

答案保证在 32 位有符号整数范围内。

提示：

- `1 <= k <= 105`
- `0 <= w <= 109`
- `n == profits.length`
- `n == capital.length`
- `1 <= n <= 105`
- `0 <= profits[i] <= 104`
- `0 <= capital[i] <= 109`

# 示例

```
输入：k = 2, w = 0, profits = [1,2,3], capital = [0,1,1]
输出：4
解释：
由于你的初始资本为 0，你仅可以从 0 号项目开始。
在完成后，你将获得 1 的利润，你的总资本将变为 1。
此时你可以选择开始 1 号或 2 号项目。
由于你最多可以选择两个项目，所以你需要完成 2 号项目以获得最大的资本。
因此，输出最后最大化的资本，为 0 + 1 + 3 = 4。
```

# 题解

## 优先队列

满足最大利润就是使用最小资本来获得最大的利润。

每次我们选择项目的时候，就是从项目中选择满足启动资本的情况下，选择利润最大的项目。从一堆数中选择最大的，自然就想到了大顶堆。

我们先将项目按照资本升序排列，每次选择项目时，将符合条件的项目放入堆中，然后取出最大利润，并更新资本`w`。

持续以上操作，直到选择次数 `k` 用完。

```ts
class MaxPQ {
  pq: number[];
  n: number;
  constructor() {
    this.pq = [];
    this.n = 0;
  }
  private less(i: number, j: number): boolean {
    return this.pq[i] < this.pq[j];
  }
  private swap(i: number, j: number) {
    [this.pq[i], this.pq[j]] = [this.pq[j], this.pq[i]];
  }
  private swin(k: number) {
    let parentIdx = (k - 1) >> 1;
    while (parentIdx >= 0 && this.less(parentIdx, k)) {
      this.swap(parentIdx, k);
      k = parentIdx;
      parentIdx = (k - 1) >> 1;
    }
  }
  private sink(k: number) {
    let childrenIdx = k * 2 + 1;
    while (childrenIdx < this.n) {
      if (childrenIdx + 1 < this.n && this.less(childrenIdx, childrenIdx + 1)) {
        childrenIdx += 1;
      }
      if (this.less(childrenIdx, k)) return;
      this.swap(childrenIdx, k);
      k = childrenIdx;
      childrenIdx = k * 2 + 1;
    }
  }
  insert(k: number) {
    this.pq[this.n++] = k;
    this.swin(this.n - 1);
  }
  poll() {
    if (this.n === 0) return null;
    return this.pq[this.n - 1];
  }
  pop() {
    if (this.n === 0) return null;
    let max = this.pq[0];
    this.swap(0, this.n - 1);
    this.pq.pop();
    this.n--;
    this.sink(0);
    return max;
  }
}

function findMaximizedCapital(k: number, w: number, profits: number[], capital: number[]): number {
  const m = profits.length;
  const ipo = new Array(m).fill(0).map((_) => new Array(2).fill(0));
  // 保存项目的启动资金和利润
  for (let i = 0; i < m; i++) {
    ipo[i][0] = capital[i];
    ipo[i][1] = profits[i];
  }
  // 按照启动资金排序
  ipo.sort((a: number[], b: number[]) => a[0] - b[0]);

  const maxPQ = new MaxPQ();
  let idx = 0;
  // 循环k次获取项目
  for (let i = 0; i < k; i++) {
    // 将小于等于w的项目放入堆中
    while (idx < m && ipo[idx][0] <= w) {
      maxPQ.insert(ipo[idx][1]);
      idx++;
    }

    if (maxPQ.n > 0) {
      w += maxPQ.pop();
    } else {
      break;
    }
  }

  return w;
}
```
