/*
 * @lc app=leetcode.cn id=1748 lang=cpp
 *
 * [1748] 唯一元素的和
 */

// @lc code=start
class Solution
{
public:
    int sumOfUnique(vector<int> &nums)
    {
        unordered_map<int, int> m;
        int ans = 0;
        for (auto x : nums)
        {
            if (m.find(x) == m.end())
            {
                m[x] = 1;
                ans += x;
            }
            else if (m[x] == 1)
            {
                m[x] = 2;
                ans -= x;
            }
        }

        return ans;
    }
};
// @lc code=end
