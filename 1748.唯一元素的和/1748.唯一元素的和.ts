/*
 * @lc app=leetcode.cn id=1748 lang=typescript
 *
 * [1748] 唯一元素的和
 */

// @lc code=start
function sumOfUnique(nums: number[]): number {
  const map = new Map<number, number>();
  for (let x of nums) {
    map.set(x, (map.get(x) || 0) + 1);
  }
  let ans = 0;
  for (let [k, v] of map.entries()) {
    if (v === 1) ans += k;
  }

  return ans;
}
// @lc code=end
