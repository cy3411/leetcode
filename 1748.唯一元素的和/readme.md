# 题目

给你一个整数数组 nums 。数组中唯一元素是那些只出现 恰好一次 的元素。

请你返回 nums 中唯一元素的 和 。

提示：

- $\color{burlywood} 1 \leq nums.length \leq 100$
- $\color{burlywood} 1 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [1,2,3,2]
输出：4
解释：唯一元素为 [1,3] ，和为 4 。
```

```
输入：nums = [1,1,1,1,1]
输出：0
解释：没有唯一元素，和为 0
```

```
输入：nums = [1,2,3,4,5]
输出：15
解释：唯一元素为 [1,2,3,4,5] ，和为 15 。
```

# 题解

## 哈希表

使用哈希表记录每个数字出现的次数，最后将出现次数为 1 的数字相加即可。

```ts
function sumOfUnique(nums: number[]): number {
  const map = new Map<number, number>();
  for (let x of nums) {
    map.set(x, (map.get(x) || 0) + 1);
  }
  let ans = 0;
  for (let [k, v] of map.entries()) {
    if (v === 1) ans += k;
  }

  return ans;
}
```

使用哈希表记录状态，未出现的数字累加，出现过 1 次的数字从结果中减去，最后结果即为唯一元素的和。

```cpp
class Solution
{
public:
    int sumOfUnique(vector<int> &nums)
    {
        unordered_map<int, int> m;
        int ans = 0;
        for (auto x : nums)
        {
            if (m.find(x) == m.end())
            {
                m[x] = 1;
                ans += x;
            }
            else if (m[x] == 1)
            {
                m[x] = 2;
                ans -= x;
            }
        }

        return ans;
    }
};
```
