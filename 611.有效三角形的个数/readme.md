# 题目

给定一个包含非负整数的数组，你的任务是统计其中可以组成三角形三条边的三元组个数。

注意:

- 数组长度不超过 1000。
- 数组里整数的范围为 [0, 1000]。

# 示例

```
输入: [2,2,3,4]
输出: 3
解释:
有效的组合是:
2,3,4 (使用第一个 2)
2,3,4 (使用第二个 2)
2,2,3
```

# 题解

## 排序+二分

对于正整数 a,b,c,假设它们可以组成三角形，那么需要满足如下条件：

$$
\begin{cases}
a+b>c \\
a+c>b \\
b+c>a \\
\end{cases}
$$

如果对三条边排序，$a \leq b \leq c$,那么$a+c > b$且$b+c>a$，我们只需要保证$a+b>c$。

双重枚举数组 a 和 b 的位置，然后在剩下的有序数组区间中二分查找最大满足$a+b>c$中，c 的下标，将答案累加即可。

```ts
function triangleNumber(nums: number[]): number {
  // 升序排序，这样只需要找到a+b>c的c就可以了
  nums.sort((a, b) => a - b);

  const m = nums.length;
  let ans = 0;
  // 双重枚举a和b
  for (let i = 0; i < m; i++) {
    for (let j = i + 1; j < m; j++) {
      let l = j + 1;
      let r = m - 1;
      // 数组中会有0的出现，将k默认为j的位置，防止异常
      let k = j;
      // 二分在剩下的区间找到c
      while (l <= r) {
        const mid = l + ((r - l) >> 1);
        if (nums[mid] < nums[i] + nums[j]) {
          k = mid;
          l = mid + 1;
        } else {
          r = mid - 1;
        }
      }
      ans += k - j;
    }
  }

  return ans;
}
```
