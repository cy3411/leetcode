/*
 * @lc app=leetcode.cn id=104 lang=javascript
 *
 * [104] 二叉树的最大深度
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var maxDepth = function (root) {
  if (!root) {
    return 0
  } else {
    let leftHeight = maxDepth(root.left)
    let rightHeight = maxDepth(root.right)

    return Math.max(leftHeight, rightHeight) + 1
  }
};
// @lc code=end

/**
 * 时间复杂度：O(n)，其中 n 为二叉树的节点个数
 * 空间复杂度：O(height)，其中 height 表示二叉树的高度。
 */