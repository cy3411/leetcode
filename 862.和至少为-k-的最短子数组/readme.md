# 题目

返回 A 的最短的非空连续子数组的长度，该子数组的和至少为 K 。

如果没有和至少为 K 的非空子数组，返回 -1 。

提示：

- 1 <= A.length <= 50000
- -10 ^ 5 <= A[i] <= 10 ^ 5
- 1 <= K <= 10 ^ 9

# 示例

```
输入：A = [1], K = 1
输出：1
```

```
输入：A = [1,2], K = 4
输出：-1
```

# 题解

## 单调队列

求取区间和，自然想到的就是前缀和数组。

获得前缀和数组后，剩下就是就是找最短的长度的连续子数组。

假设[i,j]区间和大于等于 k，那么要找更短的子数组，自然就是 i 和 j 之间的区间和。

应为我们要求的是大于等于 k 的值，所以 i 的位置元素需要最小，如果 i 满足条件大于等于 k，求更短的话，那么 i 位置的元素需要再大一些才能满足条件。

从上面可以看见，i 元素满足单调递增的性质。

```ts
function shortestSubarray(nums: number[], k: number): number {
  const m = nums.length;
  // 前缀和数组
  const sum = new Array(m + 1).fill(0);
  for (let i = 0; i < m; i++) {
    sum[i + 1] = sum[i] + nums[i];
  }

  let pre = -1;
  let ans = -1;
  const deque = [];
  deque.push(0);
  for (let i = 1; i <= m; i++) {
    // 满足结果，我们将队首弹出，继续找距离更短的结果
    while (deque.length && sum[i] - sum[deque[0]] >= k) {
      pre = deque.shift();
    }
    // 更新结果
    if (pre !== -1 && (ans === -1 || i - pre < ans)) {
      ans = i - pre;
    }
    // 单调递增队列
    while (deque.length && sum[i] < sum[deque[deque.length - 1]]) {
      deque.pop();
    }
    deque.push(i);
  }

  return ans;
}
```
