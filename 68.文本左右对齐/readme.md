# 题目

给定一个单词数组和一个长度 _maxWidth_，重新排版单词，使其成为每行恰好有 _maxWidth_ 个字符，且左右两端对齐的文本。

你应该使用“贪心算法”来放置给定的单词；也就是说，尽可能多地往每行中放置单词。必要时可用空格 `' '` 填充，使得每行恰好有 _maxWidth_ 个字符。

要求尽可能均匀分配单词间的空格数量。如果某一行单词间的空格不能均匀分配，则左侧放置的空格数要多于右侧的空格数。

文本的最后一行应为左对齐，且单词之间不插入**额外的**空格。

说明:

- 单词是指由非空格字符组成的字符序列。
- 每个单词的长度大于 0，小于等于 maxWidth。
- 输入单词数组 `words` 至少包含一个单词。

# 示例

```
输入:
words = ["This", "is", "an", "example", "of", "text", "justification."]
maxWidth = 16
输出:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]
```

# 题解

## 模拟

根据题意，我们可以分以下几种情况处理：

- 最后一行,单词左对齐，单词之间只有一个空格，其他的空格填充到尾部
- 当前行只有一个单词，单词左对齐，尾部填充空格
- 当前行有多个单词。假设当前行有 `nWords` 个单词,空格数为 `nBlanks`,那么平均分配的空格
  $$avgBlank=\lfloor \frac{nBlanks}{nWords-1} \rfloor$$

  多余的空格
  $$ extrBlank = nBlanks \% (nWords-1) $$
  应该填充到前 extrBlank 个单词之间。

```ts
function fullJustify(words: string[], maxWidth: number): string[] {
  const createBlank = (num: number): string => {
    return ''.padStart(num, ' ');
  };
  const m = words.length;
  const ans: string[] = [];
  // 记录最后访问的words数组下标
  let tail = 0;

  while (1) {
    // 当前行的单词长度之和
    let sum = 0;
    // 当前行第一个单词的数组下标
    let head = tail;
    while (tail < m && sum + words[tail].length + tail - head <= maxWidth) {
      sum += words[tail++].length;
    }

    // 最后一行,单词左对齐，单词之间只有一个空格，其他的填充到尾部
    if (tail === m) {
      let str = words.slice(head).join(' ');
      str = str.padEnd(maxWidth, ' ');
      ans.push(str);
      break;
    }

    // 当前行只有一个单词，单词左对齐，尾部填充空格
    const wordCount = tail - head;
    if (wordCount === 1) {
      let str = words[head];
      str = str.padEnd(maxWidth, ' ');
      ans.push(str);
      continue;
    }

    // 普通情况
    const blankCount = maxWidth - sum;
    const avgBlank = (blankCount / (wordCount - 1)) >> 0;
    const extrBlank = blankCount % (wordCount - 1);
    const s1 = words.slice(head, head + extrBlank + 1).join(createBlank(avgBlank + 1));
    const s2 = words.slice(head + extrBlank + 1, tail).join(createBlank(avgBlank));
    ans.push(s1 + createBlank(avgBlank) + s2);
  }

  return ans;
}
```
