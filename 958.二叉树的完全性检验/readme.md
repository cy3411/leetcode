# 题目

给定一个二叉树，确定它是否是一个完全二叉树。

百度百科中对完全二叉树的定义如下：

若设二叉树的深度为 h，除第 h 层外，其它各层 (1 ～ h-1) 的结点数都达到最大个数，第 h 层所有的结点都连续集中在最左边，这就是完全二叉树。（注：第 h 层可能包含 1~ 2h 个节点。）

提示：

- 树中将会有 1 到 100 个结点。

# 示例

[![WxS9FP.png](https://z3.ax1x.com/2021/07/31/WxS9FP.png)](https://imgtu.com/i/WxS9FP)

```
输入：[1,2,3,4,5,6]
输出：true
解释：最后一层前的每一层都是满的（即，结点值为 {1} 和 {2,3} 的两层），且最后一层中的所有结点（{4,5,6}）都尽可能地向左。
```

# 题解

## 递归

完全二叉树除了最后一层，其他层都是满节点。最后一层的叶子节点，也是从左向右连续排列。

利用这一特性，我们可以计算最后一层左右节点正确的节点数量。

定义 n 为所有节点的数量， m 为倒数第二层的节点数量，k 为除了最后一层所有节点的数量。

定义 l 为最后一层左边节点数量 min(m, n-k)，定义 r 为最后一层右节点数量 n-k-l。

递归求解最后一层左右节点数量是否满足题目给定的二叉树数，来返回答案。

```ts
function countNodes(node: TreeNode | null): number {
  if (node === null) return 0;
  return countNodes(node.left) + countNodes(node.right) + 1;
}

function judge(node: TreeNode | null, n: number, m: number): boolean {
  // 没有节点了，判断节点总数是否等于0
  if (node === null) return n === 0;
  // 总数1，最后一个必须是叶子节点
  if (n === 1) return node.left === null && node.right === null;
  // 总数0，但还有节点，只能返回false
  if (n === 0) return false;
  // 除了最后一层，剩余的节点总数
  const k = 2 * m - 1;
  // 下一层左节点的总数
  const l = Math.min(n - k, m);
  // 下一层右节点的总数
  const r = n - k - l;
  // 递归判断左右节点的数量是否符合完全二叉树特性
  return (
    judge(node.left, ((k - 1) >> 1) + l, m >> 1) && judge(node.right, ((k - 1) >> 1) + r, m >> 1)
  );
}

function isCompleteTree(root: TreeNode | null): boolean {
  // 总共有多少个节点
  const n = countNodes(root);
  // 倒数第二层有几个节点
  let m = 1;
  // n层的总结点数
  let k = 1;
  while (k + 2 * m <= n) {
    m *= 2;
    k += m;
  }

  return judge(root, n, m);
}
```
