/*
 * @lc app=leetcode.cn id=600 lang=typescript
 *
 * [600] 不含连续1的非负整数
 */

// @lc code=start
function findIntegers(n: number): number {
  // 长度为i的二进制位，不包含 连续的1 的个数
  const dp = new Array(31).fill(0);
  // 初始化
  dp[0] = dp[1] = 1;
  // 规律类似斐波那契
  for (let i = 2; i < 31; i++) {
    dp[i] = dp[i - 1] + dp[i - 2];
  }
  // 上一个高位的数字
  let pre = 0;
  let ans = 0;
  for (let i = 29; i >= 0; i--) {
    let val = 1 << i;
    // 高位是1，就是有右子树
    if ((n & val) !== 0) {
      n -= val;
      ans += dp[i + 1];
      // 上一位是1，表示是连续1，结束循环
      if (pre === 1) break;
      pre = 1;
    } else {
      pre = 0;
    }

    if (i === 0) ans++;
  }

  return ans;
}
// @lc code=end
