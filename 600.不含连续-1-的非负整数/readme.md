# 题目

给定一个正整数 `n`，找出小于或等于 `n` 的非负整数中，其二进制表示不包含 连续的 `1` 的个数。

说明: `1 <= n <= 1**9`

# 示例

```
输入: 5
输出: 5
解释:
下面是带有相应二进制表示的非负整数<= 5：
0 : 0
1 : 1
2 : 10
3 : 11
4 : 100
5 : 101
其中，只有整数3违反规则（有两个连续的1），其他5个满足规则。
```

# 题解

## 字典树+动态规划

[不含连续 1 的非负整数](https://leetcode-cn.com/problems/non-negative-integers-without-consecutive-ones/solution/bu-han-lian-xu-1de-fei-fu-zheng-shu-by-l-9l86/)

```ts
function findIntegers(n: number): number {
  // 长度为i的二进制位，不包含 连续的1 的个数
  const dp = new Array(31).fill(0);
  // 初始化
  dp[0] = dp[1] = 1;
  // 规律类似斐波那契
  for (let i = 2; i < 31; i++) {
    dp[i] = dp[i - 1] + dp[i - 2];
  }
  // 上一个高位的数字
  let pre = 0;
  let ans = 0;
  for (let i = 29; i >= 0; i--) {
    let val = 1 << i;
    // 高位是1，就是有右子树
    if ((n & val) !== 0) {
      n -= val;
      ans += dp[i + 1];
      // 上一位是1，表示是连续1，结束循环
      if (pre === 1) break;
      pre = 1;
    } else {
      pre = 0;
    }

    if (i === 0) ans++;
  }

  return ans;
}
```
