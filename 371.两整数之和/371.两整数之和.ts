/*
 * @lc app=leetcode.cn id=371 lang=typescript
 *
 * [371] 两整数之和
 */

// @lc code=start
function getSum(a: number, b: number): number {
  while (b !== 0) {
    // 进位
    const carry = (a & b) << 1;
    a ^= b;
    b = carry;
  }
  return a;
}
// @lc code=end
