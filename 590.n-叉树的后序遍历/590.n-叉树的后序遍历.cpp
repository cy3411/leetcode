/*
 * @lc app=leetcode.cn id=590 lang=cpp
 *
 * [590] N 叉树的后序遍历
 */

// @lc code=start
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution
{
public:
    vector<int> postorder(Node *root)
    {
        vector<int> ans;
        if (root == NULL)
        {
            return ans;
        }
        stack<Node *> stk;
        stk.push(root);
        while (!stk.empty())
        {
            Node *curr = stk.top();
            stk.pop();
            ans.push_back(curr->val);
            for (auto child : curr->children)
            {
                stk.push(child);
            }
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
// @lc code=end
