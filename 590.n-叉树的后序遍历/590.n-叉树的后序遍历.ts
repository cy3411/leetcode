/*
 * @lc app=leetcode.cn id=590 lang=typescript
 *
 * [590] N 叉树的后序遍历
 */

// @lc code=start
/**
 * Definition for node.
 * class Node {
 *     val: number
 *     children: Node[]
 *     constructor(val?: number) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.children = []
 *     }
 * }
 */

function postorder(root: Node | null): number[] {
  const ans: number[] = [];
  const _postorder = (root: Node | null) => {
    if (root === null) return;
    for (const child of root.children) {
      _postorder(child);
    }
    ans.push(root.val);
  };
  _postorder(root);
  return ans;
}
// @lc code=end
