# 题目

给定一个 `n` 叉树的根节点 `root` ，返回 **其节点值的** 后序遍历 。

`n` 叉树 在输入中按层序遍历进行序列化表示，每组子节点由空值 `null` 分隔（请参见示例）。

提示：

- 节点总数在范围 $[0, 10^4]$ 内
- $0 \leq Node.val \leq 10^4$
- `n` 叉树的高度小于或等于 `1000`

进阶：递归法很简单，你可以使用迭代法完成此题吗?

# 示例

[![b7noa8.png](https://s1.ax1x.com/2022/03/12/b7noa8.png)](https://imgtu.com/i/b7noa8)

```
输入：root = [1,null,3,2,4,null,5,6]
输出：[5,6,3,2,4,1]
```

[![b7nLxs.png](https://s1.ax1x.com/2022/03/12/b7nLxs.png)](https://imgtu.com/i/b7nLxs)

```
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：[2,6,14,11,7,3,12,8,4,13,9,10,5,1]
```

# 题解

## 递归

正常的后续遍历即可。

```ts
function postorder(root: Node | null): number[] {
  const ans: number[] = [];
  const _postorder = (root: Node | null) => {
    if (root === null) return;
    for (const child of root.children) {
      _postorder(child);
    }
    ans.push(root.val);
  };
  _postorder(root);
  return ans;
}
```

## 迭代

利用栈的特性，按序将节点压入栈中，并在节点被弹出时，将其值放入答案中。最后将答案数组反转后返回。

```cpp
class Solution
{
public:
    vector<int> postorder(Node *root)
    {
        vector<int> ans;
        if (root == NULL)
        {
            return ans;
        }
        stack<Node *> stk;
        stk.push(root);
        while (!stk.empty())
        {
            Node *curr = stk.top();
            stk.pop();
            ans.push_back(curr->val);
            for (auto child : curr->children)
            {
                stk.push(child);
            }
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
```
