# 题目

给定一个非负整数，你至多可以交换一次数字中的任意两位。返回你能得到的最大值。

注意:

- 给定数字的范围是 $[0, 10^8]$

# 示例

```
输入: 2736
输出: 7236
解释: 交换数字2和数字7。
```

```
输入: 9973
输出: 9973
解释: 不需要交换。
```

# 题解

## 遍历

遍历枚举每一种数字组合，返回最大值。

```js
function maximumSwap(num: number): number {
  const chars = String(num).split('');
  const n = chars.length;
  let ans = num;

  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      // 交换，得到每一种组合
      [chars[i], chars[j]] = [chars[j], chars[i]];
      ans = Math.max(ans, Number(chars.join('')));
      // 处理完毕，将交换结果还原
      [chars[i], chars[j]] = [chars[j], chars[i]];
    }
  }

  return ans;
}
```

## 贪心

要想数字更大，只需要将靠右的大数字和靠左边的小数字交换即可。

```cpp
class Solution {
public:
    int maximumSwap(int num) {
        string chars = to_string(num);
        int n = chars.size();
        int maxIdx = n - 1, leftIdx = -1, rightIdx = -1;

        for (int i = n - 1; i >= 0; i--) {
            if (chars[i] > chars[maxIdx]) {
                // 找到更大的值，更新 maxIdx
                maxIdx = i;
            } else if (chars[i] < chars[maxIdx]) {
                // 找到比 maxIdx 值， 更新需要交换的两个下标
                leftIdx = i;
                rightIdx = maxIdx;
            }
        }

        if (leftIdx >= 0) {
            swap(chars[leftIdx], chars[rightIdx]);
            return stoi(chars);
        } else {
            return num;
        }
    }
};
```
