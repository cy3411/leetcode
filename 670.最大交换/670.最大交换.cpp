/*
 * @lc app=leetcode.cn id=670 lang=cpp
 *
 * [670] 最大交换
 */

// @lc code=start
class Solution {
public:
    int maximumSwap(int num) {
        string chars = to_string(num);
        int n = chars.size();
        int maxIdx = n - 1, leftIdx = -1, rightIdx = -1;

        for (int i = n - 1; i >= 0; i--) {
            if (chars[i] > chars[maxIdx]) {
                // 找到更大的值，更新 maxIdx
                maxIdx = i;
            } else if (chars[i] < chars[maxIdx]) {
                // 找到比 maxIdx 值， 更新需要交换的两个下标
                leftIdx = i;
                rightIdx = maxIdx;
            }
        }

        if (leftIdx >= 0) {
            swap(chars[leftIdx], chars[rightIdx]);
            return stoi(chars);
        } else {
            return num;
        }
    }
};
// @lc code=end
