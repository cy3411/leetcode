/*
 * @lc app=leetcode.cn id=670 lang=typescript
 *
 * [670] 最大交换
 */

// @lc code=start
function maximumSwap(num: number): number {
  const chars = String(num).split('');
  const n = chars.length;
  let ans = num;

  for (let i = 0; i < n; i++) {
    for (let j = i + 1; j < n; j++) {
      // 交换，得到每一种组合
      [chars[i], chars[j]] = [chars[j], chars[i]];
      ans = Math.max(ans, Number(chars.join('')));
      // 处理完毕，将交换结果还原
      [chars[i], chars[j]] = [chars[j], chars[i]];
    }
  }

  return ans;
}
// @lc code=end
