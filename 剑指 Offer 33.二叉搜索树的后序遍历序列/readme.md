# 题目

输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历结果。如果是则返回 true，否则返回 false。假设输入的数组的任意两个数字都互不相同。

# 示例

```
参考以下这颗二叉搜索树：
     5
    / \
   2   6
  / \
 1   3
```

```
输入: [1,6,3,2,5]
输出: false
```

```
输入: [1,3,2,6,5]
输出: true
```

# 题解

## 中序遍历

二叉搜索树的中序遍历结果是一个升序序列。

题目给出的后序遍历结果，我们可以找到对应的根和左右子节点，然后中序遍历这个结果，判断是否是升序状态即可。

后续遍历的最后一位是根节点，前面第一个大于根节点的位置是右子树第一个节点，之前的是左子树。

```js
var verifyPostorder = function (postorder) {
  let pre = -1;
  const inorder = (nums, l, r) => {
    if (l > r) return true;
    let index = l;
    // 找到右子树的第一个节点
    while (nums[index] < nums[r]) index++;
    if (!inorder(nums, l, index - 1)) return false;
    if (pre !== -1 && nums[pre] > nums[r]) return false;
    pre = r;
    if (!inorder(nums, index, r - 1)) return false;
    return true;
  };
  return inorder(postorder, 0, postorder.length - 1);
};
```
