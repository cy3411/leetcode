/*
 * @lc app=leetcode.cn id=1288 lang=typescript
 *
 * [1288] 删除被覆盖区间
 */

// @lc code=start
function removeCoveredIntervals(intervals: number[][]): number {
  // 左边界升序，右边界降序
  intervals.sort((a, b) => {
    if (a[0] === b[0]) return b[1] - a[1];
    return a[0] - b[0];
  });

  // 前一个右边界
  let pre = -1;
  // 覆盖区间的数量
  let cnt = 0;
  for (let [_, r] of intervals) {
    if (pre >= r) cnt++;
    pre = Math.max(pre, r);
  }

  return intervals.length - cnt;
}
// @lc code=end
