# 题目
给你一个区间列表，请你删除列表中被其他区间所覆盖的区间。

只有当 `c <= a` 且 `b <= d` 时，我们才认为区间 `[a,b)` 被区间 `[c,d)` 覆盖。

在完成所有删除操作后，请你返回列表中剩余区间的数目。

提示：​​​​​​
+ 1 <= intervals.length <= 1000
+ 0 <= intervals[i][0] < intervals[i][1] <= 10^5
+ 对于所有的 i != j：intervals[i] != intervals[j]

# 示例
```
输入：intervals = [[1,4],[3,6],[2,8]]
输出：2
解释：区间 [3,6] 被区间 [2,8] 覆盖，所以它被删除了。
```

# 题解
## 排序
只有当 `c <= a` 且 `b <= d` 时，我们才认为区间 `[a,b)` 被区间 `[c,d)` 覆盖。

将区间数组按照左边界升序，如果左边界相同，就按照右边界降序。

遍历数组，只要判断前一个右边界是否大于当前右边界就可以判定是否可以覆盖。

```ts
function removeCoveredIntervals(intervals: number[][]): number {
  // 左边界升序，右边界降序
  intervals.sort((a, b) => {
    if (a[0] === b[0]) return b[1] - a[1];
    return a[0] - b[0];
  });

  // 前一个右边界
  let pre = -1;
  // 覆盖区间的数量
  let cnt = 0;
  for (let [_, r] of intervals) {
    if (pre >= r) cnt++;
    pre = Math.max(pre, r);
  }

  return intervals.length - cnt;
}
```