/*
 * @lc app=leetcode.cn id=447 lang=typescript
 *
 * [447] 回旋镖的数量
 */

// @lc code=start
function numberOfBoomerangs(points: number[][]): number {
  let ans = 0;
  for (let p of points) {
    const hash: Map<number, number> = new Map();
    // 计算出相同距离的点的集合
    for (let q of points) {
      const dist = (p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2;
      hash.set(dist, (hash.get(dist) ?? 0) + 1);
    }
    // 排列点
    for (let [_, n] of hash.entries()) {
      ans += n * (n - 1);
    }
  }

  return ans;
}
// @lc code=end
