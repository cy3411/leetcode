# 题目

给定平面上 `n` 对 **互不相同** 的点 `points` ，其中 `points[i] = [xi, yi]` 。**回旋镖** 是由点 `(i, j, k)` 表示的元组 ，其中 `i` 和 `j` 之间的距离和 `i` 和 `k` 之间的距离相等（**需要考虑元组的顺序**）。

返回平面上所有回旋镖的数量。

提示：

- `n == points.length`
- `1 <= n <= 500`
- `points[i].length == 2`
- `-10**4 <= xi, yi <= 10**4`
- 所有点都 互不相同

# 示例

```
输入：points = [[0,0],[1,0],[2,0]]
输出：2
解释：两个回旋镖为 [[1,0],[0,0],[2,0]] 和 [[1,0],[2,0],[0,0]]
```

# 题解

## 枚举+哈希表

枚举每个点，将与当前相同距离的的点的数量存入哈希中。

平面两点距离公式：
$$|AB| = \sqrt{(x1-x2)^2+(y1-y2)^2}$$

将与当前点相同距离的点取 2 个出来做排列，排列的数量就是答案。

n 中取 m 个排列公式：
$$ A_n^m=\frac{n!}{(n-m)!} $$

题解中省略了计算两点距离的开方，这个不影响结果。

```ts
function numberOfBoomerangs(points: number[][]): number {
  let ans = 0;
  for (let p of points) {
    const hash: Map<number, number> = new Map();
    // 计算出相同距离的点的集合
    for (let q of points) {
      const dist = (p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2;
      hash.set(dist, (hash.get(dist) ?? 0) + 1);
    }
    // 排列点
    for (let [_, n] of hash.entries()) {
      ans += n * (n - 1);
    }
  }

  return ans;
}
```
