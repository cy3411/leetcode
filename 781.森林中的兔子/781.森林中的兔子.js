/*
 * @lc app=leetcode.cn id=781 lang=javascript
 *
 * [781] 森林中的兔子
 */

// @lc code=start
/**
 * @param {number[]} answers
 * @return {number}
 */
var numRabbits = function (answers) {
  const map = new Map();
  // 统计同一种回答的数量
  for (let answer of answers) {
    map.set(answer, (map.get(answer) || 0) + 1);
  }

  let result = 0;
  // 遍历统计结果，按照规律分析的公式套进去
  for (let [x, count] of map.entries()) {
    result += Math.ceil(count / (x + 1)) * (x + 1);
  }

  return result;
};
// @lc code=end
