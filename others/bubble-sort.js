function bubbleSrot (arr) {
  const len = arr.length
  let last_change_index = 0
  let sort_border = len - 1

  for (let i = 0; i < len; i++) {
    let is_sorted = true
    for (let j = 0; j < sort_border; j++) {
      if (arr[j] > arr[j + 1]) {
        [arr[j], arr[j + 1]] = [arr[j + 1], arr[j]]
        is_sorted = false
        last_change_index = j
      }
    }
    sort_border = last_change_index
    if (is_sorted) break
  }

  return arr

}

const arr = [1, 5, 3, 6, 2, 4, 7, 8, 9]

console.time()
console.log(bubbleSrot(arr))
console.timeEnd()