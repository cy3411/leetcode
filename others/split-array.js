function splitArray (arr = []) {
  let sum = 0
  let dp = []
  let len = arr.length
  for (let num of arr) {
    sum += num
  }
  let mid = Math.ceil(sum / 2)
  for (let i = 0; i <= len; i++) {
    let tmp = []
    for (let j = 0; j <= mid; j++) {
      tmp.push(0)
    }
    dp.push(tmp)
  }

  for (let i = 1; i <= len; i++) {
    for (let j = 1; j <= mid; j++) {
      if (j >= arr[i - 1]) {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i - 1][j - arr[i - 1]] + arr[i - 1]);
      }
      else {
        dp[i][j] = dp[i - 1][j];
      }
    }
  }

  console.log('splitArray -> dp', dp[len][mid])

}

function splictArray2 (arr) {
  let a = [...arr]
  let b = []
  let sum = 0
  a.forEach(n => sum = sum + n)
  let half = Math.floor(sum / 2)
  let idx = a.length - 1

  while (idx >= 0) {
    let current = a[idx]
    if (half > current) {
      half = half - current
      b.push(current)
      a.splice(idx, 1)
    }
    idx--
  }
  console.log(a, b)
}

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14]

splictArray2(arr)

