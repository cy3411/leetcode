/**
 * @description: 获取2个数的最大公约数
 * @param {Number} a 
 * @param {Number} b
 * @return {Number}
 */
function greatest_common_divisor (a, b) {
  if (a === b) {
    return a
  }

  if ((a & 1) === 0 && (b & 1) === 0) {
    return greatest_common_divisor(a >> 1, b >> 1)
  } else if ((a & 1) === 0 && (b & 1) !== 0) {
    return greatest_common_divisor(a >> 1, b)
  } else if ((a & 1) !== 0 && (b & 1) === 0) {
    return greatest_common_divisor(a, b >> 1)
  } else {
    const big = Math.max(a, b)
    const small = Math.min(a, b)
    return greatest_common_divisor(big - small, small)
  }
}

const a = 4
const b = 3

console.time()
console.log(greatest_common_divisor(a, b))
console.timeEnd()