# 题目

我们把玻璃杯摆成金字塔的形状，其中 第一层 有 1 个玻璃杯， 第二层 有 2 个，依次类推到第 100 层，每个玻璃杯 (250ml) 将盛有香槟。

从顶层的第一个玻璃杯开始倾倒一些香槟，当顶层的杯子满了，任何溢出的香槟都会立刻等流量的流向左右两侧的玻璃杯。当左右两边的杯子也满了，就会等流量的流向它们左右两边的杯子，依次类推。（当最底层的玻璃杯满了，香槟会流到地板上）

例如，在倾倒一杯香槟后，最顶层的玻璃杯满了。倾倒了两杯香槟后，第二层的两个玻璃杯各自盛放一半的香槟。在倒三杯香槟后，第二层的香槟满了 - 此时总共有三个满的玻璃杯。在倒第四杯后，第三层中间的玻璃杯盛放了一半的香槟，他两边的玻璃杯各自盛放了四分之一的香槟，如下图所示。

[![zMTe0J.png](https://s1.ax1x.com/2022/11/20/zMTe0J.png)](https://imgse.com/i/zMTe0J)

现在当倾倒了非负整数杯香槟后，返回第 i 行 j 个玻璃杯所盛放的香槟占玻璃杯容积的比例（ i 和 j 都从 0 开始）。

# 示例

```
示例 1:
输入: poured(倾倒香槟总杯数) = 1, query_glass(杯子的位置数) = 1, query_row(行数) = 1
输出: 0.00000
解释: 我们在顶层（下标是（0，0））倒了一杯香槟后，没有溢出，因此所有在顶层以下的玻璃杯都是空的。
```

```
示例 2:
输入: poured(倾倒香槟总杯数) = 2, query_glass(杯子的位置数) = 1, query_row(行数) = 1
输出: 0.50000
解释: 我们在顶层（下标是（0，0）倒了两杯香槟后，有一杯量的香槟将从顶层溢出，位于（1，0）的玻璃杯和（1，1）的玻璃杯平分了这一杯香槟，所以每个玻璃杯有一半的香槟。
```

```
输入: poured = 100000009, query_row = 33, query_glass = 17
输出: 1.00000
```

# 题解

## 模拟

首先将所有的 poured 倒入第一层杯子。当有溢出的时候，将溢出的部分平均分配到下一层相邻的两个杯子中。

我们可以模拟上述过程，记录每一层杯子的状态。最后返回 `rowsCapacity[query_glass]` 和 1 中取最小值即可。

```ts
function champagneTower(poured: number, query_row: number, query_glass: number): number {
  // 初始化第一层的容量
  let rowsCapacity: number[] = [poured];
  // 遍历到目标层
  for (let i = 1; i <= query_row; i++) {
    // 下一层的杯子数
    const nextRowCapacity = new Array(i + 1).fill(0);
    for (let j = 0; j < i; j++) {
      const val = rowsCapacity[j];
      // 杯子满了才会溢出到下一层
      if (val > 1) {
        nextRowCapacity[j] += (val - 1) / 2;
        nextRowCapacity[j + 1] += (val - 1) / 2;
      }
    }
    // 重置当前层
    rowsCapacity = nextRowCapacity;
  }

  return Math.min(1, rowsCapacity[query_glass]);
}
```

```cpp
class Solution {
public:
    double champagneTower(int poured, int query_row, int query_glass) {
        vector<double> rows = {(double)poured};
        for (int i = 1; i <= query_row; i++) {
            vector<double> nextrows(i + 1, (double)0);
            for (int j = 0; j < i; j++) {
                double val = rows[j];
                if (val > 1) {
                    nextrows[j] += (val - 1) / 2;
                    nextrows[j + 1] += (val - 1) / 2;
                }
            }
            rows = nextrows;
        }
        return min(1.0, rows[query_glass]);
    }
};
```

```py
class Solution:
    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:
        rows = [poured]
        for i in range(1, query_row + 1):
            nextRows = [0] * (i + 1)
            for j, val in enumerate(rows):
                if val > 1:
                    nextRows[j] += (val - 1) / 2
                    nextRows[j + 1] += (val - 1) / 2
            rows = nextRows
        return min(1, rows[query_glass])
```
