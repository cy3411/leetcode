#
# @lc app=leetcode.cn id=799 lang=python3
#
# [799] 香槟塔
#

# @lc code=start
class Solution:
    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:
        rows = [poured]
        for i in range(1, query_row + 1):
            nextRows = [0] * (i + 1)
            for j, val in enumerate(rows):
                if val > 1:
                    nextRows[j] += (val - 1) / 2
                    nextRows[j + 1] += (val - 1) / 2
            rows = nextRows
        return min(1, rows[query_glass])
# @lc code=end
