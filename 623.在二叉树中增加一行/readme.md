# 题目

给定一个二叉树的根 `root` 和两个整数 `val` 和 `depth` ，在给定的深度 `depth` 处添加一个值为 `val` 的节点行。

注意，根节点 `root` 位于深度 `1` 。

加法规则如下:

- 给定整数 `depth`，对于深度为 `depth - 1` 的每个非空树节点 `cur` ，创建两个值为 `val` 的树节点作为 `cur` 的左子树根和右子树根。
- `cur` 原来的左子树应该是新的左子树根的左子树。
- `cur` 原来的右子树应该是新的右子树根的右子树。
- 如果 `depth == 1` 意味着 `depth - 1` 根本没有深度，那么创建一个树节点，值 `val` 作为整个原始树的新根，而原始树就是新根的左子树。

提示:

- 节点数在 $[1, 10^4]$ 范围内
- 树的深度在 $[1, 10^4]$范围内
- $-100 \leq Node.val \leq 100$
- $-10^5 \leq val \leq 10^5$
- $1 \leq depth \leq the depth of tree + 1$

# 示例

```
输入: root = [4,2,6,3,1,5], val = 1, depth = 2
输出: [4,1,1,2,null,null,6,3,1,5]
```

```
输入: root = [4,2,null,3,1], val = 1, depth = 3
输出:  [4,2,null,1,1,3,null,null,1]
```

# 题解

## 深度优先搜索

depth 为 1 的时候，直接构建一个新的根节点，将新的根节点的左子树指向原根节点。
depth 为 2 的时候，原根节点的左子树指向新构建的节点，新节点的左子树指向原根节点的左子树。原根节点的右子树指向新构建的节点，新节点的右子树指向原根节点的右子树。

depth 大于 2 的时候，递归到下一层搜索，并将 depth 减少 1，直到 depth 为 2 的时候处理。

```ts
function addOneRow(root: TreeNode | null, val: number, depth: number): TreeNode | null {
  if (!root) return null;
  if (depth === 1) {
    return new TreeNode(val, root, null);
  } else if (depth === 2) {
    root.left = new TreeNode(val, root.left, null);
    root.right = new TreeNode(val, null, root.right);
  } else {
    root.left = addOneRow(root.left, val, depth - 1);
    root.right = addOneRow(root.right, val, depth - 1);
  }
  return root;
}
```

## 广度优先搜索

广度优先搜索，将需要插入节点的上一层的所有节点找到放入队列中，然后依次处理。

```cpp
class Solution {
public:
    TreeNode *addOneRow(TreeNode *root, int val, int depth) {
        if (root == nullptr) return root;
        if (depth == 1) {
            return new TreeNode(val, root, nullptr);
        }

        deque<TreeNode *> que;
        que.push_back(root);
        for (int i = 1; i < depth - 1; i++) {
            deque<TreeNode *> tmp;
            for (auto x : que) {
                if (x->left) tmp.push_back(x->left);
                if (x->right) tmp.push_back(x->right);
            }
            que = tmp;
        }

        for (auto &node : que) {
            node->left = new TreeNode(val, node->left, nullptr);
            node->right = new TreeNode(val, nullptr, node->right);
        }

        return root;
    }
};
```
