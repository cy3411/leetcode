/*
 * @lc app=leetcode.cn id=19 lang=javascript
 *
 * [19] 删除链表的倒数第N个节点
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function (head, n) {
  const dummy = new ListNode(0, head)
  let node = dummy
  const size = getSize(head)

  for (let i = 1; i < size - n + 1; i++) {
    node = node.next
  }
  node.next = node.next.next

  return dummy.next

  function getSize (head) {
    let i = 0
    while (head) {
      i++
      head = head.next
    }
    return i
  }
};
// @lc code=end

