# 题目

序列化是将数据结构或对象转换为一系列位的过程，以便它可以存储在文件或内存缓冲区中，或通过网络连接链路传输，以便稍后在同一个或另一个计算机环境中重建。

设计一个算法来序列化和反序列化 二叉搜索树 。 对序列化/反序列化算法的工作方式没有限制。 您只需确保二叉搜索树可以序列化为字符串，并且可以将该字符串反序列化为最初的二叉搜索树。

**编码的字符串应尽可能紧凑。**

提示：

- 树中节点数范围是 [0, 104]
- 0 <= Node.val <= 104
- 题目数据 保证 输入的树是一棵二叉搜索树。

**注意**：不要使用类成员/全局/静态变量来存储状态。 你的序列化和反序列化算法应该是无状态的。

# 示例

```
输入：root = [2,1,3]
输出：[2,1,3]
```

# 题解

## 广义码+自动机

序列化就是把一个数据结构转换成可标识的字符串格式。

这里使用广义码表示序列化的记过。

例如：`root = [2,1,3]`，序列化的结果是`2(1,3)`，子节点放在括号内，逗号后面表示的是左右子节点用逗号分隔。

```ts
// 广义码
function serialize(root: TreeNode | null, res: Array<number | string> = []): string {
  if (root === null) return '';
  res.push(root.val);
  if (root.left === null && root.right === null) return res.join('');
  res.push('(');
  serialize(root.left, res);
  if (root.right) {
    res.push(',');
    serialize(root.right, res);
  }
  res.push(')');
  return res.join('');
}

/*
 * Decodes your encoded data to tree.
 */
// 自动机
function deserialize(data: string): TreeNode | null {
  const stack = [];
  //   状态码
  let scode = 0;
  let idx = 0;
  //   1表示左子节点，2表示右子节点
  let childType = 0;
  //   根节点
  let root = null;
  //   节点指针，当前处理的节点
  let p = root;
  while (idx < data.length) {
    switch (scode) {
      case 0: {
        if (data[idx] >= '0' && data[idx] <= '9') scode = 1;
        else if (data[idx] === '(') scode = 2;
        else if (data[idx] === ',') scode = 3;
        else if (data[idx] === ')') scode = 4;
        break;
      }
      case 1: {
        // 处理数字
        let num = 0;
        while (idx < data.length && data[idx] >= '0' && data[idx] <= '9') {
          num = num * 10 + Number(data[idx]);
          idx++;
        }
        p = new TreeNode(num);
        // 如果根节点为空，将p赋值给根节点
        root === null && (root = p);
        // 处理左节点
        childType === 1 && (stack[stack.length - 1].left = p);
        // 处理右节点
        childType === 2 && (stack[stack.length - 1].right = p);
        scode = 0;
        break;
      }
      case 2: {
        // 处理左括号
        idx++;
        stack.push(p);
        childType = 1;
        scode = 0;
        break;
      }
      case 3: {
        // 处理逗号，也就是右节点
        idx++;
        childType = 2;
        scode = 0;
        break;
      }
      case 4: {
        idx++;
        stack.pop();
        scode = 0;
        break;
      }
    }
  }
  return root;
}
```
