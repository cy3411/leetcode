/*
 * @lc app=leetcode.cn id=953 lang=typescript
 *
 * [953] 验证外星语词典
 */

// @lc code=start
function isAlienSorted(words: string[], order: string): boolean {
  const n = words.length;
  for (let i = 0; i < n - 1; i++) {
    const a = words[i];
    const b = words[i + 1];
    const len = Math.min(a.length, b.length);
    let isValid = false;
    for (let j = 0; j < len; j++) {
      const ac = a[j];
      const bc = b[j];
      if (order.indexOf(ac) > order.indexOf(bc)) {
        // 如果前一个字符比后一个字符大，顺序错误，返回false
        return false;
      } else if (order.indexOf(ac) < order.indexOf(bc)) {
        // 如果顺序正确，跳出当前比较循环
        isValid = true;
        break;
      }
    }
    // 上面循环比较字符都相等，需要判断长度
    if (!isValid) {
      if (a.length > b.length) {
        return false;
      }
    }
  }
  return true;
}
// @lc code=end
