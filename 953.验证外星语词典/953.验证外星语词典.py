#
# @lc app=leetcode.cn id=953 lang=python3
#
# [953] 验证外星语词典
#

# @lc code=start
class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:
        map = {c: i for i, c in enumerate(order)}
        return all(a <= b for a, b in pairwise([map[c] for c in word] for word in words))

# @lc code=end
