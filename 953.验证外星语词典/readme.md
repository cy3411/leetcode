# 题目

某种外星语也使用英文小写字母，但可能顺序 order 不同。字母表的顺序（order）是一些小写字母的排列。

给定一组用外星语书写的单词 words，以及其字母表的顺序 order，只有当给定的单词在这种外星语中按字典序排列时，返回 true；否则，返回 false。

提示：

- $1 \leq words.length \leq 100$
- $1 \leq words[i].length \leq 20$
- $order.length \equiv 26$
- 在 `words[i]` 和 `order` 中的所有字符都是英文小写字母。

# 示例

```
输入：words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
输出：true
解释：在该语言的字母表中，'h' 位于 'l' 之前，所以单词序列是按字典序排列的。
```

```
输入：words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
输出：false
解释：在该语言的字母表中，'d' 位于 'l' 之后，那么 words[0] > words[1]，因此单词序列不是按字典序排列的。
```

# 题解

## 遍历

遍历 words，比较每一对单词，如果单词在 order 中的顺序不同，则返回 false。

```ts
function isAlienSorted(words: string[], order: string): boolean {
  const n = words.length;
  for (let i = 0; i < n - 1; i++) {
    const a = words[i];
    const b = words[i + 1];
    const len = Math.min(a.length, b.length);
    let isValid = false;
    for (let j = 0; j < len; j++) {
      const ac = a[j];
      const bc = b[j];
      if (order.indexOf(ac) > order.indexOf(bc)) {
        // 如果前一个字符比后一个字符大，顺序错误，返回false
        return false;
      } else if (order.indexOf(ac) < order.indexOf(bc)) {
        // 如果顺序正确，跳出当前比较循环
        isValid = true;
        break;
      }
    }
    // 上面循环比较字符都相等，需要判断长度
    if (!isValid) {
      if (a.length > b.length) {
        return false;
      }
    }
  }
  return true;
}
```

```cpp
class Solution {
public:
    bool compare(string &a, string &b, unordered_map<char, int> &m) {
        int an = a.size(), bn = b.size();
        int n = min(an, bn);
        bool isEqual = false;
        for (int i = 0; i < n; i++) {
            if (m[a[i]] > m[b[i]]) {
                return false;
            } else if (m[a[i]] < m[b[i]]) {
                isEqual = true;
                break;
            }
        }
        if (!isEqual && an > bn) {
            return false;
        }
        return true;
    }
    bool isAlienSorted(vector<string> &words, string order) {
        unordered_map<char, int> m;
        for (int i = 0; i < order.size(); i++) {
            m[order[i]] = i;
        }
        for (int i = 0; i < words.size() - 1; i++) {
            if (!compare(words[i], words[i + 1], m)) {
                return false;
            }
        }
        return true;
    }
};
```

```py
class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:
        map = {c: i for i, c in enumerate(order)}
        return all(a <= b for a, b in pairwise([map[c] for c in word] for word in words))
```
