# 题目

给你一个 m x n 的矩阵 board ，由若干字符 'X' 和 'O' ，找到所有被 'X' 围绕的区域，并将这些区域里所有的 'O' 用 'X' 填充。

提示：

- m == board.length
- n == board[i].length
- 1 <= m, n <= 200
- board[i][j] 为 'X' 或 'O'

# 示例

[![RkyObQ.md.png](https://z3.ax1x.com/2021/06/21/RkyObQ.md.png)](https://imgtu.com/i/RkyObQ)

```
输入：board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
输出：[["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
解释：被围绕的区间不会存在于边界上，换句话说，任何边界上的 'O' 都不会被填充为 'X'。 任何不在边界上，或不与边界上的 'O' 相连的 'O' 最终都会被填充为 'X'。如果两个元素在水平或垂直方向相邻，则称它们是“相连”的。
```

# 题解

题意就是没有跟边界上的 `O` 相连的 `O`，都会转化为 `X`。

我们可以从边界位置的 `O` 开始搜索，把相连的元素改成 `o`，搜索完成，剩下的 `O` 就是被围绕的。

最后遍历一次矩阵，将 `O` 改成 `X`，`o` 还原成 `O` 就可以了。

## 深度优先搜索

```ts
function solve(board: string[][]): void {
  const dir = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  const n = board.length;
  const m = board[0].length;
  // 搜索每一个跟边界上的O相连的O
  const dfs = (i: number, j: number) => {
    board[i][j] = 'o';

    for (let k = 0; k < dir.length; k++) {
      const x = i + dir[k][0];
      const y = j + dir[k][1];
      if (x < 0 || x >= n) continue;
      if (y < 0 || y >= m) continue;
      if (board[x][y] !== 'O') continue;
      dfs(x, y);
    }
    return;
  };

  // 矩阵的第一列和最后列
  for (let i = 0; i < n; i++) {
    if (board[i][0] === 'O') dfs(i, 0);
    if (board[i][m - 1] === 'O') dfs(i, m - 1);
  }
  // 矩阵的第一行和最后行
  for (let j = 0; j < m; j++) {
    if (board[0][j] === 'O') dfs(0, j);
    if (board[n - 1][j] === 'O') dfs(n - 1, j);
  }

  // 将O换成X，o还原
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      if (board[i][j] === 'O') board[i][j] = 'X';
      if (board[i][j] === 'o') board[i][j] = 'O';
    }
  }

  return;
}
```
