# 题目

给定一个只包含正整数的非空数组。是否可以将这个数组分割成两个子集，使得两个子集的元素和相等。

注意:

- 每个数组中的元素不会超过 100
- 数组的大小不会超过 200

# 示例

```
输入: [1, 5, 11, 5]

输出: true

解释: 数组可以分割成 [1, 5, 5] 和 [11].
```

```
输入: [1, 2, 3, 5]

输出: false

解释: 数组不能分割成两个元素和相等的子集.
```

# 方法

对于这个问题，我们可以先对集合求和，得出 sum，把问题转化为背包问题：

给一个可装载重量为 sum / 2 的背包和 N 个物品，每个物品的重量为 nums[i]。现在让你装物品，是否存在一种装法，能够恰好将背包装满？

### 「状态」和「选择」

状态就是「背包的容量」和「可选择的物品」，选择就是「装进背包」或者「不装进背包」。

### dp 数组的定义

`dp[i][j] = x` 表示，对于前 i 个物品，当前背包的容量为 j 时，若 x 为 true，则说明可以恰好将背包装满，若 x 为 false，则说明不能恰好将背包装满。

根据上面的定义，我们要求的结果就是`dp[size][sum]`

base case 就是`dp[...][0]=true`，因为没有空间的话，背包就是满的。`dp[0][...]=false`, 没有物品的话，背包肯定放不满。

### 根据选择，考虑状态转移的逻辑

如果不把 nums[i]放入子集，是否能装满，取决上一个状态，`dp[i-1][j]`，继承上一个状态
如果把 nums[i]放入自己，是否能装满，取决于状态`dp[i-1][j-nums[i-1]]`

```js
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
  let size = nums.length;
  // 计算总和
  let sum = nums.reduce((acc, cur) => acc + cur);
  // 奇数不能平分，肯定没有等和子集
  if ((sum & 1) === 1) return false;

  sum /= 2;

  // 前i个数字和是否等于j
  const dp = new Array(size + 1).fill(0).map(() => new Array(sum + 1).fill(false));

  for (let i = 0; i <= size; i++) {
    // base case
    // 空间为0的话，肯定是满的了
    dp[i][0] = true;
  }

  for (let i = 1; i <= size; i++) {
    for (let j = 1; j <= sum; j++) {
      if (j - nums[i - 1] >= 0) {
        dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i - 1]];
      } else {
        dp[i][j] = dp[i - 1][j];
      }
    }
  }
  return dp[size][sum];
};
```

当前的状态总是从上一个状态转换而来，所以可以做状态压缩。

```ts
function canPartition(nums: number[]): boolean {
  let sum = nums.reduce((prev, curr) => prev + curr);
  // 不能均分
  if (sum % 2 !== 0) return false;
  // f[i]表示为能否凑成值为i
  const f: boolean[] = new Array(sum + 1).fill(false);
  // 初始化
  f[0] = true;
  sum = 0;
  for (let num of nums) {
    sum += num;
    for (let i = sum; i >= num; i--) {
      f[i] ||= f[i - num];
    }
  }

  return f[sum / 2];
}
```
