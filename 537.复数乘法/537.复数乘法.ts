/*
 * @lc app=leetcode.cn id=537 lang=typescript
 *
 * [537] 复数乘法
 */

// @lc code=start
function complexNumberMultiply(num1: string, num2: string): string {
  const complex1 = num1.split('+');
  const complex2 = num2.split('+');
  const a1 = parseInt(complex1[0]);
  const b1 = parseInt(complex1[1]);
  const a2 = parseInt(complex2[0]);
  const b2 = parseInt(complex2[1]);

  return `${a1 * a2 - b1 * b2}+${a1 * b2 + a2 * b1}i`;
}
// @lc code=end
