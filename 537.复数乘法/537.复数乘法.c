/*
 * @lc app=leetcode.cn id=537 lang=c
 *
 * [537] 复数乘法
 */

// @lc code=start

void parseComplexNumber(const char *num, int *real, int *image)
{
    char *token = strtok(num, "+");
    *real = atoi(token);
    token = strtok(NULL, "i");
    *image = atoi(token);
}

char *complexNumberMultiply(char *num1, char *num2)
{
    int a1 = 0, b1 = 0, a2 = 0, b2 = 0;
    char *ans = (char *)malloc(sizeof(char) * 20);
    parseComplexNumber(num1, &a1, &b1);
    parseComplexNumber(num2, &a2, &b2);
    snprintf(ans, 20, "%d+%di", a1 * a2 - b1 * b2, a1 * b2 + a2 * b1);
    return ans;
}
// @lc code=end
