/*
 * @lc app=leetcode.cn id=537 lang=cpp
 *
 * [537] 复数乘法
 */

// @lc code=start
class Solution
{
public:
    vector<int> getNumber(string num, const char demlite)
    {
        vector<int> res;
        stringstream input(num);
        string temp;
        while (getline(input, temp, demlite))
        {
            res.push_back(stoi(temp));
        }
        return res;
    }
    string complexNumberMultiply(string num1, string num2)
    {
        int a1 = getNumber(num1, '+')[0];
        int b1 = getNumber(num1, '+')[1];
        int a2 = getNumber(num2, '+')[0];
        int b2 = getNumber(num2, '+')[1];

        return to_string(a1 * a2 - b1 * b2) + "+" + to_string(a1 * b2 + a2 * b1) + "i";
    }
};
// @lc code=end
