# 题目

[复数](https://baike.baidu.com/item/%E5%A4%8D%E6%95%B0/254365?fr=aladdin) 可以用字符串表示，遵循 `"实部+虚部 i"` 的形式，并满足下述条件：

- `实部` 是一个整数，取值范围是 `[-100, 100]`
- `虚部` 也是一个整数，取值范围是 `[-100, 100]`
- $i^2 == -1$

给你两个字符串表示的复数 `num1` 和 `num2` ，请你遵循复数表示形式，返回表示它们乘积的字符串。

提示：

- `num1` 和 `num2` 都是有效的复数表示。

# 示例

```
输入：num1 = "1+1i", num2 = "1+1i"
输出："0+2i"
解释：(1 + i) * (1 + i) = 1 + i2 + 2 * i = 2i ，你需要将它转换为 0+2i 的形式。
```

```
输入：num1 = "1+-1i", num2 = "1+-1i"
输出："0+-2i"
解释：(1 - i) * (1 - i) = 1 + i2 - 2 * i = -2i ，你需要将它转换为 0+-2i 的形式。
```

# 题解

## 模拟

解析出 `num1` 和 `num2` 的实部和虚部，然后套用复数乘法公式，最后再转换为字符串。

$$
\begin{aligned}
(a_1 + b_1 * i) x (a_2 + b_2 * i)
& = (a_1 * a_2) + (a_1 * b_2 + b_1 * a_2) * i + (b_1 * b_2) * i^2, (i^2=-1)\\
& = (a_1 * a_2) + (a_1 * b_2 + b_1 * a_2) * i - (b_1 * b_2) \\
& = (a_1 * a_2 - b_1 * b_2) + (a_1 * b_2 + b_1 * a_2) * i \\
\end{aligned}
$$

```ts
function complexNumberMultiply(num1: string, num2: string): string {
  const complex1 = num1.split('+');
  const complex2 = num2.split('+');
  const a1 = parseInt(complex1[0]);
  const b1 = parseInt(complex1[1]);
  const a2 = parseInt(complex2[0]);
  const b2 = parseInt(complex2[1]);

  return `${a1 * a2 - b1 * b2}+${a1 * b2 + a2 * b1}i`;
}
```

```cpp
class Solution
{
public:
    vector<int> getNumber(string num, const char demlite)
    {
        vector<int> res;
        stringstream input(num);
        string temp;
        while (getline(input, temp, demlite))
        {
            res.push_back(stoi(temp));
        }
        return res;
    }
    string complexNumberMultiply(string num1, string num2)
    {
        int a1 = getNumber(num1, '+')[0];
        int b1 = getNumber(num1, '+')[1];
        int a2 = getNumber(num2, '+')[0];
        int b2 = getNumber(num2, '+')[1];

        return to_string(a1 * a2 - b1 * b2) + "+" + to_string(a1 * b2 + a2 * b1) + "i";
    }
};
```

```cpp
void parseComplexNumber(const char *num, int *real, int *image)
{
    char *token = strtok(num, "+");
    *real = atoi(token);
    token = strtok(NULL, "i");
    *image = atoi(token);
}

char *complexNumberMultiply(char *num1, char *num2)
{
    int a1 = 0, b1 = 0, a2 = 0, b2 = 0;
    char *ans = (char *)malloc(sizeof(char) * 20);
    parseComplexNumber(num1, &a1, &b1);
    parseComplexNumber(num2, &a2, &b2);
    snprintf(ans, 20, "%d+%di", a1 * a2 - b1 * b2, a1 * b2 + a2 * b1);
    return ans;
}
```
