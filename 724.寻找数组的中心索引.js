/*
 * @lc app=leetcode.cn id=724 lang=javascript
 *
 * [724] 寻找数组的中心索引
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var pivotIndex = function (nums) {
  const total = nums.reduce((a, b) => a + b, 0);
  let sum = 0;
  // sum*2+nums[i] === total
  for (let i = 0; i < nums.length; i++) {
    if ((sum << 1) + nums[i] === total) {
      return i;
    }
    sum += nums[i];
  }

  return -1;
};
// @lc code=end
