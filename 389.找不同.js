/*
 * @lc app=leetcode.cn id=389 lang=javascript
 *
 * [389] 找不同
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {character}
 */
var findTheDifference = function (s, t) {
  let bit = 0;

  for (let i = 0; i < s.length; i++) {
    bit ^= s.charCodeAt(i);
  }

  for (let i = 0; i < t.length; i++) {
    bit ^= t.charCodeAt(i);
  }

  return String.fromCharCode(bit);
};
// @lc code=end
