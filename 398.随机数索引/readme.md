# 题目

给定一个可能含有重复元素的整数数组，要求随机输出给定的数字的索引。 您可以假设给定的数字一定存在于数组中。

注意：
数组大小可能非常大。 使用太多额外空间的解决方案将不会通过测试。

# 示例

```
int[] nums = new int[] {1,2,3,3,3};
Solution solution = new Solution(nums);

// pick(3) 应该返回索引 2,3 或者 4。每个索引的返回概率应该相等。
solution.pick(3);

// pick(1) 应该返回 0。因为只有nums[0]等于1。
solution.pick(1);
```

# 题解

## 哈希表

使用哈希表将数组中的元素和索引存储在一个键值对中，然后随机选择一个索引。

```ts
class Solution {
  hash: Map<number, number[]> = new Map();
  constructor(nums: number[]) {
    for (let i = 0; i < nums.length; i++) {
      const x = nums[i];
      if (!this.hash.has(x)) this.hash.set(x, []);
      this.hash.get(x).push(i);
    }
  }

  pick(target: number): number {
    const res = this.hash.get(target);
    return res[Math.floor(Math.random() * res.length)];
  }
}
```

## 蓄水池抽样

```cpp
class Solution
{
    vector<int> nums;

public:
    Solution(vector<int> &nums) : nums(nums)
    {
    }

    int pick(int target)
    {
        int res = 0, count = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] == target)
            {
                // 第count次出现的target
                count++;
                if (rand() % count == 0)
                    res = i;
            }
        }
        return res;
    }
};
```
