/*
 * @lc app=leetcode.cn id=398 lang=typescript
 *
 * [398] 随机数索引
 */

// @lc code=start
class Solution {
  hash: Map<number, number[]> = new Map();
  constructor(nums: number[]) {
    for (let i = 0; i < nums.length; i++) {
      const x = nums[i];
      if (!this.hash.has(x)) this.hash.set(x, []);
      this.hash.get(x).push(i);
    }
  }

  pick(target: number): number {
    const res = this.hash.get(target);
    return res[Math.floor(Math.random() * res.length)];
  }
}

/**
 * Your Solution object will be instantiated and called as such:
 * var obj = new Solution(nums)
 * var param_1 = obj.pick(target)
 */
// @lc code=end
