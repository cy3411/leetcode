# 题目

给定一个化学式 formula（作为字符串），返回每种原子的数量。

原子总是以一个大写字母开始，接着跟随 0 个或任意个小写字母，表示原子的名字。

如果数量大于 1，原子后会跟着数字表示原子的数量。如果数量等于 1 则不会跟数字。例如，H2O 和 H2O2 是可行的，但 H1O2 这个表达是不可行的。

两个化学式连在一起是新的化学式。例如 H2O2He3Mg4 也是化学式。

一个括号中的化学式和数字（可选择性添加）也是化学式。例如 (H2O2) 和 (H2O2)3 是化学式。

给定一个化学式 formula ，返回所有原子的数量。格式为：第一个（按字典序）原子的名字，跟着它的数量（如果数量大于 1），然后是第二个原子的名字（按字典序），跟着它的数量（如果数量大于 1），以此类推。

提示：

- 1 <= formula.length <= 1000
- formula 由小写英文字母、数字 '(' 和 ')' 组成。
- formula 是有效的化学式。

# 示例

```
输入：formula = "H2O"
输出："H2O"
解释：
原子的数量是 {'H': 2, 'O': 1}。
```

# 题解

## 哈希表+栈

从后往前遍历：

- 遇到数字，保存到当前变量
- 遇到小写字母，保存到当前变量中
- 遇到右括号，将当前数组和栈顶数组相乘入栈，并清空当前数字变量
- 遇到左括号，栈顶数字出栈
- 遇到大写字母，将当前数字和栈顶数字相乘后，与哈希表中的数量相加

统计完原子的出现次数后，按照字典序排序后，更新答案。

```ts
function countOfAtoms(formula: string): string {
  // 看见右括号，入栈次数
  const stack = [1];
  // 保存原子出现的总次数
  const hash: Map<string, number> = new Map();

  let i = formula.length - 1;
  let count = '';
  let lowerChar = '';
  while (i >= 0) {
    const char = formula[i];
    if (char >= '0' && char <= '9') {
      count = char + count;
    } else if (char >= 'a' && char <= 'z') {
      lowerChar = char + lowerChar;
    } else if (char === ')') {
      stack.push(stack[stack.length - 1] * Number(count || '1'));
      count = '';
    } else if (char === '(') {
      stack.pop();
    } else {
      let atom = char + lowerChar;
      hash.set(atom, (hash.get(atom) || 0) + stack[stack.length - 1] * Number(count || '1'));
      lowerChar = count = '';
    }
    i--;
  }
  // 按照字典序排序
  const atoms = Array.from(hash);
  atoms.sort((a, b): number => {
    if (a[0] < b[0]) return -1;
    if (a[0] < b[0]) return 1;
    return 0;
  });
  // 更新结果
  let ans = '';
  for (let [char, count] of atoms) {
    ans += char;
    if (count > 1) {
      ans += String(count);
    }
  }

  return ans;
}
```
