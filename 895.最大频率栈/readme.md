# 题目

设计一个类似堆栈的数据结构，将元素推入堆栈，并从堆栈中弹出出现频率最高的元素。

实现 `FreqStack` 类:

- `FreqStack()` 构造一个空的堆栈。
- `void push(int val)` 将一个整数 `val` 压入栈顶。
- `int pop()` 删除并返回堆栈中出现频率最高的元素。
  - 如果出现频率最高的元素不只一个，则移除并返回最接近栈顶的元素。

提示：

- $0 \leq val \leq 10^9$
- `push` 和 `pop` 的操作数不大于 $2 * 10^4$。
- 输入保证在调用 `pop` 之前堆栈中至少有一个元素。

# 示例

```
输入：
["FreqStack","push","push","push","push","push","push","pop","pop","pop","pop"],
[[],[5],[7],[5],[7],[4],[5],[],[],[],[]]
输出：[null,null,null,null,null,null,null,5,7,5,4]
解释：
FreqStack = new FreqStack();
freqStack.push (5);//堆栈为 [5]
freqStack.push (7);//堆栈是 [5,7]
freqStack.push (5);//堆栈是 [5,7,5]
freqStack.push (7);//堆栈是 [5,7,5,7]
freqStack.push (4);//堆栈是 [5,7,5,7,4]
freqStack.push (5);//堆栈是 [5,7,5,7,4,5]
freqStack.pop ();//返回 5 ，因为 5 出现频率最高。堆栈变成 [5,7,5,7,4]。
freqStack.pop ();//返回 7 ，因为 5 和 7 出现频率最高，但7最接近顶部。堆栈变成 [5,7,5,4]。
freqStack.pop ();//返回 5 ，因为 5 出现频率最高。堆栈变成 [5,7,4]。
freqStack.pop ();//返回 4 ，因为 4, 5 和 7 出现频率最高，但 4 是最接近顶部的。堆栈变成 [5,7]。
```

# 题解

## 哈希表

使用哈希表 freq 记录每个数字的出现的频率，stacks 记录每个频率对应的栈。

- push 时，增加 val 频率，并将 val 压入到当前频率对应的栈中，同步更新最高频率值。
- pop 时，取得最高频率对应的栈，返回栈顶元素。并更新返回值的频率和最高频率值。

```ts
class FreqStack {
  // 数字的频率
  freq: Map<number, number>;
  // 不同的频率对应的栈
  stacks: Map<number, number[]>;
  // 最高频率
  maxFreq: number;
  constructor() {
    this.freq = new Map();
    this.stacks = new Map();
    this.maxFreq = 0;
  }

  push(val: number): void {
    // 增加val的频率
    this.freq.set(val, (this.freq.get(val) ?? 0) + 1);
    const freq = this.freq.get(val)!;
    // 如果当前频率没有栈，创建
    if (!this.stacks.has(freq)) {
      this.stacks.set(freq, []);
    }
    // 将val压入多对应频率的栈中
    this.stacks.get(freq)!.push(val);
    // 更新最高频率
    this.maxFreq = Math.max(this.maxFreq, freq);
  }

  pop(): number {
    const stack = this.stacks.get(this.maxFreq)!;
    // 将当前数字从最高频率栈中移除
    const val = stack.pop()!;
    // 减少当前数字的频率
    this.freq.set(val, this.freq.get(val)! - 1);
    // 如果最高频率栈中为空，最高频率减少
    if (this.stacks.get(this.maxFreq)?.length === 0) {
      this.maxFreq--;
    }
    return val;
  }
}
```
