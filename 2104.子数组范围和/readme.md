# 题目

给你一个整数数组 `nums` `。nums` 中，子数组的 **范围** 是子数组中最大元素和最小元素的差值。

返回 `nums` 中 **所有** 子数组范围的 **和** 。

子数组是数组中一个连续 **非空** 的元素序列。

提示：

- $\color{burlywood} 1 \leq nums.length \leq 1000$
- $\color{burlywood} -10^9 \leq nums[i] \leq 10^9$

# 示例

```
输入：nums = [1,2,3]
输出：4
解释：nums 的 6 个子数组如下所示：
[1]，范围 = 最大 - 最小 = 1 - 1 = 0
[2]，范围 = 2 - 2 = 0
[3]，范围 = 3 - 3 = 0
[1,2]，范围 = 2 - 1 = 1
[2,3]，范围 = 3 - 2 = 1
[1,2,3]，范围 = 3 - 1 = 2
所有范围的和是 0 + 0 + 0 + 1 + 1 + 2 = 4
```

# 题解

## 单调队列

定义两个单调队列，一个用来存储最小值下标，一个用来存储最大值下标。

每次更新完队列，就去计算当前位置的范围和，将结果累加到答案中。

计算范围和的方式就是按序取两个队列的队首元素，计算差值乘上可形成的区间个数，然后根据将两者中最小的索引位置后移，继续计算。

举个例子：

`nums = [8,7,9]`,当计算的到最后一个元素的时候，队列 `minQ = [1,2]`, `maxQ = [2]`。

这个时候 `[8,7,9]` 和 `[7,9]` 这两个子数组的范围和是一样的，只需要计算一次，让后将结果乘上当前最小的索引和前面位置的元素个数，也就是可以形成几个子数组的数量。

```ts
function subArrayRanges(nums: number[]): number {
  // 计算当前位置前的区间和
  const getValue = (): number => {
    let res = 0;
    let pre_pos = -1;
    let i = 0;
    let j = 0;
    while (i < minQ.length) {
      let p = minQ[i];
      let q = maxQ[j];
      let cur_pos = Math.min(p, q);
      res += (cur_pos - pre_pos) * (nums[q] - nums[p]);
      if (p === cur_pos) i++;
      if (q === cur_pos) j++;
      pre_pos = cur_pos;
    }

    return res;
  };

  let n = nums.length;
  let ans = 0;
  // 单调队列，保存区间最小值的下标
  const minQ = [];
  // 单调队列，保存区间最大值的下标
  const maxQ = [];
  for (let i = 0; i < n; i++) {
    while (minQ.length && nums[i] <= nums[minQ[minQ.length - 1]]) {
      minQ.pop();
    }
    while (maxQ.length && nums[i] >= nums[maxQ[maxQ.length - 1]]) {
      maxQ.pop();
    }
    minQ.push(i), maxQ.push(i);
    // 计算当前位置前的区间和
    ans += getValue();
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    long long getValue(const deque<int> &q_min, const deque<int> &q_max, vector<int> &nums)
    {
        // 迭代器
        auto p = q_min.begin(), q = q_max.begin();
        int pre_pos = -1;
        long long res = 0;
        while (p != q_min.end())
        {
            // 取出最小值
            int pos = min(*p, *q);
            // 计算和
            res += (long long)(pos - pre_pos) * (long long)(nums[*q] - nums[*p]);
            // 如果最小值是当前值，则迭代器后移
            if (*p == pos)
                p++;
            // 如果最大值是当前值，则迭代器后移
            if (*q == pos)
                q++;
            // 更新最小值
            pre_pos = pos;
        }

        return res;
    }
    long long subArrayRanges(vector<int> &nums)
    {
        deque<int> q_min, q_max;
        long long ans = 0;
        int n = nums.size();

        for (int i = 0; i < n; i++)
        {
            while (!q_min.empty() && nums[i] <= nums[q_min.back()])
            {
                q_min.pop_back();
            }
            while (!q_max.empty() && nums[i] >= nums[q_max.back()])
            {
                q_max.pop_back();
            }
            q_min.push_back(i), q_max.push_back(i);
            ans += getValue(q_min, q_max, nums);
        }

        return ans;
    }
};
```
