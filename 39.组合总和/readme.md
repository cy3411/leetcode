# 题目

给定一个无重复元素的数组 `candidates` 和一个目标数 `target` ，找出 `candidates` 中所有可以使数字和为 `target` 的组合。

`candidates` 中的数字可以无限制重复被选取。

说明：

- 所有数字（包括 `target`）都是正整数。
- 解集不能包含重复的组合。

提示：

- `1 <= candidates.length <= 30`
- `1 <= candidates[i] <= 200`
- `candidate` 中的每个元素都是独一无二的。
- `1 <= target <= 500`

# 示例

```
输入：candidates = [2,3,6,7], target = 7,
所求解集为：
[
  [7],
  [2,2,3]
]
```

# 题解

## 回溯

```ts
function combinationSum(candidates: number[], target: number): number[][] {
  const temp = [];
  const ans = [];
  const backtrack = (idx: number, target: number): void => {
    if (target < 0) return;
    // 满足和为target的集合
    if (target === 0) {
      ans.push([...temp]);
      return;
    }
    if (idx === candidates.length) return;
    // 不选择当前数字
    backtrack(idx + 1, target);
    // 选择当前数字
    temp.push(candidates[idx]);
    // 选择当前数字后，继续递归找target剩余的数字
    backtrack(idx, target - candidates[idx]);
    // 回溯
    temp.pop();
    return;
  };

  backtrack(0, target);

  return ans;
}
```
