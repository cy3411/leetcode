# 题目

给你一个日志数组 `logs`。每条日志都是以空格分隔的字串，其第一个字为字母与数字混合的 **标识符** 。

有两种不同类型的日志：

- **字母日志**：除标识符之外，所有字均由小写字母组成
- **数字日志**：除标识符之外，所有字均由数字组成

请按下述规则将日志重新排序：

所有 **字母日志** 都排在 **数字日志** 之前。

- **字母日志** 在内容不同时，忽略标识符后，按内容字母顺序排序；在内容相同时，按标识符排序。
- **数字日志** 应该保留原来的相对顺序。

返回日志的最终顺序。

提示：

- $1 \leq logs.length \leq 100$
- $3 \leq logs[i].length \leq 100$
- `logs[i]` 中，字与字之间都用 **单个** 空格分隔
- 题目数据保证 `logs[i]` 都有一个标识符，并且在标识符之后至少存在一个字

# 示例

```
输入：logs = ["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"]
输出：["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]
解释：
字母日志的内容都不同，所以顺序为 "art can", "art zero", "own kit dig" 。
数字日志保留原来的相对顺序 "dig1 8 1 5 1", "dig2 3 6" 。
```

```
输入：logs = ["a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo"]
输出：["g1 act car","a8 act zoo","ab1 off key dog","a1 9 2 3 1","zo4 4 7"]
```

# 题解

## 自定义排序

自定义排序规则，先将日志按照第一个空格分成标识符和内容两部分，当两个日志进行比较时，先确定标识符类型，然后按照以下规则比较：

- 数字日志只需要保留相对位置，不需要比较内容，只需要比较下标位置
- 字母日志如果内容相同，则按照标识符排序，如果内容不同，则内容排序
- 字母日志始终小于数字日志

```ts
type UnitData = [string, number];

function logCompare(a: UnitData, b: UnitData): number {
  const [aUID, ...aLogs]: string[] = a[0].split(' ');
  const [bUID, ...bLogs]: string[] = b[0].split(' ');
  const aPrefix = aLogs.join(' ');
  const bPrefix = bLogs.join(' ');

  // 如果是数字日志，直接比较索引
  if (isDigit(aPrefix) && isDigit(bPrefix)) {
    return a[1] - b[1];
  }
  // 如果是字符日志，比较前缀
  if (!isDigit(aPrefix) && !isDigit(bPrefix)) {
    if (aPrefix === bPrefix) {
      return Stringcompare(aUID, bUID);
    }
    return Stringcompare(aPrefix, bPrefix);
  }
  // 字符日志放在前面
  return isDigit(aPrefix) ? 1 : -1;
}

function Stringcompare(a: string, b: string): number {
  if (a === b) return 0;
  if (a > b) return 1;
  return -1;
}

function isDigit(s: string): boolean {
  return !isNaN(parseInt(s));
}

function reorderLogFiles(logs: string[]): string[] {
  const n = logs.length;
  const data: UnitData[] = new Array(n);
  // 预处理，将索引也放入到需要排序的数组中
  // 处理相对位置不变的数字日志
  for (let i = 0; i < n; i++) {
    data[i] = [logs[i], i];
  }
  // 自定义排序
  data.sort((a, b) => logCompare(a, b));

  // 处理结果
  const ans = new Array(n);
  for (let i = 0; i < n; i++) {
    ans[i] = data[i][0];
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<string> reorderLogFiles(vector<string> &logs)
    {
        stable_sort(logs.begin(), logs.end(), [&](const string &a, const string &b)
                    {
            int aPos = a.find_first_of(' '), bPos = b.find_first_of(' ');
            int isDigitA = isdigit(a[aPos + 1]), isDigitB = isdigit(b[bPos + 1]);
            if (isDigitA && isDigitB)
            {
                return false;
            }
            if (!isDigitA && !isDigitB)
            {
                string aStr = a.substr(aPos), bStr = b.substr(bPos);
                if (aStr == bStr)
                    return a < b;
                return aStr < bStr;
            }
            return isDigitA ? false : true; });

        return logs;
    }
};
```

```py
class Solution:
    def transKey(self, key) -> tuple:
        id, cnt = key.split(" ", 1)
        # 如果是数字日志，按照(0, 内容，标识符) 排序
        # 如果是字母日志，按照(1, 内容，标识符) 排序
        return (0, cnt, id) if cnt[0].isalpha() else (1,)

    def reorderLogFiles(self, logs: List[str]) -> List[str]:
        logs.sort(key=self.transKey)

        return logs
```
