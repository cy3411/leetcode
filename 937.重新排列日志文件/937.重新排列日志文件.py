#
# @lc app=leetcode.cn id=937 lang=python3
#
# [937] 重新排列日志文件
#

# @lc code=start


class Solution:
    def transKey(self, key) -> tuple:
        id, cnt = key.split(" ", 1)
        # 如果是数字日志，按照(0, 内容，标识符) 排序
        # 如果是字母日志，按照(1, 内容，标识符) 排序
        return (0, cnt, id) if cnt[0].isalpha() else (1,)

    def reorderLogFiles(self, logs: List[str]) -> List[str]:
        logs.sort(key=self.transKey)

        return logs


# @lc code=end
