/*
 * @lc app=leetcode.cn id=937 lang=cpp
 *
 * [937] 重新排列日志文件
 */

// @lc code=start
class Solution
{
public:
    vector<string> reorderLogFiles(vector<string> &logs)
    {
        stable_sort(logs.begin(), logs.end(), [&](const string &a, const string &b)
                    {
            int aPos = a.find_first_of(' '), bPos = b.find_first_of(' ');
            int isDigitA = isdigit(a[aPos + 1]), isDigitB = isdigit(b[bPos + 1]);
            if (isDigitA && isDigitB)
            {
                return false;
            }
            if (!isDigitA && !isDigitB)
            {
                string aStr = a.substr(aPos), bStr = b.substr(bPos);
                if (aStr == bStr)
                    return a < b;
                return aStr < bStr;
            }
            return isDigitA ? false : true; });

        return logs;
    }
};
// @lc code=end
