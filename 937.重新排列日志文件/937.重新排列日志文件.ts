/*
 * @lc app=leetcode.cn id=937 lang=typescript
 *
 * [937] 重新排列日志文件
 */

// @lc code=start

type UnitData = [string, number];

function logCompare(a: UnitData, b: UnitData): number {
  const [aUID, ...aLogs]: string[] = a[0].split(' ');
  const [bUID, ...bLogs]: string[] = b[0].split(' ');
  const aPrefix = aLogs.join(' ');
  const bPrefix = bLogs.join(' ');

  // 如果是数字日志，直接比较索引
  if (isDigit(aPrefix) && isDigit(bPrefix)) {
    return a[1] - b[1];
  }
  // 如果是字符日志，比较前缀
  if (!isDigit(aPrefix) && !isDigit(bPrefix)) {
    if (aPrefix === bPrefix) {
      return Stringcompare(aUID, bUID);
    }
    return Stringcompare(aPrefix, bPrefix);
  }
  // 字符日志放在前面
  return isDigit(aPrefix) ? 1 : -1;
}

function Stringcompare(a: string, b: string): number {
  if (a === b) return 0;
  if (a > b) return 1;
  return -1;
}

function isDigit(s: string): boolean {
  return !isNaN(parseInt(s));
}

function reorderLogFiles(logs: string[]): string[] {
  const n = logs.length;
  const data: UnitData[] = new Array(n);
  // 预处理，将索引也放入到需要排序的数组中
  // 处理相对位置不变的数字日志
  for (let i = 0; i < n; i++) {
    data[i] = [logs[i], i];
  }
  // 自定义排序
  data.sort((a, b) => logCompare(a, b));

  // 处理结果
  const ans = new Array(n);
  for (let i = 0; i < n; i++) {
    ans[i] = data[i][0];
  }

  return ans;
}
// @lc code=end
