# 题目

给定一个二叉树，计算 整个树 的坡度 。

一个树的 节点的坡度 定义即为，该节点左子树的节点之和和右子树节点之和的 差的绝对值 。如果没有左子树的话，左子树的节点之和为 0 ；没有右子树的话也是一样。空结点的坡度是 0 。

整个树 的坡度就是其所有节点的坡度之和。

提示：

- 树中节点数目的范围在 $[0, 10^4]$ 内
- `-1000 <= Node.val <= 1000`

# 示例

# 题解

## 深度优先搜索

根节点开始遍历，计算左右子树的坡度，然后计算自身的坡度，最后把所有节点的坡度加起来。

```ts
function findTilt(root: TreeNode | null): number {
  let ans = 0;
  // 深度搜索
  const dfs = (node: TreeNode | null): number => {
    if (node === null) return 0;
    const left = dfs(node.left);
    const right = dfs(node.right);
    ans += Math.abs(left - right);
    return left + right + node.val;
  };
  dfs(root);
  return ans;
}
```
