# 题目

给定一个整数数组 `nums`，其中恰好有两个元素只出现一次，其余所有元素均出现两次。 找出只出现一次的那两个元素。你可以按 **任意顺序** 返回答案。

进阶：你的算法应该具有线性时间复杂度。你能否仅使用常数空间复杂度来实现？

提示：

- $2 \leq nums.length \leq 3 * 10^4$
- $-2^31 \leq nums[i] \leq 2^{31} - 1$
- 除两个只出现一次的整数外，`nums` 中的其他数字都出现两次

# 示例

```
输入：nums = [1,2,1,3,2,5]
输出：[3,5]
解释：[5, 3] 也是有效的答案。
```

```
输入：nums = [-1,0]
输出：[-1,0]
```

# 题解

## 位操作

我们对整个数组做异或运算，最后的结果 `xornum` 一定是两个只出现一次的元素的异或结果。

现在就是该如何把两个元素从异或结果中区分出来。

我们可以找出 `xornum` 中某个二进制位为 1 的位置（`lowbit`）。根据异或运算的特性，两个不同数的相同位的异或结果为 1，肯定是一个 0 和一个 1 才可以。

可以通过`lowbit`将 `nums` 分成 `2` 组,一组是跟 lowbit 同位都为 1，一组是跟 lowbit 同位不相同。

分别的异或结果就是两个不同的元素。

```ts
function singleNumber(nums: number[]): number[] {
  let xornum = 0;
  // 最后的结果是只出现1次的两个元素的异或结果
  for (let num of nums) {
    xornum ^= num;
  }

  let x = 0;
  let y = 0;
  // 最低位的1的位置
  const lowbit = xornum & -xornum;

  for (let num of nums) {
    if (num & lowbit) {
      x ^= num;
    } else {
      y ^= num;
    }
  }

  return [x, y];
}
```
