/*
 * @lc app=leetcode.cn id=260 lang=typescript
 *
 * [260] 只出现一次的数字 III
 */

// @lc code=start
function singleNumber(nums: number[]): number[] {
  let xornum = 0;
  // 最后的结果是只出现1次的两个元素的异或结果
  for (let num of nums) {
    xornum ^= num;
  }

  let x = 0;
  let y = 0;
  // 最低位的1的位置
  const lowbit = xornum & -xornum;

  for (let num of nums) {
    if (num & lowbit) {
      x ^= num;
    } else {
      y ^= num;
    }
  }

  return [x, y];
}
// @lc code=end
