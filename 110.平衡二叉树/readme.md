# 题目
给定一个二叉树，判断它是否是高度平衡的二叉树。

本题中，一棵高度平衡二叉树定义为：

> 一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。

提示：

+ 树中的节点数在范围 [0, 5000] 内
+ -104 <= Node.val <= 104

# 示例
```
输入：root = [3,9,20,null,null,15,7]
输出：true
```
```
输入：root = [1,2,2,3,3,null,null,4,4]
输出：false
```
```
输入：root = []
输出：true
```

# 方法
判断当前节点的左右节点是否平衡，是的话就返回高度，否的话就返回一个负值。
```js
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isBalanced = function (root) {
  if (root === null) return true;

  const getHeight = (root) => {
    if (root === null) return 0;

    // 左子树高度
    let left = getHeight(root.left);
    // 右子树高度
    let right = getHeight(root.right);

    // 高度差超过1，无效的
    if (Math.abs(left - right) > 1) return -1;
    // 左子树或者右子树有无效的
    if (left < 0 || right < 0) return -1;

    // root的高度
    return Math.max(left, right) + 1;
  };

  return getHeight(root) >= 0;
};
```