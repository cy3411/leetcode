# 题目

**n 皇后问题** 研究的是如何将 `n` 个皇后放置在 `n×n` 的棋盘上，并且使皇后彼此之间不能相互攻击。

给你一个整数 `n` ，返回所有不同的 **n 皇后问题** 的解决方案。

每一种解法包含一个不同的 **n 皇后问题** 的棋子放置方案，该方案中 'Q' 和 '.' 分别代表了皇后和空位。

提示：

- 1 <= n <= 9
- 皇后彼此不能相互攻击，也就是说：任何两个皇后都不能处于同一条横行、纵行或斜线上。

# 示例

[![RJAJpT.md.png](https://z3.ax1x.com/2021/06/26/RJAJpT.md.png)](https://imgtu.com/i/RJAJpT)

```
输入：n = 4
输出：[[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
解释：如上图所示，4 皇后问题存在两个不同的解法。
```

# 题解

## 回溯

选择行，然后遍历当前行的每一列，选择合法的位置放置皇后，然后继续选择下一行执行相同操作。如果当前行选择不到合法位置，就回溯回去，找其他选择。

最后行数等于 n，代表到头了，有合法数据，就可以更新答案了。

判断合法位置的时候，有 3 个条件，列、对角线和反对角线。

```ts
function solveNQueens(n: number): string[][] {
  // 初始化棋盘
  const board = new Array(n).fill('.').map(() => new Array(n).fill('.'));
  const backtrack = (idx: number) => {
    // 最后一行，代表有合法数据，更新结果
    if (idx === n) {
      const result = new Array(n).fill('.');
      for (let i = 0; i < board.length; i++) {
        result[i] = board[i].join('');
      }
      ans.push(result);
      return;
    }
    // 遍历每一列
    for (let j = 0; j < n; j++) {
      if (visitedCol.has(j)) continue;
      if (visitedDiagonal.has(idx - j)) continue;
      if (visitedBackDiagonal.has(idx + j)) continue;
      // 选择当前位置
      board[idx][j] = 'Q';
      visitedCol.add(j);
      visitedDiagonal.add(idx - j);
      visitedBackDiagonal.add(idx + j);
      // 继续选择下一行
      backtrack(idx + 1);
      // 回溯到上一个位置
      board[idx][j] = '.';
      visitedCol.delete(j);
      visitedDiagonal.delete(idx - j);
      visitedBackDiagonal.delete(idx + j);
    }
  };
  // 存在皇后的列
  const visitedCol: Set<number> = new Set();
  // 存在皇后的对角线
  const visitedDiagonal: Set<number> = new Set();
  // 存在皇后的反对角线
  const visitedBackDiagonal: Set<number> = new Set();
  const ans = [];

  backtrack(0);

  return ans;
}
```
