# 描述

给你一个长度为 n 的链表，每个节点包含一个额外增加的随机指针 random ，该指针可以指向链表中的任何节点或空节点。

构造这个链表的 深拷贝。 深拷贝应该正好由 n 个 全新 节点组成，其中每个新节点的值都设为其对应的原节点的值。新节点的 next 指针和 random 指针也都应指向复制链表中的新节点，并使原链表和复制链表中的这些指针能够表示相同的链表状态。复制链表中的指针都不应指向原链表中的节点 。

例如，如果原链表中有 X 和 Y 两个节点，其中 X.random --> Y 。那么在复制链表中对应的两个节点 x 和 y ，同样有 x.random --> y 。

返回复制链表的头节点。

用一个由 n 个节点组成的链表来表示输入/输出中的链表。每个节点用一个 [val, random_index] 表示：

- val：一个表示 Node.val 的整数。
- random_index：随机指针指向的节点索引（范围从 0 到 n-1）；如果不指向任何节点，则为 null 。

你的代码 只 接受原链表的头节点 head 作为传入参数。

# 示例

![示例图片](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2020/01/09/e1.png)
<https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2020/01/09/e1.png>

```
输入：head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
输出：[[7,null],[13,0],[11,4],[10,2],[1,0]]
```

# 方法

由于有随机节点的存在，我们要想办法找到这个随机的位置。
在原链表上复制每一个节点，将复制节点链接到原节点的后面：

```
前:1->2->3
后:1->1'->2->2'->3->3'
```

这样就可以知道原节点的后面就是复制节点，这样可以方便更新 random 的指针到复制节点上

```js
/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  if (head === null) return head;

  // 复制节点,每个原节点后都有1个复制的新节点
  let p = head;
  while (p) {
    let node = new Node(p.val, p.next, p.random);
    p.next = node;
    p = node.next;
  }

  // 更新复制节点的random指针
  p = head.next;
  while (p) {
    p.random = p.random ? p.random.next : null;
    // 最后一个节点判断
    p = p.next ? p.next.next : null;
  }

  // 将复制节点取出来
  const newHead = head.next;
  let q = head.next;
  p = head;
  while (q) {
    p.next = q.next;
    p = p.next;
    // 最后一个节点判断
    q.next = p ? p.next : null;
    q = q.next;
  }
  return newHead;
};
```

```ts
function copyRandomList(head: Node | null): Node | null {
  if (head === null) return null;

  // 复制每个节点，将复制的节点插入到当前节点后面
  let p = head;
  while (p !== null) {
    const node = new Node(p.val, p.next, p.random);
    p.next = node;
    p = node.next;
  }
  // 更新复制节点的random节点
  p = head.next;
  while (p !== null) {
    p.random = p.random ? p.random.next : null;
    p = p.next ? p.next.next : null;
  }

  // 将复制节点拆出来
  const ans = head.next;
  let q = head.next;
  p = head;
  while (q !== null) {
    p.next = q.next;
    p = p.next;
    q.next = p ? p.next : null;
    q = q.next;
  }

  return ans;
}
```
