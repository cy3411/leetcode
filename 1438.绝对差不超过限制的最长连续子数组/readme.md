# 题目

给你一个整数数组 nums ，和一个表示限制的整数 limit，请你返回最长连续子数组的长度，该子数组中的任意两个元素之间的绝对差必须小于或者等于 limit 。

如果不存在满足条件的子数组，则返回 0 。

提示：

- 1 <= nums.length <= 10^5
- 1 <= nums[i] <= 10^9
- 0 <= limit <= 10^9

# 示例

```
输入：nums = [8,2,4,7], limit = 4
输出：2
解释：所有子数组如下：
[8] 最大绝对差 |8-8| = 0 <= 4.
[8,2] 最大绝对差 |8-2| = 6 > 4.
[8,2,4] 最大绝对差 |8-2| = 6 > 4.
[8,2,4,7] 最大绝对差 |8-2| = 6 > 4.
[2] 最大绝对差 |2-2| = 0 <= 4.
[2,4] 最大绝对差 |2-4| = 2 <= 4.
[2,4,7] 最大绝对差 |2-7| = 5 > 4.
[4] 最大绝对差 |4-4| = 0 <= 4.
[4,7] 最大绝对差 |4-7| = 3 <= 4.
[7] 最大绝对差 |7-7| = 0 <= 4.
因此，满足题意的最长子数组的长度为 2 。
```

# 题解

## 二分查找

题目的意思就是在 [0,nums.length] 之间找一个长度，符合长度区间内最大值和最小值差值小于等于 limit。

我们这里可以通过二分取检索长度，然后通过判定函数来调整二分区间，直到找到答案。

判定函数的定义就是在窗口区间里的最大值和最小值的差值是否小于等于 limit。定义单调递增和单调递减队列，维护区间的最大值和最小值即可。

```ts
function longestSubarray(nums: number[], limit: number): number {
  // 检测长度为l字符串是否满足题意
  const m = nums.length;
  // 判定函数，维护窗口区间的最大值和最小值，然后判断差集是否大于等于limit
  const isCheck = (l: number): boolean => {
    // 单调递增
    const dequeAsc: number[] = [];
    // 单调递减
    const dequeDsc: number[] = [];
    for (let i = 0; i < m; i++) {
      while (dequeAsc.length && nums[i] < nums[dequeAsc[dequeAsc.length - 1]]) {
        dequeAsc.pop();
      }
      while (dequeDsc.length && nums[i] > nums[dequeDsc[dequeDsc.length - 1]]) {
        dequeDsc.pop();
      }
      dequeAsc.push(i);
      dequeDsc.push(i);
      if (i + 1 < l) continue;
      if (i - dequeAsc[0] === l) dequeAsc.shift();
      if (i - dequeDsc[0] === l) dequeDsc.shift();

      if (nums[dequeDsc[0]] - nums[dequeAsc[0]] <= limit) return true;
    }
    return false;
  };

  // 再长度[0,m]之间找最大符合题意的长度
  let l = 0;
  let r = m;
  while (l < r) {
    // 往后找，这里需要加1
    let mid = (r + l + 1) >> 1;

    if (isCheck(mid)) l = mid;
    else r = mid - 1;
  }

  return l;
}
```
