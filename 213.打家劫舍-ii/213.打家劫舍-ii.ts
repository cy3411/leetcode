/*
 * @lc app=leetcode.cn id=213 lang=typescript
 *
 * [213] 打家劫舍 II
 */

// @lc code=start
function rob(nums: number[]): number {
  // 偷最后的房子和偷第一个房子，分成2次动态规划，取最大结果
  const n = nums.length;
  if (n === 1) return nums[0];
  // dp[i][0] 不偷i的最大收益
  // dp[i][1] 偷i的最大收益
  const dp = new Array(n).fill(0).map((_) => new Array(2));
  // 初始化
  // 偷第一个房子
  dp[0][0] = 0;
  dp[0][1] = nums[0];
  for (let i = 1; i < n; i++) {
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
    dp[i][1] = dp[i - 1][0] + nums[i];
  }

  // 最后一个房子的最大收益
  const ans1 = dp[n - 1][0];

  // 不偷第一个房子
  dp[0][0] = 0;
  dp[0][1] = 0;
  for (let i = 1; i < n; i++) {
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
    dp[i][1] = dp[i - 1][0] + nums[i];
  }

  // 最后一个房子的最大收益
  const ans2 = Math.max(dp[n - 1][0], dp[n - 1][1]);

  return Math.max(ans1, ans2);
}
// @lc code=end
