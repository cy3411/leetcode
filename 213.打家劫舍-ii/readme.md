# 题目

你是一个专业的小偷，计划偷窃沿街的房屋，每间房内都藏有一定的现金。这个地方所有的房屋都 **围成一圈** ，这意味着第一个房屋和最后一个房屋是紧挨着的。同时，相邻的房屋装有相互连通的防盗系统，**如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警 。**

给定一个代表每个房屋存放金额的非负整数数组，计算你 **在不触动警报装置的情况下** ，能够偷窃到的最高金额。

提示：

- 1 <= nums.length <= 100
- 0 <= nums[i] <= 1000

# 示例

```
输入：nums = [2,3,2]
输出：3
解释：你不能先偷窃 1 号房屋（金额 = 2），然后偷窃 3 号房屋（金额 = 2）, 因为他们是相邻的。
```

```
输入：nums = [1,2,3,1]
输出：4
解释：你可以先偷窃 1 号房屋（金额 = 1），然后偷窃 3 号房屋（金额 = 3）。
     偷窃到的最高金额 = 1 + 3 = 4 。
```

# 题解

## 动态规划

这道题是[198. 打家劫舍](https://gitee.com/cy3411/leecode/tree/master/198.%E6%89%93%E5%AE%B6%E5%8A%AB%E8%88%8D)的扩展题，区别就是这道题中的房屋是首尾相连的，第一间房屋和最后一间房屋相邻，因此第一间房屋和最后一间房屋不能在同一晚上偷窃。

边界:

一间房子的时候，只能偷窃这一间屋子，结果最大。
二间房子的时候，由于两间屋子相连，不能同时偷取，只能取二者中的最大值。

$$
\begin{cases}
    dp[0]=nums[0],  & \text{只有一间房子} \\
    dp[1]=max(nums[0],nums[1]), & \text{只有两间房子} \\
\end{cases}
$$

状态：

能够偷窃到的最高金额。

选择：

$$
dp[i] = Math.max(dp[i-2] + nums[i], dp[i-1])
$$

假设数组长度为`n`，如果不偷窃第一间房子，范围就是`[1,n-1]`。如果不偷窃最后一间房子，范围就是`[0,n-2]`。

我们分别计算`[1,n-1]`和`[0,n-2]`中的最大值，就是结果。

考虑到每间房子的最高金额只跟前两间房子有关，这里使用滚动数组，只保留前两间房子的总金额。把空间复杂度降到`O(1)`。

```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const size = nums.length;

  if (size === 1) return nums[0];
  if (size === 2) return Math.max(nums[0], nums[1]);

  const robRange = (nums, start, end) => {
    // 只有一间房子
    let first = nums[start];
    // 只有两间房子
    let second = Math.max(nums[start], nums[start + 1]);

    for (let i = start + 2; i <= end; i++) {
      let t = second;
      second = Math.max(nums[i] + first, second);
      first = t;
    }

    return second;
  };

  return Math.max(robRange(nums, 0, size - 2), robRange(nums, 1, size - 1));
};
```

```ts
function rob(nums: number[]): number {
  // 偷最后的房子和偷第一个房子，分成2次动态规划，取最大结果
  const n = nums.length;
  if (n === 1) return nums[0];
  // dp[i][0] 不偷i的最大收益
  // dp[i][1] 偷i的最大收益
  const dp = new Array(n).fill(0).map((_) => new Array(2));
  // 初始化
  // 偷第一个房子
  dp[0][0] = 0;
  dp[0][1] = nums[0];
  for (let i = 1; i < n; i++) {
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
    dp[i][1] = dp[i - 1][0] + nums[i];
  }

  // 最后一个房子的最大收益
  const ans1 = dp[n - 1][0];

  // 不偷第一个房子
  dp[0][0] = 0;
  dp[0][1] = 0;
  for (let i = 1; i < n; i++) {
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
    dp[i][1] = dp[i - 1][0] + nums[i];
  }

  // 最后一个房子的最大收益
  const ans2 = Math.max(dp[n - 1][0], dp[n - 1][1]);

  return Math.max(ans1, ans2);
}
```
