/*
 * @lc app=leetcode.cn id=405 lang=typescript
 *
 * [405] 数字转换为十六进制数
 */

// @lc code=start
function toHex(num: number): string {
  if (num === 0) return '0';

  const base = 'a'.charCodeAt(0);
  let ans: string[] = [];
  // 32位有符号整数，4位为1组表示1个16进制，一共8组
  for (let i = 7; i >= 0; i--) {
    // 从高位开始转换
    const value = (num >> (4 * i)) & 0xf;
    // 避免前导零出现
    if (ans.length > 0 || value > 0) {
      // 将大于9的数值转换为16进制字符
      const digit = value < 10 ? String(value) : String.fromCharCode(base + value - 10);
      ans.push(digit);
    }
  }

  return ans.join('');
}
// @lc code=end
