# 题目

给定一个整数，编写一个算法将这个数转换为十六进制数。对于负整数，我们通常使用 补码运算 方法。

注意:

- 十六进制中所有字母`(a-f)`都必须是小写。
- 十六进制字符串中不能包含多余的前导零。如果要转化的数为`0`，那么以单个字符`'0'`来表示；对于其他情况，十六进制字符串中的第一个字符将不会是`0`字符。
- 给定的数确保在 `32` 位有符号整数范围内。
- 不能使用任何由库提供的将数字直接转换或格式化为十六进制的方法。

# 示例

```
输入:
26

输出:
"1a"
```

# 题解

## 位运算

在 32 位有序号整数中，最高位为 0 表示整数或者 0，为 1 表示负整数。

由于一位 16 进制对应四位二进制，因此 32 位有符号整数的 16 进制数有 8 位。

将 num 的二进制数分成 8 组，将每一组转换为一个 16 进制数，即可得到 num 的 16 进制数。

```ts
function toHex(num: number): string {
  if (num === 0) return '0';

  const base = 'a'.charCodeAt(0);
  let ans: string[] = [];
  // 32位有符号整数，4位为1组表示1个16进制，一共8组
  for (let i = 7; i >= 0; i--) {
    // 从高位开始转换
    const value = (num >> (4 * i)) & 0xf;
    // 避免前导零出现
    if (ans.length > 0 || value > 0) {
      // 将大于9的数值转换为16进制字符
      const digit = value < 10 ? String(value) : String.fromCharCode(base + value - 10);
      ans.push(digit);
    }
  }

  return ans.join('');
}
```
