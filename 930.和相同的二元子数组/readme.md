# 题目

给你一个二元数组 nums ，和一个整数 goal ，请你统计并返回有多少个和为 goal 的 非空 子数组。

子数组 是数组的一段连续部分。

提示：

- 1 <= nums.length <= 3 \* 104
- nums[i] 不是 0 就是 1
- 0 <= goal <= nums.length

# 示例

```
输入：nums = [1,0,1,0,1], goal = 2
输出：4
解释：
有 4 个满足题目要求的子数组：[1,0,1]、[1,0,1,0]、[0,1,0,1]、[1,0,1]
```

# 题解

## 哈希表+前缀和

子数组的和就是区间和问题，然后就想到前缀和。

我们假设 [i,j] 是区间和的范围，遍历数组，将 i 放入哈希，然后计算下一个前缀和，然后用 j 减去 goal 的值去哈希表中查找是否出现过，出现过就累加答案。

```ts
function numSubarraysWithSum(nums: number[], goal: number): number {
  const hash = new Map();
  let ans = 0;
  let sum = 0;

  // 计算区间和[i,j]，同步更新答案
  for (let num of nums) {
    // 将前缀和的i部分放入哈希
    hash.set(sum, (hash.get(sum) || 0) + 1);
    sum += num;
    // 每次用j减去goal的值，看看是否能找到i值
    ans += hash.get(sum - goal) || 0;
  }

  return ans;
}
```
