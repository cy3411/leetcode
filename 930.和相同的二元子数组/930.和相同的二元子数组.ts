/*
 * @lc app=leetcode.cn id=930 lang=typescript
 *
 * [930] 和相同的二元子数组
 */

// @lc code=start
function numSubarraysWithSum(nums: number[], goal: number): number {
  const hash = new Map();
  let ans = 0;
  let sum = 0;

  // 计算区间和[i,j]
  for (let num of nums) {
    // 将前缀和的i部分放入哈希
    hash.set(sum, (hash.get(sum) || 0) + 1);
    sum += num;
    // 每次用j减去goal的值，看看是否能找到i值
    ans += hash.get(sum - goal) || 0;
  }

  return ans;
}
// @lc code=end
