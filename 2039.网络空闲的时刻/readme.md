# 题目

给你一个有 `n` 个服务器的计算机网络，服务器编号为 `0` 到 `n - 1` 。同时给你一个二维整数数组 `edges` ，其中 $edges[i] = [u_i, v_i]$ 表示服务器 $u_i$ 和 $v_i$ 之间有一条信息线路，在 一秒 内它们之间可以传输 **任意** 数目的信息。再给你一个长度为 `n` 且下标从 `0` 开始的整数数组 `patience` 。

题目保证所有服务器都是 **相通** 的，也就是说一个信息从任意服务器出发，都可以通过这些信息线路直接或间接地到达任何其他服务器。

编号为 `0` 的服务器是 **主** 服务器，其他服务器为 **数据** 服务器。每个数据服务器都要向主服务器发送信息，并等待回复。信息在服务器之间按 **最优** 线路传输，也就是说每个信息都会以 **最少时间** 到达主服务器。主服务器会处理 **所有** 新到达的信息并 **立即** 按照每条信息来时的路线 **反方向** 发送回复信息。

在 `0` 秒的开始，所有数据服务器都会发送各自需要处理的信息。从第 `1` 秒开始，**每** 一秒最 **开始** 时，每个数据服务器都会检查它是否收到了主服务器的回复信息（包括新发出信息的回复信息）：

- 如果还没收到任何回复信息，那么该服务器会周期性 **重发** 信息。数据服务器 `i` 每 `patience[i]` 秒都会重发一条信息，也就是说，数据服务器 `i` 在上一次发送信息给主服务器后的 `patience[i]` 秒 **后** 会重发一条信息给主服务器。
- 否则，该数据服务器 **不会重发** 信息。

当没有任何信息在线路上传输或者到达某服务器时，该计算机网络变为 **空闲** 状态。

请返回计算机网络变为 **空闲** 状态的 **最早秒数** 。

提示：

- $n \equiv patience.length$
- $2 \leq n \leq 105$
- $patience[0] \equiv 0$
- 对于 $1 \leq i < n$ ，满足 $1 \leq patience[i] \leq 105$
- $1 \leq edges.length \leq min(10^5, n * (n - 1) / 2)$
- $edges[i].length \equiv 2$
- $0 \leq ui, vi < n$
- $ui \not = vi$
- 不会有重边。
- 每个服务器都直接或间接与别的服务器相连。

# 示例

[![qm5vJP.png](https://s1.ax1x.com/2022/03/21/qm5vJP.png)](https://imgtu.com/i/qm5vJP)

```
输入：edges = [[0,1],[1,2]], patience = [0,2,1]
输出：8
解释：
0 秒最开始时，
- 数据服务器 1 给主服务器发出信息（用 1A 表示）。
- 数据服务器 2 给主服务器发出信息（用 2A 表示）。

1 秒时，
- 信息 1A 到达主服务器，主服务器立刻处理信息 1A 并发出 1A 的回复信息。
- 数据服务器 1 还没收到任何回复。距离上次发出信息过去了 1 秒（1 < patience[1] = 2），所以不会重发信息。
- 数据服务器 2 还没收到任何回复。距离上次发出信息过去了 1 秒（1 \equiv patience[2] = 1），所以它重发一条信息（用 2B 表示）。

2 秒时，
- 回复信息 1A 到达服务器 1 ，服务器 1 不会再重发信息。
- 信息 2A 到达主服务器，主服务器立刻处理信息 2A 并发出 2A 的回复信息。
- 服务器 2 重发一条信息（用 2C 表示）。
...
4 秒时，
- 回复信息 2A 到达服务器 2 ，服务器 2 不会再重发信息。
...
7 秒时，回复信息 2D 到达服务器 2 。

从第 8 秒开始，不再有任何信息在服务器之间传输，也不再有信息到达服务器。
所以第 8 秒是网络变空闲的最早时刻。
```

# 题解

## 广度优先搜索

利用广度优先搜索求出结点 0 到其他结点的最短距离，然后依次每个结点变为空闲的时间，当所有结点都变为空闲的时候，就是整个网络的空闲状态。应此整个网络的最早空闲时间，就是所有结点中最晚的空闲时间。

假设结点 v 与结点 0 的最短距离是 dist，则此节点的空闲时间是收到主机回复信息后的下一秒。消息完成需要在最短距离上走一个来回，所以是 $2*dist$ 秒。具体每个结点的空闲时间需要分情况讨论：

- 若$2*dist \leq patience[v]$，此时结点还没发送第二个消息就收到了回复，所以空闲时间是 $2*dist + 1$ 秒。
- 若$2*dist > patience[v]$，此时节点在等待第一次小时的回复时，就会再次重复发送。那么在$[1, 2*dist)$时间内会最多再次发送$\lfloor\frac{2*dist-1}{patience[v]}\rfloor$次消息。最后一次发送消息的时间为$patience[v]*\lfloor\frac{2*dist-1}{patience[v]}\rfloor$，而结点 v 需要等待$2*dist_v$收到回复，所以 v 最后收到消息的时间为$patience[v]*\lfloor\frac{2*dist-1}{patience[v]}\rfloor + 2*dist_v$。则结点变为空闲的时间为：
  $$
  patience[v]*\lfloor\frac{2*dist-1}{patience[v]}\rfloor + (2*dist_v+1)
  $$

当$2*dist \leq patience[v]$， $\lfloor\frac{2*dist-1}{patience[v]}\rfloor = 0$，所以上面公式可以合并。

```ts
interface Status {
  idx: number;
  dist: number;
}

function networkBecomesIdle(edges: number[][], patience: number[]): number {
  const n = patience.length;
  const adj = new Array(n).fill(0).map(() => []);
  const visited = new Array(n).fill(0);
  // 构建邻接矩阵
  for (const v of edges) {
    adj[v[0]].push(v[1]);
    adj[v[1]].push(v[0]);
  }

  // 广度优先搜索最短路径
  const queue: Status[] = [];
  let ans = 0;
  // 初始化
  queue.push({ idx: 0, dist: 0 });
  visited[0] = 1;
  while (queue.length) {
    const curr = queue.shift()!;
    for (const next of adj[curr.idx]) {
      if (visited[next]) continue;
      const dist = curr.dist + 1;
      queue.push({ idx: next, dist });
      visited[next] = 1;
      const time = patience[next] * (((2 * dist - 1) / patience[next]) >> 0) + (2 * dist + 1);
      ans = Math.max(ans, time);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
    typedef pair<int, int> PII;

public:
    int networkBecomesIdle(vector<vector<int>> &edges, vector<int> &patience)
    {
        int n = patience.size();
        vector<vector<int>> adj(n);
        vector<int> visited(n, 0);
        for (auto &v : edges)
        {
            adj[v[0]].push_back(v[1]);
            adj[v[1]].push_back(v[0]);
        }

        queue<PII> que;
        que.emplace(PII(0, 0));
        visited[0] = 1;
        int ans = 0;
        while (!que.empty())
        {
            auto [idx, step] = que.front();
            que.pop();
            for (auto next : adj[idx])
            {
                if (visited[next])
                {
                    continue;
                }
                que.emplace(PII(next, step + 1));
                visited[next] = 1;
                int time = patience[next] * ((2 * (step + 1) - 1) / patience[next]) + (2 * (step + 1) + 1);
                ans = max(ans, time);
            }
        }

        return ans;
    }
};
```
