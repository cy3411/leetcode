# 题目

给你一个整数数组 `nums` ，返回 `nums[i] XOR nums[j]` 的最大运算结果，其中 `0 ≤ i ≤ j < n` 。

进阶：你可以在 `O(n)` 的时间解决这个问题吗？

提示：

- 1 <= nums.length <= 2 \* 104
- 0 <= nums[i] <= 231 - 1

# 示例

```
输入：nums = [3,10,5,25,2,8]
输出：28
解释：最大运算结果是 5 XOR 25 = 28.
```

```
输入：nums = [14,70,53,83,49,91,36,80,92,51,66,70]
输出：127
```

# 题解

## 暴力

遍历数组，每次更新最大结果值。

```ts
function findMaximumXOR(nums: number[]): number {
  const size = nums.length;
  if (size === 1) return 0;

  let result = 0;
  for (let i = 0; i < size; i++) {
    for (let j = i; j < size; j++) {
      result = Math.max(result, nums[i] ^ nums[j]);
    }
  }

  return result;
}
```

## 哈希表

由于数组中的每个元素都在$[0,2^{31}-1]$之间，我们可以将数字表示为长度为 31 的二进制数。

我们要求的结果是最大的，在枚举每一位的时候，我们判断是否可以取到 `1`。

我们先将 `nums` 中的二进制位存入哈希表中，枚举结果的每一位和 `num` 的二进制为异或。如果再哈希表中查到结果，表示可以凑成一对，结果的二进制为可以取到 `1`。

```ts
function findMaximumXOR(nums: number[]): number {
  const size = nums.length;
  if (size === 1) return 0;

  let result = 0;
  const bits = 30;
  for (let k = bits; k >= 0; k--) {
    const hash = new Set();
    for (let num of nums) {
      hash.add(num >> k);
    }
    // 假设下一位取到1
    let next = 2 * result + 1;
    let isFound = false;
    // 枚举，看看是否可以从哈希表中找到另一个
    for (let num of nums) {
      if (hash.has(next ^ (num >> k))) {
        isFound = true;
        break;
      }
    }

    if (isFound) {
      result = next;
    } else {
      result = next - 1;
    }
  }

  return result;
}
```

## Tire 树

将数组中的数字转换为二进制，然后构建 Tire 树。

遍历数组中的数字，枚举每个二进制位，如果可以取到 `1`，则更新结果。

最后取遍历结果中的最大值。

```ts
class TireNode {
  next: TireNode[];
  constructor() {
    this.next = new Array(2).fill(null);
  }
}

class Tire {
  root: TireNode;
  constructor() {
    this.root = new TireNode();
  }
  insert(num: number) {
    let cur = this.root;
    for (let i = 30; i >= 0; i--) {
      const bit = (num >> i) & 1;
      if (!cur.next[bit]) {
        cur.next[bit] = new TireNode();
      }
      cur = cur.next[bit];
    }
  }
  search(num: number): number {
    let cur = this.root;
    let res = 0;
    for (let i = 30; i >= 0; i--) {
      const bit = (num >> i) & 1;
      if (cur.next[+!bit]) {
        res |= 1 << i;
        cur = cur.next[+!bit];
      } else {
        cur = cur.next[bit];
      }
    }
    return res;
  }
}

function findMaximumXOR(nums: number[]): number {
  const tire = new Tire();
  // 构建 Tire
  for (const num of nums) {
    tire.insert(num);
  }
  let ans = 0;
  // 查找最大异或值
  for (const num of nums) {
    ans = Math.max(ans, tire.search(num));
  }
  return ans;
}
```
