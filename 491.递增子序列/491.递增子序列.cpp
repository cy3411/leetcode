/*
 * @lc app=leetcode.cn id=491 lang=cpp
 *
 * [491] 递增子序列
 */

// @lc code=start
class Solution
{
public:
    vector<vector<int>> ans;
    vector<int> temp;

    void dfs(int i, int n, vector<int> &nums)
    {
        if (i == n)
        {
            if (temp.size() >= 2)
            {
                ans.push_back(temp);
            }
            return;
        }
        // 选择当前数字
        if (temp.size() == 0 || nums[i] >= temp.back())
        {
            temp.push_back(nums[i]);
            dfs(i + 1, n, nums);
            temp.pop_back();
        }
        // 不选择当前数字
        if (temp.size() == 0 || nums[i] != temp.back())
        {
            dfs(i + 1, n, nums);
        }
        return;
    }

    vector<vector<int>> findSubsequences(vector<int> &nums)
    {
        int n = nums.size();
        dfs(0, n, nums);
        return ans;
    }
};
// @lc code=end
