# 题目

给定一个整型数组, 你的任务是找到所有该数组的递增子序列，递增子序列的长度至少是 `2` 。

提示：

- 给定数组的长度不会超过 15。
- 数组中的整数范围是 [-100,100]。
- 给定数组中可能包含重复数字，相等的数字应该被视为递增的一种情况。

# 示例

```
输入：[4, 6, 7, 7]
输出：[[4, 6], [4, 7], [4, 6, 7], [4, 6, 7, 7], [6, 7], [6, 7, 7], [7,7], [4,7,7]]
```

# 题解

## 回溯

枚举所有类型组合，典型的回溯问题。

这里唯一要做的就是答案的去重，可以使用哈希来存储出现过的组合。

如何组合哈希中没有，就添加到结果中。

```ts
function findSubsequences(nums: number[]): number[][] {
  const result: number[][] = [];
  const hash = new Set();
  const size = nums.length;

  const backtrack = (index: number, path: number[]): void => {
    if (path.length > 1) {
      const key = path.toString();
      if (!hash.has(key)) {
        result.push([...path]);
        hash.add(key);
      }
    }

    if (index === size) {
      return;
    }

    for (let i = index; i < size; i++) {
      if (path.length === 0 || path[path.length - 1] <= nums[i]) {
        // 选择
        path.push(nums[i]);
        // 递归
        backtrack(i + 1, path);
        // 回溯
        path.pop();
      }
    }
  };

  backtrack(0, []);

  return result;
}
```

```cpp
class Solution
{
public:
    vector<vector<int>> ans;
    vector<int> temp;

    void dfs(int i, int n, vector<int> &nums)
    {
        if (i == n)
        {
            if (temp.size() >= 2)
            {
                ans.push_back(temp);
            }
            return;
        }
        // 选择当前数字
        if (temp.size() == 0 || nums[i] >= temp.back())
        {
            temp.push_back(nums[i]);
            dfs(i + 1, n, nums);
            temp.pop_back();
        }
        // 不选择当前数字
        if (temp.size() == 0 || nums[i] != temp.back())
        {
            dfs(i + 1, n, nums);
        }
        return;
    }

    vector<vector<int>> findSubsequences(vector<int> &nums)
    {
        int n = nums.size();
        dfs(0, n, nums);
        return ans;
    }
};
```
