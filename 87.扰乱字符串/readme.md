# 题目
使用下面描述的算法可以扰乱字符串 `s` 得到字符串 `t` ：

1. 如果字符串的长度为 1 ，算法停止
2. 如果字符串的长度 > 1 ，执行下述步骤：
   + 在一个随机下标处将字符串分割成两个非空的子字符串。即，如果已知字符串 s ，则可以将其分成两个子字符串 `x` 和 `y` ，且满足 `s = x + y` 。
   + `随机` 决定是要「交换两个子字符串」还是要「保持这两个子字符串的顺序不变」。即，在执行这一步骤之后，s 可能是 `s = x + y` 或者 `s = y + x` 。
   + 在 `x` 和 `y` 这两个子字符串上继续从步骤 1 开始递归执行此算法。

给你两个 **长度相等** 的字符串 `s1` 和` s2`，判断 `s2` 是否是 `s1` 的扰乱字符串。如果是，返回 `true` ；否则，返回 `false` 

提示：

+ s1.length == s2.length
+ 1 <= s1.length <= 30
+ s1 和 s2 由小写英文字母组成

# 示例
```
输入：s1 = "great", s2 = "rgeat"
输出：true
解释：s1 上可能发生的一种情形是：
"great" --> "gr/eat" // 在一个随机下标处分割得到两个子字符串
"gr/eat" --> "gr/eat" // 随机决定：「保持这两个子字符串的顺序不变」
"gr/eat" --> "g/r / e/at" // 在子字符串上递归执行此算法。两个子字符串分别在随机下标处进行一轮分割
"g/r / e/at" --> "r/g / e/at" // 随机决定：第一组「交换两个子字符串」，第二组「保持这两个子字符串的顺序不变」
"r/g / e/at" --> "r/g / e/ a/t" // 继续递归执行此算法，将 "at" 分割得到 "a/t"
"r/g / e/ a/t" --> "r/g / e/ a/t" // 随机决定：「保持这两个子字符串的顺序不变」
算法终止，结果字符串和 s2 相同，都是 "rgeat"
这是一种能够扰乱 s1 得到 s2 的情形，可以认为 s2 是 s1 的扰乱字符串，返回 true
```

```
输入：s1 = "abcde", s2 = "caebd"
输出：false
```

# 题解
## 动态规划

**扰乱字符串**的关系具有对称性，`s1` 可以是 `s2`的扰乱字符串，`s2` 也可以是 `s1` 的扰乱字符串。

判断`s1`和`s2`是否是扰乱字符串，我们可以通过下面几点：
+ `s1 === s2 `
+ `s1.length === s2.length`
+ `s1`中出现的字符和字符的数量与`s2`中一样，只是顺序不同

剩下的其他情况就需要分割字符串来处理了，题目提到了随机分割，所以需要遍历每个位置，进行分割比较字串。

```js
function isScramble(s1: string, s2: string): boolean {
  /**
   * @description: 判断同长字符串中字符出现次数是否一样
   */
  const hashCode = (s: string): number => {
    let hash = 0;
    for (let c of s) {
      hash += 1 << c.charCodeAt(0);
    }
    return hash;
  };

  /**
   * @description: 记忆化搜索
   */
  const dp = (i: number, j: number, size: number): boolean => {
    if (memo[i][j][size] !== 0) {
      return memo[i][j][size] === 1;
    }

    let str1 = s1.slice(i, size + i);
    let str2 = s2.slice(j, size + j);
    // 两个字串是否相等
    if (str1 === str2) {
      memo[i][j][size] = 1;
      return true;
    }
    // 两个字串的字符是否相同
    // 比如：acdb和abcd是相同的
    if (hashCode(str1) !== hashCode(str2)) {
      memo[i][j][size] = -1;
      return false;
    }
    // 字符串同长并且里面的字符相同但位置不同，可以分割
    for (let k = 1; k < size; k++) {
      if (dp(i, j, k) && dp(i + k, j + k, size - k)) {
        memo[i][j][size] = 1;
        return true;
      }
      if (dp(i, j + size - k, k) && dp(i + k, j, size - k)) {
        memo[i][j][size] = 1;
        return true;
      }
    }

    memo[i][j][size] = -1;
    return false;
  };

  let size = s1.length;
  // 备忘录
  // i：分割s1的起始位置  j:分割s2的起始位置  size:分割的长度
  // memo[i][j][size]表示s1[i,size+i)和s2[j,size+j)字符串是否相同
  let memo = new Array(size)
    .fill(0)
    .map(() => new Array(size).fill(0).map(() => new Array(size + 1).fill(0)));

  return dp(0, 0, s1.length);
}
```