# 题目

给定两个由小写字母构成的字符串 A 和 B ，只要我们可以通过交换 A 中的两个字母得到与 B 相等的结果，就返回 true ；否则返回 false 。

交换字母的定义是取两个下标 i 和 j （下标从 0 开始），只要 i!=j 就交换 A[i] 和 A[j] 处的字符。例如，在 "abcd" 中交换下标 0 和下标 2 的元素可以生成 "cbad" 。

# 示例

```
输入： A = "ab", B = "ba"
输出： true
解释： 你可以交换 A[0] = 'a' 和 A[1] = 'b' 生成 "ba"，此时 A 和 B 相等。
```

```
输入： A = "ab", B = "ab"
输出： false
解释： 你只能交换 A[0] = 'a' 和 A[1] = 'b' 生成 "ba"，此时 A 和 B 不相等。
```

# 方法

由题目可知，最多只有 2 个不同的字符，遍历字符串，利用边界或相同字符分割出来 2 个不同的字符，然后对比。

```js
var buddyStrings = function (a, b) {
  const isRepeat = (str) => {
    const memo = new Map();
    for (let s of str) {
      memo.set(s, (memo.get(s) || 0) + 1);
      if (memo.get(s) > 1) return true;
    }
    return false;
  };
  // 长度不同
  if (a.length !== b.length) return false;
  // 字符串相同，判断是否有重复的
  if (a === b) return isRepeat(a);

  // 找到第一个不同的字符
  let i = 0;
  while (i < a.length && a[i] === b[i]) i++;
  // 找到第二个不同的字符
  let j = i + 1;
  while (j < a.length && a[j] === b[j]) j++;
  // 未找到第二个不同的字符
  if (j === a.length) return false;
  // 判断字符是否符合互换条件
  if (a[i] !== b[j] || a[j] !== b[i]) {
    return false;
  }

  // 继续找，看看是否还有不同的字符
  while (++j < a.length) {
    if (a[j] !== b[j]) return false;
  }

  return true;
};
```

```js
/**
 * @param {string} a
 * @param {string} b
 * @return {boolean}
 */
var buddyStrings = function (a, b) {
  if (a.length !== b.length) return false;
  // 字符相等，需要判断是否有重复字符
  if (a === b) return a !== [...new Set(a)].join('');

  let strA = '';
  let strB = '';
  let i = 0;
  while (i < a.length) {
    if (strA.length > 2 || strB.length > 2) return false;
    // 如果不相等，交换a中字符的位置
    if (a[i] !== b[i]) {
      strA = a[i] + strA;
      strB += b[i];
    }
    i++;
  }

  return strA === strB;
};
```

遍历字符串，如果遇到不同的字符，分别存入两个变量 a 和 b，a 中正序拼接字符，b 中倒序拼接字符，如果 a 和 b 相等，则返回 true，否则返回 false。

```ts
function buddyStrings(s: string, goal: string): boolean {
  const n = s.length;
  // 如果目标字符串长度不等于原字符串长度，则不是亲密字符串
  if (n !== goal.length) return false;
  // 如果相等，判断是否有重复字符
  if (s === goal) return s !== [...new Set(s)].join('');

  let a = '';
  let b = '';
  let i = 0;
  while (i < n) {
    // 超过两个相同字符，则不是亲密字符串
    if (a.length > 2 || b.length > 2) return false;
    // 遇到不相等的字符，a中正序放，b中逆序放
    if (s[i] !== goal[i]) {
      a += s[i];
      b = goal[i] + b;
    }
    i++;
  }

  return a === b;
}
```
