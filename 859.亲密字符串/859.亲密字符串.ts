/*
 * @lc app=leetcode.cn id=859 lang=typescript
 *
 * [859] 亲密字符串
 */

// @lc code=start
function buddyStrings(s: string, goal: string): boolean {
  const n = s.length;
  // 如果目标字符串长度不等于原字符串长度，则不是亲密字符串
  if (n !== goal.length) return false;
  // 如果相等，判断是否有重复字符
  if (s === goal) return s !== [...new Set(s)].join('');

  let a = '';
  let b = '';
  let i = 0;
  while (i < n) {
    // 超过两个相同字符，则不是亲密字符串
    if (a.length > 2 || b.length > 2) return false;
    // 遇到不相等的字符，a中正序放，b中逆序放
    if (s[i] !== goal[i]) {
      a += s[i];
      b = goal[i] + b;
    }
    i++;
  }

  return a === b;
}
// @lc code=end
