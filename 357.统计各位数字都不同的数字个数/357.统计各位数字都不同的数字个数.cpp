/*
 * @lc app=leetcode.cn id=357 lang=cpp
 *
 * [357] 统计各位数字都不同的数字个数
 */

// @lc code=start
class Solution
{
public:
    int countNumbersWithUniqueDigits(int n)
    {
        if (n == 0)
        {
            return 1;
        }
        if (n == 1)
        {
            return 10;
        }

        int ans = 10, base = 9;
        for (int i = 0; i < n - 1; i++)
        {
            base *= 9 - i;
            ans += base;
        }

        return ans;
    }
};
// @lc code=end
