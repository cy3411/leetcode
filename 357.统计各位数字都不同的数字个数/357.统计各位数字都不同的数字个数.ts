/*
 * @lc app=leetcode.cn id=357 lang=typescript
 *
 * [357] 统计各位数字都不同的数字个数
 */

// @lc code=start
function countNumbersWithUniqueDigits(n: number): number {
  // n 为 0 的情况，只有 0 一个数字
  let prev = 1;
  if (n === 0) return prev;
  // n 为 1 的情况，只有 0-9 10个数字
  let curr = 10;
  // 从 n = 2 开始遍历
  for (let i = 2; i <= n; i++) {
    let temp = curr;
    // (前 i 位统计结果) + (前 1 位统计结果 * 数字范围的组合)
    // 数字范围在 0 - 9 中，减去前面出现的位数
    curr = curr + (curr - prev) * (10 - (i - 1));
    // 更新前i位统计结果
    prev = temp;
  }
  return curr;
}
// @lc code=end
