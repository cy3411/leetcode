# 题目

有 n 个城市通过一些航班连接。给你一个数组 `flights` ，其中 `flights[i] = [fromi, toi, pricei]` ，表示该航班都从城市 `fromi` 开始，以价格 `pricei` 抵达 `toi。`

现在给定所有的城市和航班，以及出发城市 `src` 和目的地 `dst`，你的任务是找到出一条最多经过 `k` 站中转的路线，使得从 `src` 到 `dst` 的 **价格最便宜** ，并返回该价格。 如果不存在这样的路线，则输出 `-1`。

提示：

- `1 <= n <= 100`
- `0 <= flights.length <= (n * (n - 1) / 2)`
- `flights[i].length == 3`
- $0 \leq from_i, to_i < n$
- $from_i \not = to_i$
- `1 <= pricei <= 104`
- 航班没有重复，且不存在自环
- `0 <= src, dst, k < n`
- `src != dst`

# 示例

```
输入:
n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
src = 0, dst = 2, k = 1
输出: 200
```

解释:
城市航班图如下

[![hAX5J1.png](https://z3.ax1x.com/2021/08/25/hAX5J1.png)](https://imgtu.com/i/hAX5J1)

从城市 0 到城市 2 在 1 站中转以内的最便宜价格是 200，如图中红色所示。

# 题解

## 动态规划

**状态**

定义 `dp[t][to]`，表示通过 `t` 次航班，从出发城市 `src` 到 `to` 城市所需要的最小花费。

**BaseCase**

当`t=0`的时候，表示未搭乘任何航班的最小花费，那么：

$$
dp[t][to] = \begin{cases}
    0, & \text{i = src} \\
    \infty & \text{i $\neq$ src}
\end{cases}
$$

**转移**

$$
dp[t][to] = min(dp[t][to], dp[t-1][from] + cost(to,from))
$$

双重循环枚举 `k` 和 `to`，计算出 `k` 次航班时，`src` 到 `to` 的最短路径的消费，取中间的最小值就是答案。

我们注意到`dp[t][to]`的状态是由`dp[t-1][from]`转移而来，可是使用滚动数组，将二维 dp 转换为一维。

```ts
function findCheapestPrice(
  n: number,
  flights: number[][],
  src: number,
  dst: number,
  k: number
): number {
  const INF = Number.POSITIVE_INFINITY;
  let dp = new Array(n).fill(INF);
  // 原地不动，消耗0，路过的边数也是0
  dp[src] = 0;
  let ans = INF;
  // 遍历，经过t条边，各个路径的消耗最小值
  for (let t = 1; t <= k + 1; t++) {
    const temp = new Array(n).fill(INF);
    for (const [from, to, price] of flights) {
      temp[to] = Math.min(temp[to], dp[from] + price);
    }
    dp = temp;
    ans = Math.min(ans, dp[dst]);
  }
  return ans === INF ? -1 : ans;
}
```
