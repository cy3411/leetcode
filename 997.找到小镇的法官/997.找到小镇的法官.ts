/*
 * @lc app=leetcode.cn id=997 lang=typescript
 *
 * [997] 找到小镇的法官
 */

// @lc code=start
function findJudge(n: number, trust: number[][]): number {
  const trustedMap = new Map<number, number>();
  const trustedCount = new Map<number, number>();

  for (const [a, b] of trust) {
    trustedMap.set(a, 1);
    trustedCount.set(b, (trustedCount.get(b) || 0) + 1);
  }

  for (const [k, v] of trustedCount) {
    if (v === n - 1 && !trustedMap.has(k)) {
      return k;
    }
  }

  return trust.length === 0 && n === 1 ? 1 : -1;
}
// @lc code=end
