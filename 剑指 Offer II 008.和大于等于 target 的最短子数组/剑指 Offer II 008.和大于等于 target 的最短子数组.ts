// @algorithm @lc id=1000242 lang=typescript
// @title 2VG8Kg
// @test(7,[2,3,1,2,4,3])=2
function minSubArrayLen(target: number, nums: number[]): number {
  const n = nums.length;
  let l = 0;
  let sum = 0;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let r = 0; r < n; r++) {
    sum += nums[r];
    // 更新区间
    while (sum - nums[l] >= target) {
      sum -= nums[l];
      l++;
    }
    // 更新答案
    if (sum >= target) {
      ans = Math.min(ans, r - l + 1);
    }
  }
  return ans === Number.MAX_SAFE_INTEGER ? 0 : ans;
}
