# 题目

给定一个含有 `n` 个正整数的数组和一个正整数 `target` 。

找出该数组中满足其和 `≥ target` 的长度最小的 连续子数组 `[numsl, numsl+1, ..., numsr-1, numsr]` ，并返回其长度。如果不存在符合条件的子数组，返回 `0` 。

提示：

- $1 \leq target \leq 10^9$
- $1 \leq nums.length \leq 10^5$
- $1 \leq nums[i] \leq 10^5$

进阶：

如果你已经实现 `O(n)` 时间复杂度的解法, 请尝试设计一个 `O(n log(n))` 时间复杂度的解法

注意：[本题与主站 209 题相同](https://leetcode-cn.com/problems/minimum-size-subarray-sum/)

# 示例

```
输入：target = 7, nums = [2,3,1,2,4,3]
输出：2
解释：子数组 [4,3] 是该条件下的长度最小的子数组。
```

```
输入：target = 4, nums = [1,4,4]
输出：1
```

# 题解

## 双指针

遍历数组，利用双指针维护一个区间，当区间和大于 target 的时候，移动左指针，否则移动右指针。

遍历过程中，同时记录最小长度。

```ts
function minSubArrayLen(target: number, nums: number[]): number {
  const n = nums.length;
  let l = 0;
  let sum = 0;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let r = 0; r < n; r++) {
    sum += nums[r];
    // 更新区间
    while (sum - nums[l] >= target) {
      sum -= nums[l];
      l++;
    }
    // 更新答案
    if (sum >= target) {
      ans = Math.min(ans, r - l + 1);
    }
  }
  return ans === Number.MAX_SAFE_INTEGER ? 0 : ans;
}
```
