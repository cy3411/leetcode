# 题目

编写一种方法，对字符串数组进行排序，将所有变位词组合在一起。变位词是指字母相同，但排列不同的字符串。

说明：

- 所有输入均为小写字母。
- 不考虑答案输出的顺序。

# 示例

```
输入: ["eat", "tea", "tan", "ate", "nat", "bat"],
输出:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
```

# 题解

## 哈希表

题目给出只有小写字母，所以我们可以利用 26 位数组来统计每个字符串中字符出现的次数。

然后使用哈希表存储，数组相同的存为一组。

最后统计答案即可。

```js
var groupAnagrams = function (strs) {
  const base = 'a'.charCodeAt(0);
  const hash = new Map();

  //统计字符出现的次数
  const checkStr = (str) => {
    let arr = new Array(26).fill(0);
    for (let char of str) {
      let key = char.charCodeAt() - base;
      arr[key] += 1;
    }
    return arr.toString();
  };

  for (const str of strs) {
    let key = checkStr(str);
    console.log(key);
    if (hash.has(key)) {
      hash.get(key).push(str);
    } else {
      hash.set(key, [str]);
    }
  }

  const ans = [];
  for (let [_, value] of hash.entries()) {
    ans.push(value);
  }

  return ans;
};
```
