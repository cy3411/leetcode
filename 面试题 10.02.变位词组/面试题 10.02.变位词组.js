// @test(["hos","boo","nay","deb","wow","bop","bob","brr","hey","rye","eve","elf","pup","bum","iva","lyx","yap","ugh","hem","rod","aha","nam","gap","yea","doc","pen","job","dis","max","oho","jed","lye","ram","pup","qua","ugh","mir","nap","deb","hog","let","gym","bye","lon","aft","eel","sol","jab"])=[["eel"],["aft"],["lon"],["bye"],["gym"],["let"],["hog"],["mir"],["iva"],["brr"],["eve"],["nay"],["sol"],["pup","pup"],["max"],["bum"],["lye"],["gap"],["hey"],["boo"],["aha"],["elf"],["bob"],["hem"],["doc"],["oho"],["wow"],["deb","deb"],["hos"],["rye"],["bop"],["yap"],["ugh","ugh"],["ram"],["rod"],["nam"],["yea"],["nap"],["pen"],["job"],["lyx"],["dis"],["jed"],["jab"],["qua"]]
// @algorithm @lc id=1000040 lang=javascript
// @title group-anagrams-lcci
// @test(["eat","tea","tan","ate","nat","bat"])=[["ate","eat","tea"],["nat","tan"],["bat"]]
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function (strs) {
  const base = 'a'.charCodeAt(0);
  const hash = new Map();

  //统计字符出现的次数
  const checkStr = (str) => {
    let arr = new Array(26).fill(0);
    for (let char of str) {
      let key = char.charCodeAt() - base;
      arr[key] += 1;
    }
    return arr.toString();
  };

  for (const str of strs) {
    let key = checkStr(str);
    console.log(key);
    if (hash.has(key)) {
      hash.get(key).push(str);
    } else {
      hash.set(key, [str]);
    }
  }

  const ans = [];
  for (let [_, value] of hash.entries()) {
    ans.push(value);
  }

  return ans;
};
