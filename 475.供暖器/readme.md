# 题目

冬季已经来临。 你的任务是设计一个有固定加热半径的供暖器向所有房屋供暖。

在加热器的加热半径范围内的每个房屋都可以获得供暖。

现在，给出位于一条水平线上的房屋 `houses` 和供暖器 `heaters` 的位置，请你找出并返回可以覆盖所有房屋的最小加热半径。

说明：所有供暖器都遵循你的半径标准，加热的半径也一样。

# 示例

```
输入: houses = [1,2,3,4], heaters = [1,4]
输出: 1
解释: 在位置1, 4上有两个供暖器。我们需要将加热半径设为1，这样所有房屋就都能得到供暖。
```

# 题解

## 二分查找

我们要对任意一个房子进行供暖，要么用房子前的供暖器，要么用房子后的供暖器，取它们两个中的最小的距离。

如果要覆盖到所有的房子，我们需要直到每个房子的和供暖器的最小距离，最后取它们中的最大值就可以全覆盖了。

遍历 `houses`，对每个房子取 `heaters` 中找最小的距离，这里可以使用二分查找，找第一个大于等于 `house` 的值。

最后计算距离来更新答案。

```ts
function findRadius(houses: number[], heaters: number[]): number {
  // 这里返回的结果在[0,heaters.length]区间
  const binarySearch = (heaters: number[], target: number): number => {
    let l = 0;
    let r = heaters.length - 1;
    let mid: number;
    while (l < r) {
      mid = (r + l) >> 1;
      if (heaters[mid] >= target) {
        r = mid;
      } else {
        l = mid + 1;
      }
    }
    return l;
  };

  heaters.sort((a, b) => a - b);

  let ans = 0;
  for (const num of houses) {
    // 二分查找第一个大于等于当前房子值的供暖器的位置
    let index = binarySearch(heaters, num);
    if (index === 0) {
      // 获取供暖器和房子之间的距离
      ans = Math.max(ans, Math.abs(heaters[index] - num));
    } else if (index === heaters.length) {
      // 房子超出了最大供暖器的位置，所以只能取最后一个供暖器和当前房子计算距离
      ans = Math.max(ans, Math.abs(heaters[index - 1] - num));
    } else {
      // 房子和当前供暖器位置，房子和之前供暖器的位置取最小值，表示取2个供暖器的最小辐射范围
      ans = Math.max(
        ans,
        Math.min(Math.abs(heaters[index] - num), Math.abs(heaters[index - 1] - num))
      );
    }
  }

  return ans;
}
```
