/*
 * @lc app=leetcode.cn id=1450 lang=cpp
 *
 * [1450] 在既定时间做作业的学生人数
 */

// @lc code=start
class Solution {
public:
    int busyStudent(vector<int> &startTime, vector<int> &endTime,
                    int queryTime) {
        int n = startTime.size();
        int maxEndTime = *max_element(endTime.begin(), endTime.end());
        if (queryTime > maxEndTime) return 0;

        vector<int> count(maxEndTime + 2, 0);
        for (int i = 0; i < n; i++) {
            count[startTime[i]]++;
            count[endTime[i] + 1]--;
        }

        int ans = 0;
        for (int i = 0; i <= queryTime; i++) {
            ans += count[i];
        }

        return ans;
    }
};
// @lc code=end
