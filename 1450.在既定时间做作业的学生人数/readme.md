# 题目

给你两个整数数组 `startTime`（开始时间）和 `endTime`（结束时间），并指定一个整数 `queryTime` 作为查询时间。

已知，第 `i` 名学生在 `startTime[i]` 时开始写作业并于 `endTime[i]` 时完成作业。

请返回在查询时间 `queryTime` 时正在做作业的学生人数。形式上，返回能够使 `queryTime` 处于区间 `[startTime[i], endTime[i]]`（含）的学生人数。

提示：

- $startTime.length \equiv endTime.length$
- $1 \leq startTime.length \leq 100$
- $1 \leq startTime[i] \leq endTime[i] \leq 1000$
- $1 \leq queryTime \leq 1000$

# 示例

```
输入：startTime = [1,2,3], endTime = [3,2,7], queryTime = 4
输出：1
解释：一共有 3 名学生。
第一名学生在时间 1 开始写作业，并于时间 3 完成作业，在时间 4 没有处于做作业的状态。
第二名学生在时间 2 开始写作业，并于时间 2 完成作业，在时间 4 没有处于做作业的状态。
第三名学生在时间 3 开始写作业，预计于时间 7 完成作业，这是是唯一一名在时间 4 时正在做作业的学生。
```

```
输入：startTime = [4], endTime = [4], queryTime = 4
输出：1
解释：在查询时间只有一名学生在做作业。
```

# 题解

## 枚举

枚举每个学生学习的开始时间和结束时间，判断查询时间是否在这个区间内。

```js
function busyStudent(startTime: number[], endTime: number[], queryTime: number): number {
  const n = startTime.length;
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (startTime[i] <= queryTime && endTime[i] >= queryTime) {
      ans++;
    }
  }
  return ans;
}
```

## 差分数组

利用差分数组的特性，我们初始化差分数组 count， 在每个学生开始的时间 count[startTime[i]] 加 1，在每个学生结束的时间 count[endTime[i] + 1] 减 1。

最后对差分数组求前缀和即可。

```cpp
class Solution {
public:
    int busyStudent(vector<int> &startTime, vector<int> &endTime,
                    int queryTime) {
        int n = startTime.size();
        int maxEndTime = *max_element(endTime.begin(), endTime.end());
        if (queryTime > maxEndTime) return 0;

        vector<int> count(maxEndTime + 2, 0);
        for (int i = 0; i < n; i++) {
            count[startTime[i]]++;
            count[endTime[i] + 1]--;
        }

        int ans = 0;
        for (int i = 0; i <= queryTime; i++) {
            ans += count[i];
        }

        return ans;
    }
};
```
