/*
 * @lc app=leetcode.cn id=1450 lang=typescript
 *
 * [1450] 在既定时间做作业的学生人数
 */

// @lc code=start
function busyStudent(startTime: number[], endTime: number[], queryTime: number): number {
  const n = startTime.length;
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (startTime[i] <= queryTime && endTime[i] >= queryTime) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
