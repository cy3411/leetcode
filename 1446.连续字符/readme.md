# 题目

给你一个字符串 s ，字符串的「能量」定义为：只包含一种字符的最长非空子字符串的长度。

请你返回字符串的能量。

提示：

- $1 \leq s.length \leq 500$
- `s` 只包含小写英文字母。

# 示例

```
输入：s = "leetcode"
输出：2
解释：子字符串 "ee" 长度为 2 ，只包含字符 'e' 。
```

```
输入：s = "abbcccddddeeeeedcba"
输出：5
解释：子字符串 "eeeee" 长度为 5 ，只包含字符 'e' 。
```

# 题解

## 遍历

遍历字符串，连续相同的话就累加，不相同的话就更新最大值。

```ts
function maxPower(s: string): number {
  let ans = 0;
  let max = 1;

  // 遍历字符串
  for (let i = 0; i < s.length; i++) {
    // 如果当前字符与上一个字符相同，则max++
    if (s[i] === s[i + 1]) {
      max++;
    } else {
      max = 1;
    }
    // 如果当前最大值大于结果，则替换
    ans = Math.max(ans, max);
  }

  return ans;
}
```

## 动态规划

**状态定义**

`dp[i]` 表示以 `s[i]` 结尾的最长连续子串的长度

**转移方程**

$dp[i] = dp[i - 1] + 1, (s[i] === s[i - 1])$

**初始化**

初始化 `dp[i]=1` ,每个字符都是一个连续子串

```ts
function maxPower(s: string): number {
  const n = s.length;
  // dp[i]表示以s[i]结尾的最长连续子串的长度
  const dp = new Array(n).fill(1);

  for (let i = 1; i < n; i++) {
    // 如果当前字符和前一个字符相同，则继续累加
    if (s[i] === s[i - 1]) {
      dp[i] = dp[i - 1] + 1;
    }
  }

  // 找出最大值
  return Math.max(...dp);
}
```
