/*
 * @lc app=leetcode.cn id=1446 lang=typescript
 *
 * [1446] 连续字符
 */

// @lc code=start
function maxPower(s: string): number {
  const n = s.length;
  // dp[i]表示以s[i]结尾的最长连续子串的长度
  const dp = new Array(n).fill(1);

  for (let i = 1; i < n; i++) {
    // 如果当前字符和前一个字符相同，则继续累加
    if (s[i] === s[i - 1]) {
      dp[i] = dp[i - 1] + 1;
    }
  }

  // 找出最大值
  return Math.max(...dp);
}
// @lc code=end
