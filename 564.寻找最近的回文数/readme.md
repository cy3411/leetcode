# 题目

给定一个表示整数的字符串 n ，返回与它最近的回文整数（不包括自身）。如果不止一个，返回较小的那个。

“最近的”定义为两个整数差的绝对值最小。

提示:

- 1 <= n.length <= 18
- n 只由数字组成
- n 不含前导 0
- n 代表在 [1, 10^{18} - 1] 范围内的整数

# 示例

```
输入: n = "123"
输出: "121"
```

```
输入: n = "1"
输出: "0"
解释: 0 和 2是最近的回文，但我们返回最小的，也就是 0。
```

# 题解

## 模拟

构建回文数，比较直观的办法就是将较高位的数字替换较低位的数字。

比如： '12345'，'1'替换'5'，'2'替换'4'，结果就是 '12321'。

我们替换的较低位的数字，因此构造出来的结果与原数字都较为接近。

- 如果大于原数字，可以将中间的数字减少 1
- 如果小于原数字，可以将中间的数字增加 1

如果中间的数字是 1 或者 9 的话，构造的时候要考虑位数的改变，我们可以提前将位数改变的回文数构造出来放入备选结果。

最后将备选结果中，最接近原数字且不等于原数字的就是答案。

```ts
function nearestPalindromic(n: string): string {
  // 计算所有可能的回文数
  const getCandidates = (n: string): number[] => {
    const len = n.length;
    const candidates = new Array<number>();
    // 初始化，将涉及到进退位的最近的回文数先加入
    candidates.push(10 ** (len - 1) - 1);
    candidates.push(10 ** len + 1);

    let selfPrefix = Number(n.substring(0, ((len + 1) / 2) >> 0));
    const selfPrefixs = [selfPrefix - 1, selfPrefix, selfPrefix + 1];
    for (const x of selfPrefixs) {
      const prefix = x.toString();
      const candidate =
        prefix +
        prefix
          .split('')
          .reverse()
          .join('')
          .substring(len % 2);
      candidates.push(Number(candidate));
    }
    return candidates;
  };

  let selfNumber = Number(n);
  let ans = -1;
  const candidates: number[] = getCandidates(n);
  for (const x of candidates) {
    if (x === selfNumber) continue;
    if (ans === -1) ans = x;
    if (Math.abs(x - selfNumber) < Math.abs(ans - selfNumber)) ans = x;
    if (Math.abs(x - selfNumber) === Math.abs(ans - selfNumber)) {
      ans = Math.min(ans, x);
    }
  }

  return String(ans);
}
```

```cpp
class Solution
{
public:
    vector<long> getCandidates(const string &n)
    {
        int len = n.length();
        vector<long> candidates = {
            (long)pow(10, len - 1) - 1,
            (long)pow(10, len) + 1,
        };
        long selfPrefix = stol(n.substr(0, (len + 1) / 2));
        for (int i : {selfPrefix - 1, selfPrefix, selfPrefix + 1})
        {
            string prefix = to_string(i);
            string candidate = prefix + string(prefix.rbegin() + (len & 1), prefix.rend());
            candidates.push_back(stol(candidate));
        }
        return candidates;
    }

    string nearestPalindromic(string n)
    {
        long selfNumber = stol(n), ans = -1;
        const vector<long> &candidates = getCandidates(n);
        for (auto &candidate : candidates)
        {
            if (candidate != selfNumber)
            {
                if (ans == -1 ||
                    abs(candidate - selfNumber) < abs(ans - selfNumber) ||
                    abs(candidate - selfNumber) == abs(ans - selfNumber) && candidate < ans)
                {
                    ans = candidate;
                }
            }
        }
        return to_string(ans);
    }
};
```
