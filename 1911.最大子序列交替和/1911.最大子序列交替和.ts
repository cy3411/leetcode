/*
 * @lc app=leetcode.cn id=1911 lang=typescript
 *
 * [1911] 最大子序列交替和
 */

// @lc code=start
function maxAlternatingSum(nums: number[]): number {
  const n = nums.length;
  // 前一个最大的相减结果
  let subMax = Number.MIN_SAFE_INTEGER;
  // 前一个最大的相加结果
  let addMax = nums[0];
  let ans = nums[0];
  let a: number, b: number;
  for (let i = 1; i < n; i++) {
    // 下一个addMax
    a = Math.max(subMax + nums[i], nums[i]);
    // 下一个subMax
    b = addMax - nums[i];
    ans = Math.max(ans, a, b);
    subMax = Math.max(subMax, b);
    addMax = Math.max(addMax, a);
  }
  return ans;
}
// @lc code=end
