/*
 * @lc app=leetcode.cn id=1911 lang=cpp
 *
 * [1911] 最大子序列交替和
 */

// @lc code=start
class Solution
{
public:
    long long maxAlternatingSum(vector<int> &nums)
    {
        int n = nums.size();
        long long subMax = INT_MIN, addMax = nums[0], ans = nums[0];
        long long a, b;
        for (int i = 1; i < n; i++)
        {
            a = max(subMax + nums[i], (long long)nums[i]);
            b = addMax - nums[i];
            ans = max(ans, max(a, b));
            subMax = max(subMax, b);
            addMax = max(addMax, a);
        }
        return ans;
    }
};
// @lc code=end
