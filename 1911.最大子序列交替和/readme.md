# 题目

一个下标从 `0` 开始的数组的 **交替和** 定义为 **偶数** 下标处元素之 **和** 减去 **奇数** 下标处元素之 **和** 。

比方说，数组 `[4,2,5,3]` 的交替和为 `(4 + 5) - (2 + 3) = 4` 。
给你一个数组 `nums` ，请你返回 `nums` 中任意子序列的 **最大交替和** （子序列的下标 **重新** 从 0 开始编号）。

一个数组的 **子序列** 是从原数组中删除一些元素后（也可能一个也不删除）剩余元素不改变顺序组成的数组。比方说，`[2,7,4]` 是 `[4,2,3,7,2,1,4]` 的一个子序列（加粗元素），但是 `[2,4,2]` 不是。

提示：

- $1 \leq nums.length \leq 10^5$
- $1 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [4,2,5,3]
输出：7
解释：最优子序列为 [4,2,5] ，交替和为 (4 + 5) - 2 = 7 。
```

```
输入：nums = [5,6,7,8]
输出：8
解释：最优子序列为 [8] ，交替和为 8 。
```

```
输入：nums = [6,2,1,2,4,5]
输出：10
解释：最优子序列为 [6,1,5] ，交替和为 (6 + 5) - 1 = 10 。
```

# 题解

## 递推

假设当前是$a_i$元素，如果要求当前位置的最大交替和，那么有两种情况：

- 前一个相减的最大和记作$subMax$，则有$subMax + a_i$
- 前一个相加的最大和记作$addMax$，则有$addMax - a_i$

遍历 nums ，每次更新当前位置的最大交替和，即为上一个位置的最大交替和加上当前位置的值，或者为上一个位置的最大交替和减去当前位置的值。同时更新最大答案即可。

```ts
function maxAlternatingSum(nums: number[]): number {
  const n = nums.length;
  // 前一个最大的相减结果
  let subMax = Number.MIN_SAFE_INTEGER;
  // 前一个最大的相加结果
  let addMax = nums[0];
  let ans = nums[0];
  let a: number, b: number;
  for (let i = 1; i < n; i++) {
    // 下一个addMax
    a = Math.max(subMax + nums[i], nums[i]);
    // 下一个subMax
    b = addMax - nums[i];
    ans = Math.max(ans, a, b);
    subMax = Math.max(subMax, b);
    addMax = Math.max(addMax, a);
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    long long maxAlternatingSum(vector<int> &nums)
    {
        int n = nums.size();
        long long subMax = INT_MIN, addMax = nums[0], ans = nums[0];
        long long a, b;
        for (int i = 1; i < n; i++)
        {
            a = max(subMax + nums[i], (long long)nums[i]);
            b = addMax - nums[i];
            ans = max(ans, max(a, b));
            subMax = max(subMax, b);
            addMax = max(addMax, a);
        }
        return ans;
    }
};
```
