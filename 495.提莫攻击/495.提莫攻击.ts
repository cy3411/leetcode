/*
 * @lc app=leetcode.cn id=495 lang=typescript
 *
 * [495] 提莫攻击
 */

// @lc code=start
function findPoisonedDuration(timeSeries: number[], duration: number): number {
  const n = timeSeries.length;
  let ans = 0;
  // 中毒时间
  let expire = 0;
  for (let i = 0; i < n; i++) {
    if (timeSeries[i] >= expire) {
      // 没有中毒
      ans += duration;
    } else {
      // 中毒
      ans += timeSeries[i] + duration - expire;
    }
    // 更新中毒时间
    expire = timeSeries[i] + duration;
  }

  return ans;
}
// @lc code=end
