# 题目

给定不同面额的硬币和一个总金额。写出函数来计算可以凑成总金额的硬币组合数。假设每一种面额的硬币有**无限个**。

# 示例

```
输入: amount = 5, coins = [1, 2, 5]
输出: 4
解释: 有四种方式可以凑成总金额:
5=5
5=2+2+1
5=2+1+1+1
5=1+1+1+1+1
```

# 题解

## 动态规划

**状态**
定义 `dp[i][j]`表示前 `i` 个硬币凑出价值为 `j` 的方案数量

**basecase**
dp[0][0] = 1，0 个硬币凑出价值为 0 的方案数量为 1

**选择**
对于每一个硬币可以选择的条件是，`coins[i] <= j`

$$
dp[i][j] = \begin{cases}
    dp[i-1][j] & \text{coins[i-1] > j,不选择} \\
    dp[i-1][j] + dp[i][j-coines[i-1]] & \text{coins[i-1] <= j},选择
\end{cases}
$$

```ts
function change(amount: number, coins: number[]): number {
  const n = coins.length;
  // dp[i][j]表示i个硬币凑出价值为j的方案个数
  const dp = new Array(n + 1).fill(0).map(() => new Array(amount + 1).fill(0));
  // 0个硬币凑出价值为0的方案数为1
  dp[0][0] = 1;

  for (let i = 1; i <= n; i++) {
    const coin = coins[i - 1];
    for (let j = 0; j <= amount; j++) {
      dp[i][j] = dp[i - 1][j];
      if (j >= coin) {
        dp[i][j] += dp[i][j - coin];
      }
    }
  }
  return dp[n][amount];
}
```

对于每个面额为 `coin` 的硬币, 当 `coin<= j <=amount`，如果 `dp[i-coin]`存在组合，该硬币组合增加一个面额为 `coin` 的硬币，即可得到面额为 `i` 的组合数量。

```ts
function change(amount: number, coins: number[]): number {
  const n = coins.length;

  // dp[i]表示金额之和等于i的方案数量
  const dp = new Array(amount + 1).fill(0);
  // 金额之和位0的方案数就是不选择任何金币这一种方案
  dp[0] = 1;

  for (const coin of coins) {
    for (let i = coin; i <= amount; i++) {
      dp[i] += dp[i - coin];
    }
  }

  return dp[amount];
}
```
