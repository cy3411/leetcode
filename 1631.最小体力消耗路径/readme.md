# 题目

你准备参加一场远足活动。给你一个二维 `rows x columns` 的地图 `heights` ，其中 `heights[row][col]` 表示格子 `(row, col)` 的高度。一开始你在最左上角的格子 `(0, 0)` ，且你希望去最右下角的格子 `(rows-1, columns-1)` （注意下标从 `0` 开始编号）。你每次可以往 **上，下，左，右** 四个方向之一移动，你想要找到耗费 **体力** 最小的一条路径。

一条路径耗费的 **体力值** 是路径上相邻格子之间 **高度差绝对值** 的 **最大值** 决定的。

请你返回从左上角走到右下角的最小 **体力消耗值** 。

提示：

- $rows \equiv heights.length$
- $columns \equiv heights[i].length$
- $1 \leq rows, columns \leq 100$
- $1 \leq heights[i][j] \leq 10^6$

# 示例

[![bc444s.png](https://s1.ax1x.com/2022/03/08/bc444s.png)](https://imgtu.com/i/bc444s)

```
输入：heights = [[1,2,2],[3,8,2],[5,3,5]]
输出：2
解释：路径 [1,3,5,3,5] 连续格子的差值绝对值最大为 2 。
这条路径比路径 [1,2,2,2,5] 更优，因为另一条路径差值最大值为 3 。
```

[![bc4Tg0.png](https://s1.ax1x.com/2022/03/08/bc4Tg0.png)](https://imgtu.com/i/bc4Tg0)

```
输入：heights = [[1,2,3],[3,8,4],[5,3,5]]
输出：1
解释：路径 [1,2,3,4,5] 的相邻格子差值绝对值最大为 1 ，比路径 [1,3,5,3,5] 更优。
```

# 题解

## 二分查找+广度优先搜索

目的可达性的问题，可以使用广度优先搜索来解决。

题意是要找到目的可达且体力消耗最小。假设当前消耗 `x` 体力，目的可达，那么消耗比 `x` 更多的体力，目的也是可达的。

那么我们可以通过二分，来确定这个 'x'，如果当前 `x` 可达，收缩右边区间，如果不可达，收缩左边区间。直到找到最小的可达的体力消耗。

```ts
interface Cell {
  x: number;
  y: number;
}

function minimumEffortPath(heights: number[][]): number {
  const m = heights.length;
  const n = heights[0].length;
  const dirs = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
  ];
  // 当前的消耗是否可达终点
  const bfs = (cost: number): boolean => {
    const mark = new Array(m).fill(0).map(() => new Array(n).fill(0));
    const queue: Cell[] = [];
    queue.push({ x: 0, y: 0 });
    mark[0][0] = 1;
    while (queue.length) {
      const curr = queue.shift();
      if (curr.x === m - 1 && curr.y === n - 1) {
        return true;
      }
      for (const [x, y] of dirs) {
        const nx = curr.x + x;
        const ny = curr.y + y;
        if (nx < 0 || nx >= m || ny < 0 || ny >= n || mark[nx][ny] === 1) {
          continue;
        }
        if (Math.abs(heights[curr.x][curr.y] - heights[nx][ny]) <= cost) {
          queue.push({ x: nx, y: ny });
          mark[nx][ny] = 1;
        }
      }
    }
    return false;
  };
  // 二分查找最小消耗
  let l = 0;
  let r = 10 ** 6;
  while (l < r) {
    let mid = l + ((r - l) >> 1);
    if (bfs(mid)) {
      r = mid;
    } else {
      l = mid + 1;
    }
  }
  return l;
}
```

```cpp
class Solution
{
public:
    struct Cell
    {
        int x, y;
    };
    int m, n;
    int dirs[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    int bfs(int cost, vector<vector<int>> &heights)
    {
        vector<vector<int>> mark(m, vector<int>(n, 0));
        queue<Cell> q;
        q.push({0, 0});
        mark[0][0] = 1;
        while (!q.empty())
        {
            Cell curr = q.front();
            q.pop();
            if (curr.x == m - 1 && curr.y == n - 1)
            {
                return 1;
            }
            for (auto [dx, dy] : dirs)
            {
                int nx = curr.x + dx;
                int ny = curr.y + dy;
                if (nx < 0 || nx >= m || ny < 0 || ny >= n || mark[nx][ny] == 1)
                {
                    continue;
                }
                if (abs(heights[curr.x][curr.y] - heights[nx][ny]) <= cost)
                {
                    mark[nx][ny] = 1;
                    q.push({nx, ny});
                }
            }
        }

        return 0;
    }

    int minimumEffortPath(vector<vector<int>> &heights)
    {
        m = heights.size();
        n = heights[0].size();
        int l = 0, r = 10e6, mid;
        while (l < r)
        {
            mid = l + ((r - l) >> 1);
            if (bfs(mid, heights))
            {
                r = mid;
            }
            else
            {
                l = mid + 1;
            }
        }
        return l;
    }
};
```
