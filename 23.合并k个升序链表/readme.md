# 题目
给你一个链表数组，每个链表都已经按升序排列。

请你将所有链表合并到一个升序链表中，返回合并后的链表。

提示：
+ k == lists.length
+ 0 <= k <= 10^4
+ 0 <= lists[i].length <= 500
+ -10^4 <= lists[i][j] <= 10^4
+ lists[i] 按 升序 排列
+ lists[i].length 的总和不超过 10^4

# 示例
```
输入：lists = [[1,4,5],[1,3,4],[2,6]]
输出：[1,1,2,3,4,4,5,6]
解释：链表数组如下：
[
  1->4->5,
  1->3->4,
  2->6
]
将它们合并到一个有序链表中得到。
1->1->2->3->4->4->5->6
```

# 题解
## 多路归并
题目给出是多组**有序**链表合并，这明显就是归并排序中多路合并的问题。

定义小顶堆，将多路数据放入，每次取最小的拿出来组成链表，再把取出的链表后的节点放入堆中。

这样重复操作，直到堆为空。

```ts
class MinPQ<T extends ListNode> {
  pq: Array<T>;
  constructor() {
    this.pq = [];
  }
  less(i: number, j: number): boolean {
    return this.pq[i].val < this.pq[j].val;
  }
  swap(i: number, j: number): void {
    [this.pq[i], this.pq[j]] = [this.pq[j], this.pq[i]];
  }
  swin(k: number): void {
    let parentIdx = (k - 1) >> 1;
    while (parentIdx >= 0 && this.less(k, parentIdx)) {
      this.swap(k, parentIdx);
      k = parentIdx;
      parentIdx = (k - 1) >> 1;
    }
  }
  size(): number {
    return this.pq.length;
  }
  sink(k: number): void {
    let childIdx = k * 2 + 1;
    while (childIdx < this.size()) {
      if (childIdx + 1 < this.size() && this.less(childIdx + 1, childIdx)) {
        childIdx++;
      }
      if (this.less(k, childIdx)) break;
      this.swap(k, childIdx);
      k = childIdx;
      childIdx = k * 2 + 1;
    }
  }
  top(): T {
    return this.pq[0];
  }
  push(k: T): void {
    this.pq.push(k);
    this.swin(this.size() - 1);
  }
  pop(): T {
    if (this.size() === 0) return;
    const min = this.pq[0];
    this.swap(0, this.size() - 1);
    this.pq.pop();
    this.sink(0);
    return min;
  }
}

function mergeKLists(lists: Array<ListNode | null>): ListNode | null {
  // 小顶堆
  // 多路归并
  const minPQ = new MinPQ<ListNode>();
  const dump = new ListNode();
  let p = dump;
  for (const node of lists) {
    if (node === null) continue;
    minPQ.push(node);
  }
  while (minPQ.size()) {
    const min = minPQ.pop();
    p.next = min;
    p = p.next;
    if (min.next) minPQ.push(min.next);
  }

  return dump.next;
}
```