# 题目
给你一个二叉搜索树的根节点 `root` ，返回 **树中任意两不同节点值之间的最小差值** 。

提示：

+ 树中节点数目在范围 `[2, 100]` 内
+ `0 <= Node.val <= 105`

# 示例
![crh6w6.png](https://z3.ax1x.com/2021/04/13/crh6w6.png)
```
输入：root = [4,2,6,1,3]
输出：1
```

# 题解
二叉搜索树有个性质，中序遍历二叉搜索树，得到的结果是**递增有序**的。

在有序数组中求2个元素的最小差值，肯定是相邻元素之差才是最小的。

$$
ans = \min_{m=0}^{n-2}{a[i+1]−a[i]}
$$

```js
/**
 * @param {TreeNode} root
 * @return {number}
 */
var minDiffInBST = function (root) {
  // 递归中序遍历
  const helper = (root) => {
    if (root === null) return;
    helper(root.left);
    if (preVal === void 0) {
      preVal = root.val;
    } else {
      result = Math.min(result, root.val - preVal);
      preVal = root.val;
    }
    helper(root.right);
  };

  let result = Number.POSITIVE_INFINITY;
  // 前驱节点
  let preVal = void 0;
  helper(root);
  return result;
};
```


```js
/**
 * @param {TreeNode} root
 * @return {number}
 */
var minDiffInBST = function (root) {
  let result = Number.POSITIVE_INFINITY;
  let curr = root;
  let pre = void 0;
  const stack = [];
  // 迭代中序遍历
  while (stack.length || curr) {
    while (curr !== null) {
      stack.push(curr);
      curr = curr.left;
    }
    curr = stack.pop();
    if (pre === void 0) {
      pre = curr.val;
    } else {
      result = Math.min(result, curr.val - pre);
      pre = curr.val;
    }
    curr = curr.right;
  }

  return result;
};
```