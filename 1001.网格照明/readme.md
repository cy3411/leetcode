# 题目

在大小为 `n x n` 的网格 `grid` 上，每个单元格都有一盏灯，最初灯都处于 **关闭** 状态。

给你一个由灯的位置组成的二维数组 `lamps` ，其中 $\color{burlywood} lamps[i] = [row_i, col_i]$ 表示 **打开** 位于 $\color{burlywood} grid[row_i][col_i]$ 的灯。即便同一盏灯可能在 `lamps` 中多次列出，不会影响这盏灯处于 **打开** 状态。

当一盏灯处于打开状态，它将会照亮 **自身所在单元格** 以及同一 行 、同一 列 和两条 **对角线** 上的 所有其他单元格 。

另给你一个二维数组 `queries` ，其中 $\color{burlywood} queries[j] = [row_j, col_j]$ 。对于第 `j` 个查询，如果单元格 $\color{burlywood} [row_j, col_j]$ 是被照亮的，则查询结果为 `1` ，否则为 `0` 。在第 `j` 次查询之后 [按照查询的顺序] ，**关闭** 位于单元格 $\color{burlywood} grid[row_j][col_j]$ 上及相邻 8 个方向上（与单元格 $\color{burlywood} grid[row_i][col_i]$ 共享角或边）的任何灯。

返回一个整数数组 `ans` 作为答案， `ans[j]` 应等于第 `j` 次查询 `queries[j]` 的结果，`1` 表示照亮，`0` 表示未照亮。

提示：

- $\color{burlywood} 1 \leq n \leq 109$
- $\color{burlywood} 0 \leq lamps.length \leq 20000$
- $\color{burlywood} 0 \leq queries.length \leq 20000$
- $\color{burlywood} lamps[i].length == 2$
- $\color{burlywood} 0 \leq rowi, coli < n$
- $\color{burlywood} queries[j].length == 2$
- $\color{burlywood} 0 \leq rowj, colj < n$

# 示例

[![H1lyAx.png](https://s4.ax1x.com/2022/02/08/H1lyAx.png)](https://imgtu.com/i/H1lyAx)

```
输入：n = 5, lamps = [[0,0],[4,4]], queries = [[1,1],[1,0]]
输出：[1,0]
解释：最初所有灯都是关闭的。在执行查询之前，打开位于 [0, 0] 和 [4, 4] 的灯。第 0 次查询检查 grid[1][1] 是否被照亮（蓝色方框）。该单元格被照亮，所以 ans[0] = 1 。然后，关闭红色方框中的所有灯。

```

[![H1lR3D.png](https://s4.ax1x.com/2022/02/08/H1lR3D.png)](https://imgtu.com/i/H1lR3D)

```
第 1 次查询检查 grid[1][0] 是否被照亮（蓝色方框）。该单元格没有被照亮，所以 ans[1] = 0 。然后，关闭红色矩形中的所有灯。
```

[![H1lOgg.png](https://s4.ax1x.com/2022/02/08/H1lOgg.png)](https://imgtu.com/i/H1lOgg)

# 题解

## 哈希表

假设一栈灯的坐标为$(x,y)$，那坐标对应的点亮规则为：

- $x$ 为行的数值
- $y$ 为列的数值
- $x-y$ 为对角线的数值
- $x+y$ 为反对角线的数值

遍历 `lamps` , 将当前灯的坐标存入哈希表中，并将对应的行、列和对角线的值存入哈希表中，`key` 为上面描述的坐标对应的数值， `value` 为 可以点亮该位置灯的数量。

遍历 `queries`,判断查询点所在的行、列和对角线是否有灯，如果有灯，则结果为 `1`，否则为 `0`。

然后遍历当前点的 `8` 个方向(包括自己)，如果有灯，将该点的行、列和对角线灯的数量减 `1`，并将当前灯从哈希表中删除。

```ts
function gridIllumination(n: number, lamps: number[][], queries: number[][]): number[] {
  // 行、列，对角线的点亮数量
  const rowMap = new Map<number, number>();
  const colMap = new Map<number, number>();
  const diagnoal = new Map<number, number>();
  const antiDiagnoal = new Map<number, number>();
  // 点亮的灯的位置
  const lampsPos = new Set<string>();

  // 初始化
  const m = lamps.length;
  for (let i = 0; i < m; i++) {
    const [x, y] = lamps[i];
    const key = `${x}-${y}`;
    // 相同位置的灯去重
    if (lampsPos.has(key)) continue;
    // 灯的位置
    lampsPos.add(key);
    // 灯光覆盖的位置
    rowMap.set(x, (rowMap.get(x) || 0) + 1);
    colMap.set(y, (colMap.get(y) || 0) + 1);
    diagnoal.set(x - y, (diagnoal.get(x - y) || 0) + 1);
    antiDiagnoal.set(x + y, (antiDiagnoal.get(x + y) || 0) + 1);
  }

  const ans = new Array(queries.length).fill(0);
  // 遍历queries，查询结果
  for (let i = 0; i < queries.length; i++) {
    const [row, col] = queries[i];
    // 判断行、列、对角线、反对角线是否有灯光
    if (
      rowMap.get(row) ||
      colMap.get(col) ||
      diagnoal.get(row - col) ||
      antiDiagnoal.get(row + col)
    ) {
      // 在灯光内，该位置为1
      ans[i] = 1;
    }
    // 遍历周围8个方向是否有灯，有的话关闭
    for (let x = row - 1; x <= row + 1; x++) {
      for (let y = col - 1; y <= col + 1; y++) {
        const key = `${x}-${y}`;
        // 坐标超出范围
        if (x < 0 || x >= n || y < 0 || y >= n) continue;
        // 当前坐标不是灯的位置
        if (!lampsPos.has(key)) continue;
        // 关闭灯
        lampsPos.delete(key);
        // 关闭灯光，相应位置的灯光数量减1
        rowMap.set(x, rowMap.get(x) - 1);
        colMap.set(y, colMap.get(y) - 1);
        diagnoal.set(x - y, diagnoal.get(x - y) - 1);
        antiDiagnoal.set(x + y, antiDiagnoal.get(x + y) - 1);
      }
    }
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> gridIllumination(int n, vector<vector<int>> &lamps, vector<vector<int>> &queries)
    {
        unordered_map<int, int> row, col, diagnoal, anti_diagnoal;
        auto hash_p = [](const pair<int, int> &p) -> size_t
        {
            static hash<long long> h;
            return h(p.first + (static_cast<long long>(p.second) << 32));
        };
        unordered_set<pair<int, int>, decltype(hash_p)> lampsPointer(0, hash_p);

        for (auto &lamp : lamps)
        {
            if (lampsPointer.count({lamp[0], lamp[1]}) > 0)
            {
                continue;
            }
            lampsPointer.insert({lamp[0], lamp[1]});
            row[lamp[0]]++;
            col[lamp[1]]++;
            diagnoal[lamp[0] - lamp[1]]++;
            anti_diagnoal[lamp[0] + lamp[1]]++;
        }

        int m = queries.size();
        vector<int> ans(m, 0);
        for (int i = 0; i < m; i++)
        {
            int r = queries[i][0], c = queries[i][1];
            if (row.count(r) > 0 && row[r] > 0)
            {
                ans[i] = 1;
            }
            else if (col.count(c) > 0 && col[c] > 0)
            {
                ans[i] = 1;
            }
            else if (diagnoal.count(r - c) > 0 && diagnoal[r - c] > 0)
            {
                ans[i] = 1;
            }
            else if (anti_diagnoal.count(r + c) > 0 && anti_diagnoal[r + c] > 0)
            {
                ans[i] = 1;
            }
            for (int x = r - 1; x <= r + 1; x++)
            {
                for (int y = c - 1; y <= c + 1; y++)
                {
                    if (x < 0 || y < 0 || x >= n || y >= n)
                    {
                        continue;
                    }

                    auto p = lampsPointer.find({x, y});
                    if (p != lampsPointer.end())
                    {
                        lampsPointer.erase(p);
                        row[x]--;
                        col[y]--;
                        diagnoal[x - y]--;
                        anti_diagnoal[x + y]--;
                    }
                }
            }
        }
        return ans;
    }
};
```
