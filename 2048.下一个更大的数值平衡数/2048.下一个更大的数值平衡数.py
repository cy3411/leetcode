#
# @lc app=leetcode.cn id=2048 lang=python3
#
# [2048] 下一个更大的数值平衡数
#

# @lc code=start
class Solution:
    def checkNum(self, n: int) -> bool:
        dict = collections.defaultdict(int)
        # 计算每个数字出现的次数
        for x in str(n):
            dict[int(x)] += 1

        # 检查数字是否跟出现次数相同
        for k, v in dict.items():
            if (k != v):
                return False

        return True

    def nextBeautifulNumber(self, n: int) -> int:
        ans = n + 1
        # 如果不合法，检查下一个数字
        while not self.checkNum(ans):
            ans += 1
        return ans

# @lc code=end
