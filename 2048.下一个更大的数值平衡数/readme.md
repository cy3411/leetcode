# 题目

如果整数 `x` 满足：对于每个数位 `d` ，这个数位 **恰好** 在 `x` 中出现 `d` 次。那么整数 `x` 就是一个 **数值平衡数** 。

给你一个整数 `n` ，请你返回 严格大于 `n` 的 最小数值平衡数 。

提示：

- $0 \leq n \leq 10^6$

# 示例

```
输入：n = 1
输出：22
解释：
22 是一个数值平衡数，因为：
- 数字 2 出现 2 次
这也是严格大于 1 的最小数值平衡数。
```

```
输入：n = 1000
输出：1333
解释：
1333 是一个数值平衡数，因为：
- 数字 1 出现 1 次。
- 数字 3 出现 3 次。
这也是严格大于 1000 的最小数值平衡数。
注意，1022 不能作为本输入的答案，因为数字 0 的出现次数超过了 0 。
```

# 题解

## 递归

最接近的 `n` 的数值平衡数，肯定是跟 `n` 的位数一样或者比 `n` 大一位的数值平衡数。

假设 `n` 的位数为 `d`，我们可以把 `d` 位 和 `d+1` 位的所有的平衡数都放到一个数组中，然后遍历数组，找出最小的大于 `n` 的平衡数。

```ts
function nextBeautifulNumber(n: number): number {
  if (n === 0) return 1;
  // 得到当前数字的位数
  const digit = Math.floor(Math.log10(n)) + 1;
  // 存储所有平衡数
  const arr: number[] = [];

  // 将位数拆分成合适的数字
  // 比如5位数拆成[1,4],[2,3]等
  const getBlanceNumber = (digit: number, idx: number, buff: number[] = []) => {
    if (digit === 0) {
      const temp = [];
      for (const x of buff) {
        for (let k = 0; k < x; k++) {
          temp.push(x);
        }
      }
      getALLPermutation(0, temp);
      return;
    }
    for (let i = idx; i <= digit; i++) {
      if (i === digit || digit - i > i) {
        buff.push(i);
        getBlanceNumber(digit - i, i + 1, buff);
        buff.pop();
      }
    }
  };
  // 平衡数的全排列
  const getALLPermutation = (i: number, buff: number[]) => {
    if (i === buff.length) {
      arr.push(Number(buff.join('')));
    }
    for (let j = i; j < buff.length; j++) {
      [buff[i], buff[j]] = [buff[j], buff[i]];
      getALLPermutation(i + 1, buff);
      [buff[i], buff[j]] = [buff[j], buff[i]];
    }
  };

  getBlanceNumber(digit, 1);
  getBlanceNumber(digit + 1, 1);

  let ans = Number.MAX_SAFE_INTEGER;
  for (const x of arr) {
    if (x > n) {
      ans = Math.min(ans, x);
    }
  }

  return ans;
}
```

```cpp
class Solution {
  public:
  void getNumber(int d, int idx, vector<int> &buff, vector<int> &arr) {
    if (d == 0) {
      vector<int> temp;
      for (auto x : buff) {
        for (int i = 0; i < x; i++) {
          temp.push_back(x);
        }
      }
      do {
        int num = 0;
        for (auto x : temp) {
          num = num * 10 + x;
        }
        arr.push_back(num);
      } while (next_permutation(temp.begin(), temp.end()));
    }

    for (int i = idx; i <= d; i++) {
      if (i == d || d - i > i) {
        buff.push_back(i);
        getNumber(d - i, i + 1, buff, arr);
        buff.pop_back();
      }
    }
  }

  void getAllNumber(int d, vector<int> &arr) {
    vector<int> buff;
    getNumber(d, 1, buff, arr);
    return;
  }

  int nextBeautifulNumber(int n) {
    if (n == 0) return 1;
    int d = floor(log10(n)) + 1;
    vector<int> arr;

    getAllNumber(d, arr);
    getAllNumber(d + 1, arr);

    int ans = INT_MAX;
    for (auto x : arr) {
      if (x > n) ans = min(ans, x);
    }
    return ans;
  }
};
```

## 枚举

从 n + 1 开始，枚举每个数字，如果满足平衡数的条件，就返回答案

```py
class Solution:
    def checkNum(self, n: int) -> bool:
        dict = collections.defaultdict(int)
        # 计算每个数字出现的次数
        for x in str(n):
            dict[int(x)] += 1

        # 检查数字是否跟出现次数相同
        for k, v in dict.items():
            if (k != v):
                return False

        return True

    def nextBeautifulNumber(self, n: int) -> int:
        ans = n + 1
        # 如果不合法，检查下一个数字
        while not self.checkNum(ans):
            ans += 1
        return ans
```
