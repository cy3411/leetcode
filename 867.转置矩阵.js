/*
 * @lc app=leetcode.cn id=867 lang=javascript
 *
 * [867] 转置矩阵
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
var transpose = function (matrix) {
  const m = matrix.length;
  const n = matrix[0].length;
  const result = new Array(n).fill(0).map(() => new Array(m).fill(0));

  for (let i = 0; i < m * n; i++) {
    result[i % n][Math.floor(i / n)] = matrix[Math.floor(i / n)][i % n];
  }

  return result;
};
// @lc code=end
