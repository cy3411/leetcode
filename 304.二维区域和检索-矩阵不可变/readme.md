## 题目描述

给定一个二维矩阵，计算其子矩形范围内元素的总和，该子矩阵的左上角为 (row1, col1) ，右下角为 (row2, col2)。

## 示例
```
给定 matrix = [
  [3, 0, 1, 4, 2],
  [5, 6, 3, 2, 1],
  [1, 2, 0, 1, 5],
  [4, 1, 0, 1, 7],
  [1, 0, 3, 0, 5]
]

sumRegion(2, 1, 4, 3) -> 8
sumRegion(1, 1, 2, 2) -> 11
sumRegion(1, 2, 2, 4) -> 12
```

## 一维前缀和

初始化时对矩阵的每一行计算前缀和，检索时对二维区域中的每一行计算子数组和，然后对每一行的子数组和计算总和。

创建m行n+1列的二维前缀和数组prefixSum，m和n分别对应matrix的行和列。

prefixSum[i]分别对应matrix[i]的前缀和数组。

```javascript
  const m = matrix.length;
  const n = matrix[0]?.length || 0;
  this.prefixSum = new Array(m).fill(0).map((_) => new Array(n + 1).fill(0));
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      this.prefixSum[i][j + 1] = this.prefixSum[i][j] + matrix[i][j];
    }
  }
```


## 二维前缀和
假设m和n分别对应matrix的行和列。当$0<=i<m且0<=j<n,f(i,j)为matrix[i][j]$为右下角的的子矩阵元素之和。

> $f(i,j)=f(i−1,j)+f(i,j−1)−f(i−1,j−1)+matrix[i][j]$


```javascript
  const m = matrix.length;
  const n = matrix[0]?.length || 0;
  // 二维前缀和
  this.prefixSum = new Array(m + 1).fill(0).map((_) => new Array(n + 1).fill(0));
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      this.prefixSum[i + 1][j + 1] =
        this.prefixSum[i][j + 1] + this.prefixSum[i + 1][j] - this.prefixSum[i][j] + matrix[i][j];
    }
  }
```

