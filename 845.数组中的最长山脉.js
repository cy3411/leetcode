/*
 * @lc app=leetcode.cn id=845 lang=javascript
 *
 * [845] 数组中的最长山脉
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {number}
 */
var longestMountain = function (A) {
  const size = A.length;
  let result = 0,
    i = 0;

  while (i < size) {
    let uphill = 0,
      downhill = 0;

    while (i < size - 1 && A[i] < A[i + 1]) {
      i++;
      uphill++;
    }

    if (uphill > 0) {
      while (i < size - 1 && A[i] > A[i + 1]) {
        i++;
        downhill++;
      }
    }

    if (uphill > 0 && downhill > 0) {
      result = Math.max(result, uphill + downhill + 1);
    }

    if (uphill === 0) {
      i++;
    }
  }

  return result;
};
// @lc code=end

/**
 * 时间复杂度：O(n)，其中 n 是数组 A 的长度
 * 空间复杂度：O(1)
 */
