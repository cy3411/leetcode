# 题目

给定一个整数数组 `nums` 和一个整数目标值 `target`，请你在该数组中找出 **和为目标值** `target` 的那 `两个` 整数，并返回它们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

你可以按任意顺序返回答案。

提示：

- 2 <= nums.length <= 104
- -109 <= nums[i] <= 109
- -109 <= target <= 109
- **只会存在一个有效答案**

进阶：你可以想出一个时间复杂度小于 $O(n^2)$ 的算法吗？

# 示例

```
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
```

# 题解

## 哈希

遍历 `nums` 数组，判断哈希表中是否存有 `target-num` 的值，如果没有就把 `target-num` 存入哈希表中。

如果有，直接返回答案即可。

```ts
function twoSum(nums: number[], target: number): number[] {
  let hash: Map<number, number> = new Map();

  const result = [];

  for (let i = 0; i < nums.length; i++) {
    const num = nums[i];
    if (hash.has(target - num)) {
      result.push(hash.get(target - num), i);
      return result;
    } else {
      hash.set(num, i);
    }
  }

  return result;
}
```

## 二分查找

我们可以将 `nums` 做索引排序，然后遍历索引数组，每次去当前元素后面的范围查找 `target-nums[idxs[i]]` 的位置，有的话直接返回结果。

```ts
function twoSum(nums: number[], target: number): number[] {
  const size = nums.length;
  const idxs = new Array(size).fill(0).map((_, i) => i);

  // 索引排序
  idxs.sort((a, b) => nums[a] - nums[b]);

  const binarySearch = (nums: number[], idxs: number[], start: number, target: number): number => {
    let head = start;
    let tail = idxs.length - 1;
    let mid: number;
    while (head <= tail) {
      mid = head + ((tail - head) >> 1);
      if (nums[idxs[mid]] === target) return mid;
      if (nums[idxs[mid]] < target) head = mid + 1;
      else tail = mid - 1;
    }
    return -1;
  };

  const result = new Array(2);
  for (let i = 0; i < idxs.length - 1; i++) {
    const num = nums[idxs[i]];
    const j = binarySearch(nums, idxs, i + 1, target - num);
    if (j !== -1) {
      result[0] = idxs[i];
      result[1] = idxs[j];
      return result;
    }
  }

  return result;
}
```
