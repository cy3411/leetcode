// @algorithm @lc id=1000276 lang=typescript
// @title lwyVBB
// @test(["hello","hello"],"abcdefghijklmnopqrstuvwxyz")  result: false ,expect: true
function isAlienSorted(words: string[], order: string): boolean {
  const compare = (a: string, b: string): boolean => {
    const n = Math.min(a.length, b.length);
    for (let i = 0; i < n; i++) {
      if (map.get(a[i]) > map.get(b[i])) return false;
      if (map.get(a[i]) < map.get(b[i])) return true;
    }
    return a.length <= b.length;
  };
  // 对外星语词典映射
  const map = new Map<string, number>();
  for (let i = 0; i < order.length; i++) {
    map.set(order[i], i);
  }

  // 比较每个单词
  for (let i = 0; i < words.length - 1; i++) {
    // 如果有一个错误顺序，直接返回
    if (!compare(words[i], words[i + 1])) {
      return false;
    }
  }

  return true;
}
