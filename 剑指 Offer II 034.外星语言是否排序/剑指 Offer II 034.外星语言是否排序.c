int compare(char *a, char *b, int *map)
{
  int a_len = strlen(a), b_len = strlen(b), n = fmin(a_len, b_len);
  for (int i = 0; i < n; i++)
  {
    if (map[a[i] - 'a'] < map[b[i] - 'a'])
      return 1;
    if (map[a[i] - 'a'] > map[b[i] - 'a'])
      return 0;
  }
  return a_len <= b_len;
}

bool isAlienSorted(char **words, int wordsSize, char *order)
{
  int map[26] = {0}, i = 0;
  for (i = 0; i < 26; i++)
  {
    map[order[i] - 'a'] = i;
  }
  for (i = 0; i < wordsSize - 1; i++)
  {
    if (!compare(words[i], words[i + 1], map))
    {
      return false;
    }
  }
  return true;
}