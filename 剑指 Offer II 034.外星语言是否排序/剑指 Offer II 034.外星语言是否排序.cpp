class Solution
{
  unordered_map<char, char> m;

public:
  int compare(string &a, string &b)
  {
    int n = min(a.size(), b.size());
    for (int i = 0; i < n; i++)
    {
      if (m[a[i]] < m[b[i]])
        return true;
      if (m[a[i]] > m[b[i]])
        return false;
    }
    return a.size() <= b.size();
  }
  bool isAlienSorted(vector<string> &words, string order)
  {
    for (int i = 0; i < 26; i++)
    {
      m[order[i]] = i + 'a';
    }

    for (int i = 0; i < words.size() - 1; i++)
    {
      if (!compare(words[i], words[i + 1]))
      {
        return false;
      }
    }

    return true;
  }
};