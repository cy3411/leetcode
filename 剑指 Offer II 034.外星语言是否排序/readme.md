# 题目

某种外星语也使用英文小写字母，但可能顺序 `order` 不同。字母表的顺序（`order`）是一些小写字母的排列。

给定一组用外星语书写的单词 `words`，以及其字母表的顺序 `order`，只有当给定的单词在这种外星语中按字典序排列时，返回 `true`；否则，返回 `false`。

提示：

- $1 <= words.length <= 100$
- $1 <= words[i].length <= 20$
- $order.length == 26$
- 在 `words[i]` 和 `order` 中的所有字符都是英文小写字母

注意：本题与主站 [953 题](https://leetcode-cn.com/problems/verifying-an-alien-dictionary/)相同。

# 示例

```
输入：words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
输出：true
解释：在该语言的字母表中，'h' 位于 'l' 之前，所以单词序列是按字典序排列的。
```

```
输入：words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
输出：false
解释：在该语言的字母表中，'d' 位于 'l' 之后，那么 words[0] > words[1]，因此单词序列不是按字典序排列的。
```

# 题解

## 哈希表

使用哈希表重新排列字符顺序，然后按照新的字符顺序比较单词大小即可。

```ts
function isAlienSorted(words: string[], order: string): boolean {
  const compare = (a: string, b: string): boolean => {
    const n = Math.min(a.length, b.length);
    for (let i = 0; i < n; i++) {
      if (map.get(a[i]) > map.get(b[i])) return false;
      if (map.get(a[i]) < map.get(b[i])) return true;
    }
    return a.length <= b.length;
  };
  // 对外星语词典映射
  const map = new Map<string, number>();
  for (let i = 0; i < order.length; i++) {
    map.set(order[i], i);
  }

  // 比较每个单词
  for (let i = 0; i < words.length - 1; i++) {
    // 如果有一个错误顺序，直接返回
    if (!compare(words[i], words[i + 1])) {
      return false;
    }
  }

  return true;
}
```

```cpp
int compare(char *a, char *b, int *map)
{
  int a_len = strlen(a), b_len = strlen(b), n = fmin(a_len, b_len);
  for (int i = 0; i < n; i++)
  {
    if (map[a[i] - 'a'] < map[b[i] - 'a'])
      return 1;
    if (map[a[i] - 'a'] > map[b[i] - 'a'])
      return 0;
  }
  return a_len <= b_len;
}

bool isAlienSorted(char **words, int wordsSize, char *order)
{
  int map[26] = {0}, i = 0;
  for (i = 0; i < 26; i++)
  {
    map[order[i] - 'a'] = i;
  }
  for (i = 0; i < wordsSize - 1; i++)
  {
    if (!compare(words[i], words[i + 1], map))
    {
      return false;
    }
  }
  return true;
}
```

```cpp
class Solution
{
  unordered_map<char, char> m;

public:
  int compare(string &a, string &b)
  {
    int n = min(a.size(), b.size());
    for (int i = 0; i < n; i++)
    {
      if (m[a[i]] < m[b[i]])
        return true;
      if (m[a[i]] > m[b[i]])
        return false;
    }
    return a.size() <= b.size();
  }
  bool isAlienSorted(vector<string> &words, string order)
  {
    for (int i = 0; i < 26; i++)
    {
      m[order[i]] = i + 'a';
    }

    for (int i = 0; i < words.size() - 1; i++)
    {
      if (!compare(words[i], words[i + 1]))
      {
        return false;
      }
    }

    return true;
  }
};
```
