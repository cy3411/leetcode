/*
 * @lc app=leetcode.cn id=905 lang=typescript
 *
 * [905] 按奇偶排序数组
 */

// @lc code=start
function sortArrayByParity(nums: number[]): number[] {
  const n = nums.length;
  let i = 0;
  let j = n - 1;

  while (i < j) {
    while (nums[i] % 2 === 0 && i < j) {
      i++;
    }
    while (nums[j] % 2 === 1 && i < j) {
      j--;
    }
    if (i < j) {
      [nums[i], nums[j]] = [nums[j], nums[i]];
    }
    i++, j--;
  }

  return nums;
}
// @lc code=end
