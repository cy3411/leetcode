# 题目

给你一个整数数组 `nums`，将 `nums` 中的的所有偶数元素移动到数组的前面，后跟所有奇数元素。

返回满足此条件的 **任一数组** 作为答案。

提示：

- $1 \leq nums.length \leq 5000$
- $0 \leq nums[i] \leq 5000$

# 示例

```
输入：nums = [3,1,2,4]
输出：[2,4,3,1]
解释：[4,2,3,1]、[2,4,1,3] 和 [4,2,1,3] 也会被视作正确答案。
```

# 题解

## 双指针

设置头尾双指针，同时遍历头尾指针，如果头指针遇到奇数，尾指针遇到偶数，则交换，直到两个指针相遇。

```ts
function sortArrayByParity(nums: number[]): number[] {
  const n = nums.length;
  let i = 0;
  let j = n - 1;

  while (i < j) {
    while (nums[i] % 2 === 0 && i < j) {
      i++;
    }
    while (nums[j] % 2 === 1 && i < j) {
      j--;
    }
    if (i < j) {
      [nums[i], nums[j]] = [nums[j], nums[i]];
    }
    i++, j--;
  }

  return nums;
}
```

```cpp
class Solution
{
public:
    vector<int> sortArrayByParity(vector<int> &nums)
    {
        int i = 0, j = nums.size() - 1;
        while (i < j)
        {
            while (i < j && nums[i] % 2 == 0)
            {
                i++;
            }
            while (i < j && nums[j] % 2 == 1)
            {
                j--;
            }
            if (i < j)
            {
                swap(nums[i], nums[j]);
            }
            i++, j--;
        }
        return nums;
    }
};
```
