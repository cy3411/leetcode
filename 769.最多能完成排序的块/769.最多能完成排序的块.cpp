/*
 * @lc app=leetcode.cn id=769 lang=cpp
 *
 * [769] 最多能完成排序的块
 */

// @lc code=start
class Solution {
public:
    int maxChunksToSorted(vector<int> &arr) {
        int n = arr.size();
        int m = 0, ans = 0;
        for (int i = 0; i < n; i++) {
            m = max(m, arr[i]);
            if (m == i) ans++;
        }
        return ans;
    }
};
// @lc code=end
