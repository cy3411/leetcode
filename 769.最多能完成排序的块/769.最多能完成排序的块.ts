/*
 * @lc app=leetcode.cn id=769 lang=typescript
 *
 * [769] 最多能完成排序的块
 */

// @lc code=start
function maxChunksToSorted(arr: number[]): number {
  const n = arr.length;
  let ans = 0;
  // 每个分区的最大值
  let max = 0;
  for (let i = 0; i < n; i++) {
    max = Math.max(max, arr[i]);
    if (max === i) ans++;
  }
  return ans;
}
// @lc code=end
