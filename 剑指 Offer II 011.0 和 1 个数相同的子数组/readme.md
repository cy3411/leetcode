# 题目

给定一个二进制数组 `nums` , 找到含有相同数量的 `0` 和 `1` 的最长连续子数组，并返回该子数组的长度。

提示：

- $1 \leq nums.length \leq 10^5$
- `nums[i]` 不是 `0` 就是 `1`

注意：本题与主站 525 题相同： https://leetcode-cn.com/problems/contiguous-array/

# 示例

```
输入: nums = [0,1]
输出: 2
说明: [0, 1] 是具有相同数量 0 和 1 的最长连续子数组。
```

```
输入: nums = [0,1,0]
输出: 2
说明: [0, 1] (或 [1, 0]) 是具有相同数量 0 和 1 的最长连续子数组。
```

# 题解

## 前缀和+哈希表

我们将数组中的 0 当作 -1 ，就将原问题转化为“求最长的连续子数组，其和为 0”。

这样我们可以使用前缀和来处理这个问题，同时使用哈希表来维护第一次出现相同和的下标位置。因为我们要计算最长的连续子数组，如果出现相同的前缀和只需要记录第一次的下标即可。

```ts
function findMaxLength(nums: number[]): number {
  const n = nums.length;
  const map = new Map<number, number>();
  map.set(0, -1);
  let count = 0;
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (nums[i] === 1) count++;
    else count--;
    if (map.has(count)) {
      ans = Math.max(ans, i - map.get(count));
    } else {
      map.set(count, i);
    }
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int findMaxLength(vector<int> &nums)
    {
        int n = nums.size(), count = 0, ans = 0;
        unordered_map<int, int> m;
        m[count] = -1;
        for (int i = 0; i < n; i++)
        {
            if (nums[i] == 1)
            {
                count++;
            }
            else
            {
                count--;
            }
            if (m.count(count))
            {
                ans = max(ans, i - m[count]);
            }
            else
            {
                m[count] = i;
            }
        }
        return ans;
    }
};
```
