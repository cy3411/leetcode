# 题目

在一个 2 x 3 的板上（board）有 5 块砖瓦，用数字 1~5 来表示, 以及一块空缺用 0 来表示.

一次移动定义为选择 0 与一个相邻的数字（上下左右）进行交换.

最终当板 board 的结果是 [[1,2,3],[4,5,0]] 谜板被解开。

给出一个谜板的初始状态，返回最少可以通过多少次移动解开谜板，如果不能解开谜板，则返回 -1 。

提示：

- board 是一个如上所述的 2 x 3 的数组.
- board[i][j] 是一个 [0, 1, 2, 3, 4, 5] 的排列.

# 示例

```
输入：board = [[4,1,2],[5,0,3]]
输出：5
解释：
最少完成谜板的最少移动次数是 5 ，
一种移动路径:
尚未移动: [[4,1,2],[5,0,3]]
移动 1 次: [[4,1,2],[0,5,3]]
移动 2 次: [[0,1,2],[4,5,3]]
移动 3 次: [[1,0,2],[4,5,3]]
移动 4 次: [[1,2,0],[4,5,3]]
移动 5 次: [[1,2,3],[4,5,0]]
```

# 题解

## 广度优先搜索

将每次移动 0 的状态放入到队列中，循环队列，直到找到结果或者循环结束。

我们这里使用字符串来表示每次搜索的状态。

```ts
class Data {
  constructor(public status: string, public step: number) {
    this.status = status;
    this.step = step;
  }
}

function slidingPuzzle(board: number[][]): number {
  const dirs = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  const endStatus = '123450';
  const initStatus = board.toString().split(',').join('');

  const getNexts = (status: string): string[] => {
    const arr = status.split('');
    const idx = arr.indexOf('0');
    // 每次0移动后的下一个状态
    const result = [];
    for (let i = 0; i < dirs.length; i++) {
      // 一维坐标转二维坐标
      let x = dirs[i][0] + ((idx / 3) >> 0);
      let y = dirs[i][1] + (idx % 3);
      if (x < 0 || x >= 2) continue;
      if (y < 0 || y >= 3) continue;
      // 二维坐标转一维坐标
      let nextIdx = x * 3 + y;
      // 交换得到新的状态
      [arr[idx], arr[nextIdx]] = [arr[nextIdx], arr[idx]];
      // 存入结果
      result.push(arr.join(''));
      // 还原，方便下一次更新状态
      [arr[idx], arr[nextIdx]] = [arr[nextIdx], arr[idx]];
    }
    return result;
  };

  const visited: Set<string> = new Set();
  const queue: Data[] = [];
  queue.push(new Data(initStatus, 0));
  visited.add(initStatus);

  while (queue.length) {
    const data = queue.shift();
    if (data.status === endStatus) return data.step;
    const nexts = getNexts(data.status);
    for (let next of nexts) {
      if (visited.has(next)) continue;
      queue.push(new Data(next, data.step + 1));
      visited.add(next);
    }
  }

  return -1;
}
```
