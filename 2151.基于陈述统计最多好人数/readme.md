# 题目

游戏中存在两种角色：

- 好人：该角色只说真话。
- 坏人：该角色可能说真话，也可能说假话。

给你一个下标从 `0` 开始的二维整数数组 `statements` ，大小为 `n x n` ，表示 `n` 个玩家对彼此角色的陈述。具体来说，`statements[i][j]` 可以是下述值之一：

- `0` 表示 `i` 的陈述认为 `j` 是 坏人 。
- `1` 表示 `i` 的陈述认为 `j` 是 好人 。
- `2` 表示 `i` 没有对 `j` 作出陈述。

另外，玩家不会对自己进行陈述。形式上，对所有 $0 \leq i < n$ ，都有 `statements[i][i] = 2` 。

根据这 `n` 个玩家的陈述，返回可以认为是 **好人** 的 最大 数目。

提示：

- $n \equiv statements.length \equiv statements[i].length$
- $2 \leq n \leq 15$
- `statements[i][j]` 的值为 `0`、`1` 或 `2`
- $statements[i][i] \equiv 2$

# 示例

[![qVYyHx.png](https://s1.ax1x.com/2022/03/20/qVYyHx.png)](https://imgtu.com/i/qVYyHx)

```
输入：statements = [[2,1,2],[1,2,2],[2,0,2]]
输出：2
解释：每个人都做一条陈述。
- 0 认为 1 是好人。
- 1 认为 0 是好人。
- 2 认为 1 是坏人。
以 2 为突破点。
- 假设 2 是一个好人：
    - 基于 2 的陈述，1 是坏人。
    - 那么可以确认 1 是坏人，2 是好人。
    - 基于 1 的陈述，由于 1 是坏人，那么他在陈述时可能：
        - 说真话。在这种情况下会出现矛盾，所以假设无效。
        - 说假话。在这种情况下，0 也是坏人并且在陈述时说假话。
    - 在认为 2 是好人的情况下，这组玩家中只有一个好人。
- 假设 2 是一个坏人：
    - 基于 2 的陈述，由于 2 是坏人，那么他在陈述时可能：
        - 说真话。在这种情况下，0 和 1 都是坏人。
            - 在认为 2 是坏人但说真话的情况下，这组玩家中没有一个好人。
        - 说假话。在这种情况下，1 是好人。
            - 由于 1 是好人，0 也是好人。
            - 在认为 2 是坏人且说假话的情况下，这组玩家中有两个好人。
在最佳情况下，至多有两个好人，所以返回 2 。
注意，能得到此结论的方法不止一种。
```

[![qVY4vd.png](https://s1.ax1x.com/2022/03/20/qVY4vd.png)](https://imgtu.com/i/qVY4vd)

```
输入：statements = [[2,0],[0,2]]
输出：1
解释：每个人都做一条陈述。
- 0 认为 1 是坏人。
- 1 认为 0 是坏人。
以 0 为突破点。
- 假设 0 是一个好人：
    - 基于与 0 的陈述，1 是坏人并说假话。
    - 在认为 0 是好人的情况下，这组玩家中只有一个好人。
- 假设 0 是一个坏人：
    - 基于 0 的陈述，由于 0 是坏人，那么他在陈述时可能：
        - 说真话。在这种情况下，0 和 1 都是坏人。
            - 在认为 0 是坏人但说真话的情况下，这组玩家中没有一个好人。
        - 说假话。在这种情况下，1 是好人。
            - 在认为 0 是坏人且说假话的情况下，这组玩家中只有一个好人。
在最佳情况下，至多有一个好人，所以返回 1 。
注意，能得到此结论的方法不止一种。
```

# 题解

## 枚举

每一个人都只有好人或者坏人两种情况，最多只有 $2^n$ 个情况，其中 `n` 为玩家数量。

我们使用二进制位来表示每一个人的情况，每一位表示一个玩家的情况，`0` 表示坏人，`1` 表示好人。枚举 `[0, 2^n)` 的所有情况，并判断每一个情况是否满足条件。如果满足条件，就获取当前组合中 `1` 的数量，用来更新答案。

组合是否满足条件，判断玩家的陈述是否可以跟二进制位的状态对应上，如果可以，则当前组合满足条件。

```ts
function maximumGood(statements: number[][]): number {
  const isValid = (mask: number): boolean => {
    for (let i = 0; i < n; i++) {
      if ((mask & (1 << i)) === 0) continue;
      // 验证第 i 个人说的是否是真话
      for (let j = 0; j < n; j++) {
        if (statements[i][j] === 2) continue;
        // 当前玩家的陈述是否能满足二进制位的状态
        if (statements[i][j] !== Number(!!(mask & (1 << j)))) return false;
      }
    }
    return true;
  };
  const getOneNumber = (mask: number): number => {
    let res = 0;
    while (mask) {
      res += 1;
      mask &= mask - 1;
    }
    return res;
  };
  const n = statements.length;
  let ans = 0;
  // 枚举每一种状态，然后验证当前状态是否可行
  for (let i = 0; i < 1 << n; i++) {
    if (!isValid(i)) continue;
    ans = Math.max(ans, getOneNumber(i));
  }

  return ans;
}
```

```ts
class Solution
{
public:
    int isValid(vector<vector<int>> &statements, int mask)
    {
        int n = statements.size();
        for (int i = 0; i < n; i++)
        {
            if ((mask & (1 << i)) == 0)
            {
                continue;
            }
            for (int j = 0; j < n; j++)
            {
                if (statements[i][j] == 2)
                {
                    continue;
                }
                if (statements[i][j] != !!(mask & (1 << j)))
                {
                    return false;
                }
            }
        }
        return true;
    }
    int getOnes(int mask)
    {
        int res = 0;
        while (mask)
        {
            res++;
            mask &= (mask - 1);
        }
        return res;
    }
    int maximumGood(vector<vector<int>> &statements)
    {
        int n = statements.size(), ans = 0;
        for (int i = 0; i < (1 << n); i++)
        {
            if (!isValid(statements, i))
            {
                continue;
            }
            ans = max(ans, getOnes(i));
        }

        return ans;
    }
};
```
