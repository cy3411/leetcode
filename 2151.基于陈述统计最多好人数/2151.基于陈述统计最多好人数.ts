/*
 * @lc app=leetcode.cn id=2151 lang=typescript
 *
 * [2151] 基于陈述统计最多好人数
 */

// @lc code=start
function maximumGood(statements: number[][]): number {
  const isValid = (mask: number): boolean => {
    for (let i = 0; i < n; i++) {
      if ((mask & (1 << i)) === 0) continue;
      // 验证第 i 个人说的是否是真话
      for (let j = 0; j < n; j++) {
        if (statements[i][j] === 2) continue;
        if (statements[i][j] !== Number(!!(mask & (1 << j)))) return false;
      }
    }
    return true;
  };
  const getOneNumber = (mask: number): number => {
    let res = 0;
    while (mask) {
      res += 1;
      mask &= mask - 1;
    }
    return res;
  };
  const n = statements.length;
  let ans = 0;
  // 枚举每一种状态，然后验证当前状态是否可行
  for (let i = 0; i < 1 << n; i++) {
    if (!isValid(i)) continue;
    ans = Math.max(ans, getOneNumber(i));
  }

  return ans;
}
// @lc code=end
