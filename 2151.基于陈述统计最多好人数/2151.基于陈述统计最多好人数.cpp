/*
 * @lc app=leetcode.cn id=2151 lang=cpp
 *
 * [2151] 基于陈述统计最多好人数
 */

// @lc code=start
class Solution
{
public:
    int isValid(vector<vector<int>> &statements, int mask)
    {
        int n = statements.size();
        for (int i = 0; i < n; i++)
        {
            if ((mask & (1 << i)) == 0)
            {
                continue;
            }
            for (int j = 0; j < n; j++)
            {
                if (statements[i][j] == 2)
                {
                    continue;
                }
                if (statements[i][j] != !!(mask & (1 << j)))
                {
                    return false;
                }
            }
        }
        return true;
    }
    int getOnes(int mask)
    {
        int res = 0;
        while (mask)
        {
            res++;
            mask &= (mask - 1);
        }
        return res;
    }
    int maximumGood(vector<vector<int>> &statements)
    {
        int n = statements.size(), ans = 0;
        for (int i = 0; i < (1 << n); i++)
        {
            if (!isValid(statements, i))
            {
                continue;
            }
            ans = max(ans, getOnes(i));
        }

        return ans;
    }
};
// @lc code=end
