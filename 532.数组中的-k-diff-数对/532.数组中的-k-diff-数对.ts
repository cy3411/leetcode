/*
 * @lc app=leetcode.cn id=532 lang=typescript
 *
 * [532] 数组中的 k-diff 数对
 */

// @lc code=start
function findPairs(nums: number[], k: number): number {
  const ans = new Set<number>();
  const visited = new Set<number>();
  for (const num of nums) {
    if (visited.has(num - k)) {
      ans.add(num - k);
    }
    if (visited.has(num + k)) {
      ans.add(num);
    }
    visited.add(num);
  }

  return ans.size;
}
// @lc code=end
