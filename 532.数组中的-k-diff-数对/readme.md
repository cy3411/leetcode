# 题目

给你一个整数数组 `nums` 和一个整数 `k` ，请你在数组中找出 **不同的** `k-diff` 数对，并返回不同的 `k-diff` **数对** 的数目。

`k-diff` 数对定义为一个整数对 `(nums[i], nums[j])` ，并满足下述全部条件：

- $0 \leq i, j < nums.length$
- $i \not = j$
- $nums[i] - nums[j] \equiv k$

注意，|val| 表示 val 的绝对值。

提示：

- $1 \leq nums.length \leq 10^4$
- $-10^7 \leq nums[i] \leq 10^7$
- $0 \leq k \leq 10^7$

# 示例

```
输入：nums = [3, 1, 4, 1, 5], k = 2
输出：2
解释：数组中有两个 2-diff 数对, (1, 3) 和 (3, 5)。
尽管数组中有两个 1 ，但我们只应返回不同的数对的数量。
```

```
输入：nums = [1, 2, 3, 4, 5], k = 1
输出：4
解释：数组中有四个 1-diff 数对, (1, 2), (2, 3), (3, 4) 和 (4, 5) 。
```

# 题解

## 哈希表

遍历数组，判断当前元素的左边是否有满足条件的 k-diff 对，如果有，则将其加入结果集。

为了降低复杂度，可以将当前元素左边的元素的值存储在哈希表中，以便在遍历时快速查找。

```ts
function findPairs(nums: number[], k: number): number {
  const ans = new Set<number>();
  const visited = new Set<number>();
  for (const num of nums) {
    if (visited.has(num - k)) {
      ans.add(num - k);
    }
    if (visited.has(num + k)) {
      ans.add(num);
    }
    visited.add(num);
  }

  return ans.size;
}
```

## 排序+双指针

将数组做升序排序，定义 i 和 j 双指针来搜索数对：

- $i < j$
- $nums[i] + k = nums[j]$

```cpp
class Solution {
public:
    int findPairs(vector<int> &nums, int k) {
        int n = nums.size();
        int ans = 0;
        sort(nums.begin(), nums.end());
        for (int i = 0, j = 0; i < n; i++) {
            // 消除重复数字
            if (i != 0 && nums[i] == nums[i - 1]) continue;
            // 开始查找 j 位置
            while (j < n && (j <= i || nums[j] < nums[i] + k)) j++;
            // 是否满足 k-diff
            if (j < n && nums[j] == nums[i] + k) ans++;
        }
        return ans;
    }
};
```
