/*
 * @lc app=leetcode.cn id=532 lang=cpp
 *
 * [532] 数组中的 k-diff 数对
 */

// @lc code=start
class Solution {
public:
    int findPairs(vector<int> &nums, int k) {
        int n = nums.size();
        int ans = 0;
        sort(nums.begin(), nums.end());
        for (int i = 0, j = 0; i < n; i++) {
            // 消除重复数字
            if (i != 0 && nums[i] == nums[i - 1]) continue;
            // 开始查找 j 位置
            while (j < n && (j <= i || nums[j] < nums[i] + k)) j++;
            // 是否满足 k-diff
            if (j < n && nums[j] == nums[i] + k) ans++;
        }
        return ans;
    }
};
// @lc code=end
