/*
 * @lc app=leetcode.cn id=8 lang=typescript
 *
 * [8] 字符串转换整数 (atoi)
 */

// @lc code=start
function myAtoi(s: string): number {
  const maxint = 2 ** 31 - 1;
  const minint = -(2 ** 31);
  let flag = 1;
  // 去掉个位数的最大整数
  const maxPre = (maxint / 10) >> 0;
  // 最大整数的个位数
  const maxLast = maxint % 10;
  let idx = 0;
  // 消除前面的空格
  while (s[idx] === ' ') idx++;
  // 根据第一个字符判断正负
  if (s[idx] === '-') {
    flag = -1;
    idx++;
  } else if (s[idx] === '+') {
    idx++;
  }
  let num = 0;
  for (; idx < s.length; idx++) {
    if (s[idx] < '0' || s[idx] > '9') break;
    // 当前整数大于maxPre，因为下一步会乘10，肯定会超过最大整数
    // 当前整数等于maxPre，但是下一位数字会超过maxLast
    if (num > maxPre || (num === maxPre && Number(s[idx]) > maxLast)) {
      if (flag > 0) return maxint;
      else return minint;
    }
    num = num * 10 + Number(s[idx]);
  }

  return num * flag;
}
// @lc code=end
