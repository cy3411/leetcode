# 题目

请你来实现一个 myAtoi(string s) 函数，使其能将字符串转换成一个 32 位有符号整数（类似 C/C++ 中的 atoi 函数）。

函数 myAtoi(string s) 的算法如下：

- 读入字符串并丢弃无用的前导空格
- 检查下一个字符（假设还未到字符末尾）为正还是负号，读取该字符（如果有）。 确定最终结果是负数还是正数。 如果两者都不存在，则假定结果为正。
- 读入下一个字符，直到到达下一个非数字字符或到达输入的结尾。字符串的其余部分将被忽略。
- 将前面步骤读入的这些数字转换为整数（即，"123" -> 123， "0032" -> 32）。如果没有读入数字，则整数为 0 。必要时更改符号（从步骤 2 开始）。
- 如果整数数超过 32 位有符号整数范围 [−$2^{31}$, $2^{31}$ − 1] ，需要截断这个整数，使其保持在这个范围内。具体来说，小于 −$2^{31}$ 的整数应该被固定为 −$2^{31}$ ，大于 $2^{31}$ − 1 的整数应该被固定为 $2^{31}$ − 1 。
- 返回整数作为最终结果。

注意：

- 本题中的空白字符只包括空格字符 ' ' 。
- 除前导空格或数字后的其余字符串外，请勿忽略 任何其他字符。

提示：

- 0 <= s.length <= 200
- s 由英文字母（大写和小写）、数字（0-9）、' '、'+'、'-' 和 '.' 组成

# 示例

```
输入：s = "42"
输出：42
解释：加粗的字符串为已经读入的字符，插入符号是当前读取的字符。
第 1 步："42"（当前没有读入字符，因为没有前导空格）
         ^
第 2 步："42"（当前没有读入字符，因为这里不存在 '-' 或者 '+'）
         ^
第 3 步："42"（读入 "42"）
           ^
解析得到整数 42 。
由于 "42" 在范围 [-231, 231 - 1] 内，最终结果为 42 。
```

# 题解

## 模拟操作

按照题意给出的步骤实现即可：

- 消除前导空格
- 判断正负
- 循环判断是否是非数字字符
- 判断是否越界，提前终止循环
- 转换整数

```ts
function myAtoi(s: string): number {
  const maxint = 2 ** 31 - 1;
  const minint = -(2 ** 31);
  let flag = 1;
  // 去掉个位数的最大整数
  const maxPre = (maxint / 10) >> 0;
  // 最大整数的个位数
  const maxLast = maxint % 10;
  let idx = 0;
  // 消除前面的空格
  while (s[idx] === ' ') idx++;
  // 根据第一个字符判断正负
  if (s[idx] === '-') {
    flag = -1;
    idx++;
  } else if (s[idx] === '+') {
    idx++;
  }
  let num = 0;
  for (; idx < s.length; idx++) {
    if (s[idx] < '0' || s[idx] > '9') break;
    // 当前整数大于maxPre，因为下一步会乘10，肯定会超过最大整数
    // 当前整数等于maxPre，但是下一位数字会超过maxLast
    if (num > maxPre || (num === maxPre && Number(s[idx]) > maxLast)) {
      if (flag > 0) return maxint;
      else return minint;
    }
    num = num * 10 + Number(s[idx]);
  }

  return num * flag;
}
```
