# 题目

返回 s 字典序最小的子序列，该子序列包含 s 的所有不同字符，且只包含一次。

提示：

- 1 <= s.length <= 1000
- s 由小写英文字母组成

# 示例

```
输入：s = "bcabc"
输出："abc"
```

```
输入：s = "cbacdcbc"
输出："acdb"
```

# 题解

## 单调栈

要求结果是单调递增，首先想到的就是单调栈。

我们可以先统计每个字符的出现次数，然后遍历每个字符，当栈中没有当前字符的时候，进行单调递增栈的处理。

需要注意的就是出栈的时候需要判断一下栈顶字符在后续的字符串中是否还有，如果还有就可以出栈，如果没有了，就不能出栈。因为题目还有一个限制，就是每个字符只能出现一次。

```ts
function smallestSubsequence(s: string): string {
  // 记录字符出现的次数
  const hash: Map<string, number> = new Map();
  for (const c of s) {
    hash.set(c, (hash.get(c) ?? 0) + 1);
  }

  const m = s.length;
  const stack = [];
  for (const c of s) {
    // 当前字符栈中没有，才进行单调栈操作
    if (!stack.includes(c)) {
      // 当前字符还有剩余才可以出栈
      while (stack.length && c <= stack[stack.length - 1] && hash.get(stack[stack.length - 1])) {
        stack.pop();
      }
      stack.push(c);
    }
    // 将扫描过的字符次数减少1次
    hash.set(c, hash.get(c) - 1);
  }
  // 最后栈中剩下的就是结果
  return stack.join('');
}
```
