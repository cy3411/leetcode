/*
 * @lc app=leetcode.cn id=2001 lang=typescript
 *
 * [2001] 可互换矩形的组数
 */

// @lc code=start
function interchangeableRectangles(rectangles: number[][]): number {
  const gcd = (a: number, b: number) => (b === 0 ? a : gcd(b, a % b));
  // 计算矩形的最小宽高比，返回拼接的字符串做为哈希key
  const getKey = (a: number, b: number): string => {
    const g = gcd(a, b);
    return `${a / g}_${b / g}`;
  };
  const map = new Map<string, number>();

  let ans = 0;
  // 遍历，累加相同的宽高比的矩形数量
  for (const [x, y] of rectangles) {
    const key = getKey(x, y);
    const count = map.get(key) ?? 0;
    ans += count;
    map.set(key, count + 1);
  }

  return ans;
}
// @lc code=end
