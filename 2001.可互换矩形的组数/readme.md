# 题目

用一个下标从 0 开始的二维整数数组 rectangles 来表示 n 个矩形，其中 rectangles[i] = [widthi, heighti] 表示第 i 个矩形的宽度和高度。

如果两个矩形 i 和 j（i < j）的宽高比相同，则认为这两个矩形 可互换 。更规范的说法是，两个矩形满足 widthi/heighti == widthj/heightj（使用实数除法而非整数除法），则认为这两个矩形 可互换 。

计算并返回 rectangles 中有多少对 可互换 矩形。

提示：

- $\color{burlywood}n \equiv rectangles.length$
- $\color{burlywood}1 \leq n \leq 10^5$
- $\color{burlywood}rectangles[i].length \equiv 2$
- $\color{burlywood}1 \leq widthi, heighti \leq 10^5$

# 示例

```
输入：rectangles = [[4,8],[3,6],[10,20],[15,30]]
输出：6
解释：下面按下标（从 0 开始）列出可互换矩形的配对情况：
- 矩形 0 和矩形 1 ：4/8 == 3/6
- 矩形 0 和矩形 2 ：4/8 == 10/20
- 矩形 0 和矩形 3 ：4/8 == 15/30
- 矩形 1 和矩形 2 ：3/6 == 10/20
- 矩形 1 和矩形 3 ：3/6 == 15/30
- 矩形 2 和矩形 3 ：10/20 == 15/30
```

```
输入：rectangles = [[4,5],[7,8]]
输出：0
解释：不存在成对的可互换矩形。
```

# 题解

## 哈希表

遍历所有矩形，将宽高比作为键，数量作为值存入哈希表。同时累加每个键对应的值。

```ts
function interchangeableRectangles(rectangles: number[][]): number {
  const gcd = (a: number, b: number) => (b === 0 ? a : gcd(b, a % b));
  // 计算矩形的最小宽高比，返回拼接的字符串做为哈希key
  const getKey = (a: number, b: number): string => {
    const g = gcd(a, b);
    return `${a / g}_${b / g}`;
  };
  const map = new Map<string, number>();

  let ans = 0;
  // 遍历，累加相同的宽高比的矩形数量
  for (const [x, y] of rectangles) {
    const key = getKey(x, y);
    const count = map.get(key) ?? 0;
    ans += count;
    map.set(key, count + 1);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    struct Data
    {
        Data(int a, int b) : a(a), b(b)
        {
            int g = gcd(a, b);
            this->a /= g;
            this->b /= g;
        };

        bool operator<(const Data &obj) const
        {
            if (a != obj.a)
            {
                return a < obj.a;
            }
            return b < obj.b;
        }

        int a, b;
    };
    long long interchangeableRectangles(vector<vector<int>> &rectangles)
    {
        map<Data, long long> m;
        long long ans = 0;

        for (auto x : rectangles)
        {
            Data d(x[0], x[1]);
            ans += m[d];
            m[d]++;
        }

        return ans;
    }
};
```
