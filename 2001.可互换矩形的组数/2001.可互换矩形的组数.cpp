/*
 * @lc app=leetcode.cn id=2001 lang=cpp
 *
 * [2001] 可互换矩形的组数
 */

// @lc code=start
class Solution
{
public:
    struct Data
    {
        Data(int a, int b) : a(a), b(b)
        {
            int g = gcd(a, b);
            this->a /= g;
            this->b /= g;
        };

        bool operator<(const Data &obj) const
        {
            if (a != obj.a)
            {
                return a < obj.a;
            }
            return b < obj.b;
        }

        int a, b;
    };
    long long interchangeableRectangles(vector<vector<int>> &rectangles)
    {
        map<Data, long long> m;
        long long ans = 0;

        for (auto x : rectangles)
        {
            Data d(x[0], x[1]);
            ans += m[d];
            m[d]++;
        }

        return ans;
    }
};
// @lc code=end
