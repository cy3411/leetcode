/*
 * @lc app=leetcode.cn id=275 lang=typescript
 *
 * [275] H 指数 II
 */

// @lc code=start
function hIndex(citations: number[]): number {
  let m = citations.length;
  let l = 0;
  let r = m - 1;
  let mid: number;
  // 搜索第一个大于m-mid的位置
  while (l <= r) {
    mid = l + ((r - l) >> 1);
    if (citations[mid] >= m - mid) r = mid - 1;
    else l = mid + 1;
  }

  return m - l;
}
// @lc code=end
