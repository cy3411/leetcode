/*
 * @lc app=leetcode.cn id=222 lang=javascript
 *
 * [222] 完全二叉树的节点个数
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function (root) {
  if (root === null) return 0;

  // 节点队列
  let queue = [root];
  let count = 0;
  // 逐层扫描
  while (queue.length) {
    let top = queue.shift();
    count++;
    if (top.left) queue.push(top.left);
    if (top.right) queue.push(top.right);
  }

  return count;
};
// @lc code=end
