# 题目

给出矩阵 `matrix` 和目标值 `target`，返回元素总和等于目标值的非空子矩阵的数量。

子矩阵 `x1, y1, x2, y2` 是满足 `x1 <= x <= x2` 且 `y1 <= y <= y2` 的所有单元 `matrix[x][y]` 的集合。

如果 `(x1, y1, x2, y2)` 和 `(x1', y1', x2', y2')` 两个子矩阵中部分坐标不同（如：`x1 != x1'`），那么这两个子矩阵也不同。

提示：

- 1 <= matrix.length <= 100
- 1 <= matrix[0].length <= 100
- -1000 <= matrix[i] <= 1000
- -10^8 <= target <= 10^8

# 示例

```
输入：matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
输出：4
解释：四个只含 0 的 1x1 子矩阵。
```

```
输入：matrix = [[1,-1],[-1,1]], target = 0
输出：5
解释：两个 1x2 子矩阵，加上两个 2x1 子矩阵，再加上一个 2x2 子矩阵。
```

# 题解

我们枚举子矩阵上下边界，并计算子矩阵内每列的和，这样问题就转化成：

> 在一个整数数组中找到子数组和等于 target 的子数组数量。

```ts
function numSubmatrixSumTarget(matrix: number[][], target: number): number {
  const m = matrix.length;
  const n = matrix[0].length;

  // 计算当前前缀和中是否有符合条件的元素
  const calcCount = (sum: number[], target: number): number => {
    let count = 0;
    let prefixSum = 0;
    const hash = new Map([[0, 1]]);

    for (const x of sum) {
      // 二维前缀和
      prefixSum += x;
      if (hash.has(prefixSum - target)) count += hash.get(prefixSum - target);
      hash.set(prefixSum, (hash.get(prefixSum) || 0) + 1);
    }
    return count;
  };

  let result = 0;
  for (let i = 0; i < m; i++) {
    const sum = new Array(n).fill(0);
    for (let j = i; j < m; j++) {
      for (let k = 0; k < n; k++) {
        // 计算同列元素的累加结果
        sum[k] += matrix[j][k];
      }
      // 每次枚举下边界后，去计算当前子矩阵内有多少结果
      result += calcCount(sum, target);
    }
  }

  return result;
}
```
