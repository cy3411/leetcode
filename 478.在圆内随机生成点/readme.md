# 题目

给定圆的半径和圆心的位置，实现函数 `randPoint` ，在圆中产生均匀随机点。

实现 Solution 类:

- `Solution(double radius, double x_center, double y_center)` 用圆的半径 `radius` 和圆心的位置 `(x_center, y_center)` 初始化对象
- `randPoint()` 返回圆内的一个随机点。圆周上的一点被认为在圆内。答案作为数组返回 `[x, y]` 。

提示：

- $0 < radius \leq 10^8$
- $-10^7 \leq x_center, y_center \leq 10^7$
- `randPoint` 最多被调用 $3 * 10^4$ 次

# 示例

```
输入:
["Solution","randPoint","randPoint","randPoint"]
[[1.0, 0.0, 0.0], [], [], []]
输出: [null, [-0.02493, -0.38077], [0.82314, 0.38945], [0.36572, 0.17248]]
解释:
Solution solution = new Solution(1.0, 0.0, 0.0);
solution.randPoint ();//返回[-0.02493，-0.38077]
solution.randPoint ();//返回[0.82314,0.38945]
solution.randPoint ();//返回[0.36572,0.17248]
```

# 题解

## 拒绝采样

我们在一个边长为 2R 的正方形中随机一个点，然后判断当前点是否在圆内，如果在圆内，则返回结果，如果不在圆内，则重新随机。

```ts
class Solution {
  x: number;
  y: number;
  r: number;
  constructor(radius: number, x_center: number, y_center: number) {
    this.x = x_center;
    this.y = y_center;
    this.r = radius;
  }

  randPoint(): number[] {
    while (1) {
      // 在[-r,r]的范围内随机生成一个数
      const x = Math.random() * 2 * this.r - this.r;
      const y = Math.random() * 2 * this.r - this.r;
      // 点如果在圆内，则返回
      if (x * x + y * y <= this.r * this.r) {
        return [this.x + x, this.y + y];
      }
    }
  }
}
```
