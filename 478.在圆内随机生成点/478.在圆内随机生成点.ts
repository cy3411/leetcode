/*
 * @lc app=leetcode.cn id=478 lang=typescript
 *
 * [478] 在圆内随机生成点
 */

// @lc code=start
class Solution {
  x: number;
  y: number;
  r: number;
  constructor(radius: number, x_center: number, y_center: number) {
    this.x = x_center;
    this.y = y_center;
    this.r = radius;
  }

  randPoint(): number[] {
    while (1) {
      // 在[-r,r]的范围内随机生成一个数
      const x = Math.random() * 2 * this.r - this.r;
      const y = Math.random() * 2 * this.r - this.r;
      //   点如果在圆内，则返回
      if (x * x + y * y <= this.r * this.r) {
        return [this.x + x, this.y + y];
      }
    }
  }
}

/**
 * Your Solution object will be instantiated and called as such:
 * var obj = new Solution(radius, x_center, y_center)
 * var param_1 = obj.randPoint()
 */
// @lc code=end
