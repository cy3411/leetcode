# 题目

在二叉树中，根节点位于深度 `0` 处，每个深度为 `k` 的节点的子节点位于深度` k+1` 处。

如果二叉树的两个节点深度相同，但 **父节点不同** ，则它们是一对堂兄弟节点。

我们给出了具有唯一值的二叉树的根节点 `root` ，以及树中两个不同节点的值 `x` 和 `y` 。

只有与值 `x` 和 `y` 对应的节点是堂兄弟节点时，才返回 `true` 。否则，返回 `false。`

提示：

- 二叉树的节点数介于 2 到 100 之间。
- 每个节点的值都是唯一的、范围为 1 到 100 的整数。

# 示例

```
输入：root = [1,2,3,4], x = 4, y = 3
输出：false
```

```
输入：root = [1,2,3,null,4,null,5], x = 5, y = 4
输出：true
```

# 题解

判断是否是堂兄弟节点，就需要知道需要比较节点的父节点和深度。  
我们需要遍历所有的节点去找到需要比较的节点。常规做法就是广度优先和深度优先搜索。

## 广度优先搜索

```ts
// 保存队列里的状态
class Data {
  node: TreeNode;
  parent: TreeNode;
  depth: number;
  constructor(node: TreeNode | null = null, parent: TreeNode | null = null, depth: number = 0) {
    this.node = node;
    this.parent = parent;
    this.depth = depth;
  }
}

function isCousins(root: TreeNode | null, x: number, y: number): boolean {
  let xnode: Data = null;
  let ynode: Data = null;
  const queue = [];
  // 初始化
  queue.push(new Data(root, null, 0));
  while (queue.length) {
    const data = queue.shift();
    if (data.node.val === x) xnode = data;
    if (data.node.val === y) ynode = data;
    if (xnode !== null && ynode !== null) break;
    if (data.node.left) queue.push(new Data(data.node.left, data.node, data.depth + 1));
    if (data.node.right) queue.push(new Data(data.node.right, data.node, data.depth + 1));
  }
  // console.log(xnode, ynode);
  return xnode.depth === ynode.depth && xnode.parent !== ynode.parent;
}
```

```ts
function isCousins(root: TreeNode | null, x: number, y: number): boolean {
  let xParent = null;
  let xDepth = 0;
  let isFoundX = false;
  let yParent = null;
  let yDepth = 0;
  let isFoundY = false;

  const updateStatus = (root: TreeNode, parent: TreeNode, depth: number): void => {
    if (root.val === x) {
      [xParent, xDepth, isFoundX] = [parent, depth, true];
    } else if (root.val === y) {
      [yParent, yDepth, isFoundY] = [parent, depth, true];
    }
  };
  type X = [TreeNode, number];
  // 存储节点和深度
  const queue: X[] = [[root, 0]];
  updateStatus(root, null, 0);
  while (queue.length) {
    const [node, depth] = queue.shift();
    if (node.left) {
      queue.push([node.left, depth + 1]);
      updateStatus(node.left, node, depth + 1);
    }
    if (node.right) {
      queue.push([node.right, depth + 1]);
      updateStatus(node.right, node, depth + 1);
    }
    if (xParent && yParent) break;
  }
  // 比较两个节点
  return xParent !== yParent && xDepth === yDepth;
}
```

## 深度优先搜索

```ts
function isCousins(root: TreeNode | null, x: number, y: number): boolean {
  let xParent = null;
  let xDepth = 0;
  let isFoundX = false;
  let yParent = null;
  let yDepth = 0;
  let isFoundY = false;

  const dfs = (node: TreeNode | null, parent: TreeNode | null, depth: number) => {
    if (node === null) return;

    if (node.val === x) {
      [xParent, xDepth, isFoundX] = [parent, depth, true];
    } else if (node.val === y) {
      [yParent, yDepth, isFoundY] = [parent, depth, true];
    }

    // 找到了，直接退出
    if (isFoundX && isFoundY) return;

    node.left && dfs(node.left, node, depth + 1);

    // 找到了，直接退出
    if (isFoundX && isFoundY) return;

    node.right && dfs(node.right, node, depth + 1);
  };

  // 深度优先搜索
  dfs(root, null, 0);

  // 比较两个节点
  return xParent !== yParent && xDepth === yDepth;
}
```
