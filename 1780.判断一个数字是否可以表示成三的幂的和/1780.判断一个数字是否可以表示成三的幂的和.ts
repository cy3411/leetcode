/*
 * @lc app=leetcode.cn id=1780 lang=typescript
 *
 * [1780] 判断一个数字是否可以表示成三的幂的和
 */

// @lc code=start
function checkPowersOfThree(n: number): boolean {
  while (n > 0 && n % 3 !== 2) {
    n = (n / 3) >> 0;
  }
  return n === 0;
}
// @lc code=end
