# 题目

给你一个整数 `n` ，如果你可以将 `n` 表示成若干个不同的三的幂之和，请你返回 `true` ，否则请返回 `false` 。

对于一个整数 `y` ，如果存在整数 `x` 满足 $y == 3^x$ ，我们称这个整数 `y` 是三的幂。

提示：

- $1 <= n <= 10^7$

# 示例

```
输入：n = 12
输出：true
解释：12 = 31 + 32
```

```
输入：n = 91
输出：true
解释：91 = 30 + 32 + 34
```

# 题解

## 3 进制转换

可以将数字转换为 3 进制。如果数字的 3 进制位中没有 2，那么就是一个合法的答案。

```ts
function checkPowersOfThree(n: number): boolean {
  while (n > 0 && n % 3 !== 2) {
    n = (n / 3) >> 0;
  }
  return n === 0;
}
```

```cpp
class Solution {
public:
    bool checkPowersOfThree(int n) {
        while (n) {
            if (n % 3 == 2) return false;
            n /= 3;
        }
        return true;
    }
};
```

```py
class Solution:
    def checkPowersOfThree(self, n: int) -> bool:
        while n:
            if n % 3 == 2:
                return False
            n //= 3
        return True
```
