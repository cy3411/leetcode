/*
 * @lc app=leetcode.cn id=26 lang=typescript
 *
 * [26] 删除有序数组中的重复项
 */

// @lc code=start
function removeDuplicates(nums: number[]): number {
  const size = nums.length;
  if (size === 0) return 0;

  // 双指针
  let slow = 0;
  for (let i = 0; i < size; i++) {
    if (nums[slow] !== nums[i]) {
      slow++;
      nums[slow] = nums[i];
    }
  }
  return slow + 1;
}
// @lc code=end
