# 题目

有些数的素因子只有 3，5，7，请设计一个算法找出第 k 个数。注意，不是必须有这些素因子，而是必须不包含其他的素因子。例如，前几个数按顺序应该是 1，3，5，7，9，15，21。

# 示例

```
输入: k = 5

输出: 9
```

# 题解

## 小顶堆

利用小顶堆的特性，初始将 1 放入堆中，每次取出堆顶元素 x，然后将 3x,5x,7x 放入堆中，循环取 k 次即可。

因为有重复的元素存在，使用哈希表来去重。

```js
function getKthMagicNumber(k: number): number {
  const minHeap = new MinHeap();
  const factors = [3, 5, 7];
  const visited = new Set<number>();
  // 初始化
  minHeap.insert(1);
  visited.add(1);

  let ans = 0;
  for (let i = 1; i <= k; i++) {
    ans = minHeap.pop();
    for (const x of factors) {
      const next = ans * x;
      if (!visited.has(next)) {
        minHeap.insert(next);
        visited.add(next);
      }
    }
  }

  return ans;
}

class MinHeap {
  heap: number[];
  constructor() {
    this.heap = [];
  }
  less(i: number, j: number): boolean {
    return this.heap[i] < this.heap[j];
  }
  swap(i: number, j: number) {
    [this.heap[i], this.heap[j]] = [this.heap[j], this.heap[i]];
  }
  swin(i: number): void {
    if (i === 0) return;
    const parentIndex = (i - 1) >> 1;
    if (this.heap[parentIndex] > this.heap[i]) {
      this.swap(parentIndex, i);
      this.swin(parentIndex);
    }
  }
  sink(i: number): void {
    const leftIndex = i * 2 + 1;
    const rightIndex = i * 2 + 2;
    if (this.heap[leftIndex] < this.heap[i]) {
      this.swap(leftIndex, i);
      this.sink(leftIndex);
    }
    if (this.heap[rightIndex] < this.heap[i]) {
      this.swap(rightIndex, i);
      this.sink(rightIndex);
    }
  }
  insert(value: number): void {
    this.heap.push(value);
    this.swin(this.heap.length - 1);
  }
  pop(): number {
    this.swap(this.heap.length - 1, 0);
    const val = this.heap.pop();
    this.sink(0);
    return val!;
  }
}
```
