class Solution {
public:
    int getKthMagicNumber(int k) {
        vector<int> factors = {3, 5, 7};
        unordered_set<int> visited;
        priority_queue<long, vector<long>, greater<long>> heap;
        heap.push(1L);
        visited.insert(1L);

        int ans = 0;
        for (int i = 1; i <= k; i++) {
            long curr = heap.top();
            heap.pop();
            ans = (int)curr;
            for (auto x : factors) {
                long next = curr * x;
                if (!visited.count(next)) {
                    heap.push(next);
                    visited.insert(next);
                }
            }
        }
        return ans;
    }
};