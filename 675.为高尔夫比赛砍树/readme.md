# 题目

你被请来给一个要举办高尔夫比赛的树林砍树。树林由一个 `m x n` 的矩阵表示， 在这个矩阵中：

- `0` 表示障碍，无法触碰
- `1` 表示地面，可以行走
- `比 1 大的数` 表示有树的单元格，可以行走，数值表示树的高度

每一步，你都可以向上、下、左、右四个方向之一移动一个单位，如果你站的地方有一棵树，那么你可以决定是否要砍倒它。

你需要按照树的高度从低向高砍掉所有的树，每砍过一颗树，该单元格的值变为 1（即变为地面）。

你将从 `(0, 0)` 点开始工作，返回你砍完所有树需要走的最小步数。 如果你无法砍完所有的树，返回 `-1` 。

可以保证的是，没有两棵树的高度是相同的，并且你至少需要砍倒一棵树。

提示：

- $m \equiv forest.length$
- $n \equiv forest[i].length$
- $1 \leq m, n \leq 50$
- $0 \leq forest[i][j] \leq 10^9$

# 示例

[![X9212D.png](https://s1.ax1x.com/2022/05/23/X9212D.png)](https://imgtu.com/i/X9212D)

```
输入：forest = [[1,2,3],[0,0,4],[7,6,5]]
输出：6
解释：沿着上面的路径，你可以用 6 步，按从最矮到最高的顺序砍掉这些树。
```

[![X92Uat.png](https://s1.ax1x.com/2022/05/23/X92Uat.png)](https://imgtu.com/i/X92Uat)

```
输入：forest = [[1,2,3],[0,0,0],[7,6,5]]
输出：-1
解释：由于中间一行被障碍阻塞，无法访问最下面一行中的树。
```

# 题解

## 广度优先搜索

题目要求按照树的高度从低向高砍掉所有的树，所以需要对有树的单元格按照高度从低到高进行排序。

然后按序对每一个单元格进行广度优先搜索，并将每次的最短路径记录，最后返回最短路径的和。

```ts
interface NextData {
  x: number;
  y: number;
  steps: number;
}

function cutOffTree(forest: number[][]): number {
  const dirs = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
  ];

  const m = forest.length;
  const n = forest[0].length;
  // 广度优先搜索，每次搜索一个树的访问路径
  const bfs = (sx: number, sy: number, tx: number, ty: number): number => {
    if (sx === tx && sy === ty) return 0;
    const visited: number[][] = new Array(m).fill(0).map(() => new Array(n).fill(0));
    const queue: NextData[] = [];
    // 初始化起点
    queue.push({ x: sx, y: sy, steps: 0 });
    visited[sx][sy] = 1;
    while (queue.length) {
      const curr = queue.shift()!;
      if (curr.x === tx && curr.y === ty) return curr.steps;
      for (const [dx, dy] of dirs) {
        const nx = curr.x + dx;
        const ny = curr.y + dy;
        if (nx < 0 || nx >= m) continue;
        if (ny < 0 || ny >= n) continue;
        if (forest[nx][ny] === 0) continue;
        if (visited[nx][ny]) continue;
        visited[nx][ny] = 1;
        queue.push({ x: nx, y: ny, steps: curr.steps + 1 });
      }
    }
    return -1;
  };

  // 将所有的树的位置放入一个数组中，并且按照树的高度从小到大排序
  const trees = [];
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (forest[i][j] > 1) {
        trees.push([i, j]);
      }
    }
  }
  trees.sort((a: number[], b: number[]) => forest[a[0]][a[1]] - forest[b[0]][b[1]]);

  // 对每一个树进行广度优先搜索，找到路径
  let ans = 0;
  let sx = 0;
  let sy = 0;
  for (let i = 0; i < trees.length; i++) {
    let steps = bfs(sx, sy, trees[i][0], trees[i][1]);
    if (steps === -1) return -1;
    ans += steps;
    sx = trees[i][0];
    sy = trees[i][1];
  }
  return ans;
}
```
