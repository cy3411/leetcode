/*
 * @lc app=leetcode.cn id=841 lang=typescript
 *
 * [841] 钥匙和房间
 */

// @lc code=start
function canVisitAllRooms(rooms: number[][]): boolean {
  const n = rooms.length;
  // 队列，保存可以访问的房间序号
  const queue: number[] = [0];
  // 访问标记，0表示未访问，1表示已访问
  const visited: number[] = new Array(n).fill(0);
  visited[0] = 1;
  // 已经访问的房间数量
  let count = 1;
  while (queue.length) {
    const curr = queue.shift();
    for (const next of rooms[curr]) {
      if (visited[next]) continue;
      visited[next] = 1;
      queue.push(next);
      count++;
    }
  }
  return count === n;
}
// @lc code=end
