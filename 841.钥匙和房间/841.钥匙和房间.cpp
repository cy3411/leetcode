/*
 * @lc app=leetcode.cn id=841 lang=cpp
 *
 * [841] 钥匙和房间
 */

// @lc code=start
class Solution
{
public:
    bool canVisitAllRooms(vector<vector<int>> &rooms)
    {
        int n = rooms.size(), count = 1;
        vector<int> visited(n, 0);
        queue<int> q;
        q.push(0);
        visited[0] = 1;
        while (!q.empty())
        {
            int cur = q.front();
            q.pop();
            for (auto next : rooms[cur])
            {
                if (visited[next])
                {
                    continue;
                }
                visited[next] = 1;
                q.push(next);
                count++;
            }
        }
        return count == n;
    }
};
// @lc code=end
