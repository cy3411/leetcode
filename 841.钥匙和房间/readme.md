# 题目

有 `n` 个房间，房间按从 `0` 到 `n - 1` 编号。最初，除 `0` 号房间外的其余所有房间都被锁住。你的目标是进入所有的房间。然而，你不能在没有获得钥匙的时候进入锁住的房间。

当你进入一个房间，你可能会在里面找到一套不同的钥匙，每把钥匙上都有对应的房间号，即表示钥匙可以打开的房间。你可以拿上所有钥匙去解锁其他房间。

给你一个数组 `rooms` 其中 `rooms[i]` 是你进入 `i` 号房间可以获得的钥匙集合。如果能进入 **所有** 房间返回 `true`，否则返回 `false`。

提示：

- $n \equiv rooms.length$
- $2 \leq n \leq 1000$
- $0 \leq rooms[i].length \leq 1000$
- $1 \leq sum(rooms[i].length) \leq 3000$
- $0 \leq rooms[i][j] < n$
- 所有 `rooms[i]` 的值 **互不相同**

# 示例

```
输入：rooms = [[1],[2],[3],[]]
输出：true
解释：
我们从 0 号房间开始，拿到钥匙 1。
之后我们去 1 号房间，拿到钥匙 2。
然后我们去 2 号房间，拿到钥匙 3。
最后我们去了 3 号房间。
由于我们能够进入每个房间，我们返回 true。
```

```
输入：rooms = [[1,3],[3,0,1],[2],[0]]
输出：false
解释：我们不能进入 2 号房间。
```

# 题解

## 广度优先搜索

题目就是判断连通性的问题，这里使用广度优先搜索。

从 0 开始，每次将所有与当前房间相通且未被访问的过的房间加入队列，同时更新访问过的房间数量，最后判断是否访问完所有房间。

```ts
function canVisitAllRooms(rooms: number[][]): boolean {
  const n = rooms.length;
  // 队列，保存可以访问的房间序号
  const queue: number[] = [0];
  // 访问标记，0表示未访问，1表示已访问
  const visited: number[] = new Array(n).fill(0);
  visited[0] = 1;
  // 已经访问的房间数量
  let count = 1;
  while (queue.length) {
    const curr = queue.shift();
    for (const next of rooms[curr]) {
      if (visited[next]) continue;
      visited[next] = 1;
      queue.push(next);
      count++;
    }
  }
  return count === n;
}
```

```cpp
class Solution
{
public:
    bool canVisitAllRooms(vector<vector<int>> &rooms)
    {
        int n = rooms.size(), count = 1;
        vector<int> visited(n, 0);
        queue<int> q;
        q.push(0);
        visited[0] = 1;
        while (!q.empty())
        {
            int cur = q.front();
            q.pop();
            for (auto next : rooms[cur])
            {
                if (visited[next])
                {
                    continue;
                }
                visited[next] = 1;
                q.push(next);
                count++;
            }
        }
        return count == n;
    }
};
```
