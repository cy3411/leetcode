/*
 * @lc app=leetcode.cn id=1646 lang=typescript
 *
 * [1646] 获取生成数组中的最大值
 */

// @lc code=start
function getMaximumGenerated(n: number): number {
  if (n === 0) return 0;
  // 初始化
  const nums = new Array(n + 1).fill(0);
  nums[1] = 1;
  for (let i = 2; i <= n; i++) {
    const half = (i / 2) >> 0;
    // 计算每个索引的值
    nums[i] = nums[half] + (i % 2) * nums[half + 1];
  }
  return Math.max(...nums);
}
// @lc code=end
