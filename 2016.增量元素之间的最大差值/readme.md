# 题目

给你一个下标从 `0` 开始的整数数组 `nums` ，该数组的大小为 `n` ，请你计算 `nums[j] - nums[i]` 能求得的 **最大差值** ，其中 $0 \leq i < j < n$ 且 `nums[i] < nums[j]` 。

返回 最大差值 。如果不存在满足要求的 i 和 j ，返回 -1 。

提示：

- $n \equiv nums.length$
- $2 \leq n \leq 1000$
- $1 \leq nums[i] \leq 10^9$

# 示例

```
输入：nums = [7,1,5,4]
输出：4
解释：
最大差值出现在 i = 1 且 j = 2 时，nums[j] - nums[i] = 5 - 1 = 4 。
注意，尽管 i = 1 且 j = 0 时 ，nums[j] - nums[i] = 7 - 1 = 6 > 4 ，但 i > j 不满足题面要求，所以 6 不是有效的答案。
```

```
输入：nums = [9,4,3,2]
输出：-1
解释：
不存在同时满足 i < j 和 nums[i] < nums[j] 这两个条件的 i, j 组合。
```

# 题解

## 贪心

维护一个最小值，遍历数组，比较当前值和最小值，如果比最小值大，更新答案，否则，更新最小值。

```ts
function maximumDifference(nums: number[]): number {
  const n = nums.length;
  let ans = -1;
  // 维护最小值
  let premin = nums[0];

  for (let i = 1; i < n; i++) {
    if (nums[i] > premin) {
      ans = Math.max(ans, nums[i] - premin);
    } else {
      premin = nums[i];
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int maximumDifference(vector<int> &nums)
    {
        int n = nums.size();
        int ans = -1;
        int premin = nums[0];
        for (int i = 1; i < n; i++)
        {
            if (nums[i] > premin)
            {
                ans = max(ans, nums[i] - premin);
            }
            else
            {
                premin = nums[i];
            }
        }
        return ans;
    }
};
```
