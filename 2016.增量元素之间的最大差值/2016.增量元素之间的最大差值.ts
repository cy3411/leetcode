/*
 * @lc app=leetcode.cn id=2016 lang=typescript
 *
 * [2016] 增量元素之间的最大差值
 */

// @lc code=start
function maximumDifference(nums: number[]): number {
  const n = nums.length;
  let ans = -1;
  let premin = nums[0];

  for (let i = 1; i < n; i++) {
    if (nums[i] > premin) {
      ans = Math.max(ans, nums[i] - premin);
    } else {
      premin = nums[i];
    }
  }

  return ans;
}
// @lc code=end
