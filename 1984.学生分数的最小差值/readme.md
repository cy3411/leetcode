# 题目

给你一个 **下标从 0 开始** 的整数数组 `nums` ，其中 `nums[i]` 表示第 `i` 名学生的分数。另给你一个整数 `k` 。

从数组中选出任意 `k` 名学生的分数，使这 `k` 个分数间 **最高分** 和 **最低分** 的 **差值** 达到 最小化 。

返回可能的 **最小差值** 。

提示：

- $\color{burlywood} 1 \leq k \leq nums.length \leq 1000$
- $\color{burlywood} 0 \leq nums[i] \leq 10^5$

# 示例

```
输入：nums = [90], k = 1
输出：0
解释：选出 1 名学生的分数，仅有 1 种方法：
- [90] 最高分和最低分之间的差值是 90 - 90 = 0
可能的最小差值是 0
```

```
输入：nums = [9,4,1,7], k = 2
输出：2
解释：选出 2 名学生的分数，有 6 种方法：
- [9,4,1,7] 最高分和最低分之间的差值是 9 - 4 = 5
- [9,4,1,7] 最高分和最低分之间的差值是 9 - 1 = 8
- [9,4,1,7] 最高分和最低分之间的差值是 9 - 7 = 2
- [9,4,1,7] 最高分和最低分之间的差值是 4 - 1 = 3
- [9,4,1,7] 最高分和最低分之间的差值是 7 - 4 = 3
- [9,4,1,7] 最高分和最低分之间的差值是 7 - 1 = 6
可能的最小差值是 2
```

# 题解

## 排序

对 nums 升序排序，遍历该数组，计算 k 长度区间内的最大值和最小值的差值，取其最小结果即可。

加入当前整数是 `nums[i]`，那么最大值就是 `nums[i + k -1]`，差值就是 `nums[i + k -1] - nums[i]`。

```ts
function minimumDifference(nums: number[], k: number): number {
  nums.sort((a, b) => a - b);
  let ans = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < nums.length - k + 1; i++) {
    ans = Math.min(ans, nums[i + k - 1] - nums[i]);
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int minimumDifference(vector<int> &nums, int k)
    {
        sort(nums.begin(), nums.end());
        int ans = INT_MAX;
        for (int i = 0; i < nums.size() - k + 1; i++)
        {
            ans = min(ans, nums[i + k - 1] - nums[i]);
        }
        return ans;
    }
};
```

```cpp
void bubbleSort(int *nums, int numsSize)
{
    int i, j, temp, isSorted;
    for (i = 0; i < numsSize - 1; i++)
    {
        isSorted = 1;
        for (j = 0; j < numsSize - 1 - i; j++)
        {
            if (nums[j] > nums[j + 1])
            {
                temp = nums[j];
                nums[j] = nums[j + 1];
                nums[j + 1] = temp;
                isSorted = 0;
            }
        }
        if (isSorted)
        {
            return;
        }
    }
}

int minimumDifference(int *nums, int numsSize, int k)
{
    bubbleSort(nums, numsSize);
    int ans = INT_MAX;
    for (int i = 0; i < numsSize - k + 1; i++)
    {
        if (ans > nums[i + k - 1] - nums[i])
        {
            ans = nums[i + k - 1] - nums[i];
        }
    }
    return ans;
}
```
