/*
 * @lc app=leetcode.cn id=1984 lang=c
 *
 * [1984] 学生分数的最小差值
 */

// @lc code=start

void bubbleSort(int *nums, int numsSize)
{
    int i, j, temp, isSorted;
    for (i = 0; i < numsSize - 1; i++)
    {
        isSorted = 1;
        for (j = 0; j < numsSize - 1 - i; j++)
        {
            if (nums[j] > nums[j + 1])
            {
                temp = nums[j];
                nums[j] = nums[j + 1];
                nums[j + 1] = temp;
                isSorted = 0;
            }
        }
        if (isSorted)
        {
            return;
        }
    }
}

int minimumDifference(int *nums, int numsSize, int k)
{
    bubbleSort(nums, numsSize);
    int ans = INT_MAX;
    for (int i = 0; i < numsSize - k + 1; i++)
    {
        if (ans > nums[i + k - 1] - nums[i])
        {
            ans = nums[i + k - 1] - nums[i];
        }
    }
    return ans;
}
// @lc code=end
