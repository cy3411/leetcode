# 题目

城市用一个 **双向连通** 图表示，图中有 `n` 个节点，从 `1` 到 `n` 编号（包含 `1` 和 `n`）。图中的边用一个二维整数数组 `edges` 表示，其中每个 `edges[i] = [ui, vi]` 表示一条节点 `ui` 和节点 `vi` 之间的双向连通边。每组节点对由 **最多一条** 边连通，顶点不存在连接到自身的边。穿过任意一条边的时间是 `time` 分钟。

每个节点都有一个交通信号灯，每 `change` 分钟改变一次，从绿色变成红色，再由红色变成绿色，循环往复。所有信号灯都 **同时** 改变。你可以在 **任何时候** 进入某个节点，但是 **只能** 在节点 信号灯是绿色时 才能离开。如果信号灯是 **绿色** ，你 **不能** 在节点等待，必须离开。

第二小的值 是 **严格大于** 最小值的所有值中最小的值。

- 例如，`[2, 3, 4]` 中第二小的值是 `3` ，而 `[2, 2, 4]` 中第二小的值是 `4` 。

给你 `n`、`edges`、`time` 和 `change` ，返回从节点 `1` 到节点 `n` 需要的 **第二短时间** 。

注意：

- 你可以 **任意次** 穿过任意顶点，包括 `1` 和 `n` 。
- 你可以假设在 **启程时** ，所有信号灯刚刚变成 **绿色** 。

提示：

- $\color{burlywood}2 \leq n \leq 10^4$
- $\color{burlywood}n - 1 \leq edges.length \leq min(2 * 10^4, n * (n - 1) / 2)$
- $\color{burlywood}edges[i].length == 2$
- $\color{burlywood}1 \leq ui, vi \leq n$
- $\color{burlywood}ui \not= vi$
- 不含重复边
- 每个节点都可以从其他节点直接或者间接到达
- $1 \leq time, change \leq 10^3$

# 示例

[![7T7qG6.png](https://s4.ax1x.com/2022/01/24/7T7qG6.png)](https://imgtu.com/i/7T7qG6)

```
输入：n = 5, edges = [[1,2],[1,3],[1,4],[3,4],[4,5]], time = 3, change = 5
输出：13
解释：
上图中的蓝色路径是最短时间路径。
花费的时间是：
- 从节点 1 开始，总花费时间=0
- 1 -> 4：3 分钟，总花费时间=3
- 4 -> 5：3 分钟，总花费时间=6
因此需要的最小时间是 6 分钟。

上图中的红色路径是第二短时间路径。
- 从节点 1 开始，总花费时间=0
- 1 -> 3：3 分钟，总花费时间=3
- 3 -> 4：3 分钟，总花费时间=6
- 在节点 4 等待 4 分钟，总花费时间=10
- 4 -> 5：3 分钟，总花费时间=13
因此第二短时间是 13 分钟。
```

# 题解

## 广度优先搜索

搜索最短路径，可以使用广度优先搜索。

题目要求得出次短路径的时间，我们可以在搜索最短路径的同时，维护次短路径，就可以得到次短路径的步数。

最后通过次短路径的步数，可以得到次短路径的时间。

每次经过节点的等待时间可通过如下计算：

$$
    T_{wait} = \begin{cases}
        0, & t_i \ mod \ (2*change) \in [0,change) \\
        (2*change) - (t_{i} \ \mod \ (2*change)) & t_i \ mod \ (2*change) \in [change, 2*change)
    \end{cases}


$$

```ts
function secondMinimum(n: number, edges: number[][], time: number, change: number): number {
  // 建模
  const graph: number[][] = new Array(n + 1).fill(0).map(() => []);
  for (const [u, v] of edges) {
    graph[u].push(v);
    graph[v].push(u);
  }

  // 计算最短路径和次短路径
  // path[i][0]表示最短路径，path[i][1]表示次短路径
  const path = new Array(n + 1).fill(0).map(() => new Array(2).fill(Number.POSITIVE_INFINITY));
  path[1][0] = 0;
  // 队列，存入[节点，距离]
  const queue: number[][] = [[1, 0]];
  while (path[n][1] === Number.POSITIVE_INFINITY) {
    const [cur, len] = queue.shift();
    for (const next of graph[cur]) {
      if (len + 1 < path[next][0]) {
        path[next][0] = len + 1;
        queue.push([next, len + 1]);
      } else if (len + 1 > path[next][0] && len + 1 < path[next][1]) {
        path[next][1] = len + 1;
        queue.push([next, len + 1]);
      }
    }
    console.log(queue);
  }

  // 计算次短路径所需时间
  let ans = 0;
  for (let i = 0; i < path[n][1]; i++) {
    if (ans % (2 * change) >= change) {
      ans += 2 * change - (ans % (2 * change));
    }
    ans += time;
  }
  return ans;
}
```
