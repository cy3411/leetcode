# 题目

给你一个数组 `nums` 。数组「动态和」的计算公式为：`runningSum[i] = sum(nums[0]…nums[i])` 。

请返回 `nums` 的动态和。

提示：

- `1 <= nums.length <= 1000`
- `-10^6 <= nums[i] <= 10^6`

# 示例

```
输入：nums = [1,2,3,4]
输出：[1,3,6,10]
解释：动态和计算过程为 [1, 1+2, 1+2+3, 1+2+3+4] 。
```

# 题解

## 前缀和

```ts
function runningSum(nums: number[]): number[] {
  const sum: number[] = [];
  const m = nums.length;
  sum[0] = nums[0];
  for (let i = 1; i < m; i++) {
    sum[i] = nums[i] + sum[i - 1];
  }

  return sum;
}
```
