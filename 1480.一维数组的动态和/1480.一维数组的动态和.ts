/*
 * @lc app=leetcode.cn id=1480 lang=typescript
 *
 * [1480] 一维数组的动态和
 */

// @lc code=start
function runningSum(nums: number[]): number[] {
  const sum: number[] = [];
  const m = nums.length;
  sum[0] = nums[0];
  for (let i = 1; i < m; i++) {
    sum[i] = nums[i] + sum[i - 1];
  }

  return sum;
}
// @lc code=end
