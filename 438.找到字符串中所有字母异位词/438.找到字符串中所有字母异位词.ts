/*
 * @lc app=leetcode.cn id=438 lang=typescript
 *
 * [438] 找到字符串中所有字母异位词
 */

// @lc code=start
function findAnagrams(s: string, p: string): number[] {
  const sLen = s.length;
  const pLen = p.length;
  if (sLen < pLen) return [];

  const sCount = new Array(26).fill(0);
  const pCount = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  const ans = [];

  // 统计p中每个字母出现的次数
  for (let i = 0; i < pLen; i++) {
    pCount[p.charCodeAt(i) - base]++;
  }

  // 统计s中每个字母出现的次数
  for (let i = 0; i < sLen; i++) {
    sCount[s[i].charCodeAt(0) - base]++;
    // 保持窗口大小为p的长度
    if (i >= pLen) {
      sCount[s[i - pLen].charCodeAt(0) - base]--;
    }
    // 如果s中的字母出现的次数与p中的字母出现的次数相等，则说明s中的字母与p中的字母是异位词
    if (sCount.join('') === pCount.join('')) {
      ans.push(i - pLen + 1);
    }
  }

  return ans;
}
// @lc code=end
