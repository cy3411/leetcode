# 题目

给定两个字符串 `s` 和 `p`，找到 `s` 中所有 `p` 的 **异位词** 的子串，返回这些子串的起始索引。不考虑答案输出的顺序。

**异位词** 指由相同字母重排列形成的字符串（包括相同的字符串）。

提示:

- $1 \leq s.length, p.length \leq 3 * 10^4$
- `s` 和 `p` 仅包含小写字母

# 示例

```
输入: s = "cbaebabacd", p = "abc"
输出: [0,6]
解释:
起始索引等于 0 的子串是 "cba", 它是 "abc" 的异位词。
起始索引等于 6 的子串是 "bac", 它是 "abc" 的异位词。
```

```
输入: s = "abab", p = "ab"
输出: [0,1,2]
解释:
起始索引等于 0 的子串是 "ab", 它是 "ab" 的异位词。
起始索引等于 1 的子串是 "ba", 它是 "ab" 的异位词。
起始索引等于 2 的子串是 "ab", 它是 "ab" 的异位词。
```

# 题解

利用哈希表统计 p 中字符出现的次数，然后遍历 s 中的字符，如果 s 中的字符在 p 中出现的次数和哈希表中的次数相同，则记录索引。

遍历 s 的时候，使用滑动窗口维护需要比较的字符串区间。

## 哈希表+滑动窗口

```ts
function findAnagrams(s: string, p: string): number[] {
  const sLen = s.length;
  const pLen = p.length;
  if (sLen < pLen) return [];

  const sCount = new Array(26).fill(0);
  const pCount = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  const ans = [];

  // 统计p中每个字母出现的次数
  for (let i = 0; i < pLen; i++) {
    pCount[p.charCodeAt(i) - base]++;
  }

  // 统计s中每个字母出现的次数
  for (let i = 0; i < sLen; i++) {
    sCount[s[i].charCodeAt(0) - base]++;
    // 保持窗口大小为p的长度
    if (i >= pLen) {
      sCount[s[i - pLen].charCodeAt(0) - base]--;
    }
    // 如果s中的字母出现的次数与p中的字母出现的次数相等，则说明s中的字母与p中的字母是异位词
    if (sCount.join('') === pCount.join('')) {
      ans.push(i - pLen + 1);
    }
  }

  return ans;
}
```
