# 题目

给你一个整数数组 `nums` ，请你将数组按照每个值的频率 **升序** 排序。如果有多个值的频率相同，请你按照数值本身将它们 **降序** 排序。

请你返回排序后的数组。

提示：

- $1 \leq nums.length \leq 100$
- $-100 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [1,1,2,2,2,3]
输出：[3,1,1,2,2,2]
解释：'3' 频率为 1，'1' 频率为 2，'2' 频率为 3 。
```

```
输入：nums = [2,3,1,3,2]
输出：[1,3,3,2,2]
解释：'2' 和 '3' 频率都为 2 ，所以它们之间按照数值本身降序排序。
```

# 题解

## 自定义排序

使用哈希表保存每个数字出现的频率，然后按照题意自定义排序规则。

```js
function frequencySort(nums: number[]): number[] {
  const mp = new Map<number, number>();
  for (const x of nums) {
    mp.set(x, (mp.get(x) ?? 0) + 1);
  }

  const ans = [...nums];

  ans.sort((a: number, b: number): number => {
    if (mp.get(a) === mp.get(b)) {
      return b - a;
    }
    return mp.get(a)! - mp.get(b)!;
  });

  return ans;
}
```

```cpp
class Solution {
public:
    vector<int> frequencySort(vector<int> &nums) {
        unordered_map<int, int> mp;
        for (auto x : nums) {
            mp[x]++;
        }
        sort(nums.begin(), nums.end(), [&](const int a, const int b) {
            if (mp[a] == mp[b]) {
                return a > b;
            }
            return mp[a] < mp[b];
        });
        return nums;
    }
};
```
