/*
 * @lc app=leetcode.cn id=1636 lang=typescript
 *
 * [1636] 按照频率将数组升序排序
 */

// @lc code=start
function frequencySort(nums: number[]): number[] {
  const mp = new Map<number, number>();
  for (const x of nums) {
    mp.set(x, (mp.get(x) ?? 0) + 1);
  }

  const ans = [...nums];

  ans.sort((a: number, b: number): number => {
    if (mp.get(a) === mp.get(b)) {
      return b - a;
    }
    return mp.get(a)! - mp.get(b)!;
  });

  return ans;
}
// @lc code=end
