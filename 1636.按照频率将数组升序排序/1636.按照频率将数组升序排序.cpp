/*
 * @lc app=leetcode.cn id=1636 lang=cpp
 *
 * [1636] 按照频率将数组升序排序
 */

// @lc code=start
class Solution {
public:
    vector<int> frequencySort(vector<int> &nums) {
        unordered_map<int, int> mp;
        for (auto x : nums) {
            mp[x]++;
        }
        sort(nums.begin(), nums.end(), [&](const int a, const int b) {
            if (mp[a] == mp[b]) {
                return a > b;
            }
            return mp[a] < mp[b];
        });
        return nums;
    }
};
// @lc code=end
