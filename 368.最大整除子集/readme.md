# 题目
给你一个由 无重复 正整数组成的集合 nums ，请你找出并返回其中最大的整除子集 answer ，子集中每一元素对 `(answer[i], answer[j])` 都应当满足：  
+ `answer[i] % answer[j] == 0` ，或
+ `answer[j] % answer[i] == 0`
  
如果存在多个有效解子集，返回其中任何一个均可。

提示：
+ 1 <= nums.length <= 1000
+ 1 <= nums[i] <= 2 * 109
+ nums 中的所有整数 互不相同

# 示例
```
输入：nums = [1,2,3]
输出：[1,2]
解释：[1,3] 也会被视为正确答案。
```

# 题解
整除关系具有传递性，比如`a/b`，而`b/c`，`a/c`也可以整数。

所以我们可以考虑从小的整除子集扩大到更大的整除子集。

将`nums`排序后，获取最长整除子集的个数，然后逆推出结果。

## 动态规划
**状态**  
定义`dp[i]`为包含当前元素，整除子集的元素个数。

**选择**
$$
dp[i] = max(dp[i], dp[j]+1)\text{, 满足i>0,j<i}
$$

**BaseCase**  
`dp[0...i]=1`，每个元素子集本上就是一个整除子集

定义`preIdxs[i]`记录当前`i`位置元素和前面哪个位置可以形成最大整数子集，利用这个逆推出结果数组。

```js
function largestDivisibleSubset(nums: number[]): number[] {
  const size = nums.length;
  const dp = new Array(size).fill(1);
  const preIdxs = new Array(size).fill(-1);

  nums.sort((a, b) => a - b);
  // 子集的最大长度
  let max = 1;
  // 子集最后一个元素的下标
  let preIdx = 0;

  for (let i = 1; i < size; i++) {
    for (let j = 0; j < i; j++) {
      // 保证是更长的子序列长度
      if (nums[i] % nums[j] === 0 && dp[j] + 1 > dp[i]) {
        dp[i] = dp[j] + 1;
        preIdxs[i] = j;
      }
    }
    if (dp[i] > max) {
      max = dp[i];
      preIdx = i;
    }
  }
  let result: number[] = [];
  // 通过记录的preIdx，逆推出结果
  while (preIdx !== -1) {
    result.push(nums[preIdx]);
    preIdx = preIdxs[preIdx];
  }

  return result;
}
```