# 题目

输入两个链表，找出它们的第一个公共节点。

如下面的两个链表：
[![28haHs.md.png](https://z3.ax1x.com/2021/06/04/28haHs.md.png)](https://imgtu.com/i/28haHs)
在节点 c1 开始相交。

# 示例

[![WUbebQ.md.png](https://z3.ax1x.com/2021/07/21/WUbebQ.md.png)](https://imgtu.com/i/WUbebQ)

```
输入：intersectVal = 8, listA = [4,1,8,4,5], listB = [5,0,1,8,4,5], skipA = 2, skipB = 3
输出：Reference of the node with value = 8
输入解释：相交节点的值为 8 （注意，如果两个列表相交则不能为 0）。从各自的表头开始算起，链表 A 为 [4,1,8,4,5]，链表 B 为 [5,0,1,8,4,5]。在 A 中，相交节点前有 2 个节点；在 B 中，相交节点前有 3 个节点。
```

# 题解

## 哈希表

遍历 headA，用哈希表记录访问过的节点。然后遍历 headB，查看哈希表中是否有记录，第一次出现的记录就是第一个相交节点。

```js
var getIntersectionNode = function (headA, headB) {
  const hash = new Map();
  let pre = headA;
  // 记录访问过的节点
  while (pre !== null) {
    hash.set(pre, 1);
    pre = pre.next;
  }
  pre = headB;
  let ans = null;
  // 第一个出现在哈希表中的节点就一个第一个相交节点
  while (pre !== null) {
    if (hash.get(pre)) {
      ans = pre;
      break;
    }
    pre = pre.next;
  }
  return ans;
};
```
