// @algorithm @lc id=100326 lang=javascript
// @title liang-ge-lian-biao-de-di-yi-ge-gong-gong-jie-dian-lcof
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
var getIntersectionNode = function (headA, headB) {
  const hash = new Map();
  let pre = headA;
  // 记录访问过的节点
  while (pre !== null) {
    hash.set(pre, 1);
    pre = pre.next;
  }
  pre = headB;
  let ans = null;
  // 第一个出现在哈希表中的节点就一个第一个相交节点
  while (pre !== null) {
    if (hash.get(pre)) {
      ans = pre;
      break;
    }
    pre = pre.next;
  }
  return ans;
};
