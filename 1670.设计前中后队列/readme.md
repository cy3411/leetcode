# 题目
请你设计一个队列，支持在前，中，后三个位置的 push 和 pop 操作。

请你完成 FrontMiddleBack 类：

+ FrontMiddleBack() 初始化队列。
+ void pushFront(int val) 将 val 添加到队列的 最前面 。
+ void pushMiddle(int val) 将 val 添加到队列的 正中间 。
+ void pushBack(int val) 将 val 添加到队里的 最后面 。
+ int popFront() 将 最前面 的元素从队列中删除并返回值，如果删除之前队列为空，那么返回 -1 。
+ int popMiddle() 将 正中间 的元素从队列中删除并返回值，如果删除之前队列为空，那么返回 -1 。
+ int popBack() 将 最后面 的元素从队列中删除并返回值，如果删除之前队列为空，那么返回 -1 。


请注意当有 两个 中间位置的时候，选择靠前面的位置进行操作。比方说：

+ 将 6 添加到 [1, 2, 3, 4, 5] 的中间位置，结果数组为 [1, 2, 6, 3, 4, 5] 。
+ 从 [1, 2, 3, 4, 5, 6] 的中间位置弹出元素，返回 3 ，数组变为 [1, 2, 4, 5, 6] 。

提示：

+ 1 <= val <= 109
+ 最多调用 1000 次 pushFront， pushMiddle， pushBack， popFront， popMiddle 和 popBack 。

# 示例
```
输入：
["FrontMiddleBackQueue", "pushFront", "pushBack", "pushMiddle", "pushMiddle", "popFront", "popMiddle", "popMiddle", "popBack", "popFront"]
[[], [1], [2], [3], [4], [], [], [], [], []]
输出：
[null, null, null, null, null, 1, 3, 4, 2, -1]

解释：
FrontMiddleBackQueue q = new FrontMiddleBackQueue();
q.pushFront(1);   // [1]
q.pushBack(2);    // [1, 2]
q.pushMiddle(3);  // [1, 3, 2]
q.pushMiddle(4);  // [1, 4, 3, 2]
q.popFront();     // 返回 1 -> [4, 3, 2]
q.popMiddle();    // 返回 3 -> [4, 2]
q.popMiddle();    // 返回 4 -> [2]
q.popBack();      // 返回 2 -> []
q.popFront();     // 返回 -1 -> [] （队列为空）
```

# 方法
这道题的设计思路，对一个队列在第一个位置增删数据，最后一个位置增删数据，中间位置增删数据。
使用2个deque，进行增删操作，中间位置的在左边或者右边队列都可以。
```js
var FrontMiddleBackQueue = function () {
  // 两个双端队列，中间数据在左边，左边永远比右边多一个数据
  this.left = new Deque();
  this.right = new Deque();
};

/**
 * @return {void}
 */
FrontMiddleBackQueue.prototype.balance = function () {
  if (this.left.size - this.right.size === 2) {
    this.right.insertFront(this.left.deleteLast());
  } else if (this.right.size > this.left.size) {
    this.left.insertLast(this.right.deleteFront());
  }
};
/**
 * @param {number} val
 * @return {void}
 */
FrontMiddleBackQueue.prototype.pushFront = function (val) {
  this.left.insertFront(val);
  this.balance();
};

/**
 * @param {number} val
 * @return {void}
 */
FrontMiddleBackQueue.prototype.pushMiddle = function (val) {
  if (this.left.size === this.right.size + 1) {
    this.right.insertFront(this.left.getRear());
    this.left.deleteLast();
  }
  this.left.insertLast(val);
  this.balance();
};

/**
 * @param {number} val
 * @return {void}
 */
FrontMiddleBackQueue.prototype.pushBack = function (val) {
  this.right.insertLast(val);
  this.balance();
};

/**
 * @return {number}
 */
FrontMiddleBackQueue.prototype.popFront = function () {
  if (this.left.size === 0) return -1;
  let val = this.left.getFront();
  this.left.deleteFront();
  this.balance();
  return val;
};

/**
 * @return {number}
 */
FrontMiddleBackQueue.prototype.popMiddle = function () {
  if (this.left.size === 0) return -1;
  let val = this.left.getRear();
  this.left.deleteLast();
  this.balance();
  return val;
};

/**
 * @return {number}
 */
FrontMiddleBackQueue.prototype.popBack = function () {
  if (this.left.size === 0 && this.right.size === 0) return -1;
  let val = this.right.size ? this.right.deleteLast() : this.left.deleteLast();
  this.balance();
  return val;
};

// 链表节点
class Node {
  constructor(val = null, next = null, prev = null) {
    this.val = val;
    this.next = next;
    this.prev = prev;
  }
}

// 双端队列
class Deque {
  constructor() {
    this.size = 0;
    this.head = new Node('head');
    this.tail = new Node('tail');
    this.head.next = this.tail;
    this.tail.prev = this.head;
  }

  insertFront(value) {
    let node = new Node(value);
    let next = this.head.next;
    node.next = this.head.next;
    node.prev = this.head;
    this.head.next = node;
    next.prev = node;
    this.size++;
  }
  insertLast(value) {
    let node = new Node(value);
    let prev = this.tail.prev;
    node.prev = prev;
    node.next = this.tail;
    this.tail.prev = node;
    prev.next = node;
    this.size++;
  }
  deleteFront() {
    if (this.isEmpty()) return false;
    let val = this.head.next.val;
    this.head.next = this.head.next.next;
    this.head.next.prev = this.head;
    this.size--;
    return val;
  }
  deleteLast() {
    if (this.isEmpty()) return false;
    let val = this.tail.prev.val;
    this.tail.prev = this.tail.prev.prev;
    this.tail.prev.next = this.tail;
    this.size--;
    return val;
  }
  getFront() {
    if (this.isEmpty()) return -1;
    return this.head.next.val;
  }
  getRear() {
    if (this.isEmpty()) return -1;
    return this.tail.prev.val;
  }
  isEmpty() {
    return this.size === 0;
  }
}
```