# 题目

设计一个算法，找出二叉搜索树中指定节点的“下一个”节点（也即中序后继）。

如果指定节点没有对应的“下一个”节点，则返回 null。

# 示例

```
输入: root = [2,1,3], p = 1

  2
 / \
1   3

输出: 2
```

# 题解

## 中序遍历

中序遍历二叉树，定义 pre 记录上一个结点，当 pre 和 p 相同时，当前遍历到的结点就是后继结点。

```js
var inorderSuccessor = function (root, p) {
  const inorder = (node) => {
    if (node === null) return;
    inorder(node.left);
    // 当前一个结点和p相同，当前结点就是后继结点
    if (pre === p) {
      ans = node;
    }
    pre = node;
    inorder(node.right);
    return;
  };
  let pre = null;
  let ans = null;
  inorder(root);
  return ans;
};
```

```cpp
class Solution {
public:
    TreeNode *inorderSuccessor(TreeNode *root, TreeNode *p) {
        if (root == NULL) return NULL;
        if (root->val <= p->val) {
            return inorderSuccessor(root->right, p);
        } else {
            TreeNode *left = inorderSuccessor(root->left, p);
            return left == NULL ? root : left;
        }
    }
};
```
