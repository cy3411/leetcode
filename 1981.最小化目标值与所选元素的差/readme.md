# 题目

给你一个大小为 `m x n` 的整数矩阵 `mat` 和一个整数 `target` 。

从矩阵的 **每一行** 中选择一个整数，你的目标是 **最小化** 所有选中元素之 **和** 与目标值 `target` 的 **绝对差** 。

返回 **最小的绝对差** 。

`a` 和 `b` 两数字的 **绝对差** 是 `a - b` 的绝对值。

提示：

- $m \equiv mat.length$
- $n \equiv mat[i].length$
- $1 \leq m, n \leq 70$
- $1 \leq mat[i][j] \leq 70$
- $1 \leq target \leq 800$

# 示例

[![b4zSkF.png](https://s1.ax1x.com/2022/03/10/b4zSkF.png)](https://imgtu.com/i/b4zSkF)

```
输入：mat = [[1,2,3],[4,5,6],[7,8,9]], target = 13
输出：0
解释：一种可能的最优选择方案是：
- 第一行选出 1
- 第二行选出 5
- 第三行选出 7
所选元素的和是 13 ，等于目标值，所以绝对差是 0 。
```

# 题解

## 哈希表

利用两个哈希表交替存储上一行和当前行的计算结果，当前行的计算结果就是遍历当前行所有元素，然后加上上一行每个元素的和。当所有的行都计算完以后，取最后一行哈希表中的和 `target` 绝对差最小的元素。

正常计算累加和的时候，会有重复的元素插入哈希表，有些样例会导致超时。

计算当前行的结果时，遍历前 i 行(包括 i 行)所有最小值的和到最大值的和的区间，然后在上一个计算结果中能否找到 当前遍历到的数字和当前行元素的差值，找到的话，就将 `j` 记录到哈希表中，跳出计算当前数字的循环。这样就保证每个数字只会被计算一次。

```ts
function minimizeTheDifference(mat: number[][], target: number): number {
  const m = mat.length;
  const maps: Set<number>[] = [new Set(), new Set()];
  // 初始化
  let sum = Number.MIN_VALUE;
  for (const num of mat[0]) {
    maps[0].add(num);
    sum = Math.max(sum, num);
  }
  // 遍历
  for (let i = 1; i < m; i++) {
    let idx = i % 2;
    let preIdx = (i - 1) % 2;
    // 每次清空当前哈希表，将新的结果通过上一个哈希表重新计算获得
    maps[idx].clear();
    // 将当前行和之前所有行的最大值累加，做为遍历的边界
    sum += Math.max(...mat[i]);
    // 遍历，看看当前数字能否加入到当前行的计算结果中
    // 遍历区间最小是每行都是1的情况，最大就是每行的最大值累加
    for (let j = i + 1; j <= sum; j++) {
      for (const num of mat[i]) {
        // 减少重复插入
        if (maps[preIdx].has(j - num)) {
          maps[idx].add(j);
          break;
        }
      }
    }
  }
  // 找到最小差值
  let ans = Number.MAX_VALUE;
  for (const num of maps[(m - 1) % 2]) {
    ans = Math.min(ans, Math.abs(num - target));
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int minimizeTheDifference(vector<vector<int>> &mat, int target)
    {
        int m = mat.size(), n = mat[0].size(), sum = 0;
        unordered_set<int> s[2];
        for (auto num : mat[0])
        {
            s[0].insert(num);
            sum = max(sum, num);
        }
        for (int i = 1; i < m; i++)
        {
            int maxNum = 0;
            for (auto num : mat[i])
            {
                maxNum = max(maxNum, num);
            }
            sum += maxNum;

            int idx = i % 2, preIdx = (i - 1) % 2;
            s[idx].clear();

            for (int j = i + 1; j <= sum; j++)
            {
                for (auto num : mat[i])
                {
                    if (s[preIdx].count(j - num) == 0)
                        continue;
                    s[idx].insert(j);
                    break;
                }
            }
        }
        int ans = __INT_MAX__;
        for (auto num : s[(m - 1) % 2])
        {
            ans = min(ans, abs(num - target));
        }
        return ans;
    }
};
```
