# 题目

给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。

你可以按 任何顺序 返回答案。

提示：

- 1 <= n <= 20
- 1 <= k <= n

# 示例

```
输入：n = 4, k = 2
输出：
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]
```

```
输入：n = 1, k = 1
输出：[[1]]
```

# 题解

## 深度优先搜索

深度优先遍历每一种可能，将符合题意的结果放入答案数组中。

```ts
function combine(n: number, k: number): number[][] {
  const ans: number[][] = [];
  const dfs = (num: number, count: number, buff: number[]) => {
    if (count === k) {
      ans.push([...buff]);
      return;
    }
    // 剩下的数字数量不够补充剩下的位置
    if (n - num + 1 < k - count) return;
    buff[count] = num;
    // 选择当前数字
    dfs(num + 1, count + 1, buff);
    // 不选择当前数字
    dfs(num + 1, count, buff);
  };
  dfs(1, 0, []);
  return ans;
}
```
