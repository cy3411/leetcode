# 题目

还记得童话《卖火柴的小女孩》吗？现在，你知道小女孩有多少根火柴，请找出一种能使用所有火柴拼成一个正方形的方法。不能折断火柴，可以把火柴连接起来，并且每根火柴都要用到。

输入为小女孩拥有火柴的数目，每根火柴用其长度表示。输出即为是否能用所有的火柴拼成正方形。

注意:

- 给定的火柴长度和在 0 到 10^9 之间。
- 火柴数组的长度不超过 15。

# 示例

```
输入: [1,1,2,2,2]
输出: true

解释: 能拼成一个边长为2的正方形，每边两根火柴。
```

# 题解

## 回溯

将 4 条边定义为 4 个容器，每个容器的容量相当于所有火柴的和除 4。

递归遍历每个元素，每个元素找到合适的盒子将火柴放进去，如果可以递归到最后，就表示找到一个有效解。

先将数组排序，从最多的火柴数开始遍历，这样可以方便判断边界问题。

```ts
function makesquare(matchsticks: number[]): boolean {
  const backtrack = (idx: number, arr: number[], matchsticks: number[]): boolean => {
    if (idx === -1) return true;
    for (let i = 0; i < 4; i++) {
      // 这条边放不下火柴
      if (arr[i] < matchsticks[idx]) continue;
      // 这条边能放下火柴或者这边的容量大于当前火柴数量加上最少的火柴数量
      if (arr[i] === matchsticks[idx] || arr[i] >= matchsticks[idx] + matchsticks[0]) {
        arr[i] -= matchsticks[idx];
        if (backtrack(idx - 1, arr, matchsticks)) return true;
        // 上面的选择无效，这里回溯回去，重新选择
        arr[i] += matchsticks[idx];
      }
    }
    return false;
  };
  let sum = 0;
  for (let n of matchsticks) {
    sum += n;
  }

  // 总数不够分成4条边
  if (sum % 4 !== 0) return false;
  matchsticks.sort((a, b) => a - b);
  const arr = new Array(4).fill(sum / 4);
  return backtrack(matchsticks.length - 1, arr, matchsticks);
}
```

```cpp
class Solution {
public:
    bool backtrack(int idx, vector<int> &matchsticks, vector<int> &sides) {
        if (idx == -1) return true;
        for (int i = 0; i < 4; i++) {
            if (sides[i] < matchsticks[idx]) continue;
            if (sides[i] == matchsticks[idx] ||
                sides[i] >= matchsticks[idx] + matchsticks[0]) {
                sides[i] -= matchsticks[idx];
                if (backtrack(idx - 1, matchsticks, sides)) return true;
                sides[i] += matchsticks[idx];
            }
        }
        return false;
    }
    bool makesquare(vector<int> &matchsticks) {
        int n = matchsticks.size(), sum = 0;

        for (auto x : matchsticks) {
            sum += x;
        }
        if (sum % 4 != 0) return false;

        vector<int> sides(4, sum / 4);
        sort(matchsticks.begin(), matchsticks.end());

        return backtrack(n - 1, matchsticks, sides);
    }
};
```
