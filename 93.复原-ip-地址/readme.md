# 题目

给定一个只包含数字的字符串，用以表示一个 IP 地址，返回所有可能从 s 获得的 有效 IP 地址 。你可以按任何顺序返回答案。

有效 IP 地址 正好由四个整数（每个整数位于 0 到 255 之间组成，且不能含有前导 0），整数之间用 '.' 分隔。

例如："0.1.2.201" 和 "192.168.1.1" 是 有效 IP 地址，但是 "0.011.255.245"、"192.168.1.312" 和 "192.168@1.1" 是 无效 IP 地址。

提示：

- 0 <= s.length <= 3000
- s 仅由数字组成

# 示例

```
输入：s = "25525511135"
输出：["255.255.11.135","255.255.111.35"]
```

# 题解

## 回溯

就是一个穷尽的问题，递归每一个可能性(当前第几个点，点后面的字符串开始的下标)。

需要注意的就是判断每次插入点之前和最后一组数字都需要判断数字是否合法性。

```ts
function restoreIpAddresses(s: string): string[] {
  const arr = s.split('');
  const ans = [];
  const backtrack = (k: number, idx: number) => {
    if (idx >= arr.length) return;
    // 3个点都插入完成了
    if (k === 4) {
      // 最后一组数字是否是0开头
      if (arr.length - idx > 1 && arr[idx] === '0') return;
      let num = 0;
      // 最后一组数字是否大于255
      for (let i = idx; i < arr.length; i++) {
        num = num * 10 + Number(arr[i]);
        if (num > 255) return;
      }
      // 更新答案
      ans.push(arr.join(''));
    }
    let num = 0;
    for (let i = idx; i < arr.length; i++) {
      num = num * 10 + Number(arr[i]);
      // 数字大于255，结束
      if (num > 255) return;
      // 数字第一位大于0，结束
      if (i - idx >= 1 && arr[idx] === '0') return;
      // 选择插入点
      arr.splice(i + 1, 0, '.');
      // 递归下一个点的位置
      backtrack(k + 1, i + 2);
      // 回溯，删除插入的点
      arr.splice(i + 1, 1);
    }
  };

  backtrack(1, 0);

  return ans;
}
```
