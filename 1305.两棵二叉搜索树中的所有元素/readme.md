# 题目

给你 root1 和 root2 这两棵二叉搜索树。

请你返回一个列表，其中包含 两棵树 中的所有整数并按 升序 排序。

提示：

- 每棵树最多有 5000 个节点。
- 每个节点的值在 [-10^5, 10^5] 之间。

# 示例

```
输入：root1 = [2,1,4], root2 = [1,0,3]
输出：[0,1,1,2,3,4]
```

# 题解

二叉搜索树的中序遍历就是一个递增序列。

取出两棵树的的递增序列，然后二路归并出结果。

```ts
function getAllElements(root1: TreeNode | null, root2: TreeNode | null): number[] {
  // 中序遍历，取出搜索树的递增序列
  const inorder = (root: TreeNode, result: number[]) => {
    if (root === null) return;
    inorder(root.left, result);
    result.push(root.val);
    inorder(root.right, result);
  };

  const result1 = [];
  inorder(root1, result1);
  const result2 = [];
  inorder(root2, result2);
  let i = 0;
  let j = 0;
  const ans = [];
  // 二路归并
  while (i < result1.length || j < result2.length) {
    if (j >= result2.length || (i < result1.length && result1[i] <= result2[j])) {
      ans.push(result1[i]);
      i++;
    } else {
      ans.push(result2[j]);
      j++;
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    void inorder(TreeNode *root, vector<int> &result)
    {
        if (root == NULL)
            return;
        inorder(root->left, result);
        result.push_back(root->val);
        inorder(root->right, result);
    }
    vector<int> getAllElements(TreeNode *root1, TreeNode *root2)
    {
        vector<int> result1, result2, ans;
        result1.clear(), result2.clear();
        inorder(root1, result1);
        inorder(root2, result2);

        int i = 0, j = 0;
        while (i < result1.size() || j < result2.size())
        {
            if (j >= result2.size() || (i < result1.size() && result1[i] <= result2[j]))
            {
                ans.push_back(result1[i]);
                i++;
            }
            else
            {
                ans.push_back(result2[j]);
                j++;
            }
        }

        return ans;
    }
};
```

```python
class Solution:
    def inorder(self, root: TreeNode) -> List[int]:
        if not root:
            return []
        return self.inorder(root.left) + [root.val] + self.inorder(root.right)

    def getAllElements(self, root1: TreeNode, root2: TreeNode) -> List[int]:
        result1 = self.inorder(root1)
        result2 = self.inorder(root2)
        i = 0
        j = 0
        result = []
        while (i < len(result1) or j < len(result2)):
            if (j >= len(result2) or (i < len(result1) and result1[i] <= result2[j])):
                result.append(result1[i])
                i += 1
            else:
                result.append(result2[j])
                j += 1

        return result
```
