/*
 * @lc app=leetcode.cn id=122 lang=typescript
 *
 * [122] 买卖股票的最佳时机 II
 */

// @lc code=start
function maxProfit(prices: number[]): number {
  const m = prices.length;
  let ans = 0;

  for (let i = 1; i < m; i++) {
    // 只要是上升的区间，就一直能获取利润
    if (prices[i] > prices[i - 1]) {
      ans += prices[i] - prices[i - 1];
    }
  }

  return ans;
}
// @lc code=end
