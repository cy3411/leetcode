# 题目

给定一个数组 `points` ，其中 $points[i] = [x_i, y_i]$ 表示 X-Y 平面上的一个点，如果这些点构成一个 **回旋镖** 则返回 `true` 。

回旋镖 定义为一组三个点，这些点 各不相同 且 不在一条直线上 。

提示：

- $points.length \equiv 3$
- $points[i].length \equiv 2$
- $0 \leq x_i, y_i \leq 100$

# 示例

```
输入：points = [[1,1],[2,3],[3,2]]
输出：true
```

```
输入：points = [[1,1],[2,2],[3,3]]
输出：false
```

# 题解

## 向量叉乘

计算从 point[0] 开始，分别指向 point[1] 和 point[2] 的向量 v1 和 v2。

如果向量 v1 和 v2 叉乘的结果不为 0 ，则表示这些点构成了一个回旋镖。

```ts
function isBoomerang(points: number[][]): boolean {
  const v1 = [points[1][0] - points[0][0], points[1][1] - points[0][1]];
  const v2 = [points[2][0] - points[0][0], points[2][1] - points[0][1]];
  return v1[0] * v2[1] - v1[1] * v2[0] !== 0;
}
```
