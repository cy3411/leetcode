/*
 * @lc app=leetcode.cn id=1656 lang=typescript
 *
 * [1656] 设计有序流
 */

// @lc code=start
class OrderedStream {
  streams: string[];
  ptr: number = 0;
  constructor(n: number) {
    this.streams = new Array(n);
  }

  insert(idKey: number, value: string): string[] {
    this.streams[idKey - 1] = value;
    let res: string[] = [];
    while (this.streams[this.ptr] !== void 0) {
      res.push(this.streams[this.ptr++]);
    }
    return res;
  }
}

/**
 * Your OrderedStream object will be instantiated and called as such:
 * var obj = new OrderedStream(n)
 * var param_1 = obj.insert(idKey,value)
 */
// @lc code=end
