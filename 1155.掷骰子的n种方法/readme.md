# 题目

这里有 `d` 个一样的骰子，每个骰子上都有 `f` 个面，分别标号为 `1, 2, ..., f`。

我们约定：掷骰子的得到总点数为各骰子面朝上的数字的总和。

如果需要掷出的总点数为 `target`，请你计算出有多少种不同的组合情况（所有的组合情况总共有 `f^d` 种），模 `10^9 + 7` 后返回。

提示：

- $\color{goldenrod}1 \leq d, f \leq 30$
- $\color{goldenrod}1 \leq target \leq 1000$

# 示例

```
输入：d = 1, f = 6, target = 3
输出：1
```

```
输入：d = 2, f = 6, target = 7
输出：6
```

# 题解

## 动态规划

**状态**

`dp[i][j]` 表示 `i` 个骰子，掷出的总点数为 `j` 的组合数。

**边界**

`dp[0][0] = 1`，表示掷出 `0` 点的情况下，只有一种组合。

**转移**

$$
    dp[i][j] = \sum_{k=1}^{f} dp[i-1][j-k]
$$

```ts
function numRollsToTarget(d: number, f: number, target: number): number {
  const mod = 1000000007;
  // dp[i][j] 表示掷骰子 i 个，目标值为 j 的方案数
  const dp: number[][] = new Array(d + 1).fill(0).map(() => new Array(target + 1).fill(0));
  // 边界条件，0个骰子，目标值为 0 的方案数为 1
  dp[0][0] = 1;

  for (let i = 1; i <= d; i++) {
    for (let j = 1; j <= target; j++) {
      for (let k = 1; k <= f; k++) {
        // 计算最后一个骰子需要投掷的点数
        if (j < k) break;
        dp[i][j] += dp[i - 1][j - k];
        dp[i][j] %= mod;
      }
    }
  }

  return dp[d][target];
}
```
