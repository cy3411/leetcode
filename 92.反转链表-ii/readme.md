# 题目
反转从位置 m 到 n 的链表。请使用一趟扫描完成反转。

说明:
1 ≤ m ≤ n ≤ 链表长度。

# 示例
```
输入: 1->2->3->4->5->NULL, m = 2, n = 4
输出: 1->4->3->2->5->NULL
```

# 方法
找出开始反转的位置，然后反转n次即可。
考虑头部节点可能会被反转，所以需要使用虚拟头。
## 递归
```js
/**
 * @param {ListNode} head
 * @param {number} left
 * @param {number} right
 * @return {ListNode}
 */
var reverseBetween = function (head, left, right) {
  if (head === null || head.next === null) {
    return head;
  }

  const reverseNode = (head, n) => {
    if (n === 1) {
      return head;
    }
    const tail = head.next;
    const p = reverseNode(head.next, n - 1);
    head.next = tail.next;
    tail.next = head;
    return p;
  };

  const dummp = new ListNode(0, head);
  const n = right - left + 1;
  let p = dummp;
  while (--left) {
    p = p.next;
  }
  p.next = reverseNode(p.next, n);

  return dummp.next;
};
```

## 迭代
```js
/**
 * @param {ListNode} head
 * @param {number} left
 * @param {number} right
 * @return {ListNode}
 */
var reverseBetween = function (head, left, right) {
  if (head === null || head.next === null) {
    return head;
  }

  const reverseNode = (head, n) => {
    let pre = null;
    let cur = head;
    while (n--) {
      let next = cur.next;
      cur.next = pre;
      pre = cur;
      cur = next;
    }
    head.next = cur;
    return pre;
  };

  const dummp = new ListNode(0, head);
  const n = right - left + 1;
  let p = dummp;
  while (--left) {
    p = p.next;
  }
  p.next = reverseNode(p.next, n);

  return dummp.next;
};
```
