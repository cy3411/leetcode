# 题目

在第一行我们写上一个 0。接下来的每一行，将前一行中的 0 替换为 01，1 替换为 10。

给定行数 N 和序数 K，返回第 N 行中第 K 个字符。（K 从 1 开始）

注意：

- N 的范围 [1, 30].
- K 的范围 [1, 2^(N-1)].

# 示例

```
输入: N = 1, K = 1
输出: 0

输入: N = 2, K = 1
输出: 0

输入: N = 2, K = 2
输出: 1

输入: N = 4, K = 5
输出: 1

解释:
第一行: 0
第二行: 01
第三行: 0110
第四行: 01101001

```

# 方法

每一行都是根据上一行而来，每次递增都是 2 的倍数。

我们可以将它看成一个二叉树。

通过观察可以看出，每一行的后半部分都是前半部分的反转，前半部分是 0，后半部分就是 1.

思路就是判断如果 K 小于等于 2N 次方的一半:

- 是，就直接取 N-1 的对应位置的值。`kthGrammar(N - 1, K)`
- 否，将值取反。`kthGrammar(N - 1, K - (1 << (N - 2))) ^ 1;`

```js
/**
 * @param {number} N
 * @param {number} K
 * @return {number}
 */
var kthGrammar = function (N, K) {
  // 右子树是左子树的反转
  if (N === 1) return 0;
  // K在父节点左子树上
  if (K <= 1 << (N - 2)) return kthGrammar(N - 1, K);
  // K在父节右子树
  return kthGrammar(N - 1, K - (1 << (N - 2))) ^ 1;
};
```

```js
function kthGrammar(n: number, k: number): number {
  if (n === 1) return 0;
  return (k & 1) ^ 1 ^ kthGrammar(n - 1, (k + 1) >> 1);
}
```
