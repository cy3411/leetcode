# 题目

存在一个由 n 个不同元素组成的整数数组 nums ，但你已经记不清具体内容。好在你还记得 nums 中的每一对相邻元素。

给你一个二维整数数组 adjacentPairs ，大小为 n - 1 ，其中每个 adjacentPairs[i] = [ui, vi] 表示元素 ui 和 vi 在 nums 中相邻。

题目数据保证所有由元素 nums[i] 和 nums[i+1] 组成的相邻元素对都存在于 adjacentPairs 中，存在形式可能是 [nums[i], nums[i+1]] ，也可能是 [nums[i+1], nums[i]] 。这些相邻元素对可以 按任意顺序 出现。

返回 原始数组 nums 。如果存在多种解答，返回 其中任意一个 即可。

提示：

- nums.length == n
- adjacentPairs.length == n - 1
- adjacentPairs[i].length == 2
- 2 <= n <= 105
- -105 <= nums[i], ui, vi <= 105
- 题目数据保证存在一些以 adjacentPairs 作为元素对的数组 nums

# 示例

```
输入：adjacentPairs = [[2,1],[3,4],[3,2]]
输出：[1,2,3,4]
解释：数组的所有相邻元素对都在 adjacentPairs 中。
特别要注意的是，adjacentPairs[i] 只表示两个元素相邻，并不保证其 左-右 顺序。
```

# 题解

## 哈希表

我们先将所有的点和它对应的边存入哈希表中，然后再开始构建整个数组。

起点或者终点，对应的边只有 1 个，遍历哈希找一个符合条件的作为起点。

从起点开始，后面的元素就是去哈希中找到对应的边，将未访问过的点放入结果。

```ts
function restoreArray(adjacentPairs: number[][]): number[] {
  // 将每个点和对应的边存入哈希
  const hash: Map<number, number[]> = new Map();
  for (const adj of adjacentPairs) {
    const [from, to] = adj;
    hash.has(from) ? hash.get(from).push(to) : hash.set(from, [to]);
    hash.has(to) ? hash.get(to).push(from) : hash.set(to, [from]);
  }

  const ans: number[] = new Array(adjacentPairs.length + 1);
  // 当前点只有1个边表示这是一个起点或者终点，我们初始化一个，方便构建整个数组
  for (const [e, adj] of hash.entries()) {
    // 代表起点
    if (adj.length === 1) {
      ans[0] = e;
      break;
    }
  }
  ans[1] = hash.get(ans[0])[0];

  // 每个点的下一个点就是边中未被访问的点
  for (let i = 2; i < ans.length; i++) {
    const adj = hash.get(ans[i - 1]);
    ans[i] = ans[i - 2] === adj[0] ? adj[1] : adj[0];
  }

  return ans;
}
```
