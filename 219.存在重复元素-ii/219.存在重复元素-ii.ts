/*
 * @lc app=leetcode.cn id=219 lang=typescript
 *
 * [219] 存在重复元素 II
 */

// @lc code=start
function containsNearbyDuplicate(nums: number[], k: number): boolean {
  const hash = new Map<number, number>();
  for (let i = 0; i < nums.length; i++) {
    const num = nums[i];
    if (hash.has(num)) {
      const pre = hash.get(num);
      // 如果当前元素和上一个元素的距离小于k，则返回true
      if (i - pre <= k) return true;
      // 更新pre
      hash.set(num, i);
    } else {
      // 新增pre
      hash.set(num, i);
    }
  }
  return false;
}
// @lc code=end
