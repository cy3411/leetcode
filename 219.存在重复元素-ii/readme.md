# 题目

给你一个整数数组 `nums` 和一个整数 `k` ，判断数组中是否存在两个 **不同的索引** `i` 和 `j` ，满足 `nums[i] == nums[j]` 且 $\color{burlywood}abs(i - j) \leq k$ 。如果存在，返回 `true` ；否则，返回 `false` 。

提示：

- $\color{burlywood}1 \leq nums.length \leq 10^5$
- $\color{burlywood}-10^9 \leq nums[i] \leq 10^9$
- $\color{burlywood}0 \leq k \leq 10^5$

# 示例

```
输入：nums = [1,2,3,1], k = 3
输出：true
```

```
输入：nums = [1,0,1,1], k = 1
输出：true
```

```
输入：nums = [1,2,3,1,2,3], k = 2
输出：false
```

# 题解

## 哈希表

使用哈希表记录每个数字出现时的索引，如果遍历过程中发现相同的数字，则比较索引的差值是否小于等于 `k` ，如果是，则返回 `true` 。

遍历结束，则返回 `false` 。

```ts
function containsNearbyDuplicate(nums: number[], k: number): boolean {
  const hash = new Map<number, number>();
  for (let i = 0; i < nums.length; i++) {
    const num = nums[i];
    if (hash.has(num)) {
      const pre = hash.get(num);
      // 如果当前元素和上一个元素的距离小于k，则返回true
      if (i - pre <= k) return true;
      // 更新pre
      hash.set(num, i);
    } else {
      // 新增pre
      hash.set(num, i);
    }
  }
  return false;
}
```

```cpp
class Solution
{
public:
    bool containsNearbyDuplicate(vector<int> &nums, int k)
    {
        unordered_map<int, int> m;

        for (int i = 0; i < nums.size(); i++)
        {
            if (m.find(nums[i]) != m.end() && i - m[nums[i]] <= k)
            {
                return true;
            }
            m[nums[i]] = i;
        }

        return false;
    }
};
```
