/*
 * @lc app=leetcode.cn id=665 lang=javascript
 *
 * [665] 非递减数列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var checkPossibility = function (nums) {
  const size = nums.length;
  let limit = 0;

  for (let i = 0; i < size - 1; i++) {
    const [x, y] = [nums[i], nums[i + 1]];
    if (x > y) {
      limit++;
      if (limit > 1) {
        return false;
      }
      if (i > 0 && y < nums[i - 1]) {
        nums[i + 1] = x;
      }
    }
  }

  return true;
};
// @lc code=end
