# 题目
给你一个整数数组 `bloomDay`，以及两个整数 `m` 和 `k` 。

现需要制作 `m` 束花。制作花束时，需要使用花园中 **相邻的** `k` 朵花 。

花园中有 `n` 朵花，第 `i` 朵花会在 `bloomDay[i]` 时盛开，恰好 可以用于 一束 花中。

请你返回从花园中摘 `m` 束花需要等待的最少的天数。如果不能摘到 `m` 束花则返回 `-1` 。

# 示例
```
输入：bloomDay = [1,10,3,10,2], m = 3, k = 1
输出：3
解释：让我们一起观察这三天的花开过程，x 表示花开，而 _ 表示花还未开。
现在需要制作 3 束花，每束只需要 1 朵。
1 天后：[x, _, _, _, _]   // 只能制作 1 束花
2 天后：[x, _, _, _, x]   // 只能制作 2 束花
3 天后：[x, _, x, _, x]   // 可以制作 3 束花，答案为 3
```
```
输入：bloomDay = [7,7,7,7,12,7,7], m = 2, k = 3
输出：12
解释：要制作 2 束花，每束需要 3 朵。
花园在 7 天后和 12 天后的情况如下：
7 天后：[x, x, x, x, _, x, x]
可以用前 3 朵盛开的花制作第一束花。但不能使用后 3 朵盛开的花，因为它们不相邻。
12 天后：[x, x, x, x, x, x, x]
显然，我们可以用不同的方式制作两束花。
```
# 题解
## 二分查找
`bloomDay` 数组给出了花朵开花的最小天数和最大天数，所以答案肯定在这个范围内。

我们查找范围内的天数，去检测是当前选择的天数是否可以制作花束，持续缩小天数范围，直到找到最小的天数。

```ts
interface Check<T> {
  (bloomDay: T[], days: T, m: T, k: T): boolean;
}

function minDays(bloomDay: number[], m: number, k: number): number {
  const size = bloomDay.length;
  // 花朵数量不够制作花束
  if (m * k > size) return -1;
  // 检测当前天数days是否可以制作满足条件的花束
  const check: Check<number> = (bloomDay, days, m, k): boolean => {
    let flowers = 0;
    let bouquet = 0;
    const size = bloomDay.length;
    for (let i = 0; i < size; i++) {
      if (bloomDay[i] <= days) {
        flowers++;
        if (flowers === k) {
          bouquet++;
          flowers = 0;
        }
      } else {
        flowers = 0;
      }
    }
    return bouquet >= m;
  };

  let low = Math.min(...bloomDay);
  let high = Math.max(...bloomDay);
  // 二分查找可以制作花束的最小天数
  while (low < high) {
    const mid = low + ((high - low) >> 1);
    if (check(bloomDay, mid, m, k)) {
      high = mid;
    } else {
      low = mid + 1;
    }
  }

  return low;
}
```
