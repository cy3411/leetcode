/*
 * @lc app=leetcode.cn id=941 lang=javascript
 *
 * [941] 有效的山脉数组
 */

// @lc code=start
/**
 * @param {number[]} A
 * @return {boolean}
 */
var validMountainArray = function (A) {
  const size = A.length;
  let i = 0;
  // 上山
  while (i + 1 < size && A[i] < A[i + 1]) {
    i++;
  }
  // 最高点出现在开始和结束位置，不是有效山峰
  if (i === 0 || i === size - 1) {
    return false;
  }
  // 下山
  while (i + 1 < size && A[i] > A[i + 1]) {
    i++;
  }
  // 判断最后下标是否扫描完毕
  return i === size - 1;
};
// @lc code=end

/**
 * 时间复杂度：O(N)，其中 N 是数组 A 的长度
 * 空间复杂度：O(1)
 */
