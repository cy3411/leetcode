# 题目

给定一个二叉搜索树 `root` 和一个目标结果 `k`，如果 BST 中存在两个元素且它们的和等于给定的目标结果，则返回 `true`。

提示:

- 二叉树的节点个数的范围是 $[1, 10^4]$
- $-10^4 \leq Node.val \leq 10^4$
- `root` 为二叉搜索树
- $-10^5 \leq k \leq 10^5$

# 示例

[![hY6Aw8.png](https://z3.ax1x.com/2021/08/30/hY6Aw8.png)](https://imgtu.com/i/hY6Aw8)

```
输入: root = [5,3,6,2,4,null,7], k = 9
输出: true
```

# 题解

二叉搜索树的中序遍历是单调递增的。

这样题目就转换成，在一个有序序列中，找到 2 数之和等于 k。

## 中序遍历

```ts
function findTarget(root: TreeNode | null, k: number): boolean {
  const nums = [];
  // 中序遍历，得到有序序列
  const inorder = (node: TreeNode | null) => {
    if (node === null) return;
    inorder(node.left);
    nums.push(node.val);
    inorder(node.right);
  };

  inorder(root);
  // 在一个有序序列中找到2数之和为k
  let l = 0;
  let r = nums.length - 1;
  while (l < r && nums[l] + nums[r] - k !== 0) {
    if (nums[l] + nums[r] < k) {
      l++;
    } else {
      r--;
    }
  }

  return l < r;
}
```

```cpp
class Solution
{
public:
    void inorder(TreeNode *root, vector<int> &nums)
    {
        if (root == nullptr)
        {
            return;
        }
        inorder(root->left, nums);
        nums.push_back(root->val);
        inorder(root->right, nums);
    }
    bool findTarget(TreeNode *root, int k)
    {
        vector<int> nums;
        inorder(root, nums);

        int l = 0, r = nums.size() - 1;
        while (l < r)
        {
            int sum = nums[l] + nums[r];
            if (sum == k)
            {
                return true;
            }
            else if (sum < k)
            {
                l++;
            }
            else
            {
                r--;
            }
        }

        return l < r;
    }
};
```

## 深度优先搜索

遍历树的每一个节点，并用哈希表去记录访问过的节点的值。

对于一个值为 x 的节点，我们只需要检查哈希表中是否有 k - x 的值即可。

```js
var findTarget = function (root, k) {
  const map = new Set();
  const dfs = (node, k) => {
    if (node === null) return false;
    if (map.has(k - node.val)) return true;
    map.add(node.val);
    return dfs(node.left, k) || dfs(node.right, k);
  };

  return dfs(root, k);
};
```
