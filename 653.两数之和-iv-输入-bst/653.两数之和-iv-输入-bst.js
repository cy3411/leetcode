/*
 * @lc app=leetcode.cn id=653 lang=javascript
 *
 * [653] 两数之和 IV - 输入 BST
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {boolean}
 */
var findTarget = function (root, k) {
  const map = new Set();
  const dfs = (node, k) => {
    if (node === null) return false;
    if (map.has(k - node.val)) return true;
    map.add(node.val);
    return dfs(node.left, k) || dfs(node.right, k);
  };

  return dfs(root, k);
};
// @lc code=end
