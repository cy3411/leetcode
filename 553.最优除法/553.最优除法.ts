/*
 * @lc app=leetcode.cn id=553 lang=typescript
 *
 * [553] 最优除法
 */

// @lc code=start
function optimalDivision(nums: number[]): string {
  const n = nums.length;
  if (n === 1) return `${nums[0]}`;
  if (n === 2) return `${nums[0]}/${nums[1]}`;

  const ans = [];
  ans.push(nums[0]);
  ans.push('/');
  ans.push('(');
  ans.push(nums[1]);
  for (let i = 2; i < n; i++) {
    ans.push('/');
    ans.push(nums[i]);
  }
  ans.push(')');

  return ans.join('');
}
// @lc code=end
