# 题目

给定一组正整数，相邻的整数之间将会进行浮点除法操作。例如， $[2,3,4] -> 2 / 3 / 4$ 。

但是，你可以在任意位置添加任意数目的括号，来改变算数的优先级。你需要找出怎么添加括号，才能得到最大的结果，并且返回相应的字符串格式的表达式。你的表达式**不应该含有冗余的括号**。

说明:

- 输入数组的长度在 `[1, 10]` 之间。
- 数组中每个元素的大小都在 `[2, 1000]` 之间。
- 每个测试用例**只有一个最优除法解**。

# 示例

```
输入: [1000,100,10,2]
输出: "1000/(100/10/2)"
解释:
1000/(100/10/2) = 1000/((100/10)/2) = 200
但是，以下加粗的括号 "1000/((100/10)/2)" 是冗余的，
因为他们并不影响操作的优先级，所以你需要返回 "1000/(100/10/2)"。

其他用例:
1000/(100/10)/2 = 50
1000/(100/(10/2)) = 50
1000/100/10/2 = 0.5
1000/100/(10/2) = 2
```

# 题解

## 数学

假设$\frac{x}{y}$为除法运算，`x` 为被除数, `y` 为除数。要想结果最大，应该使 `x` 尽可能的大，`y` 尽可能的小。

题目提供的数字都是正整数，所以$\frac{nums_0}{nums_1 \div nums_2 \div … \div nums_{n-1}}$是最优除法。

```ts
function optimalDivision(nums: number[]): string {
  const n = nums.length;
  if (n === 1) return `${nums[0]}`;
  if (n === 2) return `${nums[0]}/${nums[1]}`;

  const ans = [];
  ans.push(nums[0]);
  ans.push('/');
  ans.push('(');
  ans.push(nums[1]);
  for (let i = 2; i < n; i++) {
    ans.push('/');
    ans.push(nums[i]);
  }
  ans.push(')');

  return ans.join('');
}
```

```cpp
class Solution
{
public:
    string optimalDivision(vector<int> &nums)
    {
        int n = nums.size();
        if (n == 1)
        {
            return to_string(nums[0]);
        }
        if (n == 2)
        {
            return to_string(nums[0]) + "/" + to_string(nums[1]);
        }

        string ans = to_string(nums[0]) + "/(" + to_string(nums[1]);
        for (int i = 2; i < n; i++)
        {
            ans += "/" + to_string(nums[i]);
        }
        ans += ")";

        return ans;
    }
};
```
