/*
 * @lc app=leetcode.cn id=904 lang=typescript
 *
 * [904] 水果成篮
 */

// @lc code=start
function totalFruit(fruits: number[]): number {
  const n = fruits.length;
  // 记录元素的个数
  const map = new Map<number, number>();
  let left = 0;
  let ans = 0;
  for (let right = 0; right < n; right++) {
    map.set(fruits[right], (map.get(fruits[right]) ?? 0) + 1);
    // 个数超过2个，将左边元素移除窗口
    while (map.size > 2) {
      map.set(fruits[left], map.get(fruits[left])! - 1);
      if (map.get(fruits[left]) === 0) map.delete(fruits[left]);
      left++;
    }
    // 记录最大的区间数量
    ans = Math.max(ans, right - left + 1);
  }
  return ans;
}
// @lc code=end
