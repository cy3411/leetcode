# 题目

你正在探访一家农场，农场从左到右种植了一排果树。这些树用一个整数数组 fruits 表示，其中 fruits[i] 是第 i 棵树上的水果 种类 。

你想要尽可能多地收集水果。然而，农场的主人设定了一些严格的规矩，你必须按照要求采摘水果：

- 你只有 两个 篮子，并且每个篮子只能装 单一类型 的水果。每个篮子能够装的水果总量没有限制。
- 你可以选择任意一棵树开始采摘，你必须从 每棵 树（包括开始采摘的树）上 恰好摘一个水果 。采摘的水果应当符合篮子中的水果类型。每采摘一次，你将会向右移动到下一棵树，并继续采摘。
- 一旦你走到某棵树前，但水果不符合篮子的水果类型，那么就必须停止采摘。

给你一个整数数组 fruits ，返回你可以收集的水果的 最大 数目。

提示：

- $1 <= fruits.length <= 10^5$
- $0 <= fruits[i] < fruits.length$

# 示例

```
输入：fruits = [1,2,1]
输出：3
解释：可以采摘全部 3 棵树。
```

```
输入：fruits = [0,1,2,2]
输出：3
解释：可以采摘 [1,2,2] 这三棵树。
如果从第一棵树开始采摘，则只能采摘 [0,1] 这两棵树。
```

# 题解

## 滑动窗口

滑动窗口统计符合题意的区间长度，为了方便计算区间内元素的个数，这里使用哈希表来记录。

```js
function totalFruit(fruits: number[]): number {
  const n = fruits.length;
  // 记录元素的个数
  const map = new Map<number, number>();
  let left = 0;
  let ans = 0;
  for (let right = 0; right < n; right++) {
    map.set(fruits[right], (map.get(fruits[right]) ?? 0) + 1);
    // 个数超过2个，将左边元素移除窗口
    while (map.size > 2) {
      map.set(fruits[left], map.get(fruits[left])! - 1);
      if (map.get(fruits[left]) === 0) map.delete(fruits[left]);
      left++;
    }
    // 记录最大的区间数量
    ans = Math.max(ans, right - left + 1);
  }
  return ans;
}
```

```cpp
class Solution {
public:
    int totalFruit(vector<int> &fruits) {
        int n = fruits.size();
        unordered_map<int, int> cnt;

        int left = 0, ans = 0;
        for (int right = 0; right < n; right++) {
            cnt[fruits[right]]++;
            while (cnt.size() > 2) {
                auto it = cnt.find(fruits[left]);
                it->second--;
                if (it->second == 0) {
                    cnt.erase(fruits[left]);
                }
                left++;
            }
            ans = max(ans, right - left + 1);
        }
        return ans;
    }
};
```

```py
class Solution:
    def totalFruit(self, fruits: List[int]) -> int:
        cnt = Counter()
        left = ans = 0
        for right, x in enumerate(fruits):
            cnt[fruits[right]] += 1
            while (len(cnt) > 2):
                cnt[fruits[left]] -= 1
                if cnt[fruits[left]] == 0:
                    cnt.pop(fruits[left])
                left += 1
            ans = max(ans, right - left + 1)
        return ans
```
