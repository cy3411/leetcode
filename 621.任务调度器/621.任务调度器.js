/*
 * @lc app=leetcode.cn id=621 lang=javascript
 *
 * [621] 任务调度器
 */

// @lc code=start
/**
 * @param {character[]} tasks
 * @param {number} n
 * @return {number}
 */
var leastInterval = function (tasks, n) {
  const count = new Array(26).fill(0);
  const base = 'A'.charCodeAt();

  for (const task of tasks) {
    count[task.charCodeAt() - base]++;
  }
  count.sort((a, b) => a - b);
  // 执行次数最多的任务，代表矩阵的行数-1
  // 因为最后一行不需要间隔了
  let maxExec = count[count.length - 1];
  // 执行次数最多的任务共有几个，矩阵最后一行任务的数量
  let maxCount = 0;
  for (let i = count.length - 1; i >= 0 && count[i] === maxExec; i--) {
    maxCount++;
  }

  return Math.max(tasks.length, (maxExec - 1) * (n + 1) + maxCount);
};
// @lc code=end
