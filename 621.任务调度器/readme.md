# 题目
给你一个用字符数组 tasks 表示的 CPU 需要执行的任务列表。其中每个字母表示一种不同种类的任务。任务可以以任意顺序执行，并且每个任务都可以在 1 个单位时间内执行完。在任何一个单位时间，CPU 可以完成一个任务，或者处于待命状态。

然而，两个 相同种类 的任务之间必须有长度为整数 n 的冷却时间，因此至少有连续 n 个单位时间内 CPU 在执行不同的任务，或者在待命状态。

你需要计算完成所有任务所需要的 最短时间 。

提示：

+ 1 <= task.length <= 104
+ tasks[i] 是大写英文字母
+ n 的取值范围为 [0, 100]

# 示例
```
输入：tasks = ["A","A","A","B","B","B"], n = 2
输出：8
解释：A -> B -> (待命) -> A -> B -> (待命) -> A -> B
     在本示例中，两个相同类型任务之间必须间隔长度为 n = 2 的冷却时间，而执行一个任务只需要一个单位时间，所以中间出现了（待命）状态。 
```
```
输入：tasks = ["A","A","A","B","B","B"], n = 0
输出：6
解释：在这种情况下，任何大小为 6 的排列都可以满足要求，因为 n = 0
["A","A","A","B","B","B"]
["A","B","A","B","A","B"]
["B","B","B","A","A","A"]
...
诸如此类
```

# 方法
设置一个矩阵，矩阵列数为n+1,因为同一行不能出现两次相同任务才能保证相隔n个时间执行最多次数的任务，次数为maxExec。

假设共需要执行3次A，3次B，2次C，则，行数maxExec=3，前maxExec-1行，都是满的，共(maxExec-1)*(n+1)个时间最后一行的个数，为等于重复最多次数的任务种类数maxCount。

如上面例子，重复最多次数为3，执行3次的任务有两个，分别为A和B，所以maxCount=2。

示例：
```
ABC
ABC
AB
maxExec=3,maxCount=2
```
总时间=(maxExec-1)*(n+1)+maxCount，

还有一种情况是，(maxExec-1)(n+1)+maxCount比任务总次数少，那总时间=tasks.length。这种情况时，任务间就不需要空闲间隔了，可以一个挨一个的执行。

比如上例中n=0的情况就是如此。

```js
/**
 * @param {character[]} tasks
 * @param {number} n
 * @return {number}
 */
var leastInterval = function (tasks, n) {
  const count = new Array(26).fill(0);
  const base = 'A'.charCodeAt();

  for (const task of tasks) {
    count[task.charCodeAt() - base]++;
  }
  count.sort((a, b) => a - b);
  // 执行次数最多的任务，代表矩阵的行数-1
  // 因为最后一行不需要间隔了
  let maxExec = count[count.length - 1];
  // 执行次数最多的任务共有几个，矩阵最后一行任务的数量
  let maxCount = 0;
  for (let i = count.length - 1; i >= 0 && count[i] === maxExec; i--) {
    maxCount++;
  }

  return Math.max(tasks.length, (maxExec - 1) * (n + 1) + maxCount);
};
```
