/*
 * @lc app=leetcode.cn id=1822 lang=typescript
 *
 * [1822] 数组元素积的符号
 */

// @lc code=start
function arraySign(nums: number[]): number {
  // 负号的数量
  let negativeSigns = 0;
  for (const n of nums) {
    if (n === 0) return 0;
    if (n < 0) negativeSigns++;
  }

  return negativeSigns % 2 === 0 ? 1 : -1;
}
// @lc code=end
