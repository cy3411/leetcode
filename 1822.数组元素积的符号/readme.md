# 题目

已知函数 signFunc(x) 将会根据 x 的正负返回特定值：

- 如果 x 是正数，返回 1 。
- 如果 x 是负数，返回 -1 。
- 如果 x 是等于 0 ，返回 0 。

给你一个整数数组 nums 。令 product 为数组 nums 中所有元素值的乘积。

返回 signFunc(product) 。

提示：

- $1 \leq nums.length \leq 1000$
- $-100 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [-1,-2,-3,-4,3,2,1]
输出：1
解释：数组中所有值的乘积是 144 ，且 signFunc(144) = 1
```

```
输入：nums = [1,5,0,2,-3]
输出：0
解释：数组中所有值的乘积是 0 ，且 signFunc(0) = 0
```

# 题解

## 数学

根据乘法的特性可知：

- 乘数中如果有 0，那么结果一定是 0
- 乘数中如果有负数，那么偶数个负数的结果是正数

遍历 nums,计算负数的数量，如果为偶数返回 1 ，否则返回 -1。

遍历过程中出现 0，直接返回 0。

```js
function arraySign(nums: number[]): number {
  // 负号的数量
  let negativeSigns = 0;
  for (const n of nums) {
    if (n === 0) return 0;
    if (n < 0) negativeSigns++;
  }

  return negativeSigns % 2 === 0 ? 1 : -1;
}
```
