/*
 * @lc app=leetcode.cn id=1370 lang=javascript
 *
 * [1370] 上升下降字符串
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var sortString = function (s) {
  const nums = new Array(26).fill(0);
  for (let char of s) {
    let idx = char.charCodeAt() - 97;
    nums[idx]++;
  }

  let result = '';
  while (result.length < s.length) {
    for (let i = 0; i < nums.length; i++) {
      if (nums[i]) {
        result += String.fromCharCode(i + 97);
        nums[i]--;
      }
    }
    for (let i = nums.length; i >= 0; i--) {
      if (nums[i]) {
        result += String.fromCharCode(i + 97);
        nums[i]--;
      }
    }
  }

  return result;
};
// @lc code=end
