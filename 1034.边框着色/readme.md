# 题目

给你一个大小为 m x n 的整数矩阵 grid ，表示一个网格。另给你三个整数 row、col 和 color 。网格中的每个值表示该位置处的网格块的颜色。

当两个网格块的颜色相同，而且在四个方向中任意一个方向上相邻时，它们属于同一 连通分量 。

连通分量的边界 是指连通分量中的所有与不在分量中的网格块相邻（四个方向上）的所有网格块，或者在网格的边界上（第一行/列或最后一行/列）的所有网格块。

请你使用指定颜色 color 为所有包含网格块 grid[row][col] 的 连通分量的边界 进行着色，并返回最终的网格 grid 。

提示：

- $m == grid.length$
- $n == grid[i].length$
- $1 \leq m, n \leq 50$
- $1 \leq grid[i][j], color \leq 1000$
- $0 \leq row < m$
- $0 \leq col < n$

# 示例

```
输入：grid = [[1,1],[1,2]], row = 0, col = 0, color = 3
输出：[[3,3],[3,2]]
```

```
输入：grid = [[1,1,1],[1,1,1],[1,1,1]], row = 1, col = 1, color = 2
输出：[[2,2,2],[2,1,2],[2,2,2]]
```

# 题解

## 深度优先搜索

使用深度优先搜索遍历连通分量，并将边界保存到 `borders` 中。

最后遍历 `borders`，将边界着色为 `color`。

```ts
function colorBorder(grid: number[][], row: number, col: number, color: number): number[][] {
  const m = grid.length;
  const n = grid[0].length;
  // 4个方向
  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ];
  // 边框集合
  const borders = [];
  // 是否访问过
  const visited = new Array(m).fill(0).map(() => new Array(n).fill(0));
  // 连通分量的颜色
  const originColor = grid[row][col];
  // 初始点标记为已访问
  visited[row][col] = 1;

  // 深度优先搜索
  const dfs = (i: number, j: number) => {
    let isBorder = false;
    for (let k = 0; k < directions.length; k++) {
      const x = i + directions[k][0];
      const y = j + directions[k][1];
      if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] === originColor) {
        if (visited[x][y] === 0) {
          visited[x][y] = 1;
          dfs(x, y);
        }
      } else {
        // 如果边界点，则添加到边界集合中
        isBorder = true;
      }
    }
    if (isBorder) borders.push([i, j]);
  };

  dfs(row, col);

  // 标记边框
  for (let i = 0; i < borders.length; i++) {
    const [x, y] = borders[i];
    grid[x][y] = color;
  }

  return grid;
}
```
