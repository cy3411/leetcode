/*
 * @lc app=leetcode.cn id=1662 lang=cpp
 *
 * [1662] 检查两个字符串数组是否相等
 */

// @lc code=start
class Solution {
public:
    bool arrayStringsAreEqual(vector<string> &word1, vector<string> &word2) {
        // 数组索引
        int p1 = 0, p2 = 0;
        // 数组元素的索引
        int i = 0, j = 0;
        // 数组长度
        int m = word1.size(), n = word2.size();
        // 遍历比较
        while (p1 < m && p2 < n) {
            if (word1[p1][i] != word2[p2][j]) {
                return false;
            }
            i++;
            if (i == word1[p1].size()) {
                p1++;
                i = 0;
            }
            j++;
            if (j == word2[p2].size()) {
                p2++;
                j = 0;
            }
        }
        return p1 == m && p2 == n;
    }
};
// @lc code=end
