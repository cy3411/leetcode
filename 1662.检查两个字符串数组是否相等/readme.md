# 题目

给你两个字符串数组 word1 和 word2 。如果两个数组表示的字符串相同，返回 true ；否则，返回 false 。

数组表示的字符串 是由数组中的所有元素 按顺序 连接形成的字符串。

提示：

- $1 \leq word1.length, word2.length \leq 10^3$
- $1 \leq word1[i].length, word2[i].length \leq 10^3$
- $1 \leq sum(word1[i].length), sum(word2[i].length) \leq 10^3$
- word1[i] 和 word2[i] 由小写字母组成

# 示例

```
输入：word1 = ["ab", "c"], word2 = ["a", "bc"]
输出：true
解释：
word1 表示的字符串为 "ab" + "c" -> "abc"
word2 表示的字符串为 "a" + "bc" -> "abc"
两个字符串相同，返回 true
```

```
输入：word1 = ["a", "cb"], word2 = ["ab", "c"]
输出：false
```

# 题解

## 拼接字符串比较

将 word1 和 word2 拼接字符串后比较。

```ts
function arrayStringsAreEqual(word1: string[], word2: string[]): boolean {
  return word1.join('') === word2.join('');
}
```

## 遍历

定义指针 p1 和 p2 分别指向 word1 和 word2 索引，另外设置 i 和 j 分别指向 word1[p1]和 word2[p2]中元素的索引。

比较 word1[p1][i] 和 word2[p2][j]，如果不等，返回 false。否则 i+1，如果 i 到达当前元素的末尾，那么将 p1+1，i 重置为 0。p2 和 j 同理。

当最后 p1 和 p2 都遍历完毕，表示完成比较，返回 true，否则，返回 false。

```cpp
class Solution {
public:
    bool arrayStringsAreEqual(vector<string> &word1, vector<string> &word2) {
        // 数组索引
        int p1 = 0, p2 = 0;
        // 数组元素的索引
        int i = 0, j = 0;
        // 数组长度
        int m = word1.size(), n = word2.size();
        // 遍历比较
        while (p1 < m && p2 < n) {
            if (word1[p1][i] != word2[p2][j]) {
                return false;
            }
            i++;
            if (i == word1[p1].size()) {
                p1++;
                i = 0;
            }
            j++;
            if (j == word2[p2].size()) {
                p2++;
                j = 0;
            }
        }
        return p1 == m && p2 == n;
    }
};
```
