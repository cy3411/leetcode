/*
 * @lc app=leetcode.cn id=978 lang=javascript
 *
 * [978] 最长湍流子数组
 */

// @lc code=start
/**
 * @param {number[]} arr
 * @return {number}
 */
var maxTurbulenceSize = function (arr) {
  const size = arr.length;
  const dp = new Array(size).fill(0).map(() => [1, 1]);

  // 0和1分别保存i结尾数字时大于前一个和小于前一个的长度
  dp[0][0] = dp[0][1] = 1;

  for (let i = 1; i < size; i++) {
    // 转移方式式
    if (arr[i - 1] > arr[i]) {
      dp[i][0] = dp[i - 1][1] + 1;
    } else if (arr[i - 1] < arr[i]) {
      dp[i][1] = dp[i - 1][0] + 1;
    }
  }

  console.log(dp);
  let result = 1;
  for (let i = 0; i < size; i++) {
    result = Math.max(result, dp[i][0]);
    result = Math.max(result, dp[i][1]);
  }

  return result;
};
// @lc code=end
