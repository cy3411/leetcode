# 题目

输入一个字符串，打印出该字符串中字符的所有排列。

你可以以任意顺序返回这个字符串数组，但里面**不能有重复元素**。

限制：

- `1 <= s 的长度 <= 8`

# 示例

```
输入：s = "abc"
输出：["abc","acb","bac","bca","cab","cba"]
```

# 题解

## 回溯

这个就是一个全排列的问题，只是在更新答案的时候需要判断一下是否有重复的元素。

```ts
function permutation(s: string): string[] {
  const arr = s.split('');
  const ans: Set<string> = new Set();

  const backtrack = (arr: string[], idx: number) => {
    if (idx === arr.length) {
      ans.add(arr.join(''));
      return;
    }
    for (let i = idx; i < arr.length; i++) {
      [arr[i], arr[idx]] = [arr[idx], arr[i]];
      backtrack(arr, idx + 1);
      [arr[i], arr[idx]] = [arr[idx], arr[i]];
    }
  };

  backtrack(arr, 0);

  return [...ans];
}
```
