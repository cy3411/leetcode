/*
 * @lc app=leetcode.cn id=1694 lang=typescript
 *
 * [1694] 重新格式化电话号码
 */

// @lc code=start
function reformatNumber(number: string): string {
  let n = number.length;
  let digits = '';
  // 获取所有数字的字符串
  for (const s of number) {
    if (s === ' ' || s === '-') continue;
    digits += s;
  }

  n = digits.length;
  let pos = 0;
  let ans = '';
  while (n > 0) {
    if (n > 4) {
      ans += `${digits.substring(pos, pos + 3)}-`;
      pos += 3;
      n -= 3;
    } else {
      if (n === 4) {
        ans += digits.slice(pos, pos + 2) + '-' + digits.slice(pos + 2, pos + 4);
      } else {
        ans += digits.slice(pos, pos + n);
      }
      break;
    }
  }

  return ans;
}
// @lc code=end
