/*
 * @lc app=leetcode.cn id=2226 lang=cpp
 *
 * [2226] 每个小孩最多能分到多少糖果
 */

// @lc code=start
class Solution
{
public:
    long long isCheck(vector<int> &candies, long long k)
    {
        if (k == 0)
            return __INT64_MAX__;

        long long res = 0;
        for (auto x : candies)
        {
            res += x / k;
        }
        return res;
    }
    int maximumCandies(vector<int> &candies, long long k)
    {
        long long l = 0, r = 0, mid;
        for (auto x : candies)
        {
            r = max(r, 1LL * x);
        }
        r++;
        while (l < r)
        {
            mid = (l + r) / 2;
            if (isCheck(candies, mid) >= k)
                l = mid + 1;
            else
                r = mid;
        }

        return l - 1;
    }
};
// @lc code=end
