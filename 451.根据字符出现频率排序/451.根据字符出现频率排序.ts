/*
 * @lc app=leetcode.cn id=451 lang=typescript
 *
 * [451] 根据字符出现频率排序
 */

// @lc code=start
function frequencySort(s: string): string {
  const hash: Map<string, number> = new Map();
  // 统计字符出现次数
  for (let char of s) {
    hash.set(char, (hash.get(char) || 0) + 1);
  }
  // 按次数排序
  const temp: [string, number][] = [...hash.entries()];
  temp.sort((a, b) => b[1] - a[1]);
  // 更新结果
  let ans = '';
  for (let i = 0; i < temp.length; i++) {
    let [char, num] = temp[i];
    while (num--) {
      ans += char;
    }
  }

  return ans;
}
// @lc code=end
