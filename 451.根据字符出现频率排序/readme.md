# 题目

给定一个字符串，请将字符串里的字符按照出现的频率降序排列。

# 示例

```
输入:
"tree"

输出:
"eert"

解释:
'e'出现两次，'r'和't'都只出现一次。
因此'e'必须出现在'r'和't'之前。此外，"eetr"也是一个有效的答案。
```

# 题解

## 哈希表

使用哈希表统计每个字符的出现次数，按照出现次数对统计结果排序。

最后遍历统计结果，更新答案。

```ts
function frequencySort(s: string): string {
  const hash: Map<string, number> = new Map();
  // 统计字符出现次数
  for (let char of s) {
    hash.set(char, (hash.get(char) || 0) + 1);
  }
  // 按次数排序
  const temp: [string, number][] = [...hash.entries()];
  temp.sort((a, b) => b[1] - a[1]);
  // 更新结果
  let ans = '';
  for (let i = 0; i < temp.length; i++) {
    let [char, num] = temp[i];
    while (num--) {
      ans += char;
    }
  }

  return ans;
}
```
