# 题目
用以太网线缆将 `n` 台计算机连接成一个网络，计算机的编号从 `0` 到 `n-1`。线缆用 `connections` 表示，其中 `connections[i] = [a, b]` 连接了计算机 `a` 和 `b`。

网络中的任何一台计算机都可以通过网络直接或者间接访问同一个网络中其他任意一台计算机。

给你这个计算机网络的初始布线 `connections`，你可以拔开任意两台直连计算机之间的线缆，并用它连接一对未直连的计算机。请你计算并返回使所有计算机都连通所需的最少操作次数。如果不可能，则返回 -1 。 

# 示例
![g8SHc8.md.png](https://z3.ax1x.com/2021/05/07/g8SHc8.md.png)
```
输入：n = 4, connections = [[0,1],[0,2],[1,2]]
输出：1
解释：拔下计算机 1 和 2 之间的线缆，并将它插到计算机 1 和 3 上。
```
# 题解
## 并查集
将`connections`加入并查集中，最后检查并查集中有几个集合，集合数量减1就是需要的最少操作次数。

这里有一个特判，如果`connections`的长度小于`n-1`的话，直接返回`-1`。因为`n`台计算机的最小连接数是`n-1`个。

```js
class UnionFind {
  parent: number[];
  size: number[];
  count: number;
  constructor(n: number = 0) {
    this.parent = new Array(n).fill(0).map((_, i) => i);
    this.size = new Array(n).fill(1);
    this.count = n;
  }
  find(x: number): number {
    return this.parent[x] === x ? x : (this.parent[x] = this.find(this.parent[x]));
  }
  union(x: number, y: number): void {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.size[ry] += this.size[rx];
    this.parent[rx] = ry;
    this.count--;
  }
}

function makeConnected(n: number, connections: number[][]): number {
  const size = connections.length;
  if (size < n - 1) return -1;

  const unionFind = new UnionFind(n);
  for (const [x, y] of connections) {
    unionFind.union(x, y);
  }

  return unionFind.count - 1;
}
```