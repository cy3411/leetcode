/*
 * @lc app=leetcode.cn id=989 lang=javascript
 *
 * [989] 数组形式的整数加法
 */

// @lc code=start
/**
 * @param {number[]} A
 * @param {number} K
 * @return {number[]}
 */
var addToArrayForm = function (A, K) {
  const size = A.length;
  const result = [];

  for (let i = size - 1; i >= 0 || K > 0; i--, K = Math.floor(K / 10)) {
    if (i >= 0) {
      K += A[i];
    }
    result.unshift(K % 10);
  }
  return result;
};
// @lc code=end
