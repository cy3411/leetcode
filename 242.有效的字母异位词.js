/*
 * @lc app=leetcode.cn id=242 lang=javascript
 *
 * [242] 有效的字母异位词
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function (s, t) {
  if (s.length !== t.length) {
    return false;
  }

  const hash = new Array(26).fill(0);

  let i = 0;
  let j = 0;
  while (i < s.length) {
    let sChar = s[i].charCodeAt() - 97;
    hash[sChar] += 1;
    i++;
  }

  while (j < s.length) {
    let tChar = t[j].charCodeAt() - 97;
    hash[tChar] -= 1;
    if (hash[tChar] < 0) {
      return false;
    }
    j++;
  }

  return true;
};
// @lc code=end
