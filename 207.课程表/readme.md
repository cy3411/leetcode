# 题目
你这个学期必须选修 `numCourses` 门课程，记为 `0` 到 `numCourses - 1` 。

在选修某些课程之前需要一些先修课程。 先修课程按数组 `prerequisites` 给出，其中 `prerequisites[i] = [ai, bi]` ，表示如果要学习课程 `ai` 则 必须 先学习课程  `bi` 。

例如，先修课程对 `[0, 1]` 表示：想要学习课程 `0` ，你需要先完成课程 `1` 。
请你判断是否可能完成所有课程的学习？如果可以，返回 `true` ；否则，返回 `false` 。

提示：
+ 1 <= numCourses <= 105
+ 0 <= prerequisites.length <= 5000
+ prerequisites[i].length == 2
+ 0 <= ai, bi < numCourses
+ prerequisites[i] 中的所有课程对 互不相同

# 示例
```
输入：numCourses = 2, prerequisites = [[1,0]]
输出：true
解释：总共有 2 门课程。学习课程 1 之前，你需要完成课程 0 。这是可能的。
```

```
输入：numCourses = 2, prerequisites = [[1,0],[0,1]]
输出：false
解释：总共有 2 门课程。学习课程 1 之前，你需要先完成​课程 0 ；并且学习课程 0 之前，你还应先完成课程 1 。这是不可能的。
```

# 题解
## 拓扑排序
我们将学习课程顺序理解成一条有向边，那这里求的就是拓扑排序的结果。

### 广度优先
先建模，将入度为`0`的点入队。遍历队首所能到达的点，再次把入度`0`为点入队，直接队列清空。

每次出队的点就是我们拓扑排序的顺序。

```ts
function canFinish(numCourses: number, prerequisites: number[][]): boolean {
  const graph: number[][] = new Array(numCourses).fill(0).map(() => []);
  const degree = new Array(numCourses).fill(0);

  // 建模
  for (let [to, from] of prerequisites) {
    graph[from].push(to);
    degree[to] += 1;
  }
  // 将入度为0的放入队列
  const queue = [];
  for (let i = 0; i < numCourses; i++) {
    if (degree[i] === 0) queue.push(i);
  }

  const result = [];
  // 每次取出队首元素，将元素放入结果
  // 同时将当前元素对应边的入度减1，并将入度为0的放入队尾
  while (queue.length) {
    const idx = queue.shift();
    result.push(idx);
    for (let to of graph[idx]) {
      degree[to] -= 1;
      if (degree[to] === 0) queue.push(to);
    }
  }
  // 最后的结果就是拓扑排序，判断是否跟课程数量一致即可
  return result.length === numCourses;
}
```