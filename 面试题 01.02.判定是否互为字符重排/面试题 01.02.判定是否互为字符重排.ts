// @algorithm @lc id=100159 lang=typescript
// @title check-permutation-lcci
// @test("abc","bca")=true
// @test("abc","bad")=false
function CheckPermutation(s1: string, s2: string): boolean {
  const n = s1.length;
  const m = s2.length;
  if (n !== m) return false;

  const cnt = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  for (let i = 0; i < n; i++) {
    const idx = s1.charCodeAt(i) - base;
    cnt[idx]++;
  }

  for (let i = 0; i < m; i++) {
    const idx = s2.charCodeAt(i) - base;
    cnt[idx]--;
    if (cnt[idx] < 0) return false;
  }

  return true;
}
