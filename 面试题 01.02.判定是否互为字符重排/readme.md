# 题目

给定两个字符串 `s1` 和 `s2`，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串。

说明：

- $0 \leq len(s1) \leq 100$
- $0 \leq len(s2) \leq 100$

# 示例

```
输入: s1 = "abc", s2 = "bca"
输出: true
```

```
输入: s1 = "abc", s2 = "bad"
输出: false
```

# 题解

## 哈希表

使用哈希表统计 s1 中每个字符出现的频次，遍历 s2 ，将当前字符的出现的频次减 1 ，如果有负数出现，表示 s2 中包含了不在 s1 中的字符，返回 false。

```js
function CheckPermutation(s1: string, s2: string): boolean {
  const n = s1.length;
  const m = s2.length;
  if (n !== m) return false;

  const cnt = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  for (let i = 0; i < n; i++) {
    const idx = s1.charCodeAt(i) - base;
    cnt[idx]++;
  }

  for (let i = 0; i < m; i++) {
    const idx = s2.charCodeAt(i) - base;
    cnt[idx]--;
    if (cnt[idx] < 0) return false;
  }

  return true;
}
```

## 排序

将两个字符串排序后比较是否相等。

```cpp
class Solution {
public:
    bool CheckPermutation(string s1, string s2) {
        if (s1.size() != s2.size()) return false;
        sort(s1.begin(), s1.end());
        sort(s2.begin(), s2.end());
        return s1 == s2;
    }
};
```
