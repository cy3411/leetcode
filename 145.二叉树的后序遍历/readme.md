# 题目
给定一个二叉树，返回它的**后序**遍历。

进阶: 递归算法很简单，你可以通过迭代算法完成吗？

# 示例
```
输入: [1,null,2,3]  
   1
    \
     2
    /
   3 

输出: [3,2,1]
```

# 方法
## 递归
```js
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var postorderTraversal = function (root) {
  const result = [];
  helper(root, result);
  return result;

  function helper(root, result) {
    if (root === null) return;
    helper(root.left, result);
    helper(root.right, result);
    result.push(root.val);
  }
};
```

## 迭代
递归本身就是调用了系统的执行栈，我们可以用栈来模拟，实现迭代算法。

```js
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var postorderTraversal = function (root) {
  if (root === null) return [];

  const result = [];
  // 双栈，1个保存节点，1个保存压入状态（0：left,1:right,2:print）
  const nodes = [root];
  const status = [0];

  while (nodes.length) {
    // 取出状态
    let s = status.pop();
    switch (s) {
      case 0:
        // 压入左子树，状态改成1
        status.push(1);
        if (nodes[nodes.length - 1].left) {
          nodes.push(nodes[nodes.length - 1].left);
          status.push(0);
        }
        break;
      case 1:
        // 压入左子树，状态改成2
        status.push(2);
        if (nodes[nodes.length - 1].right) {
          nodes.push(nodes[nodes.length - 1].right);
          status.push(0);
        }
        break;
      case 2:
        // 输出值，将节点弹出栈
        result.push(nodes[nodes.length - 1].val);
        nodes.pop();
        break;
    }
  }

  return result;
};
```