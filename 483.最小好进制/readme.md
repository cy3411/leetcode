# 题目

对于给定的整数 n, 如果 n 的 k（k>=2）进制数的所有数位全为 1，则称 k（k>=2）是 n 的一个**好进制**。

以字符串的形式给出 n, 以字符串的形式返回 n 的最小好进制。

# 示例

```
输入："13"
输出："3"
解释：13 的 3 进制是 111。
```

# 题解

## 数学公式

```ts
function smallestGoodBase(n: string): string {
  let num = Number(n);
  // 最多的位数就是2进制的位数
  const max = Math.floor(Math.log(num) / Math.log(2));

  for (let i = max; i > 1; i--) {
    let k = BigInt(Math.floor(Math.pow(num, 1 / i)));
    if (k > 1) {
      let cur = 1n;
      let sum = 1n;
      for (let j = 1; j <= i; j++) {
        cur *= k;
        sum += cur;
      }
      if (sum === BigInt(n)) {
        return String(k);
      }
    }
  }

  return String(BigInt(n) - 1n);
```
