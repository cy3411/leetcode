# 题目

给你一个二维整数数组 ranges 和两个整数 left 和 right 。每个 ranges[i] = [starti, endi] 表示一个从 starti 到 endi 的 闭区间 。

如果闭区间 [left, right] 内每个整数都被 ranges 中 至少一个 区间覆盖，那么请你返回 true ，否则返回 false 。

已知区间 ranges[i] = [starti, endi] ，如果整数 x 满足 starti <= x <= endi ，那么我们称整数 x 被覆盖了。

提示：

- 1 <= ranges.length <= 50
- 1 <= starti <= endi <= 50
- 1 <= left <= right <= 50

# 示例

```
输入：ranges = [[1,2],[3,4],[5,6]], left = 2, right = 5
输出：true
解释：2 到 5 的每个整数都被覆盖了：
- 2 被第一个区间覆盖。
- 3 和 4 被第二个区间覆盖。
- 5 被第三个区间覆盖。
```

# 题解

## 合并区间

先将 ranges 中重叠或者相邻的区间合并，最后判断 left 和 right 是否在某个合并区间内。

```ts
function isCovered(ranges: number[][], left: number, right: number): boolean {
  ranges.sort((a: number[], b: number[]): number => {
    if (a[0] === b[0]) return a[1] - b[1];
    else return a[0] - b[0];
  });

  const mergeRanges: number[][] = [];
  let l: number;
  let r: number;
  // 合并重复区间
  for (let i = 0; i < ranges.length; i++) {
    if (i === 0) {
      l = ranges[i][0];
      r = ranges[i][1];
    }

    if (r + 1 >= ranges[i][0]) {
      r = Math.max(r, ranges[i][1]);
    } else {
      mergeRanges.push([l, r]);
      l = ranges[i][0];
      r = ranges[i][1];
    }

    if (i === ranges.length - 1) {
      mergeRanges.push([l, r]);
    }
  }
  // 判断是否在区间内
  for (const range of mergeRanges) {
    const [l, r] = range;
    if (left >= l && right <= r) return true;
  }

  return false;
}
```
