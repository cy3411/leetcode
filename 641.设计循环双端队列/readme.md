# 题目

设计实现双端队列。
你的实现需要支持以下操作：

- MyCircularDeque(k)：构造函数,双端队列的大小为 k。
- insertFront()：将一个元素添加到双端队列头部。 如果操作成功返回 true。
- insertLast()：将一个元素添加到双端队列尾部。如果操作成功返回 true。
- deleteFront()：从双端队列头部删除一个元素。 如果操作成功返回 true。
- deleteLast()：从双端队列尾部删除一个元素。如果操作成功返回 true。
- getFront()：从双端队列头部获得一个元素。如果双端队列为空，返回 -1。
- getRear()：获得双端队列的最后一个元素。 如果双端队列为空，返回 -1。
- isEmpty()：检查双端队列是否为空。
- isFull()：检查双端队列是否满了。

提示：

- 所有值的范围为 [1, 1000]
- 操作次数的范围为 [1, 1000]
- 请不要使用内置的双端队列库。

# 示例

```
MyCircularDeque circularDeque = new MycircularDeque(3); // 设置容量大小为3
circularDeque.insertLast(1);			        // 返回 true
circularDeque.insertLast(2);			        // 返回 true
circularDeque.insertFront(3);			        // 返回 true
circularDeque.insertFront(4);			        // 已经满了，返回 false
circularDeque.getRear();  				// 返回 2
circularDeque.isFull();				        // 返回 true
circularDeque.deleteLast();			        // 返回 true
circularDeque.insertFront(4);			        // 返回 true
circularDeque.getFront();				// 返回 4
```

# 方法

使用数组存储数据，使用 head 和 tail 指针分别指向头和尾。

```js
var MyCircularDeque = function (k) {
  this.k = k;
  this.queue = new Array(k);
  this.size = 0;
  this.head = 0;
  this.tail = 1;
};

/**
 * Adds an item at the front of Deque. Return true if the operation is successful.
 * @param {number} value
 * @return {boolean}
 */
MyCircularDeque.prototype.insertFront = function (value) {
  if (this.isFull()) return false;
  let index = this.head;
  this.queue[index] = value;
  this.head = (this.head - 1 + this.k) % this.k;
  this.size++;
  return true;
};

/**
 * Adds an item at the rear of Deque. Return true if the operation is successful.
 * @param {number} value
 * @return {boolean}
 */
MyCircularDeque.prototype.insertLast = function (value) {
  if (this.isFull()) return false;
  let index = this.tail;
  this.queue[index] = value;
  this.tail = (this.tail + 1) % this.k;
  this.size++;
  return true;
};

/**
 * Deletes an item from the front of Deque. Return true if the operation is successful.
 * @return {boolean}
 */
MyCircularDeque.prototype.deleteFront = function () {
  if (this.isEmpty()) return false;
  this.head = (this.head + 1) % this.k;
  this.queue[this.head] = void 0;
  this.size--;
  return true;
};

/**
 * Deletes an item from the rear of Deque. Return true if the operation is successful.
 * @return {boolean}
 */
MyCircularDeque.prototype.deleteLast = function () {
  if (this.isEmpty()) return false;
  this.tail = (this.tail - 1 + this.k) % this.k;
  this.queue[this.tail] = void 0;
  this.size--;
  return true;
};

/**
 * Get the front item from the deque.
 * @return {number}
 */
MyCircularDeque.prototype.getFront = function () {
  if (this.isEmpty()) return -1;
  let index = (this.head + 1) % this.k;
  return this.queue[index];
};

/**
 * Get the last item from the deque.
 * @return {number}
 */
MyCircularDeque.prototype.getRear = function () {
  if (this.isEmpty()) return -1;
  let index = (this.tail - 1 + this.k) % this.k;
  return this.queue[index];
};

/**
 * Checks whether the circular deque is empty or not.
 * @return {boolean}
 */
MyCircularDeque.prototype.isEmpty = function () {
  return this.size === 0;
};

/**
 * Checks whether the circular deque is full or not.
 * @return {boolean}
 */
MyCircularDeque.prototype.isFull = function () {
  return this.size === this.k;
};
```

```js
class MyCircularDeque {
  queue: number[];
  head: number;
  tail: number;
  constructor(k: number) {
    this.queue = new Array(k + 1);
    this.head = 0;
    this.tail = 0;
  }

  insertFront(value: number): boolean {
    if (this.isFull()) return false;
    this.queue[this.head] = value;
    this.head = (this.head - 1 + this.queue.length) % this.queue.length;
    return true;
  }

  insertLast(value: number): boolean {
    if (this.isFull()) return false;
    this.tail = (this.tail + 1) % this.queue.length;
    this.queue[this.tail] = value;
    return true;
  }

  deleteFront(): boolean {
    if (this.isEmpty()) return false;
    this.head = (this.head + 1) % this.queue.length;
    return true;
  }

  deleteLast(): boolean {
    if (this.isEmpty()) return false;
    this.tail = (this.tail - 1 + this.queue.length) % this.queue.length;
    return true;
  }

  getFront(): number {
    if (this.isEmpty()) return -1;
    return this.queue[(this.head + 1) % this.queue.length];
  }

  getRear(): number {
    if (this.isEmpty()) return -1;
    return this.queue[(this.tail + this.queue.length) % this.queue.length];
  }

  isEmpty(): boolean {
    return this.head === this.tail;
  }

  isFull(): boolean {
    return (this.tail + 1) % this.queue.length === this.head;
  }
}
```
