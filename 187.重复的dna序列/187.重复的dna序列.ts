/*
 * @lc app=leetcode.cn id=187 lang=typescript
 *
 * [187] 重复的DNA序列
 */

// @lc code=start
function findRepeatedDnaSequences(s: string): string[] {
  const size = s.length;
  const hash: Map<string, number> = new Map();

  for (let i = 0; i <= size - 10; i++) {
    const substr = s.substring(i, i + 10);
    hash.set(substr, (hash.get(substr) || 0) + 1);
  }
  const ans: string[] = [];
  hash.forEach((val, key) => {
    if (val > 1) {
      ans.push(key);
    }
  });

  return ans;
}
// @lc code=end
