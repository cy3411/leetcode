# 题目

所有 DNA 都由一系列缩写为 `'A'`，`'C'`，`'G'` 和 `'T'` 的核苷酸组成，例如：`"ACGAATTCCG"`。在研究 DNA 时，识别 DNA 中的重复序列有时会对研究非常有帮助。

编写一个函数来找出所有目标子串，目标子串的长度为 10，且在 DNA 字符串 `s` 中出现次数超过一次。

# 示例

```
输入：s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
输出：["AAAAACCCCC","CCCCCAAAAA"]
```

# 题解

## 哈希表

遍历字符串，将所有长度为 `10` 的字串和出现次数存入哈希表中，最后统计哈希表中出现次数超过 `1` 的字串。

```ts
function findRepeatedDnaSequences(s: string): string[] {
  const size = s.length;
  const hash: Map<string, number> = new Map();

  for (let i = 0; i <= size - 10; i++) {
    const substr = s.substring(i, i + 10);
    hash.set(substr, (hash.get(substr) || 0) + 1);
  }
  const ans = [];
  hash.forEach((val, key) => {
    if (val > 1) {
      ans.push(key);
    }
  });

  return ans;
}
```
