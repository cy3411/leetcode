# 题目

集合 s 包含从 1 到 n 的整数。不幸的是，因为数据错误，导致集合里面某一个数字复制了成了集合里面的另外一个数字的值，导致集合 丢失了一个数字 并且 有一个数字重复 。

给定一个数组 nums 代表了集合 S 发生错误后的结果。

请你找出重复出现的整数，再找到丢失的整数，将它们以数组的形式返回。

提示：

- 2 <= nums.length <= 104
- 1 <= nums[i] <= 104

# 示例

```
输入：nums = [1,2,2,4]
输出：[2,3]
```

```
输入：nums = [1,1]
输出：[1,2]
```

# 题解

## 哈希表

使用 hash 统计 num 出现的次数，最后遍历 hash，更新答案。

```ts
function findErrorNums(nums: number[]): number[] {
  const m = nums.length;
  const map = new Array(m + 1).fill(0);
  // 统计数字出现的次数
  for (let num of nums) {
    map[num]++;
  }
  // 更新答案,重复的数字在前面，错误的数字在后面
  const ans = new Array(2);
  for (let i = 1; i <= m; i++) {
    if (map[i] > 1) ans[0] = i;
    if (map[i] === 0) ans[1] = i;
  }

  return ans;
}
```
