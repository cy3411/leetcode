/*
 * @lc app=leetcode.cn id=645 lang=typescript
 *
 * [645] 错误的集合
 */

// @lc code=start
function findErrorNums(nums: number[]): number[] {
  const m = nums.length;
  const map = new Array(m + 1).fill(0);
  // 统计数字出现的次数
  for (let num of nums) {
    map[num]++;
  }
  // 更新答案,重复的数字在前面，错误的数字在后面
  const ans = new Array(2);
  for (let i = 1; i <= m; i++) {
    if (map[i] > 1) ans[0] = i;
    if (map[i] === 0) ans[1] = i;
  }

  return ans;
}
// @lc code=end
