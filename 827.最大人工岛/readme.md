# 题目

给你一个大小为 `n x n` 二进制矩阵 `grid` 。最多 只能将一格 `0` 变成 `1` 。

返回执行此操作后，`grid` 中最大的岛屿面积是多少？

**岛屿** 由一组上、下、左、右四个方向相连的 `1` 形成。

提示：

- $n \equiv grid.length$
- $n \equiv grid[i].length$
- $1 \leq n \leq 500$
- `grid[i][j]` 为 `0` 或 `1`

# 示例

```
输入: grid = [[1, 0], [0, 1]]
输出: 3
解释: 将一格0变成1，最终连通两个小岛得到面积为 3 的岛屿。
```

```
输入: grid = [[1, 1], [1, 0]]
输出: 4
解释: 将一格0变成1，岛屿的面积扩大为 4。
```

# 题解

## 并查集

使用并查集统计所有 1 结点的连通性已经对应的面积。

最后枚举所有的 0 结点，检查它对应的四个方向上是否有 1 ，如果有，将所有相邻的 1 的面积计入结果，并返回最大值。

```js
class Union {
  data: number[];
  size: number[];
  constructor(n: number) {
    this.data = new Array(n).fill(-1).map((_, idx) => idx);
    this.size = new Array(n).fill(1);
  }
  union(x: number, y: number): void {
    let rx = this.find(x);
    let ry = this.find(y);
    if (rx === ry) return;
    if (this.size[rx] > this.size[ry]) {
        [rx, ry] = [ry, rx]
    }
    this.size[ry] += this.size[rx];
    this.data[rx] = ry;
  }
  find(x: number): number {
    if (this.data[x] !== x) this.data[x] = this.find(this.data[x]);
    return this.data[x];
  }
}

function largestIsland(grid: number[][]): number {
  const n = grid.length;
  const dirs = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];
  // 从下标1开始计算，所以数组长度加1
  const unions = new Union(n * n + 1);
  // 遍历每个点，相邻的1做连通连接
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 0) continue;
      for (const [x, y] of dirs) {
        const nx = i + x;
        const ny = j + y;
        if (nx < 0 || nx >= n || ny < 0 || ny >= n || grid[nx][ny] === 0) continue;
        // 二维坐标转一维坐标，从1开始计数
        unions.union(i * n + j + 1, nx * n + ny + 1);
      }
    }
  }

  // 遍历每个值为0的点，尝试反转后计算连通岛屿的面积
  let ans = 0;
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 1) {
        const root = unions.find(i * n + j + 1);
        ans = Math.max(ans, unions.size[root]);
      } else {
        let total = 1;
        // 记录四边扩展的时候是否有重复岛屿
        const set = new Set<number>();
        for (const [x, y] of dirs) {
          let nx = i + x;
          let ny = j + y;
          // 四边扩展时是否越界或者相邻点不是岛屿
          if (nx < 0 || nx >= n || ny < 0 || ny >= n || grid[nx][ny] === 0) continue;
          const root = unions.find(nx * n + ny + 1);
          if (set.has(root)) continue;
          total += unions.size[root];
          set.add(root);
        }
        ans = Math.max(ans, total);
      }
    }
  }
  return ans;
}
```
