/*
 * @lc app=leetcode.cn id=345 lang=typescript
 *
 * [345] 反转字符串中的元音字母
 */

// @lc code=start
function reverseVowels(s: string): string {
  const hash = new Set(['a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U']);
  const strs = s.split('');

  let l = 0;
  let r = strs.length - 1;
  while (l <= r) {
    while (!hash.has(strs[l]) && l <= r) {
      l++;
    }
    while (!hash.has(strs[r]) && l <= r) {
      r--;
    }
    if (hash.has(strs[l]) && hash.has(strs[r])) {
      [strs[l], strs[r]] = [strs[r], strs[l]];
      l++;
      r--;
    }
  }

  return strs.join('');
}
// @lc code=end
