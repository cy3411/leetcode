# 题目

给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。

元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现。

提示：

- $1 <= s.length <= 3 * 105$
- $s 由 可打印的 ASCII 字符组成$

# 示例

```
输入：s = "hello"
输出："holle"
```

# 题解

## 双指针

定义 l 为头指针，定义 r 为尾指针，头尾同时找元音字母，找到后交换，然后继续前面的操作。

将元音字母预处理存入哈希中，方便遍历的过程中快速定位是否是元音字母。

```ts
function reverseVowels(s: string): string {
  const hash = new Set(['a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U']);
  const strs = s.split('');

  let l = 0;
  let r = strs.length - 1;
  while (l <= r) {
    // 头指针找元音字母
    while (!hash.has(strs[l]) && l <= r) {
      l++;
    }
    // 尾指针找元音字母
    while (!hash.has(strs[r]) && l <= r) {
      r--;
    }
    // 头尾都是元音字母，交换
    if (hash.has(strs[l]) && hash.has(strs[r])) {
      [strs[l], strs[r]] = [strs[r], strs[l]];
      l++;
      r--;
    }
  }

  return strs.join('');
}
```
