/*
 * @lc app=leetcode.cn id=1051 lang=cpp
 *
 * [1051] 高度检查器
 */

// @lc code=start
class Solution {
public:
    int heightChecker(vector<int> &heights) {
        int cnt[101] = {0};
        for (auto x : heights) cnt[x]++;

        int ans = 0;
        int idx = 0;
        for (int i = 1; i <= 100; i++) {
            while (cnt[i]--) {
                if (heights[idx++] != i) ans++;
            }
        }
        return ans;
    }
};
// @lc code=end
