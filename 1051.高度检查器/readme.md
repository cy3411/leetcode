# 题目

学校打算为全体学生拍一张年度纪念照。根据要求，学生需要按照 **非递减** 的高度顺序排成一行。

排序后的高度情况用整数数组 `expected` 表示，其中 `expected[i]` 是预计排在这一行中第 `i` 位的学生的高度（下标从 `0` 开始）。

给你一个整数数组 `heights` ，表示 **当前学生站位** 的高度情况。 `heights[i`] 是这一行中第 `i` 位学生的高度（下标从 `0` 开始）。

返回满足 $heights[i] != expected[i]$ 的 下标数量 。

提示：

- $1 \leq heights.length \leq 100$
- $1 \leq heights[i] \leq 100$

# 示例

```
输入：heights = [1,1,4,2,1,3]
输出：3
解释：
高度：[1,1,4,2,1,3]
预期：[1,1,1,2,3,4]
下标 2 、4 、5 处的学生高度不匹配。
```

```
输入：heights = [5,1,2,3,4]
输出：5
解释：
高度：[5,1,2,3,4]
预期：[1,2,3,4,5]
所有下标的对应学生高度都不匹配。
```

# 题解

## 排序后比较

将 heights 复制 到 expected 中，然后排序。

我们统计 heights 中不等于 expected 的下标的数量，即为答案。

```ts
function heightChecker(heights: number[]): number {
  const n = heights.length;
  const expected = [...heights];
  expected.sort((a, b) => a - b);
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (heights[i] !== expected[i]) {
      ans++;
    }
  }
  return ans;
}
```

## 计数排序

题目给出的数据范围最大为 100， 我们可以使用计数排序。

只需要判断对应位置的最小值是否匹配即可。

```cpp
class Solution {
public:
    int heightChecker(vector<int> &heights) {
        int cnt[101] = {0};
        for (auto x : heights) cnt[x]++;

        int ans = 0;
        int idx = 0;
        for (int i = 1; i <= 100; i++) {
            while (cnt[i]--) {
                if (heights[idx++] != i) ans++;
            }
        }
        return ans;
    }
};
```
