/*
 * @lc app=leetcode.cn id=1051 lang=typescript
 *
 * [1051] 高度检查器
 */

// @lc code=start
function heightChecker(heights: number[]): number {
  const n = heights.length;
  const expected = [...heights];
  expected.sort((a, b) => a - b);
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (heights[i] !== expected[i]) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
