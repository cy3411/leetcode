/*
 * @lc app=leetcode.cn id=191 lang=javascript
 *
 * [191] 位1的个数
 */

// @lc code=start
/**
 * @param {number} n - a positive integer
 * @return {number}
 */
var hammingWeight = function (n) {
  let result = 0;
  while (n !== 0) {
    // 每次消掉最后一位1
    n &= n - 1;
    result++;
  }
  return result;
};
// @lc code=end
