# 题目

给定一个 `m x n` 二维字符网格 `board` 和一个字符串单词 `word` 。如果 `word` 存在于网格中，返回 `true` ；否则，返回 `false` 。

单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。

提示：

- $m \equiv board.length$
- $n \equiv board[i].length$
- $1 \leq m, n \leq 6$
- $1 \leq word.length \leq 15$
- `board` 和 `word` 仅由大小写英文字母组成

进阶：你可以使用搜索剪枝的技术来优化解决方案，使其在 board 更大的情况下可以更快解决问题？

# 示例

[![HqfpiF.png](https://s4.ax1x.com/2022/02/20/HqfpiF.png)](https://imgtu.com/i/HqfpiF)

```
输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
输出：true
```

[![HqfCRJ.png](https://s4.ax1x.com/2022/02/20/HqfCRJ.png)](https://imgtu.com/i/HqfCRJ)

```
输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
输出：true
```

# 题解

## 深度优先搜索

```ts
function exist(board: string[][], word: string): boolean {
  const dfs = (i: number, j: number, k: number): boolean => {
    if (k === word.length) return true;
    for (let x = 0; x < 4; x++) {
      const ni = i + dirs[x];
      const nj = j + dirs[x + 1];
      if (ni < 0 || ni >= m) continue;
      if (nj < 0 || nj >= n) continue;
      if (visited[ni][nj] === 1) continue;
      if (board[ni][nj] !== word[k]) continue;
      visited[ni][nj] = 1;
      if (dfs(ni, nj, k + 1)) return true;
      visited[ni][nj] = 0;
    }
    return false;
  };
  // 方向数组
  const dirs = [0, 1, 0, -1, 0];
  const m = board.length;
  const n = board[0].length;
  // 记录访问过的单元格
  const visited: number[][] = new Array(m).fill(0).map(() => new Array(n).fill(0));

  // 遍历每个单元格，当作搜索起点
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (board[i][j] === word[0]) {
        visited[i][j] = 1;
        if (dfs(i, j, 1)) return true;
        visited[i][j] = 0;
      }
    }
  }

  return false;
}
```

```js
/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function (board, word) {
  if (board.length === 0 || word.length === 0) {
    return fasle;
  }
  let rows = board.length;
  let cols = board[0].length;
  const directions = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ];

  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < cols; j++) {
      const result = dfs(board, i, j, 0);
      if (result) {
        return true;
      }
    }
  }

  return false;

  /**
   * @description: 递归查询字符
   * @param {[][]} board
   * @param {number} i
   * @param {number} j
   * @param {number} index
   * @return {boolean}
   */
  function dfs(board, i, j, index) {
    if (board[i][j] !== word[index]) {
      return false;
    } else if (index === word.length - 1) {
      return true;
    }
    let char = word[index];
    let result = false;
    board[i][j] = 0;

    for (const [dx, dy] of directions) {
      let newi = dx + i;
      let newj = dy + j;
      if (newi >= 0 && newi < rows && newj >= 0 && newj < cols && board[newi][newj] !== 0) {
        const res = dfs(board, newi, newj, index + 1);
        if (res) {
          result = true;
          break;
        }
      }
    }

    board[i][j] = char;
    return result;
  }
};
```

```cpp
class Solution
{
    int m, n;
    int dirs[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

public:
    int dfs(vector<vector<char>> &board, string &word, vector<vector<int>> &mark, int i, int j, int len)
    {
        if (board[i][j] != word[len])
        {
            return 0;
        }
        if (len == word.size() - 1)
        {
            return 1;
        }
        mark[i][j] = 1;
        for (auto &d : dirs)
        {
            int ni = i + d[0];
            int nj = j + d[1];
            if (ni < 0 || ni >= m || nj < 0 || nj >= n || mark[ni][nj] == 1)
            {
                continue;
            }
            if (dfs(board, word, mark, ni, nj, len + 1))
            {
                return 1;
            }
        }
        mark[i][j] = 0;
        return 0;
    }
    bool exist(vector<vector<char>> &board, string word)
    {
        m = board.size(), n = board[0].size();
        vector<vector<int>> mark(m, vector<int>(n, 0));
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (board[i][j] == word[0])
                {
                    if (dfs(board, word, mark, i, j, 0))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
};
```
