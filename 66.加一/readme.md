# 题目

给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。

最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。

你可以假设除了整数 0 之外，这个整数不会以零开头。

提示：

- `1 <= digits.length <= 100`
- `0 <= digits[i] <= 9`

# 示例

```
输入：digits = [1,2,3]
输出：[1,2,4]
解释：输入数组表示数字 123。
```

# 题解

## 模拟

题目固定了只能加 1，所以只要找低位有几个 9 就行了。

离 9 最近的数加 1，所有的 9 置 0。

```ts
function plusOne(digits: number[]): number[] {
  const n = digits.length;

  for (let i = n - 1; i >= 0; i--) {
    digits[i]++;
    digits[i] %= 10;
    // 如果加1后，没有变成10，就直接返回结果
    if (digits[i] !== 0) {
      return digits;
    }
  }
  // 表示前面都有进位
  return [1].concat(new Array(n).fill(0));
}
```
