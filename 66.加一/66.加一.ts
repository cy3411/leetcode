/*
 * @lc app=leetcode.cn id=66 lang=typescript
 *
 * [66] 加一
 */

// @lc code=start
function plusOne(digits: number[]): number[] {
  const n = digits.length;

  for (let i = n - 1; i >= 0; i--) {
    digits[i]++;
    digits[i] %= 10;
    // 如果加1后，没有变成10，就直接返回结果
    if (digits[i] !== 0) {
      return digits;
    }
  }
  // 表示前面都有进位
  return [1].concat(new Array(n).fill(0));
}
// @lc code=end
