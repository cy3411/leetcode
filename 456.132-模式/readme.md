# 题目

给你一个整数数组 nums ，数组中共有 n 个整数。132 模式的子序列 由三个整数 nums[i]、nums[j] 和 nums[k] 组成，并同时满足：`i < j < k` 和 `nums[i] < nums[k] < nums[j]` 。

如果 nums 中存在 132 模式的子序列 ，返回 true ；否则，返回 false 。

进阶：很容易想到时间复杂度为 O(n^2) 的解决方案，你可以设计一个时间复杂度为 O(n logn) 或 O(n) 的解决方案吗？

提示：

- n == nums.length
- 1 <= n <= 104
- -109 <= nums[i] <= 109

# 示例

```
输入：nums = [1,2,3,4]
输出：false
解释：序列中不存在 132 模式的子序列。
```

```
输入：nums = [3,1,4,2]
输出：true
解释：序列中有 1 个 132 模式的子序列： [1, 4, 2] 。
```

# 方法

## 暴力

从左到右遍历一次，遍历的数字是 `nums[j]` 也就是 132 模式中的 3。根据上面的贪心思想分析，我们想让 1 是 3 左边最小的元素，然后使用暴力在 `nums[j+1..N−1]` 中找到 132 模式中的 2 就行。

```js
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var find132pattern = function (nums) {
  const n = nums.length;
  let numi = nums[0];
  for (let j = 1; j < n; j++) {
    for (let k = n - 1; k > j; k--) {
      if (numi < nums[k] && nums[k] < nums[j]) {
        return true;
      }
    }
    numi = Math.min(numi, nums[j]);
  }
  return false;
};
```

## 单调栈

我们维护 132 模式中的 3，希望 1 尽可能的小，2 尽可能的大

- 遍历位置 j，nums[j]
- 找到 j 左边最小的元素，nums[i]
- 找到 j 右边最大的元素，nums[k]

实现方式：

- 提前求出任意位置左边最小元素，num[i]
- 使用单调递减栈，把 nums[j]入栈，提前把小于它的元素全部出栈，这样栈底的元素就是小于 3 的最大元素 nums[k]
- 判断 nums[i]是否小于 nums[k]

```js
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var find132pattern = function (nums) {
  const n = nums.length;
  const left = new Array(n).fill(Number.POSITIVE_INFINITY);

  // 任意位置左边最小元素
  for (let i = 1; i < n; i++) {
    left[i] = Math.min(left[i - 1], nums[i - 1]);
  }

  const stack = [];
  for (let j = n - 1; j >= 0; j--) {
    let right = Number.NEGATIVE_INFINITY;
    // nums[j]大于栈顶的话，出栈，栈中最大的元素赋值给right
    while (stack.length && nums[j] > stack[stack.length - 1]) {
      right = stack.pop();
    }
    // 当前j位置的左边最小元素小于right的话，就是132模式 nums[i]<nums[j]>nums[k]
    if (left[j] < right) return true;
    stack.push(nums[j]);
  }

  return false;
};
```

```ts
function find132pattern(nums: number[]): boolean {
  const m = nums.length;
  // 保存每个元素左边的最小值
  const l = new Array(m).fill(Number.POSITIVE_INFINITY);
  for (let i = 1; i < m; i++) {
    l[i] = Math.min(nums[i - 1], l[i - 1]);
  }

  const stack: number[] = [];
  // 倒叙遍历，去找小于当前元素的第一个值，也就是132中的2
  for (let i = m - 1; i >= 0; i--) {
    let r: number = Number.NEGATIVE_INFINITY;
    while (stack.length && nums[i] > stack[stack.length - 1]) {
      r = stack.pop();
    }
    if (l[i] < r && l[i] < nums[i] && r < nums[i]) return true;
    stack.push(nums[i]);
  }

  return false;
}
```
