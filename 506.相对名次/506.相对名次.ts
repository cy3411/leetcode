/*
 * @lc app=leetcode.cn id=506 lang=typescript
 *
 * [506] 相对名次
 */

// @lc code=start
function findRelativeRanks(score: number[]): string[] {
  const hash = new Map<number, number>();
  // 降序排序
  const sorted = [...score].sort((a, b) => b - a);

  // 记录排名
  for (let i = 0; i < sorted.length; i++) {
    hash.set(sorted[i], i + 1);
  }

  // 相对排名
  const ans = score.map((s) => {
    const rank = hash.get(s);
    switch (rank) {
      case 1:
        return 'Gold Medal';
      case 2:
        return 'Silver Medal';
      case 3:
        return 'Bronze Medal';
      default:
        return String(rank);
    }
  });

  return ans;
}
// @lc code=end
