# 题目

给你一个长度为 `n` 的整数数组 `score` ，其中 `score[i]` 是第 `i` 位运动员在比赛中的得分。所有得分都 **互不相同** 。

运动员将根据得分 **决定名次** ，其中名次第 `1` 的运动员得分最高，名次第 `2` 的运动员得分第 `2` 高，依此类推。运动员的名次决定了他们的获奖情况：

- 名次第 `1` 的运动员获金牌 "Gold Medal" 。
- 名次第 `2` 的运动员获银牌 "Silver Medal" 。
- 名次第 `3` 的运动员获铜牌 "Bronze Medal" 。
- 从名次第 `4` 到第 `n` 的运动员，只能获得他们的名次编号（即，名次第 x 的运动员获得编号 "x"）。

使用长度为 `n` 的数组 `answer` 返回获奖，其中 `answer[i]` 是第 `i` 位运动员的获奖情况。

提示：

- `n == score.length`
- $1 \leq n \leq 10^4$
- $0 \leq score[i] \leq 10^6$
- `score` 中的所有值 **互不相同**

# 示例

```
输入：score = [5,4,3,2,1]
输出：["Gold Medal","Silver Medal","Bronze Medal","4","5"]
解释：名次为 [1st, 2nd, 3rd, 4th, 5th] 。
```

```
输入：score = [10,3,8,9,4]
输出：["Gold Medal","5","Bronze Medal","Silver Medal","4"]
解释：名次为 [1st, 5th, 3rd, 2nd, 4th] 。
```

# 题解

## 哈希表

将 `score` 排序，然后按照 `score` 的顺序构建哈希表。

最后遍历 `score`，按照哈希表中的顺序构建 `ans`。

```ts
function findRelativeRanks(score: number[]): string[] {
  const hash = new Map<number, number>();
  // 降序排序
  const sorted = [...score].sort((a, b) => b - a);

  // 记录排名
  for (let i = 0; i < sorted.length; i++) {
    hash.set(sorted[i], i + 1);
  }

  // 相对排名
  const ans = score.map((s) => {
    const rank = hash.get(s);
    switch (rank) {
      case 1:
        return 'Gold Medal';
      case 2:
        return 'Silver Medal';
      case 3:
        return 'Bronze Medal';
      default:
        return String(rank);
    }
  });

  return ans;
}
```
