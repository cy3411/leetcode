/*
 * @lc app=leetcode.cn id=1812 lang=typescript
 *
 * [1812] 判断国际象棋棋盘中一个格子的颜色
 */

// @lc code=start
function squareIsWhite(coordinates: string): boolean {
  const base = 'a'.charCodeAt(0);
  const cell = coordinates.split('');
  // 行
  const row = Number(cell[1]) - 1;
  // 列
  const col = cell[0].charCodeAt(0) - base;

  if (row & 1) {
    if (col & 1) return false;
    else return true;
  } else {
    if (col & 1) return true;
    else return false;
  }
}
// @lc code=end
