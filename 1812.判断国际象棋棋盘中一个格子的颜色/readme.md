# 题目

给你一个坐标 `coordinates` ，它是一个字符串，表示国际象棋棋盘中一个格子的坐标。下图是国际象棋棋盘示意图。

[![zRApVI.png](https://s1.ax1x.com/2022/12/08/zRApVI.png)](https://imgse.com/i/zRApVI)

如果所给格子的颜色是白色，请你返回 `true`，如果是黑色，请返回 `false` 。

给定坐标一定代表国际象棋棋盘上一个存在的格子。坐标第一个字符是字母，第二个字符是数字。

提示：

- $coordinates.length \equiv 2$
- $'a' \leq coordinates[0] \leq 'h'$
- $'1' \leq coordinates[1] \leq '8'$

# 示例

```
输入：coordinates = "a1"
输出：false
解释：如上图棋盘所示，"a1" 坐标的格子是黑色的，所以返回 false 。
```

```
输入：coordinates = "h3"
输出：true
解释：如上图棋盘所示，"h3" 坐标的格子是白色的，所以返回 true 。
```

# 题解

## 模拟

将棋盘按照从下到上，从左到右的查看，可以看出偶数行中，偶数列是黑，奇数列是白，奇数行中，偶数列是白，奇数列是黑。

按照上面的规则判断即可。

```ts
function squareIsWhite(coordinates: string): boolean {
  const base = 'a'.charCodeAt(0);
  const cell = coordinates.split('');
  // 行
  const row = Number(cell[1]) - 1;
  // 列
  const col = cell[0].charCodeAt(0) - base;

  if (row & 1) {
    if (col & 1) return false;
    else return true;
  } else {
    if (col & 1) return true;
    else return false;
  }
}
```

```py
class Solution:
    def squareIsWhite(self, coordinates: str) -> bool:
        return (ord(coordinates[0]) - ord('a') + 1 + int(coordinates[1])) % 2 == 1
```

```cpp
class Solution {
public:
    bool squareIsWhite(string coordinates) {
        return ((coordinates[0] - 'a' + 1) + (coordinates[1] - '0')) % 2 == 1;
    }
};
```
