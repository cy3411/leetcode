# 题目
请考虑一棵二叉树上所有的叶子，这些叶子的值按从左到右的顺序排列形成一个 **叶值序列** 。

![gt1W9A.md.png](https://z3.ax1x.com/2021/05/10/gt1W9A.md.png)]

举个例子，如上图所示，给定一棵叶值序列为 `(6, 7, 4, 9, 8)` 的树。

如果有两棵二叉树的叶值序列是相同，那么我们就认为它们是 叶相似 的。

如果给定的两个根结点分别为 `root1` 和 `root2` 的树是叶相似的，则返回 `true`；否则返回 `false` 。

提示：
+ 给定的两棵树可能会有 1 到 200 个结点。
+ 给定的两棵树上的值介于 0 到 200 之间。

# 示例
![gt1qhj.md.png](https://z3.ax1x.com/2021/05/10/gt1qhj.md.png)]
```
输入：root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 = [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
输出：true
```
# 题解
## 递归
递归找出2棵树所有叶子节点，然后对比返回结果。
```ts
function leafSimilar(root1: TreeNode | null, root2: TreeNode | null): boolean {
  const getLeafNodes = (root: TreeNode | null, result: number[]): void => {
    if (root.left === null && root.right === null) {
      result.push(root.val);
      return;
    }
    root.left && getLeafNodes(root.left, result);
    root.right && getLeafNodes(root.right, result);
  };

  const isSameVal = (a: number[], b: number[]): boolean => {
    if (a.length !== b.length) return false;
    let i = 0,
      j = 0;
    while (i < a.length) {
      if (a[i++] !== b[j++]) return false;
    }
    return true;
  };

  const leafs1 = [];
  getLeafNodes(root1, leafs1);
  const leafs2 = [];
  getLeafNodes(root2, leafs2);
  return isSameVal(leafs1, leafs2);
}
```

## 迭代
```ts
function leafSimilar(root1: TreeNode | null, root2: TreeNode | null): boolean {
  const getLeafNodes = (root: TreeNode | null, result: number[]): void => {
    const stack = [];
    while (root || stack.length) {
      while (root) {
        stack.push(root);
        root = root.left;
      }
      root = stack.pop();
      if (root.left === null && root.right === null) {
        result.push(root.val);
      }
      root = root.right;
    }
  };

  const isSameVal = (a: number[], b: number[]): boolean => {
    if (a.length !== b.length) return false;
    let i = 0,
      j = 0;
    while (i < a.length) {
      if (a[i++] !== b[j++]) return false;
    }
    return true;
  };

  const leafs1 = [];
  getLeafNodes(root1, leafs1);
  const leafs2 = [];
  getLeafNodes(root2, leafs2);
  return isSameVal(leafs1, leafs2);
}
```