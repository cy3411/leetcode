/*
 * @lc app=leetcode.cn id=142 lang=javascript
 *
 * [142] 环形链表 II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var detectCycle = function (head) {
  if (!head || !head.next) {
    return null
  }

  let slow = head, fast = head

  while (fast !== null && fast.next !== null) {
    // 假设从头结点到环形入口节点 的节点数为x。
    // 环形入口节点到 fast指针与slow指针相遇节点 节点数为y。
    // 从相遇节点 再到环形入口节点节点数为 z。
    // (x + y) * 2 = x + y + n (y + z)
    // x + y = n (y + z) 
    // x = n (y + z) - y
    // x = (n - 1) (y + z) + z 
    slow = slow.next
    fast = fast.next.next
    if (fast === slow) {
      let hIdx = head
      let fIdx = fast
      while (hIdx !== fIdx) {
        hIdx = hIdx.next
        fIdx = fIdx.next
      }
      return hIdx
    }
  }
  return null
};
// @lc code=end

