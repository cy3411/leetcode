/*
 * @lc app=leetcode.cn id=565 lang=typescript
 *
 * [565] 数组嵌套
 */

// @lc code=start
function arrayNesting(nums: number[]): number {
  const n = nums.length;
  const visited = new Array(n).fill(0);

  let ans = 0;
  for (let i = 0; i < n; i++) {
    let count = 0;
    while (!visited[i]) {
      visited[i] = 1;
      count++;
      i = nums[i];
    }
    ans = Math.max(ans, count);
  }
  return ans;
}
// @lc code=end
