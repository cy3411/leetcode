# 题目

索引从`0`开始长度为`N`的数组`A`，包含`0`到`N - 1`的所有整数。找到最大的集合`S`并返回其大小，其中 `S[i] = {A[i], A[A[i]], A[A[A[i]]], ... }`且遵守以下的规则。

假设选择索引为`i`的元素`A[i]`为`S`的第一个元素，`S`的下一个元素应该是`A[A[i]]`，之后是`A[A[A[i]]]...` 以此类推，不断添加直到`S`出现重复的元素。

提示：

- `N`是`[1, 20,000]`之间的整数。
- `A`中不含有重复的元素。
- `A`中的元素大小在`[0, N-1]`之间。

# 示例

```
输入: A = [5,4,0,3,1,6,2]
输出: 4
解释:
A[0] = 5, A[1] = 4, A[2] = 0, A[3] = 3, A[4] = 1, A[5] = 6, A[6] = 2.

其中一种最长的 S[K]:
S[0] = {A[0], A[5], A[6], A[2]} = {5, 6, 2, 0}
```

# 题解

## 有向图

根据题意，我们从 i 向 nums[i] 连变，可得一张有向图。

题目保证不会出现重复的元素，因此每个点的出入度都是 1。

这种情况下，必然会形成一个环，我们只要找到节点个数最多的环即可。

遍历数组，把每个元素当作环的起点来搜索，同时使用 visited 数组记录节点是否被访问过，如果被访问过，则说明该节点已经在环中了。每个起点搜索结束后，同步更新答案，取最大的结果。

```ts
function arrayNesting(nums: number[]): number {
  const n = nums.length;
  const visited = new Array(n).fill(0);

  let ans = 0;
  for (let i = 0; i < n; i++) {
    let count = 0;
    while (!visited[i]) {
      visited[i] = 1;
      count++;
      i = nums[i];
    }
    ans = Math.max(ans, count);
  }
  return ans;
}
```

```cpp
class Solution {
public:
    int arrayNesting(vector<int> &nums) {
        int n = nums.size(), ans = 0;

        for (int i = 0; i < n; i++) {
            int count = 0;
            while (nums[i] != n) {
                int num = nums[i];
                nums[i] = n;
                i = num;
                count++;
            }
            ans = max(ans, count);
        }

        return ans;
    }
};
```

```py
class Solution:
    def arrayNesting(self, nums: List[int]) -> int:
        n = len(nums)
        ans = 0
        for i in range(n):
            count = 0
            while (nums[i] != n):
                num = nums[i]
                nums[i] = n
                i = num
                count += 1
            ans = max(ans, count)
        return ans
```
