#
# @lc app=leetcode.cn id=565 lang=python3
#
# [565] 数组嵌套
#

# @lc code=start
class Solution:
    def arrayNesting(self, nums: List[int]) -> int:
        n = len(nums)
        ans = 0
        for i in range(n):
            count = 0
            while (nums[i] != n):
                num = nums[i]
                nums[i] = n
                i = num
                count += 1
            ans = max(ans, count)
        return ans
# @lc code=end
