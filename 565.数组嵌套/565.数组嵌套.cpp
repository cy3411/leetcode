/*
 * @lc app=leetcode.cn id=565 lang=cpp
 *
 * [565] 数组嵌套
 */

// @lc code=start
class Solution {
public:
    int arrayNesting(vector<int> &nums) {
        int n = nums.size(), ans = 0;

        for (int i = 0; i < n; i++) {
            int count = 0;
            while (nums[i] != n) {
                int num = nums[i];
                nums[i] = n;
                i = num;
                count++;
            }
            ans = max(ans, count);
        }

        return ans;
    }
};
// @lc code=end
