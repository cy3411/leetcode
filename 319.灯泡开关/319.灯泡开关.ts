/*
 * @lc app=leetcode.cn id=319 lang=typescript
 *
 * [319] 灯泡开关
 */

// @lc code=start
function bulbSwitch(n: number): number {
  return Math.sqrt(n) >> 0;
}
// @lc code=end
