/*
 * @lc app=leetcode.cn id=1805 lang=cpp
 *
 * [1805] 字符串中不同整数的数目
 */

// @lc code=start
class Solution {
public:
    int numDifferentIntegers(string word) {
        unordered_set<string> mp;
        int n = word.size();
        int i = 0;
        while (i < n) {
            while (i < n && word[i] >= 'a' && word[i] <= 'z') {
                i++;
            }
            if (i == n) break;
            int j = i;
            while (i < n && word[i] >= '0' && word[i] <= '9') {
                i++;
            }
            while (i - j > 1 && word[j] == '0') {
                j++;
            }
            string substr;
            while (j < i) {
                substr += word[j++];
            }
            mp.insert(substr);
        }
        return mp.size();
    }
};
// @lc code=end
