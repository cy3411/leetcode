#
# @lc app=leetcode.cn id=1805 lang=python3
#
# [1805] 字符串中不同整数的数目
#

# @lc code=start
class Solution:
    def numDifferentIntegers(self, word: str) -> int:
        mp = set()
        n = len(word)
        i = 0
        while i < n:
            while i < n and not word[i].isdigit():
                i += 1
            if i == n:
                break
            j = i
            while i < n and word[i].isdigit():
                i += 1
            while i - j > 1 and word[j] == '0':
                j += 1
            substr = ''
            while j < i:
                substr += word[j]
                j += 1
            mp.add(substr)
        return len(mp)
# @lc code=end
