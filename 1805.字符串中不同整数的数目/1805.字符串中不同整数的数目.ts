/*
 * @lc app=leetcode.cn id=1805 lang=typescript
 *
 * [1805] 字符串中不同整数的数目
 */

// @lc code=start
function numDifferentIntegers(word: string): number {
  const mp = new Set<string>();
  const n = word.length;

  let i = 0;
  while (i < n) {
    // 跳过字符
    while (word[i] >= 'a' && word[i] <= 'z' && i < n) {
      i++;
    }
    if (i === n) break;

    let substr = '';
    let j = i;
    // 获取整数字符串的结束位置
    while (i < n && word[i] >= '0' && word[i] <= '9') {
      i++;
    }
    // 消除前导0
    while (i - j > 1 && word[j] === '0') {
      j++;
    }
    // 获取整数字符串
    while (j < i) {
      substr += word[j++];
    }

    // 将整数字符串放入哈希表
    mp.add(substr);
  }

  return mp.size;
}
// @lc code=end
