# 题目

给你一个字符串 `word` ，该字符串由数字和小写英文字母组成。

请你用空格替换每个不是数字的字符。例如，`"a123bc34d8ef34"` 将会变成 `" 123 34 8 34"` 。注意，剩下的这些整数为（相邻彼此至少有一个空格隔开）：`"123"`、`"34"`、`"8"` 和 `"34"` 。

返回对 `word` 完成替换后形成的 **不同** 整数的数目。

只有当两个整数的 **不含前导零** 的十进制表示不同， 才认为这两个整数也不同。

提示：

- $1 \leq word.length \leq 1000$
- word 由数字和小写英文字母组成

# 示例

```
输入：word = "a123bc34d8ef34"
输出：3
解释：不同的整数有 "123"、"34" 和 "8" 。注意，"34" 只计数一次。
```

```
输入：word = "a1b01c001"
输出：1
解释："1"、"01" 和 "001" 视为同一个整数的十进制表示，因为在比较十进制值时会忽略前导零的存在。
```

# 题解

## 遍历

遍历字符串，取出整数部分，使用哈希表保存出现的整数，最后返回哈希表的长度即可。

```ts
function numDifferentIntegers(word: string): number {
  const mp = new Set<string>();
  const n = word.length;

  let i = 0;
  while (i < n) {
    // 跳过字符
    while (word[i] >= 'a' && word[i] <= 'z' && i < n) {
      i++;
    }
    if (i === n) break;

    let substr = '';
    let j = i;
    // 获取整数字符串的结束位置
    while (i < n && word[i] >= '0' && word[i] <= '9') {
      i++;
    }
    // 消除前导0
    while (i - j > 1 && word[j] === '0') {
      j++;
    }
    // 获取整数字符串
    while (j < i) {
      substr += word[j++];
    }

    // 将整数字符串放入哈希表
    mp.add(substr);
  }

  return mp.size;
}
```

```cpp
class Solution {
public:
    int numDifferentIntegers(string word) {
        unordered_set<string> mp;
        int n = word.size();
        int i = 0;
        while (i < n) {
            while (i < n && word[i] >= 'a' && word[i] <= 'z') {
                i++;
            }
            if (i == n) break;
            int j = i;
            while (i < n && word[i] >= '0' && word[i] <= '9') {
                i++;
            }
            while (i - j > 1 && word[j] == '0') {
                j++;
            }
            string substr;
            while (j < i) {
                substr += word[j++];
            }
            mp.insert(substr);
        }
        return mp.size();
    }
};
```

```py
class Solution:
    def numDifferentIntegers(self, word: str) -> int:
        mp = set()
        n = len(word)
        i = 0
        while i < n:
            while i < n and not word[i].isdigit():
                i += 1
            if i == n:
                break
            j = i
            while i < n and word[i].isdigit():
                i += 1
            while i - j > 1 and word[j] == '0':
                j += 1
            substr = ''
            while j < i:
                substr += word[j]
                j += 1
            mp.add(substr)
        return len(mp)
```
