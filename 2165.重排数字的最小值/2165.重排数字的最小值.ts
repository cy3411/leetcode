/*
 * @lc app=leetcode.cn id=2165 lang=typescript
 *
 * [2165] 重排数字的最小值
 */

// @lc code=start
function smallestNumber(num: number): number {
  if (num === 0) return 0;

  // 正数为1，负数为-1
  let isFlag = 1;
  if (num < 0) {
    isFlag = -1;
    num = -num;
  }
  // 记录每个数字出现的次数
  const count = new Array(10).fill(0);
  while (num) {
    count[num % 10]++;
    num = Math.floor(num / 10);
  }

  let ans = 0;
  // 如果为正数，取出最小的非0数字做为最高位
  if (isFlag === 1) {
    for (let i = 1; i < 10; i++) {
      if (count[i] === 0) continue;
      ans = i;
      count[i]--;
      break;
    }
  }

  // 正数的话，顺序遍历
  // 负数的话，逆序遍历
  let i = isFlag === 1 ? 0 : 9;
  const n = 9 - i;
  for (; i * isFlag <= n; i += isFlag) {
    for (let j = 0; j < count[i]; j++) {
      ans = ans * 10 + i;
    }
  }

  return isFlag === 1 ? ans : -ans;
}
// @lc code=end
