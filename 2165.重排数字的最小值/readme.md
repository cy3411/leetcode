# 题目

给你一个整数 `num` 。重排 `num` 中的各位数字，使其值 **最小化** 且不含 **任何** 前导零。

返回不含前导零且值最小的重排数字。

注意，重排各位数字后，**num** 的符号不会改变。

提示：

- $-10^{15} \leq num \leq 10^{15}$

# 示例

```
输入：num = 310
输出：103
解释：310 中各位数字的可行排列有：013、031、103、130、301、310 。
不含任何前导零且值最小的重排数字是 103 。
```

```
输入：num = -7605
输出：-7650
解释：-7605 中各位数字的部分可行排列为：-7650、-6705、-5076、-0567。
不含任何前导零且值最小的重排数字是 -7650 。
```

# 题解

## 排序

将数字按照顺序排序，然后拼接起来。

- 如果是正数，取出数字中最小的非 `0` 数字做为最高位，剩下位按照顺序拼接
- 如果是负数，逆序拼接即可

```ts
function smallestNumber(num: number): number {
  if (num === 0) return 0;

  // 正数为1，负数为-1
  let isFlag = 1;
  if (num < 0) {
    isFlag = -1;
    num = -num;
  }
  // 记录每个数字出现的次数
  const count = new Array(10).fill(0);
  while (num) {
    count[num % 10]++;
    num = Math.floor(num / 10);
  }

  let ans = 0;
  // 如果为正数，取出最小的非0数字做为最高位
  if (isFlag === 1) {
    for (let i = 1; i < 10; i++) {
      if (count[i] === 0) continue;
      ans = i;
      count[i]--;
      break;
    }
  }

  // 正数的话，顺序遍历
  // 负数的话，逆序遍历
  let i = isFlag === 1 ? 0 : 9;
  const n = 9 - i;
  for (; i * isFlag <= n; i += isFlag) {
    for (let j = 0; j < count[i]; j++) {
      ans = ans * 10 + i;
    }
  }

  return isFlag === 1 ? ans : -ans;
}
```

```cpp
class Solution
{
public:
    long long smallestNumber(long long num)
    {
        if (num == 0)
            return 0;

        int isFlag = 1;
        if (num < 0)
        {
            isFlag = -1;
            num = -num;
        }

        int count[10] = {0};
        while (num)
        {
            count[num % 10]++;
            num /= 10;
        }

        long long ans = 0;
        if (isFlag == 1)
        {
            for (int i = 1; i < 10; i++)
            {
                if (count[i] == 0)
                    continue;
                ans = i;
                count[i]--;
                break;
            }
        }

        int i = isFlag == 1 ? 0 : 9;
        int n = 9 - i;
        for (; i * isFlag <= n; i += isFlag)
        {
            for (int j = 0; j < count[i]; j++)
            {
                ans = ans * 10 + i;
            }
        }

        return isFlag == 1 ? ans : -ans;
    }
};
```
