# 题目

如果一个由 `'0'` 和 `'1'` 组成的字符串，是以一些 `'0'`（可能没有 `'0'`）后面跟着一些 `'1'`（也可能没有 `'1'`）的形式组成的，那么该字符串是单调递增的。

我们给出一个由字符 `'0'` 和 `'1'` 组成的字符串 `S`，我们可以将任何 `'0'` 翻转为 `'1'` 或者将 `'1'` 翻转为 `'0'`。

返回使 `S` 单调递增的最小翻转次数。

提示：

- $\color{burlywood}1 \leq S.length \leq 20000$
- `S` 中只包含字符 `'0'` 和 `'1'`

# 示例

```
输入："00110"
输出：1
解释：我们翻转最后一位得到 00111.
```

```
输入："010110"
输出：2
解释：我们翻转得到 011111，或者是 000111。
```

# 题解

## 枚举

枚举每个位置，判断当前位置前 `1` 的数量和当前位置后 `0` 的数量，这个表示需要反转的次数。

从每个位置的反转次数中选出最小的。

```ts
function minFlipsMonoIncr(s: string): number {
  let n0 = 0;
  let n1 = 0;
  for (const x of s) {
    if (x === '0') n0++;
  }

  // 默认是反转所有0为答案
  let ans = n0;
  // 枚举每个位置，判断当前位置后面0的数量和前面1的数量，表示需要反转的次数
  // 取中间需要反转的次数最小值
  for (const x of s) {
    if (x === '1') n1++;
    if (x === '0') n0--;
    ans = Math.min(ans, n0 + n1);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int minFlipsMonoIncr(string s)
    {
        int a0 = 0, a1 = 0;
        for (char c : s)
        {
            if (c == '0')
            {
                a0++;
            }
        }

        int ans = a0;
        for (char c : s)
        {
            if (c == '1')
            {
                a1++;
            }
            else
            {
                a0--;
            }
            ans = min(ans, a0 + a1);
        }

        return ans;
    }
};
```

```py
class Solution:
    def minFlipsMonoIncr(self, s: str) -> int:
        n0 = 0
        n1 = 0
        for c in s:
            if c == '0':
                n0 += 1

        ans = n0
        for c in s:
            if c == '1':
                n1 += 1
            elif c == '0':
                n0 -= 1
            ans = min(ans, n0 + n1)
        return ans
```
