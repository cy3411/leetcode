#
# @lc app=leetcode.cn id=926 lang=python3
#
# [926] 将字符串翻转到单调递增
#

# @lc code=start
class Solution:
    def minFlipsMonoIncr(self, s: str) -> int:
        n0 = 0
        n1 = 0
        for c in s:
            if c == '0':
                n0 += 1

        ans = n0
        for c in s:
            if c == '1':
                n1 += 1
            elif c == '0':
                n0 -= 1
            ans = min(ans, n0 + n1)
        return ans
# @lc code=end
