/*
 * @lc app=leetcode.cn id=926 lang=typescript
 *
 * [926] 将字符串翻转到单调递增
 */

// @lc code=start
function minFlipsMonoIncr(s: string): number {
  let n0 = 0;
  let n1 = 0;
  for (const x of s) {
    if (x === '0') n0++;
  }

  // 默认是反转所有0为答案
  let ans = n0;
  // 枚举每个位置，判断当前位置后面0的数量和前面1的数量，表示需要反转的次数
  // 取中间需要反转的次数最小值
  for (const x of s) {
    if (x === '1') n1++;
    if (x === '0') n0--;
    ans = Math.min(ans, n0 + n1);
  }

  return ans;
}
// @lc code=end
