/*
 * @lc app=leetcode.cn id=926 lang=cpp
 *
 * [926] 将字符串翻转到单调递增
 */

// @lc code=start
class Solution
{
public:
    int minFlipsMonoIncr(string s)
    {
        int a0 = 0, a1 = 0;
        for (char c : s)
        {
            if (c == '0')
            {
                a0++;
            }
        }

        int ans = a0;
        for (char c : s)
        {
            if (c == '1')
            {
                a1++;
            }
            else
            {
                a0--;
            }
            ans = min(ans, a0 + a1);
        }

        return ans;
    }
};
// @lc code=end
