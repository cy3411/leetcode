/*
 * @lc app=leetcode.cn id=24 lang=javascript
 *
 * [24] 两两交换链表中的节点
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
/**
 * 时间复杂度：O(n)，其中 n 是链表的节点数量。需要对每个节点进行更新指针的操作。
 * 空间复杂度：O(1)。
 */
var swapPairs = function (head) {
  const dummpHead = new ListNode(0)
  dummpHead.next = head
  let temp = dummpHead

  while (temp.next !== null && temp.next.next !== null) {
    // 需要交换的node
    const n1 = temp.next
    const n2 = n1.next
    temp.next = n2
    n1.next = n2.next
    n2.next = n1
    temp = n1
  }

  return dummpHead.next
};

// @lc code=end

