# 题目

列表 `arr` 由在范围 `[1, n]` 中的所有整数组成，并按严格递增排序。请你对 `arr` 应用下述算法：

- 从左到右，删除第一个数字，然后每隔一个数字删除一个，直到到达列表末尾。
- 重复上面的步骤，但这次是从右到左。也就是，删除最右侧的数字，然后剩下的数字每隔一个删除一个。
- 不断重复这两步，从左到右和从右到左交替进行，直到只剩下一个数字。

给你整数 `n` ，返回 `arr` 最后剩下的数字。

提示：

- $\color{burlywood}1 \leq n \leq 10^9$

# 示例

```
输入：n = 9
输出：6
解释：
arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
arr = [2, 4, 6, 8]
arr = [2, 6]
arr = [6]
```

```
输入：n = 1
输出：1
```

# 题解

## 模拟

假设 `k` 为扫描次数， `step` 为步进， `count` 为当前元素的总数量。

每次扫描后，`count` 的数量会变为 `count/2`，step 会变为 `step*2`。

题意要求 `count` 的数量为 1 的时候，返回第一个元素的值。

所以我们只要在每次扫描的时候，计算首元素的值，同时更新 `count` 和 `step` 的值，就可以得到最终的结果。

需要注意的是，反向扫描的时候，如果 `count` 为偶数，那么首元素是不会被删除的，需要保留原值，否则需要更新首元素的值。

```ts
function lastRemaining(n: number): number {
  let ans = 1;
  // 扫描的次数
  let k = 0;
  // 等差数列的步进
  let step = 1;
  // 元素的数量
  let count = n;

  while (count > 1) {
    if (k % 2 === 0) {
      //正向扫描
      ans += step;
    } else {
      // 反向扫描
      ans = count % 2 === 0 ? ans : ans + step;
    }
    k++;
    step = step << 1;
    count = count >> 1;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int lastRemaining(int n)
    {
        int ans = 1;
        int k = 0;
        int step = 1;
        int count = n;

        while (count > 1)
        {
            if (k % 2 == 0)
            {
                ans += step;
            }
            else
            {
                ans = (count % 2 == 0) ? ans : ans + step;
            }
            k++;
            step *= 2;
            count /= 2;
        }

        return ans;
    }
};
```
