/*
 * @lc app=leetcode.cn id=2140 lang=typescript
 *
 * [2140] 解决智力问题
 */

// @lc code=start
function mostPoints(questions: number[][]): number {
  const n = questions.length;
  // dp[i]表示以i为开始到结尾的区间的能获得最高分数
  const dp = new Array(n + 1).fill(0);

  // dp[i]的状态转移依赖后面的状态，这里需要逆序遍历
  for (let i = n - 1; i >= 0; i--) {
    dp[i] = Math.max(dp[i + 1], dp[Math.min(i + questions[i][1] + 1, n)] + questions[i][0]);
  }

  return dp[0];
}
// @lc code=end
