# 题目
给定一个二叉树，返回它的 前序 遍历。

# 示例
```js
输入: [1,null,2,3]  
   1
    \
     2
    /
   3 

输出: [1,2,3]
```

# 方法
## 递归
前序遍历就是按照root->left->right顺序访问。
```js
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
  // 前序遍历以root为根的节点
  const preorder = (root, result) => {
    if (root === null) return;
    // 输出root的节点
    result.push(root.val);
    preorder(root.left, result);
    preorder(root.right, result);
    return;
  };

  const result = [];
  preorder(root, result);

  return result;
};
```

## 迭代
单栈模拟执行顺序。
```js
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
  const result = [];

  if (root === null) return result;

  const stack = [];
  let node = root;

  while (stack.length || node !== null) {
    while (node) {
      result.push(node.val);
      stack.push(node);
      node = node.left;
    }
    node = stack.pop();
    node = node.right;
  }

  return result;
};
```

```js
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
  const result = [];

  if (root === null) return result;

  const stack = [root];

  while (stack.length) {
    let node = stack.pop();
    result.push(node.val);
    if (node.right) stack.push(node.right);
    if (node.left) stack.push(node.left);
  }

  return result;
};
```

## Morris遍历
Morris 遍历的核心思想是利用树的大量空闲指针，实现空间开销的极限缩减。
前序遍历的思想:
+ 定义`current=root`,定义`mostRight`为前序节点
+ 如果current.left === null，将current加入答案，继续遍历current.right
+ 如果current.left不为空
  + 如果mostRight为空，将当前节点加入答案，mostRight.right = current, current = current.left
  + 如果mostRight不为空，mostRight.right = null
+ 重复2、3步骤

```js
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
  const result = [];

  if (root === null) return result;

  let current = root;
  // 通过mostRight.right是否有值来判断是第几次访问每个子树的root节点
  let mostRight = null;

  while (current !== null) {
    mostRight = current.left;
    if (mostRight !== null) {
      while (mostRight.right !== null && mostRight.right !== current) {
        mostRight = mostRight.right;
      }
      if (mostRight.right === null) {
        result.push(current.val);
        mostRight.right = current;
        current = current.left;
        continue;
      } else {
        mostRight.right = null;
      }
    } else {
      result.push(current.val);
    }
    current = current.right;
  }

  return result;
};
```