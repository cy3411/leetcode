# 题目

集团里有 `n` 名员工，他们可以完成各种各样的工作创造利润。

第 `i` 种工作会产生 `profit[i]` 的利润，它要求 `group[i]` 名成员共同参与。如果成员参与了其中一项工作，就不能参与另一项工作。

工作的任何至少产生 `minProfit` 利润的子集称为 盈利计划 。并且工作的成员总数最多为 `n` 。

有多少种计划可以选择？因为答案很大，所以 **返回结果模 `10^9 + 7` 的值**。

# 示例

```
输入：n = 5, minProfit = 3, group = [2,2], profit = [2,3]
输出：2
解释：至少产生 3 的利润，该集团可以完成工作 0 和工作 1 ，或仅完成工作 1 。
总的来说，有两种计划。
```

# 题解

## 动态规划

**状态**
定义 `dp[i][j][k]`，表示前 `i` 个工作，人数不超过 `j`，所得到利润为 `k` 的方案数

**basecase**
`dp[0][j][0] = 1`，0 个工作只能不管有多少人，所得利润为 0 的方案是 1.

**选择**

- 不选择当前工作
  - `dp[i][j][k] = dp[i-1][j][k]`
- 选择当前工作，只能是人数大于或者等于工作所需要的人数才可以。
  - `dp[i][j][k] = dp[i-1][j][k] + dp[i-1][j-group[i-1]][max(0,k-profit[i-1])]`

$$
dp[i][j][k] = \begin{cases}
    dp[i-1][j][k] & \text{j<group[i-1]} \\
    dp[i-1][j][k] + dp[i-1][j-group[i-1]][max(0,k-profit[i-1])] & \text {j >= group[i-1]}
\end{cases}
$$

```ts
function profitableSchemes(
  n: number,
  minProfit: number,
  group: number[],
  profit: number[]
): number {
  const mod = 1e9 + 7;
  let m = group.length;
  // dp[i][j][k]，表示前i个工作，人数不超过j，所得到利润为k的方案数
  const dp = new Array(m + 1)
    .fill(0)
    .map(() => new Array(n + 1).fill(0).map(() => new Array(minProfit + 1).fill(0)));

  // basecase
  for (let j = 0; j <= n; j++) {
    dp[0][j][0] = 1;
  }

  for (let i = 1; i <= m; i++) {
    const g = group[i - 1];
    const p = profit[i - 1];
    for (let j = 0; j <= n; j++) {
      for (let k = 0; k <= minProfit; k++) {
        // 不选择
        dp[i][j][k] = dp[i - 1][j][k];
        // 人数满足条件选择
        if (j >= g) {
          // 正常是k-p，由于这里有负利润的情况，可以直接等价赋值给0
          let diff = Math.max(0, k - p);
          dp[i][j][k] = (dp[i][j][k] + dp[i - 1][j - g][diff]) % mod;
        }
      }
    }
  }

  return dp[m][n][minProfit];
}
```
