/*
 * @lc app=leetcode.cn id=879 lang=typescript
 *
 * [879] 盈利计划
 */

// @lc code=start
function profitableSchemes(
  n: number,
  minProfit: number,
  group: number[],
  profit: number[]
): number {
  const mod = 1e9 + 7;
  let m = group.length;
  // dp[i][j][k]，表示前i个工作，人数不超过j，所得到利润为k的方案数
  const dp = new Array(m + 1)
    .fill(0)
    .map(() => new Array(n + 1).fill(0).map(() => new Array(minProfit + 1).fill(0)));

  // basecase
  for (let j = 0; j <= n; j++) {
    dp[0][j][0] = 1;
  }

  for (let i = 1; i <= m; i++) {
    const g = group[i - 1];
    const p = profit[i - 1];
    for (let j = 0; j <= n; j++) {
      for (let k = 0; k <= minProfit; k++) {
        // 不选择
        dp[i][j][k] = dp[i - 1][j][k];
        // 人数满足条件选择
        if (j >= g) {
          // 正常是k-p，由于这里有负利润的情况，可以直接等价赋值给0
          let diff = Math.max(0, k - p);
          dp[i][j][k] = (dp[i][j][k] + dp[i - 1][j - g][diff]) % mod;
        }
      }
    }
  }

  return dp[m][n][minProfit];
}
// @lc code=end
