# 描述

给定一个循环数组（最后一个元素的下一个元素是数组的第一个元素），输出每个元素的下一个更大元素。数字 x 的下一个更大的元素是按数组遍历顺序，这个数字之后的第一个比它更大的数，这意味着你应该循环地搜索它的下一个更大的数。如果不存在，则输出 -1。

# 示例

```
输入: [1,2,1]
输出: [2,-1,2]
解释: 第一个 1 的下一个更大的数是 2；
数字 2 找不到下一个更大的数；
第二个 1 的下一个最大的数需要循环搜索，结果也是 2。
```

# 方法

## 单调栈

题目的意思就是找一个比当前元素大的最近的元素，由于是循环数组，所以需要最多遍历 2 次。
我们可以将原数组再连接一个原数组，这样就可以实现循环遍历。
最大的元素我们可以用单调栈来存储

```javascript
var nextGreaterElements = function (nums) {
  const size = nums.length;
  const result = new Array(size).fill(-1);
  const stack = [];

  for (let i = 0; i < 2 * size; i++) {
    // 这里使用了i%size来获取下标，2次遍历数组
    // 每次比较栈中最大的元素，当前元素更大的，更新结果数据并将栈顶弹出
    while (stack.length && nums[stack[stack.length - 1]] < nums[i % size]) {
      result[stack[stack.length - 1]] = nums[i % size];
      stack.pop();
    }
    stack.push(i % size);
  }

  return result;
};
```

```ts
function nextGreaterElements(nums: number[]): number[] {
  const stack: number[] = [];
  const ans: number[] = new Array(nums.length).fill(-1);
  // 循环数组
  const temp = [...nums, ...nums];
  for (let i = 0; i < temp.length; i++) {
    while (stack.length && temp[i] > temp[stack[stack.length - 1]]) {
      let idx = stack.pop();
      ans[idx] = temp[i];
    }
    stack.push(i);
  }

  // 只取一个数组的结果
  return ans.slice(0, nums.length);
}
```
