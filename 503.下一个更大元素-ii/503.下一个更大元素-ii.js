/*
 * @lc app=leetcode.cn id=503 lang=javascript
 *
 * [503] 下一个更大元素 II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var nextGreaterElements = function (nums) {
  const size = nums.length;
  const result = new Array(size).fill(-1);
  const stack = [];

  for (let i = 0; i < 2 * size; i++) {
    while (stack.length && nums[stack[stack.length - 1]] < nums[i % size]) {
      result[stack[stack.length - 1]] = nums[i % size];
      stack.pop();
    }
    stack.push(i % size);
  }

  return result;
};
// @lc code=end
