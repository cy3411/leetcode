/*
 * @lc app=leetcode.cn id=74 lang=typescript
 *
 * [74] 搜索二维矩阵
 */

// @lc code=start
function searchMatrix(matrix: number[][], target: number): boolean {
  const m = matrix.length;
  const n = matrix[0].length;
  let i = 0;
  let j = n - 1;

  while (i < m && j >= 0) {
    let mid = matrix[i][j];
    if (mid === target) return true;
    if (mid < target) i++;
    else j--;
  }

  return false;
}
// @lc code=end
