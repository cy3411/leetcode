/*
 * @lc app=leetcode.cn id=74 lang=javascript
 *
 * [74] 搜索二维矩阵
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function (matrix, target) {
  let m = matrix.length;
  let n = matrix[0].length;

  let left = 0;
  let right = m * n - 1;
  // 二分查找
  while (left <= right) {
    let mid = left + (((right - left) / 2) >> 0);
    // 转换成二维的i
    let i = (mid / n) >> 0;
    // 转换成二维的j
    let j = mid % n;
    if (matrix[i][j] < target) {
      left = mid + 1;
    } else if (matrix[i][j] > target) {
      right = mid - 1;
    } else {
      return true;
    }
  }
  return false;
};
// @lc code=end
