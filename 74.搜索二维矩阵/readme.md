# 题目

编写一个高效的算法来判断 m x n 矩阵中，是否存在一个目标值。该矩阵具有如下特性：

- 每行中的整数从左到右按升序排列。
- 每行的第一个整数大于前一行的最后一个整数。

提示：

- m == matrix.length
- n == matrix[i].length
- 1 <= m, n <= 100
- -104 <= matrix[i][j], target <= 104

# 示例

```
输入：matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
输出：true
```

```
输入：matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
输出：false
```

# 方法

二维数组转换成一维数组，就是一个升序排列的数组。

问题就转化成在一个升序数组中找 target，这里就用到了二分查找。

```js
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function (matrix, target) {
  let m = matrix.length;
  let n = matrix[0].length;

  let left = 0;
  let right = m * n - 1;
  // 二分查找
  while (left <= right) {
    let mid = left + (((right - left) / 2) >> 0);
    // 转换成二维的i
    let i = (mid / n) >> 0;
    // 转换成二维的j
    let j = mid % n;
    if (matrix[i][j] < target) {
      left = mid + 1;
    } else if (matrix[i][j] > target) {
      right = mid - 1;
    } else {
      return true;
    }
  }
  return false;
};
```

从右上或者左下开始，查找元素就具有二分性。

从右上开始，如果当前位置的值小于 target，就行数加 1。否者，列数减 1.

```ts
function searchMatrix(matrix: number[][], target: number): boolean {
  const m = matrix.length;
  const n = matrix[0].length;
  let i = 0;
  let j = n - 1;

  while (i < m && j >= 0) {
    let mid = matrix[i][j];
    if (mid === target) return true;
    if (mid < target) i++;
    else j--;
  }

  return false;
}
```
