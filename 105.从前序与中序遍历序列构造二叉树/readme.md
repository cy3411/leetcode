# 题目
根据一棵树的前序遍历与中序遍历构造二叉树。

注意:
你可以假设树中没有重复的元素。

# 示例
```
例如，给出：
前序遍历 preorder = [3,9,20,15,7]
中序遍历 inorder = [9,3,15,20,7]

返回如下的二叉树：
    3
   / \
  9  20
    /  \
   15   7
```
# 方法
根据前序遍历，我们可以获得根节点的位置。

更具中序遍历，我们可以或者根节点的左右子树。

我们可以先构建根节点，然后获得对应的左右子树位置，构建左右子树的根节点，重复上述动作知道节点全部构建出来。

```js
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */
var buildTree = function (preorder, inorder) {
  const mapInoder = new Map();
  // 缓存inorder 值->索引
  // 方便preorder中拿到根节点的值时，可以快速获取到左右节点的位置
  for (let i = 0; i < inorder.length; i++) {
    mapInoder.set(inorder[i], i);
  }
  // 构建根节点以及根节点的左右子树根
  const createRoot = (pLeft, pRight, iLeft, iRight) => {
    // 左索引超过右索引，到达边界。
    if (pLeft > pRight) return null;
    // 前序遍历第一个必然是根节点
    let rootVal = preorder[pLeft];
    // 构建根节点
    const root = new TreeNode(rootVal);
    // 从缓存中找到根节点的在中序中的位置，方便定位左右子树
    const inorderMidIndex = mapInoder.get(rootVal);
    // 左子树的节点数
    const leftNum = inorderMidIndex - iLeft;
    // 构建左右子树的根
    root.left = createRoot(pLeft + 1, pLeft + leftNum, iLeft, inorderMidIndex - 1);
    root.right = createRoot(pLeft + leftNum + 1, pRight, inorderMidIndex + 1, iRight);
    return root;
  };

  return createRoot(0, preorder.length - 1, 0, inorder.length);
};
```