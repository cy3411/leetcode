# 题目
给你一个二维矩阵 `matrix` 和一个整数 `k` ，矩阵大小为 `m x n` 由非负整数组成。

矩阵中坐标 `(a, b)` 的 值 可由对所有满足 `0 <= i <= a < m 且 0 <= j <= b < n` 的元素 `matrix[i][j]`（下标从 `0` 开始计数）执行**异或运算**得到。

请你找出 `matrix` 的所有坐标中第 `k` 大的值（`k` 的值从 `1` 开始计数）。

# 示例
```
输入：matrix = [[5,2],[1,6]], k = 1
输出：7
解释：坐标 (0,1) 的值是 5 XOR 2 = 7 ，为最大的值。
```

# 题解
## 前缀和
获得某个区间的值，自然想到了前缀和。

这道题只是从一维换成了二维。

计算出前缀和数组后，将结果排序或者使用大顶堆，就可以获得结果了。

```ts
function kthLargestValue(matrix: number[][], k: number): number {
  const m = matrix.length;
  const n = matrix[0].length;
  // 二维前缀和数组
  const preSum = new Array(m + 1).fill(0).map(() => new Array(n + 1).fill(0));

  const result: number[] = [];
  for (let i = 1; i <= m; i++) {
    for (let j = 1; j <= n; j++) {
      preSum[i][j] =
        preSum[i - 1][j] ^ preSum[i][j - 1] ^ preSum[i - 1][j - 1] ^ matrix[i - 1][j - 1];
      result.push(preSum[i][j]);
    }
  }
  // 排序
  result.sort((a, b) => a - b);

  return result[result.length - k];
}
```

```ts
type QuickSort = (a: number[], l: number, r: number) => void;
type Medina = (l: number, m: number, r: number) => number;

function kthLargestValue(matrix: number[][], k: number): number {
  const m = matrix.length;
  const n = matrix[0].length;
  // 二维前缀和数组
  const preSum = new Array(m + 1).fill(0).map(() => new Array(n + 1).fill(0));
  // 快速排序
  const quicksort: QuickSort = (arr, l, r) => {
    if (l >= r) return;
    let x = l;
    let y = r;
    let base = medina(arr[l], arr[(l + r) >> 1], arr[r]);
    while (x < y) {
      while (x < y && arr[y] >= base) {
        y--;
      }
      if (x < y) {
        arr[x++] = arr[y];
      }
      while (x < y && arr[x] <= base) {
        x++;
      }
      if (x < y) {
        arr[y--] = arr[x];
      }
    }
    arr[x] = base;
    quicksort(arr, l, x - 1);
    quicksort(arr, x + 1, r);
  };
  // 三点取中法
  const medina: Medina = (a, b, c) => {
    if (a > b) [a, b] = [b, a];
    if (a > c) [a, c] = [c, a];
    if (b > c) [b, c] = [c, b];
    return b;
  };

  const result: number[] = [];
  for (let i = 1; i <= m; i++) {
    for (let j = 1; j <= n; j++) {
      preSum[i][j] =
        preSum[i - 1][j] ^ preSum[i][j - 1] ^ preSum[i - 1][j - 1] ^ matrix[i - 1][j - 1];
      result.push(preSum[i][j]);
    }
  }
  // 排序
  quicksort(result, 0, result.length - 1);
  return result[result.length - k];
}
```