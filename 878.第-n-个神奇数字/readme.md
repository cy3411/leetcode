# 题目

如果正整数可以被 A 或 B 整除，那么它是神奇的。

返回第 N 个神奇数字。由于答案可能非常大，返回它模 $10^9 + 7$ 的结果。

提示：

- $1 \leq N \leq 10^9$
- $2 \leq A \leq 40000$
- $2 \leq B \leq 40000$

# 示例

```
输入：N = 1, A = 2, B = 3
输出：2
```

```
输入：N = 4, A = 2, B = 3
输出：6
```

# 题解

## 二分查找

题意就是在[1,n]中找到第 N 个神奇数字，那么我们可以用二分查找来找到答案。

区间的最大值是 $n * Math.min(a, b)$，因为这个区间最少可以保证有 n 个最小值的倍数。

利用二分查找，判断当前数字有多少个 a 和 b 的倍数，如果是神奇数字，那么就是答案。

```ts
function nthMagicalNumber(n: number, a: number, b: number): number {
  const mod = 10 ** 9 + 7;
  // 最小公约数
  const gcd = (a: number, b: number): number => {
    return b === 0 ? a : gcd(b, a % b);
  };
  // 最小公倍数
  const lcm = (a: number, b: number): number => {
    return (a * b) / gcd(a, b);
  };
  // 可以被a和b整除的数的个数
  const magicNumber = (n: number): number => {
    return Math.floor(n / a) + Math.floor(n / b) - Math.floor(n / lcm(a, b));
  };

  let l = 1;
  let r = n * Math.min(a, b);

  let mid: number;
  while (l < r) {
    mid = l + Math.floor((r - l) / 2);
    if (magicNumber(mid) < n) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }

  return l % mod;
}
```

```cpp
class Solution {
public:
    int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); };
    int lcm(int a, int b) { return (a * b) / gcd(a, b); }
    long long magicNumber(long long n, int a, int b) {
        return (n / a + n / b - n / (lcm(a, b)));
    }
    int nthMagicalNumber(int n, int a, int b) {
        int mod = 1e9 + 7;
        long long l = 1;
        long long r = (long long)n * min(a, b);
        long long mid;
        while (l < r) {
            mid = l + (r - l) / 2;
            if (magicNumber(mid, a, b) < n) {
                l = mid + 1;
            } else {
                r = mid;
            }
        }
        return l % mod;
    };
};
```

```py
class Solution:
    def gcd(self, a: int, b: int) -> int:
        return a if b == 0 else self.gcd(b, a % b)

    def lcm(self, a: int, b: int) -> int:
        return (a * b) / self.gcd(a, b)

    def maginNumber(self, n: int, a: int, b: int) -> int:
        return n // a + n // b - n // self.lcm(a, b)

    def nthMagicalNumber(self, n: int, a: int, b: int) -> int:
        mod = 10 ** 9 + 7
        l, r = min(a, b), n * min(a, b)
        while l < r:
            mid = l + (r - l) // 2
            if (self.maginNumber(mid, a, b) < n):
                l = mid + 1
            else:
                r = mid
        return l % mod
```
