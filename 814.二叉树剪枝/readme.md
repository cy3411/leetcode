# 题目

给你二叉树的根结点 `root` ，此外树的每个结点的值要么是 `0` ，要么是 `1` 。

返回移除了所有不包含 `1` 的子树的原二叉树。

节点 `node` 的子树为 `node` 本身加上所有 `node` 的后代。

提示：

- 树中节点的数目在范围 `[1, 200]` 内
- `Node.val` 为 `0` 或 `1`

# 示例

[![jLfElj.png](https://s1.ax1x.com/2022/07/21/jLfElj.png)](https://imgtu.com/i/jLfElj)

```
输入：root = [1,null,0,0,1]
输出：[1,null,0,null,1]
解释：
只有红色节点满足条件“所有不包含 1 的子树”。 右图为返回的答案。
```

[![jLfmmq.png](https://s1.ax1x.com/2022/07/21/jLfmmq.png)](https://imgtu.com/i/jLfmmq)

```
输入：root = [1,0,1,0,0,0,1]
输出：[1,null,1,null,1]
```

# 题解

## 递归

递归重构左右子树，当递归完成后，满足左子树为 null、右子树为 null、节点值为 0 的节点，则删除该节点。

```ts
function pruneTree(root: TreeNode | null): TreeNode | null {
  if (root === null) return null;
  root.left = pruneTree(root.left);
  root.right = pruneTree(root.right);
  if (root.left === null && root.right === null && root.val === 0) {
    return null;
  }
  return root;
}
```

```py
class Solution:
    def pruneTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        if not root:
            return None
        root.left = self.pruneTree(root.left)
        root.right = self.pruneTree(root.right)
        if (not root.left) and (not root.right) and (root.val == 0):
            return None
        return root
```

```cpp
class Solution {
public:
    TreeNode *pruneTree(TreeNode *root) {
        if (root == nullptr) return nullptr;
        root->left = pruneTree(root->left);
        root->right = pruneTree(root->right);
        if (root->left == nullptr && root->right == nullptr && root->val == 0)
            return nullptr;
        return root;
    }
};
```
