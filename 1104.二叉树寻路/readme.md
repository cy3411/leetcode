# 题目

在一棵无限的二叉树上，每个节点都有两个子节点，树中的节点 逐行 依次按 “之” 字形进行标记。

如下图所示，在奇数行（即，第一行、第三行、第五行……）中，按从左到右的顺序进行标记；

而偶数行（即，第二行、第四行、第六行……）中，按从右到左的顺序进行标记。

[![Wbis6s.md.png](https://z3.ax1x.com/2021/07/29/Wbis6s.md.png)](https://imgtu.com/i/Wbis6s)

给你树上某一个节点的标号 label，请你返回从根节点到该标号为 label 节点的路径，该路径是由途经的节点标号所组成的。

提示：

- 1 <= label <= 10^6

# 示例

```
输入：label = 14
输出：[1,3,4,14]
```

# 题解

## 二叉树

定义 i 为二叉树的层数, v 为当前节点的 label:

- 每层最小 label 是 $2^{i-1}$
- 每层最大 label 是 $2^i-1$
- 父节点的 label 是 $v\div2$

先计算出 label 所在的层数，然后逐步去找父节点，直到层数为 0。

需要注意的就是偶数层的时候，需要通过 label 计算出反转前的值。

再寻址的过程中将 label 放入答案数组中。

```ts
function pathInZigZagTree(label: number): number[] {
  // 获取反转前的label
  const getReverse = (label: number, row: number): number => {
    // 每层的最小值
    const min = 1 << (row - 1);
    // 每层的最大值
    const max = (1 << row) - 1;
    return min + max - label;
  };

  let row = 1;
  let maxCount = 1;
  // 计算label所在的层数
  // 定义层数为i，每层的左右区间是2**(i-1)和2**i-1之间。
  while (maxCount * 2 <= label) {
    row++;
    maxCount *= 2;
  }
  // 如果label是反转后的值，将其反转成正确的值
  if (row % 2 === 0) label = getReverse(label, row);
  let ans: number[] = [];
  while (row > 0) {
    if (row % 2 === 0) {
      ans.push(getReverse(label, row));
    } else {
      ans.push(label);
    }
    row--;
    label >>= 1;
  }

  return ans.reverse();
}
```
