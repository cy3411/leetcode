/*
 * @lc app=leetcode.cn id=728 lang=typescript
 *
 * [728] 自除数
 */

// @lc code=start
function selfDividingNumbers(left: number, right: number): number[] {
  // 判断是否是自除数
  const isSelfDividingNumber = (num: number): boolean => {
    let n = num;
    while (n > 0) {
      // 取出最后一位
      const digit = n % 10;
      // 如果是0或者不能整除
      if (digit === 0 || num % digit !== 0) return false;
      // 去掉最后一位
      n = (n / 10) >> 0;
    }
    return true;
  };

  const ans: number[] = [];
  for (let i = left; i <= right; i++) {
    if (isSelfDividingNumber(i)) ans.push(i);
  }

  return ans;
}
// @lc code=end
