/*
 * @lc app=leetcode.cn id=728 lang=cpp
 *
 * [728] 自除数
 */

// @lc code=start
class Solution
{
public:
    int isSelfDividingNumber(int num)
    {
        int n = num;
        while (n)
        {
            int d = n % 10;
            if (d == 0 || num % d != 0)
            {
                return 0;
            }
            n /= 10;
        }
        return 1;
    }

    vector<int> selfDividingNumbers(int left, int right)
    {
        vector<int> ans;
        for (int i = left; i <= right; i++)
        {
            if (isSelfDividingNumber(i))
            {
                ans.push_back(i);
            }
        }
        return ans;
    }
};
// @lc code=end
