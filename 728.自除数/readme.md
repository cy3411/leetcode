# 题目

**自除数** 是指可以被它包含的每一位数整除的数。

- 例如，`128` 是一个 自除数 ，因为 `128 % 1 == 0`，`128 % 2 == 0`，`128 % 8 == 0`。

**自除数** 不允许包含 `0` 。

给定两个整数 `left` 和 `right` ，返回一个列表，列表的元素是范围 `[left, right]` 内所有的 **自除数** 。

提示：

$1 \leq left \leq right \leq 10^4$

# 示例

```
输入：left = 1, right = 22
输出：[1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
```

```
输入：left = 47, right = 85
输出：[48,55,66,77]
```

# 题解

## 模拟

枚举 `[left,right]` 中的所有数，然后判断是否是自除数。

判断是否为自除数，按照题目的要求，对数字的每一位做如下判断：

- 是否为 0，如果是，则不是自除数
- 是否 num 取模为 0，如果不是，则不是自除数

```ts
function selfDividingNumbers(left: number, right: number): number[] {
  // 判断是否是自除数
  const isSelfDividingNumber = (num: number): boolean => {
    let n = num;
    while (n > 0) {
      // 取出最后一位
      const digit = n % 10;
      // 如果是0或者不能整除
      if (digit === 0 || num % digit !== 0) return false;
      // 去掉最后一位
      n = (n / 10) >> 0;
    }
    return true;
  };

  const ans: number[] = [];
  for (let i = left; i <= right; i++) {
    if (isSelfDividingNumber(i)) ans.push(i);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int isSelfDividingNumber(int num)
    {
        int n = num;
        while (n)
        {
            int d = n % 10;
            if (d == 0 || num % d != 0)
            {
                return 0;
            }
            n /= 10;
        }
        return 1;
    }

    vector<int> selfDividingNumbers(int left, int right)
    {
        vector<int> ans;
        for (int i = left; i <= right; i++)
        {
            if (isSelfDividingNumber(i))
            {
                ans.push_back(i);
            }
        }
        return ans;
    }
};
```
