/*
 * @lc app=leetcode.cn id=1106 lang=typescript
 *
 * [1106] 解析布尔表达式
 */

// @lc code=start
function parseBoolExpr(expression: string): boolean {
  const n = expression.length;
  const stk: string[] = [];

  for (let i = 0; i < n; i++) {
    const ch = expression[i];
    if (ch === ',') {
      continue;
    } else if (ch !== ')') {
      stk.push(ch);
    } else {
      // t和f的数量
      let t = 0;
      let f = 0;
      while (stk[stk.length - 1] !== '(') {
        const val = stk.pop();
        if (val === 't') t++;
        else f++;
      }
      // 将做括号弹出
      stk.pop();
      // 拿到到操作符开始计算，将结果压入栈中
      const op = stk.pop();
      switch (op) {
        case '!':
          stk.push(t === 1 ? 'f' : 't');
          break;
        case '|':
          stk.push(t >= 1 ? 't' : 'f');
          break;
        case '&':
          stk.push(f >= 1 ? 'f' : 't');
      }
    }
  }

  return stk.pop() === 't';
}
// @lc code=end
