# 题目

给你一个以字符串形式表述的 布尔表达式（boolean） expression，返回该式的运算结果。

有效的表达式需遵循以下约定：

- "t"，运算结果为 True
- "f"，运算结果为 False
- "!(expr)"，运算过程为对内部表达式 expr 进行逻辑 非的运算（NOT）
- "&(expr1,expr2,...)"，运算过程为对 2 个或以上内部表达式 expr1, expr2, ... 进行逻辑 与的运算（AND）
- "|(expr1,expr2,...)"，运算过程为对 2 个或以上内部表达式 expr1, expr2, ... 进行逻辑 或的运算（OR）

提示：

- $1 \leq expression.length \leq 20000$
- expression[i] 由 {'(', ')', '&', '|', '!', 't', 'f', ','} 中的字符组成。
- expression 是以上述形式给出的有效表达式，表示一个布尔值。

# 示例

```
输入：expression = "!(f)"
输出：true
```

```
输入：expression = "|(f,t)"
输出：true
```

# 题解

## 栈

由题目可以看出，合法的表达式有操作符、小括号和括号内的 t,f 构成，这里我们可以通过 t,f 的数量的来判断表达式的值：

- 操作符是 ^ ，如果 t 的数量等于 1，结果就是 f，否则为 t
- 操作符是 | ，如果 t 的数量大于等于 1，结果为 t， 否则为 f
- 操作符是 &， 如果 f 的数量大于等于 1，结果为 f，否则为 t

为了解决计算优先级的问题，我们可以引入栈来处理。

遍历字符串，将所有等于','和')' 的字符压入栈中。如果字符串为')'，将栈中的字符弹出，统计 t 和 f 的数量，然后利用上面的规则求值，再将结果压入栈中。最后栈中留下的就是答案。

```ts
function parseBoolExpr(expression: string): boolean {
  const n = expression.length;
  const stk: string[] = [];

  for (let i = 0; i < n; i++) {
    const ch = expression[i];
    if (ch === ',') {
      continue;
    } else if (ch !== ')') {
      stk.push(ch);
    } else {
      // t和f的数量
      let t = 0;
      let f = 0;
      while (stk[stk.length - 1] !== '(') {
        const val = stk.pop();
        if (val === 't') t++;
        else f++;
      }
      // 将左括号弹出
      stk.pop();
      // 拿到到操作符开始计算，将结果压入栈中
      const op = stk.pop();
      switch (op) {
        case '!':
          stk.push(t === 1 ? 'f' : 't');
          break;
        case '|':
          stk.push(t >= 1 ? 't' : 'f');
          break;
        case '&':
          stk.push(f >= 1 ? 'f' : 't');
      }
    }
  }

  return stk.pop() === 't';
}
```
