# 题目
给你一个二维整数数组 `orders` ，其中每个 `orders[i] = [pricei, amounti, orderTypei]` 表示有 `amounti` 笔类型为 `orderTypei` 、价格为 `pricei` 的订单。

订单类型 `orderTypei` 可以分为两种：

+ 0 表示这是一批采购订单 `buy`
+ 1 表示这是一批销售订单 `sell`

注意，`orders[i]` 表示一批共计 `amounti` 笔的独立订单，这些订单的价格和类型相同。对于所有有效的 `i` ，由 `orders[i]` 表示的所有订单提交时间均早于 `orders[i+1]` 表示的所有订单。

存在由未执行订单组成的 **积压订单** 。积压订单最初是空的。提交订单时，会发生以下情况：

+ 如果该订单是一笔采购订单 `buy` ，则可以查看积压订单中价格 **最低** 的销售订单 `sell` 。如果该销售订单 `sell` 的价格 低于或等于 当前采购订单 buy 的价格，则匹配并执行这两笔订单，并将销售订单 `sell` 从积压订单中删除。否则，采购订单 `buy` 将会添加到积压订单中。
+ 反之亦然，如果该订单是一笔销售订单 `sell` ，则可以查看积压订单中价格 **最高** 的采购订单 buy 。如果该采购订单 `buy` 的价格 高于或等于 当前销售订单 `sell` 的价格，则匹配并执行这两笔订单，并将采购订单 buy 从积压订单中删除。否则，销售订单 `sell` 将会添加到积压订单中。
  
输入所有订单后，返回积压订单中的 订单总数 。由于数字可能很大，所以需要返回对 `10**9 + 7` 取余的结果。

提示：

+ 1 <= orders.length <= 105
+ orders[i].length == 3
+ 1 <= pricei, amounti <= 109
+ orderTypei 为 0 或 1

# 示例
![crS3B8.png](https://z3.ax1x.com/2021/04/12/crS3B8.png)
```
输入：orders = [[10,5,0],[15,2,1],[25,1,1],[30,4,0]]
输出：6
解释：输入订单后会发生下述情况：
- 提交 5 笔采购订单，价格为 10 。没有销售订单，所以这 5 笔订单添加到积压订单中。
- 提交 2 笔销售订单，价格为 15 。没有采购订单的价格大于或等于 15 ，所以这 2 笔订单添加到积压订单中。
- 提交 1 笔销售订单，价格为 25 。没有采购订单的价格大于或等于 25 ，所以这 1 笔订单添加到积压订单中。
- 提交 4 笔采购订单，价格为 30 。前 2 笔采购订单与价格最低（价格为 15）的 2 笔销售订单匹配，从积压订单中删除这 2 笔销售订单。第 3 笔采购订单与价格最低的 1 笔销售订单匹配，销售订单价格为 25 ，从积压订单中删除这 1 笔销售订单。积压订单中不存在更多销售订单，所以第 4 笔采购订单需要添加到积压订单中。
最终，积压订单中有 5 笔价格为 10 的采购订单，和 1 笔价格为 30 的采购订单。所以积压订单中的订单总数为 6 。
```


# 题解
## 堆
根据提议，我们需要找到价格最低的销售订单和价格最高的采购订单。

维护一个`buy`的大顶堆和一个`sell`的小顶堆，迭代订单列表：

+ 采购订单的价格如果大于 `sell` 的堆顶，我们就可以处理这笔采购订单。
+ 销售订单的价格如果小于 `buy` 的堆顶，我们就可以处理这边销售订单。

处理完列表后，堆中剩余的就是积压的订单，统计它们的数量就是结果。

```js
class PQ {
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;

    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  less(i, j) {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }

  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }

  size() {
    return this.keys.length;
  }

  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }

  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }

  peek() {
    return this.keys[0];
  }
}
// 大顶堆
class MaxPQ extends PQ {}
// 小顶堆
class MinPQ extends PQ {
  constructor(keys, mapKeysValue = (x) => x) {
    super(keys, (x) => -1 * mapKeysValue(x));
  }
}

/**
 * @param {number[][]} orders
 * @return {number}
 */
var getNumberOfBacklogOrders = function (orders) {
  const buy = new MaxPQ([], (x) => x[0]);
  const sell = new MinPQ([], (x) => x[0]);

  for (let order of orders) {
    if (order[2] === 0) {
      // 采购订单
      while (order[1] !== 0 && sell.size() && sell.peek()[0] <= order[0]) {
        let count = Math.min(sell.peek()[1], order[1]);
        order[1] -= count;
        sell.peek()[1] -= count;
        if (sell.peek()[1] === 0) sell.poll();
      }
      // 处理后的采购订单，如果还有订单数量，就放入堆中等待下次处理
      if (order[1] !== 0) buy.insert(order);
    } else if (order[2] === 1) {
      // 销售订单
      while (order[1] !== 0 && buy.size() && order[0] <= buy.peek()[0]) {
        let count = Math.min(buy.peek()[1], order[1]);
        order[1] -= count;
        buy.peek()[1] -= count;
        if (buy.peek()[1] === 0) buy.poll();
      }
      // 处理后的销售订单，如果还有订单数量，就放入堆中等待下次处理
      if (order[1] !== 0) sell.insert(order);
    }
  }

  let result = 0;
  const mod = 10 ** 9 + 7;

  for (let order of buy.keys) {
    result = (result + order[1]) % mod;
  }

  for (let order of sell.keys) {
    result = (result + order[1]) % mod;
  }

  return result;
};
```