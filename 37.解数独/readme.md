# 题目
编写一个程序，通过填充空格来解决数独问题。

数独的解法需 遵循如下规则：

+ 数字 1-9 在每一行只能出现一次。
+ 数字 1-9 在每一列只能出现一次。
+ 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）
  
数独部分空格内已填入了数字，空白格用 '.' 表示。

提示：

+ `board.length == 9`
+ `board[i].length == 9`
+ `board[i][j]` 是一位数字或者 '.'
+ 题目数据 **保证** 输入数独仅有一个解

# 示例
![c2wYAe.png](https://z3.ax1x.com/2021/04/15/c2wYAe.png)
```
输入：board = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]

输出：[["5","3","4","6","7","8","9","1","2"],["6","7","2","1","9","5","3","4","8"],["1","9","8","3","4","2","5","6","7"],["8","5","9","7","6","1","4","2","3"],["4","2","6","8","5","3","7","9","1"],["7","1","3","9","2","4","8","5","6"],["9","6","1","5","3","7","2","8","4"],["2","8","7","4","1","9","6","3","5"],["3","4","5","2","8","6","1","7","9"]]
```
解释：输入的数独如上图所示，唯一有效的解决方案如下所示：
![c2wcNQ.png](https://z3.ax1x.com/2021/04/15/c2wcNQ.png)

# 题解
## 回溯
穷举问题，每个可以填入数字的格子，按序1-9，检测后填入，然后下一个格子，不能填入的就回溯到上一个格子换数字继续。

```js
/**
 Do not return anything, modify board in-place instead.
 */

function solveSudoku(board: string[][]): void {
  // 存储每行已使用的数字
  const X: number[] = new Array(9).fill(0);
  // 存储每列已使用的数字
  const Y: number[] = new Array(9).fill(0);
  // 存储每个单元已使用的数字
  const Z: number[] = new Array(9).fill(0);
  /**
   * @description: 存储行、列、单元内的已使用的数字
   * @example
   * X[0]代表第一行，比如第一行如：["5","3",".",".","7",".",".",".","."]
   * X[0] = 10101000
   * 其中的1代表已经存在，具体代表的数字从低位到高位，从0开始计算。
   */
  const setCheck = (num: number, x: number, y: number): void => {
    X[x] ^= 1 << num;
    Y[y] ^= 1 << num;
    Z[((x / 3) >> 0) * 3 + ((y / 3) >> 0)] ^= 1 << num;
  };
  /**
   * @description: 检测数字是否已经在行、列或单元内
   */
  const check = (num: number, x: number, y: number): boolean => {
    if (X[x] & (1 << num)) return false;
    if (Y[y] & (1 << num)) return false;
    if (Z[((x / 3) >> 0) * 3 + ((y / 3) >> 0)] & (1 << num)) return false;
    return true;
  };
  /**
   * @description 初始化棋盘内已用的数据到存储中
   */
  const initBoard = (board: string[][]): void => {
    for (let i = 0; i < 9; i++) {
      for (let j = 0; j < 9; j++) {
        if (board[i][j] === '.') continue;
        setCheck(Number(board[i][j]), i, j);
      }
    }
  };

  /**
   * @description: 回溯
   */
  const backtrack = (board: string[][], index: number): boolean => {
    if (index === 81) return true;
    let x = (index / 9) >> 0;
    let y = index % 9;
    if (board[x][y] !== '.') return backtrack(board, index + 1);

    for (let i = 1; i <= 9; i++) {
      if (!check(i, x, y)) continue;
      setCheck(i, x, y);
      board[x][y] = String(i);
      if (backtrack(board, index + 1)) return true;
      setCheck(i, x, y);
      board[x][y] = '.';
    }
    return false;
  };

  initBoard(board);
  backtrack(board, 0);
}
```
