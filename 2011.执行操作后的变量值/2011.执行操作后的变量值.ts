/*
 * @lc app=leetcode.cn id=2011 lang=typescript
 *
 * [2011] 执行操作后的变量值
 */

// @lc code=start
function finalValueAfterOperations(operations: string[]): number {
  let ans = 0;
  for (const op of operations) {
    if (op === '++X' || op === 'X++') ans++;
    else ans--;
  }
  return ans;
}
// @lc code=end
