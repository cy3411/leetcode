# 题目

给出非负整数数组 `A` ，返回两个非重叠（连续）子数组中元素的最大和，子数组的长度分别为 `L` 和 `M`。（这里需要澄清的是，长为 `L` 的子数组可以出现在长为 `M` 的子数组之前或之后。）

从形式上看，返回最大的 `V`，而 `V = (A[i] + A[i+1] + ... + A[i+L-1]) + (A[j] + A[j+1] + ... + A[j+M-1])` 并满足下列条件之一：

- $\color{burlywood}0 \leq i < i + L - 1 < j < j + M - 1 < A.length$, 或
- $\color{burlywood}0 \leq j < j + M - 1 < i < i + L - 1 < A.length$

提示：

- $\color{burlywood}L \geq 1$
- $\color{burlywood}M \geq 1$
- $\color{burlywood}L + M \leq A.length \leq 1000$
- $\color{burlywood}0 \leq A[i] \leq 1000$

# 示例

```
输入：A = [0,6,5,2,2,5,1,9,4], L = 1, M = 2
输出：20
解释：子数组的一种选择中，[9] 长度为 1，[6,5] 长度为 2。
```

```
输入：A = [3,8,1,3,2,1,8,9,0], L = 3, M = 2
输出：29
解释：子数组的一种选择中，[3,8,1] 长度为 3，[8,9] 长度为 2。
```

```
输入：A = [2,1,5,6,0,9,5,0,3,8], L = 4, M = 3
输出：31
解释：子数组的一种选择中，[5,6,0,9] 长度为 4，[0,3,8] 长度为 3。
```

# 题解

## 遍历

逆向遍历，统计满足长度 `firstLen` 和 `secondLen` 的子数组的和，并记录。

正向遍历，统计满足长度 `firstLen` 和 `secondLen` 的子数组的和，和上面记录的逆序的最大值求和，取最大值就是答案。

```ts
function maxSumTwoNoOverlap(nums: number[], firstLen: number, secondLen: number): number {
  const n = nums.length;
  // 当前位置后满足firstLen的最大和
  const fmax = new Array(n + 1).fill(0);
  // 当前位置后满足secondLen的最大和
  const smax = new Array(n + 1).fill(0);

  // 统计当前位置往后满足长度的最大和
  for (let i = n - 1, fsum = 0, ssum = 0; i >= 0; i--) {
    fsum += nums[i];
    ssum += nums[i];
    // 区间长度小于遍历范围，需要将尾部的值去除
    if (i + firstLen < n) fsum -= nums[i + firstLen];
    if (i + secondLen < n) ssum -= nums[i + secondLen];
    if (i + firstLen <= n) fmax[i] = Math.max(fmax[i + 1], fsum);
    if (i + secondLen <= n) smax[i] = Math.max(smax[i + 1], ssum);
  }

  let ans = 0;
  // 统计当前位置前的最大和，并同时更新ans
  for (let i = 0, fsum = 0, ssum = 0; i < n; i++) {
    fsum += nums[i];
    ssum += nums[i];
    if (i >= firstLen) fsum -= nums[i - firstLen];
    if (i >= secondLen) ssum -= nums[i - secondLen];
    ans = Math.max(ans, fsum + smax[i + 1], ssum + fmax[i + 1]);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int maxSumTwoNoOverlap(vector<int> &nums, int firstLen, int secondLen)
    {
        int n = nums.size();
        vector<int> fmax(n + 1, 0);
        vector<int> smax(n + 1, 0);
        for (int i = n - 1, fsum = 0, ssum = 0; i >= 0; i--)
        {
            fsum += nums[i];
            ssum += nums[i];
            if (i + firstLen < n)
                fsum -= nums[i + firstLen];
            if (i + secondLen < n)
                ssum -= nums[i + secondLen];
            if (i + firstLen <= n)
                fmax[i] = max(fmax[i + 1], fsum);
            if (i + secondLen <= n)
                smax[i] = max(smax[i + 1], ssum);
        }

        int ans = 0;
        for (int i = 0, fsum = 0, ssum = 0; i < n; i++)
        {
            fsum += nums[i];
            ssum += nums[i];
            if (i >= firstLen)
                fsum -= nums[i - firstLen];
            if (i >= secondLen)
                ssum -= nums[i - secondLen];

            ans = max(ans, fsum + smax[i + 1]);
            ans = max(ans, ssum + fmax[i + 1]);
        }

        return ans;
    }
};
```
