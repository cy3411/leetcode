/*
 * @lc app=leetcode.cn id=1031 lang=typescript
 *
 * [1031] 两个非重叠子数组的最大和
 */

// @lc code=start
function maxSumTwoNoOverlap(nums: number[], firstLen: number, secondLen: number): number {
  const n = nums.length;
  // 当前位置后满足firstLen的最大和
  const fmax = new Array(n + 1).fill(0);
  // 当前位置后满足secondLen的最大和
  const smax = new Array(n + 1).fill(0);

  // 统计当前位置往后满足长度的最大和
  for (let i = n - 1, fsum = 0, ssum = 0; i >= 0; i--) {
    fsum += nums[i];
    ssum += nums[i];
    // 区间长度小于遍历范围，需要将尾部的值去除
    if (i + firstLen < n) fsum -= nums[i + firstLen];
    if (i + secondLen < n) ssum -= nums[i + secondLen];
    if (i + firstLen <= n) fmax[i] = Math.max(fmax[i + 1], fsum);
    if (i + secondLen <= n) smax[i] = Math.max(smax[i + 1], ssum);
  }

  let ans = 0;
  // 统计当前位置前的最大和，并同时更新ans
  for (let i = 0, fsum = 0, ssum = 0; i < n; i++) {
    fsum += nums[i];
    ssum += nums[i];
    if (i >= firstLen) fsum -= nums[i - firstLen];
    if (i >= secondLen) ssum -= nums[i - secondLen];
    ans = Math.max(ans, fsum + smax[i + 1], ssum + fmax[i + 1]);
  }

  return ans;
}
// @lc code=end
