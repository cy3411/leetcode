// @algorithm @lc id=1000230 lang=typescript
// @title w3tCBm
function countBits(n: number): number[] {
  const ans: number[] = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    if (i & 1) {
      ans[i] = ans[i - 1] + 1;
    } else {
      ans[i] = ans[i / 2];
    }
  }
  return ans;
}
