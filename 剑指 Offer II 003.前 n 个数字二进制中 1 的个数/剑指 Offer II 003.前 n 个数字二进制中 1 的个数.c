int *countBits(int n, int *returnSize)
{
    int *ans = malloc(sizeof(int) * (n + 1));
    *returnSize = n + 1;
    ans[0] = 0;
    for (int i = 1; i <= n; i++)
    {
        if (i & 1)
        {
            ans[i] = ans[i - 1] + 1;
        }
        else
        {
            ans[i] = ans[i / 2];
        }
    }
    return ans;
}