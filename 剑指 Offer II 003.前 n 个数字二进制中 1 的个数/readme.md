# 题目

给定一个非负整数 `n` ，请计算 `0` 到 `n` 之间的每个数字的二进制表示中 1 的个数，并输出一个数组

说明 :

- $0 \leq n \leq 10^5$

进阶:

- 给出时间复杂度为 O(n\*sizeof(integer)) 的解答非常容易。但你可以在线性时间 O(n) 内用一趟扫描做到吗？
- 要求算法的空间复杂度为 O(n) 。
- 你能进一步完善解法吗？要求在 C++或任何其他语言中不使用任何内置函数（如 C++ 中的 \_\_builtin_popcount ）来执行此操作。

注意：本题与主站 338 题相同：https://leetcode-cn.com/problems/counting-bits/

# 示例

```
输入: n = 2
输出: [0,1,1]
解释:
0 --> 0
1 --> 1
2 --> 10
```

```
输入: n = 5
输出: [0,1,1,2,1,2]
解释:
0 --> 0
1 --> 1
2 --> 10
3 --> 11
4 --> 100
5 --> 101
```

# 题解

## 递推

观察示例可以看出，当 n > 1 的时候：

- n 为奇数， 二进制中 1 的个数是 n - 1 的二进制中 1 的个数加 1
- n 为偶数，二进制中 1 的个数是 n / 2 的二进制中 1 的个数

$$
 f(n) = \begin{cases}
    f(n - 1) + 1 & \text{当 n 为奇数} \\
    F(n / 2) & \text{当 n 为偶数}
 \end{cases}
$$

```ts
function countBits(n: number): number[] {
  const ans: number[] = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    if (i & 1) {
      ans[i] = ans[i - 1] + 1;
    } else {
      ans[i] = ans[i / 2];
    }
  }
  return ans;
}
```

```cpp
class Solution {
public:
    vector<int> countBits(int n) {
      vector<int> ans(n + 1, 0);
      for (int i = 1; i <= n; i++) {
        if (i & 1) {
          ans[i] = ans[i - 1] + 1;
        } else {
          ans[i] = ans[i / 2];
        }
      }
      return ans;
    }
};
```

```cpp
int* countBits(int n, int* returnSize){
  int* ans = malloc(sizeof(int) * (n + 1));
  *returnSize = n + 1;
  ans[0] = 0;
  for (int i = 1; i <= n; i++) {
    if (i & 1) {
      ans[i] = ans[i - 1] + 1;
    }else {
      ans[i] = ans[i / 2];
    }
  }
  return ans;
}
```
