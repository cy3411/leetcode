/*
 * @lc app=leetcode.cn id=594 lang=typescript
 *
 * [594] 最长和谐子序列
 */

// @lc code=start
function findLHS(nums: number[]): number {
  const hash = new Map<number, number>();
  // 统计每个数的出现次数
  for (const num of nums) {
    hash.set(num, (hash.get(num) ?? 0) + 1);
  }
  let ans = 0;
  // 找出每个数的最长和谐子序列
  for (const [key, value] of hash.entries()) {
    if (hash.has(key + 1)) {
      ans = Math.max(ans, value + hash.get(key + 1));
    }
  }

  return ans;
}
// @lc code=end
