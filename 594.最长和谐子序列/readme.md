# 题目

和谐数组是指一个数组里元素的最大值和最小值之间的差别 正好是 1 。

现在，给你一个整数数组 `nums` ，请你在所有可能的子序列中找到最长的和谐子序列的长度。

数组的子序列是一个由数组派生出来的序列，它可以通过删除一些元素或不删除元素、且不改变其余元素的顺序而得到。

提示：

- $1 \leq nums.length \leq 2 * 10^4$
- $-10^9 \leq nums[i] \leq 10^9$

# 示例

```
输入：nums = [1,3,2,2,5,2,3,7]
输出：5
解释：最长的和谐子序列是 [3,2,2,2,3]
```

```
输入：nums = [1,2,3,4]
输出：2
```

# 题解

## 哈希表

枚举 `nums`，使用哈希表记录每个数字的出现次数。

遍历哈希表，统计 `x` 和 `x+1` 数字出现的次数，记录最大值。

```ts
function findLHS(nums: number[]): number {
  const hash = new Map<number, number>();
  // 统计每个数的出现次数
  for (const num of nums) {
    hash.set(num, (hash.get(num) ?? 0) + 1);
  }
  let ans = 0;
  // 找出每个数的最长和谐子序列
  for (const [key, value] of hash.entries()) {
    if (hash.has(key + 1)) {
      ans = Math.max(ans, value + hash.get(key + 1));
    }
  }

  return ans;
}
```
