/*
 * @lc app=leetcode.cn id=856 lang=typescript
 *
 * [856] 括号的分数
 */

// @lc code=start
function scoreOfParentheses(s: string): number {
  const n = s.length;
  if (n === 2) return 1;
  // 平衡括号字符串的长度
  let size = 0;
  // 左括号为1， 右括号为-1
  let score = 0;
  for (let i = 0; i < n; i++) {
    score += s[i] === '(' ? 1 : -1;
    if (score === 0) {
      size = i + 1;
      break;
    }
  }

  if (size === n) {
    // (A)结构
    return 2 * scoreOfParentheses(s.substring(1, n - 1));
  } else {
    // AB结构
    return scoreOfParentheses(s.substring(0, size)) + scoreOfParentheses(s.substring(size));
  }
}
// @lc code=end
