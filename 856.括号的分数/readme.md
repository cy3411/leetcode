# 题目

给定一个平衡括号字符串 S，按下述规则计算该字符串的分数：

- () 得 1 分。
- AB 得 A + B 分，其中 A 和 B 是平衡括号字符串。
- (A) 得 $2 * A$ 分，其中 A 是平衡括号字符串。

提示：

- `S` 是平衡括号字符串，且只含有 `(` 和 `)` 。
- $2 \leq S.length \leq 50$

# 示例

```
输入： "()"
输出： 1
```

```
输入： "(())"
输出： 2
```

# 题解

## 递归

我们将左括号记为 1 ， 右括号记为 -1 ， 当总分 score 为 0 的时候，那么这个就是一个平衡字符串。

- 如果该字符串长度等于 s 的长度，那么 s 可以分解为 (A) 的形式
- 否则，可以分解为 A+B 形式

当 s 长度为 2 的时候，递归退出，返回分数 1 。

```js
function scoreOfParentheses(s: string): number {
  const n = s.length;
  if (n === 2) return 1;
  // 平衡括号字符串的长度
  let size = 0;
  // 左括号为1， 右括号为-1
  let score = 0;
  for (let i = 0; i < n; i++) {
    score += s[i] === '(' ? 1 : -1;
    if (score === 0) {
      size = i + 1;
      break;
    }
  }

  if (size === n) {
    // (A)结构
    return 2 * scoreOfParentheses(s.substring(1, n - 1));
  } else {
    // AB结构
    return scoreOfParentheses(s.substring(0, size)) + scoreOfParentheses(s.substring(size));
  }
}
```

## 栈

使用栈来记录平衡括号的分数，默认栈顶为 0，表示总分。然后遍历字符串：

- 遇到左括号，将 0 压入栈中，表示当前还未计算
- 遇到右括号，需要计算分数，将栈顶弹出并记录到 value 中。如果 value 为 0 ，表示是空串，分数为 1 ，否则为 $2 * value$，然后将结果累加到栈顶。

```cpp
class Solution {
public:
    int scoreOfParentheses(string s) {
        stack<int> stk;
        stk.push(0);

        int n = s.size();
        for (int i = 0; i < n; i++) {
            if (s[i] == '(') {
                stk.push(0);
            } else {
                int value = stk.top();
                stk.pop();
                stk.top() += max(2 * value, 1);
            }
        }

        return stk.top();
    }
};
```
