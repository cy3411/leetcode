/*
 * @lc app=leetcode.cn id=856 lang=cpp
 *
 * [856] 括号的分数
 */

// @lc code=start
class Solution {
public:
    int scoreOfParentheses(string s) {
        stack<int> stk;
        stk.push(0);

        int n = s.size();
        for (int i = 0; i < n; i++) {
            if (s[i] == '(') {
                stk.push(0);
            } else {
                int value = stk.top();
                stk.pop();
                stk.top() += max(2 * value, 1);
            }
        }

        return stk.top();
    }
};
// @lc code=end
