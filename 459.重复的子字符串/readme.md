# 题目

给定一个非空的字符串，判断它是否可以由它的一个子串重复多次构成。给定的字符串只含有小写英文字母，并且长度不超过 10000。

# 示例

```
输入: "abab"

输出: True

解释: 可由子字符串 "ab" 重复两次构成。
```

```
输入: "abcabcabcabc"

输出: True

解释: 可由子字符串 "abc" 重复四次构成。 (或者子字符串 "abcabc" 重复两次构成。)
```

# 题解

## 相同前后缀

重复子串构成的字符串，那么它肯定有相同的前后缀。

我们可以计算前后缀位置，判断以最后一个字符为结尾的子串是否有公共前缀，并且子串能否被平均分割来返回答案。

```ts
function repeatedSubstringPattern(s: string): boolean {
  const n = s.length;
  const next = new Array(n).fill(-1);
  let j = -1;
  // 计算next数组，最长公共前后缀
  for (let i = 1; i < n; i++) {
    while (j !== -1 && s[i] !== s[j + 1]) {
      j = next[j];
    }
    if (s[i] === s[j + 1]) {
      j++;
    }
    next[i] = j;
  }

  // 最后一位有公共前缀且公共前缀可以被字符串平均分割
  return next[n - 1] !== -1 && n % (n - next[n - 1] - 1) === 0;
}
```
