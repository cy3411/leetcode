# 题目

给定一个只包含三种字符的字符串：`（` ，`）` 和 `*`，写一个函数来检验这个字符串是否为有效字符串。有效字符串具有如下规则：

- 任何左括号 `(` 必须有相应的右括号 `)`。
- 任何右括号 `)` 必须有相应的左括号 `(` 。
- 左括号 `(`必须在对应的右括号之前 `)`。
- `*` 可以被视为单个右括号 `)` ，或单个左括号 `(` ，或一个空字符串。
- 一个空字符串也被视为有效字符串。

注意:

- 字符串大小将在 `[1，100]` 范围内。

# 示例

```
输入: "()"
输出: True
```

```
输入: "(*)"
输出: True
```

# 题解

## 动态规划

**状态**

定义 `dp[i][j]` 表示字符串 `s` 从下标 `i` 到 `j` 的子串是否为有效的括号字符串，其中 `0 ≤ i ≤ j < m`。

**边界**

- 字符串长度为 1 的时候，只有字符是 `*` 的时候才是 true
- 字符串长度为 2 的时候，只有字符串是 `(),*),(*,**` 其中某个组合才是 true

**转移**
假设 `c[i]` 和 `c[j]` 为左括号和右括时，或者分别为 `*` 的时候。当 `dp[i+1][j-1]=true` 的时候， `dp[i][j] = true`

如果存在 `i<= k < j` , 同时 `dp[i][k]` 和 `dp[k+1][j]` 都为 `true`，那么 `dp[i][j]=true`。因为这里是多组有效括号形成的字符串。

```ts
function checkValidString(s: string): boolean {
  const m = s.length;
  // dp[i][j]表示[i,j]范围内的字符串是否是有效括号
  const dp: boolean[][] = new Array(m).fill(0).map((_) => new Array(m).fill(false));
  // 初始化
  for (let i = 0; i < m; i++) {
    // 1个字符的话，值为‘*’是有效括号
    if (s[i] === '*') {
      dp[i][i] = true;
    }
  }
  for (let i = 1; i < m; i++) {
    // 2个字符的话，值为'()','*)','(*','**'是有效括号
    const c1 = s[i - 1];
    const c2 = s[i];
    dp[i - 1][i] = (c1 === '(' || c1 === '*') && (c2 === ')' || c2 === '*');
  }

  // 状态转移
  for (let i = m - 3; i >= 0; i--) {
    const c1 = s[i];
    for (let j = i + 2; j < m; j++) {
      const c2 = s[j];
      // 如果头尾也构成有效括号
      if ((c1 === '(' || c1 === '*') && (c2 === ')' || c2 === '*')) {
        dp[i][j] = dp[i + 1][j - 1];
      }
      // 如果是多组有效括号组成
      for (let k = i; k < j && !dp[i][j]; k++) {
        dp[i][j] = dp[i][k] && dp[k + 1][j];
      }
    }
  }

  return dp[0][m - 1];
}
```

## 栈

定义两个左括号栈和星号栈，分别来存储 `(` 和 `*` 。

遍历字符串:

- 是 `(` ,压入左括号栈
- 是 `*` ,压入星号栈
- 是 `)` 的话，需要一个左括号或者星号做匹配
  - 如果左括号栈有值，栈顶弹出
  - 如果左括号栈为空，但是星号栈有值，栈顶弹出
  - 两个栈都为空，返回 `false`

遍历完毕后，这个时候两个栈中可能还有元素。因为需要匹配左括号，这里需要将星号看作右括号。

需要注意的是，遍历栈中元素的时候需要比较一下左括号栈元素要比星号栈元素小，这里比较是下标。

最后检查左括号栈是否为空。

```ts
function checkValidString(s: string): boolean {
  const m = s.length;
  const leftStack = [];
  const starStack = [];

  // 利用双栈处理括号匹配
  for (let i = 0; i < m; i++) {
    const char = s[i];
    if (char === '(') {
      leftStack.push(i);
    } else if (char === '*') {
      starStack.push(i);
    } else {
      if (leftStack.length) {
        leftStack.pop();
      } else if (starStack.length) {
        starStack.pop();
      } else {
        return false;
      }
    }
  }

  // 双栈中可能还留有元素
  // 这个时候需要将'('和'*'做匹配。并且'('下标小于'*'
  while (leftStack.length && starStack.length) {
    const leftIdx = leftStack.pop();
    const starIdx = starStack.pop();
    if (leftIdx > starIdx) return false;
  }

  return leftStack.length === 0;
}
```
