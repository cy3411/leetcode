# 题目

为了不在赎金信中暴露字迹，从杂志上搜索各个需要的字母，组成单词来表达意思。

给你一个赎金信 (`ransomNote`) 字符串和一个杂志(`magazine`)字符串，判断 `ransomNote` 能不能由 `magazines` 里面的字符构成。

如果可以构成，返回 `true` ；否则返回 `false` 。

magazine 中的每个字符只能在 `ransomNote` 中使用一次。

提示：

- $1 \leq ransomNote.length, magazine.length \leq 10^5$
- `ransomNote` 和 `magazine` 由小写英文字母组成

# 示例

```
输入：ransomNote = "a", magazine = "b"
输出：false
```

```
输入：ransomNote = "aa", magazine = "ab"
输出：false
```

```
输入：ransomNote = "aa", magazine = "aab"
输出：true
```

# 题解

## 哈希表

使用哈希表记录杂志中的字母出现的次数，然后遍历赎金信，每次检查哈希表中是否有对应的字母，如果有，则减去一次，如果没有，则返回 false。

```ts
function canConstruct(ransomNote: string, magazine: string): boolean {
  const hash = new Map<string, number>();
  // 初始化赎金信所需的字母及其出现次数
  for (const c of magazine) {
    hash.set(c, (hash.get(c) ?? 0) + 1);
  }

  // 遍历赎金信，每次减去赎金信中的字母
  for (const c of ransomNote) {
    const count = hash.get(c) ?? 0;
    if (count === 0) {
      return false;
    }
    hash.set(c, count - 1);
  }
  return true;
}
```
