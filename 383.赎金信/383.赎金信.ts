/*
 * @lc app=leetcode.cn id=383 lang=typescript
 *
 * [383] 赎金信
 */

// @lc code=start
function canConstruct(ransomNote: string, magazine: string): boolean {
  const hash = new Map<string, number>();
  // 初始化赎金信所需的字母及其出现次数
  for (const c of magazine) {
    hash.set(c, (hash.get(c) ?? 0) + 1);
  }

  // 遍历赎金信，每次减去赎金信中的字母
  for (const c of ransomNote) {
    const count = hash.get(c) ?? 0;
    if (count === 0) {
      return false;
    }
    hash.set(c, count - 1);
  }
  return true;
}
// @lc code=end
