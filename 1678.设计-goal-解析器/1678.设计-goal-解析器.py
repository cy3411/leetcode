#
# @lc app=leetcode.cn id=1678 lang=python3
#
# [1678] 设计 Goal 解析器
#

# @lc code=start
class Solution:
    def interpret(self, command: str) -> str:
        ans = []
        for i, ch in enumerate(command):
            if ch == 'G':
                ans.append('G')
            elif ch == '(':
                ans.append('o' if command[i + 1] == ')' else 'al')

        return ''.join(ans)


# @lc code=end
