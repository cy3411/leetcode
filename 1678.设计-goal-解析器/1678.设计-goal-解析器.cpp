/*
 * @lc app=leetcode.cn id=1678 lang=cpp
 *
 * [1678] 设计 Goal 解析器
 */

// @lc code=start
class Solution {
public:
    string interpret(string command) {
        int n = command.size();
        string ans;
        for (int i = 0; i < n; i++) {
            if (command[i] == 'G') {
                ans += "G";
            } else if (command[i] == '(') {
                if (command[i + 1] == ')') {
                    ans += "o";
                } else {
                    ans += "al";
                }
            }
        }
        return ans;
    }
};
// @lc code=end
