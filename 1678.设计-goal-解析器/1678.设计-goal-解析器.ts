/*
 * @lc app=leetcode.cn id=1678 lang=typescript
 *
 * [1678] 设计 Goal 解析器
 */

// @lc code=start
function interpret(command: string): string {
  const map = new Map<string, string>([
    ['G', 'G'],
    ['()', 'o'],
    ['(al)', 'al'],
  ]);
  const n = command.length;
  let i = 0;
  let ans = '';
  while (i < n) {
    let token = '';
    if (command[i] === 'G') {
      token = 'G';
    } else if (command[i] === '(') {
      i++;
      if (command[i] === ')') {
        token = '()';
      } else {
        token = '(al)';
        i += 2;
      }
    }
    ans += map.get(token);
    i++;
  }

  return ans;
}
// @lc code=end
