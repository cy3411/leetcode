# 题目

请你设计一个可以解释字符串 command 的 Goal 解析器 。command 由 "G"、"()" 和/或 "(al)" 按某种顺序组成。Goal 解析器会将 "G" 解释为字符串 "G"、"()" 解释为字符串 "o" ，"(al)" 解释为字符串 "al" 。然后，按原顺序将经解释得到的字符串连接成一个字符串。

给你字符串 command ，返回 Goal 解析器 对 command 的解释结果。

提示：

- $1 \leq command.length \leq 100$
- command 由 "G"、"()" 和/或 "(al)" 按某种顺序组成

# 示例

```
输入：command = "G()(al)"
输出："Goal"
解释：Goal 解析器解释命令的步骤如下所示：
G -> G
() -> o
(al) -> al
最后连接得到的结果是 "Goal"
```

```
输入：command = "G()()()()(al)"
输出："Gooooal"
```

# 题解

## 遍历

遍历 command，取出对应的 token，按照题意转换即可。

```js
function interpret(command: string): string {
  const map = new Map<string, string>([
    ['G', 'G'],
    ['()', 'o'],
    ['(al)', 'al'],
  ]);
  const n = command.length;
  let i = 0;
  let ans = '';
  while (i < n) {
    let token = '';
    if (command[i] === 'G') {
      token = 'G';
    } else if (command[i] === '(') {
      i++;
      if (command[i] === ')') {
        token = '()';
      } else {
        token = '(al)';
        i += 2;
      }
    }
    ans += map.get(token);
    i++;
  }

  return ans;
}
```

```py
class Solution:
    def interpret(self, command: str) -> str:
        ans = []
        for i, ch in enumerate(command):
            if ch == 'G':
                ans.append('G')
            elif ch == '(':
                ans.append('o' if command[i + 1] == ')' else 'al')

        return ''.join(ans)
```

```cpp
class Solution {
public:
    string interpret(string command) {
        int n = command.size();
        string ans;
        for (int i = 0; i < n; i++) {
            if (command[i] == 'G') {
                ans += "G";
            } else if (command[i] == '(') {
                if (command[i + 1] == ')') {
                    ans += "o";
                } else {
                    ans += "al";
                }
            }
        }
        return ans;
    }
};
```
