# 题目
编写一个程序，找出第 n 个丑数。

丑数就是质因数只包含 2, 3, 5 的正整数。

说明:  

+ 1 是丑数。
+ n 不超过1690。

# 示例
```
输入: n = 10
输出: 12
解释: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 是前 10 个丑数。
```

# 题解
## 动态规划
定义p2,p3,p5指针，迭代处理质因数是2，3，5的正整数，每次将最小的正整数放入结果中。

指针的意义就是类似分治排序中最后合并的过程，将质因数分别是2，3，5的正整数合并成一个结果。

比如：
```
质因数是2的数列：2，4，6，8，10
质因数是3的数列：3，6，9，15
质因数是5的数列：5，10，15

每次取最小的放入新的数列，更新对应的指针位置
```

```js
/**
 * @param {number} n
 * @return {number}
 */
var nthUglyNumber = function (n) {
  const dp = new Array(n).fill(0);
  dp[0] = 1;

  let p2 = 0;
  let p3 = 0;
  let p5 = 0;
  for (let i = 1; i < n; i++) {
    dp[i] = Math.min(dp[p2] * 2, dp[p3] * 3, dp[p5] * 5);
    if (dp[i] === dp[p2] * 2) p2++;
    if (dp[i] === dp[p3] * 3) p3++;
    if (dp[i] === dp[p5] * 5) p5++;
  }
  return dp[n - 1];
};
```


## 堆
维护一个小顶堆，每次取堆顶的数据计算新的丑数后入堆，最后的堆顶就是结果。

```js
// 堆
class PQ {
  constructor(keys = [], mapKeysValue = (x) => x) {
    this.keys = [...keys];
    this.mapKeysValue = mapKeysValue;

    for (let i = (this.keys.length - 2) >> 1; i >= 0; i--) {
      this.sink(i);
    }
  }

  less(i, j) {
    return this.mapKeysValue(this.keys[i]) < this.mapKeysValue(this.keys[j]);
  }

  exch(i, j) {
    [this.keys[i], this.keys[j]] = [this.keys[j], this.keys[i]];
  }

  swin(index) {
    let parent = (index - 1) >> 1;
    while (parent >= 0 && this.less(parent, index)) {
      this.exch(parent, index);
      index = parent;
      parent = (parent - 1) >> 1;
    }
  }

  sink(index) {
    let child = index * 2 + 1;
    let len = this.keys.length;
    while (child < len) {
      if (child + 1 < len && this.less(child, child + 1)) {
        child++;
      }
      if (this.less(child, index)) break;

      this.exch(child, index);
      index = child;
      child = index * 2 + 1;
    }
  }

  size() {
    return this.keys.length;
  }

  insert(key) {
    this.keys.push(key);
    this.swin(this.keys.length - 1);
  }

  poll() {
    let head = this.peek();
    this.exch(0, this.size() - 1);
    this.keys.pop();
    this.sink(0);
    return head;
  }

  peek() {
    return this.keys[0];
  }
}
// 大顶堆
class MaxPQ extends PQ {}
// 小顶堆
class MinPQ extends PQ {
  constructor(keys, mapKeysValue = (x) => x) {
    super(keys, (x) => -1 * mapKeysValue(x));
  }
}

/**
 * @param {number} n
 * @return {number}
 */
var nthUglyNumber = function (n) {
  // 小顶堆
  const minPQ = new MinPQ([1]);

  let result = 0;
  // 每次取出最小值，将计算后的数值重新入堆。
  while (n--) {
    result = minPQ.poll();
    if (result % 5 === 0) {
      minPQ.insert(result * 5);
    } else if (result % 3 === 0) {
      minPQ.insert(result * 3);
      minPQ.insert(result * 5);
    } else {
      minPQ.insert(result * 2);
      minPQ.insert(result * 3);
      minPQ.insert(result * 5);
    }
  }

  return result;
};
```