# 题目

假设有从 1 到 N 的 N 个整数，如果从这 N 个数字中成功构造出一个数组，使得数组的第 i 位 (1 <= i <= N) 满足如下两个条件中的一个，我们就称这个数组为一个优美的排列。条件：

- 第 i 位的数字能被 i 整除
- i 能被第 i 位上的数字整除

现在给定一个整数 N，请问可以构造多少个优美的排列？

说明:

- N 是一个正整数，并且不会超过 15。

# 示例

```
输入: 2
输出: 2
解释:

第 1 个优美的排列是 [1, 2]:
  第 1 个位置（i=1）上的数字是1，1能被 i（i=1）整除
  第 2 个位置（i=2）上的数字是2，2能被 i（i=2）整除

第 2 个优美的排列是 [2, 1]:
  第 1 个位置（i=1）上的数字是2，2能被 i（i=1）整除
  第 2 个位置（i=2）上的数字是1，i（i=2）能被 1 整除
```

# 题解

## 回溯

递归遍历每个数字，如果满足条件，就设置为优美排列的元素，然后递归去找下一个满足条件的元素。

如果找寻失败，就回溯回去，找前一个新的匹配。

```ts
function countArrangement(n: number): number {
  const visited = new Array(n + 1).fill(0);
  let ans = 0;
  //  idx表示优美排列的第几位数字
  const backtrack = (idx: number) => {
    if (idx === n + 1) {
      ans++;
      return;
    }

    for (let i = 1; i <= n; i++) {
      if (visited[i]) continue;
      // 满足条件的，继续找下一个元素
      if (i % idx === 0 || idx % i === 0) {
        visited[i] = 1;
        backtrack(idx + 1);
        visited[i] = 0;
      }
    }
  };

  backtrack(1);

  return ans;
}
```
