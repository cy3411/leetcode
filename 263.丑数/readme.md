# 题目
给你一个整数 n ，请你判断 n 是否为 丑数 。如果是，返回 true ；否则，返回 false 。

丑数 就是只包含质因数 2、3 和/或 5 的正整数。

提示：

+ -231 <= n <= 231 - 1

# 示例
```
输入：n = 6
输出：true
解释：6 = 2 × 3
```

```
输入：n = 1
输出：true
解释：1 通常被视为丑数。
```

# 题解
```js
/**
 * @param {number} n
 * @return {boolean}
 */
var isUgly = function (n) {
  // 小于0的，不符合结果
  if (n <= 0) return false;
  if (n === 1) return true;

  while (n > 1) {
    if (n % 5 === 0) {
      n = n / 5;
    } else if (n % 3 === 0) {
      n = n / 3;
    } else if (n % 2 === 0) {
      n = n / 2;
    } else {
      return false;
    }
  }

  return true;
};
```