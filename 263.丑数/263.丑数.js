/*
 * @lc app=leetcode.cn id=263 lang=javascript
 *
 * [263] 丑数
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */
var isUgly = function (n) {
  // 小于0的，不符合结果
  if (n <= 0) return false;
  if (n === 1) return true;

  while (n > 1) {
    if (n % 5 === 0) {
      n = n / 5;
    } else if (n % 3 === 0) {
      n = n / 3;
    } else if (n % 2 === 0) {
      n = n / 2;
    } else {
      return false;
    }
  }

  return true;
};
// @lc code=end
