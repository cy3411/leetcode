# 题目

N x N 的棋盘 board 上，按从 1 到 N\*N 的数字给方格编号，编号 从左下角开始，每一行交替方向。

例如，一块 6 x 6 大小的棋盘，编号如下：

[![RYcPgA.md.png](https://z3.ax1x.com/2021/06/27/RYcPgA.md.png)](https://imgtu.com/i/RYcPgA)

r 行 c 列的棋盘，按前述方法编号，棋盘格中可能存在 “蛇” 或 “梯子”；如果 board[r][c] != -1，那个蛇或梯子的目的地将会是 board[r][c]。

玩家从棋盘上的方格 1 （总是在最后一行、第一列）开始出发。

每一回合，玩家需要从当前方格 x 开始出发，按下述要求前进：

- 选定目标方格：选择从编号 x+1，x+2，x+3，x+4，x+5，或者 x+6 的方格中选出一个目标方格 s ，目标方格的编号 <= N\*N。
  - 该选择模拟了掷骰子的情景，无论棋盘大小如何，你的目的地范围也只能处于区间 [x+1, x+6] 之间。
- 传送玩家：如果目标方格 S 处存在蛇或梯子，那么玩家会传送到蛇或梯子的目的地。否则，玩家传送到目标方格 S。
  注意，玩家在每回合的前进过程中最多只能爬过蛇或梯子一次：就算目的地是另一条蛇或梯子的起点，你也不会继续移动。

返回达到方格 N\*N 所需的最少移动次数，如果不可能，则返回 -1。

提示：

- 2 <= board.length = board[0].length <= 20
- board[i][j] 介于 1 和 N\*N 之间或者等于 -1。
- 编号为 1 的方格上没有蛇或梯子。
- 编号为 N\*N 的方格上没有蛇或梯子。

# 示例

```
输入：[
[-1,-1,-1,-1,-1,-1],
[-1,-1,-1,-1,-1,-1],
[-1,-1,-1,-1,-1,-1],
[-1,35,-1,-1,13,-1],
[-1,-1,-1,-1,-1,-1],
[-1,15,-1,-1,-1,-1]]
输出：4
解释：
首先，从方格 1 [第 5 行，第 0 列] 开始。
你决定移动到方格 2，并必须爬过梯子移动到到方格 15。
然后你决定移动到方格 17 [第 3 行，第 5 列]，必须爬过蛇到方格 13。
然后你决定移动到方格 14，且必须通过梯子移动到方格 35。
然后你决定移动到方格 36, 游戏结束。
可以证明你需要至少 4 次移动才能到达第 N*N 个方格，所以答案是 4。
```

# 题解

## 广度优先搜索

这里所谓蛇或者梯子，就是可以二次移动。当目标坐标的值大于 0 的话，我们可以再一次移动到目标坐标值的位置。

```ts
class Data {
  constructor(public positon: number, public step: number) {
    this.positon = positon;
    this.step = step;
  }
}

function snakesAndLadders(board: number[][]): number {
  const n = board.length;
  const getCoordinates = (position: number): number[] => {
    // 这里格子的下标是从1开始计算的，计算坐标的时候要减去1
    let r = ((position - 1) / n) >> 0;
    let c = (position - 1) % n;
    // 奇数行，从右往左排列
    if (r % 2 === 1) {
      c = n - 1 - c;
    }
    return [n - 1 - r, c];
  };
  // 已访问状态
  const visited: number[] = new Array(n * n + 1).fill(0);
  // 搜索状态
  const queue: Data[] = [];
  // 初始化从位置1开始搜索
  queue.push(new Data(1, 0));
  visited[1] = 1;

  while (queue.length) {
    const data = queue.shift();
    if (data.positon === n * n) return data.step;
    for (let i = 1; i <= 6; i++) {
      let next = data.positon + i;
      if (next > n * n) break;
      // 一维坐标转二维坐标
      let [x, y] = getCoordinates(next);
      // 是否有蛇或者梯子
      if (board[x][y] > 0) next = board[x][y];
      if (visited[next] === 1) continue;
      visited[next] = 1;
      queue.push(new Data(next, data.step + 1));
    }
  }

  return -1;
}
```
