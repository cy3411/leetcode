# 题目
在本问题中, 树指的是一个连通且无环的无向图。

输入一个图，该图由一个有着N个节点 (节点值不重复1, 2, ..., N) 的树及一条附加的边构成。附加的边的两个顶点包含在1到N中间，这条附加的边不属于树中已存在的边。

结果图是一个以`边`组成的二维数组。每一个边的元素是一对`[u, v]` ，满足 `u < v`，表示连接顶点`u`和`v`的**无向**图的边。

返回一条可以删去的边，使得结果图是一个有着N个节点的树。如果有多个答案，则返回二维数组中最后出现的边。答案边 `[u, v]` 应满足相同的格式 `u < v`。

# 示例
```
输入: [[1,2], [1,3], [2,3]]
输出: [2,3]
解释: 给定的无向图为:
  1
 / \
2 - 3
```

```
输入: [[1,2], [2,3], [3,4], [1,4], [1,5]]
输出: [1,4]
解释: 给定的无向图为:
5 - 1 - 2
    |   |
    4 - 3
```
# 题解
## 并查集
遍历数组，按需将边加入到并查集中，加入之前检查当前2个点是否已经连通，是的话当前边就是答案。
```js
class UnionFind {
  parent: number[];
  size: number[];
  count: number;
  constructor(n: number = 0) {
    this.parent = new Array(n).fill(0).map((_, i) => i);
    this.size = new Array(n).fill(1);
    this.count = n;
  }
  find(x: number): number {
    return this.parent[x] === x ? x : (this.parent[x] = this.find(this.parent[x]));
  }
  union(x: number, y: number): void {
    let rx = this.find(x);
    let ry = this.find(y);
    if (this.size[rx] > this.size[ry]) {
      [rx, ry] = [ry, rx];
    }
    this.size[ry] += this.size[rx];
    this.parent[rx] = ry;
    this.count--;
  }
  isConnected(x: number, y: number): boolean {
    return this.find(x) === this.find(y);
  }
}
function findRedundantConnection(edges: number[][]): number[] {
  const size = edges.length;
  const unionFind = new UnionFind(size + 1);

  for (let edge of edges) {
    let [x, y] = edge;
    // 判断是否有连通性，是的话就是一个冗余链接
    if (unionFind.isConnected(x, y)) {
      return edge;
    }
    unionFind.union(x, y);
  }
}
```