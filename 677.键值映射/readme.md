# 题目

实现一个 `MapSum` 类，支持两个方法，`insert` 和 `sum`：

- `MapSum()` 初始化 `MapSum` 对象
- `void insert(String key, int val)` 插入 `key-val` 键值对，字符串表示键 `key` ，整数表示值 `val` 。如果键 `key` 已经存在，那么原来的键值对将被替代成新的键值对。
- `int sum(string prefix)` 返回所有以该前缀 `prefix` 开头的键 `key` 的值的总和。

提示：

- `1 <= key.length, prefix.length <= 50`
- `key` 和 `prefix` 仅由小写英文字母组成
- `1 <= val <= 1000`
- 最多调用 `50` 次 `insert` 和 `sum`

# 示例

```
输入：
["MapSum", "insert", "sum", "insert", "sum"]
[[], ["apple", 3], ["ap"], ["app", 2], ["ap"]]
输出：
[null, null, 3, null, 5]

解释：
MapSum mapSum = new MapSum();
mapSum.insert("apple", 3);
mapSum.sum("ap");           // return 3 (apple = 3)
mapSum.insert("app", 2);
mapSum.sum("ap");           // return 5 (apple + app = 3 + 2 = 5)
```

# 题解

## Tire Tree

使用 trie 树来实现，每个节点都保存当前字符前缀的 word。

使用哈希表来保存每个 word 的值。

sum 时，取出所有以 prefix 开头的 word，然后求和。

```ts
class Node677 {
  next: Map<string, Node677>;
  // 当前前缀的所有单词
  words: Set<string>;
  isWord: boolean;
  constructor(word: string = '') {
    this.next = new Map();
    this.words = new Set();
    this.isWord = false;
    if (word) {
      this.words.add(word);
    }
  }

  addWord(word: string) {
    this.words.add(word);
  }
}

class Tire677 {
  root: Node677;
  constructor() {
    this.root = new Node677();
  }

  insert(word: string, val: number) {
    let node: Node677 = this.root;
    for (let i = 0; i < word.length; i++) {
      let c = word[i];
      if (!node.next.has(c)) {
        node.next.set(c, new Node677(word));
      }
      node = node.next.get(c);
      node.addWord(word);
    }
    node.isWord = true;
  }

  search(word: string): Set<string> | null {
    let node: Node677 = this.root;
    for (let i = 0; i < word.length; i++) {
      let c = word[i];
      if (!node.next.has(c)) {
        return null;
      }
      node = node.next.get(c);
    }
    return node.words;
  }
}

class MapSum {
  root: Tire677;
  // 键值映射
  hash: Map<string, number> = new Map();
  constructor() {
    this.root = new Tire677();
  }

  insert(key: string, val: number): void {
    this.hash.set(key, val);
    this.root.insert(key, val);
  }

  sum(prefix: string): number {
    const words = this.root.search(prefix);
    let ans = 0;
    if (words) {
      for (let word of words) {
        ans += this.hash.get(word);
      }
    }
    return ans;
  }
}
```
