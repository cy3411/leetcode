/*
 * @lc app=leetcode.cn id=2181 lang=typescript
 *
 * [2181] 合并零之间的节点
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

function mergeNodes(head: ListNode | null): ListNode | null {
  if (head.next === null) return null;
  // 值为0的结点
  let p = head;
  // 合并后面的结点值
  while (p.next.val !== 0) {
    p.val += p.next.val;
    p.next = p.next.next;
  }
  // 将下一个0结点合并值后放到当前结点后面
  p.next = mergeNodes(p.next);

  return head;
}
// @lc code=end
