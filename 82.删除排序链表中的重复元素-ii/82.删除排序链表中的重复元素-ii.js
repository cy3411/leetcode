/*
 * @lc app=leetcode.cn id=82 lang=javascript
 *
 * [82] 删除排序链表中的重复元素 II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
  if (!head || !head.next) {
    return head;
  }
  const dummy = new ListNode(-1, head);
  let prev = dummy;
  let curr = dummy.next;

  while (curr && curr.next) {
    if (prev.next.val === curr.next.val) {
      while (curr && curr.next && prev.next.val === curr.next.val) {
        curr = curr.next;
      }
      prev.next = curr.next;
      curr = curr.next;
    } else {
      prev = prev.next;
      curr = curr.next;
    }
  }
  return dummy.next;
};
// @lc code=end
