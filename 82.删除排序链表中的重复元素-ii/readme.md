# 描述

给定一个排序链表，删除所有含有重复数字的节点，只保留原始链表中 没有重复出现 的数字

# 示例

```
输入: 1->2->3->3->4->4->5
输出: 1->2->5
```
```
输入: 1->1->1->2->3
输出: 2->3
```

# 方法
涉及到头节点的修改，需要一个虚拟头节点，定义 pre=dummy,curr=dummy.next，比较 pre.next 和 curr.next 的值是否相等，不相等的时候2个节点同时后移，相等的时候将curr节点后移比较。

```js
var deleteDuplicates = function (head) {
  if (!head || !head.next) {
    return head;
  }
  const dummy = new ListNode(-1, head);
  let prev = dummy;
  let curr = dummy.next;

  while (curr && curr.next) {
    if (prev.next.val === curr.next.val) {
      while (curr && curr.next && prev.next.val === curr.next.val) {
        curr = curr.next;
      }
      prev.next = curr.next;
      curr = curr.next;
    } else {
      prev = prev.next;
      curr = curr.next;
    }
  }
  return dummy.next;
};
```