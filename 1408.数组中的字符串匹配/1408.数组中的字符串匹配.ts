/*
 * @lc app=leetcode.cn id=1408 lang=typescript
 *
 * [1408] 数组中的字符串匹配
 */

// @lc code=start
function stringMatching(words: string[]): string[] {
  const ans: string[] = [];
  const n = words.length;
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (i === j) continue;
      if (words[j].includes(words[i])) {
        ans.push(words[i]);
        break;
      }
    }
  }
  return ans;
}
// @lc code=end
