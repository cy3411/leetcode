/*
 * @lc app=leetcode.cn id=796 lang=typescript
 *
 * [796] 旋转字符串
 */

// @lc code=start
function rotateString(s: string, goal: string): boolean {
  const m = s.length;
  const n = goal.length;

  if (m !== n) return false;

  const str = s + s;

  return str.indexOf(goal) !== -1;
}
// @lc code=end
