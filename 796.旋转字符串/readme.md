# 题目

给定两个字符串, `s` 和 `goal`。如果在若干次旋转操作之后，`s` 能变成 `goal` ，那么返回 `true` 。

`s` 的 **旋转操作** 就是将 `s` 最左边的字符移动到最右边。

例如, 若 `s = 'abcde'`，在旋转一次之后结果就是 `'bcdea'` 。

提示:

- $1 \leq s.length, goal.length \leq 100$
- `s` 和 `goal` 由小写英文字母组成

# 示例

```
输入: s = "abcde", goal = "cdeab"
输出: true
```

```
输入: s = "abcde", goal = "abced"
输出: false
```

# 题解

将 s 重复 2 次拼接后得到新的字符串，再将从新的字符串中查找 goal。

## JS API

```ts
function rotateString(s: string, goal: string): boolean {
  const m = s.length;
  const n = goal.length;

  if (m !== n) return false;

  const str = s + s;

  return str.indexOf(goal) !== -1;
}
```

## KMP

```cpp
class Solution
{
public:
    void getNext(string &patten, vector<int> &next)
    {
        next[0] = -1;
        for (int i = 1, j = -1; i < patten.size(); i++)
        {
            while (j != -1 && patten[i] != patten[j + 1])
            {
                j = next[j];
            }
            if (patten[i] == patten[j + 1])
            {
                j++;
            }
            next[i] = j;
        }
    }
    int kmp(string &s, string &goal, vector<int> &next)
    {
        for (int i = 0, j = -1; i < s.size(); i++)
        {
            while (j != -1 && s[i] != goal[j + 1])
            {
                j = next[j];
            }
            if (s[i] == goal[j + 1])
            {
                j++;
            }
            if (j == goal.size() - 1)
            {
                return i - j;
            }
        }
        return -1;
    }
    bool rotateString(string s, string goal)
    {
        int m = s.size(), n = goal.size();
        if (m != n)
        {
            return false;
        }

        string s1 = s + s;
        vector<int> next(n, 0);
        getNext(goal, next);

        return kmp(s1, goal, next) != -1;
    }
};
```
