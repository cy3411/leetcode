# 题目

给你一个 **互不相同** 的整数数组，其中 `locations[i]` 表示第 `i` 个城市的位置。同时给你 `start`，`finish` 和 `fuel` 分别表示出发城市、目的地城市和你初始拥有的汽油总量

每一步中，如果你在城市 `i` ，你可以选择任意一个城市 `j` ，满足 $j != i 且 0 \leq j < locations.length$ ，并移动到城市 `j` 。从城市 `i` 移动到 `j` 消耗的汽油量为 `|locations[i] - locations[j]|`，`|x|` 表示 `x` 的绝对值。

请注意， `fuel` 任何时刻都 **不能** 为负，且你 **可以** 经过任意城市超过一次（包括 start 和 finish ）。

请你返回从 `start` 到 `finish` 所有可能路径的数目。

由于答案可能很大， 请将它对 `10^9 + 7` 取余后返回。

提示：

- $2 \leq locations.length \leq 100$
- $1 \leq locations[i] \leq 10^9$
- 所有 locations 中的整数 **互不相同** 。
- $0 \leq start, finish < locations.length$
- $1 \leq fuel \leq 200$

# 示例

```
输入：locations = [2,3,6,8,4], start = 1, finish = 3, fuel = 5
输出：4
解释：以下为所有可能路径，每一条都用了 5 单位的汽油：
1 -> 3
1 -> 2 -> 3
1 -> 4 -> 3
1 -> 4 -> 2 -> 3
```

```
输入：locations = [4,3,1], start = 1, finish = 0, fuel = 6
输出：5
解释：以下为所有可能的路径：
1 -> 0，使用汽油量为 fuel = 1
1 -> 2 -> 0，使用汽油量为 fuel = 5
1 -> 2 -> 1 -> 0，使用汽油量为 fuel = 5
1 -> 0 -> 1 -> 0，使用汽油量为 fuel = 3
1 -> 0 -> 1 -> 0 -> 1 -> 0，使用汽油量为 fuel = 5
```

```
输入：locations = [5,2,1], start = 0, finish = 2, fuel = 3
输出：0
解释：没有办法只用 3 单位的汽油从 0 到达 2 。因为最短路径需要 4 单位的汽油。
```

# 题解

## 递推

定义 f[i][j] 表示从 i 到终点剩余 j 油量的方法总数。

状态转移：

$$
    f[i][j] = \sum_{k=1}^{locations.length} f[k][j - |locations[i] - locations[k]|]
$$

由于 f[i][j] 依赖之前城市所有状态，所以这里使用递归的方式，在回溯的过程中，依次更新状态。

```ts
function countRoutes(locations: number[], start: number, finish: number, fuel: number): number {
  const mod = 1e9 + 7;
  const n = locations.length;
  // f[i][j] 表示从 i 到终点剩余 j 油量的方法数
  const f: number[][] = new Array(n + 1).fill(0).map(() => new Array(fuel + 1).fill(-1));
  // 计算过程
  const getResult = (s: number, e: number, r: number): number => {
    // 如果已经计算过，直接返回
    if (f[s][r] !== -1) return f[s][r];
    // 如果起点终点相同，最少有 1 个方法
    if (s === e) f[s][r] = 1;
    // 如果起点终点不同，方法数为 0
    else f[s][r] = 0;

    for (let i = 0; i < n; i++) {
      // 起点相同，跳过
      if (s === i) continue;
      // 如果 s 到 i 的距离大于剩余油量，跳过
      const diff = Math.abs(locations[s] - locations[i]);
      if (diff > r) continue;
      // 更新方法数
      f[s][r] += getResult(i, e, r - diff);
      f[s][r] %= mod;
    }

    return f[s][r];
  };

  return getResult(start, finish, fuel);
}
```

```cpp
class Solution
{
    int f[105][205], modNum = 1e9 + 7, n;

public:
    int getResult(int s, int e, int r, vector<int> &locations)
    {
        if (f[s][r] != -1)
            return f[s][r];

        if (s == e)
            f[s][r] = 1;
        else
            f[s][r] = 0;

        for (int i = 0; i < n; i++)
        {
            if (s == i)
                continue;
            int diff = abs(locations[s] - locations[i]);
            if (diff > r)
                continue;
            f[s][r] += getResult(i, e, r - diff, locations);
            f[s][r] %= modNum;
        }

        return f[s][r];
    }

    int countRoutes(vector<int> &locations, int start, int finish, int fuel)
    {
        n = locations.size();
        // 初始化为 -1， 表示未访问
        memset(f, -1, sizeof(f));

        return getResult(start, finish, fuel, locations);
    }
};
```
