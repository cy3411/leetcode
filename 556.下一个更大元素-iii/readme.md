# 题目

给你一个正整数 `n` ，请你找出符合条件的最小整数，其由重新排列 `n` 中存在的每位数字组成，并且其值大于 `n` 。如果不存在这样的正整数，则返回 `-1` 。

注意 ，返回的整数应当是一个 `32` 位整数 ，如果存在满足题意的答案，但不是 `32` 位整数 ，同样返回 `-1` 。

提示：

- $1 \leq n \leq 2^{31} - 1$

# 示例

```
输入：n = 12
输出：21
```

```
输入：n = 21
输出：-1
```

# 题解

## 下一个排列

题意给出下一个排列一定要比现在的排列大，具体的规则是：

- 找到左边的较小数和右边的较大数交换，能够使排列变大
- 较小数要尽可能的靠右边，较大数要尽可能的小
- 交换完毕后，较大数右边的数字要升序排列，这样可以保证变大的幅度最小

```ts
function nextGreaterElement(n: number): number {
  const max = 2 ** 31 - 1;
  const nums = [...String(n)];
  const reverse = (nums: string[], begin: number) => {
    let i = begin;
    let j = nums.length - 1;
    while (i < j) {
      [nums[i], nums[j]] = [nums[j], nums[i]];
      i++;
      j--;
    }
  };

  // 尽量靠右的较小数
  let i = nums.length - 2;
  while (i >= 0 && nums[i] >= nums[i + 1]) {
    i--;
  }

  if (i < 0) return -1;

  // 比较小数稍大一点的数字
  let j = nums.length - 1;
  while (j >= 0 && nums[i] >= nums[j]) {
    j--;
  }
  [nums[i], nums[j]] = [nums[j], nums[i]];
  // 反转右边为升序序列
  reverse(nums, i + 1);

  const ans = Number(nums.join(''));

  return ans > max ? -1 : ans;
}
```
