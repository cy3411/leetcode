#
# @lc app=leetcode.cn id=791 lang=python3
#
# [791] 自定义字符串排序
#

# @lc code=start
class Solution:
    def customSortString(self, order: str, s: str) -> str:
        m = defaultdict(int)
        for i, ch in enumerate(order):
            m[ch] = i + 1

        return ''.join(sorted(s, key=lambda x: m[x]))

# @lc code=end
