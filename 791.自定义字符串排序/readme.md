# 题目

给定两个字符串 order 和 s 。order 的所有单词都是 唯一 的，并且以前按照一些自定义的顺序排序。

对 s 的字符进行置换，使其与排序的 order 相匹配。更具体地说，如果在 order 中的字符 x 出现字符 y 之前，那么在排列后的字符串中， x 也应该出现在 y 之前。

返回 满足这个性质的 s 的任意排列 。

提示:

- $1 \leq order.length \leq 26$
- $1 \leq s.length \leq 200$
- order 和 s 由小写英文字母组成
- order 中的所有字符都 不同

# 示例

```
输入: order = "cba", s = "abcd"
输出: "cbad"
解释:
“a”、“b”、“c”是按顺序出现的，所以“a”、“b”、“c”的顺序应该是“c”、“b”、“a”。
因为“d”不是按顺序出现的，所以它可以在返回的字符串中的任何位置。“dcba”、“cdba”、“cbda”也是有效的输出。
```

```
输入: order = "cbafg", s = "abcd"
输出: "cbad"
```

# 题解

## 自定义排序

预先遍历 order， 给每一个字符赋予排序的权值。

最后自定义排序，根据权值表对 s 进行排序，最后得到满足题意的答案。

```ts
function customSortString(order: string, s: string): string {
  // 字符与顺序的映射关系
  const map = new Map<string, number>();
  for (let i = 0; i < order.length; i++) {
    map.set(order[i], i);
  }

  const arr = s.split('');
  // 排序
  arr.sort((a: string, b: string): number => {
    const aidx = map.get(a) ?? Number.MAX_SAFE_INTEGER;
    const bidx = map.get(b) ?? Number.MAX_SAFE_INTEGER;
    return aidx - bidx;
  });

  return arr.join('');
}
```

```cpp
class Solution {
public:
    string customSortString(string order, string s) {
        vector<int> m(26);
        for (int i = 0; i < order.size(); i++) {
            m[order[i] - 'a'] = i + 1;
        }
        sort(s.begin(), s.end(),
             [&](char a, char b) { return m[a - 'a'] < m[b - 'a']; });

        return s;
    }
};
```

```py
class Solution:
    def customSortString(self, order: str, s: str) -> str:
        m = defaultdict(int)
        for i, ch in enumerate(order):
            m[ch] = i + 1

        return ''.join(sorted(s, key=lambda x: m[x]))
```
