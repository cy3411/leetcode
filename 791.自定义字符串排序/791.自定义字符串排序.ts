/*
 * @lc app=leetcode.cn id=791 lang=typescript
 *
 * [791] 自定义字符串排序
 */

// @lc code=start
function customSortString(order: string, s: string): string {
  // 字符与顺序的映射关系
  const map = new Map<string, number>();
  for (let i = 0; i < order.length; i++) {
    map.set(order[i], i);
  }

  const arr = s.split('');
  // 排序
  arr.sort((a: string, b: string): number => {
    const aidx = map.get(a) ?? Number.MAX_SAFE_INTEGER;
    const bidx = map.get(b) ?? Number.MAX_SAFE_INTEGER;
    return aidx - bidx;
  });

  return arr.join('');
}
// @lc code=end
