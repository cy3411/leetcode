/*
 * @lc app=leetcode.cn id=791 lang=cpp
 *
 * [791] 自定义字符串排序
 */

// @lc code=start
class Solution {
public:
    string customSortString(string order, string s) {
        vector<int> m(26);
        for (int i = 0; i < order.size(); i++) {
            m[order[i] - 'a'] = i + 1;
        }
        sort(s.begin(), s.end(),
             [&](char a, char b) { return m[a - 'a'] < m[b - 'a']; });

        return s;
    }
};
// @lc code=end
