# 题目

给你一个由非负整数 `a1, a2, ..., an` 组成的数据流输入，请你将到目前为止看到的数字总结为不相交的区间列表。

实现 `SummaryRanges` 类：

- `SummaryRanges()` 使用一个空数据流初始化对象。
- `void addNum(int val)` 向数据流中加入整数 `val` 。
- `int[][] getIntervals()` 以不相交区间 $[start_i, end_i]$ 的列表形式返回对数据流中整数的总结。

提示：

- $0 <= val <= 10^4$
- 最多调用 addNum 和 getIntervals 方法 $3 * 10^4$ 次

# 示例

```
输入：
["SummaryRanges", "addNum", "getIntervals", "addNum", "getIntervals", "addNum", "getIntervals", "addNum", "getIntervals", "addNum", "getIntervals"]
[[], [1], [], [3], [], [7], [], [2], [], [6], []]
输出：
[null, null, [[1, 1]], null, [[1, 1], [3, 3]], null, [[1, 1], [3, 3], [7, 7]], null, [[1, 3], [7, 7]], null, [[1, 3], [6, 7]]]

解释：
SummaryRanges summaryRanges = new SummaryRanges();
summaryRanges.addNum(1);      // arr = [1]
summaryRanges.getIntervals(); // 返回 [[1, 1]]
summaryRanges.addNum(3);      // arr = [1, 3]
summaryRanges.getIntervals(); // 返回 [[1, 1], [3, 3]]
summaryRanges.addNum(7);      // arr = [1, 3, 7]
summaryRanges.getIntervals(); // 返回 [[1, 1], [3, 3], [7, 7]]
summaryRanges.addNum(2);      // arr = [1, 2, 3, 7]
summaryRanges.getIntervals(); // 返回 [[1, 3], [7, 7]]
summaryRanges.addNum(6);      // arr = [1, 2, 3, 6, 7]
summaryRanges.getIntervals(); // 返回 [[1, 3], [6, 7]]
```

# 题解

## 并查集

利用并查集特性，每次 `addNum` 时，将下一个能与 `val` 相连的数字当作值放入并查集数组。

在计算区间的时候，右边最大区间就是当前相连集合的根元素。

```ts
class SummaryRanges {
  nums: number[];
  constructor() {
    this.nums = new Array(10002);
  }

  addNum(val: number): void {
    if (this.nums[val] === void 0) {
      this.nums[val] = val + 1;
    }
    this.find(val);
  }

  getIntervals(): number[][] {
    const ans = [];
    for (let i = 0; i < 10001; ) {
      if (this.nums[i] != undefined) {
        let tmp = new Array(2);
        tmp[0] = i;
        tmp[1] = this.find(this.nums[i]) - 1;
        i = tmp[1] + 1;
        ans.push(tmp);
      } else {
        i++;
      }
    }
    return ans;
  }

  find(x: number) {
    if (this.nums[x] === void 0) {
      return x;
    }
    this.nums[x] = this.find(this.nums[x]);
    return this.nums[x];
  }
}
```
