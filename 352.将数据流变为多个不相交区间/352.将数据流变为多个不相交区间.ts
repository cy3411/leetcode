/*
 * @lc app=leetcode.cn id=352 lang=typescript
 *
 * [352] 将数据流变为多个不相交区间
 */

// @lc code=start
class SummaryRanges {
  nums: number[];
  constructor() {
    this.nums = new Array(10002);
  }

  addNum(val: number): void {
    if (this.nums[val] === void 0) {
      this.nums[val] = val + 1;
    }
    this.find(val);
  }

  getIntervals(): number[][] {
    const ans = [];
    for (let i = 0; i < 10001; ) {
      if (this.nums[i] != undefined) {
        let tmp = new Array(2);
        tmp[0] = i;
        tmp[1] = this.find(this.nums[i]) - 1;
        i = tmp[1] + 1;
        ans.push(tmp);
      } else {
        i++;
      }
    }
    return ans;
  }

  find(x: number) {
    if (this.nums[x] === void 0) {
      return x;
    }
    this.nums[x] = this.find(this.nums[x]);
    return this.nums[x];
  }
}

/**
 * Your SummaryRanges object will be instantiated and called as such:
 * var obj = new SummaryRanges()
 * obj.addNum(val)
 * var param_2 = obj.getIntervals()
 */
// @lc code=end
