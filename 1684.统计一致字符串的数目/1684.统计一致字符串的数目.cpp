/*
 * @lc app=leetcode.cn id=1684 lang=cpp
 *
 * [1684] 统计一致字符串的数目
 */

// @lc code=start
class Solution {
public:
    int countConsistentStrings(string allowed, vector<string> &words) {
        int mask = 0;
        for (int i = 0; i < allowed.size(); i++) {
            mask |= 1 << (allowed[i] - 'a');
        }

        int ans = 0;
        for (auto &word : words) {
            int m = 0;
            for (auto c : word) {
                m |= 1 << (c - 'a');
            }
            if ((mask | m) == mask) {
                ans++;
            }
        }
        return ans;
    }
};
// @lc code=end
