/*
 * @lc app=leetcode.cn id=1684 lang=typescript
 *
 * [1684] 统计一致字符串的数目
 */

// @lc code=start
function countConsistentStrings(allowed: string, words: string[]): number {
  const map = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  for (let i = 0; i < allowed.length; i++) {
    const idx = allowed.charCodeAt(i) - base;
    map[idx] = 1;
  }

  let ans = 0;
  for (const word of words) {
    let flag = true;
    for (const char of word) {
      const idx = char.charCodeAt(0) - base;
      if (map[idx] === 0) {
        flag = false;
        break;
      }
    }

    flag && ans++;
  }

  return ans;
}
// @lc code=end
