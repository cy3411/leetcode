# 题目

给你一个由不同字符组成的字符串 allowed 和一个字符串数组 words 。如果一个字符串的每一个字符都在 allowed 中，就称这个字符串是 一致字符串 。

请你返回 words 数组中 一致字符串 的数目。

提示：

- $1 \leq words.length \leq 104$
- $1 \leq allowed.length \leq 26$
- $1 \leq words[i].length \leq 10$
- allowed 中的字符 互不相同 。
- words[i] 和 allowed 只包含小写英文字母。

# 示例

```
输入：allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
输出：2
解释：字符串 "aaab" 和 "baa" 都是一致字符串，因为它们只包含字符 'a' 和 'b' 。
```

```
输入：allowed = "abc", words = ["a","b","c","ab","ac","bc","abc"]
输出：7
解释：所有字符串都是一致的。
```

# 题解

## 遍历

利用哈希表来保存 allowed 中字符是否出现。

遍历字符串数组 words，如果 words[i]某个字符没有在哈希表中出现，则跳过。否则，当前元素加入结果计数。

```ts
function countConsistentStrings(allowed: string, words: string[]): number {
  const map = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  for (let i = 0; i < allowed.length; i++) {
    const idx = allowed.charCodeAt(i) - base;
    map[idx] = 1;
  }

  let ans = 0;
  for (const word of words) {
    let flag = true;
    for (const char of word) {
      const idx = char.charCodeAt(0) - base;
      if (map[idx] === 0) {
        flag = false;
        break;
      }
    }

    flag && ans++;
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int countConsistentStrings(string allowed, vector<string> &words) {
        int mask = 0;
        for (int i = 0; i < allowed.size(); i++) {
            mask |= 1 << (allowed[i] - 'a');
        }

        int ans = 0;
        for (auto &word : words) {
            int m = 0;
            for (auto c : word) {
                m |= 1 << (c - 'a');
            }
            if ((mask | m) == mask) {
                ans++;
            }
        }
        return ans;
    }
};
```
