# 题目

给定一个 正整数 `num` ，编写一个函数，如果 `num` 是一个完全平方数，则返回 `true` ，否则返回 `false` 。

进阶：不要 使用任何内置的库函数，如 `sqrt` 。

提示：

- $1 <= num <= 2^{31} - 1$

# 示例

```
输入：num = 16
输出：true
```

```
输入：num = 14
输出：false
```

# 题解

## 二分查找

二分查找，查找的范围是 [1, num] 。

找到一个数字，使得它的平方和等于 num 。则返回 true 。

```ts
function isPerfectSquare(num: number): boolean {
  let left = 1;
  let right = num;
  let mid: number;
  // 当 left > right 时，说明 num 不是完全平方数
  while (left <= right) {
    const mid = left + ((right - left) >> 1);
    if (mid * mid === num) {
      return true;
    } else if (mid * mid < num) {
      left = mid + 1;
    } else {
      right = mid - 1;
    }
  }

  return false;
}
```
