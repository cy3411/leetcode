/*
 * @lc app=leetcode.cn id=367 lang=typescript
 *
 * [367] 有效的完全平方数
 */

// @lc code=start
function isPerfectSquare(num: number): boolean {
  let left = 1;
  let right = num;
  let mid: number;
  // 当 left > right 时，说明 num 不是完全平方数
  while (left <= right) {
    const mid = left + ((right - left) >> 1);
    if (mid * mid === num) {
      return true;
    } else if (mid * mid < num) {
      left = mid + 1;
    } else {
      right = mid - 1;
    }
  }

  return false;
}
// @lc code=end
