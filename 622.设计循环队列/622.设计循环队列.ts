/*
 * @lc app=leetcode.cn id=622 lang=typescript
 *
 * [622] 设计循环队列
 */

// @lc code=start
class MyCircularQueue {
  //   最大长度
  length: number = 0;
  //   循环队列
  pq: number[];
  //   头指针
  head: number = 0;
  //   尾指针
  tail: number = 0;
  constructor(k: number) {
    this.length = k + 1;
    this.pq = new Array(k + 1);
  }

  enQueue(value: number): boolean {
    if (this.isFull()) return false;
    this.tail = (this.tail + 1) % this.length;
    this.pq[this.tail] = value;
    return true;
  }

  deQueue(): boolean {
    if (this.isEmpty()) return false;
    this.head = (this.head + 1) % this.length;
    return true;
  }

  Front(): number {
    if (this.isEmpty()) return -1;
    return this.pq[(this.head + 1) % this.length];
  }

  Rear(): number {
    if (this.isEmpty()) return -1;
    return this.pq[(this.tail + this.length) % this.length];
  }

  isEmpty(): boolean {
    return this.head === this.tail;
  }

  isFull(): boolean {
    return this.head === (this.tail + 1) % this.length;
  }
}

/**
 * Your MyCircularQueue object will be instantiated and called as such:
 * var obj = new MyCircularQueue(k)
 * var param_1 = obj.enQueue(value)
 * var param_2 = obj.deQueue()
 * var param_3 = obj.Front()
 * var param_4 = obj.Rear()
 * var param_5 = obj.isEmpty()
 * var param_6 = obj.isFull()
 */
// @lc code=end
