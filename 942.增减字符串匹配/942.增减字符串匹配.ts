/*
 * @lc app=leetcode.cn id=942 lang=typescript
 *
 * [942] 增减字符串匹配
 */

// @lc code=start
function diStringMatch(s: string): number[] {
  const n = s.length;
  const ans: number[] = new Array(n + 1);

  let min = 0;
  let max = n;
  for (let i = 0; i < n; i++) {
    ans[i] = s[i] === 'I' ? min++ : max--;
  }
  ans[n] = max;

  return ans;
}
// @lc code=end
