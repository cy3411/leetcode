/*
 * @lc app=leetcode.cn id=942 lang=cpp
 *
 * [942] 增减字符串匹配
 */

// @lc code=start
class Solution
{
public:
    vector<int> diStringMatch(string s)
    {
        int minNum = 0, maxNum = s.size();
        vector<int> ans;
        for (auto c : s)
        {
            if (c == 'I')
            {
                ans.push_back(minNum++);
            }
            else
            {
                ans.push_back(maxNum--);
            }
        }
        ans.push_back(minNum);

        return ans;
    }
};
// @lc code=end
