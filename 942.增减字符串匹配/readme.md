# 题目

由范围 `[0,n]` 内所有整数组成的 `n + 1` 个整数的排列序列可以表示为长度为 `n` 的字符串 `s` ，其中:

- 如果 $perm[i] < perm[i + 1]$ ，那么 $s[i] \equiv 'I'$
- 如果 $perm[i] > perm[i + 1]$ ，那么 $s[i] \equiv 'D'$

给定一个字符串 s ，重构排列 perm 并返回它。如果有多个有效排列 perm，则返回其中 任何一个 。

提示：

- $1 \leq s.length \leq 10^5$
- `s` 只包含字符 `"I"` 或 `"D"`

# 示例

```
输入：s = "IDID"
输出：[0,4,1,3,2]
```

```
输入：s = "III"
输出：[0,1,2,3]
```

# 题解

## 贪心

构建一个排列，如果是'I'，那么就将最小的数字放到该位置，如果是'D'，那么就将最大的数字放到该位置。

遍历数组，重复上述过程，并同步更新最小和最大数字。

```ts
function diStringMatch(s: string): number[] {
  const n = s.length;
  const ans: number[] = new Array(n + 1);

  let min = 0;
  let max = n;
  for (let i = 0; i < n; i++) {
    ans[i] = s[i] === 'I' ? min++ : max--;
  }
  ans[n] = max;

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> diStringMatch(string s)
    {
        int minNum = 0, maxNum = s.size();
        vector<int> ans;
        for (auto c : s)
        {
            if (c == 'I')
            {
                ans.push_back(minNum++);
            }
            else
            {
                ans.push_back(maxNum--);
            }
        }
        ans.push_back(minNum);

        return ans;
    }
};
```

```python
class Solution
{
public:
    vector<int> diStringMatch(string s)
    {
        int minNum = 0, maxNum = s.size();
        vector<int> ans;
        for (auto c : s)
        {
            if (c == 'I')
            {
                ans.push_back(minNum++);
            }
            else
            {
                ans.push_back(maxNum--);
            }
        }
        ans.push_back(minNum);

        return ans;
    }
};
```
