#
# @lc app=leetcode.cn id=942 lang=python3
#
# [942] 增减字符串匹配
#

# @lc code=start
class Solution:
    def diStringMatch(self, s: str) -> List[int]:
        min = 0
        max = len(s)
        ans = []

        for c in s:
            if c == 'I':
                ans.append(min)
                min += 1
            else:
                ans.append(max)
                max -= 1

        ans.append(min)

        return ans
# @lc code=end
