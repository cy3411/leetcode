# 题目

给你一个整数数组 `nums` ，该数组具有以下属性：

- $nums.length \equiv 2 * n$
- `nums` 包含 `n + 1` 个 **不同的** 元素
- `nums` 中恰有一个元素重复 `n` 次

找出并返回重复了 `n` 次的那个元素。

提示：

- $2 \leq n \leq 5000$
- $nums.length \equiv 2 * n$
- $0 \leq nums[i] \leq 10^4$
- `nums` 由 `n + 1` 个 **不同的** 元素组成，且其中一个元素恰好重复 `n` 次

# 示例

```
输入：nums = [1,2,3,3]
输出：3
```

```
输入：nums = [2,1,2,5,3,2]
输出：2
```

# 题解

## 哈希表

利用哈希表存储每个元素是否访问过，如果访问过则返回该元素。

```ts
function repeatedNTimes(nums: number[]): number {
  const n = nums.length;
  const map = new Set<number>();

  for (let i = 0; i < n; i++) {
    if (map.has(nums[i])) {
      return nums[i];
    } else {
      map.add(nums[i]);
    }
  }

  return -1;
}
```

## 微扰理论

我们考虑重复的元素 x 在数组出现的位置。

如果每个元素之间间隔两个位置，那么数组总元素的数量至少为 $n+2(n-1)=3n-2$个，可以看出如果 n > 2 的话，那么 $3n-2 > 2n$，数组长度不满足题意。因此一定存在连续的的 x、间隔一个位置的 x、间隔两个位置的 x。

我们遍历所有的间隔，判断元素是否相等即可。

```cpp
class Solution {
public:
    int repeatedNTimes(vector<int> &nums) {
        int n = nums.size();
        for (int gap = 1; gap <= 3; gap++) {
            for (int i = 0; i + gap < n; i++) {
                if (nums[i] == nums[i + gap]) return nums[i];
            }
        }
        return -1;
    }
};
```
