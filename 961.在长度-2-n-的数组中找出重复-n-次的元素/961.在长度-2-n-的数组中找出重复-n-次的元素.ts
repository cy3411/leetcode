/*
 * @lc app=leetcode.cn id=961 lang=typescript
 *
 * [961] 在长度 2N 的数组中找出重复 N 次的元素
 */

// @lc code=start
function repeatedNTimes(nums: number[]): number {
  const n = nums.length;
  const map = new Set<number>();

  for (let i = 0; i < n; i++) {
    if (map.has(nums[i])) {
      return nums[i];
    } else {
      map.add(nums[i]);
    }
  }

  return -1;
}
// @lc code=end
