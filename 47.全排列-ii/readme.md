# 题目

给定一个可包含重复数字的序列 nums **，按任意顺序** 返回所有**不重复**的全排列

提示：

- 1 <= nums.length <= 8
- -10 <= nums[i] <= 10

# 示例

```
输入：nums = [1,1,2]
输出：
[[1,1,2],
 [1,2,1],
 [2,1,1]]
```

# 题解

## 回溯

问题跟[46.全排列](https://gitee.com/cy3411/leetcode/tree/master/46.%E5%85%A8%E6%8E%92%E5%88%97)一样，只是多了 1 个不能重复的条件。

我们可以将已经出现的组合存入哈希，每次更新答案之前去哈希中找一个，看看当前组合是否出现过。

```ts
function permuteUnique(nums: number[]): number[][] {
  const backtrack = (nums: number[], idx: number): void => {
    if (idx === nums.length) {
      // 组合是否出现过
      if (hash.has(nums.toString())) return;
      // 记录第一次出现的组合
      hash.add(nums.toString());
      ans.push([...nums]);
      return;
    }
    // 处理全组合
    for (let i = idx; i < nums.length; i++) {
      [nums[i], nums[idx]] = [nums[idx], nums[i]];
      backtrack(nums, idx + 1);
      [nums[i], nums[idx]] = [nums[idx], nums[i]];
    }
  };
  const ans: number[][] = [];
  const hash: Set<string> = new Set();
  backtrack(nums, 0);
  return ans;
}
```
