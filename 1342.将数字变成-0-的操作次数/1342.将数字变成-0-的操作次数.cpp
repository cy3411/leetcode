/*
 * @lc app=leetcode.cn id=1342 lang=cpp
 *
 * [1342] 将数字变成 0 的操作次数
 */

// @lc code=start
class Solution
{
public:
    int numberOfSteps(int num)
    {
        int ans = 0;
        while (num)
        {
            if ((num & 1) == 1)
            {
                num--;
            }
            else
            {
                num >>= 1;
            }
            ans++;
        }

        return ans;
    }
};
// @lc code=end
