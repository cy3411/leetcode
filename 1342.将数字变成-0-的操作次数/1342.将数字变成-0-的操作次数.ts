/*
 * @lc app=leetcode.cn id=1342 lang=typescript
 *
 * [1342] 将数字变成 0 的操作次数
 */

// @lc code=start
function numberOfSteps(num: number): number {
  let ans = 0;

  while (num !== 0) {
    if ((num & 1) === 0) {
      num = num >> 1;
    } else {
      num -= 1;
    }
    ans++;
  }

  return ans;
}
// @lc code=end
