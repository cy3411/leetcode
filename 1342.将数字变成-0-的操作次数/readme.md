# 题目

给你一个非负整数 `num` ，请你返回将它变成 `0` 所需要的步数。 如果当前数字是偶数，你需要把它除以 `2` ；否则，减去 `1` 。

提示：

- $\color{burlywood}0 \leq num \leq 10^6$

# 示例

```
输入：num = 14
输出：6
解释：
步骤 1) 14 是偶数，除以 2 得到 7 。
步骤 2） 7 是奇数，减 1 得到 6 。
步骤 3） 6 是偶数，除以 2 得到 3 。
步骤 4） 3 是奇数，减 1 得到 2 。
步骤 5） 2 是偶数，除以 2 得到 1 。
步骤 6） 1 是奇数，减 1 得到 0 。
```

```
输入：num = 8
输出：4
解释：
步骤 1） 8 是偶数，除以 2 得到 4 。
步骤 2） 4 是偶数，除以 2 得到 2 。
步骤 3） 2 是偶数，除以 2 得到 1 。
步骤 4） 1 是奇数，减 1 得到 0 。
```

# 题解

## 模拟

按照题意， 对 `num` 进行模拟， 如果 `num` 是偶数， 则 `num / 2`， 否则 `num - 1`， 直到 `num = 0`， 则返回步数。

```ts
function numberOfSteps(num: number): number {
  let ans = 0;

  while (num !== 0) {
    if ((num & 1) === 0) {
      num = num >> 1;
    } else {
      num -= 1;
    }
    ans++;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int numberOfSteps(int num)
    {
        int ans = 0;
        while (num)
        {
            if ((num & 1) == 1)
            {
                num--;
            }
            else
            {
                num >>= 1;
            }
            ans++;
        }

        return ans;
    }
};
```
