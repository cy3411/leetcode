# 题目

给你一棵二叉树的根节点 `root` ，请你构造一个下标从 `0` 开始、大小为 `m x n` 的字符串矩阵 `res` ，用以表示树的 **格式化布局** 。构造此格式化布局矩阵需要遵循以下规则：

- 树的 **高度** 为 `height` ，矩阵的行数 `m` 应该等于 `height + 1` 。
- 矩阵的列数 `n` 应该等于 $2^{height+1} - 1$ 。
- 根节点 需要放置在 **顶行** 的 **正中间** ，对应位置为 $res[0][(n-1)/2]$ 。
- 对于放置在矩阵中的每个节点，设对应位置为 $res[r][c]$ ，将其左子节点放置在 $res[r+1][c-2height-r-1]$ ，右子节点放置在 $res[r+1][c+2height-r-1]$ 。
- 继续这一过程，直到树中的所有节点都妥善放置。
- 任意空单元格都应该包含空字符串 `""` 。

返回构造得到的矩阵 res 。

提示：

- 树中节点数在范围 $[1, 2^{10}]$ 内
- $-99 \leq Node.val \leq 99$
- 树的深度在范围 $[1, 10]$ 内

# 示例

```
输入：root = [1,2]
输出：
[["","1",""],
 ["2","",""]]
```

# 题解

## 深度优先搜索

深度优先搜索得到树的高度 depth (从 0 开始) ， 按照题意创建一个行数为$depth+1$，列数为$(1 << (depth + 1)) - 1$的矩阵。然后递归将每个值放在矩阵对应的位置上。

```js
function printTree(root: TreeNode | null): string[][] {
  const calcDepth = (node: TreeNode | null): number => {
    if (node === null) return 0;
    return Math.max(calcDepth(node.left), calcDepth(node.right)) + 1;
  };

  const dfs = (
    ans: string[][],
    node: TreeNode | null,
    depth: number,
    r: number,
    c: number
  ): void => {
    ans[r][c] = node.val.toString();
    if (node.left) {
      dfs(ans, node.left, depth, r + 1, c - (1 << (depth - r - 1)));
    }
    if (node.right) {
      dfs(ans, node.right, depth, r + 1, c + (1 << (depth - r - 1)));
    }
  };

  // 高度从0开始
  const depth = calcDepth(root) - 1;
  const m = depth + 1;
  const n = (1 << m) - 1;
  const ans = new Array(m).fill(0).map(() => new Array(n).fill(''));
  dfs(ans, root, depth, 0, (n - 1) >> 1);

  return ans;
}
```
