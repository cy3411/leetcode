/*
 * @lc app=leetcode.cn id=1002 lang=javascript
 *
 * [1002] 查找常用字符
 */

// @lc code=start
/**
 * @param {string[]} A
 * @return {string[]}
 */
var commonChars = function (A) {
  const res = []
  const size = 26
  const minFreg = new Array(size).fill(Number.MAX_SAFE_INTEGER)

  for (let word of A) {
    const freg = new Array(size).fill(0)
    for (let letter of word) {
      freg[letter.charCodeAt() - 97]++
    }
    for (let i = 0; i < size; i++) {
      minFreg[i] = Math.min(minFreg[i], freg[i])
    }
  }
  for (let i = 0; i < size; i++) {
    for (j = 0; j < minFreg[i]; j++) {
      res.push(String.fromCharCode(i + 97))
    }
  }
  return res
};
// @lc code=end

