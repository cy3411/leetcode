# 题目

基因序列可以表示为一条由 8 个字符组成的字符串，其中每个字符都是 `'A'`、`'C'`、`'G'` 和 `'T'` 之一。

假设我们需要调查从基因序列 `start` 变为 `end` 所发生的基因变化。一次基因变化就意味着这个基因序列中的一个字符发生了变化。

例如，`"AACCGGTT" --> "AACCGGTA"` 就是一次基因变化。
另有一个基因库 `bank` 记录了所有有效的基因变化，只有基因库中的基因才是有效的基因序列。

给你两个基因序列 `start` 和 `end` ，以及一个基因库 `bank` ，请你找出并返回能够使 `start` 变化为 `end` 所需的最少变化次数。如果无法完成此基因变化，返回 `-1` 。

注意：起始基因序列 `start` 默认是有效的，但是它并不一定会出现在基因库中。

提示：

- $start.length \equiv 8$
- $end.length \equiv 8$
- $0 \leq bank.length \leq 10$
- $bank[i].length \equiv 8$
- `start`、`end` 和 `bank[i]` 仅由字符 `['A', 'C', 'G', 'T']` 组成

# 示例

```
输入：start = "AACCGGTT", end = "AACCGGTA", bank = ["AACCGGTA"]
输出：1
```

```
输入：start = "AACCGGTT", end = "AAACGGTA", bank = ["AACCGGTA","AACCGCTA","AAACGGTA"]
输出：2
```

# 题解

## 广度优先搜索

最少变化次数是从起始基因序列到结束基因序列的最短路径长度，可以用广度优先搜索来解决。

需要注意的是下一个状态的需要保证基因状态在基因库中。

```ts
interface UnitData {
  step: number;
  dna: string;
}

function minMutation(start: string, end: string, bank: string[]): number {
  const keys: string[] = ['A', 'C', 'G', 'T'];
  //   基因库
  const bankSet = new Set<string>(bank);
  //   已经访问过的基因
  const visited = new Set<string>();

  // 如果首尾相等，则返回0
  if (start === end) return 0;
  //   基因库中没有end，则返回-1
  if (!bankSet.has(end)) return -1;

  const queue: UnitData[] = [{ dna: start, step: 0 }];
  visited.add(start);
  while (queue.length) {
    const { dna, step } = queue.shift();
    // 已经找到end，返回步数
    if (dna === end) return step;
    for (let i = 0; i < 8; i++) {
      for (let k = 0; k < 4; k++) {
        if (dna[i] === keys[k]) continue;
        const str = [...dna];
        str[i] = keys[k];
        const next = str.join('');
        if (visited.has(next)) continue;
        if (!bankSet.has(next)) continue;
        queue.push({ dna: next, step: step + 1 });
        visited.add(next);
      }
    }
  }
  return -1;
}
```

```cpp
class Solution
{
public:
    typedef pair<string, int> PII;

    int minMutation(string start, string end, vector<string> &bank)
    {
        unordered_set<string> bank_set(bank.begin(), bank.end());

        if (start == end)
            return 0;
        if (!bank_set.count(end))
            return -1;

        char keys[] = {'A', 'C', 'G', 'T'};
        unordered_set<string> visited;
        queue<PII> que({PII(start, 0)});
        visited.insert(start);

        while (!que.empty())
        {
            PII curr = que.front();
            que.pop();
            if (curr.first == end)
                return curr.second;
            for (int i = 0; i < 8; i++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (curr.first[i] == keys[k])
                        continue;
                    string next = curr.first;
                    next[i] = keys[k];
                    if (bank_set.count(next) == 0)
                        continue;
                    if (visited.count(next))
                        continue;
                    que.push(PII(next, curr.second + 1));
                    visited.insert(next);
                }
            }
        }

        return -1;
    }
};
```

```py
class Solution:
    def minMutation(self, start: str, end: str, bank: List[str]) -> int:
        if start == end:
            return 0

        bankSet: Set[str] = set(bank)

        if end not in bankSet:
            return -1

        keys = ['A', 'C', 'G', 'T']
        visited: Set[str] = set()
        que: list[dict(str, int)] = [{'dna': start, 'step': 0}]
        visited.add(start)

        while (len(que)):
            curr = que.pop(0)
            dna = curr['dna']
            step = curr['step']

            if dna == end:
                return step

            for i in range(len(dna)):
                for k in range(len(keys)):
                    if (dna[i] == keys[k]):
                        continue
                    next = dna[:i] + keys[k] + dna[i + 1:]
                    if (next in visited or next not in bankSet):
                        continue
                    que.append({'dna': next, 'step': step + 1})
                    visited.add(next)
        return -1
```
