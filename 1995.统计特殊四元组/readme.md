# 题目

给你一个 下标从 0 开始 的整数数组 `nums` ，返回满足下述条件的 **不同四元组** `(a, b, c, d)` 的 数目 ：

- `nums[a] + nums[b] + nums[c] == nums[d]` ，且
- `a < b < c < d`

提示：

- $\color{burlywood}4 \leq nums.length \leq 50$
- $\color{burlywood}1 \leq nums[i] \leq 100$

# 示例

```
输入：nums = [1,2,3,6]
输出：1
解释：满足要求的唯一一个四元组是 (0, 1, 2, 3) 因为 1 + 2 + 3 == 6 。
```

```
输入：nums = [3,3,6,4,5]
输出：0
解释：[3,3,6,4,5] 中不存在满足要求的四元组。
```

# 题解

## 暴力

暴力枚举 a,b,c,d 位置。

```cpp
class Solution
{
public:
    int countQuadruplets(vector<int> &nums)
    {
        int ans = 0;
        int n = nums.size();
        for (int a = 0; a < n; a++)
        {
            for (int b = a + 1; b < n; b++)
            {
                for (int c = b + 1; c < n; c++)
                {
                    for (int d = c + 1; d < n; d++)
                    {
                        if (nums[a] + nums[b] + nums[c] == nums[d])
                        {
                            ans++;
                        }
                    }
                }
            }
        }
        return ans;
    }
};
```

## 枚举

我们改变一下等式：

$$nums[a] + nums[b] = nums[d] - nums[c]$$

逆序枚举 b,将 d-c 同值的数量存入哈希表中，继续枚举 a , 同时在哈希表中找 a + b 的 key， 将数量放入答案。

```ts
function countQuadruplets(nums: number[]): number {
  let n = nums.length;
  let ans = 0;
  const hash = new Map<number, number>();

  // 枚举b
  for (let b = n - 3; b >= 1; b--) {
    for (let d = b + 2; d < n; d++) {
      // a+b=d-c，这里记录d-c的差值
      const diff = nums[d] - nums[b + 1];
      hash.set(diff, (hash.get(diff) ?? 0) + 1);
    }

    // 枚举a
    for (let a = 0; a < b; a++) {
      // 这里枚举 a+b ， 看看哈希表中的数量
      const sum = nums[a] + nums[b];
      if (hash.has(sum)) {
        ans += hash.get(sum);
      }
    }
  }

  return ans;
}
```
