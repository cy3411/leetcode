/*
 * @lc app=leetcode.cn id=1995 lang=typescript
 *
 * [1995] 统计特殊四元组
 */

// @lc code=start
function countQuadruplets(nums: number[]): number {
  let n = nums.length;
  let ans = 0;
  const hash = new Map<number, number>();

  // 枚举b
  for (let b = n - 3; b >= 1; b--) {
    for (let d = b + 2; d < n; d++) {
      // a+b=d-c，这里记录d-c的差值
      const diff = nums[d] - nums[b + 1];
      hash.set(diff, (hash.get(diff) ?? 0) + 1);
    }

    // 枚举a
    for (let a = 0; a < b; a++) {
      // 这里枚举 a+b ， 看看哈希表中的数量
      const sum = nums[a] + nums[b];
      if (hash.has(sum)) {
        ans += hash.get(sum);
      }
    }
  }

  return ans;
}
// @lc code=end
