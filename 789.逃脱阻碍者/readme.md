# 题目

你在进行一个简化版的吃豆人游戏。你从 `[0, 0]` 点开始出发，你的目的地是 `target = [xtarget, ytarget]` 。地图上有一些阻碍者，以数组 `ghosts` 给出，第 `i` 个阻碍者从 `ghosts[i] = [xi, yi]` 出发。所有输入均为 **整数坐标** 。

每一回合，你和阻碍者们可以同时向东，西，南，北四个方向移动，每次可以移动到距离原位置 **1 个单位** 的新位置。当然，也可以选择 **不动** 。所有动作 **同时** 发生。

如果你可以在任何阻碍者抓住你 **之前** 到达目的地（阻碍者可以采取任意行动方式），则被视为逃脱成功。如果你和阻碍者同时到达了一个位置（包括目的地）都不算是逃脱成功。

只有在你有可能成功逃脱时，输出 `true` ；否则，输出 `false` 。

提示：

- `1 <= ghosts.length <= 100`
- `ghosts[i].length == 2`
- `-104 <= xi, yi <= 104`
- 同一位置可能有 **多个阻碍者** 。
- `target.length == 2`
- `-104 <= xtarget, ytarget <= 104`

# 示例

```
输入：ghosts = [[1,0],[0,3]], target = [0,1]
输出：true
解释：你可以直接一步到达目的地 (0,1) ，在 (1, 0) 或者 (0, 3) 位置的阻碍者都不可能抓住你。
```

```
输入：ghosts = [[1,0]], target = [2,0]
输出：false
解释：你需要走到位于 (2, 0) 的目的地，但是在 (1, 0) 的阻碍者位于你和目的地之间。
```

# 题解

## 曼哈顿距离

由于每个点只能上下左右移动，所以到底目的地的最短路径就是曼哈顿距离：

$$
dist(x,y) = |x_a-x_b|+|y_a-y_b|
$$

只要玩家到达 target 的曼哈顿距离比所有拦截者都小，就可以安全到达。

```ts
// 计算2个点之间的曼哈顿距离，也就是最短路径
function manhattanDist(point1: number[], point2: number[]): number {
  return Math.abs(point1[0] - point2[0]) + Math.abs(point1[1] - point2[1]);
}

function escapeGhosts(ghosts: number[][], target: number[]): boolean {
  const start = [0, 0];
  const dist = manhattanDist(start, target);
  for (let ghost of ghosts) {
    const ghostDist = manhattanDist(ghost, target);
    // 如果拦截点与target的最短距离小于目标与taget的最短距离，那么就会被拦截
    if (ghostDist <= dist) return false;
  }
  return true;
}
```
