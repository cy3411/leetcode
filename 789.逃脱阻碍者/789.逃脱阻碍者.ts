/*
 * @lc app=leetcode.cn id=789 lang=typescript
 *
 * [789] 逃脱阻碍者
 */

// @lc code=start

// 计算2个点之间的曼哈顿距离，也就是最短路径
function manhattanDist(point1: number[], point2: number[]): number {
  return Math.abs(point1[0] - point2[0]) + Math.abs(point1[1] - point2[1]);
}

function escapeGhosts(ghosts: number[][], target: number[]): boolean {
  const start = [0, 0];
  const dist = manhattanDist(start, target);
  for (let ghost of ghosts) {
    const ghostDist = manhattanDist(ghost, target);
    // 如果拦截点与target的最短距离小于目标与taget的最短距离，那么就会被拦截
    if (ghostDist <= dist) return false;
  }
  return true;
}
// @lc code=end
