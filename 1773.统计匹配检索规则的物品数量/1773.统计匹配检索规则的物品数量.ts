/*
 * @lc app=leetcode.cn id=1773 lang=typescript
 *
 * [1773] 统计匹配检索规则的物品数量
 */

// @lc code=start
function countMatches(items: string[][], ruleKey: string, ruleValue: string): number {
  const n = items.length;
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (ruleKey === 'type' && ruleValue === items[i][0]) {
      ans++;
    } else if (ruleKey === 'color' && ruleValue === items[i][1]) {
      ans++;
    } else if (ruleKey === 'name' && ruleValue === items[i][2]) {
      ans++;
    }
  }
  return ans;
}
// @lc code=end
