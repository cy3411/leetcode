# 题目

给你一个数组 items ，其中 $items[i] = [type_i, color_i, name_i]$ ，描述第 i 件物品的类型、颜色以及名称。

另给你一条由两个字符串 ruleKey 和 ruleValue 表示的检索规则。

如果第 i 件物品能满足下述条件之一，则认为该物品与给定的检索规则 匹配 ：

- $ruleKey \equiv "type"$ 且 $ruleValue \equiv type_i$ 。
- $ruleKey \equiv "color"$ 且 $ruleValue \equiv color_i$ 。
- $ruleKey \equiv "name"$ 且 $ruleValue \equiv name_i$ 。

统计并返回 匹配检索规则的物品数量 。

提示：

- $1 \leq items.length \leq 10^4$
- $1 \leq typei.length, colori.length, namei.length, ruleValue.length \leq 10$
- ruleKey 等于 "type"、"color" 或 "name"
- 所有字符串仅由小写字母组成

# 示例

```
输入：items = [["phone","blue","pixel"],["computer","silver","lenovo"],["phone","gold","iphone"]], ruleKey = "color", ruleValue = "silver"
输出：1
解释：只有一件物品匹配检索规则，这件物品是 ["computer","silver","lenovo"] 。
```

```
输入：items = [["phone","blue","pixel"],["computer","silver","phone"],["phone","gold","iphone"]], ruleKey = "type", ruleValue = "phone"
输出：2
解释：只有两件物品匹配检索规则，这两件物品分别是 ["phone","blue","pixel"] 和 ["phone","gold","iphone"] 。注意，["computer","silver","phone"] 未匹配检索规则。
```

# 题解

## 遍历

遍历 items，统计符合条件的数对数目。

```js
function countMatches(items: string[][], ruleKey: string, ruleValue: string): number {
  const n = items.length;
  let ans = 0;
  for (let i = 0; i < n; i++) {
    if (ruleKey === 'type' && ruleValue === items[i][0]) {
      ans++;
    } else if (ruleKey === 'color' && ruleValue === items[i][1]) {
      ans++;
    } else if (ruleKey === 'name' && ruleValue === items[i][2]) {
      ans++;
    }
  }
  return ans;
}
```

```py
class Solution:
    def countMatches(self, items: List[List[str]], ruleKey: str, ruleValue: str) -> int:
        key = {'type': 0, 'color': 1, 'name': 2}[ruleKey]
        return sum(item[key] == ruleValue for item in items)
```

```cpp
class Solution {
public:
    int countMatches(vector<vector<string>> &items, string ruleKey,
                     string ruleValue) {
        unordered_map<string, int> dict = {
            {"type", 0}, {"color", 1}, {"name", 2}};

        int ans = 0, key = dict[ruleKey];

        for (auto &item : items) {
            if (item[key] == ruleValue) {
                ans++;
            }
        }

        return ans;
    }
};
```
