/*
 * @lc app=leetcode.cn id=1574 lang=typescript
 *
 * [1574] 删除最短的子数组使剩余数组有序
 */

// @lc code=start
function findLengthOfShortestSubarray(arr: number[]): number {
  let n = arr.length;
  let p = n - 1;
  // 右边非递减子树组的起始位置
  while (p > 0 && arr[p - 1] <= arr[p]) p--;
  if (p === 0) return 0;

  // 初始化答案
  let ans = p;
  // i 的位置是左边非递减序列的结尾位置
  for (let i = 0; i < n; i++) {
    // 遇到递增序列，直接结束循环
    if (i > 0 && arr[i] < arr[i - 1]) break;
    // 调整右边界的位置
    while (p <= i || (p < n && arr[p] < arr[i])) p++;
    ans = Math.min(ans, p - i - 1);
  }
  return ans;
}
// @lc code=end
