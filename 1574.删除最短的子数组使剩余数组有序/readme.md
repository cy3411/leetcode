# 题目

给你一个整数数组 `arr` ，请你删除一个子数组（可以为空），使得 `arr` 中剩下的元素是 **非递减** 的。

一个子数组指的是原数组中连续的一个子序列。

请你返回满足题目要求的最短子数组的长度。

提示：

- $1 \leq arr.length \leq 10^5$
- $0 \leq arr[i] \leq 10^9$

# 示例

```
输入：arr = [1,2,3,10,4,2,3,5]
输出：3
解释：我们需要删除的最短子数组是 [10,4,2] ，长度为 3 。剩余元素形成非递减数组 [1,2,3,3,5] 。
另一个正确的解为删除子数组 [3,10,4] 。
```

```
输入：arr = [5,4,3,2,1]
输出：4
解释：由于数组是严格递减的，我们只能保留一个元素。所以我们需要删除长度为 4 的子数组，要么删除 [5,4,3,2]，要么删除 [4,3,2,1]。
```

# 题解

## 枚举

预处理最右边的非递减序列，找到起始位置。

然后枚举数组中的每个元素，检查当前元素是否是左边非递减序列的最后一个元素，如果是，则更新最短子数组的长度。

```ts
function findLengthOfShortestSubarray(arr: number[]): number {
  let n = arr.length;
  let p = n - 1;
  // 右边非递减子树组的起始位置
  while (p > 0 && arr[p - 1] <= arr[p]) p--;
  if (p === 0) return 0;

  // 初始化答案
  let ans = p;
  // i 的位置是左边非递减序列的结尾位置
  for (let i = 0; i < n; i++) {
    // 遇到递增序列，直接结束循环
    if (i > 0 && arr[i] < arr[i - 1]) break;
    // 调整右边界的位置
    while (p <= i || (p < n && arr[p] < arr[i])) p++;
    ans = Math.min(ans, p - i - 1);
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int findLengthOfShortestSubarray(vector<int> &arr)
    {
        int n = arr.size(), p = n - 1;
        while (p > 0 && arr[p - 1] <= arr[p])
            p--;
        if (p == 0)
            return 0;

        int ans = p;
        for (int i = 0; i < n; i++)
        {
            if (i && arr[i - 1] > arr[i])
                break;

            while (p <= i || (p < n && arr[p] < arr[i]))
                p++;
            ans = min(ans, p - i - 1);
        }

        return ans;
    }
};
```
