/*
 * @lc app=leetcode.cn id=628 lang=javascript
 *
 * [628] 三个数的最大乘积
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var maximumProduct = function (nums) {
  let [min1, min2] = [Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER];
  let [max1, max2, max3] = [
    Number.MIN_SAFE_INTEGER,
    Number.MIN_SAFE_INTEGER,
    Number.MIN_SAFE_INTEGER,
  ];

  for (const num of nums) {
    if (num < min1) {
      [min2, min1] = [min1, num];
    } else if (num < min2) {
      min2 = num;
    }

    if (num > max1) {
      [max3, max2, max1] = [max2, max1, num];
    } else if (num > max2) {
      [max3, max2] = [max2, num];
    } else if (num > max3) {
      max3 = num;
    }
  }

  return Math.max(max1 * max2 * max3, max1 * min1 * min2);
};
// @lc code=end
