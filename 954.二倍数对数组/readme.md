# 题目

给定一个长度为偶数的整数数组 `arr`，只有对 `arr` 进行重组后可以满足 “对于每个 `0 <= i < len(arr) / 2`，都有 `arr[2 * i + 1] = 2 * arr[2 * i]`” 时，返回 `true`；否则，返回 `false`。

# 示例

```
输入：arr = [3,1,3,6]
输出：false
```

```
输入：arr = [2,1,2,6]
输出：false
```

# 题解

## 哈希表+排序

假设 arr 的长度是 n，那么题意就是能否将 arr 分成 $\frac{n}{2}$对，每对元素的第二个值是第一个值的两倍。

利用哈希表 map 来统计 arr 中每个数字的出现的次数。

对于数字 0 ，它只能和 0 匹配，那么 0 出现的次数必然是偶数才可以。

我们将去重后的数字按照数字的绝对值升序排序，这样保证当前数字 x 只能与 2x 匹配。

如果$2x数量 < x数量$，那么会有部分 x 找不到匹配，这时候就不满足题意。

否则，更新 2x 的数量，继续遍历。直到所有的数字都遍历完成。

```ts
function canReorderDoubled(arr: number[]): boolean {
  // 每个数字出现的次数
  const map = new Map<number, number>();
  // 初始化哈希表
  for (const num of arr) {
    map.set(num, (map.get(num) || 0) + 1);
  }

  // 数字0的个数需要为偶数，因为0的另一对也是0
  if ((map.get(0) & 1) === 1) {
    return false;
  }

  const keys = [...map.keys()];
  // 升序排序，利用当前数字去找2倍的数字
  keys.sort((a: number, b: number) => Math.abs(a) - Math.abs(b));
  for (const num of keys) {
    // 如果倍数数量小于当前数字，表示不能满足条件
    if ((map.get(2 * num) || 0) < map.get(num)) return false;
    // 满足条件，更新倍数的数量，减去当前数字的数量
    map.set(2 * num, (map.get(2 * num) || 0) - map.get(num));
  }

  return true;
}
```

```cpp
class Solution
{
public:
    bool canReorderDoubled(vector<int> &arr)
    {
        unordered_map<int, int> cnt;
        for (int x : arr)
        {
            cnt[x]++;
        }
        if (cnt[0] % 2)
        {

            return false;
        }

        vector<int> vals;
        for (auto &[x, _] : cnt)
        {
            vals.push_back(x);
        }
        sort(vals.begin(), vals.end(), [](int a, int b)
             { return abs(a) < abs(b); });

        for (int x : vals)
        {
            if (cnt[2 * x] < cnt[x])
            {
                return false;
            }
            cnt[2 * x] -= cnt[x];
        }
        return true;
    }
};
```
