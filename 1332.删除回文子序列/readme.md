# 题目

给你一个字符串 `s`，它仅由字母 `'a'` 和 `'b'` 组成。每一次删除操作都可以从 `s` 中删除一个回文 **子序列**。

返回删除给定字符串中所有字符（字符串为空）的最小删除次数。

「子序列」定义：如果一个字符串可以通过删除原字符串某些字符而不改变原字符顺序得到，那么这个字符串就是原字符串的一个子序列。

「回文」定义：如果一个字符串向后和向前读是一致的，那么这个字符串就是一个回文。

提示：

- $\color{burlywood}1 \leq s.length \leq 1000$
- `s` 仅包含字母 `'a'` 和 `'b'`

# 示例

```
输入：s = "ababa"
输出：1
解释：字符串本身就是回文序列，只需要删除一次。
```

```
输入：s = "abb"
输出：2
解释："abb" -> "bb" -> "".
先删除回文子序列 "a"，然后再删除 "bb"。
```

# 题解

## 模拟

题目给出只有 `a` 和 `b` 两种字母，那么可以拆成只有 `a` 或者只有 `b` 的两种子序列，如果 `s` 本身不是回文的话，那么最多只需要删除两次即可。

判断字符串是否是回文，是的话返回 `1`，不是的话返回 `2`。

```ts
function removePalindromeSub(s: string): number {
  const n = s.length;

  for (let i = 0; i <= (n / 2) >> 0; i++) {
    // s 本身不是回文，需要删除2次
    if (s[i] !== s[n - 1 - i]) {
      return 2;
    }
  }

  return 1;
}
```

```cpp
class Solution
{
public:
    int removePalindromeSub(string s)
    {
        int n = s.size();
        for (int i = 0; i <= n / 2; i++)
        {
            if (s[i] != s[n - 1 - i])
            {
                return 2;
            }
        }
        return 1;
    }
};
```
