/*
 * @lc app=leetcode.cn id=1332 lang=typescript
 *
 * [1332] 删除回文子序列
 */

// @lc code=start
function removePalindromeSub(s: string): number {
  const n = s.length;

  for (let i = 0; i <= (n / 2) >> 0; i++) {
    // s 本身不是回文，需要删除2次
    if (s[i] !== s[n - 1 - i]) {
      return 2;
    }
  }

  return 1;
}
// @lc code=end
