/*
 * @lc app=leetcode.cn id=71 lang=typescript
 *
 * [71] 简化路径
 */

// @lc code=start
function simplifyPath(path: string): string {
  const stack: string[] = [];
  const paths = path.split('/');

  for (let i = 0; i < paths.length; i++) {
    const path = paths[i];
    if (path === '.' || path === '') continue;
    if (path === '..') {
      stack.length && stack.pop();
    } else {
      stack.push(path);
    }
  }

  return '/' + stack.join('/');
}
// @lc code=end
