# 题目

给你一棵根节点为 `0` 的 二叉树 ，它总共有 `n` 个节点，节点编号为 `0` 到 `n - 1` 。同时给你一个下标从 `0` 开始的整数数组 `parents` 表示这棵树，其中 `parents[i]` 是节点 `i` 的父节点。由于节点 `0` 是根，所以 `parents[0] == -1` 。

一个子树的 **大小** 为这个子树内节点的数目。每个节点都有一个与之关联的 **分数** 。求出某个节点分数的方法是，将这个节点和与它相连的边全部 **删除** ，剩余部分是若干个 **非空** 子树，这个节点的 **分数** 为所有这些子树 **大小的乘积** 。

请你返回有 **最高得分** 节点的 数目 。

提示：

- $n \equiv parents.length$
- $2 \leq n \leq 10^5$
- $parents[0] \equiv -1$
- 对于 $i \not = 0$ ，有 $0 \leq parents[i] \leq n - 1$
- `parents` 表示一棵二叉树。

# 示例

[![b5Xr4J.png](https://s1.ax1x.com/2022/03/11/b5Xr4J.png)](https://imgtu.com/i/b5Xr4J)

```
输入：parents = [-1,2,0,2,0]
输出：3
解释：
- 节点 0 的分数为：3 * 1 = 3
- 节点 1 的分数为：4 = 4
- 节点 2 的分数为：1 * 1 * 2 = 2
- 节点 3 的分数为：4 = 4
- 节点 4 的分数为：4 = 4
最高得分为 4 ，有三个节点得分为 4 （分别是节点 1，3 和 4 ）。
```

[![b5Xgjx.png](https://s1.ax1x.com/2022/03/11/b5Xgjx.png)](https://imgtu.com/i/b5Xgjx)

```
输入：parents = [-1,2,0]
输出：2
解释：
- 节点 0 的分数为：2 = 2
- 节点 1 的分数为：2 = 2
- 节点 2 的分数为：1 * 1 = 1
最高分数为 2 ，有两个节点分数为 2 （分别为节点 0 和 1 ）。
```

# 题解

## 深度优先搜索

题目给出的是一棵二叉树结构，所以最多可以将树分为 `3` 棵子树，当前节点的左子树、右子树和父节点及父节点的另一边子树。

使用深度优先搜索，遍历每一个节点，计算其分数，并同步更新最高分数的节点数目（也就是最高分出现了几次）。

```ts
function countHighestScoreNodes(parents: number[]): number {
  const n = parents.length;
  // 记录每个节点的子节点
  const children: number[][] = new Array(n).fill(0).map(() => []);
  for (let i = 0; i < n; i++) {
    const idx = parents[i];
    if (idx === -1) continue;
    children[idx].push(i);
  }
  // dfs搜索每个节点的最高分
  const dfs = (node: number): number => {
    // 当前节点的分数
    let score = 1;
    // 默认为当前节点的数量为1
    let sum = 1;
    // 深搜子节点
    for (const child of children[node]) {
      const cnt = dfs(child);
      score *= cnt;
      sum += cnt;
    }
    // 计算当前节点父节点及父节点另一半子树
    if (node !== 0) {
      score *= n - sum;
    }
    // 计算最高分的数量
    if (maxScore === score) {
      ans++;
    } else if (score > maxScore) {
      maxScore = score;
      ans = 1;
    }
    // 返回当前节点和它的子节点的数量
    return sum;
  };

  let ans = 0;
  let maxScore = 0;

  dfs(0);
  return ans;
}
```

```cpp
class Solution
{
public:
    int n;

    long long dfs(int node, int &ans, long long &maxScore, vector<vector<int>> &children)
    {
        long long score = 1, sum = 1;
        for (auto child : children[node])
        {
            int childNum = dfs(child, ans, maxScore, children);
            score *= childNum;
            sum += childNum;
        }
        // 如果不是根节点，可以计算非左右子树的最大分数
        if (node != 0)
        {
            score *= n - sum;
        }
        // 更新最大分数
        if (score == maxScore)
        {
            ans++;
        }
        else if (score > maxScore)
        {
            maxScore = score;
            ans = 1;
        }
        return sum;
    }

    int countHighestScoreNodes(vector<int> &parents)
    {
        n = parents.size();
        vector<vector<int>> children(n, vector<int>());
        for (int i = 0; i < n; i++)
        {
            int idx = parents[i];
            if (idx == -1)
            {
                continue;
            }
            children[idx].push_back(i);
        }

        int ans = 0;
        long long maxScore = 0;
        dfs(0, ans, maxScore, children);
        return ans;
    }
};
```
