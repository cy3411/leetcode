# 题目

给你一个整数数组 `cost` 和一个整数 `target` 。请你返回满足如下规则可以得到的 **最大** 整数：

- 给当前结果添加一个数位（`i + 1`）的成本为 `cost[i]` （`cost` 数组下标从 0 开始）。
- 总成本必须恰好等于 `target` 。
- 添加的数位中没有数字 0 。

由于答案可能会很大，请你以字符串形式返回。

如果按照上述要求无法得到任何整数，请你返回 "0" 。

# 示例

```
# 题解
输入：cost = [4,3,2,5,6,7,2,5,5], target = 9
输出："7772"
解释：添加数位 '7' 的成本为 2 ，添加数位 '2' 的成本为 3 。所以 "7772" 的代价为 2*3+ 3*1 = 9 。 "977" 也是满足要求的数字，但 "7772" 是较大的数字。
 数字     成本
  1  ->   4
  2  ->   3
  3  ->   2
  4  ->   5
  5  ->   6
  6  ->   7
  7  ->   2
  8  ->   5
  9  ->   5
```

# 题解

## 动态规划 + 贪心

比较数字的大小：先看长度，长度长的比较大。同长度的情况下，比较首位，首位数大的比较大。

考虑长度问题，题目就转化成在若干的数字中，求累加和等于给定的 **taget** 值，所能选择的数字的最多数量。

**状态**

`dp[i][j]`，前 `i` 个数字中，总价值等于 `j`，所需要的最多数字数量。

**base case**

`dp[0][0]=0`，0 个数字中选择，总价值等于 0，需要的数字数量只能为 0。

`d[0][j]=Number.NEGATIVE_INFINITY`，都是非法的结果，因为需要求最大值，所以非法的结果给一个负无穷。

**选择**
只有 j 大于当前数的才可以选择当前数

$$
dp[i][j] = \begin{cases}
    dp[i-1][j] & \text{j<cost[i-1]} \\
    max(dp[i-1][j], dp[i][j-cost[i-1]+1]) & \text{j>=cost[i-1]}
\end{cases}
$$

这个时候，我可以从 cost.length 到 1 开始倒叙遍历，如果状态能够从该值转换过来，就选择该值。

```ts
function largestNumber(cost: number[], target: number): string {
  const size = cost.length;
  // 前i个元素和等于j的最大元素数量
  const dp = new Array(size + 1)
    .fill(0)
    .map(() => new Array(target + 1).fill(Number.NEGATIVE_INFINITY));
  // 前0个元素和等于0的最大数量是0，没法选择任何元素
  dp[0][0] = 0;

  for (let i = 1; i <= size; i++) {
    const c = cost[i - 1];
    for (let j = 0; j <= target; j++) {
      dp[i][j] = dp[i - 1][j];
      if (j >= c) {
        dp[i][j] = Math.max(dp[i][j], dp[i][j - c] + 1);
      }
    }
  }

  if (dp[size][target] < 0) return '0';

  let ans = '';
  let i = size;
  let j = target;

  while (i >= 1) {
    let c = cost[i - 1];
    while (j >= c && dp[i][j] === dp[i][j - c] + 1) {
      ans += String(i);
      j -= c;
    }
    i--;
  }

  return ans;
}
```

二维改一维

```ts
function largestNumber(cost: number[], target: number): string {
  const dp = new Array(target + 1).fill(Number.NEGATIVE_INFINITY);
  dp[0] = 0;
  for (const c of cost) {
    for (let j = c; j <= target; j++) {
      dp[j] = Math.max(dp[j], dp[j - c] + 1);
    }
  }

  if (dp[target] < 0) return '0';

  let ans = '';
  let i = cost.length;
  let j = target;

  while (i >= 1) {
    let c = cost[i - 1];
    while (j >= c && dp[j] === dp[j - c] + 1) {
      ans += String(i);
      j -= c;
    }
    i--;
  }

  return ans;
}
```
