/*
 * @lc app=leetcode.cn id=780 lang=typescript
 *
 * [780] 到达终点
 */

// @lc code=start
function reachingPoints(sx: number, sy: number, tx: number, ty: number): boolean {
  // 辗转相除
  while (tx > sx && ty > sy && tx !== ty) {
    if (tx > ty) tx %= ty;
    else ty %= tx;
  }

  // 如果相等，则可以直接到达
  if (tx === sx && ty === sy) return true;
  // tx等于sx,则验证ty是否能到达
  else if (tx === sx) return ty > sy && (ty - sy) % sx === 0;
  // ty等于sy,则验证tx是否能到达
  else if (ty === sy) return tx > sx && (tx - sx) % sy === 0;
  // 否则不能到达
  else return false;
}
// @lc code=end
