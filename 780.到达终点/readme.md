# 题目

给定四个整数 `sx` , `sy` ，`tx` 和 `ty`，如果通过一系列的转换可以从起点 `(sx, sy)` 到达终点 `(tx, ty)`，则返回 `true` ，否则返回 `false` 。

从点 `(x, y)` 可以转换到 `(x, x+y)` 或者 `(x+y, y)`。

提示:

- $1 \leq sx, sy, tx, ty \leq 10^9$

# 示例

```
输入: sx = 1, sy = 1, tx = 3, ty = 5
输出: true
解释:
可以通过以下一系列转换从起点转换到终点：
(1, 1) -> (1, 2)
(1, 2) -> (3, 2)
(3, 2) -> (3, 5)
```

```
输入: sx = 1, sy = 1, tx = 2, ty = 2
输出: false
```

```
输入: sx = 1, sy = 1, tx = 1, ty = 1
输出: true
```

# 题解

## 反向计算

如果从 (sx,sy) 开始计算，情况会有很多。题目给定的参数都是正整数，所以可以通过反向计算来确定是否可以从 (tx,ty) 到达 (sx, sy):

- 如果 $tx \equiv ty$，状态 `(tx, ty)` 就已经是起始状态
- 如果 $tx > ty$ ，下一个状态为 `(tx - ty, ty)`
- 如果 $tx < ty$ ，下一个状态为 `(tx, ty - tx)`

当反向计算的条件不成立的时候，可以通过 tx 和 ty 的不同情况来判断是否可以到达 (sx, sy)：

- $tx \equiv sx 且 ty \equiv sy$ ，已经到达起点状态
- $tx \equiv sx 且 ty \not ={} sy$ , `tx` 不能减少了，只能减少 `ty` 。 因此只能当 $ty > sy$ 且 $(ty - sy) \mod tx \equiv 0$ 时，才能到达 `(sx, sy)`
- $ty \equiv sy 且 tx \not ={} sx$，`ty` 不能减少了，只能减少 `tx` 。 因此只能当 $tx > sx$ 且 $(tx - sx) \mod ty \equiv 0$ 时，才能到达 `(sx, sy)`
- $tx \not ={} sx 且 ty \not ={} sy$ , 不能到达起点状态

```ts
function reachingPoints(sx: number, sy: number, tx: number, ty: number): boolean {
  // 辗转相除
  while (tx > sx && ty > sy && tx !== ty) {
    if (tx > ty) tx %= ty;
    else ty %= tx;
  }

  // 如果相等，则可以直接到达
  if (tx === sx && ty === sy) return true;
  // tx等于sx,则验证ty是否能到达
  else if (tx === sx) return ty > sy && (ty - sy) % sx === 0;
  // ty等于sy,则验证tx是否能到达
  else if (ty === sy) return tx > sx && (tx - sx) % sy === 0;
  // 否则不能到达
  else return false;
}
```

```cpp
class Solution
{
public:
    bool reachingPoints(int sx, int sy, int tx, int ty)
    {
        while (tx > sx && ty > sy && tx != ty)
        {
            if (tx > ty)
            {
                tx %= ty;
            }
            else
            {
                ty %= tx;
            }
        }

        if (tx == sx && ty == sy)
        {
            return true;
        }
        else if (tx == sx)
        {
            return ty > sy && (ty - sy) % sx == 0;
        }
        else if (ty == sy)
        {
            return tx > sx && (tx - sx) % sy == 0;
        }
        else
        {
            return false;
        }
    }
};
```
