# 题目

给定 n 个非负整数，用来表示柱状图中各个柱子的高度。每个柱子彼此相邻，且宽度为 1 。

求在该柱状图中，能够勾勒出来的矩形的最大面积。

[![WaLbWR.png](https://z3.ax1x.com/2021/07/21/WaLbWR.png)](https://imgtu.com/i/WaLbWR)

以上是柱状图的示例，其中每个柱子的宽度为 1，给定的高度为 [2,1,5,6,2,3]。

[![WaOZm8.png](https://z3.ax1x.com/2021/07/21/WaOZm8.png)](https://imgtu.com/i/WaOZm8)

图中阴影部分为所能勾勒出的最大矩形面积，其面积为 10 个单位。

# 示例

```
输入: [2,1,5,6,2,3]
输出: 10
```

# 题解

## 单调栈

计算当前高度所能形成的矩形面积，我们需要找到当前高度可以形成宽度是多少。

如图所示，我们令 l 为左边第一个小于当前高度的位置，令 r 为右边第一个小于当前高度的位置，那么宽度就是 (r-l-1)。我们就可以用当前高度乘上宽度就能得到当前位置的矩形面积。

遍历数组，统计每一个高度所能形成的矩形面积，取最大值即可。

我们利用单调递增栈的特性，使用 l 和 r 分别保存左边最近和右边最近小于当前高度的位置。

```ts
function largestRectangleArea(heights: number[]): number {
  const m = heights.length;
  // 左边第一个比i元素小的位置
  const l = new Array(m).fill(-1);
  // 右边第一个比i元素小位置
  const r = new Array(m).fill(m);
  // 单调递增栈
  const stack: number[] = [];
  for (let i = 0; i < m; i++) {
    while (stack.length && heights[i] < heights[stack[stack.length - 1]]) {
      r[stack[stack.length - 1]] = i;
      stack.pop();
    }
    if (stack.length) l[i] = stack[stack.length - 1];
    stack.push(i);
  }

  let ans = 0;
  for (let i = 0; i < m; i++) {
    // 计算面积，高度*跨度
    ans = Math.max(ans, heights[i] * (r[i] - l[i] - 1));
  }

  return ans;
}
```
