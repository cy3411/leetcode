/*
 * @lc app=leetcode.cn id=1833 lang=typescript
 *
 * [1833] 雪糕的最大数量
 */

// @lc code=start
function maxIceCream(costs: number[], coins: number): number {
  const maxNumber = 10 ** 5;
  // 计数数组
  const freg: number[] = new Array(maxNumber + 1).fill(0);
  for (let cost of costs) {
    freg[cost]++;
  }

  let ans = 0;
  for (let i = 0; i <= maxNumber; i++) {
    // 选择当前价值的雪糕数量
    // 有可能数量的总价值会超过coins，所以这里有最小值处理
    // costs=[5,5,5],coins=10。避免这种情况。
    let count = Math.min(freg[i], (coins / i) >> 0);
    ans += count;
    coins -= i * count;
  }

  return ans;
}
// @lc code=end
