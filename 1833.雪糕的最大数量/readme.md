# 题目

夏日炎炎，小男孩 Tony 想买一些雪糕消消暑。

商店中新到 n 支雪糕，用长度为 n 的数组 costs 表示雪糕的定价，其中 costs[i] 表示第 i 支雪糕的现金价格。Tony 一共有 coins 现金可以用于消费，他想要买尽可能多的雪糕。

给你价格数组 costs 和现金量 coins ，请你计算并返回 Tony 用 coins 现金能够买到的雪糕的 最大数量 。

注意：Tony 可以按任意顺序购买雪糕。

提示：

- costs.length == n
- 1 <= n <= 105
- 1 <= costs[i] <= 105
- 1 <= coins <= 108

# 示例

```
输入：costs = [1,3,2,4,1], coins = 7
输出：4
解释：Tony 可以买下标为 0、1、2、4 的雪糕，总价为 1 + 3 + 2 + 1 = 7
```

# 题解

## 排序+贪心

题意是可以按照任意顺序，购买最多的雪糕数量。

如果可以购买更多，那 cost 就要选择少的。

考虑将 costs 升序排序，遍历 costs，每次将 coins 减去当前 cost，当 coins 不够买后面的雪糕就直接退出循环。

```ts
function maxIceCream(costs: number[], coins: number): number {
  // 归并排序
  const mergeSort = (costs: number[], l: number, r: number): void => {
    if (l >= r) return;

    let mid = l + ((r - l) >> 1);

    mergeSort(costs, l, mid);
    mergeSort(costs, mid + 1, r);

    let temp: number[] = new Array(r - l + 1).fill(0);
    let i = l;
    let j = mid + 1;
    let idx = 0;
    while (i <= mid || j <= r) {
      if (j > r || (i <= mid && costs[i] < costs[j])) {
        temp[idx++] = costs[i++];
      } else {
        temp[idx++] = costs[j++];
      }
    }
    for (let i = 0; i < temp.length; i++) {
      costs[l + i] = temp[i];
    }
  };

  mergeSort(costs, 0, costs.length - 1);

  let ans = 0;
  for (let cost of costs) {
    if (coins >= cost) {
      ans++;
      coins -= cost;
    }
  }

  return ans;
}
```

## 计数排序+贪心

题目给出的 cost 值最多只有 100000，可以考虑使用计数排序来统计个数。

```ts
function maxIceCream(costs: number[], coins: number): number {
  const maxNumber = 10 ** 5;
  // 计数数组
  const freg: number[] = new Array(maxNumber + 1).fill(0);
  for (let cost of costs) {
    freg[cost]++;
  }

  let ans = 0;
  for (let i = 0; i <= maxNumber; i++) {
    // 选择当前价值的雪糕数量
    // 有可能数量的总价值会超过coins，所以这里有最小值处理
    // costs=[5,5,5],coins=10。避免这种情况。
    let count = Math.min(freg[i], (coins / i) >> 0);
    ans += count;
    coins -= i * count;
  }

  return ans;
}
```
