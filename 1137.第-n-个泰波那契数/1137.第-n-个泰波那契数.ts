/*
 * @lc app=leetcode.cn id=1137 lang=typescript
 *
 * [1137] 第 N 个泰波那契数
 */

// @lc code=start
function tribonacci(n: number): number {
  if (n === 0) return 0;
  if (n < 3) return 1;
  let a = 0;
  let b = 1;
  let c = 1;
  for (let i = 3; i <= n; i++) {
    let d = a + b + c;
    a = b;
    b = c;
    c = d;
  }

  return c;
}
// @lc code=end
