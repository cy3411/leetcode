# 题目

泰波那契序列 Tn 定义如下：

T0 = 0, T1 = 1, T2 = 1, 且在 n >= 0 的条件下 Tn+3 = Tn + Tn+1 + Tn+2

给你整数 n，请返回第 n 个泰波那契数 Tn 的值。

# 示例

```
输入：n = 4
输出：4
解释：
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4
```

# 题解

## 滚动数组

```ts
function tribonacci(n: number): number {
  if (n === 0) return 0;
  if (n < 3) return 1;
  let a = 0;
  let b = 1;
  let c = 1;
  for (let i = 3; i <= n; i++) {
    let d = a + b + c;
    a = b;
    b = c;
    c = d;
  }

  return c;
}
```
