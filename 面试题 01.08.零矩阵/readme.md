# 题目

编写一种算法，若 M × N 矩阵中某个元素为 0，则将其所在的行与列清零。

# 示例

```
输入：
[
  [1,1,1],
  [1,0,1],
  [1,1,1]
]
输出：
[
  [1,0,1],
  [0,0,0],
  [1,0,1]
]
```

```
输入：
[
  [0,1,2,0],
  [3,4,5,2],
  [1,3,1,5]
]
输出：
[
  [0,0,0,0],
  [0,4,5,0],
  [0,3,1,0]
]
```

# 题解

## 标记数组

我们使用标记数组 row 和 col 来记录每一行和每一列是否有 0 出现。

最后遍历矩阵，如果当前元素的行列出现过 0 ，就将当前元素清零。

```js
function setZeroes(matrix: number[][]): void {
  const m = matrix.length;
  const n = matrix[0].length;
  const row = new Array(m).fill(0);
  const col = new Array(n).fill(0);

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (matrix[i][j] === 0) {
        (row[i] = 1), (col[j] = 1);
      }
    }
  }

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (row[i] || col[j]) {
        matrix[i][j] = 0;
      }
    }
  }
}
```
