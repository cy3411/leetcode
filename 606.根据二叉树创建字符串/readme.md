# 题目

你需要采用前序遍历的方式，将一个二叉树转换成一个由括号和整数组成的字符串。

空节点则用一对空括号 "()" 表示。而且你需要省略所有不影响字符串与原始二叉树之间的一对一映射关系的空括号对。

# 示例

```
输入: 二叉树: [1,2,3,4]
       1
     /   \
    2     3
   /
  4

输出: "1(2(4))(3)"

解释: 原本将是“1(2(4)())(3())”，
在你省略所有不必要的空括号对之后，
它将是“1(2(4))(3)”。
```

```
输入: 二叉树: [1,2,3,null,4]
       1
     /   \
    2     3
     \
      4

输出: "1(2()(4))(3)"

解释: 和第一个示例相似，
除了我们不能省略第一个对括号来中断输入和输出之间的一对一映射关系。
```

# 题解

## 二叉树的前序遍历

前序遍历二叉树，按序输出二叉树的节点值。需要注意一下：

- 结点有右子树，左子树为空的话，需要输出一对空括号。

```ts
function tree2str(root: TreeNode | null): string {
  let ans = '';
  const _preOrder = (node: TreeNode | null) => {
    if (node === null) return;
    ans += node.val;
    // 如果有右子树，左子树的括号不能省略
    if (node.left !== null || node.right !== null) {
      ans += '(';
      _preOrder(node.left);
      ans += ')';
    }
    if (node.right !== null) {
      ans += '(';
      _preOrder(node.right);
      ans += ')';
    }
  };
  _preOrder(root);
  return ans;
}
```

```cpp
class Solution
{
public:
    void preorder(TreeNode *root, string &s)
    {
        if (root == nullptr)
        {
            return;
        }
        s += to_string(root->val);
        if (root->left != nullptr || root->right != nullptr)
        {
            s += "(";
            preorder(root->left, s);
            s += ")";
        }
        if (root->right != nullptr)
        {
            s += "(";
            preorder(root->right, s);
            s += ")";
        }
    }
    string tree2str(TreeNode *root)
    {
        string ans;
        preorder(root, ans);
        return ans;
    }
};
```
