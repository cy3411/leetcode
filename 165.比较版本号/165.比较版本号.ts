/*
 * @lc app=leetcode.cn id=165 lang=typescript
 *
 * [165] 比较版本号
 */

// @lc code=start
function compareVersion(version1: string, version2: string): number {
  const strToNumber = (str: string): number => {
    let res = 0;
    for (let i = 0; i < str.length; i++) {
      res = res * 10 + Number(str[i]);
    }

    return res;
  };

  const v1 = version1.split('.');
  const v2 = version2.split('.');
  const m = v1.length;
  const n = v2.length;
  const k = Math.max(m, n);
  // 将短的版本号后面补0
  for (let i = 0; i < Math.abs(m - n); i++) {
    if (m > n) v2.push('0');
    else v1.push('0');
  }
  // 遍历比较每个修订号
  for (let i = 0; i < k; i++) {
    const n1 = strToNumber(v1[i]);
    const n2 = strToNumber(v2[i]);
    if (n1 > n2) return 1;
    else if (n1 < n2) return -1;
  }

  return 0;
}
// @lc code=end
