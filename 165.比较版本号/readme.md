# 题目

给你两个版本号 `version1` 和 `version2` ，请你比较它们。

版本号由一个或多个修订号组成，各修订号由一个 `'.'` 连接。每个修订号由 **多位数字** 组成，可能包含 **前导零** 。每个版本号至少包含一个字符。修订号从左到右编号，下标从 0 开始，最左边的修订号下标为 0 ，下一个修订号下标为 1 ，以此类推。例如，`2.5.33` 和 `0.1` 都是有效的版本号。

比较版本号时，请按从左到右的顺序依次比较它们的修订号。比较修订号时，只需比较 **忽略任何前导零后的整数值** 。也就是说，修订号 `1` 和修订号 `001` 相等 。如果版本号没有指定某个下标处的修订号，则该修订号视为 `0` 。例如，版本 `1.0` 小于版本 1.1 ，因为它们下标为 `0` 的修订号相同，而下标为 `1` 的修订号分别为 `0` 和 `1` ，`0 < 1` 。

返回规则如下：

- 如果 `version1 > version2` 返回 `1`，
- 如果 `version1 < version2` 返回 `-1`，
- 除此之外返回 `0`。

提示：

- `1 <= version1.length, version2.length <= 500`
- `version1` 和 `version2` 仅包含数字和 `'.'`
- `version1` 和 `version2` 都是 **有效版本号**
- `version1` 和 `version2` 的所有修订号都可以存储在 **32 位整数** 中

# 示例

```
输入：version1 = "1.01", version2 = "1.001"
输出：0
解释：忽略前导零，"01" 和 "001" 都表示相同的整数 "1"
```

```
输入：version1 = "1.0", version2 = "1.0.0"
输出：0
解释：version1 没有指定下标为 2 的修订号，即视为 "0"
```

# 题解

## 模拟

遍历每个修订号,相互比较,按照题目返回答案.

提前可以将短的版本号后面补`'0'`,这样可以减少边界条件的处理,方便比较.

```ts
function compareVersion(version1: string, version2: string): number {
  const strToNumber = (str: string): number => {
    let res = 0;
    for (let i = 0; i < str.length; i++) {
      res = res * 10 + Number(str[i]);
    }

    return res;
  };

  const v1 = version1.split('.');
  const v2 = version2.split('.');
  const m = v1.length;
  const n = v2.length;
  const k = Math.max(m, n);
  // 将短的版本号后面补0
  for (let i = 0; i < Math.abs(m - n); i++) {
    if (m > n) v2.push('0');
    else v1.push('0');
  }
  // 遍历比较每个修订号
  for (let i = 0; i < k; i++) {
    const n1 = strToNumber(v1[i]);
    const n2 = strToNumber(v2[i]);
    if (n1 > n2) return 1;
    else if (n1 < n2) return -1;
  }

  return 0;
}
```
