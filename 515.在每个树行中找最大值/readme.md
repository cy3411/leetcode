# 题目

给定一棵二叉树的根节点 `root` ，请找出该二叉树中每一层的最大值。

提示：

- 二叉树的节点个数的范围是 $[0,10^4]$
- $-2^{31} \leq Node.val \leq 2^{31} - 1$

# 示例

[![jF6ncd.png](https://s1.ax1x.com/2022/06/25/jF6ncd.png)](https://imgtu.com/i/jF6ncd)

```
输入: root = [1,3,2,5,3,null,9]
输出: [1,3,9]
```

# 题解

遍历所有节点，找出每行最大的放入结果中即可。

遍历节点，可以使用广度优先或深度优先遍历。

## 广度优先搜索

```ts
function largestValues(root: TreeNode | null): number[] {
  if (root === null) return [];
  const ans: number[] = [];
  const queue = [root];
  while (queue.length) {
    const n = queue.length;
    let max = -Infinity;
    for (let i = 0; i < n; i++) {
      const node = queue.shift();
      max = Math.max(max, node.val);
      if (node.left) queue.push(node.left);
      if (node.right) queue.push(node.right);
    }
    ans.push(max);
  }
  return ans;
}
```

## 深度优先搜索

```cpp
class Solution {
public:
    void dfs(TreeNode *root, vector<int> &ans, int depth) {
        if (root == NULL) return;
        if (depth == ans.size()) {
            ans.push_back(root->val);
        } else {
            ans[depth] = max(ans[depth], root->val);
        }
        dfs(root->left, ans, depth + 1);
        dfs(root->right, ans, depth + 1);
    }
    vector<int> largestValues(TreeNode *root) {
        vector<int> ans;
        if (root == nullptr) return ans;
        dfs(root, ans, 0);
        return ans;
    }
};
```
