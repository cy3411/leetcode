/*
 * @lc app=leetcode.cn id=515 lang=cpp
 *
 * [515] 在每个树行中找最大值
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left),
 * right(right) {}
 * };
 */
class Solution {
public:
    void dfs(TreeNode *root, vector<int> &ans, int depth) {
        if (root == NULL) return;
        if (depth == ans.size()) {
            ans.push_back(root->val);
        } else {
            ans[depth] = max(ans[depth], root->val);
        }
        dfs(root->left, ans, depth + 1);
        dfs(root->right, ans, depth + 1);
    }
    vector<int> largestValues(TreeNode *root) {
        vector<int> ans;
        if (root == nullptr) return ans;
        dfs(root, ans, 0);
        return ans;
    }
};
// @lc code=end
