# 题目

字符串轮转。给定两个字符串 s1 和 s2，请编写代码检查 s2 是否为 s1 旋转而成（比如，waterbottle 是 erbottlewat 旋转后的字符串）。

提示：

- 字符串长度在[0, 100000]范围内。

说明:

- 你能只调用一次检查子串的方法吗？

# 示例

```
 输入：s1 = "waterbottle", s2 = "erbottlewat"
 输出：True
```

```
 输入：s1 = "aa", s2 = "aba"
 输出：False
```

# 题解

## 搜索子串

字符串 s1 + s1 包含了所有 s1 轮转可以得到的字符串，所以只要检查 s2 是否是 s1 + s1 的子串即可。

```js
function isFlipedString(s1: string, s2: string): boolean {
  return s1.length === s2.length && (s1 + s1).includes(s2);
}
```
