# 题目

给定一个正整数数组 `w` ，其中 `w[i]` 代表下标 i 的权重（下标从 `0` 开始），请写一个函数 `pickIndex` ，它可以随机地获取下标 `i`，选取下标 `i` 的概率与 `w[i]` 成正比。

例如，对于 `w = [1, 3]`，挑选下标 `0` 的概率为 `1 / (1 + 3) = 0.25` （即，25%），而选取下标 `1` 的概率为 `3 / (1 + 3) = 0.75`（即，75%）。

也就是说，选取下标 i 的概率为 `w[i] / sum(w)` 。

提示：

- `1 <= w.length <= 10000`
- `1 <= w[i] <= 10^5`
- `pickIndex` 将被调用不超过 `10000` 次

# 示例

```
输入：
["Solution","pickIndex","pickIndex","pickIndex","pickIndex","pickIndex"]
[[[1,3]],[],[],[],[],[]]
输出：
[null,1,1,1,1,0]
解释：
Solution solution = new Solution([1, 3]);
solution.pickIndex(); // 返回 1，返回下标 1，返回该下标概率为 3/4 。
solution.pickIndex(); // 返回 1
solution.pickIndex(); // 返回 1
solution.pickIndex(); // 返回 1
solution.pickIndex(); // 返回 0，返回下标 0，返回该下标概率为 1/4 。

由于这是一个随机问题，允许多个答案，因此下列输出都可以被认为是正确的:
[null,1,1,1,1,0]
[null,1,1,1,1,1]
[null,1,1,1,0,0]
[null,1,1,1,0,1]
[null,1,0,1,0,0]
......
诸若此类。
```

# 题解

## 二分搜索

利用前缀和,计算出所有权重在数组中的线段类型的结构.

例如:
`w = [1, 3]`,那么它的前缀和`sum=[1, 4]`.从结果中可以看出,小于等于 1 为索引 0,小于等于 4 的为索引 1.

假设`n`为最大权重`4`,那么我们可以随机一个`[0,n)`的随机数`x`,然后利用二分搜索在`sum`找到第一个大于等于 x 的位置即可.

```ts
class Solution {
  // 线段数组
  sum: number[];
  n: number;
  constructor(w: number[]) {
    //   前缀和，划分区间
    // 比如[1,3]，索引0划分到1，索引1划分到(1,3]区间
    this.sum = [...w];
    for (let i = 1; i < this.sum.length; i++) {
      this.sum[i] += this.sum[i - 1];
    }
    this.n = this.sum[this.sum.length - 1];
  }

  pickIndex(): number {
    // [0,n-1]的随机数
    // 二分查找第一个大于x的索引
    const x = (Math.random() * this.n) >> 0;
    let l = 0;
    let r = this.sum.length - 1;
    while (l < r) {
      let mid = (l + r) >> 1;
      if (this.sum[mid] <= x) l = mid + 1;
      else r = mid;
    }
    return l;
  }
}
```
