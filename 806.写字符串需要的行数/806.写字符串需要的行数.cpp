/*
 * @lc app=leetcode.cn id=806 lang=cpp
 *
 * [806] 写字符串需要的行数
 */

// @lc code=start
class Solution
{
public:
    vector<int> numberOfLines(vector<int> &widths, string s)
    {
        int n = s.size();
        int line = 1;
        int width = 0;
        for (auto &c : s)
        {
            width += widths[c - 'a'];
            if (width > 100)
            {
                line++;
                width = widths[c - 'a'];
            }
        }

        return vector<int>{line, width};
    }
};
// @lc code=end
