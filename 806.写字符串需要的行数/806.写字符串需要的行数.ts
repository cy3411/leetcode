/*
 * @lc app=leetcode.cn id=806 lang=typescript
 *
 * [806] 写字符串需要的行数
 */

// @lc code=start
function numberOfLines(widths: number[], s: string): number[] {
  const base = 'a'.charCodeAt(0);
  const n = s.length;
  let line = 1;
  let width = 0;
  for (let i = 0; i < n; i++) {
    const c = s.charCodeAt(i) - base;
    width += widths[c];
    if (width > 100) {
      line++;
      width = widths[c];
    }
  }
  return [line, width];
}
// @lc code=end
