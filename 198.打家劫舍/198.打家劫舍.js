/*
 * @lc app=leetcode.cn id=198 lang=javascript
 *
 * [198] 打家劫舍
 */

// @lc code=start
/**
 * @description 动态规定滚动数组
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const size = nums.length;
  if (size === 0) return 0;
  if (size === 1) return nums[0];
  if (size === 2) return Math.max(nums[0], nums[1]);

  let first = nums[0];
  let second = Math.max(nums[0], nums[1]);
  for (let i = 2; i < size; i++) {
    let t = second;
    second = Math.max(first + nums[i], second);
    first = t;
  }

  return second;
};
// @lc code=end
