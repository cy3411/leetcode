/*
 * @lc app=leetcode.cn id=198 lang=typescript
 *
 * [198] 打家劫舍
 */

// @lc code=start
function rob(nums: number[]): number {
  const m = nums.length;
  // dp[i][0]不偷当前房子所得到最高金额
  // dp[i][1]偷当前房子所得到的最高金额
  const dp = new Array(m).fill(0).map((_) => new Array(2).fill(0));
  //初始化
  dp[0][0] = 0;
  dp[0][1] = nums[0];

  for (let i = 1; i < m; i++) {
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
    dp[i][1] = dp[i - 1][0] + nums[i];
  }
  // 返回最后房子两种状态的最大值
  return Math.max(dp[m - 1][0], dp[m - 1][1]);
}
// @lc code=end
