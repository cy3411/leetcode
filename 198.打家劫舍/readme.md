# 题目

你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，**如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。**

给定一个代表每个房屋存放金额的非负整数数组，计算你 **不触动警报装置的情况下** ，一夜之内能够偷窃到的最高金额。

提示：

- 0 <= nums.length <= 100
- 0 <= nums[i] <= 400

# 示例

```
输入：[1,2,3,1]
输出：4
解释：偷窃 1 号房屋 (金额 = 1) ，然后偷窃 3 号房屋 (金额 = 3)。
     偷窃到的最高金额 = 1 + 3 = 4 。
```

# 题解

## 动态规划

边界:

一间房子的时候，只能偷窃这一间屋子，结果最大。
二间房子的时候，由于两间屋子相连，不能同时偷取，只能取二者中的最大值。

$$
\begin{cases}
    dp[0]=nums[0],  & \text{只有一间房子} \\
    dp[1]=max(nums[0],nums[1]), & \text{只有两间房子} \\
\end{cases}
$$

状态：

能够偷窃到的最高金额。

选择：

$$dp[i] = Math.max(dp[i-2] + nums[i], dp[i-1])$$

```js
/**
 * @description 动态规划，DP数组
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const size = nums.length;
  const dp = new Array(size + 1).fill(0);

  for (let i = 1; i <= size; i++) {
    if (i === 1) {
      dp[i] = nums[i - 1];
      continue;
    }
    if (i === 2) {
      dp[i] = Math.max(nums[i - 2], nums[i - 1]);
      continue;
    }

    dp[i] = Math.max(dp[i - 2] + nums[i - 1], dp[i - 1]);
  }

  return dp[size];
};
```

```ts
function rob(nums: number[]): number {
  const m = nums.length;
  // dp[i][0]不偷当前房子所得到最高金额
  // dp[i][1]偷当前房子所得到的最高金额
  const dp = new Array(m).fill(0).map((_) => new Array(2).fill(0));
  //初始化
  dp[0][0] = 0;
  dp[0][1] = nums[0];

  for (let i = 1; i < m; i++) {
    dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1]);
    dp[i][1] = dp[i - 1][0] + nums[i];
  }
  // 返回最后房子两种状态的最大值
  return Math.max(dp[m - 1][0], dp[m - 1][1]);
}
```

```js
/**
 * @description 动态规划，滚动数组
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const size = nums.length;
  if (size === 0) return 0;
  if (size === 1) return nums[0];
  if (size === 2) return Math.max(nums[0], nums[1]);

  let first = nums[0];
  let second = Math.max(nums[0], nums[1]);
  for (let i = 2; i < size; i++) {
    let t = second;
    second = Math.max(first + nums[i], second);
    first = t;
  }

  return second;
};
```
