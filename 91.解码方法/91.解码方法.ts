/*
 * @lc app=leetcode.cn id=91 lang=typescript
 *
 * [91] 解码方法
 */

// @lc code=start
function numDecodings(s: string): number {
  let size = s.length;
  // 前面加入一个哨兵位置，简化s[i-1]需要判断负数位的情况
  s = '_' + s;
  // dp[i-2]
  let x = 0;
  // dp[i-1]
  let y = 1;
  // dp[i]
  let z = 0;

  for (let i = 1; i <= size; i++) {
    z = 0;
    let a = Number(s[i]);
    let b = Number(s[i - 1]) * 10 + Number(s[i]);
    if (a !== 0) {
      z = y;
    }
    if (b >= 10 && b <= 26) {
      z += x;
    }
    x = y;
    y = z;
  }

  return z;
}
// @lc code=end
