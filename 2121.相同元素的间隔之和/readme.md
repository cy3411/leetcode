# 题目

给你一个下标从 `0` 开始、由 `n` 个整数组成的数组 `arr` 。

`arr` 中两个元素的 **间隔** 定义为它们下标之间的 **绝对差** 。更正式地，`arr[i]` 和 `arr[j]` 之间的间隔是 `|i - j|` 。

返回一个长度为 `n` 的数组 `intervals` ，其中 `intervals[i]` 是 `arr[i]` 和 `arr` 中每个相同元素（与 `arr[i]` 的值相同）的 间隔之和 。

注意：`|x|` 是 `x` 的绝对值。

提示：

- $\color{burlywood} n \equiv arr.length$
- $\color{burlywood} 1 \leq n \leq 10^5$
- $\color{burlywood} 1 \leq arr[i] \leq 10^5$

# 示例

```
输入：arr = [2,1,3,1,2,3,3]
输出：[4,2,7,2,4,4,5]
解释：
- 下标 0 ：另一个 2 在下标 4 ，|0 - 4| = 4
- 下标 1 ：另一个 1 在下标 3 ，|1 - 3| = 2
- 下标 2 ：另两个 3 在下标 5 和 6 ，|2 - 5| + |2 - 6| = 7
- 下标 3 ：另一个 1 在下标 1 ，|3 - 1| = 2
- 下标 4 ：另一个 2 在下标 0 ，|4 - 0| = 4
- 下标 5 ：另两个 3 在下标 2 和 6 ，|5 - 2| + |5 - 6| = 4
- 下标 6 ：另两个 3 在下标 2 和 5 ，|6 - 2| + |6 - 5| = 5
```

```
输入：arr = [10,5,10,10]
输出：[5,0,3,4]
解释：
- 下标 0 ：另两个 10 在下标 2 和 3 ，|0 - 2| + |0 - 3| = 5
- 下标 1 ：只有这一个 5 在数组中，所以到相同元素的间隔之和是 0
- 下标 2 ：另两个 10 在下标 0 和 3 ，|2 - 0| + |2 - 3| = 3
- 下标 3 ：另两个 10 在下标 0 和 2 ，|3 - 0| + |3 - 2| = 4
```

# 题解

## 排序+前缀和

我们先使用下标排序，将相同数字的下标聚在一起，然后遍历数组计算间隔和。

假设 `[i ,j]` 区间，是**相同数字**的下标区间。 这个时候我们要计算 `k` 位置的间隔和，比如： `[i...k...j]`。

$$
\begin{cases}
    (v_k-v_i) + (v_k-v_{i+1}) + ... + (v_k - v_{k-1}) ,& k位置之前的间隔和 \\
    (v_j-v_k) + (v_{j-1}-v_k) + ... + (v_{k+1}-v_k),& k位置之后的间隔和 \\
\end{cases}
$$

上面公式可以简化为:

$$
\begin{cases}
    v_k*(k-i) - sum\_pre &sum\_pre就是i到k-1位置的前缀和 \\
    next\_sum - v_k*(j-k) &next\_sum就是k+1到j位置的前缀和 \\
\end{cases}
$$

将上面两项相加就是 `k` 位置的间隔和。

```ts
function getDistances(arr: number[]): number[] {
  const n = arr.length;
  // 下标排序
  const idxs = new Array(n).fill(0).map((_, i) => i);
  // 对数组按照数字升序排序，如果数字相同，按照下标升序
  idxs.sort((a, b) => {
    if (arr[a] !== arr[b]) {
      return arr[a] - arr[b];
    }
    return a - b;
  });

  const ans = new Array(n).fill(0);
  for (let i = 0, j = 0; i < n; i = j + 1) {
    j = i;
    // 找到相同数字的区间
    while (j + 1 < n && arr[idxs[j + 1]] === arr[idxs[i]]) j++;
    let sum_pre = 0;
    let sum_next = 0;
    for (let k = i; k <= j; k++) sum_next += idxs[k];
    for (let k = i; k <= j; k++) {
      sum_next -= idxs[k];
      ans[idxs[k]] = (k - i) * idxs[k] - sum_pre + (sum_next - (j - k) * idxs[k]);
      sum_pre += idxs[k];
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<long long> getDistances(vector<int> &arr)
    {
        int n = arr.size();
        vector<int> idxs(n);
        // 初始化下标数组
        for (int i = 0; i < n; i++)
        {
            idxs[i] = i;
        }
        // 排序
        sort(idxs.begin(), idxs.end(), [&](int i, int j) -> bool
             {
                if (arr[i] != arr[j]) {
                    return arr[i] < arr[j];
                }
                return i < j; });
        // 初始化答案数组
        vector<long long> ans(n);
        // 计算答案
        for (int i = 0, j = 0; i < n; i = j + 1)
        {
            // [i ,j] 是相同数字的下标区间
            j = i;
            while (j + 1 < n && arr[idxs[j + 1]] == arr[idxs[i]])
            {
                j++;
            }
            // k 位置前后的前缀和
            long long sum_pre = 0, sum_next = 0;
            for (int k = i; k <= j; k++)
            {
                sum_next += idxs[k];
            }
            for (int k = i; k <= j; k++)
            {
                sum_next -= idxs[k];
                ans[idxs[k]] = (long long)(2 * k - i - j) * idxs[k] - sum_pre + sum_next;
                sum_pre += idxs[k];
            }
        }
        return ans;
    }
};
```
