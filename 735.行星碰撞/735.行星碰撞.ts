/*
 * @lc app=leetcode.cn id=735 lang=typescript
 *
 * [735] 行星碰撞
 */

// @lc code=start
function asteroidCollision(asteroids: number[]): number[] {
  const stack: number[] = [];
  for (const asteroid of asteroids) {
    let isAlive = true;
    while (isAlive && stack.length && asteroid < 0 && stack[stack.length - 1] > 0) {
      isAlive = stack[stack.length - 1] < -asteroid;
      if (stack[stack.length - 1] <= -asteroid) {
        stack.pop();
      }
    }
    if (isAlive) stack.push(asteroid);
  }

  return stack;
}
// @lc code=end
