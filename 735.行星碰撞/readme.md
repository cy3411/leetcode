# 题目

给定一个整数数组 `asteroids`，表示在同一行的行星。

对于数组中的每一个元素，其绝对值表示行星的大小，正负表示行星的移动方向（正表示向右移动，负表示向左移动）。每一颗行星以相同的速度移动。

找出碰撞后剩下的所有行星。碰撞规则：两个行星相互碰撞，较小的行星会爆炸。如果两颗行星大小相同，则两颗行星都会爆炸。两颗移动方向相同的行星，永远不会发生碰撞。

提示：

- $2 \leq asteroids.length \leq 10^4$
- $-1000 \leq asteroids[i] \leq 1000$
- $asteroids[i] \not = 0$

# 示例

```
输入：asteroids = [5,10,-5]
输出：[5,10]
解释：10 和 -5 碰撞后只剩下 10 。 5 和 10 永远不会发生碰撞。
```

```
输入：asteroids = [8,-8]
输出：[]
解释：8 和 -8 碰撞后，两者都发生爆炸。
```

# 题解

## 栈

根据题意可以看出，我们需要维护访问过的行星，所以可以使用栈来解决问题。

当遍历到行星 asteroid 时，使用 isAlive 来记录行星是否爆炸。当行星 asteroid 存在且小于 0，并且栈不为空，且栈顶元素大于 0 的时，说明两个行星将发生碰撞。

当栈顶元素大于 -asteroid 时， asteroid 将爆炸，将 isAlive 设置为 false，如果栈顶元素小于 -asteroid，则将栈顶元素出栈。重复以上判断，直到不满足条件。

如果 isAlive 为 true，则将 asteroid 压入栈中。

```ts
function asteroidCollision(asteroids: number[]): number[] {
  const stack: number[] = [];
  for (const asteroid of asteroids) {
    let isAlive = true;
    while (isAlive && stack.length && asteroid < 0 && stack[stack.length - 1] > 0) {
      isAlive = stack[stack.length - 1] < -asteroid;
      if (stack[stack.length - 1] <= -asteroid) {
        stack.pop();
      }
    }
    if (isAlive) stack.push(asteroid);
  }

  return stack;
}
```
