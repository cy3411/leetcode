/*
 * @lc app=leetcode.cn id=852 lang=typescript
 *
 * [852] 山脉数组的峰顶索引
 */

// @lc code=start
function peakIndexInMountainArray(arr: number[]): number {
  let l = 0;
  let r = arr.length - 1;
  let mid: number;

  // 逐渐缩小区间，保证查找的元素一定在区间范围内
  while (l <= r) {
    mid = l + ((r - l) >> 1);
    if (arr[mid] > arr[mid - 1] && arr[mid] > arr[mid + 1]) return mid;
    if (arr[mid] > arr[mid - 1]) l = mid;
    if (arr[mid] > arr[mid + 1]) r = mid;
  }
}
// @lc code=end
