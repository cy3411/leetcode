# 题目

符合下列属性的数组 `arr` 称为 **山脉数组** ：

- `arr.length >= 3`
- 存在 `i（0 < i < arr.length - 1）`使得：
  - `arr[0] < arr[1] < ... arr[i-1] < arr[i]`
  - `arr[i] > arr[i+1] > ... > arr[arr.length - 1]`
  - 给你由整数组成的山脉数组 `arr` ，返回任何满足 `arr[0] < arr[1] < ... arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1]` 的下标 `i` 。

提示：

- `3 <= arr.length <= 104`
- `0 <= arr[i] <= 106`
- 题目数据保证 `arr` 是一个山脉数组

进阶：很容易想到时间复杂度 `O(n)` 的解决方案，你可以设计一个 `O(log(n))` 的解决方案吗？

# 示例

```
输入：arr = [0,1,0]
输出：1
```

```
输入：arr = [0,2,1,0]
输出：1
```

# 题解

## 贪心

遍历数组，每次比较当前元素和前一个元素。如果当前元素比较大，就更新结果。

```ts
function peakIndexInMountainArray(arr: number[]): number {
  const size = arr.length;
  let ans = -1;

  for (let i = 1; i < size - 1; i++) {
    if (arr[i] > arr[i - 1]) {
      ans = i;
    }
  }

  return ans;
}
```

## 二分查找

观察可以看出数组中元素在部分区间内具有单调性，可以使用二分去查找结果。

```ts
function peakIndexInMountainArray(arr: number[]): number {
  let l = 0;
  let r = arr.length - 1;
  let mid: number;

  while (l <= r) {
    mid = l + ((r - l) >> 1);
    if (arr[mid] > arr[mid - 1] && arr[mid] > arr[mid + 1]) return mid;
    if (arr[mid] > arr[mid - 1]) l = mid;
    if (arr[mid] > arr[mid + 1]) r = mid;
  }
}
```
