# 题目

给你一个 `m x n` 的二元矩阵 `matrix` ，且所有值被初始化为 `0` 。请你设计一个算法，随机选取一个满足 `matrix[i][j] == 0` 的下标 `(i, j)` ，并将它的值变为 `1` 。所有满足 `matrix[i][j] == 0` 的下标 `(i, j)` 被选取的概率应当均等。

尽量最少调用内置的随机函数，并且优化时间和空间复杂度。

实现 Solution 类：

- `Solution(int m, int n)` 使用二元矩阵的大小 `m` 和 `n` 初始化该对象
- `int[] flip()` 返回一个满足 `matrix[i][j] == 0` 的随机下标 `[i, j]` ，并将其对应格子中的值变为 `1`
- `void reset()` 将矩阵中所有的值重置为 `0`

提示：

- $1 <= m, n <= 10^4$
- 每次调用 `flip` 时，矩阵中至少存在一个值为 `0` 的格子。
- 最多调用 `1000` 次 `flip` 和 `reset` 方法。

# 示例

```
输入
["Solution", "flip", "flip", "flip", "reset", "flip"]
[[3, 1], [], [], [], [], []]
输出
[null, [1, 0], [2, 0], [0, 0], null, [2, 0]]

解释
Solution solution = new Solution(3, 1);
solution.flip();  // 返回 [1, 0]，此时返回 [0,0]、[1,0] 和 [2,0] 的概率应当相同
solution.flip();  // 返回 [2, 0]，因为 [1,0] 已经返回过了，此时返回 [2,0] 和 [0,0] 的概率应当相同
solution.flip();  // 返回 [0, 0]，根据前面已经返回过的下标，此时只能返回 [0,0]
solution.reset(); // 所有值都重置为 0 ，并可以再次选择下标返回
solution.flip();  // 返回 [2, 0]，此时返回 [0,0]、[1,0] 和 [2,0] 的概率应当相同
```

# 题解

## 哈希表

可以将矩阵转换为长度为 `m x n` 的一维数组，通过随机`[0,m*n)`区间的值来获取随机下标。

当随机到某个位置 `idx` 的时候：

- 该位置未被哈希表真实记录（未被翻转）：说明 `idx` 可被直接使用，使用 `idx` 作为本次随机点。同时将右端点（未被使用的）位置的映射值放到该位置，将右端点左移。确保下次再随机到 `idx` ，仍能使用 `idx` 的映射值，同时维护了随机区间的连续性；
- 该位置已被哈希表真实记录（已被翻转）：此时直接使用 `idx` 存储的映射值（上一次交换时的右端点映射值）即可，然后用新的右端点映射值将其进行覆盖，更新右端点。确保下次再随机到 `idx`，仍能直接使用 `idx` 的映射值，同时维护了随机区间的连续性。

```ts
class Solution {
  private total: number;
  private hash: Map<number, number> = new Map();
  constructor(private m: number, private n: number) {
    this.m = m;
    this.n = n;
    // 值为0的元素个数
    this.total = m * n;
    this.hash = new Map();
  }

  flip(): number[] {
    if (this.total === 0) {
      return [];
    }
    // 随机选择一个数
    let num = Math.floor(Math.random() * this.total);
    this.total--;
    // 如果这个数已经出现过了，就选择哈希表中的数
    const idx = this.hash.get(num) || num;
    // 将num映射为值为0的元素下标
    this.hash.set(num, this.hash.get(this.total) || this.total);
    // 一维坐标转换为二维坐标
    return [Math.floor(idx / this.n), idx % this.n];
  }

  reset(): void {
    this.total = this.m * this.n;
    this.hash.clear();
  }
}
```
