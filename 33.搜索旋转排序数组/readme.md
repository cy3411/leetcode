# 题目
整数数组 `nums` 按升序排列，数组中的值 互不相同 。

在传递给函数之前，`nums` 在预先未知的某个下标 `k（0 <= k < nums.length）`上进行了 旋转，使数组变为 `[nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]`（下标 从 0 开始 计数）。例如， `[0,1,2,4,5,6,7]` 在下标 3 处经旋转后可能变为 `[4,5,6,7,0,1,2]` 。

给你 旋转后 的数组 nums 和一个整数 target ，如果 nums 中存在这个目标值 target ，则返回它的下标，否则返回 -1 。

# 示例
```
输入：nums = [4,5,6,7,0,1,2], target = 0
输出：4
```

```
输入：nums = [4,5,6,7,0,1,2], target = 3
输出：-1
```
# 方法
我们将数组分成两个部分的时候，一定有一个是有序的。

然后根据有序的那个区间，来改变二分查找的上下界。

因为我们可以根据有序的区间来判断target在不在这里区间：
+ 如果`[left,mid-1]`有序，且target满足`[nums[left],nums[mid])`，我们将搜索范围缩小至`[left,mid-1]`,
  否则在`[mid+1,right]`中搜索。
+ 如果`[mid+1,right]`有序，且target满足`(nums[mid],nums[right]]`，我们将搜索范围缩小至`[mid+1, right]`,
  否则在`[left, mid-1]`中搜索。

```js
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function (nums, target) {
  const size = nums.length;
  if (size === 0) return -1;
  if (size === 1) return target === nums[0] ? 0 : -1;

  let left = 0;
  let right = size - 1;

  while (left <= right) {
    let mid = left + ((right - left) >> 1);

    if (nums[mid] === target) return mid;

    if (nums[left] <= nums[mid]) {
      if (nums[left] <= target && target < nums[mid]) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    } else {
      if (nums[mid] < target && target <= nums[size - 1]) {
        left = mid + 1;
      } else {
        right = mid - 1;
      }
    }
  }

  return -1;
};
```