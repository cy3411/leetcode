# 题目
给定一个二叉树，返回其节点值的锯齿形层序遍历。（即先从左往右，再从右往左进行下一层遍历，以此类推，层与层之间交替进行）。

# 示例
```
给定二叉树 [3,9,20,null,null,15,7]:

    3
   / \
  9  20
    /  \
   15   7

返回锯齿形层序遍历如下：

[
  [3],
  [20,9],
  [15,7]
]
```

# 方法
获取层序遍历结果的方法可以参考这里：[ 107.二叉树的层序遍历-ii ](https://gitee.com/cy3411/leecode/tree/master/107.%E4%BA%8C%E5%8F%89%E6%A0%91%E7%9A%84%E5%B1%82%E5%BA%8F%E9%81%8D%E5%8E%86-ii)

对最后获取的结果，对偶数位的进行一次逆序反转就可以了。

```js
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var zigzagLevelOrder = function (root) {
  if (root === null) return [];
  // 将root节点放入结果中
  const getResult = (root, k, result) => {
    if (root === null) return;
    if (k === result.length) result.push([]);
    result[k].push(root.val);

    getResult(root.left, k + 1, result);
    getResult(root.right, k + 1, result);
  };
  // 反转数组
  const reverse = (arr) => {
    for (let i = 0, j = arr.length - 1; i < j; i++, j--) {
      [arr[i], arr[j]] = [arr[j], arr[i]];
    }
  };
  const result = [];

  getResult(root, 0, result);
  // 将偶数位的节点数组反转
  for (let i = 1; i < result.length; i += 2) {
    reverse(result[i]);
  }

  return result;
};
```
