/*
 * @lc app=leetcode.cn id=517 lang=typescript
 *
 * [517] 超级洗衣机
 */

// @lc code=start
function findMinMoves(machines: number[]): number {
  const total = machines.reduce((prev, curr) => prev + curr);
  const n = machines.length;
  // 不能均分
  if (total % n !== 0) return -1;

  const avg = total / n;
  let ans = 0;
  // 统计前面需要向后移动多少次
  let sum = 0;
  for (let num of machines) {
    // 记录差值，移出或者移入
    num -= avg;
    sum += num;
    ans = Math.max(ans, Math.abs(sum), num);
  }

  return ans;
}
// @lc code=end
