# 题目

假设有 `n` 台超级洗衣机放在同一排上。开始的时候，每台洗衣机内可能有一定量的衣服，也可能是空的。

在每一步操作中，你可以选择任意 `m` (`1 <= m <= n`) 台洗衣机，与此同时将每台洗衣机的一件衣服送到相邻的一台洗衣机。

给定一个整数数组 `machines` 代表从左至右每台洗衣机中的衣物数量，请给出能让所有洗衣机中剩下的衣物的数量相等的 **最少的操作步数** 。如果不能使每台洗衣机中衣物的数量相等，则返回 `-1` 。

提示：

- `n == machines.length`
- $\color{#FFECD1} 1 \leq n \leq 10^4$
- $\color{#FFECD1} 0 \leq machines[i] \leq 10^5$

# 示例

```
输入：machines = [1,0,5]
输出：3
解释：
第一步:    1     0 <-- 5    =>    1     1     4
第二步:    1 <-- 1 <-- 4    =>    2     1     3
第三步:    2     1 <-- 3    =>    2     2     2
```

# 题解

## 贪心

设定所有的的衣服为 `total`，要使所有的洗衣机的衣服数量一致，那么 `total` 必须是 `n` 的倍数，否则返回`-1`。

设 `avg = total/n`,定义`machines[i]′=machines[i]-avg`，如果 `machines[i]′` 为正，表示要移走 `machines[i]′` 件衣服，如果 `machines[i]′` 为负，表示要移入 `-machines[i]′` 件衣服。

将前 i 台洗衣机看成一组，记作 A，其余洗衣机看成另一组，记作 B。设 $\textit{sum}[i]=\sum_{j=0}^i \textit{machines[i]′}$，若 sum[i] 为正则说明需要从 A 向 B 移入 sum[i] 件衣服，为负则说明需要从 B 向 A 移入 −sum[i] 件衣服。

```ts
function findMinMoves(machines: number[]): number {
  const total = machines.reduce((prev, curr) => prev + curr);
  const n = machines.length;
  // 不能均分
  if (total % n !== 0) return -1;

  const avg = total / n;
  let ans = 0;
  // 统计前面需要向后移动多少次
  let sum = 0;
  for (let num of machines) {
    // 记录差值，移出或者移入
    num -= avg;
    sum += num;
    ans = Math.max(ans, Math.abs(sum), num);
  }

  return ans;
}
```
