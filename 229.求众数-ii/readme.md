# 题目

给定一个大小为 `n` 的整数数组，找出其中所有出现超过 $\color{burlywood} \lfloor n/3 \rfloor$ 次的元素。

提示：

- $\color{burlywood}1 \leq nums.length \leq 5 * 10^4$
- $\color{burlywood}-10^9 \leq nums[i] \leq 10^9$

进阶：尝试设计时间复杂度为 O(n)、空间复杂度为 O(1)的算法解决此问题。

# 示例

```
输入：[3,2,3]
输出：[3]
```

```
输入：nums = [1]
输出：[1]
```

# 题解

## 计数

正常处理的话，可以使用哈希表记录每个数字的出现次数，然后遍历哈希，找出次数大于 $\color{burlywood}\lfloor n/3 \rfloor$ 的数字。

但是题目进阶的要求是使用 `O(1)` 的空间复杂度。

题目要求找出出现次数超过 $\color{burlywood}\lfloor n/3 \rfloor$ 的数字，那么只需要记录两个数字，即可。

定义 `a,b` 记录两个数字和出现的次数：

- 如果 `a` 为空，将当前数字记录到 `a` 中，并记录次数为 `1`
- 如果当前数字和 `a` 相同，则次数加 `1`
- 如果 `b` 为空，将当前数字记录到 `b` 中，并记录次数为 `1`
- 如果当前数字和 `b` 相同，则次数加 `1`
- 否则，a 和 b 的次数减 1

遍历完毕，最后 `a，b` 中保留的是出现次数较多的两个数字。我们在遍历一次数组，统计 `a，b` 的次数，如果次数大于 `n/3`，则输出。

```ts
function majorityElement(nums: number[]): number[] {
  // 出现次数最多的数字和数量
  const a = [0, 0];
  // 出现次数次多的数字和数量
  const b = [0, 0];

  for (const x of nums) {
    if (x === a[0] && a[1]) {
      a[1]++;
    } else if (x === b[0] && b[1]) {
      b[1]++;
    } else if (a[1] === 0) {
      a[0] = x;
      a[1] = 1;
    } else if (b[1] === 0) {
      b[0] = x;
      b[1] = 1;
    } else {
      a[1]--;
      b[1]--;
    }
  }

  // 重置计数，然后统计2个数字的具体出现次数
  a[1] = b[1] = 0;
  for (const x of nums) {
    if (x === a[0]) {
      a[1]++;
    } else if (x === b[0]) {
      b[1]++;
    }
  }

  const ans = [];
  const n = nums.length;
  // 出现次数超过1/3的数字放入答案
  if (a[1] * 3 > n) ans.push(a[0]);
  if (b[1] * 3 > n) ans.push(b[0]);

  return ans;
}
```

```cpp
class Solution
{
public:
    vector<int> majorityElement(vector<int> &nums)
    {
        pair<int, int> a(0, 0), b(0, 0);

        for (int x : nums)
        {
            if (a.second && a.first == x)
            {
                a.second++;
            }
            else if (b.second && b.first == x)
            {
                b.second++;
            }
            else if (a.second == 0)
            {
                a.first = x;
                a.second = 1;
            }
            else if (b.second == 0)
            {
                b.first = x;
                b.second = 1;
            }
            else
            {
                a.second--;
                b.second--;
            }
        }

        a.second = b.second = 0;
        for (int x : nums)
        {
            if (x == a.first)
            {
                a.second++;
            }
            else if (x == b.first)
            {
                b.second++;
            }
        }

        vector<int> ans;
        int n = nums.size();
        if (a.second * 3 > n)
            ans.push_back(a.first);
        if (b.second * 3 > n)
            ans.push_back(b.first);

        return ans;
    }
};
```
