/*
 * @lc app=leetcode.cn id=860 lang=javascript
 *
 * [860] 柠檬水找零
 */

// @lc code=start
/**
 * @param {number[]} bills
 * @return {boolean}
 */
var lemonadeChange = function (bills) {
  let bill5 = 0;
  let bill10 = 0;

  for (const bill of bills) {
    if (bill === 5) {
      bill5++;
    } else if (bill === 10) {
      if (bill5 === 0) {
        return false;
      }
      bill5--;
      bill10++;
    } else if (bill === 20) {
      if (bill5 && bill10) {
        bill5--;
        bill10--;
      } else if (bill5 >= 3) {
        bill5 -= 3;
      } else {
        return false;
      }
    }
  }
  return true;
};
// @lc code=end
