# 题目

给定一棵二叉树，其中每个节点都含有一个整数数值(该值或正或负)。设计一个算法，打印节点数值总和等于某个给定值的所有路径的数量。注意，路径不一定非得从二叉树的根节点或叶节点开始或结束，但是其方向必须向下(只能从父节点指向子节点方向)。

提示：

- `节点总数 <= 10000`

# 示例

给定如下二叉树，以及目标和 sum = 22，

```
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1
```

返回:

```
3
解释：和为 22 的路径有：[5,4,11,2], [5,8,4,5], [4,11,7]
```

# 题解

## 递归

递归计算以当前节点为根节点的所有路径的和，如果和等于 sum，则计数加一。

题目要求不一定是根节点开始，所以我们递归所有的节点当作根节点，调用上面的方式来计算。最后返回计数。

```ts
function pathSum(root: TreeNode | null, sum: number): number {
  if (root === null) return 0;
  const a = pathSum(root.left, sum);
  const b = pathSum(root.right, sum);

  return dfs(root, sum) + a + b;
}

function dfs(root: TreeNode | null, sum: number): number {
  if (root === null) return 0;
  sum -= root.val;

  return Number(sum === 0) + dfs(root.left, sum) + dfs(root.right, sum);
}
```
