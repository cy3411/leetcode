// @algorithm @lc id=1000009 lang=typescript
// @title paths-with-sum-lcci
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function pathSum(root: TreeNode | null, sum: number): number {
  if (root === null) return 0;
  const a = pathSum(root.left, sum);
  const b = pathSum(root.right, sum);

  return dfs(root, sum) + a + b;
}

function dfs(root: TreeNode | null, sum: number): number {
  if (root === null) return 0;
  sum -= root.val;

  return Number(sum === 0) + dfs(root.left, sum) + dfs(root.right, sum);
}
