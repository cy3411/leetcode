# 题目

给你一个整数 `n` ，请你返回所有 0 到 1 之间（不包括 0 和 1）满足分母小于等于 `n` 的 **最简** 分数 。分数可以以 **任意** 顺序返回。

提示：

`1 <= n <= 100`

# 示例

```
输入：n = 2
输出：["1/2"]
解释："1/2" 是唯一一个分母小于等于 2 的最简分数。
```

```
输入：n = 3
输出：["1/2","1/3","2/3"]
```

# 题解

## 最大公约数

所谓的最简分数，就是分子分母的最大公约数为 1。

遍历分子和分母，找到最大公约数为 1 的分子和分母，将其放入结果中。

```ts
function simplifiedFractions(n: number): string[] {
  // 获取最大公约数
  const gcd = (a: number, b: number): number => {
    return b === 0 ? a : gcd(b, a % b);
  };

  const ans = [];
  for (let i = 1; i <= n; i++) {
    for (let j = i + 1; j <= n; j++) {
      // 如果最大公约数为1，则说明最简分数
      if (gcd(i, j) !== 1) continue;
      ans.push(`${i}/${j}`);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int gcd(int a, int b)
    {
        return b == 0 ? a : gcd(b, a % b);
    }
    vector<string> simplifiedFractions(int n)
    {
        vector<string> ans;
        for (int i = 1; i <= n; i++)
        {
            for (int j = i + 1; j <= n; j++)
            {
                int g = gcd(i, j);
                if (g == 1)
                {
                    ans.push_back(to_string(i) + "/" + to_string(j));
                }
            }
        }

        return ans;
    }
};
```
