/*
 * @lc app=leetcode.cn id=946 lang=cpp
 *
 * [946] 验证栈序列
 */

// @lc code=start
class Solution {
public:
    bool validateStackSequences(vector<int> &pushed, vector<int> &popped) {
        int n = pushed.size(), i = 0;
        stack<int> stk;
        for (auto num : pushed) {
            stk.emplace(num);
            while (!stk.empty() && stk.top() == popped[i]) {
                i++;
                stk.pop();
            }
        }
        return stk.empty();
    }
};
// @lc code=end
