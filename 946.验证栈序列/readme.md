# 题目

给定 pushed 和 popped 两个序列，每个序列中的 值都不重复，只有当它们可能是在最初空栈上进行的推入 push 和弹出 pop 操作序列的结果时，返回 true；否则，返回 false 。

# 示例

```
输入：pushed = [1,2,3,4,5], popped = [4,5,3,2,1]
输出：true
解释：我们可以按以下顺序执行：
push(1), push(2), push(3), push(4), pop() -> 4,
push(5), pop() -> 5, pop() -> 3, pop() -> 2, pop() -> 1
```

# 方法

## 栈

模拟入栈操作，每次入栈元素后检查栈顶元素是否和出栈序列的元素匹配。匹配的话就出栈，最后判断栈中是否为空。

```js
/**
 * @param {number[]} pushed
 * @param {number[]} popped
 * @return {boolean}
 */
var validateStackSequences = function (pushed, popped) {
  // 出栈元素必然等于当前栈顶元素或者下一个入栈元素
  let stack = [];
  let i = 0;

  for (const n of pushed) {
    stack.push(n);
    while (stack.length && popped[i] === stack[stack.length - 1]) {
      stack.pop();
      i++;
    }
  }

  return !stack.length;
};
```

```js
function validateStackSequences(pushed: number[], popped: number[]): boolean {
  // 模拟栈
  const stack = new Array<number>();
  const n = pushed.length;
  // popped序列索引
  let i = 0;
  for (const num of pushed) {
    stack.push(num);
    // 检查栈顶元素是否和出栈序列匹配
    while (stack.length && popped[i] === stack.at(-1)) {
      stack.pop();
      i++;
    }
  }
  return stack.length === 0;
}
```

```cpp
class Solution {
public:
    bool validateStackSequences(vector<int> &pushed, vector<int> &popped) {
        int n = pushed.size(), i = 0;
        stack<int> stk;
        for (auto num : pushed) {
            stk.emplace(num);
            while (!stk.empty() && stk.top() == popped[i]) {
                i++;
                stk.pop();
            }
        }
        return stk.empty();
    }
};
```

```py
class Solution:
    def validateStackSequences(self, pushed: List[int], popped: List[int]) -> bool:
        stk = []
        i = 0
        for x in pushed:
            stk.append(x)
            while stk and popped[i] == stk[-1]:
                stk.pop()
                i += 1
        return len(stk) == 0
```
