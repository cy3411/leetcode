# 题目

请你设计一个数据结构，支持 添加新单词 和 查找字符串是否与任何先前添加的字符串匹配 。

实现词典类 `WordDictionary` ：

-` WordDictionary()` 初始化词典对象

- `void addWord(word)` 将 `word` 添加到数据结构中，之后可以对它进行匹配
- `bool search(word)` 如果数据结构中存在字符串与 `word` 匹配，则返回 `true` ；否则，返回 `false` `。word` 中可能包含一些 `'.'` ，每个 `.` 都可以表示任何一个字母。

提示：

- `1 <= word.length <= 500`
- `addWord` 中的 `word` 由小写英文字母组成
- `search` 中的 `word` 由 `'.'` 或小写英文字母组成
- 最多调用 `50000` 次 `addWord` 和 `search`

# 示例

```
输入：
["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
[[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
输出：
[null,null,null,null,false,true,true,true]

解释：
WordDictionary wordDictionary = new WordDictionary();
wordDictionary.addWord("bad");
wordDictionary.addWord("dad");
wordDictionary.addWord("mad");
wordDictionary.search("pad"); // return False
wordDictionary.search("bad"); // return True
wordDictionary.search(".ad"); // return True
wordDictionary.search("b.."); // return True
```

# 题解

## Tire 树

字典树的实现可以参考[「208. 实现 Trie (前缀树) 的官方题解」](https://gitee.com/cy3411/leetcode/tree/master/208.%E5%AE%9E%E7%8E%B0-trie-%E5%89%8D%E7%BC%80%E6%A0%91)

需要注意的是就是字符是`.`的话，就需要访问当前字典树的所有子节点。

```ts
class WordDictionary {
  tireRoot: Tire;
  base: number;
  constructor() {
    this.tireRoot = new Tire();
    this.base = 'a'.charCodeAt(0);
  }

  addWord(word: string): void {
    this.tireRoot.insert(word);
  }

  search(word: string): boolean {
    const dfs = (index: number, node: Tire): boolean => {
      if (index === word.length) return node.isEnd;

      const char = word[index];
      if (char === '.') {
        for (const child of node.children) {
          if (child && dfs(index + 1, child)) {
            return true;
          }
        }
      } else {
        const child = node.children[char.charCodeAt(0) - this.base];
        if (child && dfs(index + 1, child)) {
          return true;
        }
      }

      return false;
    };

    return dfs(0, this.tireRoot);
  }
}

class Tire {
  children: Tire[];
  isEnd: boolean;
  base: number;
  constructor() {
    this.children = new Array(26).fill(null);
    this.isEnd = false;
    this.base = 'a'.charCodeAt(0);
  }

  insert(word: string) {
    let node: Tire = this;
    for (let i = 0; i < word.length; i++) {
      const index = word.charCodeAt(i) - this.base;
      if (node.children[index] === null) {
        node.children[index] = new Tire();
      }
      node = node.children[index];
    }
    node.isEnd = true;
  }
}
```
