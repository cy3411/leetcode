# 题目

给你一个有效的 `IPv4` 地址 `address`，返回这个 `IP` 地址的无效化版本。

所谓无效化 `IP` 地址，其实就是用 `"[.]"` 代替了每个 `"."`。

提示：

- 给出的 `address` 是一个有效的 `IPv4` 地址

# 示例

```
输入：address = "1.1.1.1"
输出："1[.]1[.]1[.]1"
```

```
输入：address = "255.100.50.0"
输出："255[.]100[.]50[.]0"
```

# 题解

## 正则表达式

利用正则表达式全局替换所有的 `"."` 为 `"[.]"`，即可实现无效化。

```ts
function defangIPaddr(address: string): string {
  return address.replace(/\.{1}/g, '[.]');
}
```

```py
class Solution:
    def defangIPaddr(self, address: str) -> str:
        return address.replace('.', '[.]')
```

## 遍历

遍历字符串，每遇到一个 `"."` 就替换为 `"[.]"`。

```cpp
class Solution {
public:
    string defangIPaddr(string address) {
        string ans;
        for (auto c : address) {
            if (c == '.') {
                ans += "[.]";
            } else {
                ans += c;
            }
        }
        return ans;
    }
};
```
