#
# @lc app=leetcode.cn id=2960 lang=python3
#
# [2960] 统计已测试设备
#

# @lc code=start
class Solution:
    def countTestedDevices(self, batteryPercentages: List[int]) -> int:
        ans = 0

        for i in range(len(batteryPercentages)):
            batteryPercentages[i] = max(0, batteryPercentages[i] - ans)
            if batteryPercentages[i] > 0:
                ans += 1

        return ans
# @lc code=end
