/*
 * @lc app=leetcode.cn id=2960 lang=typescript
 *
 * [2960] 统计已测试设备
 */

// @lc code=start
function countTestedDevices(batteryPercentages: number[]): number {
  const n = batteryPercentages.length;
  let ans = 0;
  for (let i = 0; i < n; i++) {
    // 计算当前设备的剩余电量
    batteryPercentages[i] = Math.max(0, batteryPercentages[i] - ans);
    // 电量大于0，说明当前设备未测试
    if (batteryPercentages[i] > 0) {
      ans++;
    }
  }

  return ans;
}
// @lc code=end
