# 题目

给你一个字符数组 `chars` ，请使用下述算法压缩：

从一个空字符串 `s` 开始。对于 `chars` 中的每组 连续重复字符 ：

- 如果这一组长度为 `1` ，则将字符追加到 `s` 中。
- 否则，需要向 `s` 追加字符，后跟这一组的长度。

压缩后得到的字符串 `s` 不应该直接返回 ，需要转储到字符数组 `chars` 中。需要注意的是，如果组长度为 `10` 或 `10` 以上，则在 `chars` 数组中会被拆分为多个字符。

请在 **修改完输入数组后** ，返回该数组的新长度。

你必须设计并实现一个只使用常量额外空间的算法来解决此问题。

提示：

- `1 <= chars.length <= 2000`
- `chars[i]` 可以是小写英文字母、大写英文字母、数字或符号

# 示例

```
输入：chars = ["a","a","b","b","c","c","c"]
输出：返回 6 ，输入数组的前 6 个字符应该是：["a","2","b","2","c","3"]
解释：
"aa" 被 "a2" 替代。"bb" 被 "b2" 替代。"ccc" 被 "c3" 替代。
```

```
输入：chars = ["a","b","b","b","b","b","b","b","b","b","b","b","b"]
输出：返回 4 ，输入数组的前 4 个字符应该是：["a","b","1","2"]。
解释：
由于字符 "a" 不重复，所以不会被压缩。"bbbbbbbbbbbb" 被 “b12” 替代。
注意每个数字在数组中都有它自己的位置。
```

# 题解

## 计数

遍历 chars，定义 currChar 和 currLen 统计当前字符和出现的次数。

当遍历到的元素不等于 currChar，表示连续字符结束，这个时候更新 chars。

否者，一直累加 currLen。

```ts
function compress(chars: string[]): number {
  let ans = 0;
  let currChar = '';
  let currLen = 0;
  let idx = 0;
  // 加入一个哨兵元素，方便处理最后一组
  chars.push(' ');
  for (const char of chars) {
    if (currChar === '') {
      // 第一次遍历
      currChar = char;
      currLen++;
    } else if (currChar !== char) {
      // 连续字符结束
      ans++;
      chars[idx++] = currChar;
      if (currLen > 1) {
        for (const c of String(currLen)) {
          chars[idx++] = c;
          ans++;
        }
      }
      currChar = char;
      currLen = 1;
    } else {
      // 连续字符计算中
      currLen++;
    }
  }

  return ans;
}
```
