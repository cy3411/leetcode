# 题目
给定一个整数 n，生成所有由 1 ... n 为节点所组成的 **二叉搜索树** 。

# 示例
```
输入：3
输出：
[
  [1,null,3,2],
  [3,2,null,1],
  [3,1,null,null,2],
  [2,1,3],
  [1,null,2,null,3]
]
解释：
以上的输出对应以下 5 种不同结构的二叉搜索树：

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
```

# 题解
## 模拟
二叉搜索树的性质就是左小右大。

枚举每个数字 `n` 当作根节点，递归 `n-1` 生成左子树，`n+1`生成右子树。

遍历左子树集合和右子树集合，组成二叉搜索树。

```js
function generateTrees(n: number): Array<TreeNode | null> {
  if (n === 0) return [];

  const dfs = (l: number, r: number): Array<TreeNode | null> => {
    const result = [];
    if (l > r) {
      result.push(null);
      return result;
    }
    for (let i = l; i <= r; i++) {
      const leftTree = dfs(l, i - 1);
      const rightTree = dfs(i + 1, r);

      for (let left of leftTree) {
        for (let right of rightTree) {
          const root = new TreeNode(i, left, right);
          result.push(root);
        }
      }
    }
    return result;
  };

  return dfs(1, n);
}
```