/*
 * @lc app=leetcode.cn id=899 lang=cpp
 *
 * [899] 有序队列
 */

// @lc code=start
class Solution {
public:
    string orderlyQueue(string s, int k) {
        if (k == 1) {
            string ans = s;
            int n = s.size();
            for (int i = 1; i < n; i++) {
                s = s.substr(1) + s[0];
                ans = ans < s ? ans : s;
            }
            return ans;
        }

        sort(s.begin(), s.end());
        return s;
    }
};
// @lc code=end
