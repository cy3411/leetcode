#
# @lc app=leetcode.cn id=899 lang=python3
#
# [899] 有序队列
#

# @lc code=start
class Solution:
    def orderlyQueue(self, s: str, k: int) -> str:
        if k == 1:
            ans = s
            for i in range(len(s) - 1):
                s = s[1:] + s[0]
                ans = min(ans, s)
            return ans
        return ''.join(sorted(s))
# @lc code=end
