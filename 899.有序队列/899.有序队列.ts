/*
 * @lc app=leetcode.cn id=899 lang=typescript
 *
 * [899] 有序队列
 */

// @lc code=start
function orderlyQueue(s: string, k: number): string {
  // 有n种字符串，取字典序最小的
  if (k === 1) {
    let ans = s;
    const n = s.length;
    for (let i = 1; i < n; i++) {
      s = s.substring(1, n) + s[0];
      ans = ans < s ? ans : s;
    }
    return ans;
  }
  // 其他的肯定可以排列成升序
  return [...s].sort().join('');
}
// @lc code=end
