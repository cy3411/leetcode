# 题目

给定一个字符串 `s` 和一个整数 `k` 。你可以从 `s` 的前 `k` 个字母中选择一个，并把它加到字符串的末尾。

返回 _在应用上述步骤的任意数量的移动后，字典上最小的字符串_ 。

提示：

- $1 \leq k \leq S.length \leq 1000$
- `s` 只由小写字母组成。

# 示例

```
输入：s = "cba", k = 1
输出："acb"
解释：
在第一步中，我们将第一个字符（“c”）移动到最后，获得字符串 “bac”。
在第二步中，我们将第一个字符（“b”）移动到最后，获得最终结果 “acb”。
```

```
输入：s = "baaca", k = 3
输出："aaabc"
解释：
在第一步中，我们将第一个字符（“b”）移动到最后，获得字符串 “aacab”。
在第二步中，我们将第三个字符（“c”）移动到最后，获得最终结果 “aaabc”。
```

# 题解

## 分类处理

当 `k = 1` 的时候，每次只能将第一个字符移动到最后，所以只能形成 n 种字符串，取字典序最小的那个。

当 ` k >= 1` 的时候，可以构造出任意的字符串方案，所以可以直接排序来获取答案。

```ts
function orderlyQueue(s: string, k: number): string {
  // 有n种字符串，取字典序最小的
  if (k === 1) {
    let ans = s;
    const n = s.length;
    for (let i = 1; i < n; i++) {
      s = s.substring(1, n) + s[0];
      ans = ans < s ? ans : s;
    }
    return ans;
  }
  // 其他的肯定可以排列成升序
  return [...s].sort().join('');
}
```

```cpp
class Solution {
public:
    string orderlyQueue(string s, int k) {
        if (k == 1) {
            string ans = s;
            int n = s.size();
            for (int i = 1; i < n; i++) {
                s = s.substr(1) + s[0];
                ans = ans < s ? ans : s;
            }
            return ans;
        }

        sort(s.begin(), s.end());
        return s;
    }
};
```

```py
class Solution:
    def orderlyQueue(self, s: str, k: int) -> str:
        if k == 1:
            ans = s
            for i in range(len(s) - 1):
                s = s[1:] + s[0]
                ans = min(ans, s)
            return ans
        return ''.join(sorted(s))
```
