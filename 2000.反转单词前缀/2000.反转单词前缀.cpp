/*
 * @lc app=leetcode.cn id=2000 lang=cpp
 *
 * [2000] 反转单词前缀
 */

// @lc code=start
class Solution
{
public:
    string reversePrefix(string word, char ch)
    {
        int n = word.size();
        int i = 0;
        while (i < n && word[i] != ch)
        {
            i++;
        }

        if (i == n)
        {
            return word;
        }

        string res = word.substr(0, i + 1);
        reverse(res.begin(), res.end());
        return res + word.substr(i + 1);
    }
};
// @lc code=end
