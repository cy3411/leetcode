/*
 * @lc app=leetcode.cn id=2000 lang=typescript
 *
 * [2000] 反转单词前缀
 */

// @lc code=start
function reversePrefix(word: string, ch: string): string {
  const n = word.length;
  // 找到第一个等于ch的位置，就是反转点
  let i = 0;
  while (i < n && word[i] !== ch) {
    i++;
  }
  // 没找到反转点，直接返回原字符串
  if (i == n) return word;

  let ans = '';
  // 反转前缀
  let j = i;
  while (j >= 0) {
    ans += word[j--];
  }
  // 将剩余的字符串拼接到反转后的字符串后面
  let k = i + 1;
  while (k < n) {
    ans += word[k++];
  }

  return ans;
}
// @lc code=end
