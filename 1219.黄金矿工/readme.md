# 题目

你要开发一座金矿，地质勘测学家已经探明了这座金矿中的资源分布，并用大小为 $\color{burlywood}m * n$ 的网格 `grid` 进行了标注。每个单元格中的整数就表示这一单元格中的黄金数量；如果该单元格是空的，那么就是 `0`。

为了使收益最大化，矿工需要按以下规则来开采黄金：

- 每当矿工进入一个单元，就会收集该单元格中的所有黄金。
- 矿工每次可以从当前位置向上下左右四个方向走。
- 每个单元格只能被开采（进入）一次。
- **不得开采**（进入）黄金数目为 `0` 的单元格。
- 矿工可以从网格中 **任意一个** 有黄金的单元格出发或者是停止。

提示：

- $\color{burlywood}1 \leq grid.length, grid[i].length \leq 15$
- $\color{burlywood}0 \leq grid[i][j] \leq 100$
- 最多 `25` 个单元格中有黄金。

# 示例

```
输入：grid = [[0,6,0],[5,8,7],[0,9,0]]
输出：24
解释：
[[0,6,0],
 [5,8,7],
 [0,9,0]]
一种收集最多黄金的路线是：9 -> 8 -> 7。
```

```
输入：grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
输出：28
解释：
[[1,0,7],
 [2,0,6],
 [3,4,5],
 [0,3,0],
 [9,0,20]]
一种收集最多黄金的路线是：1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7。
```

# 题解

## 回溯

我们在 $\color{burlywood} m * n$ 个网格内枚举起始点。只要格子中数大于 `0`，就可以做为起始点。

定义枚举的起点是 `(i,j)`，我们就从`(i,j)`开始进行递归回溯，枚举所有可行的开采路径。

题目规定每个单元格只能开采一次，因此我们需要记录已经开采过的单元格。因此我们到达`(x,y)`时，将 `grid[x][y]`设置为 `0`，表示已经开采过。在进行回溯之前，再将 `grid[x][y]`设置为原来的值。

```ts
function getMaximumGold(grid: number[][]): number {
  const m = grid.length;
  const n = grid[0].length;
  const dirs = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
  ];
  let ans = 0;
  const dfs = (i: number, j: number, gold: number): void => {
    gold += grid[i][j];
    ans = Math.max(ans, gold);
    const rec = grid[i][j];
    grid[i][j] = 0;
    for (const [x, y] of dirs) {
      const nx = i + x;
      const ny = j + y;
      if (nx < 0 || nx >= m) continue;
      if (ny < 0 || ny >= n) continue;
      if (grid[nx][ny] === 0) continue;
      dfs(nx, ny, gold);
    }
    grid[i][j] = rec;
  };

  // 枚举每一个起始点
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] == 0) continue;
      dfs(i, j, 0);
    }
  }

  return ans;
}
```

```cpp
class Solution
{
    int ans = 0;
    int dirs[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

public:
    void dfs(int i, int j, vector<vector<int>> &grid, int golds)
    {
        golds += grid[i][j];
        ans = max(ans, golds);

        int rec = grid[i][j];
        grid[i][j] = 0;

        for (auto [x, y] : dirs)
        {
            int nx = x + i, ny = y + j;
            if (nx < 0 || nx >= grid.size() || ny < 0 || ny >= grid[0].size() || grid[nx][ny] == 0)
            {
                continue;
            }
            dfs(nx, ny, grid, golds);
        }

        grid[i][j] = rec;
    }
    int getMaximumGold(vector<vector<int>> &grid)
    {
        int m = grid.size(), n = grid[0].size();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                dfs(i, j, grid, 0);
            }
        }

        return ans;
    }
};
```
