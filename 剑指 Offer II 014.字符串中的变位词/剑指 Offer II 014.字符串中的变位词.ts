// @algorithm @lc id=1000250 lang=typescript
// @title MPnaiL
// @test("adc","dcda")=true
function checkInclusion(s1: string, s2: string): boolean {
  // s2要包含s1，所以s1的长度必须小于等于s2
  if (s1.length > s2.length) return false;

  // 初始化字符数组
  const s1Chars: number[] = new Array(26).fill(0);
  const s2Chars: number[] = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  for (let i = 0; i < s1.length; i++) {
    s1Chars[s1.charCodeAt(i) - base]++;
    s2Chars[s2.charCodeAt(i) - base]++;
  }

  // 计算不同的字符数量
  let diff = 0;
  for (let i = 0; i < 26; i++) {
    if (s1Chars[i] !== s2Chars[i]) {
      diff++;
    }
  }
  // 滑动窗口遍历数组
  const s1Len = s1.length;
  for (let i = s1Len; i < s2.length; i++) {
    if (diff === 0) break;
    // 新加入一个字符
    if (s1Chars[s2.charCodeAt(i) - base] === s2Chars[s2.charCodeAt(i) - base]) {
      diff++;
    }
    s2Chars[s2.charCodeAt(i) - base]++;
    if (s1Chars[s2.charCodeAt(i) - base] === s2Chars[s2.charCodeAt(i) - base]) {
      diff--;
    }

    // 删除区间第一个字符
    if (s1Chars[s2.charCodeAt(i - s1Len) - base] === s2Chars[s2.charCodeAt(i - s1Len) - base]) {
      diff++;
    }
    s2Chars[s2.charCodeAt(i - s1Len) - base]--;
    if (s1Chars[s2.charCodeAt(i - s1Len) - base] === s2Chars[s2.charCodeAt(i - s1Len) - base]) {
      diff--;
    }
  }

  return diff === 0;
}
