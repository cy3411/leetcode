# 题目

给定两个字符串 `s1` 和 `s2`，写一个函数来判断 `s2` 是否包含 `s1` 的某个变位词。

换句话说，第一个字符串的排列之一是第二个字符串的 **子串** 。

提示：

- $1 \leq s1.length, s2.length \leq 10^4$
- `s1` 和 `s2` 仅包含小写字母

注意：[本题与主站 567 题相同](https://leetcode-cn.com/problems/permutation-in-string/)

# 示例

```
输入: s1 = "ab" s2 = "eidbaooo"
输出: True
解释: s2 包含 s1 的排列之一 ("ba").
```

```
输入: s1= "ab" s2 = "eidboaoo"
输出: False
```

# 题解

## 滑动窗口

控制 `s1` 长度的滑动窗口，每次一进一出一个字符，记作 `x` 和 `y`。

使用数组 `s1Chars` 和 `s2Chars` 记录 `s1` 和 `s2` 窗口内字符的个数，然后记录两者之间不同的字符数量 `diff`。

每次移动窗口，在修改 $s2Chars[x]$ 之前， 若 $s1Chars[x] == s2Chars[x], 则diff加1$；修改 $s2Chars[x]$ 之后，若 $s1Chars[x] == s2Chars[x] + 1$, 则 diff 减 1$。 `y` 同理。

最后返回 `diff` 是否为 `0`。

有一个优化的地方，如果进出的字符相等，可以跳过本次循环。

```ts
function checkInclusion(s1: string, s2: string): boolean {
  // s2要包含s1，所以s1的长度必须小于等于s2
  if (s1.length > s2.length) return false;

  // 初始化字符数组
  const s1Chars: number[] = new Array(26).fill(0);
  const s2Chars: number[] = new Array(26).fill(0);
  const base = 'a'.charCodeAt(0);
  for (let i = 0; i < s1.length; i++) {
    s1Chars[s1.charCodeAt(i) - base]++;
    s2Chars[s2.charCodeAt(i) - base]++;
  }

  // 计算不同的字符数量
  let diff = 0;
  for (let i = 0; i < 26; i++) {
    if (s1Chars[i] !== s2Chars[i]) {
      diff++;
    }
  }
  // 滑动窗口遍历数组
  const s1Len = s1.length;
  for (let i = s1Len; i < s2.length; i++) {
    if (diff === 0) break;
    // 新加入一个字符
    if (s1Chars[s2.charCodeAt(i) - base] === s2Chars[s2.charCodeAt(i) - base]) {
      diff++;
    }
    s2Chars[s2.charCodeAt(i) - base]++;
    if (s1Chars[s2.charCodeAt(i) - base] === s2Chars[s2.charCodeAt(i) - base]) {
      diff--;
    }

    // 删除区间第一个字符
    if (s1Chars[s2.charCodeAt(i - s1Len) - base] === s2Chars[s2.charCodeAt(i - s1Len) - base]) {
      diff++;
    }
    s2Chars[s2.charCodeAt(i - s1Len) - base]--;
    if (s1Chars[s2.charCodeAt(i - s1Len) - base] === s2Chars[s2.charCodeAt(i - s1Len) - base]) {
      diff--;
    }
  }

  return diff === 0;
}
```

```cpp
class Solution
{
public:
    bool checkInclusion(string s1, string s2)
    {
        int s1Len = s1.size(), s2Len = s2.size();
        if (s1Len > s2Len)
        {
            return false;
        }
        int s1Chars[26] = {0}, s2Chars[26] = {0};
        for (int i = 0; i < s1Len; i++)
        {
            s1Chars[s1[i] - 'a']++;
            s2Chars[s2[i] - 'a']++;
        }
        int diff = 0;
        for (int i = 0; i < 26; i++)
        {
            if (s1Chars[i] != s2Chars[i])
            {
                diff++;
            }
        }
        cout << diff << endl;
        for (int i = s1Len; i < s2Len; i++)
        {
            if (diff == 0)
                break;
            int idx = s2[i] - 'a';
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff++;
            }
            s2Chars[idx]++;
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff--;
            }
            idx = s2[i - s1Len] - 'a';
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff++;
            }
            s2Chars[idx]--;
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff--;
            }
        }
        return diff == 0;
    }
};
```
