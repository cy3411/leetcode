class Solution
{
public:
    bool checkInclusion(string s1, string s2)
    {
        int s1Len = s1.size(), s2Len = s2.size();
        if (s1Len > s2Len)
        {
            return false;
        }
        int s1Chars[26] = {0}, s2Chars[26] = {0};
        for (int i = 0; i < s1Len; i++)
        {
            s1Chars[s1[i] - 'a']++;
            s2Chars[s2[i] - 'a']++;
        }
        int diff = 0;
        for (int i = 0; i < 26; i++)
        {
            if (s1Chars[i] != s2Chars[i])
            {
                diff++;
            }
        }
        cout << diff << endl;
        for (int i = s1Len; i < s2Len; i++)
        {
            if (diff == 0)
                break;
            int idx = s2[i] - 'a';
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff++;
            }
            s2Chars[idx]++;
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff--;
            }
            idx = s2[i - s1Len] - 'a';
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff++;
            }
            s2Chars[idx]--;
            if (s1Chars[idx] == s2Chars[idx])
            {
                diff--;
            }
        }
        return diff == 0;
    }
};