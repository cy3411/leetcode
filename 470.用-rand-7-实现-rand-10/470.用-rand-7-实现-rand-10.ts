/*
 * @lc app=leetcode.cn id=470 lang=typescript
 *
 * [470] 用 Rand7() 实现 Rand10()
 */

// @lc code=start
/**
 * The rand7() API is already defined for you.
 * function rand7(): number {}
 * @return a random integer in the range 1 to 7
 */

function rand10(): number {
  while (true) {
    let num: number = rand7();
    // 1-49
    num = (num - 1) * 7 + rand7();
    if (num <= 40) return (num % 10) + 1;
    // 1-63
    num = (num - 40 - 1) * 7 + rand7();
    if (num <= 60) return (num % 10) + 1;
    // 1-21
    num = (num - 60 - 1) * 7 + rand7();
    if (num <= 20) return (num % 10) + 1;
  }
}
// @lc code=end
