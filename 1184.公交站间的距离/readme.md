# 题目

环形公交路线上有 `n` 个站，按次序从 `0` 到 `n - 1` 进行编号。我们已知每一对相邻公交站之间的距离，`distance[i]` 表示编号为 `i` 的车站和编号为 `(i + 1) % n` 的车站之间的距离。

环线上的公交车都可以按顺时针和逆时针的方向行驶。

返回乘客从出发点 `start` 到目的地 `destination` 之间的最短距离。

提示：

- $1 \leq n \leq 10^4$
- $distance.length \equiv n$
- $0 \leq start, destination < n$
- $0 \leq distance[i] \leq 10^4$

# 示例

[![jjyb8J.png](https://s1.ax1x.com/2022/07/24/jjyb8J.png)](https://imgtu.com/i/jjyb8J)

```
输入：distance = [1,2,3,4], start = 0, destination = 1
输出：1
解释：公交站 0 和 1 之间的距离是 1 或 9，最小值是 1。
```

[![jjyXK1.png](https://s1.ax1x.com/2022/07/24/jjyXK1.png)](https://imgtu.com/i/jjyXK1)

```
输入：distance = [1,2,3,4], start = 0, destination = 3
输出：4
解释：公交站 0 和 3 之间的距离是 6 或 4，最小值是 4。
```

# 题解

## 遍历

题意就是起点开始，顺指针到终点或者逆时针到终点，取最小的距离。

由于公交线路是一个环，那么 `start` 和 `destination` 之间的距离就是顺时针的距离，剩下其他的距离就是逆时
针的距离。

遍历 `distance`，如果 `i >= start && i < destination` 表示为起点和终点之间的距离，我们记录在 `sum1` 中。
其他条件下的距离记录在 `sum2` 中。

返回两者的最小值即可。

```ts
function distanceBetweenBusStops(distance: number[], start: number, destination: number): number {
  if (start > destination) {
    [start, destination] = [destination, start];
  }

  const n = distance.length;
  let sum1 = 0;
  let sum2 = 0;
  for (let i = 0; i < n; i++) {
    if (i >= start && i < destination) {
      // 在起点和终点之间
      sum1 += distance[i];
    } else {
      // 在起点和终点之外
      sum2 += distance[i];
    }
  }
  return Math.min(sum1, sum2);
}
```

```cpp
class Solution {
public:
    int distanceBetweenBusStops(vector<int> &distance, int start,
                                int destination) {
        if (start > destination) {
            swap(start, destination);
        }

        int n = distance.size(), sum1 = 0, sum2 = 0;

        for (int i = 0; i < n; i++) {
            if (i >= start && i < destination) {
                sum1 += distance[i];
            } else {
                sum2 += distance[i];
            }
        }

        return min(sum1, sum2);
    }
};
```

```py
class Solution:
    def distanceBetweenBusStops(self, distance: List[int], start: int, destination: int) -> int:
        if start > destination:
            start, destination = destination, start
        return min(sum(distance[start:destination]), sum(distance[:start]) + sum(distance[destination:]))
```
