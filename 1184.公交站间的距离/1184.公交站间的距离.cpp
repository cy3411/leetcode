/*
 * @lc app=leetcode.cn id=1184 lang=cpp
 *
 * [1184] 公交站间的距离
 */

// @lc code=start
class Solution {
public:
    int distanceBetweenBusStops(vector<int> &distance, int start,
                                int destination) {
        if (start > destination) {
            swap(start, destination);
        }

        int n = distance.size(), sum1 = 0, sum2 = 0;

        for (int i = 0; i < n; i++) {
            if (i >= start && i < destination) {
                sum1 += distance[i];
            } else {
                sum2 += distance[i];
            }
        }

        return min(sum1, sum2);
    }
};
// @lc code=end
