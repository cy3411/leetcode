/*
 * @lc app=leetcode.cn id=1184 lang=typescript
 *
 * [1184] 公交站间的距离
 */

// @lc code=start
function distanceBetweenBusStops(distance: number[], start: number, destination: number): number {
  if (start > destination) {
    [start, destination] = [destination, start];
  }

  const n = distance.length;
  let sum1 = 0;
  let sum2 = 0;
  for (let i = 0; i < n; i++) {
    if (i >= start && i < destination) {
      // 在起点和终点之间
      sum1 += distance[i];
    } else {
      // 在起点和终点之外
      sum2 += distance[i];
    }
  }
  return Math.min(sum1, sum2);
}
// @lc code=end
