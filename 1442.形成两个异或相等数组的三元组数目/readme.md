# 题目
给你一个整数数组 `arr` 。

现需要从数组中取三个下标 `i`、`j` 和 `k` ，其中 `(0 <= i < j <= k < arr.length)` 。

`a` 和 `b` 定义如下：

`a = arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1]`
`b = arr[j] ^ arr[j + 1] ^ ... ^ arr[k]`
注意：^ 表示 按位异或 操作。

请返回能够令 `a == b` 成立的三元组 `(i, j , k)` 的数目。

# 示例
```
输入：arr = [2,3,1,6,7]
输出：4
解释：满足题意的三元组分别是 (0,1,2), (0,2,2), (2,3,4) 以及 (2,4,4)
```

# 题解
计算异或前缀和数组。

对于两个不同下标异或前缀和$S_i$和$S_j$，设 `0<i<j` ，有
$$
S_i \oplus S_j = (arr0 \oplus arr1 \oplus … \oplus arri-1) \oplus (arr0 \oplus … \oplus arri-1 \oplus arri \oplus … \oplus arrj-1)
$$

异或满足结合律和交换律，任意异或自身元素等于0，上面可以简化：
$$
S_i \oplus S_j = arri \oplus … \oplus arrj-1
$$

所以，数组`arr`子区间`[i,j]`的异或和可以表示为：
$$
S_i \oplus S_{j+1}
$$

题目的`a`和`b`可以表示为：
$$
a = S_i \oplus S_{j} \\[2ex]
b = S_j \oplus S_k+1
$$

若`a=b`，则有：
$$
S_i \oplus S_{j} = S_j \oplus S_k+1 \\
\Downarrow \\
S_i = S_k+1
$$

所以我们只要求异或前缀和数组中上面等式的数量即可。

### 暴力
计算异或前缀和数组，枚举符合条件`0<=i<j<k<n`的`i,j,k`，统计满足等式的数量。
```ts
function countTriplets(arr: number[]): number {
  const size = arr.length;
  const preSum: number[] = new Array(size + 1).fill(0);
  // 计算异或前缀和
  for (let i = 1; i <= size; i++) {
    preSum[i] = arr[i - 1] ^ preSum[i - 1];
  }

  let result = 0;
  // 0<=i<j<k<size
  for (let i = 0; i < size; i++) {
    for (let j = i + 1; j < size; j++) {
      for (let k = j; k < size; k++) {
        if (preSum[i] === preSum[k + 1]) result++;
      }
    }
  }
  return result;
}
```

当等式成立，`[i+1,k]`范围内的任何`j`都是成立的，所以我们只要枚举`i,k`即可，对应的三元数目是`k-i`
```ts
function countTriplets(arr: number[]): number {
  const size = arr.length;
  const preSum: number[] = new Array(size + 1).fill(0);
  // 计算异或前缀和
  for (let i = 1; i <= size; i++) {
    preSum[i] = arr[i - 1] ^ preSum[i - 1];
  }

  let result = 0;
  for (let i = 0; i < size; i++) {
    for (let k = i + 1; k < size; k++) {
      if (preSum[i] === preSum[k + 1]) {
        result += k - i;
      }
    }
  }
  return result;
}
```

## 哈希表
对于`k`，若`i=i1,i2,…,im`时均满足等式，则这些二元数组对应的答案是：
$$
(k-i1)+(k-i2)+…+(k-im) = m*k - (i1+i2+…+im)
$$

我们可以遍历`k`，记录下标`i` 出现的次数和下标`i`之和。

```ts
function countTriplets(arr: number[]): number {
  const size = arr.length;
  const preSum: number[] = new Array(size + 1).fill(0);
  // 计算异或前缀和
  for (let i = 1; i <= size; i++) {
    preSum[i] = arr[i - 1] ^ preSum[i - 1];
  }

  let result = 0;
  let count = new Map();
  let total = new Map();
  for (let k = 0; k < size; k++) {
    if (count.has(preSum[k + 1])) {
      result += count.get(preSum[k + 1]) * k - total.get(preSum[k + 1]);
    }
    // 相当于之前的i
    count.set(preSum[k], (count.get(preSum[k]) || 0) + 1);
    total.set(preSum[k], (total.get(preSum[k]) || 0) + k);
  }
  return result;
}
```