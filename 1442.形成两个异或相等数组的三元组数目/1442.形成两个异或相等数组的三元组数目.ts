/*
 * @lc app=leetcode.cn id=1442 lang=typescript
 *
 * [1442] 形成两个异或相等数组的三元组数目
 */

// @lc code=start
function countTriplets(arr: number[]): number {
  const size = arr.length;
  const preSum: number[] = new Array(size + 1).fill(0);
  // 计算异或前缀和
  for (let i = 1; i <= size; i++) {
    preSum[i] = arr[i - 1] ^ preSum[i - 1];
  }

  let result = 0;
  let count = new Map();
  let total = new Map();
  for (let k = 0; k < size; k++) {
    if (count.has(preSum[k + 1])) {
      result += count.get(preSum[k + 1]) * k - total.get(preSum[k + 1]);
    }
    // 相当于之前的i
    count.set(preSum[k], (count.get(preSum[k]) || 0) + 1);
    total.set(preSum[k], (total.get(preSum[k]) || 0) + k);
  }
  return result;
}
// @lc code=end
