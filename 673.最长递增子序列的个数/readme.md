# 题目

给定一个未排序的整数数组，找到最长递增子序列的个数。

注意: 给定的数组长度不超过 `2000` 并且结果一定是 `32` 位有符号整数。

# 示例

```
输入: [1,3,5,4,7]
输出: 2
解释: 有两个最长递增子序列，分别是 [1, 3, 4, 7] 和[1, 3, 5, 7]。
```

# 题解

## 动态规划

**状态**

定义 `dp[i]` ，`i` 字符为结尾的最长递增子序列长度
定义 `cnt[i]`, `i` 字符为结尾的最长递增子序列个数

**转移**

$$dp[i]=max(dp[j])+1, 0<=j<i, nums[j]<nums[i]$$

**边界**

$$dp[0]=1 \\ cnt[0]=1$$

```ts
function findNumberOfLIS(nums: number[]): number {
  const n = nums.length;
  // 以i字符为结尾最长递增子序列的长度
  const dp = new Array(n).fill(1);
  // 以i字符为结尾的最长递增子序列个数
  const cnt = new Array(n).fill(1);
  // 最长递增子序列的长度
  let max = 0;
  let ans = 0;

  for (let i = 0; i < n; i++) {
    for (let j = 0; j < i; j++) {
      // 在j后面形成更长的最长递增子序列
      if (nums[i] > nums[j]) {
        if (dp[j] + 1 > dp[i]) {
          dp[i] = dp[j] + 1;
          // 重置
          cnt[i] = cnt[j];
        } else if (dp[j] + 1 === dp[i]) {
          // 更新
          cnt[i] += cnt[j];
        }
      }
    }
    // 这里更新答案
    if (dp[i] > max) {
      // 重置
      max = dp[i];
      ans = cnt[i];
    } else if (dp[i] === max) {
      // 更新
      ans += cnt[i];
    }
  }

  return ans;
}
```
