/*
 * @lc app=leetcode.cn id=667 lang=typescript
 *
 * [667] 优美的排列 II
 */

// @lc code=start
function constructArray(n: number, k: number): number[] {
  const ans = new Array<number>(n);
  let idx = 0;
  // 计算前半部分
  for (let i = 1; i < n - k; i++) {
    ans[idx++] = i;
  }
  // 计算后半部分，对称位置互换
  for (let i = n - k, j = n; i <= j; i++, j--) {
    ans[idx++] = i;
    if (i !== j) {
      ans[idx++] = j;
    }
  }

  return ans;
}
// @lc code=end
