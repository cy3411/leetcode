# 题目

给你一棵二叉树，它的根为 root 。请你删除 1 条边，使二叉树分裂成两棵子树，且它们子树和的乘积尽可能大。

由于答案可能会很大，请你将结果对 10^9 + 7 取模后再返回。

提示：

- 每棵树最多有 50000 个节点，且至少有 2 个节点。
- 每个节点的值在 [1, 10000] 之间。

# 示例

[![f2DCu9.md.png](https://z3.ax1x.com/2021/08/15/f2DCu9.md.png)](https://imgtu.com/i/f2DCu9)

```
输入：root = [1,2,3,4,5,6]
输出：110
解释：删除红色的边，得到 2 棵子树，和分别为 11 和 10 。它们的乘积是 110 （11*10）
```

# 题解

## 递归

题意就是在一个二叉树中找 2 个累加值，使得它们的乘积最大。

要想乘积最大，那么 2 个数要最接近才可以。

我们可以递归求的树的累加值，然后获得平均值是多少。

在递归回溯的过程中，找到最接近平均数的值，另一个值也就能得到了。

```ts
function maxProduct(root: TreeNode | null): number {
  const dfs = (root: TreeNode | null): number => {
    if (root === null) return 0;
    let res = root.val + dfs(root.left) + dfs(root.right);
    // 找到最接近平均值的数，乘积最大
    if (Math.abs(res - avg) < Math.abs(ans - avg)) ans = res;
    return res;
  };
  let ans = 0;
  let avg = 0;
  // 第一次调用，获取累加值的数
  const total = dfs(root);
  avg = (total / 2) >> 0;
  ans = total;
  // 第二次调用，ans的值是最接近平均数的值
  dfs(root);

  return (ans * (total - ans)) % (10 ** 9 + 7);
}
```
