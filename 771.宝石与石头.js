/*
 * @lc app=leetcode.cn id=771 lang=javascript
 *
 * [771] 宝石与石头
 */

// @lc code=start
/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
var numJewelsInStones = function (J, S) {
  const hash = new Map()
  for (let c of J) {
    hash.set(c, 1)
  }
  let res = 0
  for (let c of S) {
    res += hash.has(c) ? 1 : 0
  }
  return res
};
// @lc code=end

