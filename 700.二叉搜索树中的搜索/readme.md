# 题目

给定二叉搜索树（BST）的根节点和一个值。 你需要在 BST 中找到节点值等于给定值的节点。 返回以该节点为根的子树。 如果节点不存在，则返回 NULL。

# 示例

例如，

```
给定二叉搜索树:

        4
       / \
      2   7
     / \
    1   3

和值: 2
```

你应该返回如下子树:

```
   2
  / \
 1   3
```

在上述示例中，如果要找的值是 5，但因为没有节点值为 5，我们应该返回 NULL。

# 题解

## 递归

利用二叉搜索树的特性，如果当前节点的值大于等于给定值，则在左子树中查找，否则在右子树中查找。

```ts
function searchBST(root: TreeNode | null, val: number): TreeNode | null {
  if (root === null) return null;
  if (root.val === val) return root;
  // 当前节点值比较大，则在左子树中查找
  if (root.val > val) return searchBST(root.left, val);
  // 当前节点值比较小，则在右子树中查找
  return searchBST(root.right, val);
}
```
