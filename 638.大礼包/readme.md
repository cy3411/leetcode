# 题目

在 LeetCode 商店中， 有 `n` 件在售的物品。每件物品都有对应的价格。然而，也有一些大礼包，每个大礼包以优惠的价格捆绑销售一组物品。

给你一个整数数组 `price` 表示物品价格，其中 `price[i]` 是第 `i` 件物品的价格。另有一个整数数组 `needs` 表示购物清单，其中 `needs[i]` 是需要购买第 `i` 件物品的数量。

还有一个数组 `special` 表示大礼包，`special[i]` 的长度为 `n + 1` ，其中 `special[i][j]` 表示第 `i` 个大礼包中内含第 `j` 件物品的数量，且 `special[i][n]` （也就是数组中的最后一个整数）为第 `i` 个大礼包的价格。

返回 **确切** 满足购物清单所需花费的最低价格，你可以充分利用大礼包的优惠活动。你不能购买超出购物清单指定数量的物品，即使那样会降低整体价格。任意大礼包可无限次购买。

提示：

- `n == price.length`
- `n == needs.length`
- `1 <= n <= 6`
- `0 <= price[i] <= 10`
- `0 <= needs[i] <= 10`
- `1 <= special.length <= 100`
- `special[i].length == n + 1`
- `0 <= special[i][j] <= 50`

# 示例

```
输入：price = [2,5], special = [[3,0,5],[1,2,10]], needs = [3,2]
输出：14
解释：有 A 和 B 两种物品，价格分别为 ¥2 和 ¥5 。
大礼包 1 ，你可以以 ¥5 的价格购买 3A 和 0B 。
大礼包 2 ，你可以以 ¥10 的价格购买 1A 和 2B 。
需要购买 3 个 A 和 2 个 B ， 所以付 ¥10 购买 1A 和 2B（大礼包 2），以及 ¥4 购买 2A 。
```

# 题解

## 深度优先+记忆化搜索

```ts
function shoppingOffers(price: number[], special: number[][], needs: number[]): number {
  const map: Map<number[], number> = new Map();
  const dfs = (price: number[], special: number[][], currNeeds: number[], n: number): number => {
    if (!map.has(currNeeds)) {
      let minPrice = 0;
      // 按照单价预处理最小价格
      for (let i = 0; i < n; i++) {
        minPrice += currNeeds[i] * price[i];
      }
      // 处理是否可以购买礼包
      for (const sp of special) {
        //
        const nextNeeds = [];
        const specialPrice = sp[n];
        for (let i = 0; i < n; i++) {
          // 礼包内物品数量超过清单的物品数量，无法购买礼包
          if (sp[i] > currNeeds[i]) break;
          // 将剩下的物品数量重新列一个清单
          nextNeeds.push(currNeeds[i] - sp[i]);
          // 礼包内的物品数量都符合清单物品数量
          if (nextNeeds.length === n) {
            // 将新的清单递归
            minPrice = Math.min(minPrice, dfs(price, special, nextNeeds, n) + specialPrice);
          }
        }
      }
      map.set(currNeeds, minPrice);
    }
    return map.get(currNeeds);
  };
  const n = price.length;
  // 有效的礼包
  const filterSpecial = [];
  // 过滤掉无效的礼包
  for (const sp of special) {
    let totalCount = 0;
    let totalPrice = 0;
    for (let i = 0; i < n; i++) {
      totalCount += sp[i];
      totalPrice += sp[i] * price[i];
    }
    // 礼包内有物品且礼包价格要小于正常价格才是优惠礼包
    if (totalCount > 0 && totalPrice > sp[n]) {
      filterSpecial.push(sp);
    }
  }

  return dfs(price, filterSpecial, needs, n);
}
```
