/*
 * @lc app=leetcode.cn id=1232 lang=javascript
 *
 * [1232] 缀点成线
 */

// @lc code=start
/**
 * @param {number[][]} coordinates
 * @return {boolean}
 */
var checkStraightLine = function (coordinates) {
  const size = coordinates.length;

  // 两点式，(y-y1)/(y2-y1) = (x-x1)/(x2-x1)
  for (let i = 1; i < size - 1; i++) {
    const [x1, y1] = coordinates[i - 1];
    const [x, y] = coordinates[i];
    const [x2, y2] = coordinates[i + 1];

    if ((y - y1) * (x2 - x1) !== (x - x1) * (y2 - y1)) {
      return false;
    }
  }
  return true;
};
// @lc code=end
