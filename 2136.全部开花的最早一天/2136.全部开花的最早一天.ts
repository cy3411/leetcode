/*
 * @lc app=leetcode.cn id=2136 lang=typescript
 *
 * [2136] 全部开花的最早一天
 */

// @lc code=start
function earliestFullBloom(plantTime: number[], growTime: number[]): number {
  const n = plantTime.length;
  // 下标排序使用
  const idxs: number[] = new Array(n).fill(0).map((_, i) => i);
  idxs.sort((a, b) => growTime[b] - growTime[a]);

  let ans = -1;
  // 累计每个花的种植时间，因为每个花种植完了才可以种植下一个，总时间需要累加
  let sumPlantTime = 0;
  for (let i = 0; i < n; i++) {
    const idx = idxs[i];
    sumPlantTime += plantTime[idx];
    ans = Math.max(ans, sumPlantTime + growTime[idx]);
  }

  return ans;
}
// @lc code=end
