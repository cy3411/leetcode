# 题目

你有 `n` 枚花的种子。每枚种子必须先种下，才能开始生长、开花。播种需要时间，种子的生长也是如此。给你两个下标从 **0** 开始的整数数组 `plantTime` 和 `growTime` ，每个数组的长度都是 `n` ：

- `plantTime[i]` 是 **播种** 第 `i` 枚种子所需的 **完整天数** 。每天，你只能为播种某一枚种子而劳作。**无须** 连续几天都在种同一枚种子，但是种子播种必须在你工作的天数达到 `plantTime[i]` 之后才算完成。
- `growTime[i]` 是第 `i` 枚种子完全种下后生长所需的 **完整天数** 。在它生长的最后一天 之后 ，将会开花并且永远 **绽放** 。

从第 `0` 开始，你可以按 **任意** 顺序播种种子。

返回所有种子都开花的 **最早** 一天是第几天。

提示：

- $\color{burlywood} n == plantTime.length == growTime.length$
- $\color{burlywood} 1 \leq n \leq 10^5$
- $\color{burlywood} 1 \leq plantTime[i], growTime[i] \leq 10^4$

# 示例

[![Hlucgs.png](https://s4.ax1x.com/2022/02/07/Hlucgs.png)](https://imgtu.com/i/Hlucgs)

```
输入：plantTime = [1,4,3], growTime = [2,3,1]
输出：9
解释：灰色的花盆表示播种的日子，彩色的花盆表示生长的日子，花朵表示开花的日子。
一种最优方案是：
第 0 天，播种第 0 枚种子，种子生长 2 整天。并在第 3 天开花。
第 1、2、3、4 天，播种第 1 枚种子。种子生长 3 整天，并在第 8 天开花。
第 5、6、7 天，播种第 2 枚种子。种子生长 1 整天，并在第 9 天开花。
因此，在第 9 天，所有种子都开花。
```

[![HluWD0.png](https://s4.ax1x.com/2022/02/07/HluWD0.png)](https://imgtu.com/i/HluWD0)

```
输入：plantTime = [1,2,3,2], growTime = [2,1,2,1]
输出：9
解释：灰色的花盆表示播种的日子，彩色的花盆表示生长的日子，花朵表示开花的日子。
一种最优方案是：
第 1 天，播种第 0 枚种子，种子生长 2 整天。并在第 4 天开花。
第 0、3 天，播种第 1 枚种子。种子生长 1 整天，并在第 5 天开花。
第 2、4、5 天，播种第 2 枚种子。种子生长 2 整天，并在第 8 天开花。
第 6、7 天，播种第 3 枚种子。种子生长 1 整天，并在第 9 天开花。
因此，在第 9 天，所有种子都开花。
```

# 题解

## 排序

根据题意我们需要找到最优的种花顺序，那么什么样的顺序才是最短的呢？

定义 (a,b) 为两枚种子，$p_a$ 为第 a 枚种子播种的天数，$p_b$ 为第 b 枚种子播种的天数，$g_a$ 为第 a 枚种子生长的天数，$g_b$ 为第 b 枚种子生长的天数。假设 $g_a \geq g_b$,种植情况如下：

$$
\begin{aligned}
    max(p_a+g_a, \quad p_a+p_b+g_b),& \text{先种植 a，再种植 b} \\
    max(p_b+g_b, \quad p_b+p_a+g_a),& \text{先种植 b，再种植 a}
\end{aligned}
$$

从上面可以看出，在$g_a \geq g_b$的情况下，先种植 a 的方案更优。因为先种植 a 的方案所需要的时间 $max(p_a+g_a, \quad p_a+p_b+g_b)$ 总是小于先种植 b 的方案所需要的时间 $(p_b+p_a+g_a)$。

综上所述，我们将种子按照 growTime 从大到小排序，然后计算答案即可。

```ts
function earliestFullBloom(plantTime: number[], growTime: number[]): number {
  const n = plantTime.length;
  // 下标排序使用
  const idxs: number[] = new Array(n).fill(0).map((_, i) => i);
  idxs.sort((a, b) => growTime[b] - growTime[a]);

  let ans = -1;
  // 累计每个花的种植时间，因为每个花种植完了才可以种植下一个，总时间需要累加
  let sumPlantTime = 0;
  for (let i = 0; i < n; i++) {
    const idx = idxs[i];
    sumPlantTime += plantTime[idx];
    ans = Math.max(ans, sumPlantTime + growTime[idx]);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int earliestFullBloom(vector<int> &plantTime, vector<int> &growTime)
    {
        int n = plantTime.size();
        vector<int> idxs(n);
        for (int i = 0; i < n; i++)
        {

            idxs[i] = i;
        }
        sort(idxs.begin(), idxs.end(), [&](int i, int j) -> bool
             { return growTime[i] > growTime[j]; });

        int ans = -1, sum = 0;
        for (int i = 0; i < n; i++)
        {
            int idx = idxs[i];
            sum += plantTime[idx];
            ans = max(ans, sum + growTime[idx]);
        }

        return ans;
    }
};
```
