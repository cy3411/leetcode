/*
 * @lc app=leetcode.cn id=2136 lang=cpp
 *
 * [2136] 全部开花的最早一天
 */

// @lc code=start
class Solution
{
public:
    int earliestFullBloom(vector<int> &plantTime, vector<int> &growTime)
    {
        int n = plantTime.size();
        vector<int> idxs(n);
        for (int i = 0; i < n; i++)
        {

            idxs[i] = i;
        }
        sort(idxs.begin(), idxs.end(), [&](int i, int j) -> bool
             { return growTime[i] > growTime[j]; });

        int ans = -1, sum = 0;
        for (int i = 0; i < n; i++)
        {
            int idx = idxs[i];
            sum += plantTime[idx];
            ans = max(ans, sum + growTime[idx]);
        }

        return ans;
    }
};
// @lc code=end
