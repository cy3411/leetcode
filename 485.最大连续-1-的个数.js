/*
 * @lc app=leetcode.cn id=485 lang=javascript
 *
 * [485] 最大连续1的个数
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxConsecutiveOnes = function (nums) {
  const size = nums.length;
  let j = 0;
  let result = 0;
  let count = 0;
  while (j < size) {
    if (nums[j] === 0) {
      count = 0;
    } else {
      count++;
    }
    result = Math.max(result, count);
    j++;
  }
  return result;
};
// @lc code=end
