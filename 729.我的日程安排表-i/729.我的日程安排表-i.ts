/*
 * @lc app=leetcode.cn id=729 lang=typescript
 *
 * [729] 我的日程安排表 I
 */

// @lc code=start
class MyCalendar {
  books: number[][] = [];
  constructor() {}

  book(start: number, end: number): boolean {
    const n = this.books.length;
    for (const [l, r] of this.books) {
      if (start < r && l < end) {
        return false;
      }
    }
    this.books.push([start, end]);
    return true;
  }
}

/**
 * Your MyCalendar object will be instantiated and called as such:
 * var obj = new MyCalendar()
 * var param_1 = obj.book(start,end)
 */
// @lc code=end
