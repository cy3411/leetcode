# 题目

给定一个有 `N` 个结点的二叉树的根结点 `root`，树中的每个结点上都对应有 `node.val` 枚硬币，并且总共有 `N` 枚硬币。

在一次移动中，我们可以选择两个相邻的结点，然后将一枚硬币从其中一个结点移动到另一个结点。(移动可以是从父结点到子结点，或者从子结点移动到父结点。)。

返回使每个结点上只有一枚硬币所需的移动次数。

# 示例

[![2bNd4H.png](https://z3.ax1x.com/2021/06/15/2bNd4H.png)](https://imgtu.com/i/2bNd4H)

```
输入：[3,0,0]
输出：2
解释：从树的根结点开始，我们将一枚硬币移到它的左子结点上，一枚硬币移到它的右子结点上。
```

# 题解

## 递归

从题目可以看出，不管硬币怎么移动，最终还是要通过父节点分发给子节点或者从子节点拿取走多余的。这里可以看出，硬币移动的次数跟经过父节点的硬币数有关。

后序遍历，将子节点剩余的金币向上返回，剩余金币的数量就是移动次数。

```ts
function distributeCoins(root: TreeNode | null): number {
  // 后序遍历，保证先处理每个子节点
  const dfs = (root: TreeNode | null) => {
    if (root === null) return 0;
    let l = dfs(root.left);
    let r = dfs(root.right);
    // 负数表示从父节点取金币，正数表示移动金币给父节点，都算移动次数，所以要取绝对值
    ans += Math.abs(l) + Math.abs(r);
    // 返回当前节点上剩余几个金币
    return root.val + l + r - 1;
  };

  let ans = 0;

  dfs(root);

  return ans;
}
```
