// @algorithm @lc id=1000246 lang=typescript
// @test([1],1)=1
// @title QTMn0o
function subarraySum(nums: number[], k: number): number {
  const n = nums.length;
  const presum = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    presum[i] = presum[i - 1] + nums[i - 1];
  }
  let ans = 0;
  // 记录之前的数字出现的次数
  const map = new Map<number, number>();
  for (let i = 0; i <= n; i++) {
    const key = presum[i] - k;
    if (map.has(key)) {
      ans += map.get(key);
    }
    map.set(presum[i], (map.get(presum[i]) || 0) + 1);
  }

  return ans;
}
