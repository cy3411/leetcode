# 题目

给定一个整数数组和一个整数 `k` ，请找到该数组中和为 `k` 的连续子数组的个数。

提示:

- $1 \leq nums.length \leq 2 * 10^4$
- $-1000 \leq nums[i] \leq 1000$
- $-10^7 \leq k \leq 10^7$

注意：本题与主站 560 题相同： https://leetcode-cn.com/problems/subarray-sum-equals-k/

# 示例

```ts
输入:nums = [1,1,1], k = 2
输出: 2
解释: 此题 [1,1] 与 [1,1] 为两种不同的情况
```

```
输入:nums = [1,2,3], k = 3
输出: 2
```

# 题解

## 前缀和

```ts
function subarraySum(nums: number[], k: number): number {
  const n = nums.length;
  const presum = new Array(n + 1).fill(0);
  for (let i = 1; i <= n; i++) {
    presum[i] = presum[i - 1] + nums[i - 1];
  }
  let ans = 0;
  // 记录之前的数字出现的次数
  const map = new Map<number, number>();
  for (let i = 0; i <= n; i++) {
    const key = presum[i] - k;
    if (map.has(key)) {
      ans += map.get(key);
    }
    map.set(presum[i], (map.get(presum[i]) || 0) + 1);
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int subarraySum(vector<int> &nums, int k)
    {
        int n = nums.size();
        vector<int> preSum(n + 1, 0);
        int i;
        for (i = 1; i <= n; i++)
        {
            preSum[i] = preSum[i - 1] + nums[i - 1];
        }

        unordered_map<int, int> m;
        int ans = 0;
        for (i = 0; i <= n; i++)
        {
            int diff = preSum[i] - k;
            if (m.count(diff))
            {
                ans += m[diff];
            }
            m[preSum[i]] += 1;
        }
        return ans;
    }
};
```
