/*
 * @lc app=leetcode.cn id=567 lang=javascript
 *
 * [567] 字符串的排列
 */

// @lc code=start
/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var checkInclusion = function (s1, s2) {
  const m = s1.length;
  const n = s2.length;
  if (m > n) return false;

  const freg = new Array(26).fill(0);
  const base = 'a'.charCodeAt();

  for (let i = 0; i < n; i++) {
    freg[s1.charCodeAt(i) - base]--;
  }

  let i = 0;
  let j = 0;
  while (j < n) {
    freg[s2.charCodeAt(j) - base]++;
    while (freg[s2.charCodeAt(j) - base] > 0) {
      freg[s2.charCodeAt(i) - base]--;
      i++;
    }
    if (j - i + 1 === m) {
      return true;
    }
    j++;
  }

  return false;
};
// @lc code=end
