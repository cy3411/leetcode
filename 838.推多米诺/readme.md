# 题目

`n` 张多米诺骨牌排成一行，将每张多米诺骨牌垂直竖立。在开始时，同时把一些多米诺骨牌向左或向右推。

每过一秒，倒向左边的多米诺骨牌会推动其左侧相邻的多米诺骨牌。同样地，倒向右边的多米诺骨牌也会推动竖立在其右侧的相邻多米诺骨牌。

如果一张垂直竖立的多米诺骨牌的两侧同时有多米诺骨牌倒下时，由于受力平衡， 该骨牌仍然保持不变。

就这个问题而言，我们会认为一张正在倒下的多米诺骨牌不会对其它正在倒下或已经倒下的多米诺骨牌施加额外的力。

给你一个字符串 `dominoes` 表示这一行多米诺骨牌的初始状态，其中：

- `dominoes[i] = 'L'`，表示第 `i` 张多米诺骨牌被推向左侧，
- `dominoes[i] = 'R'`，表示第 `i` 张多米诺骨牌被推向右侧，
- `dominoes[i] = '.'`，表示没有推动第 `i` 张多米诺骨牌。

返回表示最终状态的字符串。

提示：

- $n \equiv dominoes.length$
- $1 \leq n \leq 105$
- `dominoes[i]` 为 `'L'`、`'R'` 或 `'.'`

# 示例

[![HxlF1g.png](https://s4.ax1x.com/2022/02/21/HxlF1g.png)](https://imgtu.com/i/HxlF1g)

```
输入：dominoes = ".L.R...LR..L.."
输出："LL.RR.LLRRLL.."
```

# 题解

## 广度优先搜索

根据题意，每个立着的骨牌，会被边上的骨牌推动从而改变状态，我们可以考虑使用广度优先搜索来解决这个问题。

使用队列 q 模拟搜索的顺序，使用 time 数组记录骨牌倒下的时间，force 数组记录骨牌受到的推力，骨牌只有受到单侧的推力才会到下。

初始将所有非站立的骨牌放入队列，然后遍历队列。取出队首元素，判断其影响的下一个骨牌，如果下一个骨牌是站立的，则更新其时间和推力，否则将其放入队列。否则表示下一个骨牌已经有一个推力了，则增加其推力数组，表示这个骨牌受到两侧推力影响，不会到下。

```ts
function pushDominoes(dominoes: string): string {
  const n = dominoes.length;
  // 广度优先搜索队列
  const queue: number[] = [];
  // 骨牌倒下的时间
  const time: number[] = new Array(n).fill(-1);
  // 骨牌受到哪个方向的推力
  const force: string[] = new Array(n).fill('');
  // 初始化
  for (let i = 0; i < n; i++) {
    if (dominoes[i] === '.') continue;
    queue.push(i);
    time[i] = 0;
    force[i] = dominoes[i];
  }

  // 计算答案
  const ans: string[] = new Array(n).fill('.');
  while (queue.length > 0) {
    const i = queue.shift()!;
    // 只有受到一个方向影响的骨牌才能推动
    if (force[i].length === 1) {
      const f = force[i][0];
      ans[i] = f;
      const ni = f === 'L' ? i - 1 : i + 1;
      // 索引是否合法
      if (ni >= 0 && ni < n) {
        const t = time[i];
        // 如果下一个骨牌没被计算过，则受到当前骨牌方向的影响
        // 加入队列，并更新其方向和倒下的时间
        if (time[ni] === -1) {
          queue.push(ni);
          time[ni] = t + 1;
          force[ni] += f;
        } else if (time[ni] === t + 1) {
          // 下一个骨已经有了一个推力，
          // 更新其推力方向
          force[ni] += f;
        }
      }
    }
  }

  return ans.join('');
}
```

```cpp
class Solution
{
public:
    string pushDominoes(string dominoes)
    {
        int n = dominoes.size();
        queue<int> q;
        vector<int> time(n, -1);
        vector<string> force(n);
        for (int i = 0; i < n; i++)
        {
            if (dominoes[i] == '.')
            {
                continue;
            }
            q.emplace(i);
            time[i] = 0;
            force[i] = dominoes[i];
        }

        string ans(n, '.');
        while (!q.empty())
        {
            int idx = q.front();
            q.pop();
            if (force[idx].size() == 1)
            {
                char f = force[idx][0];
                ans[idx] = f;
                int nIdx = f == 'L' ? idx - 1 : idx + 1;
                if (nIdx >= 0 && nIdx < n)
                {
                    int t = time[idx];
                    if (time[nIdx] == -1)
                    {
                        q.emplace(nIdx);
                        time[nIdx] = t + 1;
                        force[nIdx].push_back(f);
                    }
                    else if (time[nIdx] == t + 1)
                    {
                        force[nIdx].push_back(f);
                    }
                }
            }
        }
        return ans;
    }
};
```
