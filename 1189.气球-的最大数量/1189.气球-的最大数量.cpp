/*
 * @lc app=leetcode.cn id=1189 lang=cpp
 *
 * [1189] “气球” 的最大数量
 */

// @lc code=start
class Solution
{
public:
    int maxNumberOfBalloons(string text)
    {
        int cnt[26] = {0};
        for (char c : text)
        {
            cnt[c - 'a']++;
        }

        return min({cnt['b' - 'a'], cnt['a' - 'a'], cnt['l' - 'a'] / 2, cnt['o' - 'a'] / 2, cnt['n' - 'a']});
    }
};
// @lc code=end
