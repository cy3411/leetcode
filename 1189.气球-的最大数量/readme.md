# 题目

给你一个字符串 `text`，你需要使用 `text` 中的字母来拼凑尽可能多的单词 **"balloon"（气球）**。

字符串 `text` 中的每个字母最多只能被使用一次。请你返回最多可以拼凑出多少个单词 **"balloon"**。

提示：

- $\color{burlywood} 1 \leq text.length \leq 10^4$
- `text` 全部由小写英文字母组成

# 示例

[![Hrbzp4.png](https://s4.ax1x.com/2022/02/13/Hrbzp4.png)](https://imgtu.com/i/Hrbzp4)

```
输入：text = "nlaebolko"
输出：1
```

[![HrqAAK.png](https://s4.ax1x.com/2022/02/13/HrqAAK.png)](https://imgtu.com/i/HrqAAK)

```
输入：text = "loonbalxballpoon"
输出：2
```

# 题解

## 计数

统计 `balloon` 中的字符在 `text` 中出现的次数，然后取所有字符出现的最小次数，就是最大可以拼凑出多少个 `balloon`。

字符 `l` 和 `o` 每个单词需要 2 个，最后统计最小次数的时候，这里需要减半再做比较。

```ts
function maxNumberOfBalloons(text: string): number {
  let b = 0,
    a = 0,
    l = 0,
    o = 0,
    n = 0;
  for (const char of text) {
    switch (char) {
      case 'b':
        b++;
        break;
      case 'a':
        a++;
        break;
      case 'l':
        l++;
        break;
      case 'o':
        o++;
        break;
      case 'n':
        n++;
        break;
    }
  }

  return Math.min(b, a, (l / 2) >> 0, (o / 2) >> 0, n);
}
```

```cpp
class Solution
{
public:
    int maxNumberOfBalloons(string text)
    {
        int cnt[26] = {0};
        for (char c : text)
        {
            cnt[c - 'a']++;
        }

        return min({cnt['b' - 'a'], cnt['a' - 'a'], cnt['l' - 'a'] / 2, cnt['o' - 'a'] / 2, cnt['n' - 'a']});
    }
};
```
