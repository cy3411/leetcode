/*
 * @lc app=leetcode.cn id=1189 lang=typescript
 *
 * [1189] “气球” 的最大数量
 */

// @lc code=start
function maxNumberOfBalloons(text: string): number {
  let b = 0,
    a = 0,
    l = 0,
    o = 0,
    n = 0;
  for (const char of text) {
    switch (char) {
      case 'b':
        b++;
        break;
      case 'a':
        a++;
        break;
      case 'l':
        l++;
        break;
      case 'o':
        o++;
        break;
      case 'n':
        n++;
        break;
    }
  }

  return Math.min(b, a, (l / 2) >> 0, (o / 2) >> 0, n);
}
// @lc code=end
