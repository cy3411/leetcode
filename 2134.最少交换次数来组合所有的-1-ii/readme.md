# 题目

**交换** 定义为选中一个数组中的两个 **互不相同** 的位置并交换二者的值。

**环形** 数组是一个数组，可以认为 **第一个** 元素和 **最后一个** 元素 相邻 。

给你一个 **二进制环形** 数组 `nums` ，返回在 **任意位置** 将数组中的所有 `1` 聚集在一起需要的最少交换次数。

提示：

- $\color{burlywood}1 \leq nums.length \leq 10^5$
- `nums[i]` 为 `0` 或者 `1`

# 示例

```
输入：nums = [0,1,0,1,1,0,0]
输出：1
解释：这里列出一些能够将所有 1 聚集在一起的方案：
[0,0,1,1,1,0,0] 交换 1 次。
[0,1,1,1,0,0,0] 交换 1 次。
[1,1,0,0,0,0,1] 交换 2 次（利用数组的环形特性）。
无法在交换 0 次的情况下将数组中的所有 1 聚集在一起。
因此，需要的最少交换次数为 1 。
```

```
输入：nums = [0,1,1,1,0,0,1,1,0]
输出：2
解释：这里列出一些能够将所有 1 聚集在一起的方案：
[1,1,1,0,0,0,0,1,1] 交换 2 次（利用数组的环形特性）。
[1,1,1,1,1,0,0,0,0] 交换 2 次。
无法在交换 0 次或 1 次的情况下将数组中的所有 1 聚集在一起。
因此，需要的最少交换次数为 2 。
```

# 题解

## 滑动窗口

题意要求最后要把所有的 1 聚集在一起，那么我们可以定义一个滑动窗口，窗口的长度就是 1 个数量。

我们只需要在遍历的过程中，维护窗口内 0 的数量(也就是需要交换的次数)，取最小的结果即可。

窗口的右边界移动时候要注意对结果取模，模拟环形数组。

```ts
function minSwaps(nums: number[]): number {
  const n = nums.length;
  // 滑动窗口的大小
  let len = 0;
  for (const n of nums) {
    if (n === 1) len++;
  }
  // 记录窗口内 0 的数量，也就是需要交换的次数
  let zeros = 0;
  // 初始化窗口状态
  for (let i = 0; i < len; i++) {
    if (nums[i] === 0) zeros++;
  }

  // 窗口的左右边界
  let l = 0;
  let r = len - 1;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < n; i++) {
    // 每个窗口内 0 的数量就是需要交换的次数
    ans = Math.min(ans, zeros);
    // 窗口右移一位
    if (nums[l] === 0) {
      zeros--;
    }
    if (nums[(r + 1) % n] === 0) {
      zeros++;
    }
    l++;
    r++;
  }

  return ans;
}
```

```cpp
class Solution
{
public:
    int minSwaps(vector<int> &nums)
    {
        int len = 0;
        for (int x : nums)
        {
            if (x == 1)
            {

                len++;
            }
        }

        int zeros = 0;
        for (int i = 0; i < len; i++)
        {
            if (nums[i] == 0)
            {
                zeros++;
            }
        }

        int l = 0, r = len - 1, n = nums.size();
        int ans = INT_MAX;
        for (int i = 0; i < n; i++)
        {
            ans = min(ans, zeros);
            if (nums[l] == 0)
            {
                zeros--;
            }
            if (nums[(r + 1) % n] == 0)
            {
                zeros++;
            }
            l++, r++;
        }

        return ans;
    }
};
```
