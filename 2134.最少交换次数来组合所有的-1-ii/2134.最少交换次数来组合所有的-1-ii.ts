/*
 * @lc app=leetcode.cn id=2134 lang=typescript
 *
 * [2134] 最少交换次数来组合所有的 1 II
 */

// @lc code=start
function minSwaps(nums: number[]): number {
  const n = nums.length;
  // 滑动窗口的大小
  let len = 0;
  for (const n of nums) {
    if (n === 1) len++;
  }
  // 记录窗口内 0 的数量，也就是需要交换的次数
  let zeros = 0;
  // 初始化窗口状态
  for (let i = 0; i < len; i++) {
    if (nums[i] === 0) zeros++;
  }

  // 窗口的左右边界
  let l = 0;
  let r = len - 1;
  let ans = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < n; i++) {
    // 每个窗口内 0 的数量就是需要交换的次数
    ans = Math.min(ans, zeros);
    // 窗口右移一位
    if (nums[l] === 0) {
      zeros--;
    }
    if (nums[(r + 1) % n] === 0) {
      zeros++;
    }
    l++;
    r++;
  }

  return ans;
}
// @lc code=end
