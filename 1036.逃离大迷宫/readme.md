# 题目

在一个 $\color{burlywood}10^6 x 10^6$ 的网格中，每个网格上方格的坐标为 `(x, y)` 。

现在从源方格 `source = [sx, sy]` 开始出发，意图赶往目标方格 `target = [tx, ty]` 。数组 `blocked` 是封锁的方格列表，其中每个 `blocked[i] = [xi, yi]` 表示坐标为 `(xi, yi)` 的方格是禁止通行的。

每次移动，都可以走到网格中在四个方向上相邻的方格，只要该方格 **不** 在给出的封锁列表 `blocked` 上。同时，不允许走出网格。

只有在可以通过一系列的移动从源方格 `source` 到达目标方格 `target` 时才返回 `true`。否则，返回 `false`。

提示：

- $\color{burlywood}0 \leq blocked.length \leq 200$
- `blocked[i].length == 2`
- $\color{burlywood}0 \leq xi, yi < 10^6$
- `source.length == target.length == 2`
- $0 \leq sx, sy, tx, ty < 10^6$
- `source != target`
- 题目数据保证 `source` 和 `target` 不在封锁列表内

# 示例

```
输入：blocked = [[0,1],[1,0]], source = [0,0], target = [0,2]
输出：false
解释：
从源方格无法到达目标方格，因为我们无法在网格中移动。
无法向北或者向东移动是因为方格禁止通行。
无法向南或者向西移动是因为不能走出网格。
```

```
输入：blocked = [], source = [0,0], target = [999999,999999]
输出：true
解释：
因为没有方格被封锁，所以一定可以到达目标方格。
```

# 题解

## 广度优先搜索

从 source 开始广度优先搜索，如果搜索过程中到达了 target ，就返回 true ，否则返回 false 。

但是题目给出了 $\color{burlywood} 10^6*10^6$的矩阵，正常的广度优先搜索会超时。

所以我们需要在有限的步数内搜索，来判断是否能到达 target 。

我们考虑包围圈的最优方式，肯定是矩阵的两条对角线。那么从 source 开始搜索，最大步数就是一个上三角的格子数，如果超过这个数量，证明 source 不在最优的包围圈中。同理从 target 开始搜索也是。

如果 source 和 target 开始搜索都可以超出最优包围圈，那么这两点就是可达的。

我们可以根据 blocked 数组的长度来构建遍历最大次数。

```ts
function isEscapePossible(blocked: number[][], source: number[], target: number[]): boolean {
  const directions = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
  ];
  const N = 1e6;
  // 广度优先遍历的最大次数
  const M = blocked.length;
  //  最优包围圈是对角线，那么剩下的位置就是一个等差数列，上三角或者下三角
  const MAX = (M * (M - 1)) / 2;
  // block哈希表
  const blockedSet = new Set(blocked.map(([x, y]) => `${x},${y}`));

  const isValid = (s: number[], t: number[]) => {
    const visited: Set<string> = new Set();
    const queue: number[][] = [s];

    while (queue.length && visited.size <= MAX) {
      const [x, y] = queue.shift();
      if (x === t[0] && y === t[1]) return true;
      for (const [dx, dy] of directions) {
        const nextX = x + dx;
        const nextY = y + dy;
        if (nextX < 0 || nextX >= N) continue;
        if (nextY < 0 || nextY >= N) continue;
        if (blockedSet.has(`${nextX},${nextY}`)) continue;
        if (visited.has(`${nextX},${nextY}`)) continue;
        visited.add(`${nextX},${nextY}`);
        queue.push([nextX, nextY]);
      }
    }

    return visited.size > MAX;
  };

  // 判断是否可以从source到target
  // 判断是否可以从target到source
  return isValid(source, target) && isValid(target, source);
}
```
