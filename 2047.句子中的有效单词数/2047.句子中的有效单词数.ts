/*
 * @lc app=leetcode.cn id=2047 lang=typescript
 *
 * [2047] 句子中的有效单词数
 */

// @lc code=start
function countValidWords(sentence: string): number {
  const isLetter = (char: string): boolean => {
    if (char >= 'a' && char <= 'z') {
      return true;
    }
    return false;
  };
  const isValid = (word: string): boolean => {
    const n = word.length;
    let hasHyphen = false;
    for (let i = 0; i < n; i++) {
      const char = word[i];
      if (char >= '0' && char <= '9') {
        // 含有数字不合法
        return false;
      } else if (char === '-') {
        // 多个连字符、连字符在首尾、连字符的前后不是字母
        if (
          hasHyphen ||
          i === 0 ||
          i === n - 1 ||
          !isLetter(word[i - 1]) ||
          !isLetter(word[i + 1])
        ) {
          return false;
        }
        // 第一个出现连字符，设置标志
        hasHyphen = true;
      } else if (char === '!' || char === ',' || char === '.') {
        // 含有标点必须在单词的最后
        if (i !== n - 1) {
          return false;
        }
      }
    }

    return true;
  };

  const n = sentence.length;
  let l = 0;
  let r = 0;
  let ans = 0;
  while (1) {
    // 单词左边界
    while (l < n && sentence[l] === ' ') {
      l++;
    }
    // 超过句子末尾
    if (l >= n) {
      break;
    }
    // 单词右边界
    r = l + 1;
    while (r < n && sentence[r] !== ' ') {
      r++;
    }
    const word = sentence.slice(l, r);
    if (isValid(word)) {
      ans++;
    }
    // 更新下一个单词左边界
    l = r + 1;
  }
  return ans;
}
// @lc code=end
