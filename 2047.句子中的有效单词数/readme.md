# 题目

句子仅由小写字母（'a' 到 'z'）、数字（'0' 到 '9'）、连字符（'-'）、标点符号（'!'、'.' 和 ','）以及空格（' '）组成。每个句子可以根据空格分解成 一个或者多个 token ，这些 token 之间由一个或者多个空格 ' ' 分隔。

如果一个 token 同时满足下述条件，则认为这个 token 是一个有效单词：

- 仅由小写字母、连字符和/或标点（不含数字）。
- 至多一个 连字符 '-' 。如果存在，连字符两侧应当都存在小写字母（"a-b" 是一个有效单词，但 "-ab" 和 "ab-" 不是有效单词）。
- 至多一个 标点符号。如果存在，标点符号应当位于 token 的 末尾 。

这里给出几个有效单词的例子："a-b."、"afad"、"ba-c"、"a!" 和 "!" 。

给你一个字符串 sentence ，请你找出并返回 sentence 中 有效单词的数目 。

提示：

- 1 <= sentence.length <= 1000
- sentence 由小写英文字母、数字（0-9）、以及字符（' '、'-'、'!'、'.' 和 ','）组成
- 句子中至少有 1 个 token

# 示例

```
输入：sentence = "cat and  dog"
输出：3
解释：句子中的有效单词是 "cat"、"and" 和 "dog"
```

```
输入：sentence = "!this  1-s b8d!"
输出：0
解释：句子中没有有效单词
"!this" 不是有效单词，因为它以一个标点开头
"1-s" 和 "b8d" 也不是有效单词，因为它们都包含数字
```

# 题解

## 模拟

根据题意我们可以得出非法单词的条件是：

- 单词中含有数字
- 单词中出现两个以上连字符
- 连字符在单词的首部或者尾部
- 连字符的左右两边不是小写字母
- 标点符号没有出现在单词的末尾

利用滑动窗口，获得句子中的每一个单词，然后使用上面的条件验证单词是否合法，统计合法单词的数量。

```ts
function countValidWords(sentence: string): number {
  const isLetter = (char: string): boolean => {
    if (char >= 'a' && char <= 'z') {
      return true;
    }
    return false;
  };
  const isValid = (word: string): boolean => {
    const n = word.length;
    let hasHyphen = false;
    for (let i = 0; i < n; i++) {
      const char = word[i];
      if (char >= '0' && char <= '9') {
        // 含有数字不合法
        return false;
      } else if (char === '-') {
        // 多个连字符、连字符在首尾、连字符的前后不是字母
        if (
          hasHyphen ||
          i === 0 ||
          i === n - 1 ||
          !isLetter(word[i - 1]) ||
          !isLetter(word[i + 1])
        ) {
          return false;
        }
        // 第一个出现连字符，设置标志
        hasHyphen = true;
      } else if (char === '!' || char === ',' || char === '.') {
        // 含有标点必须在单词的最后
        if (i !== n - 1) {
          return false;
        }
      }
    }

    return true;
  };

  const n = sentence.length;
  let l = 0;
  let r = 0;
  let ans = 0;
  while (1) {
    // 单词左边界
    while (l < n && sentence[l] === ' ') {
      l++;
    }
    // 超过句子末尾
    if (l >= n) {
      break;
    }
    // 单词右边界
    r = l + 1;
    while (r < n && sentence[r] !== ' ') {
      r++;
    }
    const word = sentence.slice(l, r);
    if (isValid(word)) {
      ans++;
    }
    // 更新下一个单词左边界
    l = r + 1;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int isLetter(char c)
    {
        return (c >= 'a' && c <= 'z');
    }

    int isDigit(char c)
    {
        return (c >= '0' && c <= '9');
    }

    int isValid(string s)
    {
        int n = s.size();
        int hasHyphen = 0;
        for (int i = 0; i < n; i++)
        {
            char c = s[i];
            if (isDigit(c))
            {
                return 0;
            }
            else if (c == '-')
            {
                if (hasHyphen || i == 0 || i == n - 1 || !isLetter(s[i - 1]) || !isLetter(s[i + 1]))
                {
                    return 0;
                }
                hasHyphen = 1;
            }
            else if (c == '!' || c == '.' || c == ',')
            {
                if (i != n - 1)
                {
                    return 0;
                }
            }
        }
        return 1;
    }

    int countValidWords(string sentence)
    {
        int n = sentence.size();
        int ans = 0;
        int l = 0, r = 0;
        while (1)
        {
            while (l < n && sentence[l] == ' ')
            {
                l++;
            }
            if (l >= n)
            {
                break;
            }
            r = l + 1;
            while (r < n && sentence[r] != ' ')
            {
                r++;
            }
            string s = sentence.substr(l, r - l);
            if (isValid(s))
            {
                ans++;
            }
            l = r + 1;
        }
        return ans;
    }
};
```
