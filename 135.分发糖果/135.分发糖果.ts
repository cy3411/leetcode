/*
 * @lc app=leetcode.cn id=135 lang=typescript
 *
 * [135] 分发糖果
 */

// @lc code=start
function candy(ratings: number[]): number {
  const m = ratings.length;
  // 往右扫描，计算当前位置是否比前一个大
  const l = new Array(m).fill(1);
  for (let i = 1; i < m; i++) {
    if (ratings[i] > ratings[i - 1]) {
      l[i] = l[i - 1] + 1;
    }
  }
  // 往左扫描，计算当前位置是否比后一个大
  const r = new Array(m).fill(1);
  for (let i = m - 2; i >= 0; i--) {
    if (ratings[i] > ratings[i + 1]) {
      r[i] = r[i + 1] + 1;
    }
  }

  let ans = 0;
  // 每次取左右里较大的那个值来更新答案
  for (let i = 0; i < m; i++) {
    ans += Math.max(l[i], r[i]);
  }

  return ans;
}
// @lc code=end
