/*
 * @lc app=leetcode.cn id=1963 lang=typescript
 *
 * [1963] 使字符串平衡的最小交换次数
 */

// @lc code=start
function minSwaps(s: string): number {
  let ans = 0;
  let l = 0;
  let r = s.length - 1;
  let lcnt = s[l] === '[' ? 1 : -1;
  let rcnt = s[r] === ']' ? 1 : -1;
  while (l < r) {
    // 计算左边的非法括号
    while (l < r && lcnt >= 0) {
      l++;
      lcnt += s[l] === '[' ? 1 : -1;
    }
    // 计算右边的非法括号
    while (l < r && rcnt >= 0) {
      r--;
      rcnt += s[r] === ']' ? 1 : -1;
    }
    if (l >= r) break;
    ans++;
    // 交换，原先是-1，交换完要变成1
    lcnt += 2;
    rcnt += 2;
  }
  return ans;
}
// @lc code=end
