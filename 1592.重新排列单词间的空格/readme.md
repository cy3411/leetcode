# 题目

给你一个字符串 `text` ，该字符串由若干被空格包围的单词组成。每个单词由一个或者多个小写英文字母组成，并且两个单词之间至少存在一个空格。题目测试用例保证 `text` **至少包含一个单词** 。

请你重新排列空格，使每对相邻单词之间的空格数目都 **相等** ，并尽可能 **最大化** 该数目。如果不能重新平均分配所有空格，请 **将多余的空格放置在字符串末尾** ，这也意味着返回的字符串应当与原 `text` 字符串的长度相等。

返回 **重新排列空格后的字符串** 。

提示：

- $1 <= text.length <= 100$
- `text` 由小写英文字母和 `' '` 组成
- `text` 中至少包含一个单词

# 示例

```
输入：text = "  this   is  a sentence "
输出："this   is   a   sentence"
解释：总共有 9 个空格和 4 个单词。可以将 9 个空格平均分配到相邻单词之间，相邻单词间空格数为：9 / (4-1) = 3 个。
```

```
输入：text = " practice   makes   perfect"
输出："practice   makes   perfect "
解释：总共有 7 个空格和 3 个单词。7 / (3-1) = 3 个空格加上 1 个多余的空格。多余的空格需要放在字符串的末尾。
```

# 题解

## 模拟

按照空格分割，得到单词数量，并计算出空格的数量。

然后计算出单词的间隔数量，按照单词和间隔数量来拼接，最后将剩余的空格拼接到答案的结尾。

需要注意一下，如果只有一个单词，只要将所有的空格拼接到单词后返回即可。

```js
function reorderSpaces(text: string): string {
  const n = text.length;
  // 将所有单词取出
  const words: string[] = [];
  text.split(' ').forEach((s) => {
    if (s.length > 0) words.push(s);
  });
  // 计算空格数量
  let spacesCount = n;
  words.forEach((word) => {
    spacesCount -= word.length;
  });
  // 计算结果
  let ans = '';
  const wordsCount = words.length - 1;
  // 没有间隔，就是只有一个单词
  if (wordsCount === 0) {
    ans += words[0];
    for (let i = 0; i < spacesCount; i++) {
      ans += ' ';
    }
    return ans;
  }
  // 多个单词，按照间隔来划分空格数量
  const perSpace = (spacesCount / wordsCount) >> 0;
  const restSpace = spacesCount % wordsCount;
  for (let i = 0; i <= wordsCount; i++) {
    if (i > 0) {
      for (let j = 0; j < perSpace; j++) {
        ans += ' ';
      }
    }
    ans += words[i];
  }

  // 追加最后的空格
  for (let i = 0; i < restSpace; i++) {
    ans += ' ';
  }

  return ans;
}
```
