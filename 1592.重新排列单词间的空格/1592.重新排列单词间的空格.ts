/*
 * @lc app=leetcode.cn id=1592 lang=typescript
 *
 * [1592] 重新排列单词间的空格
 */

// @lc code=start
function reorderSpaces(text: string): string {
  const n = text.length;
  // 将所有单词取出
  const words: string[] = [];
  text.split(' ').forEach((s) => {
    if (s.length > 0) words.push(s);
  });
  // 计算空格数量
  let spacesCount = n;
  words.forEach((word) => {
    spacesCount -= word.length;
  });
  // 计算结果
  let ans = '';
  const wordsCount = words.length - 1;
  // 没有间隔，就是只有一个单词
  if (wordsCount === 0) {
    ans += words[0];
    for (let i = 0; i < spacesCount; i++) {
      ans += ' ';
    }
    return ans;
  }
  // 多个单词，按照间隔来划分空格数量
  const perSpace = (spacesCount / wordsCount) >> 0;
  const restSpace = spacesCount % wordsCount;
  for (let i = 0; i <= wordsCount; i++) {
    if (i > 0) {
      for (let j = 0; j < perSpace; j++) {
        ans += ' ';
      }
    }
    ans += words[i];
  }

  // 追加最后的空格
  for (let i = 0; i < restSpace; i++) {
    ans += ' ';
  }

  return ans;
}
// @lc code=end
