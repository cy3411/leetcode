# 题目

**最大树** 定义：一棵树，并满足：其中每个节点的值都大于其子树中的任何其他值。

给你最大树的根节点 `root` 和一个整数 `val` 。

就像 之前的问题 那样，给定的树是利用 `Construct(a)` 例程从列表 `a（root = Construct(a)）`递归地构建的：

- `如果 `a`为空，返回`null` 。
- 否则，令 `a[i]` 作为 `a` 的最大元素。创建一个值为 `a[i]` 的根节点 `root` 。
- `root` 的左子树将被构建为 `Construct([a[0], a[1], ..., a[i - 1]])` 。
- `root` 的右子树将被构建为 `Construct([a[i + 1], a[i + 2], ..., a[a.length - 1]])` 。
- 返回 `root` 。

请注意，题目没有直接给出 `a` ，只是给出一个根节点 `root = Construct(a)` 。

假设 `b` 是 `a` 的副本，并在末尾附加值 `val`。题目数据保证 `b` 中的值互不相同。

返回 `Construct(b)` 。

提示：

- 树中节点数目在范围 `[1, 100]` 内
- $1 \leq Node.val \leq 100$
- 树中的所有值 **互不相同**
- $1 \leq val \leq 100$

# 示例

![https://wx2.sinaimg.cn/mw2000/a8da15f5ly1h5p8g0bkmvj207p07aaa9.jpg](https://wx2.sinaimg.cn/mw2000/a8da15f5ly1h5p8g0bkmvj207p07aaa9.jpg)

```
输入：root = [4,1,3,null,null,2], val = 5
输出：[5,4,null,1,3,null,null,2]
解释：a = [1,4,2,3], b = [1,4,2,3,5]
```

```
输入：root = [5,2,4,null,1], val = 3
输出：[5,2,4,null,1,null,3]
解释：a = [2,1,5,4], b = [2,1,5,4,3]
```

# 题解

## 遍历节点

如果根节点小于 val， 那么新的树将以 val 为根节点， 原来的树将作为新树的左子节点返回。

遍历右子树，找到第一个小于 val 的节点，将当前节点作为 val 节点的左节点，val 节点作为父节点的右子树返回。 否则，就是找不到小于 val 的节点，那么 val 节点作为右子树的最后一个节点的右节点后返回。

```js
function insertIntoMaxTree(root: TreeNode | null, val: number): TreeNode | null {
  let parentNode: TreeNode | null = null;
  let currNode = root;
  while (currNode) {
    if (val > currNode.val) {
      if (!parentNode) {
        return new TreeNode(val, root);
      }
      const node = new TreeNode(val, currNode);
      parentNode.right = node;
      return root;
    } else {
      parentNode = currNode;
      currNode = currNode.right;
    }
  }
  parentNode!.right = new TreeNode(val);
  return root;
}
```
