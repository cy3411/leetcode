# 题目

我们有 `n` 栋楼，编号从 `0` 到 `n - 1` 。每栋楼有若干员工。由于现在是换楼的季节，部分员工想要换一栋楼居住。

给你一个数组 `requests` ，其中 $requests[i] = [from_i, to_i]$ ，表示一个员工请求从编号为 $from_i$ 的楼搬到编号为 $to_i$ 的楼。

一开始 **所有楼都是满的**，所以从请求列表中选出的若干个请求是可行的需要满足 **每栋楼员工净变化为 0** 。意思是每栋楼 **离开** 的员工数目 等于 该楼 **搬入** 的员工数数目。比方说 `n = 3` 且两个员工要离开楼 `0` ，一个员工要离开楼 `1` ，一个员工要离开楼 `2` ，如果该请求列表可行，应该要有两个员工搬入楼 `0` ，一个员工搬入楼 `1` ，一个员工搬入楼 `2` 。

请你从原请求列表中选出若干个请求，使得它们是一个可行的请求列表，并返回所有可行列表中最大请求数目。

提示：

- $1 \leq n \leq 20$
- $1 \leq requests.length \leq 16$
- $requests[i].length \equiv 2$
- $0 \leq from_i, to_i < n$

# 示例

[![bM5uTK.png](https://s4.ax1x.com/2022/03/01/bM5uTK.png)](https://imgtu.com/i/bM5uTK)

```
输入：n = 5, requests = [[0,1],[1,0],[0,1],[1,2],[2,0],[3,4]]
输出：5
解释：请求列表如下：
从楼 0 离开的员工为 x 和 y ，且他们都想要搬到楼 1 。
从楼 1 离开的员工为 a 和 b ，且他们分别想要搬到楼 2 和 0 。
从楼 2 离开的员工为 z ，且他想要搬到楼 0 。
从楼 3 离开的员工为 c ，且他想要搬到楼 4 。
没有员工从楼 4 离开。
我们可以让 x 和 b 交换他们的楼，以满足他们的请求。
我们可以让 y，a 和 z 三人在三栋楼间交换位置，满足他们的要求。
所以最多可以满足 5 个请求。
```

[![bM5QYD.png](https://s4.ax1x.com/2022/03/01/bM5QYD.png)](https://imgtu.com/i/bM5QYD)

```
输入：n = 3, requests = [[0,0],[1,2],[2,1]]
输出：3
解释：请求列表如下：
从楼 0 离开的员工为 x ，且他想要回到原来的楼 0 。
从楼 1 离开的员工为 y ，且他想要搬到楼 2 。
从楼 2 离开的员工为 z ，且他想要搬到楼 1 。
我们可以满足所有的请求。
```

# 题解

## 二进制枚举

我们可以使用一个长度为 `m` 的二进制数 mask 表示所有的请求，二进制中从低到高，第 `i` 位为 `1` 表示选择了第 `i` 个请求，第 `i` 位为 `0` 表示不选择第 `i` 个请求。

比如：二进制 `11`，这个是数字 `3`，表示使用了 `requests[0]` 和 `requests[1]` 请求

我们枚举所有的 `mask`，并计算每个 `mask` 的可行请求数目和合法性，如果合法，就更新答案。

合法性的验证就是检查当前 `mask` 中所有请求中搬入和搬出的差值，如果有的楼的差值不等于 `0` ，那么这个 `mask` 不合法。

```ts
function maximumRequests(n: number, requests: number[][]): number {
  // 检查当前数字二进制位1的数量，也就是请求的数量
  const getRequests = (num: number): number => {
    let ret = 0;
    while (num > 0) {
      ret++;
      num &= num - 1;
    }
    return ret;
  };
  // 检查请求的合法性
  const checkRequest = (mask: number): boolean => {
    // 每栋楼的空闲房间数
    const delta = new Array(n).fill(0);
    for (let i = 0; i < m; i++) {
      // 处理当前请求
      if ((mask & (1 << i)) !== 0) {
        const [from, to] = requests[i];
        // 搬出，空闲房间数+1
        delta[from]++;
        // 搬入，空闲房间数-1
        delta[to]--;
      }
    }
    for (const x of delta) {
      // 如果有空闲房间数不等于0，则不满足题目条件
      if (x !== 0) {
        return false;
      }
    }
    return true;
  };

  const m = requests.length;
  let ans = 0;
  // 枚举所有可能的请求
  // 数字的二进制位为1的话，表示选择了该位置对应的request
  for (let mask = 1; mask < 1 << m; mask++) {
    const cnt = getRequests(mask);
    if (cnt <= ans) continue;
    if (checkRequest(mask)) ans = cnt;
  }
  return ans;
}
```

```cpp
class Solution
{
public:
    int getRequests(int num)
    {
        int ret = 0;
        while (num != 0)
        {
            ret++;
            num &= num - 1;
        }
        return ret;
    }
    int checkRequest(int mask, int n, vector<vector<int>> &requests, vector<int> delta)
    {
        int m = requests.size();
        fill(delta.begin(), delta.end(), 0);
        for (int i = 0; i < m; i++)
        {
            if ((mask & (1 << i)) == 0)
            {
                continue;
            }
            int a = requests[i][0];
            int b = requests[i][1];
            delta[a]++;
            delta[b]--;
        }
        for (auto x : delta)
        {
            if (x != 0)
            {
                return 0;
            }
        }
        return 1;
    }
    int maximumRequests(int n, vector<vector<int>> &requests)
    {
        int m = requests.size();
        int ans = 0;
        vector<int> delta(n);
        for (int mask = 1; mask < (1 << m); mask++)
        {
            int cnt = getRequests(mask);
            if (cnt <= ans)
            {
                continue;
            }
            if (checkRequest(mask, n, requests, delta))
            {
                ans = cnt;
            }
        }
        return ans;
    }
};
```
