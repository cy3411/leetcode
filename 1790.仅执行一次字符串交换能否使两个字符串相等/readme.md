# 题目

给你长度相等的两个字符串 s1 和 s2 。一次 字符串交换 操作的步骤如下：选出某个字符串中的两个下标（不必不同），并交换这两个下标所对应的字符。

如果对 其中一个字符串 执行 最多一次字符串交换 就可以使两个字符串相等，返回 true ；否则，返回 false 。

提示：

- $1 \leq s1.length, s2.length \leq 100$
- $s1.length \equiv s2.length$
- `s1` 和 `s2` 仅由小写英文字母组成

# 示例

```
输入：s1 = "bank", s2 = "kanb"
输出：true
解释：例如，交换 s2 中的第一个和最后一个字符可以得到 "bank"
```

```
输入：s1 = "attack", s2 = "defend"
输出：false
解释：一次字符串交换无法使两个字符串相等
```

# 题解

## 计数统计

题意要求最多只交换一次使得两个字符串相等，那么两个字符中最多存在两个不同位置不相等。

遍历字符串，统计两者的不同点，如果数量不等于 2，那么就返回 false。

否则检查两者位置的字符串是否相等即可。

需要注意一下，如果字符串完全相等，不需要交换，需要返回 true 。

```js
function areAlmostEqual(s1: string, s2: string): boolean {
  const n = s1.length;
  const diff: number[] = [];

  for (let i = 0; i < n; i++) {
    if (s1[i] === s2[i]) continue;
    diff.push(i);
    if (diff.length > 2) return false;
  }

  const m = diff.length;
  // 两个字符串完全相等
  if (m === 0) return true;
  //  两个字符串的不同点不是2个
  if (m !== 2) return false;

  return s1[diff[0]] === s2[diff[1]] && s1[diff[1]] === s2[diff[0]];
}
```

```cpp
class Solution {
public:
    bool areAlmostEqual(string s1, string s2) {
        int temp1 = 0, temp2 = 0, n = s1.size();

        for (int i = 0; i < n; i++) {
            if (s1[i] == s2[i]) continue;
            // 找到一对字符
            if (temp1 == s2[i] && temp2 == s1[i]) {
                temp1 = -1;
            }
            // 初始值
            else if (temp1 == 0) {
                temp1 = s1[i];
                temp2 = s2[i];
            }
            // 超过一对
            else {
                return false;
            }
        }

        return temp1 <= 0;
    }
};
```
