/*
 * @lc app=leetcode.cn id=1790 lang=cpp
 *
 * [1790] 仅执行一次字符串交换能否使两个字符串相等
 */

// @lc code=start
class Solution {
public:
    bool areAlmostEqual(string s1, string s2) {
        int temp1 = 0, temp2 = 0, n = s1.size();

        for (int i = 0; i < n; i++) {
            if (s1[i] == s2[i]) continue;
            // 找到一对字符
            if (temp1 == s2[i] && temp2 == s1[i]) {
                temp1 = -1;
            }
            // 初始值
            else if (temp1 == 0) {
                temp1 = s1[i];
                temp2 = s2[i];
            }
            // 超过一对
            else {
                return false;
            }
        }

        return temp1 <= 0;
    }
};
// @lc code=end
