# 题目

有一个具有 n 个顶点的 双向 图，其中每个顶点标记从 `0` 到 `n - 1`（包含 `0` 和 `n - 1`）。图中的边用一个二维整数数组 `edges` 表示，其中 `edges[i] = [ui, vi]` 表示顶点 `ui` 和顶点 `vi` 之间的双向边。 每个顶点对由 **最多一条** 边连接，并且没有顶点存在与自身相连的边。

请你确定是否存在从顶点 `source` 开始，到顶点 `destination` 结束的 有效路径 。

给你数组 `edges` 和整数 `n`、`source` 和 `destination`，如果从 `source` 到 `destination` 存在 **有效路径** ，则返回 `true`，否则返回 `false` 。

提示：

- $1 <= n <= 2 * 10^5$
- $0 <= edges.length <= 2 * 10^5$
- $edges[i].length == 2$
- $0 <= ui, vi <= n - 1$
- $ui != vi$
- $0 <= source, destination <= n - 1$
- 不存在重复边
- 不存在指向顶点自身的边

# 示例

[![zL8gUO.png](https://s1.ax1x.com/2022/12/19/zL8gUO.png)](https://imgse.com/i/zL8gUO)

```
输入：n = 3, edges = [[0,1],[1,2],[2,0]], source = 0, destination = 2
输出：true
解释：存在由顶点 0 到顶点 2 的路径:
- 0 → 1 → 2
- 0 → 2
```

[![zL825D.png](https://s1.ax1x.com/2022/12/19/zL825D.png)](https://imgse.com/i/zL825D)

```
输入：n = 6, edges = [[0,1],[0,2],[3,5],[5,4],[4,3]], source = 0, destination = 5
输出：false
解释：不存在由顶点 0 到顶点 5 的路径.
```

# 题解

求取图中 source 和 destination 两个顶点之间是否连通即可。

## BFS

```ts
function validPath(n: number, edges: number[][], source: number, destination: number): boolean {
  // 建模
  const adj: number[][] = new Array(n).fill(0).map((_) => []);
  for (const [u, v] of edges) {
    adj[u].push(v);
    adj[v].push(u);
  }

  const que: number[] = [];
  const visited: boolean[] = new Array(n).fill(false);
  // 初始化
  que.push(source);
  visited[source] = true;

  // bfs
  while (que.length) {
    const curr = que.shift()!;
    if (curr === destination) return true;
    for (const v of adj[curr]) {
      if (visited[v]) continue;
      visited[v] = true;
      que.push(v);
    }
  }

  return false;
}
```
