/*
 * @lc app=leetcode.cn id=1971 lang=typescript
 *
 * [1971] 寻找图中是否存在路径
 */

// @lc code=start
function validPath(n: number, edges: number[][], source: number, destination: number): boolean {
  // 建模
  const adj: number[][] = new Array(n).fill(0).map((_) => []);
  for (const [u, v] of edges) {
    adj[u].push(v);
    adj[v].push(u);
  }

  const que: number[] = [];
  const visited: boolean[] = new Array(n).fill(false);
  // 初始化
  que.push(source);
  visited[source] = true;

  // bfs
  while (que.length) {
    const curr = que.shift()!;
    if (curr === destination) return true;
    for (const v of adj[curr]) {
      if (visited[v]) continue;
      visited[v] = true;
      que.push(v);
    }
  }

  return false;
}
// @lc code=end
