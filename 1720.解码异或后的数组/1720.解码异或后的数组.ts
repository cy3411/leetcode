/*
 * @lc app=leetcode.cn id=1720 lang=typescript
 *
 * [1720] 解码异或后的数组
 */

// @lc code=start
function decode(encoded: number[], first: number): number[] {
  const result = [first];

  for (const num of encoded) {
    let temp = result[result.length - 1];
    result.push(temp ^ num);
  }

  return result;
}
// @lc code=end
