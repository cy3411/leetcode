# 题目
未知 整数数组 `arr` 由 `n` 个非负整数组成。

经编码后变为长度为 `n - 1` 的另一个整数数组 `encoded` ，其中 `encoded[i] = arr[i] XOR arr[i + 1]` 。例如，`arr = [1,0,2,1]` 经编码后得到 `encoded = [1,2,3]` 。

给你编码后的数组 `encoded` 和原数组 arr 的第一个元素 `first（arr[0]）`。

请解码返回原数组 `arr` 。可以证明答案存在并且是唯一的。

# 示例
```
输入：encoded = [1,2,3], first = 1
输出：[1,0,2,1]
解释：若 arr = [1,0,2,1] ，那么 first = 1 且 encoded = [1 XOR 0, 0 XOR 2, 2 XOR 1] = [1,2,3]
```
# 题解
## 位运算
异或运算具有如下性质：
+ 任何整数与自身异或都等于0
+ 任何整数与0异或都等于自身

当 `0 < i < n` 的时候，$encoded[i-1] = arr[i-1] \bigoplus arr[i]$。  
公式两边同时异或 `arr[i-1`，可以得到$arr[i]=arr[i-1]\bigoplus encoded[i-1]$

```js
function decode(encoded: number[], first: number): number[] {
  const result = [first];

  for (const num of encoded) {
    let temp = result[result.length - 1];
    result.push(temp ^ num);
  }

  return result;
}
```