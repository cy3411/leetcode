# 题目

给你一个整数数组 nums 和两个整数：left 及 right 。找出 nums 中连续、非空且其中最大元素在范围 [left, right] 内的子数组，并返回满足条件的子数组的个数。

生成的测试用例保证结果符合 32-bit 整数范围。

提示：

- $1 <= nums.length <= 10^5$
- $0 <= nums[i] <= 10^9$
- $0 <= left <= right <= 10^9$

# 示例

```
输入：nums = [2,1,4,3], left = 2, right = 3
输出：3
解释：满足条件的三个子数组：[2], [2, 1], [3]
```

```
输入：nums = [2,9,2,5,6], left = 2, right = 8
输出：7
```

# 题解

## 遍历

遍历 nums ，寻找合法的右边界和左边界，最后两个边界累加和就是答案。

```ts
function numSubarrayBoundedMax(nums: number[], left: number, right: number): number {
  let ans = 0;
  // 右边界
  let last1 = -1;
  // 左边界
  let last2 = -1;
  for (let i = 0; i < nums.length; i++) {
    // 合法的右边界
    if (nums[i] >= left && nums[i] <= right) {
      last1 = i;
    } else if (nums[i] > right) {
      // 左边界变更，重新寻找右边界
      last2 = i;
      last1 = -1;
    }
    // 更新答案
    if (last1 !== -1) {
      ans += last1 - last2;
    }
  }

  return ans;
}
```
