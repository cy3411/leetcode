/*
 * @lc app=leetcode.cn id=744 lang=typescript
 *
 * [744] 寻找比目标字母大的最小字母
 */

// @lc code=start
function nextGreatestLetter(letters: string[], target: string): string {
  let l = 0;
  let r = letters.length - 1;
  while (l < r) {
    const mid = l + ((r - l) >> 1);
    if (letters[mid] <= target) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }
  // 最后一位如果还是小于target，则返回第一位。题意是循环出现的。
  return letters[l] > target ? letters[l] : letters[0];
}
// @lc code=end
