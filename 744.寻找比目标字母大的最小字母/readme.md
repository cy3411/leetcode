# 题目

给你一个排序后的字符列表 `letters` ，列表中只包含小写英文字母。另给出一个目标字母 `target`，请你寻找在这一有序列表里比目标字母大的最小字母。

在比较时，字母是依序循环出现的。举个例子：

- 如果目标字母 `target = 'z'` 并且字符列表为 `letters = ['a', 'b']`，则答案返回 `'a'`

提示：

- $2 \leq letters.length \leq 10^4$
- `letters[i]` 是一个小写字母
- `letters` 按非递减顺序排序
- `letters` 最少包含两个不同的字母
- `target` 是一个小写字母

# 示例

```
输入: letters = ["c", "f", "j"]，target = "a"
输出: "c"
```

```
输入: letters = ["c","f","j"], target = "c"
输出: "f"
```

# 题解

## 二分查找

题目给出的字符列表已经排序，所以可以使用二分查找第一个大于 target 的字符。

由于字母是循环出现的，如果最后一位字符依然小于 target，则说明字符列表中没有大于 target 的字符。则直接返回第一位字符即可。

```ts
function nextGreatestLetter(letters: string[], target: string): string {
  let l = 0;
  let r = letters.length - 1;
  while (l < r) {
    const mid = l + ((r - l) >> 1);
    if (letters[mid] <= target) {
      l = mid + 1;
    } else {
      r = mid;
    }
  }
  // 最后一位如果还是小于target，则返回第一位。题意是循环出现的。
  return letters[l] > target ? letters[l] : letters[0];
}
```

```cpp
class Solution
{
public:
    char nextGreatestLetter(vector<char> &letters, char target)
    {
        int l = 0, r = letters.size() - 1;
        while (l <= r)
        {
            int mid = l + (r - l) / 2;
            if (letters[mid] <= target)
                l = mid + 1;
            else
                r = mid - 1;
        }

        return letters[l % letters.size()];
    }
};
```
