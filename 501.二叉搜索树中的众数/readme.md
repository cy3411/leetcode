# 题目

给定一个有相同值的二叉搜索树（BST），找出 BST 中的所有众数（出现频率最高的元素）。

假定 BST 有如下定义：

- 结点左子树中所含结点的值小于等于当前结点的值
- 结点右子树中所含结点的值大于等于当前结点的值
- 左子树和右子树都是二叉搜索树

提示：如果众数超过 1 个，不需考虑输出顺序

进阶：你可以不使用额外的空间吗？（假设由递归产生的隐式调用栈的开销不被计算在内）

# 示例

```
 1
    \
     2
    /
   2
返回[2]
```

# 题解

## 中序遍历

二叉搜索树的中序遍历返回是一个单调递增的序列。

其实就是在一个有序序列中寻找出现次数最多的数。

```ts
function findMode(root: TreeNode | null): number[] {
  const inorder = (root: TreeNode | null) => {
    if (root === null) return;
    inorder(root.left);
    // 统计连续出现相同值的次数
    if (curr.val === root.val) {
      count++;
    } else {
      curr = root;
      count = 1;
    }
    // 值出现次数等于最大次数，相同出现次数的众数放入结果
    if (count === maxCount) {
      ans.push(root.val);
      // 值出现的次数大于最大值，清空数组，放入新的众数，并更新最大次数
    } else if (count > maxCount) {
      maxCount = count;
      ans.length = 0;
      ans.push(root.val);
    }
    inorder(root.right);
  };
  const ans: number[] = [];
  let count = 0;
  let maxCount = 0;
  let curr = root;

  inorder(root);

  return ans;
}
```
