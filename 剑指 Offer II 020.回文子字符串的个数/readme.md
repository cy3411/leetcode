# 题目

给定一个字符串 `s` ，请计算这个字符串中有多少个回文子字符串。

具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被视作不同的子串。

提示：

- $1 \leq s.length \leq 1000$
- `s` 由小写英文字母组成

注意：[本题与主站 647 题相同](https://leetcode-cn.com/problems/palindromic-substrings/)

# 示例

```
输入：s = "abc"
输出：3
解释：三个回文子串: "a", "b", "c"
```

```
输入：s = "aaa"
输出：6
解释：6个回文子串: "a", "a", "a", "aa", "aa", "aaa"
```

# 题解

## 中心扩展

枚举所有的中心点，然后分别扩展左右两边，判断是否是回文串并更新答案。

由于中心点有奇数和偶数的区分，所以 `n` 个字符的字符串有 `(2n - 1)` (最后一位字符只有奇数中心点) 个中心点。

```ts
function countSubstrings(s: string): number {
  const n = s.length;
  let ans = 0;
  // 枚举所有的中心点，然后向两边扩展查找回文子串
  for (let i = 0; i < 2 * n - 1; i++) {
    let l = (i / 2) >> 0;
    let r = l + (i % 2);
    while (l >= 0 && r < n && s[l] === s[r]) {
      ans++;
      l--;
      r++;
    }
  }

  return ans;
}
```

```cpp
class Solution {
public:
    int countSubstrings(string s) {
      int n = s.size(), ans = 0;
      for (int i = 0; i < (2 * n) -1; i++) {
        int l = i / 2, r = l + (i % 2);
        while (l >= 0 && r < n && s[l] == s[r]) {
          ans++, l--, r++;
        }
      }
      return ans;
    }
};
```
