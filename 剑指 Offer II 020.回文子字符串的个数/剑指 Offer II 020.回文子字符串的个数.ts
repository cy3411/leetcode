// @algorithm @lc id=1000256 lang=typescript
// @title a7VOhD
// @test("abc")=3
function countSubstrings(s: string): number {
  const n = s.length;
  let ans = 0;
  // 枚举所有的中心点，然后向两边扩展查找回文子串
  for (let i = 0; i < 2 * n - 1; i++) {
    let l = (i / 2) >> 0;
    let r = l + (i % 2);
    while (l >= 0 && r < n && s[l] === s[r]) {
      ans++;
      l--;
      r++;
    }
  }

  return ans;
}
