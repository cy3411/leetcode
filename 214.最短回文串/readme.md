# 题目

给定一个字符串 s，你可以通过在字符串前面添加字符将其转换为回文串。找到并返回可以用这种方式转换的最短回文串。

提示：

- $0 \leq s.length \leq 5 * 10^4$
- `s` 仅由小写英文字母组成

# 示例

```
输入：s = "aacecaaa"
输出："aaacecaaa"
```

```
输入：s = "abcd"
输出："dcbabcd"
```

# 题解

## KMP

最直接的做法就是将原串反转后拼接到前面，就是一个回文串。

```
原串：aabc
最直接办法:cbaaaabc
```

我们可以看出，如果想要回文串最短，只要将原串的起始部分和反转后串的结束部分，重复的部分删除掉一份就可以了。

我们可以先将原串和反转后串用一个特殊符号拼接起来，利用 `KMP` 算法，求出最后一个字符为结尾的最长相同前后缀。

```
新的拼接，通过这个计算最长相同前后缀
aabc#cbaa
```

这样就可以截取出没有最长前缀的字符串。反转后和原串拼接就是答案。

```
得到的新串：bc
反转后和原串拼接就是答案：cbaabc
```

```ts
function shortestPalindrome(s: string): string {
  let revs = s.split('').reverse().join('');
  // 将原字符串和反转后的串用一个特殊符号拼接
  // 用KMP去找这个新串的最长相同前后缀
  revs = s + '#' + revs;

  const n = revs.length;
  const next = new Array(n).fill(-1);
  let j = -1;
  for (let i = 1; i < n; i++) {
    // 如果有公共前后缀，就跳转
    while (j !== -1 && revs[i] !== revs[j + 1]) {
      j = next[j];
    }
    if (revs[i] === revs[j + 1]) {
      j++;
    }
    next[i] = j;
  }
  // 拿到去掉公共前缀的字符串
  revs = revs.substring(next[n - 1] + 1, s.length);
  revs = revs.split('').reverse().join('');
  return revs + s;
}
```
