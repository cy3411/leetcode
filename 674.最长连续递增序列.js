/*
 * @lc app=leetcode.cn id=674 lang=javascript
 *
 * [674] 最长连续递增序列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findLengthOfLCIS = function (nums) {
  const size = nums.length;
  if (!size) {
    return 0;
  }

  let start = 0;
  let end = 0;
  let result = 1;

  for (let i = 0; i < size - 1; i++) {
    if (nums[i] < nums[i + 1]) {
      end = i + 1;
    } else {
      start = i + 1;
    }
    result = Math.max(result, end - start + 1);
  }

  return result;
};
// @lc code=end
