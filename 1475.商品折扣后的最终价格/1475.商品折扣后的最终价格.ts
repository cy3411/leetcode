/*
 * @lc app=leetcode.cn id=1475 lang=typescript
 *
 * [1475] 商品折扣后的最终价格
 */

// @lc code=start
function finalPrices(prices: number[]): number[] {
  const n = prices.length;
  const ans = new Array<number>(n);
  for (let i = 0; i < n; i++) {
    let discount = 0;
    for (let j = i + 1; j < n; j++) {
      if (prices[j] <= prices[i]) {
        discount = prices[j];
        break;
      }
    }
    ans[i] = prices[i] - discount;
  }
  return ans;
}
// @lc code=end
