# 题目

给你一个数组 `prices` ，其中 `prices[i]` 是商店里第 `i` 件商品的价格。

商店里正在进行促销活动，如果你要买第 `i` 件商品，那么你可以得到与 `prices[j]` 相等的折扣，其中 `j` 是满足 $j > i$ 且 $prices[j] \leq prices[i]$ 的 **最小下标** ，如果没有满足条件的 `j` ，你将没有任何折扣。

请你返回一个数组，数组中第 `i` 个元素是折扣后你购买商品 `i` 最终需要支付的价格。

提示：

- $1 \leq prices.length \leq 500$
- $1 \leq prices[i] \leq 10^3$

# 示例

```
输入：prices = [8,4,6,2,3]
输出：[4,2,4,2,3]
解释：
商品 0 的价格为 price[0]=8 ，你将得到 prices[1]=4 的折扣，所以最终价格为 8 - 4 = 4 。
商品 1 的价格为 price[1]=4 ，你将得到 prices[3]=2 的折扣，所以最终价格为 4 - 2 = 2 。
商品 2 的价格为 price[2]=6 ，你将得到 prices[3]=2 的折扣，所以最终价格为 6 - 2 = 4 。
商品 3 和 4 都没有折扣。
```

```
输入：prices = [1,2,3,4,5]
输出：[1,2,3,4,5]
解释：在这个例子中，所有商品都没有折扣。
```

# 题解

## 遍历

对于第 i 件商品，我们从 i + 1 开始往后遍历，找到满足题意的下标 j ，然后计算 i 商品的折扣价格。

```js
function finalPrices(prices: number[]): number[] {
  const n = prices.length;
  const ans = new Array() < number > n;
  for (let i = 0; i < n; i++) {
    let discount = 0;
    for (let j = i + 1; j < n; j++) {
      if (prices[j] <= prices[i]) {
        discount = prices[j];
        break;
      }
    }
    ans[i] = prices[i] - discount;
  }
  return ans;
}
```

## 单调栈

倒序遍历 prices ， 使用单调栈维护右边小于 prices[i] 的元素 j 。当栈不为空的时候，栈顶元素就是最近的折扣。

```cpp
class Solution {
public:
    vector<int> finalPrices(vector<int> &prices) {
        int n = prices.size();
        vector<int> ans(n);
        stack<int> stk;
        for (int i = n - 1; i >= 0; i--) {
            while (!stk.empty() && stk.top() > prices[i]) {
                stk.pop();
            }
            ans[i] = stk.empty() ? prices[i] : prices[i] - stk.top();
            stk.emplace(prices[i]);
        }

        return ans;
    }
};
```
