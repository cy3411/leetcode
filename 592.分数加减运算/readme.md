# 题目

给定一个表示分数加减运算的字符串 `expression` ，你需要返回一个字符串形式的计算结果。

这个结果应该是不可约分的分数，即[最简分数](https://baike.baidu.com/item/%E6%9C%80%E7%AE%80%E5%88%86%E6%95%B0)。 如果最终结果是一个整数，例如 `2`，你需要将它转换成分数形式，其分母为 `1`。所以在上述例子中, `2` 应该被转换为 `2/1`。

提示:

- 输入和输出字符串只包含 `'0'` 到 `'9'` 的数字，以及 `'/'`, `'+'` 和 `'-'`。
- 输入和输出分数格式均为 `± 分子/分母`。如果输入的第一个分数或者输出的分数是正数，则 `'+'` 会被省略掉。
- 输入只包含合法的最简分数，每个分数的分子与分母的范围是 [1,10]。 如果分母是 1，意味着这个分数实际上是一个整数。
- 输入的分数个数范围是 [1,10]。
- 最终结果的分子与分母保证是 32 位整数范围内的有效整数。

# 示例

```
输入: expression = "-1/2+1/2"
输出: "0/1"
```

```
输入: expression = "-1/2+1/2+1/3"
输出: "1/3"
```

```
输入: expression = "1/3-1/2"
输出: "-1/6"
```

# 题解

## 模拟

先了解一下分数相加的公式：

$$
    \frac{x1}{y1} + \frac{x2}{y2} = \frac{x1y2+x2y1}{y1y2}
$$

枚举字符串所有的分数，然后模拟相加。最后返回结果的时候，如果分子为 0，直接返回 `0/1`，否则返回 `分子/分母`的最简分数。

最简分数就是将分数的分子和分母分别与其最大公约数相除后的结果。

```ts
function isDigit(c: string): boolean {
  return c >= '0' && c <= '9';
}

function gcd(a: number, b: number): number {
  if (b === 0) return a;
  return gcd(b, a % b);
}

function fractionAddition(expression: string): string {
  const n = expression.length;
  // 分子
  let numerator = 0;
  // 分母
  let denominator = 1;
  // 从 0 开始遍历
  let idx = 0;

  while (idx < n) {
    // 数字的正负号
    let sign = 1;
    if (expression[idx] === '-' || expression[idx] === '+') {
      sign = expression[idx] === '-' ? -1 : 1;
      idx++;
    }
    // 读取分子
    let numerator1 = 0;
    while (idx < n && isDigit(expression[idx])) {
      numerator1 = numerator1 * 10 + Number(expression[idx]);
      idx++;
    }
    numerator1 = numerator1 * sign;
    // 跳过 /
    idx++;
    // 读取分母
    let denominator1 = 0;
    while (idx < n && isDigit(expression[idx])) {
      denominator1 = denominator1 * 10 + Number(expression[idx]);
      idx++;
    }
    // 累加结果
    numerator = numerator * denominator1 + numerator1 * denominator;
    denominator *= denominator1;
  }

  // 如果分子为0，直接返回
  if (numerator === 0) {
    return '0/1';
  }
  // 计算分子和分母的最大公约数
  const g = gcd(Math.abs(numerator), denominator);
  // 返回结果
  return `${numerator / g}/${denominator / g}`;
}
```

```cpp
class Solution {
public:
    bool isDigit(char c) { return c >= '0' && c <= '9'; }
    int gcd(int a, int b) {
        if (b == 0) return a;
        return gcd(b, a % b);
    }
    string fractionAddition(string expression) {
        int n = expression.size(), numerator = 0, denominator = 1, idx = 0;
        while (idx < n) {
            int sign = 1;
            if (expression[idx] == '-' || expression[idx] == '+') {
                sign = expression[idx] == '-' ? -1 : 1;
                idx++;
            }

            int num = 0;
            while (idx < n && isDigit(expression[idx])) {
                num = num * 10 + (expression[idx] - '0');
                idx++;
            }
            num *= sign;

            idx++;

            int den = 0;
            while (idx < n && isDigit(expression[idx])) {
                den = den * 10 + (expression[idx] - '0');
                idx++;
            }

            numerator = numerator * den + num * denominator;
            denominator *= den;
        }

        if (numerator == 0) return "0/1";

        int g = gcd(abs(numerator), denominator);

        return to_string(numerator / g) + "/" + to_string(denominator / g);
    }
};
```

```py
class Solution:
    def fractionAddition(self, expression: str) -> str:
        n, idx = len(expression), 0
        numerator, denominator = 0, 1
        while idx < n:
            sign = 1
            if expression[idx] == '-' or expression[idx] == '+':
                sign = 1 if expression[idx] == '+' else -1
                idx += 1
            num = 0
            while idx < n and expression[idx].isdigit():
                num = num * 10 + int(expression[idx])
                idx += 1
            num *= sign
            idx += 1
            den = 0
            while idx < n and expression[idx].isdigit():
                den = den * 10 + int(expression[idx])
                idx += 1
            numerator = numerator * den + num * denominator
            denominator *= den

        if numerator == 0:
            return '0/1'
        g = gcd(abs(numerator), denominator)

        return str(numerator // g) + '/' + str(denominator // g)
```
