# 题目

运用你所掌握的数据结构，设计和实现一个 LRU (最近最少使用) 缓存机制 。

实现 `LRUCache` 类：

- `LRUCache(int capacity)` 以正整数作为容量 capacity 初始化 LRU 缓存
- `int get(int key)` 如果关键字 `key` 存在于缓存中，则返回关键字的值，否则返回` -1` 。
- `void put(int key, int value)` 如果关键字已经存在，则变更其数据值；如果关键字不存在，则插入该组「关键字-值」。当缓存容量达到上限时，它应该在写入新数据之前删除最久未使用的数据值，从而为新的数据值留出空间。

进阶：你是否可以在 `O(1)` 时间复杂度内完成这两种操作？

# 示例

```
输入
["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
输出
[null, null, null, 1, null, -1, null, -1, 3, 4]

解释
LRUCache lRUCache = new LRUCache(2);
lRUCache.put(1, 1); // 缓存是 {1=1}
lRUCache.put(2, 2); // 缓存是 {1=1, 2=2}
lRUCache.get(1);    // 返回 1
lRUCache.put(3, 3); // 该操作会使得关键字 2 作废，缓存是 {1=1, 3=3}
lRUCache.get(2);    // 返回 -1 (未找到)
lRUCache.put(4, 4); // 该操作会使得关键字 1 作废，缓存是 {4=4, 3=3}
lRUCache.get(1);    // 返回 -1 (未找到)
lRUCache.get(3);    // 返回 3
lRUCache.get(4);    // 返回 4
```

# 题解

## 哈希表

LRU 缓存机制，就是淘汰最少访问次数的数据。题目要求在 `O(1)` 的时间复杂度，哈希表负责读取数据，链表负责存储数据，这样都可以满足题目的需求。

需要注意的地方：

- 每次插入数据，检测时候有这个数据，有的话更新，没有的话新建节点，然后放到链表尾。最后需要检测容量，把超出容量的缓存删除。
- 获取数据的时候，需要把最新获取的数据，放到链表尾部，代表是最新数据。

```ts
class HashListNode {
  key: number;
  value: number;
  next: HashListNode;
  prev: HashListNode;
  constructor(
    key: number = 0,
    value: number = 0,
    next: HashListNode = null,
    prev: HashListNode = null
  ) {
    this.key = key;
    this.value = value;
    this.next = next;
    this.prev = prev;
  }
  insertBefore(node: HashListNode) {
    node.next = this;
    node.prev = this.prev;
    this.prev.next = node;
    this.prev = node;
  }
  remove(): HashListNode {
    this.prev.next = this.next;
    this.next.prev = this.prev;
    this.prev = this.next = null;
    return this;
  }
}

class HashList {
  head: HashListNode;
  tail: HashListNode;
  hash: Map<number, HashListNode>;
  capcity: number;
  constructor(capcity: number) {
    this.head = new HashListNode();
    this.tail = new HashListNode();
    this.head.next = this.tail;
    this.tail.prev = this.head;
    this.hash = new Map();
    this.capcity = capcity;
  }
  put(key: number, value: number): void {
    if (this.hash.has(key)) {
      this.hash.get(key).value = value;
      this.hash.get(key).remove();
    } else {
      this.hash.set(key, new HashListNode(key, value));
    }
    this.tail.insertBefore(this.hash.get(key));
    if (this.hash.size > this.capcity) {
      this.hash.delete(this.head.next.key);
      this.head.next.remove();
    }
  }

  get(key: number): number {
    if (this.hash.has(key)) {
      const node = this.hash.get(key);
      this.tail.insertBefore(node.remove());
      return node.value;
    } else {
      return -1;
    }
  }
}

class LRUCache {
  hashlist: HashList;
  constructor(capacity: number) {
    this.hashlist = new HashList(capacity);
  }

  get(key: number): number {
    return this.hashlist.get(key);
  }

  put(key: number, value: number): void {
    this.hashlist.put(key, value);
  }
}
```
