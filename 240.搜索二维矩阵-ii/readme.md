# 题目

编写一个高效的算法来搜索 `m x n` 矩阵 `matrix` 中的一个目标值 `target` 。该矩阵具有以下特性：

- 每行的元素从左到右升序排列。
- 每列的元素从上到下升序排列。

提示：

- `m == matrix.length`
- `n == matrix[i].length`
- `1 <= n, m <= 300`
- `-109 <= matix[i][j] <= 109`
- 每行的所有元素从左到右升序排列
- 每列的所有元素从上到下升序排列
- `-109 <= target <= 109`

# 示例

[![2HBVQs.png](https://z3.ax1x.com/2021/06/15/2HBVQs.png)](https://imgtu.com/i/2HBVQs)

```
输入：matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 5
输出：true
```

# 题解

## 二分查找

观察题目可以看出，矩阵右上角或者左下角相邻元素，符合二分性。

下面我们从右上角开始，小于当前元素的，列数减少，大于当前元素的，行数增加。直到找到元素或者超出边界，循环结束。

```ts
function searchMatrix(matrix: number[][], target: number): boolean {
  const m = matrix.length;
  const n = matrix[0].length;

  // 从右上角开始搜索
  let i = 0;
  let j = n - 1;
  while (i < m && j >= 0) {
    if (matrix[i][j] === target) return true;
    if (matrix[i][j] < target) i++;
    else j--;
  }

  return false;
}
```
