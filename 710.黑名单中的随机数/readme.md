# 题目

给定一个整数 `n` 和一个 无重复 黑名单整数数组 `blacklist` 。设计一种算法，从 `[0, n - 1]` 范围内的任意整数中选取一个 **未加入** 黑名单 `blacklist` 的整数。任何在上述范围内且不在黑名单 `blacklist` 中的整数都应该有 **同等的可能性** 被返回。

优化你的算法，使它最小化调用语言 内置 随机函数的次数。

实现 `Solution` 类:

- `Solution(int n, int[] blacklist)` 初始化整数 `n` 和被加入黑名单 `blacklist` 的整数
- `int pick()` 返回一个范围为 `[0, n - 1]` 且不在黑名单 `blacklist` 中的随机整数

提示:

- $1 \leq n \leq 109$
- $0 \leq blacklist.length \leq min(10^5, n - 1)$
- $0 \leq blacklist[i] < n$
- `blacklist` 中所有值都 **不同**
- `pick` 最多被调用 $2 * 10^4$ 次

# 示例

```
输入
["Solution", "pick", "pick", "pick", "pick", "pick", "pick", "pick"]
[[7, [2, 3, 5]], [], [], [], [], [], [], []]
输出
[null, 0, 4, 1, 6, 1, 0, 4]

解释
Solution solution = new Solution(7, [2, 3, 5]);
solution.pick(); // 返回0，任何[0,1,4,6]的整数都可以。注意，对于每一个pick的调用，
                 // 0、1、4和6的返回概率必须相等(即概率为1/4)。
solution.pick(); // 返回 4
solution.pick(); // 返回 1
solution.pick(); // 返回 6
solution.pick(); // 返回 1
solution.pick(); // 返回 0
solution.pick(); // 返回 4
```

# 题解

## 哈希表

题意要求最小化调用语言内置随机函数的次数，因此我们的随机范围只能在非黑名单整数范围内。

假设 m 为黑名单的长度，那么随机范围只能在 [0, n-m) 内。

我们可以将 [0, n-m) 内的黑名单使用哈希表去映射到 [n - m, n-1] 中的白名单整数。这样就保证了随机数的范围。

```ts
class Solution {
  map: Map<number, number> = new Map();
  black: Set<number> = new Set();
  bound: number;
  constructor(n: number, blacklist: number[]) {
    this.bound = n - blacklist.length;
    // 超过随机范围的黑名单
    for (const b of blacklist) {
      if (b >= this.bound) this.black.add(b);
    }

    let val = this.bound;
    // 超出随机范围数字跟黑名单数字做映射
    for (const b of blacklist) {
      if (b < this.bound) {
        while (this.black.has(val)) {
          val++;
        }
        this.map.set(b, val);
        val++;
      }
    }
  }

  pick(): number {
    const rand = Math.floor(Math.random() * this.bound);
    return this.map.get(rand) || rand;
  }
}
```
