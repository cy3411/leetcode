# 题目

给定一个 `N` 叉树，找到其最大深度。

最大深度是指从根节点到最远叶子节点的最长路径上的节点总数。

`N` 叉树输入按层序遍历序列化表示，每组子节点由空值分隔（请参见示例）。

提示：

- 树的深度不会超过 `1000` 。
- 树的节点数目位于 $[0, 10^4]$ 之间。

# 示例

[![b7KvNT.png](https://s1.ax1x.com/2022/03/12/b7KvNT.png)](https://imgtu.com/i/b7KvNT)

```
输入：root = [1,null,3,2,4,null,5,6]
输出：3
```

[![b7MEE6.png](https://s1.ax1x.com/2022/03/12/b7MEE6.png)](https://imgtu.com/i/b7MEE6)
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：5

# 题解

## 递归

定义 F(n)表示返回当前 n 节点的高度，递归调用取返回的最大结果即可。

```ts
function maxDepth(root: Node | null): number {
  if (!root) return 0;
  let max = 0;
  for (let i = 0; i < root.children.length; i++) {
    max = Math.max(max, maxDepth(root.children[i]));
  }
  return max + 1;
}
```

```cpp
class Solution
{
public:
    int maxDepth(Node *root)
    {
        if (root == NULL)
        {
            return 0;
        }
        int ans = 0;
        for (auto child : root->children)
        {
            ans = max(ans, maxDepth(child));
        }

        return ans + 1;
    }
};
```
