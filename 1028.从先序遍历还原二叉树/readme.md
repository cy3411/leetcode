# 题目

我们从二叉树的根节点 `root` 开始进行深度优先搜索。

在遍历中的每个节点处，我们输出 `D` 条短划线（其中 `D` 是该节点的深度），然后输出该节点的值。（如果节点的深度为 `D`，则其直接子节点的深度为 `D + 1`。根节点的深度为 `0`）。

如果节点只有一个子节点，那么保证该子节点为左子节点。

给出遍历输出 `S`，还原树并返回其根节点 `root`。

提示：

- 原始树中的节点数介于 `1` 和 `1000` 之间。
- 每个节点的值介于 `1` 和 $\color{burlywood} 10 ^ 9$ 之间。

# 示例

[![HNrN80.png](https://s4.ax1x.com/2022/02/10/HNrN80.png)](https://imgtu.com/i/HNrN80)

```
输入："1-2--3--4-5--6--7"
输出：[1,2,5,3,4,6,7]
```

[![HNrrVJ.png](https://s4.ax1x.com/2022/02/10/HNrrVJ.png)](https://imgtu.com/i/HNrrVJ)

```
输入："1-2--3---4-5--6---7"
输出：[1,2,5,3,null,6,null,4,null,7]
```

# 题解

## 栈

根据题意，字符串中的数字表示节点，短划线表示树的深度。

使用栈模拟树的访问过程，树的深度需要跟栈的深度保持一致，这样就可以对应上各自的父节点。

遍历字符串，先计算当前深度，然后计算出当前节点的值。如果栈中的深度大于当前深度，则需要弹出栈中的节点，直到栈中的深度等于当前深度。这样如果栈不为空，栈顶元素就是当前节点的父节点，将当前节点挂载上去。
然后将当前节点压入栈中。

最后只要返回栈底的节点即可。

```ts
function recoverFromPreorder(traversal: string): TreeNode | null {
  // 栈
  const n = traversal.length;
  const stk = [];
  let i = 0;
  // 树的深度，也就是"-"字符的数量
  let deep = 0;
  // 将数字字符转换为数字
  let num = 0;
  let node: TreeNode;

  while (i < n) {
    deep = num = 0;
    // 当前节点的深度
    while (traversal[i] == '-' && i < n) {
      i++;
      deep++;
    }
    // 当前节点的值
    while (traversal[i] !== '-' && i < n) {
      num = num * 10 + Number(traversal[i]);
      i++;
    }

    // 栈中元素数量大于深度，就将栈顶弹出，直到匹配深度
    while (stk.length > deep) {
      stk.pop();
    }
    node = new TreeNode(num);
    // 将当前节点挂载到栈顶的左右子节点上
    if (stk.length > 0) {
      const top = stk[stk.length - 1];
      if (top.left === null) {
        top.left = node;
      } else {
        top.right = node;
      }
    }
    stk.push(node);
  }

  // 返回栈底节点
  return stk[0];
}
```

```cpp
class Solution
{
public:
    TreeNode *recoverFromPreorder(string traversal)
    {
        int i = 0, deep, num;
        stack<TreeNode *> stk;
        TreeNode *node;

        while (traversal[i])
        {
            deep = num = 0;
            while (traversal[i] == '-')
            {
                i++;
                deep++;
            }
            while (traversal[i] != '-' && traversal[i])
            {
                num = num * 10 + (traversal[i] - '0');
                i++;
            }
            while (stk.size() > deep)
            {
                stk.pop();
            }
            node = new TreeNode(num);
            if (!stk.empty())
            {
                if (stk.top()->left == NULL)
                {
                    stk.top()->left = node;
                }
                else
                {
                    stk.top()->right = node;
                }
            }
            stk.push(node);
        }
        // 取出栈底元素
        while (!stk.empty())
        {
            node = stk.top();
            stk.pop();
        }

        return node;
    }
};
```
