# 题目

给你一个整数数组 nums ，你需要找出一个 连续子数组 ，如果对这个子数组进行升序排序，那么整个数组都会变为升序排序。

请你找出符合题意的 最短 子数组，并输出它的长度。

提示：

- 1 <= nums.length <= 104
- -105 <= nums[i] <= 105

进阶：你可以设计一个时间复杂度为 O(n) 的解决方案吗？

# 示例

```
输入：nums = [2,6,4,8,10,9,15]
输出：5
解释：你只需要对 [6, 4, 8, 10, 9] 进行升序排序，那么整个表都会变为升序排序。
```

# 题解

## 一次遍历

对于数组我们可以分为 A，B,C 三个部分。

A 中所有的元素肯定小于等于 B 中最小的元素。

C 中所有的元素肯定大于等于 B 中最大的元素。

定义 B 的区间是[left,right]。

我们可以倒叙遍历，一边更新最小元素，一边查找第一个大于最小元素的位置。这样就可以确定 left 的位置 。

我们可以用类似的方法确定 right 的位置。

确定了最有边界，就可以返回 B 的长度了。

```ts
function findUnsortedSubarray(nums: number[]): number {
  let max = Number.NEGATIVE_INFINITY;
  let min = Number.POSITIVE_INFINITY;
  let left = -1;
  let right = -1;
  let m = nums.length;
  for (let i = 0; i < m; i++) {
    // 同步更新min和第一个大于等于min的位置来确定左边界
    if (min < nums[m - 1 - i]) {
      left = m - 1 - i;
    } else {
      min = nums[m - 1 - i];
    }
    // 同步更新max和第一个小于等于max的位置来确定右边界
    if (max > nums[i]) {
      right = i;
    } else {
      max = nums[i];
    }
  }

  return left === -1 ? 0 : right - left + 1;
}
```

## 单调栈

```ts
function findUnsortedSubarray(nums: number[]): number {
  const stack = [];
  const m = nums.length;
  let left = Number.POSITIVE_INFINITY;
  let right = Number.NEGATIVE_INFINITY;

  // 单调递增栈，找left
  for (let i = 0; i < m; i++) {
    while (stack.length && nums[i] < nums[stack[stack.length - 1]]) {
      left = Math.min(left, stack.pop());
    }
    stack.push(i);
  }
  stack.length = 0;
  // 单调递减栈，找right
  for (let i = m - 1; i >= 0; i--) {
    while (stack.length && nums[i] > nums[stack[stack.length - 1]]) {
      right = Math.max(right, stack.pop());
    }
    stack.push(i);
  }

  return isFinite(left) ? right - left + 1 : 0;
}
```
