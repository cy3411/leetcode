# 题目

给你二叉搜索树的根节点 `root` ，该树中的两个节点被错误地交换。请在不改变其结构的情况下，恢复这棵树。

进阶：使用 O(n) 空间复杂度的解法很容易实现。你能想出一个只使用常数空间的解决方案吗？

提示：

- 树上节点的数目在范围 `[2, 1000]` 内
- `-231 <= Node.val <= 231 - 1`

# 示例

[![hJOvy6.png](https://z3.ax1x.com/2021/08/30/hJOvy6.png)](https://imgtu.com/i/hJOvy6)

```
输入：root = [1,3,null,null,2]
输出：[3,1,null,null,2]
解释：3 不能是 1 左孩子，因为 3 > 1 。交换 1 和 3 使二叉搜索树有效。
```

# 题解

## 中序遍历

二叉搜索树的特性，中序遍历是单调递增的。

如果中间有 2 个节点被错误交换，那么肯定有 2 个点不符合单调递增。中序遍历过程中，找到不符合的 2 个节点，交换后即可。

例如:

```
[1,2,3,4,7,6,5,8]

可以看出5和7是错误交换，可以看出76、65都是不符合递增的，找到第一个组合的第一个节点，和第二个组合的第二个节点。
```

```ts
function recoverTree(root: TreeNode | null): void {
  // 中序遍历，找到2组前面大于后面的组合
  let prev: TreeNode | null = null;
  let p: TreeNode | null = null;
  let q: TreeNode | null = null;
  // 中序遍历，找到需要交换的2个节点
  const inorder = (node: TreeNode | null) => {
    if (node === null) return;
    inorder(node.left);
    if (prev !== null && prev.val > node.val) {
      if (p === null) p = prev;
      q = node;
    }
    prev = node;
    inorder(node.right);
  };

  inorder(root);
  // 交换节点值
  let temp = p.val;
  p.val = q.val;
  q.val = temp;
}
```
